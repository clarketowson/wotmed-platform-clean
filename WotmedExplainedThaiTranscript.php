<?php
	/*if ($_SERVER['HTTPS'] != "on") {
		$url = "https://". $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
		header("location: $url");
	}  */

	session_start();
	include("includes/connect.php");
	include("includes/functions.php");
	
	if(isset($_SESSION['id']))
	{
		$redirect="profile.php";
		if(isset($_SESSION['practitioner_id']))
			$redirect='practitioner_profile.php';
		redirect($redirect);
	}
	elseif(isset($_POST['email']))
	{
		$inputtext=$_POST['inputtext'];
	$inputpassword=$_POST['inputpassword'];
		login($conn,$inputtext,$inputpassword);
	}
?>



<!DOCTYPE html PUBLIC
"-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
		<title>Welcome to Wotmed</title>
		<meta http-equiv="Content-Type" content="'text'/html; charset=utf-8" />
        
        
 <script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-38343871-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-49954656-1', 'wotmed.com');
		  ga('send', 'pageview');

		</script>

       
		<script type="text/javascript" src="classes/jquery.js"></script>
		<script type="text/javascript" src="classes/hover.js"></script>
		<script src="classes/jquery.validation.js"></script>
		<script src="classes/dateSelectBoxes.js"></script>

    <link href="style/i_style.css" rel="stylesheet"></link>

    <link rel="stylesheet" type="text/css" href="signup.css">

    <link rel="stylesheet" href="/proposals/date/css/pikaday.css">

 <!--   <link rel="stylesheet" href="/proposals/date/css/site.css"> -->


 <!--   <style type="text/css">
        <!--
        #PatientBudget {	font-size:14px;
        }
        #PatientCountries {	font-size:14px;
        }
        #ProcedureList {
            font-size:14px;
        }
        #Submit {	background-color:#125c5d;
            -moz-border-radius:1px;
            -webkit-border-radius:1px;
            border-radius:1px;
            display:inline-block;
            cursor:pointer;
            color:#ffffff;
            font-family:arial;
            font-size:22px;
            padding:12px 50px;
            text-decoration:none;
            text-shadow:0px 1px 0px #125c5d;
        }

    </style> -->



    <meta name="description" content="Wotmed.com - Packaging Medical Tourism">
    <meta name="keywords" content="Wotmed.com - Packaging Medical Tourism.  Cheap Overseas Dental Care, Cheap overseas Dentists, Cheap Overseas Doctors, Overseas Cosmetic Surgery, Overseas Surgery, Overseas Dental Surgery, Tummy Tucks overseas, Boob Jobs overseas, Breast Augmentation overseas, Mummy Makeovers">

    <meta property="og:locale" content="en_US">
    <meta property="og:type" content="website">
    <meta property="og:title" content="Welcome to Wotmed">
    <meta property="og:description" content="Wotmed.com - Packaging Medical Tourism">
    <meta property="og:site_name" content="Wotmed">
    <meta property="og:url" content="http://platform.wotmed.com">

    <meta property="og:image" content="http://platform.wotmed.com/WotmedFacebookLogo6.png">
        
	</head>
	<body onload="searchUsers()">
		<div class="topBar" >
			<div class="bar_frame">
				<div class="w_logo"><img src="images/WotmedLogoTransparent.png" height="45" alt="wotmedLogo" style='position:relative; left:-30px; padding:0px;padding-top:10px;' /></div>
				<div class="div_login">
					<form id="loginForm" method="post" action="login.php">
					<table>
						<tr>
							<td><label>Email</label></td>
							<td><label>Password</label></td>
						</tr>
						<tr>
							<td width="150"><input type="text" class="inputtext required email" name="email" id="email"/></td>
							<td width="150"><input type="password" class="inputpassword required" name="password" id="password"/></td>
							<td><input type="submit" value="Login"/></td>
						</tr>
						<tr>
							<td><input name="persistent" type="checkbox" style="float:left; margin: 0px; padding: 0px;"/> <label>Keep me logged in</label></td>
							<td><a href="resetPassword.php"><label style="cursor:pointer;text-decoration:underline;">Forget your password?</label></a></td>
                            
                           
						</tr>
                        
					</table>
					</form>
				<div id="errMsg">
				</div>
				</div>
				<div>
					<?php //include "fbConnect.php"; ?>
				</div>
				<div>
					<?php //include "googleConnect.php"; ?>
				</div>
			</div>
		</div>



		<div id="container">



            <div id="system">


                <article class="item">




                    <div class="content clearfix">





                        <div class="contents">
                             <p style="text-align: center;">&nbsp;</p>
                            <p style="text-align: center;">&nbsp;</p>
                            <h2 style="text-align: center;"><strong>Wotmed.com อธิบาย วีดีโอ สำเนา</strong></h2>
                            <p>&nbsp;</p>
                            <p>&nbsp;</p>
                            <p>วันนี้ ผู้คนมากขึ้น การเดินทาง ต่างประเทศ สำหรับ ทางการแพทย์ ทันตกรรม และ อื่น ๆ ชนิด ของ การดูแลสุขภาพ การบริการ กว่า เคย</p>
                            <p><br />
                            </p>
                            <p>การดูแลสุขภาพอย่างมืออาชีพ และ อำนวยความสะดวกในการผ่าตัด ตั้งอยู่ ใน ประเทศที่กำลังพัฒนา เป็น กระตือรือร้น ไปยัง ให้ความบันเทิง ไปยัง นี้ ใหญ่ และ ที่เพิ่มมากขึ้น ของลูกค้า ฐาน</p>
                            <p><br />
                            </p>
                            <p>ในปัจจุบัน เดินทางทางการแพทย์ ไป มัน คนเดียว หรือ ใช้ ในประเทศ การท่องเที่ยวทางการแพทย์ การอำนวยความสะดวก ผู้ให้บริการ ไปยัง ใส่ พวกเขา ใน การติดต่อ กับ ต่างประเทศ ผู้เชี่ยวชาญด้านการดูแลสุขภาพ</p>
                            <p><br />
                            </p>
                            <p>ปัญหา ด้วย นี้ เข้าใกล้ เป็น เดินทางทางการแพทย์ ไม่ มี มาก ข้อมูล และ เป็น ความพยายามที่ ไปยัง การวิจัย ผู้เชี่ยวชาญด้านการดูแลสุขภาพ บน ของพวกเขา ด้วยตัวเอง ซึ่ง เป็น ยาก และ เวลา การบริโภค</p>
                            <p><br />
                            </p>
                            <p>การแพทย์การเลือกของนักท่องเที่ยว ไม่ ทราบ ซึ่ง ผู้เชี่ยวชาญด้านการดูแลสุขภาพ ต่างประเทศ พวกเขา สามารถ วางใจ และ เป็น การ ความเสี่ยงที่มีขนาดใหญ่ โดย การทำ ไส้ตัดสินใจ กับ อะไรก็ตาม ข้อมูล พวกเขา สามารถ หา<br />
                            </p>
                            <p>ดังนั้น สิ่งที่ เป็น คำตอบ ไปยัง นี้ ปัญหา?</p>
                            <p><br />
                            </p>
                            <p>อย่างไร สามารถ เดินทางทางการแพทย์ ได้รับ ถูกต้อง ความเข้าใจ ของ ต่างประเทศ ผู้เชี่ยวชาญด้านการดูแลสุขภาพ?</p>
                            <p><br />
                            </p>
                            <p>อย่างไร สามารถ ผู้เชี่ยวชาญด้านการดูแลสุขภาพ โฆษณา และ ขาย ของพวกเขา การบริการ ไปยัง เดินทางทางการแพทย์?</p>
                            <p><br />
                            </p>
                            <p>อย่างไร สามารถ อำนวยความสะดวกในการผ่าตัด ช่วยเหลือ เดินทางทางการแพทย์ ไปยัง ตอบสนองความ ของพวกเขา การท่องเที่ยวทางการแพทย์ ความต้องการ?</p>
                            <p><br />
                            </p>
                            <p>Wotmed.com เป็น คำตอบ</p>
                            <p><br />
                            </p>
                            <p>Wotmed.com เป็น เครือข่ายทางการแพทย์ ว่า ต่อ เดินทางทางการแพทย์ ด้วย ผู้เชี่ยวชาญด้านการดูแลสุขภาพ และ อำนวยความสะดวกในการผ่าตัด ตั้งอยู่ ใน ใด ประเทศ ของ โลก</p>
                            <p><br />
                            </p>
                            <p>ผู้เชี่ยวชาญด้านการดูแลสุขภาพ สร้าง Wotmed.com โปรไฟล์ การโฆษณา ของพวกเขา การบริการ ไปยัง</p>
                            <p>&nbsp;</p>
                            <p> เดินทางทางการแพทย์ ตาม กับ ของพวกเขา ทักษะ, คุณสมบัติ, การศึกษา และ อื่น ๆ สำคัญ ข้อมูล ว่า </p>
                            <p>&nbsp;</p>
                            <p>เดินทางทางการแพทย์ จำเป็นต้อง ไปยัง ทำ ข้อมูลประกอบการตัดสินใจ เกี่ยวกับ อยู่ระหว่างการ ศัลยกรรม ต่างประเทศ</p>
                            <p><br />
                            </p>
                            <p>อำนวยความสะดวกในการผ่าตัด สร้าง Wotmed.com โปรไฟล์ การโฆษณา ของพวกเขา การอำนวยความสะดวก การบริการ ไปยัง เดินทางทางการแพทย์ เช่น ตอบสนองความ เดินทาง ที่ สนามบิน, เอา พวกเขา ไปยัง โรงแรม และ แนะนำ พวกเขา ใน ต่างประเทศ</p>
                            <p><br />
                            </p>
                            <p>ถ้า จำเป็นต้อง ช่วย Wotmed.com มี ฟรี โทรศัพท์ และ อีเมล์ สนับสนุน เพียงแค่ โทรศัพท์ หรือ คลิก <br />
                            </p>
                            <p>ผู้ป่วย - ลงทะเบียน วันนี้ และ เริ่มต้น การค้นคว้า ต่างประเทศ ผู้เชี่ยวชาญด้านการดูแลสุขภาพ ก่อนที่ คุณ การเดินทาง</p>
                            <p><br />
                            </p>
                            <p>ผู้เชี่ยวชาญด้านการดูแลสุขภาพ ลงทะเบียน วันนี้ และ เริ่มต้น ขาย ของคุณ ทางการแพทย์, ทันตกรรม และ อื่น ๆ การดูแลสุขภาพ การบริการ ไปยัง ผู้ป่วย ทั่วโลก</p>
                            <p><br />
                            </p>
                            <p>อำนวยความสะดวกในการผ่าตัด ลงทะเบียน วันนี้ ไปยัง ช่วย ผู้ป่วย การเดินทาง ไปยัง ของคุณ ประเทศ สำหรับ บริการดูแลสุขภาพ</p>
                            <p><br />
                            </p>
                            <p>ด้วย ฟรี หนึ่งปี การทดลอง ของ Wotmed.com ไปยัง ผู้เชี่ยวชาญด้านการดูแลสุขภาพ และ ฟรี ลงทะเบียน สำหรับ ทุกคน, อะไร คุณ มี ไปยัง สูญเสีย?</p>
                            <p><br />
                            </p>
                            <p>ลงทะเบียน วันนี้ และ ช่วย เรา ให้อำนาจ คุณ ไปยัง ทำ ธุรกิจ ด้วย กันและกัน บน หนึ่ง ส่วนกลาง เวที คุณ สามารถ วางใจ: Wotmed.com<br />
                            </p>
                            <p></p>
                            <p></p>
                            <p></p>
                            <p style="text-align: center;">&nbsp;</p>
                            <p>&nbsp;</p>
                            <p>การติดต่อ:<br />
                            </p>
                            <p>&nbsp;</p>
                            <p>Clarke Towson<br />
                                Wotmed CTO<br />

                            <p class="signup thai text">
                                <br>
                                <a href="reg-practitioner.php"
                                    >แพทย์ลงทะเบียน</a>
                                <br><br>

                                <a href="reg-facilitator.php"
                                    >อำนวยความสะดวกลงทะเบียน</a>
                            </p>

                            <p>&nbsp;</p>
                        </div>



                    </div>








            <p>&nbsp;</p>



        </div>


			<p>&nbsp;</p>
			<p>&nbsp;</p>
			<?php include "includes/p_footer.php"; ?>
