<?php
	session_start();
	include "includes/connect.php";
	include "includes/functions.php";
	include "classes/SimpleImage.php";
	

		
	
	
	$rowSession=getParticipantDetail($conn,$_SESSION['id']);
	$row=getPractitionerDetail($conn,$_SESSION['id']);
	$ppFileNameSession="blankSilhouetteMale.png";
	$ppFileName="blankSilhouetteMale.png";
	
	
	
      
// if the logged in user is a surgery facilitator then we want to show the profile photo at left as the business logo
// otherwise the user is a practitioner and we show the profile photo on the left as the practitioner photo

if($row['ISFACILITATOR'] == 1)
{
	if($row['PRACTITIONER_BUSINESSLOGO']=="")
	{
			$ppFileNameSession="blankSilhouetteMale.png";
	}else
	
		{
			$ppFileNameSession=$row['PRACTITIONER_BUSINESSLOGO'];
		}
	
	
	
}

else
{
	if($row['PRACTITIONER_BUSINESSLOGO']!="")
	{
		$ppFileNameSession=$row['PRACTITIONER_BUSINESSLOGO'];
		$ppFileName=$row['PRACTITIONER_BUSINESSLOGO'];
	}
	if($rowSession['PROFILEPHOTO']!="")
	{
		$ppFileNameSession=$rowSession['PROFILEPHOTO'];
	}
	
}


	
	
	
	
	
	$query="SELECT COUNT(*) FROM PATIENTPRACTITIONERRELATIONSHIP WHERE PRACTITIONER_NUMBER = '" . $_SESSION['practitioner_id'] . "' AND RECOMMENDEDAS='1'";
	$tempOfRecommend=mysqli_query($conn,$query);
	if(mysqli_num_rows($tempOfRecommend)!=0)
		$numbOfRecommend=mysqli_fetch_array($tempOfRecommend);
	else
		$numbOfRecommend[]=0;
	if(isset($_POST) && $_POST!=NULL)
	{	
		if($_POST['status']=="insert"){			
		$id=$_SESSION['practitioner_id'];
		$day=mysqli_real_escape_string($conn,$_POST['insertDay']);
		$startHour=mysqli_real_escape_string($conn,$_POST['insertStartHour']);
		$endHour=mysqli_real_escape_string($conn,$_POST['insertEndHour']);
		$duration=mysqli_real_escape_string($conn,$_POST['insertDuration']);
			$query="INSERT INTO AVAILABILITY (PRACTITIONER_NUMBER,DAY,TIMESTART,TIMEEND,DURATION) VALUES ('" . $id . "','" . $day . "','" . $startHour . "','" . $endHour . "','" . $duration . "');";
			mysqli_query($conn,$query);
		}else
		{
			$id=$_SESSION['practitioner_id'];
			$day=mysqli_real_escape_string($conn,$_POST['day']);
			$startHour=mysqli_real_escape_string($conn,$_POST['startHour']);
			$endHour=mysqli_real_escape_string($conn,$_POST['endHour']);
			$duration=mysqli_real_escape_string($conn,$_POST['duration']);
			if($_POST['status']=="update"){
				$availabilityNumber=mysqli_real_escape_string($conn,$_POST['availabilityNumber']);
				$query="UPDATE AVAILABILITY SET DAY ='" . $day . "', TIMESTART ='" . $startHour . "', TIMEEND ='" . $endHour . "',DURATION ='" . $duration . "' WHERE AVAILABILITY_NUMBER ='" . $availabilityNumber . "'";
				mysqli_query($conn,$query);
			}
			if($_POST['status']=="delete"){
				$availabilityNumber=mysqli_real_escape_string($conn,$_POST['availabilityNumber']);
				$query="DELETE FROM AVAILABILITY WHERE AVAILABILITY_NUMBER='" . $availabilityNumber . "'";
				mysqli_query($conn,$query);
			}
		}
		?>
		<script language="javascript"> 
			<?php echo "window.location = 'mySchedule.php'";?>
		</script>
		<?php
	}
	$details=getDetails($conn,$row["PRACTITIONER_NUMBER"],13);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link href="<?php echo $path; ?>style/p_style.css" rel="stylesheet"></link>
<script type="text/javascript" src="<?php echo $path; ?>classes/jquery.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>My Schedule</title>
<link href="style/apple.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
	function back()
	{
		window.location = "practitioner_profile.php"
	}
	function updateScheduleDetails(scheduleDetails)
	{
		if (window.XMLHttpRequest)
		{// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp=new XMLHttpRequest();
		}
		else
		{// code for IE6, IE5
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange=function()
		{
			if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				var detail=xmlhttp.responseText.split("-"); 
				document.getElementById("day").selectedIndex=detail[0];
				document.getElementById("startHour").value=detail[1];
				document.getElementById("endHour").value=detail[2];
				document.getElementById("duration").value=detail[3];
				document.getElementById("availabilityNumber").value=detail[4];
			}
		}
		xmlhttp.open("GET","ajax/updateScheduleDetails.php?details="+scheduleDetails,true);
		xmlhttp.send();
	}
</script>
</head>

<body>
<?php include $path."includes/p_header.php"; ?>
<?php
if($row['ISFACILITATOR'] == 1){
	$backLink = "
		<a href='facilitator/cpanel.php'>Back to Your Control Panel</a>
	";
	include $path."facilitator/cpanelHeader.php"; 
}
else{
	$backLink = "
		<a href='cpanel.php'>Back to Your Control Panel</a>
	";
	include $path."cpanelHeader.php"; 
}
?>
<div class='lfloat' style='width:98%'>
	<div class="stdWrapper" onMouseOver="this.style.background='white';"onmouseout="this.style.background='#F0F0F0';">
	  <p><span class="PractitionerBody"><span class="copyrightText"><img src="images/PractitionerNews.png" alt="" width="72" height="72" /></span><h3 class="hyperlinks">Create New Schedule</h3></span></p>
	  <p class="PractitionerMainText">Create schedule here</p>	
	
	  <form action="" method="post" enctype="multipart/form-data" name="CreatePractitionerService" id="form1">
		<input type="hidden" name="status" value="insert">
		<table width="789" border="0">
		  <tr>
			<td style='width:250px'><span class="PractitionerMainText">Days </span></td>
			<td><span class="PractitionerMainText">
				<select name='insertDay' id='insertDay'>
					<option value='0'>Sunday</option>
					<option value='1'>Monday</option>
					<option value='2'>Tuesday</option>
					<option value='3'>Wednesday</option>
					<option value='4'>Thursday</option>
					<option value='5'>Friday</option>
					<option value='6'>Saturday</option>
				</select>
			</span></td>
		  </tr>
		  <tr>
			<td><span class="PractitionerMainText">Time Start (24 Hours Format)</span></td>
			<td><span class="PractitionerMainText">
				<input type="text" name='insertStartHour' id='insertStartHour'>
			</span></td>
		  </tr>
		  <tr>
			<td><span class="PractitionerMainText">End Start (24 Hours Format)</span></td>
			<td><span class="PractitionerMainText">
				<input type="text" name='insertEndHour' id='insertEndHour'>
			</span></td>
		  </tr>
		  <tr>
			<td><span class="PractitionerMainText">Duration (in minutes) </span></td>
			<td><input type="text" name='insertDuration' id='insertDuration'></td>
		  </tr>
		</table>
		<p>
		  <input type="submit" name="UploadBlog" id="UploadBlog" value="Create Schedule" />
		</p>
	  </form>
	</div>
	<div class="stdWrapper" onMouseOver="this.style.background='white';"onmouseout="this.style.background='#F0F0F0';">
	  <p><span class="PractitionerBody"><span class="copyrightText"><img src="images/PractitionerNews.png" alt="" width="72" height="72" /></span><h3 class="hyperlinks">Update Schedule</h3></span></p>
	  <p class="PractitionerMainText">Update  your schedule here</p>
	  <form action="" method="post" enctype="multipart/form-data" name="UpdatePractitionerPromotion" id="UpdatePractitionerPromotion">
		<input type="hidden" name="status" id="status" value="update">
		<table width="789" border="0">
		<?php
		$availNumb="";
		echo "<tr><td><span class='PractitionerMainText'>Your schedule </span></td>";
		echo "<td><span class='PractitionerMainText'>";
		echo "<select name='schedule' id='schedule' onchange=\"updateScheduleDetails(this.value)\" >"; 
			$query="SELECT * FROM AVAILABILITY WHERE PRACTITIONER_NUMBER = '" . $_SESSION['practitioner_id'] . "'";
			$result=mysqli_query($conn,$query);
			while($availability=mysqli_fetch_array($result)){
				if($availNumb=="")
				{
					$availNumb=$availability[0];
					$tempDay=$availability[2];
					$tempTimeStart=$availability[3];
					$tempEndTime=$availability[4];
					$tempDuration=$availability[5];
				}
				Switch($availability[2])
				{
					case 0:
						echo "<option value='" . $availability[2] . "-" . $availability[3] . "-" . $availability[4] . "-" . $availability[5] . "-" . $availability[0] . "'>Sunday : " . $availability[3] . " - " . $availability[4] . " Duration : " . $availability[5] . " Mins </option>";
						break;
					case 1:
						echo "<option value='" . $availability[2] . "-" . $availability[3] . "-" . $availability[4] . "-" . $availability[5] . "-" . $availability[0] . "'>Monday : " . $availability[3] . " - " . $availability[4] . " Duration : " . $availability[5] . " Mins </option>";
						break;
					case 2:
						echo "<option value='" . $availability[2] . "-" . $availability[3] . "-" . $availability[4] . "-" . $availability[5] . "-" . $availability[0] . "'>Tuesday : " . $availability[3] . " - " . $availability[4] . " Duration : " . $availability[5] . " Mins </option>";
						break;
					case 3:
						echo "<option value='" . $availability[2] . "-" . $availability[3] . "-" . $availability[4] . "-" . $availability[5] . "-" . $availability[0] . "'>Wednesday : " . $availability[3] . " - " . $availability[4] . " Duration : " . $availability[5] . " Mins </option>";
						break;
					case 4:
						echo "<option value='" . $availability[2] . "-" . $availability[3] . "-" . $availability[4] . "-" . $availability[5] . "-" . $availability[0] . "'>Thursday : " . $availability[3] . " - " . $availability[4] . " Duration : " . $availability[5] . " Mins </option>";
						break;
					case 5:
						echo "<option value='" . $availability[2] . "-" . $availability[3] . "-" . $availability[4] . "-" . $availability[5] . "-" . $availability[0] . "'>Friday : " . $availability[3] . " - " . $availability[4] . " Duration : " . $availability[5] . " Mins </option>";
						break;
					case 6:
						echo "<option value='" . $availability[2] . "-" . $availability[3] . "-" . $availability[4] . "-" . $availability[5] . "-" . $availability[0] . "'>Saturday : " . $availability[3] . " - " . $availability[4] . " Duration : " . $availability[5] . " Mins </option>";
						break;
				}
			}
		echo "</select><br>";
		echo "</span></td>";
		echo "<input type='hidden' name='availabilityNumber' id='availabilityNumber' value='$availNumb'>"; 
		?>
		  <tr>
			<td><span class="PractitionerMainText">Days </span></td>
			<td><span class="PractitionerMainText">
				<select name='day' id='day'>
				<?php
				switch($tempDay)
				{
					case 0;
						echo "<option value='0' selected>Sunday</option>";
						echo "<option value='1'>Monday</option>";
						echo "<option value='2'>Tuesday</option>";
						echo "<option value='3'>Wednesday</option>";
						echo "<option value='4'>Thursday</option>";
						echo "<option value='5'>Friday</option>";
						echo "<option value='6'>Saturday</option>";
						break;
					case 1;
						echo "<option value='0'>Sunday</option>";
						echo "<option value='1' selected>Monday</option>";
						echo "<option value='2'>Tuesday</option>";
						echo "<option value='3'>Wednesday</option>";
						echo "<option value='4'>Thursday</option>";
						echo "<option value='5'>Friday</option>";
						echo "<option value='6'>Saturday</option>";
						break;
					case 2;
						echo "<option value='0'>Sunday</option>";
						echo "<option value='1'>Monday</option>";
						echo "<option value='2' selected>Tuesday</option>";
						echo "<option value='3'>Wednesday</option>";
						echo "<option value='4'>Thursday</option>";
						echo "<option value='5'>Friday</option>";
						echo "<option value='6'>Saturday</option>";
						break;
					case 3;
						echo "<option value='0'>Sunday</option>";
						echo "<option value='1'>Monday</option>";
						echo "<option value='2'>Tuesday</option>";
						echo "<option value='3' selected>Wednesday</option>";
						echo "<option value='4'>Thursday</option>";
						echo "<option value='5'>Friday</option>";
						echo "<option value='6'>Saturday</option>";
						break;
					case 4;
						echo "<option value='0'>Sunday</option>";
						echo "<option value='1'>Monday</option>";
						echo "<option value='2'>Tuesday</option>";
						echo "<option value='3'>Wednesday</option>";
						echo "<option value='4' selected>Thursday</option>";
						echo "<option value='5'>Friday</option>";
						echo "<option value='6'>Saturday</option>";
						break;
					case 5;
						echo "<option value='0'>Sunday</option>";
						echo "<option value='1'>Monday</option>";
						echo "<option value='2'>Tuesday</option>";
						echo "<option value='3'>Wednesday</option>";
						echo "<option value='4'>Thursday</option>";
						echo "<option value='5' selected>Friday</option>";
						echo "<option value='6'>Saturday</option>";
						break;
					case 6;
						echo "<option value='0'>Sunday</option>";
						echo "<option value='1'>Monday</option>";
						echo "<option value='2'>Tuesday</option>";
						echo "<option value='3'>Wednesday</option>";
						echo "<option value='4'>Thursday</option>";
						echo "<option value='5'>Friday</option>";
						echo "<option value='6' selected>Saturday</option>";
						break;
				}?>
				</select>
			</span></td>
		  </tr>
		  <tr>
			<td><span class="PractitionerMainText">Time Start </span></td>
			<td><span class="PractitionerMainText">
				<input type="text" name='startHour' id='startHour' value='<?php echo $tempTimeStart; ?>'>
			</span></td>
		  </tr>
		  <tr>
			<td><span class="PractitionerMainText">End Start </span></td>
			<td><span class="PractitionerMainText">
				<input type="text" name='endHour' id='endHour' value='<?php echo $tempEndTime; ?>'>
			</span></td>
		  </tr>
		  <tr>
			<td><span class="PractitionerMainText">Duration(in minutes) </span></td>
			<td>
				<input type="text" name='duration' id='duration' value='<?php echo $tempDuration; ?>'>
			</td>
		  </tr>
		</table>
		<p class="PractitionerMainText">
		<input type="submit" name="UploadNews" id="UploadNews" value="Update schedule" />
		  <input type="submit" name="DeleteNewsArticle" id="DeleteNewsArticle" value="Delete schedule" onClick="javascript:document.getElementById('status').value='delete'" />
	  </p>
	  </form>
	</div>
</div>
<p>&nbsp;</p>
<?php include $path."includes/p_footer.php"; ?>
</body>

</html>
