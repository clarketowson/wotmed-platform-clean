

<?php

//ini_set('display_errors',1);
//ini_set('display_startup_errors',1);
//error_reporting(-1);


	include "includes/connect.php";
	include "includes/functions.php";

session_start();

	$header=array( 1=>"Ethnic","Religion",7=>"Birthday","Gender","Email","Bank Name", "Branch", "BSB Number","Account Name", "Account Number","Internet Payment Method", 18=> "Home Phone Number","Mobile Phone Number","Work Phone Number","Preferred Phone Number","Skype Username","healthinsuranceprovider_number","Address","Suburb","Postcode","State","Country");
	$rowSessionHeader=array("PARTICIPANT_NUMBER","ETHNIC","RELIGION","USERNAME","PASSWORD","FIRSTNAME","SURNAME","DOB","GENDER","EMAILADDRESS","BANKNAME","BANKBRANCH","BSBNUMBER","ACCOUNTNAME","ACCOUNTNUMBER","INTERNETPAYMENTMETHOD","PROFILEPHOTO","GOOGLEMAPADDRESS","HOMEPHONENUMBER","MOBILEPHONENUMBER","WORKPHONENUMBER","PREFERREDPHONENUMBER","SKYPEUSERNAME","HEALTHINSURANCEPROVIDER_NUMBER","ADDRESS","SUBURB","POSTCODE","STATE","CITY","COUNTRY_NUMBER");
	if(isset($_POST['status']))
	{
		$ethnic=mysqli_real_escape_string($conn,$_POST['ETHNIC_NUMBER']);
		$religion=mysqli_real_escape_string($conn,$_POST['RELIGION_NUMBER']);
		$BankName=mysqli_real_escape_string($conn,$_POST[$rowSessionHeader[10]]);
		$Branch=mysqli_real_escape_string($conn,$_POST[$rowSessionHeader[11]]);
		$BSBNumb=mysqli_real_escape_string($conn,$_POST[$rowSessionHeader[12]]);
		$AccountName=mysqli_real_escape_string($conn,$_POST[$rowSessionHeader[13]]);
		$AccountNumber=mysqli_real_escape_string($conn,$_POST[$rowSessionHeader[14]]);
		$InternetPaymentMethod=mysqli_real_escape_string($conn,$_POST[$rowSessionHeader[15]]);
		$HomePhoneNumber=mysqli_real_escape_string($conn,$_POST[$rowSessionHeader[18]]);
		$MobilePhoneNumber=mysqli_real_escape_string($conn,$_POST[$rowSessionHeader[19]]);
		$WorkPhoneNumber=mysqli_real_escape_string($conn,$_POST[$rowSessionHeader[20]]);
		$PreferedPhoneNumber=mysqli_real_escape_string($conn,$_POST[$rowSessionHeader[21]]);
		$SkypeUsername=mysqli_real_escape_string($conn,$_POST[$rowSessionHeader[22]]);
		$HealthInsurance=mysqli_real_escape_string($conn,$_POST[$rowSessionHeader[23]]);
		$Address=mysqli_real_escape_string($conn,$_POST[$rowSessionHeader[24]]);
		$Suburb=mysqli_real_escape_string($conn,$_POST[$rowSessionHeader[25]]);
		$Postcode=mysqli_real_escape_string($conn,$_POST[$rowSessionHeader[26]]);
		$State=mysqli_real_escape_string($conn,$_POST[$rowSessionHeader[27]]);
		$City=mysqli_real_escape_string($conn,$_POST[$rowSessionHeader[28]]);
		$Country=mysqli_real_escape_string($conn,$_POST[$rowSessionHeader[29]]);
		
		$query="UPDATE PARTICIPANT SET BANKNAME='" . $BankName . "', BANKBRANCH='" . $Branch . "', BSBNUMBER='" . $BSBNumb . "', ACCOUNTNAME='" . $AccountName . "', ACCOUNTNUMBER='" . $AccountNumber . "', INTERNETPAYMENTMETHOD='" . $InternetPaymentMethod . "', HOMEPHONENUMBER='" . $HomePhoneNumber . "', MOBILEPHONENUMBER='" . $MobilePhoneNumber . "', WORKPHONENUMBER='" . $WorkPhoneNumber . "', PREFERREDPHONENUMBER='" . $PreferedPhoneNumber . "', ETHNIC_NUMBER='" . $ethnic . "', RELIGION_NUMBER='" . $religion . "', SKYPEUSERNAME='" . $SkypeUsername . "', HEALTHINSURANCEPROVIDER_NUMBER='" . $HealthInsurance . "', ADDRESS='" . $Address . "', SUBURB='" . $Suburb . "', POSTCODE='" . $Postcode . "', STATE='" . $State . "', CITY='" . $City . "', COUNTRY_NUMBER='" . $Country . "' WHERE PARTICIPANT_NUMBER=" . $_SESSION['id'];
		//echo $query;
		mysqli_query($conn,$query);
		
		for($i=0;$i<count($_POST['LANGUAGESPOKEN_NUMBER']);$i++){
			$LANGUAGESPOKEN_NUMBER=$_POST['LANGUAGESPOKEN_NUMBER'][$i];
			$LANGUAGE_NUMBER=$_POST['LANGUAGE'][$i];
			$index_1=$i+1;
			$temp_name='LANGUAGE' . $index_1;
			$LANGUAGE=$_POST[$temp_name];
			if($LANGUAGE=="")
				$query="DELETE FROM LANGUAGESPOKEN WHERE LANGUAGESPOKEN_NUMBER='$LANGUAGESPOKEN_NUMBER'";
			else
				$query="UPDATE LANGUAGESPOKEN SET LANGUAGE_NUMBER='$LANGUAGE_NUMBER' WHERE LANGUAGESPOKEN_NUMBER='$LANGUAGESPOKEN_NUMBER'";
			mysqli_query($conn,$query);
		}
		$LANGUAGE_NUMBER=$_POST['LANGUAGE'][$i];
		$query="INSERT INTO LANGUAGESPOKEN(PARTICIPANT_NUMBER,LANGUAGE_NUMBER) VALUES('" . $_SESSION['id'] . "','$LANGUAGE_NUMBER')";
		if($LANGUAGE_NUMBER!=0)
			mysqli_query($conn,$query);
		
		if(isset($_SESSION['practitioner_id'])){
			?>
			<script language="javascript"> 
				<?php echo "window.location = 'practitioner_profile.php'";?>
			</script> <?php
			?><?php
		}
		else
			if(isset($_SESSION['id'])){



				?>
				<script language="javascript"> 
					<?php echo "window.location = 'profile.php'";?>
				</script> <?php
				?><?php
			}
	}
	if(isset($_POST['ADDRESS'])){
		$query="UPDATE PARTICIPANT SET ADDRESS = '{$_POST['ADDRESS']}', SUBURB = '{$_POST['SUBURB']}', POSTCODE = '{$_POST['POSTCODE']}', CITY = '{$_POST['CITY']}', STATE = '{$_POST['STATE']}', COUNTRY_NUMBER = '{$_POST['COUNTRY_NUMBER']}', STEP=1 WHERE PARTICIPANT_NUMBER = {$_SESSION['id']}";
		//msgBox($query);
		mysqli_query($conn,$query);
	}
	if(!isset($_SESSION['id'])){
		header("location:login.php");
	}
	if(isset($_SESSION['id'])){
		$rowSession=getParticipantDetail($conn,$_SESSION['id']);
	}
	
	$name = $rowSession['FIRSTNAME']." ".$rowSession['SURNAME'];
	
	if(isset($_GET['id'])){
		$temp=explode("'",$_GET['id']);
		$id=$temp[0];
		$row=getParticipantDetail($conn,$id);
		$name = $row['FIRSTNAME']." ".$row['SURNAME'];
	}
	//$rowSession=getParticipantDetail($conn,$_SESSION['id']);


$whoAmI = $rowSession['PARTICIPANT_NUMBER'];

if ($whoAmI == 2266)
{
    //  echo "User is search User";
    // user is the search user - log this user out right away and return then back to the search dialog box
    //header('Location: http://platform.wotmed.com/WotmedOpenSearchGuest.php');
    //  echo "window.location = 'WotmedOpenSearchGuest.php'";


    echo " <script language='javascript'>";
    echo "window.location = 'WotmedOpenSearchGuest.php'";
    echo "</script>";

}



?>
<!DOCTYPE html PUBLIC
"-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
	<head>
		<title><?php echo $name; ?>'s Wotmed Profile</title>
		<meta http-equiv="Content-Type" content="'text'/html; charset=utf-8" />
        
        <link rel="stylesheet" type="text/css" href="advanceSearch3.css">
        
         <link rel="stylesheet" type="text/css" href="ThankRecommendTrustButtonStyle.css">
        
		<link rel="stylesheet" href="classes/ads files/default/default.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="classes/ads files/nivo-slider.css" type="text/css" media="screen" />
		<link href="style/p_style.css" rel="stylesheet"></link>
		<link href="style/jquery.autocomplete.css" rel="stylesheet"></link>
		<link href="style/popbox.css" rel="stylesheet"></link>
		<script type="text/javascript" src="classes/jquery.js"></script>
		<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>
		<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>
		<script type="text/javascript" src="classes/jquery.autocomplete.js"></script>
		<script type="text/javascript" src="classes/dateSelectBoxes.js"></script>
		<script type="text/javascript" src="classes/jquery.validation.js"></script>
		<script type="text/javascript" src="classes/jquery.qtip.js"></script>
		<script type="text/javascript" src="classes/popbox.min.js"></script>
		<script type="text/javascript" src="classes/autocomplete.js"></script>
		<script type="text/javascript" src="classes/userInfo.js"></script>
		<script type="text/javascript" src="classes/hover.js"></script>
		<script language="javascript"> 
			$().ready(function () {
				$().dateSelectBoxes($('#birthMonth2'),$('#birthDay2'),$('#birthYear2'),true);
			});
			function back()
			{
				window.location = "profile.php"
			}
			function closeDiv(checkupId)
			{
				$(document).click();
				document.getElementById("updateHealthProfileDiv"+checkupId).innerHTML="<img src='images/loading.gif' width='50'/>";
			}
			$(document).ready(function(){
			});
			function open_win()
			{
				window.open("recommend.php")
			}
		function updateSearchList(name)
		{
			if(name.length==0){
				document.getElementById("listOfPatients").innerHTML="";
				return;
			}
			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp=new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp.onreadystatechange=function()
			{
				if (xmlhttp.readyState==4 && xmlhttp.status==200)
				{
					document.getElementById("lYou are locaistOfPatients").innerHTML=xmlhttp.responseText;
				}
			}
			xmlhttp.open("GET","ajax/updateSearchList.php?name="+name,true);
			xmlhttp.send();
		}
		function toggle(selected_div) {
			var ele = document.getElementById(selected_div);
			if(ele.style.display == "block") {
					ele.style.display = "none";
			}
			else {
				document.getElementById('noticeDiv').style.display = "block";
				ele.style.display = "block";
			}
		} 
		function updateHealthProfileForm(checkupId){
			var requestBody="checkupId="+checkupId;
			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp=new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp.onreadystatechange=function()
			{
				if (xmlhttp.readyState==4 && xmlhttp.status==200)
				{
					document.getElementById("updateHealthProfileDiv"+checkupId).innerHTML=xmlhttp.responseText;
					$(".result").validate({
						messages: {
							Month: "*",
							Day: "*",
							Year: "*",
							bmi: "Minimum value 0, maximum value 50",
							sist: "Minimum value 1, maximum value 200",
							dias: "Minimum value 1, maximum value 200"
						},
						rules: {
							Month:{
								min: 1
							},
							Day:{
								min: 1
							},
							Year:{
								min: 1
							},
							bmi:{
								min:0,
								max:50
							},
							sist:{
								min:1,
								max:200
							},
							dias:{
								min:1,
								max:200
							}
					  }
					});
				}
			}
			xmlhttp.open("POST","ajax/add_health_profile.php",true);
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded")
			xmlhttp.send(requestBody);
		}
			$(document).ready(function(){
				$('.popbox').popbox();
				$('input.thanksForm').click(function(){
				showThanksForm();
				// stop the modal link from doing its default action
				return false;
			});
			});
        function grantTrust(pracId){
            //alert(pracId);
            $.ajax({
                type: "POST",
                url: "ajax/grantTrust.php",
                data: { pracId : pracId }
            }).done(function ( msg ) {
                window.location = 'profile.php';
            });
        }
		</script>
		<?php
		if($rowSession['PROFILEPHOTO']==""){
			$ppFileNameSession="blankSilhouetteMale.png";
		}else{
			$ppFileNameSession=$rowSession['PROFILEPHOTO'];
		}
		if(isset($_GET['id'])){
			if($row['PROFILEPHOTO']==""){
				$ppFileName="blankSilhouetteMale.png";
			}else{
				$ppFileName=$row['PROFILEPHOTO'];
			}
		}else{
			$ppFileName=$ppFileNameSession;
		}
		?>
		<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-49954656-1', 'wotmed.com');
  ga('send', 'pageview');

</script>
	</head>


    <SCRIPT TYPE="text/javascript">
        <!--
        function popup(mylink, windowname)
        {
            if (! window.focus)return true;
            var href;
            if (typeof(mylink) == 'signupWizardPatient/index.htm')
                href=mylink;
            else
                href=mylink.href;
            window.open(href, windowname, 'width=1200,height=550,scrollbars=yes');
            return false;
        }

        function load()
        {
            window.location.href = "signupWizardPatient/index.htm";

        }

        //-->
    </SCRIPT>



    <body>


    <?php


    $coatOfArms = $rowSession['COUNTRYCOATOFARMS'];

    if(empty($coatOfArms))
    {

        // if the patient has not set their countryof origin then the coat of arms will also be blank -
        // if this is the case then display the wizard
        // otherwise dont

            session_start();
            if (isset($_SESSION['views']))
                $_SESSION['views'] = $_SESSION['views'] + 1;
            else {
                echo "<body onload='load()'>";
                $_SESSION['views'] = 1;
            }


    }

    ?>



    <?php include "includes/p_header.php"; ?>
			<div id="adsSlideShow" style="width:100%; float:left; margin-top:0px; margin-bottom:20px; display:none; height:100px;">
			<div id="wrapper">
				<div class="slider-wrapper theme-default">
					<div id="slider" class="nivoSlider">
						<a href="advanceSearch.php"><img src="classes/ads files/images/Untitled-Advert Health profile 3 Thai boat.jpg" data-thumb="classes/ads files/images/Untitled-Advert Health profile 3 Thai boat.jpg" alt="" title="#htmlcaption"/></a>
						<a href="advanceSearch.php"><img src="classes/ads files/images/advert health profile 3 Indonesia.jpg" data-thumb="classes/ads files/images/advert health profile 3 Indonesia.jpg" alt="" title="#htmlcaption"/></a>
						<a href="advanceSearch.php"><img src="classes/ads files/images/Untitled-Advert Health profile 2 sea and mountain.jpg" data-thumb="classes/ads files/images/Untitled-Advert Health profile 2 sea and mountain.jpg" alt="" data-transition="slideInLeft" title="#htmlcaption"/></a>
					</div>
					<div id="htmlcaption" class="nivo-html-caption">
						<strong>Try our <a href="advanceSearch.php">medical tourism</a>. </strong>
					</div>
				</div>

			</div>
			<script type="text/javascript" src="classes/ads files/jquery.nivo.slider.js"></script>
			<script type="text/javascript">
			$(window).load(function() {
				$('#slider').nivoSlider();
			});
			</script>
			</div>
			<div id="leftCol" class="lfloat">
				<div id="leftProfPic" class="lfloat gborder">
					<div class="profPicDiv"><img src="<?php echo "photos/originals/" . $ppFileName; ?>" height="175px"/></div>
					<?php
						if(!isset($_GET['id'])){
							//document.getElementById('uploadPhotoDiv').style.display = 'block';
					?>
					<a href="#" onClick="toggle('uploadPhotoDiv');">Change Profile Photo</a>
					<?php
						}
					?>
				</div>
				<div class="gborder lfloat marBotBig" style="width:100%;margin-top:7px">

					<div id="flagDiv"><img id='flag' src='images/flag
					<?php
                        $flag = $rowSession['COUNTRYFLAG'];

                        if(empty($flag))
                        {
                            // display the divercity flag
                            $flag = '/OneWorldFlag.gif';
                            echo $flag;

                        }

                        else
                        {
                            echo $rowSession['COUNTRYFLAG'];
                        }
                        ?>' height='20px' align='left'/>


					<?php
						if(isset($_GET['id'])){

							$subject1="You are located in {$rowSession['CITY']} {$rowSession['COUNTRYNAME']}";
						}
						else{
						//	$subject1="You are located in {$rowSession['CITY']} {$rowSession['COUNTRYNAME']}";

                            // advise the patient to set country of origin

                            $countryName = $rowSession['COUNTRYNAME'];

                            if(empty($countryName))
                            {
                    $subject1="Please set your country name by clicking the Update your Personal Information at the right hand side of your profile";
                            }
                            else {
                                $subject1 = "You are located in {$rowSession['CITY']} {$rowSession['COUNTRYNAME']}";
                            }
                            }
					?>








					<div id="flagDivPopUp" class="Indices"><?php echo $subject1; ?></div>
					</div>
					<img src='images/coatofarms<?php



                    // blank the coat of arms if the country has not been set

                    $coatOfArms = $rowSession['COUNTRYCOATOFARMS'];

                    if(empty($coatOfArms))
                    {
                        // blank the coat of arms
                        $coatOfArms = '/blankcoatofarms.gif';
                        echo $coatOfArms;


                    }
                    else
                    {
                        echo $rowSession['COUNTRYCOATOFARMS'];


                    }

                    ?>

                    ' height='20px' align='left'/><a href="#" onClick="showVisaReq('http://en.wikipedia.org/wiki/Visa_requirements_for_<?php echo $rowSession['WIKIVR']; ?>','http://en.wikipedia.org/wiki/Visa_requirements_for_<?php echo $rowSession['WIKIVR']; ?>')"></a>
				</div>
					<!--<?php
					$familyList=getFamily($conn,$_SESSION['id']);
					if(isset($_GET['id']))
					{
						if($id==$_SESSION['id'])
						{
							?>
							<script language="javascript"> 
								window.location = "profile.php";
							</script>
							<?php
						}
						$familyList=getFamily($conn,$id);
					}
					if($familyList!=Null)
					{
						echo "
					<div class='lfloat gborder marBotBig' style='width:100%;'>
					<table>
					";
						while ($listOfFamily=mysqli_fetch_array($familyList)){
							$familyDetail=getParticipantDetail($conn,$listOfFamily[1]);
							$relation=getRelation($listOfFamily[2],$familyDetail['GENDER']);
							if($familyDetail['PROFILEPHOTO']==""){
								$pp="blankSilhouetteMale.png";
							}else{
								$pp=$familyDetail['PROFILEPHOTO'];
							}
							
							$relationshipLink = "<tr><td><div class='tinyDiv'><img src='photos/thumbs/" . $pp . "' height='25px'/></div></td>";
							$relationshipLink.="<td><a href='profile.php?id=" . $listOfFamily[1] . "'>" . $familyDetail['FIRSTNAME'] . " " . $familyDetail['SURNAME'] . "</a></td>";
							if($listOfFamily[1]==$_SESSION['id'])
							{
								$relationshipLink="<tr><td><a href='profile.php'>" . $familyDetail['FIRSTNAME'] . " " . $familyDetail['SURNAME'] . "</a></td>";
							}
							echo $relationshipLink;
							echo "<td>" . $relation . "</td></tr>";
						}
						echo "
					</table>
				</div>
						";
					}
					?>-->
					<?php if(!isset($_GET['id'])){ ?>
				<div class="lfloat gborder marBotBig" style="width:100%;">
					<table style='width:100%'>
					<tr>
						<td>
							<div id="addAlbum" class="lfloat">
                            
							<!--	<a href='gallery/addAlbum.php'> -->
                                
									<img src='images/icon_video_album_default.png' style='width:25px;'>
									<div id="addAlbumPopUp" class="Indices">
										Add photo or video album (Coming Soon)
									</div>
								</a>
							</div>
						</td>
						<td>
							<div id="gallery" class="lfloat">
                            
							<!--	<a href='gallery/gallery.php'> -->
                                
									<img src='images/PhotoAlbum.png' style='width:25px;'>
									<div id="galleryPopUp" class="Indices">
										Manage your photographs (Coming Soon)
									</div>
								</a>
							</div>	
						</td>
						<td>
							<div id="videoGallery" class="lfloat">
							<!--	<a href='gallery/videoGallery.php'> -->
                            
									<img src='images/VideoIcon.png' style='width:25px;'>
									<div id="videoGalleryPopUp" class="Indices">
										Manage your videos (Coming Soon)
									</div>
								</a>
							</div>	
						</td>
						<td>
							<div id="aboutMe" class="lfloat">
							
                            <!--	<a href='aboutMe/aboutMeParticipantVideo.php'> -->
                            
									<img src='images/video.png' style='width:25px;'>
									<div id="aboutMePopUp" class="Indices">
										Manage your About Video (Coming Soon)
									</div>
								</a>
							</div>	
						</td>
						<td>
							<div id="conditionReport" class="lfloat">
                            
						<!--		<a href='aboutMe/conditionReport.php'> -->
                                
									<img src='images/conditionCard.png' style='width:25px;'>
									<div id="conditionReportPopUp" class="Indices">
										 Manage your surgical condition report (Coming Soon)
									</div>
								</a>
							</div>	
						</td>
					</tr>
					</table>
					<!--
					<a href='gallery/addAlbum.php'><img src='images/icon_video_album_default.png' style='width:40px;'> Add album</a><br>
					<a href='gallery/gallery.php'><img src='images/PhotoAlbum.png' style='width:40px;'> Photos gallery</a><br><br>
					<a href='gallery/videoGallery.php'><img src='images/VideoIcon.png' style='width:40px;'> Videos gallery</a><br>
					<div>
						<div class="lfoat" style='padding-top:7px;'><a href="aboutMe/aboutMeParticipantVideo.php"><img src="images/video.png" style='height:40px' alt="Participant About Me Video"> About Me Video</a>
						</div>
						<div class="lfoat"><a href="aboutMe/conditionReport.php"><img src="images/conditionCard.png" style='height:40px' alt="Participant Condition Report"> Condition Report</a>
						</div>						
					</div>
					-->
						<?php
						$id=$_SESSION['id'];
						if($id==106 || $id==110 || $id==111 || $id==117)
							echo "<a href='admin'>Admin Interface</a>";
						?>
				</div>
						<?php }	?>
					<?php /*
						else{
							$tempId=$_GET['id'];
							echo "<table>
							<tr>
							<td>
								<div id='gallery' class='lfloat'>
									<a href='gallery/gallery.php?id=" . $tempId . "'>
										<img src='images/PhotoAlbum.png' style='width:25px;'>
										<div id='galleryPopUp' class='Indices'>
											See Photos
										</div>
									</a>
								</div>	
							</td>
							<td>
								<div id='videoGallery' class='lfloat'>
									<a href='gallery/videoGallery.php?id=" . $tempId . "'>
										<img src='images/VideoIcon.png' style='width:25px;'>
										<div id='videoGalleryPopUp' class='Indices'>
											See Videos
										</div>
									</a>
								</div>	
							</td>
							</tr>
							</table>";
					/*
							echo "<a href='gallery/gallery.php?id=" . $tempId . "'>Photos gallery</a><br><br>";
							echo "<a href='gallery/videoGallery.php?id=" . $tempId . "'>Videos gallery</a><br>";
							*/
					?>
			</div>
			<div id="contentMain" class="lfloat">
				<?php
				if(!isset($_GET['id'])){
					if($rowSession['COUNTRY_NUMBER']==""){
						?>
						<div id="noticeDiv">
							<form action="#" method='POST'>
								<div class="fMid fcg">
									<p> Please update your address details</p>
									<table>
								<?php
									$header=array("Address","Suburb","Postcode","State","City","Country");
									$rowPatientSessionHeader=array("ADDRESS","SUBURB","POSTCODE","STATE","CITY","COUNTRY_NUMBER");
									for($i=0;$i<count($header)-1;$i++){
										echo "<tr><th>" . $header[$i] . "</th>";
										echo "<td><input type='text' name='". $rowPatientSessionHeader[$i] . "' id='". $rowPatientSessionHeader[$i] . "' value=''></td></tr>";
									}
									echo "<tr><th>" . $header[$i] . "</th>";
								?>
									<td>
									<select name='<?php echo $rowPatientSessionHeader[$i]; ?>' >
										<option value='0'>Select Country</option>
										<?php
										$countryList=getCountryList($conn);
										while($country=mysqli_fetch_array($countryList)){
											if($rowPatientSession[$rowPatientSessionHeader[$i]]==$country[0])
												$selected=" selected=true ";
											else
												$selected="";
											echo "<option value='$country[0]'$selected>$country[1]</option>";
										}
										?>
									</select>
									</td></tr>
									<tr><td>
										<input type="submit" value="Update"/>
									</td></tr>
									</table>
								</div>
							</form>
							<div id="uploadPhotoDiv" class="fMid fcg">
								<form method='POST' enctype="multipart/form-data" action= 'uploadProfilePicture.php'>
								Please upload a profile picture:<br/>
								<input type="file" name="pp"/>
								<input type="submit" value="Upload"/>
								</form>
							</div>
						</div>
						<script> document.getElementById("noticeDiv").style.display = "block";</script>
						<?php
					}else if($rowSession['PROFILEPHOTO']==""){
						?>
						<div id="noticeDiv">
							<div id="uploadPhotoDiv" class="fMid fcg">
								<form method='POST' enctype="multipart/form-data" action= 'uploadProfilePicture.php'>
								Please update your profile picture:<br/>
								<input type="file" name="pp"/>
								<input type="submit" value="Upload"/>
								</form>
							</div>
						</div>
						<script> document.getElementById("noticeDiv").style.display = "block";document.getElementById("uploadPhotoDiv").style.display = "block"; </script>
						<?php
					}else{
						?>
						<div id="noticeDiv">
							<div id="uploadPhotoDiv" class="fMid fcg">
								<form method='POST' enctype="multipart/form-data" action= 'uploadProfilePicture.php'>
								Please update your profile picture:<br/>
								<input type="file" name="pp"/>
								<input type="submit" value="Upload"/>
								</form>
							</div>
						</div>
						<?php
					}
				}
				?>
				<div class="marBotBig marTopBig">
					<?php
						if(isset($_GET['id'])){
					?>
					<div class="bold marBotBig subtitle fMid fColor" style='padding-top:7px'>
						<span style="font-size:20px"><?php echo $row['FIRSTNAME']." ".$row['SURNAME']; ?><br></span>
						
					</div>
						<?php }
						else 
						{?>
					
					<div class="bold marBotBig subtitle fMid fColor" style='padding-top:7px;margin-bottom:18px;'>
						<span style="font-size:20px"><?php echo $rowSession['FIRSTNAME']." ".$rowSession['SURNAME']; ?><br></span>
					<div class="rfloat" style="font-size:11px;font-weight:normal;padding-top:-25px;color:rgb(170, 170, 170)">
						<span>Patient No: <?php echo $rowSession['PARTICIPANT_NUMBER']; ?></span><br>
					</div>



						<?php




						
						//  accumulators for Trust, Thanked and Recommended 
						//	for PRACTITIONERS
						
							echo "<div style='font-size:12px;font-weight:normal;padding-left:20px;padding-top:5px;'>";
							echo "You personally <div id='trustText' style='display:inline-block;padding-top:3px;'><a href='#' style='color:#195297;font-style:italic;'>Trust</a><div id='trustTextPopUp' class='Indices'>This is the total number of Wotmed practitioners that you trust.</div></div> " . participantGetTrust($conn,$_SESSION['id']) . " Wotmed Practitioner(s)<BR>";
							
							echo "You have personally <div id='thankText' style='display:inline-block;padding-top:3px;'><a href='#' style='color:#195297;font-style:italic;'>Thanked</a><div id='thankTextPopUp' class='Indices'>This is the total number of Wotmed Practitioners that you have thanked.</div></div> ". getNumberOfThanksParticipant($conn,$_SESSION['id']) ." Wotmed Practitioner(s)<BR>";
							
							echo "You have <div id='recomText' style='display:inline-block;padding-top:3px;'><a href='#' style='color:#195297;font-style:italic;'>Recommended</a><div id='recomTextPopUp' class='Indices'>This is the total number of Wotmed Practitioners that you have recommended.</div></div> ". getNumberOfRecommended($conn,$_SESSION['id']) ." Wotmed Practitioner(s)<br><br>";
							
							
						//  accumulators for Trust, Thanked and Recommended 
						//	for SURGERY FACILITATORS
							
							echo "You personally <div id='trustText' style='display:inline-block;padding-top:3px;'><a href='#' style='color:#195297;font-style:italic;'>Trust</a><div id='trustTextPopUp' class='Indices'>This is the total number of Wotmed Surgery Facilitators that you trust.</div></div> " . participantGetTrustSurgeryFacilitator($conn,$_SESSION['id']) . " Wotmed Surgery Facilitators(s)<BR>";
							
							echo "You have personally <div id='thankText' style='display:inline-block;padding-top:3px;'><a href='#' style='color:#195297;font-style:italic;'>Thanked</a><div id='thankTextPopUp' class='Indices'>This is the total number of Wotmed Surgery Facilitators that you have thanked.</div></div> ". getNumberOfThanksParticipant($conn,$_SESSION['id']) ." Wotmed Surgery Facilitator(s)<BR>";
							
							echo "You have <div id='recomText' style='display:inline-block;padding-top:3px;'><a href='#' style='color:#195297;font-style:italic;'>Recommended</a><div id='recomTextPopUp' class='Indices'>This is the total number of Wotmed Surgery Facilitators that you have recommended.</div></div> ". getNumberOfRecommendedSurgeryFacilitator($conn,$_SESSION['id']) ." Wotmed Surgery Facilitator(s)<br><br>";
							
							
							
							echo "
							You are <a href='#' style='color:#195297;font-style:italic;'>Connected</a> to " . getNumberOfFam($conn,$_SESSION['id']) . " Wotmed Patient(s)<BR>";
							
							echo "
							You are <div id='connectedToText' style='display:inline-block;padding-top:3px;'><a href='#' style='color:#195297;font-style:italic;'>Connected</a><div id='connectedToTextPopUp' class='Indices'>This is the total number of Wotmed Practitioners that you have established relationships with.</div></div> to " . getNumberOfConnection($conn,$_SESSION['id']) . " Wotmed Practitioner(s)<BR>";
							
							echo "
							You are <div id='connectedToText' style='display:inline-block;padding-top:3px;'><a href='#' style='color:#195297;font-style:italic;'>Connected</a><div id='connectedToTextPopUp' class='Indices'>This is the total number of Wotmed Surgery Facilitators that you have established relationships with.</div></div> to " . getNumberOfConnectionSurgeryFacilitator($conn,$_SESSION['id']) . " Wotmed Surgery Facilitator(s)<BR>";
							
							
							echo "</div>";
							//echo "This practitioner recommended by : " . $numbOfRecommend[0] . " / " . $numbOfPatients[0];
						?>
					</div>
					<div class='gborder marBotBig fMid' style="width:99%" title='Search for Wotmed Practitioners and Surgery Facilitators'>
						
						<table style="width:100%">
							
                              <?php
							  
				//	echo "<p><b>Search and connect with Practitioners and Surgery Facilitators globally who have an account on Wotmed</b>";

                            echo "<img src='images/WotmedSearchIcon.png' style='width:30px;'>";

                            echo "<b style='color:#195297;font-size:16px;'>Search and connect with Practitioners and Surgery Facilitators globally</b>";

					echo "<BR><BR>";
 					echo "<form name='WotmedOpenSearch' method='GET' action='WotmedOpenSearchAll.php'>";
  					echo "<input type='text' name='searchTerm' id='searchTerm'/>";
					echo "<input type='radio' name='includeFacilitators' value='Facilitator' unchecked>Include Surgery Facilitators in your search<BR><BR>";
 					echo "<input type='submit' name='searchsubmit' value='Wotmed Search' class='form-submit' id='searchsubmit' />";
					echo "</form>";
    
  								 ?>
      
						</table>
					</div>
						<?php } ?>
				</div>
					<?php
						if(isset($_GET['id'])){/*
					echo "<div>
							<table>";
							$relationship="<a href='addRelationship.php?isId=" . $_SESSION['id'] . "&ofId=" . $_GET['id'] . "'>Add Relationship</a>";
							if(getRelationship($conn,$_SESSION['id'],$_GET['id'])!=NULL)
							{
								$relationship=getRelationship($conn,$_SESSION['id'],$_GET['id']);
							}
							echo "<div>";
								echo $relationship;
							echo "</div>";
							echo "</table>
					</div>";
						*/}
					?>
					<?php
						if(!isset($_GET['id'])){
					?>
				<div>
				
					<div class="marBotBig marTopBig gborder" style='background:rgb(240, 240, 240)'>
						<div style='margin-bottom:5px;padding-bottom:5px'>
							<img src='images/WotmedPractitionerScheduleIcon.png' style='width:30px;'> 
							<b style="color:#195297;font-size:16px;">Your Appointments with Wotmed Practitioner(s)</b>
						</div>
						<table border=1>
						<tr align=center>
							<th width=150>Name of Doctors</th>
							<th width=80>Reason</th>
							<th width=80>Date</th>
							<th width=80>Time</th>
							<th width=150 colspan="2">Status</th>
						</tr>
						<?php
							$query="SELECT * FROM BOOKING WHERE PARTICIPANT_NUMBER ='" . $_SESSION['id'] . "'";
							$books=mysqli_query($conn,$query);
							if(mysqli_num_rows($books)!=0){
								while($listOfBooking=mysqli_fetch_array($books)){
									$temp="SELECT PRACTITIONER_BUSINESSNAME FROM PRACTITIONER WHERE PRACTITIONER_NUMBER ='" . $listOfBooking['PRACTITIONER_NUMBER'] . "'";
									$name=mysqli_query($conn,$temp);
									$fullName=mysqli_fetch_array($name);
									echo "<tr><td>" . $fullName[0] . "</td>";
									echo "<td>" . $listOfBooking[5] . "</td>";
									echo "<td>" . $listOfBooking[3] . "</td>";
									echo "<td>" . $listOfBooking[4] . "</td>";
									echo "<td colspan='2' align=center>" . $listOfBooking[8] . "</td></tr>";
								}
							}
							else
							{
								echo "<tr><td colspan='6' align=center>No appointment scheduled</td></tr>";
							}
						?>
						</table>
					</div>
					<div class="marBotBig marTopBig gborder">
						<div style='margin-bottom:5px;padding-bottom:5px'>
							<img src='images/List-icon.png' style='width:30px'/>
							<b style="color:#195297;font-size:16px">Wotmed Practitioner(s) you are connected to</b>
						</div>
						<table>
						<?php
							$doctorList=getDoctor($conn,$_SESSION['id']);
							if($doctorList!=Null)
							{
								while ($listOfDoctor=mysqli_fetch_array($doctorList)){
									$DoctorDetail=getPractitionerDetailByPractitionerId($conn,$listOfDoctor['PRACTITIONER_NUMBER']);
									
									if($DoctorDetail['ISFACILITATOR']==0){
										if($DoctorDetail['PROFILEPHOTO']==""){
											$pp="blankSilhouetteMale.png";
										}else{
											$pp=$DoctorDetail['PROFILEPHOTO'];
										}
									
										
										echo "<tr><td rowspan='2'><div class='pracListDic'><img src='photos/thumbs/" . $pp . "' height='50px'></div></td>";
										echo "<td><a href='practitioner_profile.php?id=" . $DoctorDetail['PARTICIPANT_NUMBER'] . "'>Dr " . $DoctorDetail['PRACTITIONER_BUSINESSNAME'] . "</a></td>";
										if(!$listOfDoctor[2]){
											echo "<td rowspan='2'>
												<div class='recommendButton lfloat' style='display:inline-block;padding-top:3px;'><input style='width:100px;white-space:normal;' type='button' class='recommendForm' value='Recommend' id='recommendsubmit' onClick=\"$('.pracId').attr('value','$listOfDoctor[0]');\"/>
												<div id='' class='recommendButtonPopUp Indices'>The Recommend button is a way for you to provide a written, publicly viewable review of a Practitioner or Surgery Facilitator.  Reading other patients recommendations is a good way for you to determine if a Wotmed Practitioner or Surgery Facilitator is trusted by other Patients.  You may consider a Practitioner or Surgery Facilitator who has been recommended highly by others to be more worthy of your trust.  Please click here to make a Recommendation.</div>
												</div>
											";
											//window.location='recommend.php?practitioner_id=$listOfDoctor[0]';
										}else{
											echo "<td rowspan='2' style='width:100px;white-space:normal;'>You have recommended this practitioner";
										}
										if(!$listOfDoctor["TRUSTED"]){
											echo "<td rowspan='2'>
												<div class='trustButton lfloat' style='display:inline-block;padding-top:3px;'><input style='width:100px;white-space:normal;' type='button' class='trustForm' value='Grant Trust' id='granttrustsubmit' onClick=\"grantTrust('$listOfDoctor[0]');\"/>
												<div id='' class='trustButtonPopUp Indices'>The Grant Trust button is a way for you to publically announce that you trust a Wotmed Practitioner or Surgery Facilitator. Trust must be earned over time and in most cases a Wotmed patient will only grant trust to a Wotmed Practitioner or Surgery Facilitator after they have performed or facilitated a successful surgical procedure which has contributed positively to their quality of life.  You may consider a Practitioner or Surgery Facilitator who has earned a high number of patient trust to be more likely to earn your own trust.  Please click this button to Grant Trust.</div>
												</div>
											";
											//window.location='recommend.php?practitioner_id=$listOfDoctor[0]';
										}else{
											echo "<td rowspan='2' style='width:100px;white-space:normal;'>You have trusted this person";
										}
										echo "
											<td rowspan='2'>
												<div class='thanksButton lfloat' style='display:inline-block;padding-top:3px;'><input style='width:100px;white-space:normal;' type='button' value='Thank' class='thanksForm' id='thanksubmit' onclick=\"$('input#pracId').val('{$DoctorDetail['PRACTITIONER_NUMBER']}');\"/>
												<div class='thanksButtonPopUp Indices'>The Thanks button is a way for you to show gratitude to a Practitioner or Surgery Facilitator who has made a positive difference in your life. In most cases you will want to Thank a Practitioner or Surgery Facilitator after they have performed or facilitated a successful surgical procedure for you which has contributed positively to your quality of life.  You may consider a Practitioner or Surgery Facilitator who has earned a high number of Thanks from other Wotmed Patients to be more worthy of your trust.  Please click this button to show your Thanks.</div>
												</div>
											</td>
										";
										$passport="";
										if($DoctorDetail['COUNTRY_NUMBER']!=$rowSession['COUNTRY_NUMBER'])
											$passport="<img src='images/passport_icon.png' height='20px' align='left'/>";
											
										// check to see if the practitioners country flag has been set
										// if it has not been then display the one world flag instead
										
										if(is_null($DoctorDetail['COUNTRYFLAG']))
										{
											
											echo "</td></tr><tr><td><img src='images/flag/OneWorldFlag.gif' height='20px' align='left'/>$passport</td></tr>";
										}
										else
										{
										echo "</td></tr><tr><td><img src='images/flag".$DoctorDetail['COUNTRYFLAG']."' height='20px' align='left'/>$passport</td></tr>";
										}
										
										
									}
								}
							}
							else{
								echo "<tr><td colspan='2'>You are not yet connected to any Wotmed Practitioners</td></tr>";
							}
						?>
						</table>
					</div>

					<div class="marBotBig marTopBig gborder">
						<div style='margin-bottom:5px;padding-bottom:5px'>
							<img src='images/List-icon.png' style='width:30px'/>
							<b style="color:#195297;font-size:16px">Surgery Facilitator(s) you are connected to</b>
						</div>
						<table>
						<?php
							$doctorList=getDoctor($conn,$_SESSION['id']);
							if($doctorList!=Null)
							{
								while ($listOfDoctor=mysqli_fetch_array($doctorList)){
									$DoctorDetail=getPractitionerDetailByPractitionerId($conn,$listOfDoctor['PRACTITIONER_NUMBER']);
									if($DoctorDetail['ISFACILITATOR']==1){
										if($DoctorDetail['PRACTITIONER_BUSINESSLOGO']==""){
											$pp="blankSilhouetteMale.png";
										}else{
											$pp=$DoctorDetail['PRACTITIONER_BUSINESSLOGO'];
										}
										echo "<tr><td rowspan='2'><div class='pracListDic'><img src='photos/thumbs/" . $pp . "' height='50px'></div></td>";
										echo "<td><a href='facilitator/index.php?id=" . $DoctorDetail['PARTICIPANT_NUMBER'] . "'>" . $DoctorDetail['PRACTITIONER_BUSINESSNAME'] . "</a></td>";
										if(!$listOfDoctor[2]){
											echo "<td rowspan='2'>
												<div class='recommendButton lfloat' style='display:inline-block;padding-top:3px;'><input style='width:100px;white-space:normal;' type='button' class='recommendForm' value='Recommend' id='recommendsubmit' onClick=\"$('.pracId').attr('value','$listOfDoctor[0]');\"/>
												<div id='' class='recommendButtonPopUp Indices'>The Recommend button is a way for you to provide a written, publicly viewable review of a Practitioner or Surgery Facilitator.  Reading other patients recommendations is a good way for you to determine if a Wotmed Practitioner or Surgery Facilitator is trusted by other Patients.  You may consider a Practitioner or Surgery Facilitator who has been recommended highly by others to be more worthy of your trust.  Please click here to make a Recommendation.</div>
												</div>
											";
											//window.location='recommend.php?practitioner_id=$listOfDoctor[0]';
										}else{
											echo "<td rowspan='2' style='width:100px;white-space:normal;'>You have recommended this person";
										}
										if(!$listOfDoctor["TRUSTED"]){
											echo "<td rowspan='2'>
												<div class='trustButton lfloat' style='display:inline-block;padding-top:3px;'><input style='width:100px;white-space:normal;' type='button' class='trustForm' value='Grant Trust' id='granttrustsubmit' onClick=\"grantTrust('$listOfDoctor[0]');\"/>
												<div id='' class='trustButtonPopUp Indices'>The Grant Trust button is a way for you to publically announce that you trust a Wotmed Practitioner or Surgery Facilitator. Trust must be earned over time and in most cases a Wotmed patient will only grant trust to a Wotmed Practitioner or Surgery Facilitator after they have performed or facilitated a successful surgical procedure which has contributed positively to their quality of life.  You may consider a Practitioner or Surgery Facilitator who has earned a high number of patient trust to be more likely to earn your own trust.  Please click this button to Grant Trust.</div>
												</div>
											";
											//window.location='recommend.php?practitioner_id=$listOfDoctor[0]';
										}else{
											echo "<td rowspan='2' style='width:100px;white-space:normal;'>You have trusted this person";
										}
										echo "
											<td rowspan='2'>
												<div class='thanksButton lfloat' style='display:inline-block;padding-top:3px;'><input style='width:100px;white-space:normal;' type='button' value='Thank' class='thanksForm' id='thanksubmit' onclick=\"$('input#pracId').val('{$DoctorDetail['PRACTITIONER_NUMBER']}');\"/>
												<div class='thanksButtonPopUp Indices'>The Thanks button is a way for you to show gratitude to a Practitioner or Surgery Facilitator who has made a positive difference in your life. In most cases you will want to Thank a Practitioner or Surgery Facilitator after they have performed or facilitated a successful surgical procedure for you which has contributed positively to your quality of life.  You may consider a Practitioner or Surgery Facilitator who has earned a high number of Thanks from other Wotmed Patients to be more worthy of your trust.  Please click this button to show your gratitude.</div>
												</div>
											</td>
										";
										$passport="";
										if($DoctorDetail['COUNTRY_NUMBER']!=$rowSession['COUNTRY_NUMBER'])
											$passport="<img src='images/passport_icon.png' height='20px' align='left'/>";
											
											// check to see if the surgery facilitators country flag has been set
										// if it has not been then display the one world flag instead
										
										if(is_null($DoctorDetail['COUNTRYFLAG']))
										{
											
											echo "</td></tr><tr><td><img src='images/flag/OneWorldFlag.gif' height='20px' align='left'/>$passport</td></tr>";
										}
										else
										{
										echo "</td></tr><tr><td><img src='images/flag".$DoctorDetail['COUNTRYFLAG']."' height='20px' align='left'/>$passport</td></tr>";
										}
										
										
									}
								}
							}
							else{
								echo "<tr><td colspan='2'>You are not yet connected to any Wotmed Surgery Facilitators</td></tr>";
							}
						?>
						</table>
					</div>

					<div class='gborder lfloat marBotBig' style='width:99%'>
						<?php include 'thanks.php'?>
						<?php include 'thanksSearch.html'?>
					</div>
				<div id="healthProfile" class="marBotBig lfloat" style='width:100%;min-height:115px;'>
					<div class="gborder" style='border-color:#ffffff;'>
						<!--	<img src='images/WotmedHealthProfileIcon2.png' style='width:35px'/> -->
							<b style="color:#ffffff;font-size:16px">Your Patient Health Profile (Coming Soon)</b>
							<div style="float:right;margin-botttom:5px">
						<!--		<a href=""  onclick="window.print();"><img src="images/print.png" style="height:25px;"/></a> -->
							</div>
							<div style='margin-top:25px' >
							<table>
					<?php
							$checkupList=getCheckup($conn,$rowSession['DOB'],$rowSession['GENDER']);
							$healthTest=getHealtProfile($conn,$_SESSION['id']);
							$checked=0;
							$total=0;
							while ($checkupRow=@mysqli_fetch_array($checkupList)){ 
								$status = true; //-- $status = condition of health profile(exist if false)
								$expired = false;
								$total++;
								if($healthTest!=NULL){
									$healthRow=mysqli_data_seek($healthTest,0);
									while($healthRow=mysqli_fetch_array($healthTest))
									{
										if($healthRow['CHECKUP_NUMBER']==$checkupRow['CHECKUP_NUMBER'])
										{
											$status = false;
											$expired=checkCheckupExpiry($healthRow['UPDATETIME'],$checkupRow['FREQUENCY']);
											break;
										}
									}
								}
								if($status or $expired)
								{
									$title="";
									if ($expired)
										$title="title=\"Its time to take another check up\"";
										
							//		echo "<tr><td><img class='tinyPhoto' " . $title . " src='images/cross.png'  width='25' height='25'/></td><td><img class='tinyPhoto' src='images/Question_mark_blue.png' width='25' height='25' title=\"".($checkupRow['CHECKUPREASON']) . "\"/></td>";
								}
								else
								{
									$checked++;
							//		echo "<tr><td><img class='tinyPhoto' src='images/check.jpg' width='25' height='25'/></td><td><img class='tinyPhoto' src='images/Question_mark_blue.png' width='25' height='25' title=\"" . ($checkupRow['CHECKUPREASON']) . "\"/></td>";
								}
							//	echo "<td>" . $checkupRow['CHECKUP_NAME'] . "</td>";
								if($status)
								{
							//		echo "<td>need to be checked</td>";
								}
								else
								{
							//		echo "<td>" . date('Y-m-d', strtotime($healthRow['UPDATETIME'])) . " - " . $healthRow['DESCRIPTION'] . "</td>";
								}
								echo "<td width='100px'>
									<input type='hidden' name='checkupId' value='" . $checkupRow['CHECKUP_NUMBER'] . "' />
									<div class='popbox'> 
									 <div class='collapse'>
										<div class='box'>
										  <div class='arrow'></div>
										  <div class='arrow-border'></div><div id='updateHealthProfileDiv" . $checkupRow['CHECKUP_NUMBER'] . "'><img src='images/loading.gif' width='50'/></div>
										</div>
									  </div></div>
								</td>"; 
								
								if($expired or $status){
						//			echo "<td><input type='button' name='bookButton' value='Book Next' \></td>";
								}
								else{
						//			echo "<td></td>";
								}
						//		echo "</tr>";
							}
							$total!=0?$width=($checked  * 490) / $total:$width=0;
						//	echo "<div style='width:490px; margin:auto;' class='ui-progress-bar'>";
						//		echo "<div style='width:" . $width . "px;' class='ui-progress'>" . $checked . "/" . $total . "</div>";
						//	echo "</div>";
						echo "</table>
					</div>
				</div>
				</div>
				
				</div>";
						}
						else{
							if(isPractitioner($conn,$_SESSION['id'])){
								$wishList=getWishlist($conn,$id);
								
								?>
						<div class="marBotBig marTopBig">
							<strong>Wishlist:</strong>
							<table border=1>
							<tr align=center>
								<th width=150>Name of Wotmed Practitioners</th>
								<th width=80>Procedure Name</th>
							</tr>
                            
                            
                            
                          
                            
                            
                            
							<?php
								if(($wishList)!=null){
									while($wish=mysqli_fetch_array($wishList)){
										echo "<tr><td>" . $wish[1] . "</td>";
										echo "<td>" . $wish[2] . "</td></tr>";
									}
								}
								else
								{
									echo "<tr><td colspan='6' align=center>This Wotmed Patient has not listed any procedures they would like done</td></tr>";
								}
							?>
							</table>
						</div>
						<?php
							}
						}
					?>
				</div>
			<div id="rightCol" class="rfloat">
				<?php
					include "contactUser.php";
					include "includes/profile_details.php";
					include("recommend.php");
				?><!--
				<div>
					<form method="post" action="add_health_profile.php">
					<?php


					//echo "Search <input type='text' name='name' id='name' onkeyup=\"updateSearchList(this.value)\" autocomplete='off'>";
					?>
					<div id="listOfPatients"></div>
					</form>
				</div>-->
			</div>

			<?php include "includes/p_footer.php"; ?>


	<script type="text/javascript" src="classes/stylingImage.js"></script>
	</body>
</html>
