<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "VideoObject",
  "name": "Start With Why",
  "description": "Clarke Towson talks about why Wotmed has been created",
  "thumbnailUrl": "http://platform.wotmed.com/StartWithWhyThumbnail.jpg",
  "uploadDate": "2015-06-06",
  "duration": "PT6M24S",
  "contentUrl": "http://platform.wotmed.com/ImageUpload/uploads/clientspeeches/StartWithWHYMultiCamera.flv",
  "transcript": "Our valued clients,
I would like to express my appreciation to you for taking the time out of your busy schedules to hear me talk today on why we have created Wotmed. I will be talking a lot in this speech about what I believe because the Wotmed platform has been built around my beliefs and the beliefs of the Wotmed team.
So let me begin today by talking about the power of belief and asking that all important question: Why.
Why have we built Wotmed?
Firstly - and most importantly - we believe that all the people of this world deserve to be empowered to access affordable medical services - no matter what country they are located in. There are countries in this world like the United States of America in which medical services are so expensive that should a person get sick and not have insurance - the medical bills can bankrupt them.
We have built Wotmed so that no matter what country patients are located in - they can become medical tourists and gain access to the medical services of Practitioners located in any country of the world. Our platform is country independent and is open to all the citizens of the world. We have built this platform for equal access and equal opportunity for all the Patients and Practitioners of this world.
We believe in the people of disruptive technology to change the world.  We believed in ourselves to develop that disruptive technology. Wotmed is the result of this belief. We know that we can help to make the world a better place and bring opportunity for access to affordable medical services to those who need them. We are the facilitators who have empowered you - Patients and Practitioners of the world to connect with each other for your mutual self interest.
To the brave men, women, children and transgendered patients who have no choice but to seek affordable medical services in the developing world - this platform is for you. Before Wotmed you had to go it alone. You had to face the fear of uncertainty in travelling to a foreign land for access to affordable medical services. You are not alone anymore. Wotmed is here to help you.
By creating a Wotmed profile - patients located anywhere in the world can connect with and establish relationships with practitioners located anywhere in the world in a libertarian spirit of freedom, individual liberty and voluntary association with little to no government interference.
We believe in the power of the free market. We have built an open and transparent global marketplace where the prices of all medical services are published and known by all the participants on the platform. Our platform is a global marketplace for the voluntarily establishment of relationships between patients and practitioners guided by each parties mutual self interest. Our platform enables patients to buy medical services from practitioners globally and includes the ability to book flights, hotel accommodation and car hire as part of this buying process.
We believe so much in the power of connecting Patients with Practitioners globally that we have spent the past year and a half working nights and weekends - we have given up our personal time and we have made great personal sacrifices to bring the Wotmed platform to the world. We have worked without funding or payment. We have worked with great passion, commitment and determination. We have worked with the knowledge that we may fail. We have worked with inspiration that the world needs the platform that we have developed. Nothing worthwhile is ever done without great effort. We have expended great time and effort to create the Wotmed platform.
So - I have talked a lot about why we have built Wotmed. I have told you today what I believe and why I believe it. Our goal is to do business with those of you who believe what we believe.
We believe in freedom, openness and transparency. We believe in the magic of a global, free market. We believe in human liberty and small government. We believe in peace and prosperity for all the citizens of the world. We welcome change as change brings disruption and creates opportunity. This opportunity can lead to more freedom.
I ask you to have faith in us and in the platform that we have developed for you. Here today I would like to ask you to help us by spreading the word about Wotmed to your friends, your family and your co-workers.
I ask you - Practitioners from every country of the world - to join us in our quest to help humanity. Create an account on Wotmed and establish relationships with patients from around the world who need your services. Fill out your Wotmed resume so we can connect you with patients globally who are in desperate need of your life saving skills.
I ask you - Patients from every country of the world - join us to connect with and establish relationships with Practitioners from around the world who can help you to gain access to affordable medical services.
No matter what country you come from or what your politics are - you are welcome on our platform.
No matter what your religious beliefs - you are welcome.
No matter if you are Straight, Gay, Lesbian, Bisexual or Trans Gendered - you are welcome on our platform.
Our platform exists to empower you - Practitioners and Patients of this world. It's now up to you to create an account on the platform and establish relationships with each other for your mutual self interest.
Once again - I would like to express my appreciation to you for taking the time out of your busy schedules to hear me talk today on our beliefs and why we have created Wotmed. Myself and the Wotmed team look forward with anticipation to doing business with those of you who believe what we believe.
Thank You. I am Clarke Towson."
}
</script>

<?php
	/*if ($_SERVER['HTTPS'] != "on") {
		$url = "https://". $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
		header("location: $url");
	}  */

	session_start();
	include("../includes/connect.php");
	include("../includes/functions.php");
	
	if(isset($_SESSION['id']))
	{
		$redirect="profile.php";
		if(isset($_SESSION['practitioner_id']))
			$redirect='practitioner_profile.php';
		redirect($redirect);
	}
	elseif(isset($_POST['email']))
	{
		$inputtext=$_POST['inputtext'];
	$inputpassword=$_POST['inputpassword'];
		login($conn,$inputtext,$inputpassword);
	}
?>



<!DOCTYPE html PUBLIC
"-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
		<title>Welcome to Wotmed</title>
		<meta http-equiv="Content-Type" content="'text'/html; charset=utf-8" />

    <!-- A minimal Flowplayer setup to get you started -->


    <!--
        include flowplayer JavaScript file that does
        Flash embedding and provides the Flowplayer API.
    -->
    <script type="text/javascript" src="../flowplayer-3.2.11.min.js"></script>




    <script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-38343871-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-49954656-1', 'wotmed.com');
		  ga('send', 'pageview');

		</script>

    <!-- Wotmed Chat System NOTE - MUST BE KEPT EXACTLY HERE-->
    <script type="text/javascript" src="http://platform.wotmed.com/WotmedChat/livechat/php/app.php?widget-init.js"></script>
    <!-- Wotmed Chat System -->

    <link href="../style/i_style.css" rel="stylesheet"></link>

    <link rel="stylesheet" type="text/css" href="../signup.css">




    <meta name="description" content="Wotmed.com - Wotmed Client Speech Submissions">
    <meta name="keywords" content="Wotmed.com - Packaging Medical Tourism.  Cheap Overseas Dental Care, Cheap overseas Dentists, Cheap Overseas Doctors, Overseas Cosmetic Surgery, Overseas Surgery, Overseas Dental Surgery, Tummy Tucks overseas, Boob Jobs overseas, Breast Augmentation overseas, Mummy Makeovers">


    <meta property="og:locale" content="en_US">
    <meta property="og:type" content="website">
    <meta property="og:title" content="Welcome to Wotmed">
    <meta property="og:description" content="Wotmed.com - Packaging Medical Tourism">
    <meta property="og:site_name" content="Wotmed">
    <meta property="og:url" content="http://platform.wotmed.com">

    <meta property="og:image" content="http://platform.wotmed.com/WotmedFacebookLogo6.png">
        
	</head>
	<body onload="searchUsers()">
		<div class="topBar" >
			<div class="bar_frame">
				<div class="w_logo"><img src="../images/WotmedLogoTransparent.png" height="45" alt="wotmedLogo" style='position:relative; left:-30px; padding:0px;padding-top:10px;' /></div>
				<div class="div_login">
					<form id="loginForm" method="post" action="../login.php">
					<table>
						<tr>
							<td><label>Email</label></td>
							<td><label>Password</label></td>
						</tr>
						<tr>
							<td width="150"><input type="text" class="inputtext required email" name="email" id="email"/></td>
							<td width="150"><input type="password" class="inputpassword required" name="password" id="password"/></td>
							<td><input type="submit" value="Login"/></td>
						</tr>
						<tr>
							<td><input name="persistent" type="checkbox" style="float:left; margin: 0px; padding: 0px;"/> <label>Keep me logged in</label></td>
							<td><a href="../resetPassword.php"><label style="cursor:pointer;text-decoration:underline;">Forget your password?</label></a></td>
                            
                           
						</tr>
                        
					</table>
					</form>
				<div id="errMsg">
				</div>
				</div>
				<div>
					<?php //include "fbConnect.php"; ?>
				</div>
				<div>
					<?php //include "googleConnect.php"; ?>
				</div>
			</div>
		</div>



		<div id="container">


            <h3><a href="#" onclick="return false;" class="about">Wotmed is the medical network that connects Patients with Practitioners and Surgery Facilitators globally</a></h3>
            <p>
                &nbsp;</p>

            <div id="system">


                <article class="item">


                    <?php

$_SESSION['pageNameDetails'] = basename($_SERVER['PHP_SELF']); /* Returns The Current PHP File Name */


$currentPageNameDetails = $_SESSION['pageNameDetails'];

$withoutExtLink = preg_replace('/\\.[^.\\s]{3,4}$/', '', $currentPageNameDetails);  // remove .php from the hyperlink

// $connGetAuthorDetails = new mysqli('localhost','wotmedco_user','Password1', 'WotmedDBLiveMirror');

$connGetSpeakerDetails = new mysqli('liberty.wotmed.com','wotmedco_user','e5~1]qu1|&8)6wJ', 'wotmedco_db');

if ($connGetSpeakerDetails->connect_error)
{
    header('Location: /DatabaseError.shtml');
}

$speakerDetails = $connGetSpeakerDetails->query("SELECT SPEECHSUBMISSIONDATE, SPEECHPUBLISHEDHYPERLINK, SPEAKERNAME, SPEAKERPOSITION, SPEAKERSHORTPHRASETOREMEMBER, SPEECHSUBMITTERCOUNTRY, PRACTICEORBUSINESSPHONE, SPEECHTITLE, SPEECHSUBJECT, SPEECHTRANSCRIPT, PRACTICEORBUSINESSSPECIALITY, PRACTICEORBUSINESSADDRESS, PRACTICEORBUSINESSINDEPENDENTWEBSITEHYPERLINK, PRACTICEORBUSINESSMARKETINGBLURB, PRACTICEORBUSINESSNAME FROM CLIENTSPEECHES WHERE SPEECHPUBLISHEDHYPERLINK = '$withoutExtLink'");

$connGetSpeakerDetails->close();

if ($speakerDetails->num_rows > 0)
{
    echo "<div class='content clearfix'>";
    echo "<div class='StoryContainer'>";


// output data of each row
   while($row = $speakerDetails->fetch_assoc())          // only show the form once as author may have submitted many articles
   {

                        echo "<h1><strong>";
                        echo $row["SPEECHTITLE"];
                        echo "</strong></h1>";

                        echo "<p> Speech Submission Date: ";
                        echo $row["SPEECHSUBMISSIONDATE"];
                        echo "</p>";

                        echo "<p> Speaker: ";
                        echo $row["SPEAKERNAME"];
                        echo "</p>";

                         echo "<p> Speaker Position: ";
                         echo $row["SPEAKERPOSITION"];
                         echo "</p>";

                        echo "<p> Speaker Speech Summary: ";
                         echo $row["SPEAKERSHORTPHRASETOREMEMBER"];
                         echo "</p>";

                         echo "<p> Country: ";
                        echo $row["SPEECHSUBMITTERCOUNTRY"];
                        echo "</p>";

                        echo "<p> Speech Title: ";
                       echo $row["SPEECHTITLE"];
                       echo "</p>";

                        echo "<p> Speech Subject: ";
                        echo $row["SPEECHSUBJECT"];
                         echo "</p>";

                         echo "<p> Speech Transcript: ";
                         echo $row["SPEECHTRANSCRIPT"];
                        echo "</p>";

                        echo "<p> Practice or Business Speciality: ";
                        echo $row["PRACTICEORBUSINESSSPECIALITY"];
                         echo "</p>";

                        echo "<p> Practice or Business Name: ";
                        echo $row["PRACTICEORBUSINESSNAME"];
                        echo "</p>";

                         echo "<p> Practice or Business Logo: ";
                        echo $row["PRACTICEORBUSINESSLOGO"];
                        echo "</p>";

                        echo "<p> Practice or Business Slogan: ";
                        echo $row["PRACTICEORBUSINESSMARKETINGBLURB"];
                         echo "</p>";

                        echo "<p> Practice or Business Blurb Image ";
                        echo $row["PRACTICEORBUSINESSMARKETINGBLURBIMAGE"];
                         echo "</p>";

                        echo "<p> Practice or Business Phone: ";
                        echo $row["PRACTICEORBUSINESSPHONE"];
                         echo "</p>";

                        echo "<p> Practice or Business Address: ";
                        echo $row["PRACTICEORBUSINESSADDRESS"];
                        echo "</p>";

                        echo "<p> Practice or Business Website: ";
                        $input = $row["PRACTICEORBUSINESSINDEPENDENTWEBSITEHYPERLINK"];

                        // in case scheme relative URI is passed, e.g., //www.google.com/
                        $input = trim($input, '/');

                        // If scheme not included, prepend it
                        if (!preg_match('#^http(s)?://#', $input))
                        {
                             $input = 'http://' . $input;
                        }
                         $urlParts = parse_url($input);

                        // remove www
                        $domain = preg_replace('/^www\./', '', $urlParts['host']);

                        $webLink = $domain;

                        $srtEmail=substr($webLink,0,20);

                        echo "<a href='http://$webLink' target='_blank'><span title='$webLink'>". $srtEmail ."</span></a>";
                        echo "</p>";

                    //    echo "<p> Practice Logo: ";
                    //    echo "<img src='../images/WotMedLogoMedium.jpg' width='80' height='20' alt='Wotmed' />";
                    //    echo "</p>";
   }
}

?>

                    </a><span style="text-align: center;"><a
                            href="../ImageUpload/uploads/clientspeeches/StartWithWHYMultiCamera.flv"
                            style="display:block;width:615px;height:344px"
                            id="player2"> </a></span></td>

                    <?php

                       //     $conn = new mysqli('localhost','wotmedco_user','Password1', 'WotmedDBLiveMirror');

                           $conn = new mysqli('liberty.wotmed.com','wotmedco_user','e5~1]qu1|&8)6wJ', 'wotmedco_db');






                            if ($conn->connect_error)
                            {
                                header('Location: /DatabaseError.shtml');
                            }

                            $authorName = " ";

                            ?>


                        <br>

                            <div class="WotmedOfficialCommentContainer">

                                <h1><strong>Wotmed Comment:</strong></h1>
                                <p> No Comment.<br> </p>

                                <br>


                                <a href="../WotmedClientSpeeches.php">Back to Wotmed Client Speeches</a>

                                <br>
                                <br>
                                <br>



                            <?php

                          //  echo "test";

                            // check to see if the speaker has chosen to be contactable by other patients
                            // if so - display the contact author form
                            // otherwise display a message saying they are not contactable by other patients



                            $_SESSION['pageName'] = basename($_SERVER['PHP_SELF']); /* Returns The Current PHP File Name */

                            $currentPageName = $_SESSION['pageName'];

                          //  echo $currentPageName;

                            $withoutExt = preg_replace('/\\.[^.\\s]{3,4}$/', '', $currentPageName);  // remove .php from the hyperlink

                         //   echo $withoutExt;

                            // select the users data
                            $result = $conn->query("SELECT SPEAKERNAME, SPEECHSUBMITTEREMAILADDRESS, SPEECHPUBLISHEDHYPERLINK, PRACTICEORBUSINESSISSPEAKERCONTACTABLE FROM CLIENTSPEECHES WHERE SPEECHPUBLISHEDHYPERLINK = '$withoutExt'");



                            $conn->close();

                            $onlyShowFormOnce = 0;

                            if ($result->num_rows > 0) {
                                // output data of each row
                                //   while($row = $result->fetch_assoc())          // only show the form once as speaker may have submitted many articles
                                //   {
                                //   echo "Name" . $row["STORYSUBMITTERNAME"]. " - Email Address: " . $row["STORYSUBMITTEREMAILADDRESS"] . "<br>";

                                $conn2 = new mysqli('liberty.wotmed.com','wotmedco_user','e5~1]qu1|&8)6wJ', 'wotmedco_db');

                            //    $conn2 = new mysqli('localhost','wotmedco_user','Password1', 'WotmedDBLiveMirror');

                             //   $contactable = $conn2->query("SELECT STORYHYPERLINK FROM PATIENTSTORIES WHERE STORYHYPERLINK = '$withoutExt'");

                                $contactable = $conn2->query("SELECT PRACTICEORBUSINESSISSPEAKERCONTACTABLE FROM CLIENTSPEECHES WHERE SPEECHPUBLISHEDHYPERLINK = '$withoutExt'");

                                $conn2->close();



                                while($row = $result->fetch_assoc())
                                {
                                    $isSpeakerContactable = $row["PRACTICEORBUSINESSISSPEAKERCONTACTABLE"];
                                  //  echo $isAuthorContactable;
                                }


                             //   if ($contactable->num_rows > 0) {

                                if ($isSpeakerContactable == "Yes")
                                {
                                    // the speaker is contactable


                                    echo "This speaker has chosen to be contactable via email.  You can use the form below to do this<br>";
                                    echo "
                                                <h1><strong>Contact this Speaker</strong></h1>


                                                </p>
                                                <form method='post' action='/proposals/proposalsubmit/WotmedClientSpeechContact.php' name='requestforProposal' id='requestforProposal'>
                                                    <table width='922' border='0'>
                                                        <tr>
                                                            <td width='282'>Name</td>
                                                            <td width='508'><label>
                                                                    <input name='Name' type='text' id='Name' size='70' />
                                                                </label></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Email Address</td>
                                                            <td><input name='EmailAddress' type='text' id='EmailAddress' size='40' /></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Your comments</td>
                                                            <td><label>
                                                                    <textarea name='Comments' id='Comments' cols='65' rows='10'></textarea>
                                                                </label></td>
                                                        </tr>
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                    </table>
                                                    <p>&nbsp;</p>
                                                    <p>
                                                        <label>
                                                            <input type='submit' name='SpeakerSpeechSubmission' id='signupsubmit' value='Contact Speaker' />
                                                        </label>
                                                    </p>

                                                </form>




";
                                }
                                else
                                {
                                    echo "This speaker has chosen not to be contactable via email.<br>";
                                }
                            }



                            ?>

                        <br><br><br>
                        <div class="fSma fcg">

                            <span>

                                 Wotmed Platform Status: <?php

                                $myfile = fopen("../WotmedPlatformStatus.txt", "r") or die("Undetermined");
                                echo fread($myfile,filesize("../WotmedPlatformStatus.txt"));
                                fclose($myfile);

                                ?>

                            </span>
                            <br>

                            <span>Copyright &copy; Wotmed.com <?php	echo date('Y'); ?> All rights reserved.  </span>
                        </div>





                                </p>



                                <p>&nbsp;</p>
                                <p>

                                    <!-- this will install flowplayer inside previous A- tag. -->
                                    <script>
                                        flowplayer("player2", "../flowplayer.commercial-3.2.12.swf", {
                                            // commercial version license key
                                            key: '#$1e327ef0a3f199461a1',



                                            clip: {
                                                // these two configuration variables does the trick

                                                autoPlay: false,
                                                accelerated: false,
                                                autoBuffering: true // <- do not place a comma here


                                            }
                                            ,


                                            plugins:  {
                                                controls: {

                                                    // tooltips configuration
                                                    tooltips: {

                                                        // enable english tooltips on all buttons
                                                        buttons: true,

                                                        // customized texts for buttons
                                                        play: 'Play',
                                                        pause: 'Pause',
                                                        fullscreen: 'Full Screen'
                                                    },

                                                    // background color for all tooltips
                                                    tooltipColor: '#195297',

                                                    // text color
                                                    tooltipTextColor: '#FFFFFF'
                                                }
                                            }
                                            ,



                                            // now we can tweak the menu
                                            contextMenu: [
                                                // 1. load related videos with jQuery and AJAX into the HTML DIV
                                                {
                                                    'Show related videos' : function() {
                                                        $("#related").load("/demos/configuration/related.html");
                                                    }
                                                },

                                                // 2. display custom player information
                                                "wotmed.com video player 1.1"
                                            ],


                                        });
                                    </script>
