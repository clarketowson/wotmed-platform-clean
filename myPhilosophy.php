<?php
	session_start();
	
	header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");


	include "includes/connect.php";
	include "includes/functions.php";
	include "classes/SimpleImage.php";
	if(isset($_SESSION['id'])){
		$rowSession=getParticipantDetail($conn,$_SESSION['id']);
		$row=getPractitionerDetail($conn,$_SESSION['id']);
	$ppFileNameSession="blankSilhouetteMale.png";
	$ppFileName="blankSilhouetteMale.png";
	
	// if the logged in user is a surgery facilitator then we want to show the profile photo at left as the business logo
// otherwise the user is a practitioner and we show the profile photo on the left as the practitioner photo

if($row['ISFACILITATOR'] == 1)
{
	if($row['PRACTITIONER_BUSINESSLOGO']=="")
	{
			$ppFileNameSession="blankSilhouetteMale.png";
	}else
	
		{
			$ppFileNameSession=$row['PRACTITIONER_BUSINESSLOGO'];
		}
		
	}

else
{
	if($row['PRACTITIONER_BUSINESSLOGO']!="")
	{
		$ppFileNameSession=$row['PRACTITIONER_BUSINESSLOGO'];
		$ppFileName=$row['PRACTITIONER_BUSINESSLOGO'];
	}
	if($rowSession['PROFILEPHOTO']!="")
	{
		$ppFileNameSession=$rowSession['PROFILEPHOTO'];
	}
	
}

	
	
//	if($row['PRACTITIONER_BUSINESSLOGO']!=""){
//		$ppFileNameSession=$row['PRACTITIONER_BUSINESSLOGO'];
//		$ppFileName=$row['PRACTITIONER_BUSINESSLOGO'];
//	}
//	if($rowSession['PROFILEPHOTO']!=""){
//		$ppFileNameSession=$rowSession['PROFILEPHOTO'];
//	}
	
	
		$query="SELECT COUNT(*) FROM PATIENTPRACTITIONERRELATIONSHIP WHERE PRACTITIONER_NUMBER = '" . $_SESSION['practitioner_id'] . "' AND RECOMMENDEDAS='1'";
		$tempOfRecommend=mysqli_query($conn,$query);
		if(mysqli_num_rows($tempOfRecommend)!=0)
			$numbOfRecommend=mysqli_fetch_array($tempOfRecommend);
		else
			$numbOfRecommend[]=0;
		if(isset($_POST) && $_POST!=NULL)
		{
			updateDetails($conn,mysqli_real_escape_string($conn,$_POST['detail_id']),$_POST);
			if(isset($_FILES) && $_FILES["flogo"]["name"]!=NULL)
			{
				uploadImage($conn,$_FILES,$row["PRACTITIONER_NUMBER"],mysqli_real_escape_string($conn,$_POST['detail_id']));
			}
			$_POST=null;
			?>
			<script language="javascript"> 
				<?php echo "window.location = 'myPhilosophy.php'";?>
			</script> <?php
		}
		
		$result=getDetails($conn,$row["PRACTITIONER_NUMBER"],14);
		if($result==null){
			insertDetails($conn,$row["PRACTITIONER_NUMBER"],null,14);
			$result=getDetails($conn,$row["PRACTITIONER_NUMBER"],14);
			$data=mysqli_fetch_array($result);
		}else{
			$data=mysqli_fetch_array($result);
		}
	}
?>
<!<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link href="<?php echo $path; ?>style/p_style.css" rel="stylesheet"></link>
<script type="text/javascript" src="<?php echo $path; ?>classes/jquery.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>My Philosophy</title>
<link href="style/apple.css" rel="stylesheet" type="text/css" />
</head>

<body>
<?php include $path."includes/p_header.php"; ?>
<?php
if($row['ISFACILITATOR'] == 1){
	$backLink = "
		<a href='facilitator/cpanel.php'>Back to Your Control Panel</a>
	";
	include $path."facilitator/cpanelHeader.php"; 
}
else{
	$backLink = "
		<a href='cpanel.php'>Back to Your Control Panel</a>
	";
	include $path."cpanelHeader.php"; 
}
?>
<div class='lfloat' style='width:98%'>
	<div class="stdWrapper" onMouseOver="this.style.background='white';"onmouseout="this.style.background='#F0F0F0';">
	  <p><span class="PractitionerBody">
	   <?php
	   
	   header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

	  if($data['DETAIL_IMAGE']!="")
		echo "<img src='photos/thumbs/" . $data['DETAIL_IMAGE'] . "' alt='' height='72' />";
	  else
		echo "<img src='images/stock-photo-stack-of-open-books-with-apple-isolated-on-white-108879077.jpg' alt='' width='72' height='72' />";
	  ?>
	  <h3 class="hyperlinks">Update Existing Philosophy</h3></span></p>
	  <p class="PractitionerMainText">Update  your existing philosophy here</p>
	  <form action="" method="post" enctype="multipart/form-data" name="UpdatePractitionerPromotion" id="UpdatePractitionerPromotion">
		<table width="789" border="0">
		  <tr>
			<td><span class="PractitionerMainText">Name</span></td>
			<td><span class="PractitionerMainText">
			  <input name="DETAIL_SUBTITLE" type="text" id="PhilosophyName2" size="80" maxlength="80" value="<?php echo $data['DETAIL_SUBTITLE']; ?>" />
			  <input name="detail_id" type="hidden" id="AboutHeadline2" value="<?php echo $data['PRACTITIONERDETAIL_ID']; ?>" />
			</span></td>
		  </tr>
		  <tr>
			<td><span class="PractitionerMainText">Philosophy Details</span></td>
			<td><span class="PractitionerMainText">
			  <label>
				<textarea name="DETAIL_MAINBODY" id="PhilosophyDetails2" cols="80" rows="20"><?php echo $data['DETAIL_MAINBODY']; ?></textarea>
			  </label>
			</span></td>
		  </tr>
		  <?php
			if($data["DETAIL_IMAGE"]!="")
				echo "<input type='hidden' name='DETAIL_IMAGE' value='" . $data['DETAIL_IMAGE'] . "'>";
		  ?>
		  <tr>
			<td><span class="PractitionerMainText">Images</span></td>
			<td><input type="file" name="flogo" id="flogo" value="Upload Logo" /></td>
		  </tr>
		  <tr>
			<td><span class="PractitionerMainText">Videos</span></td>
			<td><span class="PractitionerMainText">
			  <input type="PhilosophyVideos" name="PhilosophyVideos2" id="PhilosophyVideos2" />
			  <input type="submit" name="UploadPhilosophyVideos2" id="UploadPhilosophyVideos2" value="Upload" />
			</span></td>
		  </tr>
		  <tr>
			<td><span class="PractitionerMainText">PDF for client download</span></td>
			<td><span class="PractitionerMainText">
			  <input type="submit" name="PhilosophyPDFUpload2" id="PhilosophyPDFUpload2" value="Upload" />
			</span></td>
		  </tr>
		  <tr>
			<td><span class="PractitionerMainText">Social Network / Send Link</span></td>
			<td><span class="PractitionerMainText">
			  <input name="DETAIL_SOCIALNETWORK" type="text" id="NewsSocialLinks2" size="80" maxlength="80" value="<?php echo $data['DETAIL_SOCIALNETWORK']; ?>" />
			</span></td>
		  </tr>
		</table>
		<p class="PractitionerMainText">
		<label>
		  <input type="submit" name="PreviewPhilosophy" id="PreviewPhilosophy" value="Preview Philosophy" />
		</label>
		<input type="submit" name="UploadPhilosophy" id="UploadPhilosophy" value="Update Philosophy" />
	  </p>
	  </form>
	</div>
</div>
<p>&nbsp;</p>
<?php include $path."includes/p_footer.php"; ?>
</body>

</html>
