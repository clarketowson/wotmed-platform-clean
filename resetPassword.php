<!DOCTYPE html PUBLIC
"-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Reset Password</title>
		<meta http-equiv="Content-Type" content="'text'/html; charset=utf-8" />
		<script type="text/javascript" src="classes/jquery.js"></script>
		<script src="classes/jquery.validation.js"></script>
         
         <link rel="stylesheet" type="text/css" href="signup.css"></link>
         
		<link href="style/l_style.css" rel="stylesheet"></link>
        
        
         
		<script language="javascript">
			function checkEmail(email)
			{
				//alert(email);
				if (window.XMLHttpRequest)
				{// code for IE7+, Firefox, Chrome, Opera, Safari
					xmlhttp=new XMLHttpRequest();
				}
				else
				{// code for IE6, IE5
					xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
				}
				xmlhttp.onreadystatechange=function()
				{
					if (xmlhttp.readyState==4 && xmlhttp.status==200)
					{
						document.getElementById("result").innerHTML=xmlhttp.responseText;
						if(xmlhttp.responseText == "New password sent to your email"){
							window.location = "../login.php";
						}
					}
				}
				xmlhttp.open("GET","ajax/checkEmail.php?email="+email,true);
				xmlhttp.send();
			}
		</script>
	</head>
	<body>
		<div class="topBar" >
			<div class="bar_frame">
                <div class="w_logo"><img src="images/WotmedLogoTransparent.png" height="45" alt="wotmedLogo" style='position:relative; left:-30px; padding:0px;padding-top:10px;' /></div>
			</div>
		</div>

		<div id="container">
			<div align="center">
				<form action="" method="post">
					<table>
						<tr>
							<td>Email address</td>
							<td><input type="text" name="email" id="email"></td>
						</tr>
						<tr>
							<td colspan="2" align="center"><input type="button" id="reset" name="reset" value="Reset" onclick="checkEmail(email.value)"></td>
						</tr>
					</table>
				</form>
				<div id="result"></div>
			<div>
			<div id="footer">
				<div id="contentCurve"></div>
				<div class="clearfix" id="footerContainer">
					<div class="mrl lfloat" role="contentinfo">
						<div class="fSma fcg">
							<span>Wotmed &copy; 2015</span>
						</div>
					</div>
					<div class="navigation fSma fcg" role="navigation"><a href=
						"#" accesskey="9" title=
						"Review our terms and policies.">Terms</a> &middot; <a href=
						"#" accesskey="0" title=
						"Visit our Help Center.">Help</a>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>