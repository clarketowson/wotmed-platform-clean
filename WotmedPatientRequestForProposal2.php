

<!-- GOOGLE SPECIFIC STRUCTURED DATA DETAIL FOR THIS PAGE -->

<script type="application/ld+json">
{
"@context": "http://schema.org",
"@type": "Service",
"name": "Wotmed Request for Surgery Proposal",
"description": "Wotmed Request for Surgery Proposal enables you to receive responses from practitioners, hospitals and clinics in your preferred countries for you to review",
"url": "http://platform.wotmed.com/WotmedPatientRequestForProposal2.php"
}
}
</script>

<!-- GOOGLE SPECIFIC STRUCTURED DATA DETAIL FOR THIS PAGE -->

<?php
	/*if ($_SERVER['HTTPS'] != "on") {
		$url = "https://". $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
		header("location: $url");
	}  */

	session_start();
	include("includes/connect.php");
	include("includes/functions.php");
	
	if(isset($_SESSION['id']))
	{
		$redirect="profile.php";
		if(isset($_SESSION['practitioner_id']))
			$redirect='practitioner_profile.php';
		redirect($redirect);
	}
	elseif(isset($_POST['email']))
	{
		$inputtext=$_POST['inputtext'];
	$inputpassword=$_POST['inputpassword'];
		login($conn,$inputtext,$inputpassword);
	}
?>



<!DOCTYPE html PUBLIC
"-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
		<title>Wotmed Request for Proposal</title>
		<meta http-equiv="Content-Type" content="'text'/html; charset=utf-8" />

    <!-- ADDTHIS BUTTON BEGIN -->
    <script type="text/javascript">
        var addthis_config = {
            pubid: "ra-556a5489192d5a6e"
        }
    </script>

    <script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js"></script>
    <!-- ADDTHIS BUTTON END -->
        
        
 <script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-38343871-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-49954656-1', 'wotmed.com');
		  ga('send', 'pageview');

		</script>

       
		<script type="text/javascript" src="classes/jquery.js"></script>
		<script type="text/javascript" src="classes/hover.js"></script>
		<script src="classes/jquery.validation.js"></script>
		<script src="classes/dateSelectBoxes.js"></script>

    <!-- Wotmed Chat System NOTE - MUST BE KEPT EXACTLY HERE-->
    <script type="text/javascript" src="http://platform.wotmed.com/WotmedChat/livechat/php/app.php?widget-init.js"></script>
    <!-- Wotmed Chat System -->

    <link href="style/i_style.css" rel="stylesheet"></link>

    <link rel="stylesheet" type="text/css" href="signup.css">

    <link rel="stylesheet" href="/proposals/date/css/pikaday.css">

 <!--   <link rel="stylesheet" href="/proposals/date/css/site.css"> -->


 <!--   <style type="text/css">
        <!--
        #PatientBudget {	font-size:14px;
        }
        #PatientCountries {	font-size:14px;
        }
        #ProcedureList {
            font-size:14px;
        }
        #Submit {	background-color:#125c5d;
            -moz-border-radius:1px;
            -webkit-border-radius:1px;
            border-radius:1px;
            display:inline-block;
            cursor:pointer;
            color:#ffffff;
            font-family:arial;
            font-size:22px;
            padding:12px 50px;
            text-decoration:none;
            text-shadow:0px 1px 0px #125c5d;
        }

    </style> -->



    <meta name="description" content="Wotmed.com - Packaging Medical Tourism">
    <meta name="keywords" content="Wotmed.com - Packaging Medical Tourism.  Cheap Overseas Dental Care, Cheap overseas Dentists, Cheap Overseas Doctors, Overseas Cosmetic Surgery, Overseas Surgery, Overseas Dental Surgery, Tummy Tucks overseas, Boob Jobs overseas, Breast Augmentation overseas, Mummy Makeovers">

    <meta property="og:locale" content="en_US">
    <meta property="og:type" content="website">
    <meta property="og:title" content="Welcome to Wotmed">
    <meta property="og:description" content="Wotmed.com - Packaging Medical Tourism">
    <meta property="og:site_name" content="Wotmed">
    <meta property="og:url" content="http://platform.wotmed.com/WotmedPatientRequestForProposal2.php">
    <meta property="og:image" content="http://platform.wotmed.com/WotmedFacebookLogo2015.png">
        
	</head>
	<body onload="searchUsers()">
		<div class="topBar" >
			<div class="bar_frame">
				<div class="w_logo"><img src="images/WotmedLogoTransparent.png" height="45" alt="wotmedLogo" style='position:relative; left:-30px; padding:0px;padding-top:10px;' /></div>
				<div class="div_login">
					<form id="loginForm" method="post" action="login.php">
					<table>
						<tr>
							<td><label>Email</label></td>
							<td><label>Password</label></td>
						</tr>
						<tr>
							<td width="150"><input type="text" class="inputtext required email" name="email" id="email"/></td>
							<td width="150"><input type="password" class="inputpassword required" name="password" id="password"/></td>
							<td><input type="submit" value="Login"/></td>
						</tr>
						<tr>
							<td><input name="persistent" type="checkbox" style="float:left; margin: 0px; padding: 0px;"/> <label>Keep me logged in</label></td>
							<td><a href="resetPassword.php"><label style="cursor:pointer;text-decoration:underline;">Forget your password?</label></a></td>
                            
                           
						</tr>
                        
					</table>
					</form>
				<div id="errMsg">
				</div>
				</div>
				<div>
					<?php //include "fbConnect.php"; ?>
				</div>
				<div>
					<?php //include "googleConnect.php"; ?>
				</div>
			</div>
		</div>



		<div id="container">


            <h3><a href="#" onclick="return false;" class="about">Wotmed is the medical network that connects Patients with Practitioners and Surgery Facilitators globally</a></h3>
            <p>
                &nbsp;</p>

            <div id="system">


                <article class="item">




                    <div class="content clearfix">


                        <h1><strong>Wotmed Request for Proposal</strong></h1>

                        <a href="http://www.addthis.com/bookmark.php?v=250"
                           class="addthis_button"><img
                                src="http://s7.addthis.com/static/btn/v2/lg-share-en.gif"
                                width="125" height="16" border="0" alt="Share" /></a>

                        <p>Upon filling out this form you will receive responses from practitioners, hospitals and clinics in your preferred countries for you to review</p>
                        <form method="post" action="/proposals/proposalsubmit/RequestForProposal2.php" name="requestforProposal" id="requestforProposal">
                            <table width="922" border="0">
                                <tr>
                                    <td width="282">Name</td>
                                    <td width="508"><label>
                                            <input name="PatientName" type="text" id="PatientName" size="70" />
                                        </label></td>
                                </tr>
                                <tr>
                                    <td>Email</td>
                                    <td><input name="PatientEmail" type="text" id="PatientEmail" size="70" /></td>
                                </tr>
                                <tr>
                                    <td>Contact Phone Number</td>
                                    <td><input name="PatientPhone" type="text" id="PatientPhone" size="35" /></td>
                                </tr>
                                <tr>
                                    <td>What questions do you want answered?</td>
                                    <td><label>
                                            <textarea name="PatientQuestions" id="PatientQuestions" cols="65" rows="10"></textarea>
                                        </label></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>What procedure or work are you looking for?</td>
                                    <td><label>
                                            <select name="ProcedureList" id="ProcedureList" type="text">
                                                <option value="Breast Augmentation">Breast Augmentation</option>
                                                <option value="Abdominal Surgery">Abdominal Surgery</option>
                                                <option value="Abdominoplasty">Abdominoplasty</option>
                                                <option value="Ablative surgery">Ablative surgery</option>
                                                <option value="Acromioplasty">Acromioplasty</option>
                                                <option value="Adenoidectomy">Adenoidectomy</option>
                                                <option value="Adrenalectomy">Adrenalectomy</option>
                                                <option value="Allograft">Allograft</option>
                                                <option value="Amniotomy">Amniotomy</option>
                                                <option value="Amputation">Amputation</option>
                                                <option value="Amygdalohippocampectomy">Amygdalohippocampectomy</option>
                                                <option value="Anal Spinchterotomy">Anal Spinchterotomy</option>
                                                <option value="Angioplasty">Angioplasty</option>
                                                <option value="Anterior Temporal Lobectomy">Anterior Temporal Lobectomy</option>
                                                <option value="Appendectomy">Appendectomy</option>
                                                <option value="Arthrodesis">Arthrodesis</option>
                                                <option value="Arthroplasty">Arthroplasty</option>
                                                <option value="Arthroscopy">Arthroscopy</option>
                                                <option value="Arthrotomy">Arthrotomy</option>
                                                <option value="Asian Blepharoplasty">Asian Blepharoplasty</option>
                                                <option value="Astragalectomy">Astragalectomy</option>
                                                <option value="Auriculectomy">Auriculectomy</option>
                                                <option value="Autograft">Autograft</option>
                                                <option value="Axotomy">Axotomy</option>
                                                <option value="Bilateral Cingulotomy">Bilateral Cingulotomy</option>
                                                <option value="Biopsy">Biopsy</option>
                                                <option value="Blepharoplasty">Blepharoplasty</option>
                                                <option value="Bone anchored hearing aids">Bone anchored hearing aids</option>
                                                <option value="Bone Graft">Bone Graft</option>
                                                <option value="Botox">Botox</option>
                                                <option value="Brain Biopsy">Brain Biopsy</option>
                                                <option value="Breast Augmentation">Breast Augmentation</option>
                                                <option value="Breast Implant">Breast Implant</option>
                                                <option value="Breast Reconstruction">Breast Reconstruction</option>
                                                <option value="Breast Reduction">Breast Reduction</option>
                                                <option value="Bridges">Bridges</option>
                                                <option value="Bronchotomy">Bronchotomy</option>
                                                <option value="Brostrom Procedure">Brostrom Procedure</option>
                                                <option value="Browlift">Browlift</option>
                                                <option value="Browplasty">Browplasty</option>
                                                <option value="Burns Surgery">Burns Surgery</option>
                                                <option value="Bursectomy">Bursectomy</option>
                                                <option value="Buttock Lift">Buttock Lift</option>
                                                <option value="Cardiotomy">Cardiotomy</option>
                                                <option value="Cauterization">Cauterization</option>
                                                <option value="Ceramic (Clear) Braces">Ceramic (Clear) Braces</option>
                                                <option value="CEREC">CEREC</option>
                                                <option value="Cervicectomy">Cervicectomy</option>
                                                <option value="Cesarean Section">Cesarean Section</option>
                                                <option value="Cheek Augmentation">Cheek Augmentation</option>
                                                <option value="Chemical Peel">Chemical Peel</option>
                                                <option value="Chin Augmentation">Chin Augmentation</option>
                                                <option value="Cholecysostomy">Cholecysostomy</option>
                                                <option value="Cholecystectomy">Cholecystectomy</option>
                                                <option value="Circumcision">Circumcision</option>
                                                <option value="Clitoridectomy">Clitoridectomy</option>
                                                <option value="Clitoridotomy">Clitoridotomy</option>
                                                <option value="Clitoroplasty">Clitoroplasty</option>
                                                <option value="Coccygectomy">Coccygectomy</option>
                                                <option value="Colectomy">Colectomy</option>
                                                <option value="Collagen Injection">Collagen Injection</option>
                                                <option value="Colon Resection">Colon Resection</option>
                                                <option value="Colostomy">Colostomy</option>
                                                <option value="Colporrhaphy">Colporrhaphy</option>
                                                <option value="Commissurotomy">Commissurotomy</option>
                                                <option value="Consultation">Consultation</option>
                                                <option value="Contracture Surgery">Contracture Surgery</option>
                                                <option value="Cordotomy">Cordotomy</option>
                                                <option value="Corneal Transplantation">Corneal Transplantation</option>
                                                <option value="Corpectomy">Corpectomy</option>
                                                <option value="Cracked teeth treatment">Cracked teeth treatment</option>
                                                <option value="Craniofacial prostheses">Craniofacial prostheses</option>
                                                <option value="Craniofacial surgery">Craniofacial surgery</option>
                                                <option value="Craniotomy">Craniotomy</option>
                                                <option value="Cricothroidotomy">Cricothroidotomy</option>
                                                <option value="Cricothyrotomy">Cricothyrotomy</option>
                                                <option value="Crowns">Crowns</option>
                                                <option value="Custom made mouthguard">Custom made mouthguard</option>
                                                <option value="Cystectomy">Cystectomy</option>
                                                <option value="Cystostomy">Cystostomy</option>
                                                <option value="Dacryocystorhinostomy">Dacryocystorhinostomy</option>
                                                <option value="Decompressive Craniectomy">Decompressive Craniectomy</option>
                                                <option value="Dental Implant">Dental Implant</option>
                                                <option value="Dental implants">Dental implants</option>
                                                <option value="Dental trauma treatment">Dental trauma treatment</option>
                                                <option value="Dentoalveolar surgery">Dentoalveolar surgery</option>
                                                <option value="Dentofacial Orthopaedics">Dentofacial Orthopaedics</option>
                                                <option value="Dentures">Dentures</option>
                                                <option value="Diagnosis">Diagnosis</option>
                                                <option value="Discectomy">Discectomy</option>
                                                <option value="Diverticulectomy">Diverticulectomy</option>
                                                <option value="Elastics">Elastics</option>
                                                <option value="Embolectomy">Embolectomy</option>
                                                <option value="Endarterectomy">Endarterectomy</option>
                                                <option value="Endodontic Retreatment">Endodontic Retreatment</option>
                                                <option value="Endodontic Surgery">Endodontic Surgery</option>
                                                <option value="Endodontic Therapy">Endodontic Therapy</option>
                                                <option value="Endometrial Biopsy">Endometrial Biopsy</option>
                                                <option value="Endoscopic Thoracic Symphathectomy">Endoscopic Thoracic Symphathectomy</option>
                                                <option value="Epiphysiodesis">Epiphysiodesis</option>
                                                <option value="Episiotomy">Episiotomy</option>
                                                <option value="Escharotomy">Escharotomy</option>
                                                <option value="Esophagectomy">Esophagectomy</option>
                                                <option value="Examination">Examination</option>
                                                <option value="Expansion Appliances">Expansion Appliances</option>
                                                <option value="Eyelid Surgery">Eyelid Surgery</option>
                                                <option value="Facetectomy">Facetectomy</option>
                                                <option value="Fasciotomy">Fasciotomy</option>
                                                <option value="Fat Injection">Fat Injection</option>
                                                <option value="Femoral Head Ostectomy">Femoral Head Ostectomy</option>
                                                <option value="Filling">Filling</option>
                                                <option value="Fistulotomy">Fistulotomy</option>
                                                <option value="Flap Surgery">Flap Surgery</option>
                                                <option value="Fluoride Treatment">Fluoride Treatment</option>
                                                <option value="Foraminotomy">Foraminotomy</option>
                                                <option value="Foreskin Restoration">Foreskin Restoration</option>
                                                <option value="Frenectomy">Frenectomy</option>
                                                <option value="Frenuloplasty">Frenuloplasty</option>
                                                <option value="Ganglionectomy">Ganglionectomy</option>
                                                <option value="Gastoenterostomy">Gastoenterostomy</option>
                                                <option value="Gastrectomy">Gastrectomy</option>
                                                <option value="Gastroduodenostomy">Gastroduodenostomy</option>
                                                <option value="Gastropexy">Gastropexy</option>
                                                <option value="Gastrostomy">Gastrostomy</option>
                                                <option value="General Instruction / Patient Education">General Instruction / Patient Education</option>
                                                <option value="Genioplasty">Genioplasty</option>
                                                <option value="Gingivectomy">Gingivectomy</option>
                                                <option value="Glossectomy">Glossectomy</option>
                                                <option value="Gonadectomy">Gonadectomy</option>
                                                <option value="Grafting">Grafting</option>
                                                <option value="Grinding Jaw Pain">Grinding Jaw Pain</option>
                                                <option value="Gum Graft">Gum Graft</option>
                                                <option value="Hand surgery">Hand surgery</option>
                                                <option value="Headgear">Headgear</option>
                                                <option value="Heart Transplant">Heart Transplant</option>
                                                <option value="Heller Myotomy">Heller Myotomy</option>
                                                <option value="Hemicorporectomy">Hemicorporectomy</option>
                                                <option value="Hemilaminectomy">Hemilaminectomy</option>
                                                <option value="Hemipelvectomy">Hemipelvectomy</option>
                                                <option value="Hemispherectomy">Hemispherectomy</option>
                                                <option value="Hemorrhoidectomy">Hemorrhoidectomy</option>
                                                <option value="Hepatectomy">Hepatectomy</option>
                                                <option value="Hepatoportoenterostomy">Hepatoportoenterostomy</option>
                                                <option value="Herbst Appliance">Herbst Appliance</option>
                                                <option value="Hernia Repair">Hernia Repair</option>
                                                <option value="Hernioplasty">Hernioplasty</option>
                                                <option value="Hyaluronic Acis injection">Hyaluronic Acis injection</option>
                                                <option value="Hygiene Treatment">Hygiene Treatment</option>
                                                <option value="Hymeenorrhaphy">Hymeenorrhaphy</option>
                                                <option value="Hymenotomy">Hymenotomy</option>
                                                <option value="Hypnosurgery">Hypnosurgery</option>
                                                <option value="Hypophysectomy">Hypophysectomy</option>
                                                <option value="Hysterectomy">Hysterectomy</option>
                                                <option value="Hysterotomy">Hysterotomy</option>
                                                <option value="Iieostomy">Iieostomy</option>
                                                <option value="Implants">Implants</option>
                                                <option value="Inguinal Hernia Surgery">Inguinal Hernia Surgery</option>
                                                <option value="Injectable Cosmetic Treatments">Injectable Cosmetic Treatments</option>
                                                <option value="Inlays">Inlays</option>
                                                <option value="Invisalign">Invisalign</option>
                                                <option value="Invisalign Teen">Invisalign Teen</option>
                                                <option value="Iridectomy">Iridectomy</option>
                                                <option value="Irregular Adjustment of Braces">Irregular Adjustment of Braces</option>
                                                <option value="Jejunostomy">Jejunostomy</option>
                                                <option value="Khyphoplasty">Khyphoplasty</option>
                                                <option value="Kidney Transplantation">Kidney Transplantation</option>
                                                <option value="Labiaplasty">Labiaplasty</option>
                                                <option value="Laminectomy">Laminectomy</option>
                                                <option value="Laminotomy">Laminotomy</option>
                                                <option value="Laparoscopy">Laparoscopy</option>
                                                <option value="Laparotomy">Laparotomy</option>
                                                <option value="Laryngectomy">Laryngectomy</option>
                                                <option value="Laser Skin Resurfacing">Laser Skin Resurfacing</option>
                                                <option value="Lateral Internal Sphincterotomy">Lateral Internal Sphincterotomy</option>
                                                <option value="Lip Enhancement">Lip Enhancement</option>
                                                <option value="Lip reconstruction">Lip reconstruction</option>
                                                <option value="Liposuction">Liposuction</option>
                                                <option value="Lithotripsy">Lithotripsy</option>
                                                <option value="Liver Biopsy">Liver Biopsy</option>
                                                <option value="Lobectomy">Lobectomy</option>
                                                <option value="Lobotomy">Lobotomy</option>
                                                <option value="Lumpectomy">Lumpectomy</option>
                                                <option value="Lung Transplant">Lung Transplant</option>
                                                <option value="Lymph Node Biopsy">Lymph Node Biopsy</option>
                                                <option value="Lymphadenectomy">Lymphadenectomy</option>
                                                <option value="Mammoplasty">Mammoplasty</option>
                                                <option value="Masectomy">Masectomy</option>
                                                <option value="Mastectomy">Mastectomy</option>
                                                <option value="Mastoidectomy">Mastoidectomy</option>
                                                <option value="Mastopexy">Mastopexy</option>
                                                <option value="Mastoplexy">Mastoplexy</option>
                                                <option value="Maxillofacial implants">Maxillofacial implants</option>
                                                <option value="Maxillomandibular Advancement">Maxillomandibular Advancement</option>
                                                <option value="Meatotomy">Meatotomy</option>
                                                <option value="Mentoplasty">Mentoplasty</option>
                                                <option value="Micro Surgery">Micro Surgery</option>
                                                <option value="Microsurgery">Microsurgery</option>
                                                <option value="Midface Lift / Cheek Lift">Midface Lift / Cheek Lift</option>
                                                <option value="Muscle Biopsy">Muscle Biopsy</option>
                                                <option value="Myomectomy">Myomectomy</option>
                                                <option value="Myotomy">Myotomy</option>
                                                <option value="Myringotomy">Myringotomy</option>
                                                <option value="Nasal Surgery">Nasal Surgery</option>
                                                <option value="Neck Liposuction">Neck Liposuction</option>
                                                <option value="Nephrectomy">Nephrectomy</option>
                                                <option value="Nephropexy">Nephropexy</option>
                                                <option value="Nephrostomy">Nephrostomy</option>
                                                <option value="Nephrotomy">Nephrotomy</option>
                                                <option value="Nerve Biopsy">Nerve Biopsy</option>
                                                <option value="Neurectomy">Neurectomy</option>
                                                <option value="Neurosurgery">Neurosurgery</option>
                                                <option value="Nissen Fundoplication">Nissen Fundoplication</option>
                                                <option value="Nuss Procedure">Nuss Procedure</option>
                                                <option value="Oculoplastics">Oculoplastics</option>
                                                <option value="Omentopexy">Omentopexy</option>
                                                <option value="Onlays">Onlays</option>
                                                <option value="Oophorectomy">Oophorectomy</option>
                                                <option value="Orchidectomy">Orchidectomy</option>
                                                <option value="Orchiopexy">Orchiopexy</option>
                                                <option value="Orthognathic Surgery">Orthognathic Surgery</option>
                                                <option value="Ostectomy">Ostectomy</option>
                                                <option value="Osteotomy">Osteotomy</option>
                                                <option value="Otoplasty">Otoplasty</option>
                                                <option value="Palatal Expander">Palatal Expander</option>
                                                <option value="Palate Surgery">Palate Surgery</option>
                                                <option value="Palatoplasty">Palatoplasty</option>
                                                <option value="Pallidotomy">Pallidotomy</option>
                                                <option value="Pancreatectomy">Pancreatectomy</option>
                                                <option value="Pancreaticoduodenectomy">Pancreaticoduodenectomy</option>
                                                <option value="Panniculectomy">Panniculectomy</option>
                                                <option value="Parathyroidectomy">Parathyroidectomy</option>
                                                <option value="Pathology">Pathology</option>
                                                <option value="Penectomy">Penectomy</option>
                                                <option value="Percutaneous Endoscopic Gastrostomy">Percutaneous Endoscopic Gastrostomy</option>
                                                <option value="Pericardiectomy">Pericardiectomy</option>
                                                <option value="Pericardiotomy">Pericardiotomy</option>
                                                <option value="Periodic Regular Adjustment of Braces">Periodic Regular Adjustment of Braces</option>
                                                <option value="Phalloplasty">Phalloplasty</option>
                                                <option value="Photorefractive Keratectomy">Photorefractive Keratectomy</option>
                                                <option value="Pinealectomy">Pinealectomy</option>
                                                <option value="Pleurodesis">Pleurodesis</option>
                                                <option value="Pneumonectomy">Pneumonectomy</option>
                                                <option value="Pneumotomy">Pneumotomy</option>
                                                <option value="Positioner">Positioner</option>
                                                <option value="Posthectomy">Posthectomy</option>
                                                <option value="Proctocolectomy">Proctocolectomy</option>
                                                <option value="Prostate Biopsy">Prostate Biopsy</option>
                                                <option value="Prostatectomy">Prostatectomy</option>
                                                <option value="Psychosurgery">Psychosurgery</option>
                                                <option value="Punctoplasty">Punctoplasty</option>
                                                <option value="Pyeloplasty">Pyeloplasty</option>
                                                <option value="Pyloromyotomy">Pyloromyotomy</option>
                                                <option value="Radial Keratotomy">Radial Keratotomy</option>
                                                <option value="Radiography (X-Ray)">Radiography (X-Ray)</option>
                                                <option value="Radiology">Radiology</option>
                                                <option value="Radiosurgery">Radiosurgery</option>
                                                <option value="Reconstructive Surgery">Reconstructive Surgery</option>
                                                <option value="Reduction Mammoplasty">Reduction Mammoplasty</option>
                                                <option value="Removable Appliances">Removable Appliances</option>
                                                <option value="Renal Biopsy">Renal Biopsy</option>
                                                <option value="Retainers">Retainers</option>
                                                <option value="Rhinectomy">Rhinectomy</option>
                                                <option value="Rhinoplasty">Rhinoplasty</option>
                                                <option value="Rhizotomy">Rhizotomy</option>
                                                <option value="Rhytidectomy/Facelift">Rhytidectomy/Facelift</option>
                                                <option value="Root Canal">Root Canal</option>
                                                <option value="Root Canal Therapy">Root Canal Therapy</option>
                                                <option value="Rotationplasty">Rotationplasty</option>
                                                <option value="Salpingectomy">Salpingectomy</option>
                                                <option value="Salpingoophorectomy">Salpingoophorectomy</option>
                                                <option value="Scrotoplasty">Scrotoplasty</option>
                                                <option value="Sedation Sleep Dentistry">Sedation Sleep Dentistry</option>
                                                <option value="Separator">Separator</option>
                                                <option value="Septoplasty">Septoplasty</option>
                                                <option value="Setoplasty">Setoplasty</option>
                                                <option value="Sigmoidostomy">Sigmoidostomy</option>
                                                <option value="Sinusotomy">Sinusotomy</option>
                                                <option value="Skin Biopsy">Skin Biopsy</option>
                                                <option value="Sleep Apnea Treatment">Sleep Apnea Treatment</option>
                                                <option value="Snoring Sleep Apnoea">Snoring Sleep Apnoea</option>
                                                <option value="Sphincterotomy">Sphincterotomy</option>
                                                <option value="Spleen Transplantation">Spleen Transplantation</option>
                                                <option value="Splenectomy">Splenectomy</option>
                                                <option value="Splenopexy">Splenopexy</option>
                                                <option value="Stapedectomy">Stapedectomy</option>
                                                <option value="Subrapublic Cystostomy">Subrapublic Cystostomy</option>
                                                <option value="Suresmile">Suresmile</option>
                                                <option value="Surgery Facilitator Services">Surgery Facilitator Services</option>
                                                <option value="Surgical correction of facial asymmetry">Surgical correction of facial asymmetry</option>
                                                <option value="Sympathectomy">Sympathectomy</option>
                                                <option value="Symphysiotomy">Symphysiotomy</option>
                                                <option value="Synovectomy">Synovectomy</option>
                                                <option value="Teeth Cleaning">Teeth Cleaning</option>
                                                <option value="Teeth Polishing">Teeth Polishing</option>
                                                <option value="Teeth Scaling ">Teeth Scaling </option>
                                                <option value="Teeth Whitening">Teeth Whitening</option>
                                                <option value="Tendon Transfer">Tendon Transfer</option>
                                                <option value="Tenotomy">Tenotomy</option>
                                                <option value="Thalamotomy">Thalamotomy</option>
                                                <option value="Thoracotomy">Thoracotomy</option>
                                                <option value="Thrombectomy">Thrombectomy</option>
                                                <option value="Thymectomy">Thymectomy</option>
                                                <option value="Thymus Transplantation">Thymus Transplantation</option>
                                                <option value="Thyroidectomy">Thyroidectomy</option>
                                                <option value="Thyrotomy">Thyrotomy</option>
                                                <option value="Tonsillectomy">Tonsillectomy</option>
                                                <option value="Tooth Extraction">Tooth Extraction</option>
                                                <option value="Trabeculectomy">Trabeculectomy</option>
                                                <option value="Trabeculoplasty">Trabeculoplasty</option>
                                                <option value="Tracheostomy">Tracheostomy</option>
                                                <option value="Tracheotomy">Tracheotomy</option>
                                                <option value="Traditional Braces">Traditional Braces</option>
                                                <option value="Tubal Ligation">Tubal Ligation</option>
                                                <option value="Tubal Reversal">Tubal Reversal</option>
                                                <option value="Ulnar Collateral Ligament Reconstruction">Ulnar Collateral Ligament Reconstruction</option>
                                                <option value="Ureterosigmoidostomy">Ureterosigmoidostomy</option>
                                                <option value="Ureterostomy">Ureterostomy</option>
                                                <option value="Urethropexy">Urethropexy</option>
                                                <option value="Urethroplasty">Urethroplasty</option>
                                                <option value="Urostomy">Urostomy</option>
                                                <option value="Uvulopalatoplasty">Uvulopalatoplasty</option>
                                                <option value="Uvulotomy">Uvulotomy</option>
                                                <option value="Vaginectomy">Vaginectomy</option>
                                                <option value="Vaginoplasty">Vaginoplasty</option>
                                                <option value="Vagotomy">Vagotomy</option>
                                                <option value="Vasectomy">Vasectomy</option>
                                                <option value="Vasoepididymostomy">Vasoepididymostomy</option>
                                                <option value="Vasovasostomy">Vasovasostomy</option>
                                                <option value="Veneers">Veneers</option>
                                                <option value="Ventriculostomy">Ventriculostomy</option>
                                                <option value="Vertebrectomy">Vertebrectomy</option>
                                                <option value="Vertical Banded Gastroplasty">Vertical Banded Gastroplasty</option>
                                                <option value="Vitrectomy">Vitrectomy</option>
                                                <option value="Vulvectomy">Vulvectomy</option>
                                                <option value="White Fillings">White Fillings</option>
                                                <option value="Whitening">Whitening</option>
                                                <option value="X Rays">X Rays</option>
                                                <option value="Z-plasty">Z-plasty</option>
                                                <option value="OTHER">Procedure not listed - please specify in question section of form</option>
                                            </select>
                                        </label></td>
                                </tr>
                                <tr>
                                    <td>Any preference of country to travel to?</td>
                                    <td><select name="PatientCountries" id="PatientCountries" type="text">
                                            <option selected="Thailand">Thailand</option>
                                            <option value="Afghanistan">Afghanistan</option>
                                            <option value="Albania">Albania</option>
                                            <option value="Algeria">Algeria</option>
                                            <option value="American Samoa">American Samoa</option>
                                            <option value="Andorra">Andorra</option>
                                            <option value="Angola">Angola</option>
                                            <option value="Anguilla">Anguilla</option>
                                            <option value="Antarctica">Antarctica</option>
                                            <option value="Antigua and Barbuda">Antigua and Barbuda</option>
                                            <option value="Argentina">Argentina</option>
                                            <option value="Armenia">Armenia</option>
                                            <option value="Aruba">Aruba</option>
                                            <option value="Australia">Australia</option>
                                            <option value="Austria">Austria</option>
                                            <option value="Azerbaijan">Azerbaijan</option>
                                            <option value="Bahamas">Bahamas</option>
                                            <option value="Bahamas The">Bahamas The</option>
                                            <option value="Bahrain">Bahrain</option>
                                            <option value="Bangladesh">Bangladesh</option>
                                            <option value="Barbados">Barbados</option>
                                            <option value="Belarus">Belarus</option>
                                            <option value="Belgium">Belgium</option>
                                            <option value="Belize">Belize</option>
                                            <option value="Benin">Benin</option>
                                            <option value="Bermuda">Bermuda</option>
                                            <option value="Bhutan">Bhutan</option>
                                            <option value="Bolivia">Bolivia</option>
                                            <option value="Bosnia">Bosnia</option>
                                            <option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
                                            <option value="Botswana">Botswana</option>
                                            <option value="Brazil">Brazil</option>
                                            <option value="British Virgin Islands">British Virgin Islands</option>
                                            <option value="Brunei">Brunei</option>
                                            <option value="Bulgaria">Bulgaria</option>
                                            <option value="Burkina Faso">Burkina Faso</option>
                                            <option value="Burundi">Burundi</option>
                                            <option value="Cambodia">Cambodia</option>
                                            <option value="Cameroon">Cameroon</option>
                                            <option value="Canada">Canada</option>
                                            <option value="Cape Verde">Cape Verde</option>
                                            <option value="Cayman Islands">Cayman Islands</option>
                                            <option value="Central African Republic">Central African Republic</option>
                                            <option value="Chad">Chad</option>
                                            <option value="Chile">Chile</option>
                                            <option value="China">China</option>
                                            <option value="Christmas Island">Christmas Island</option>
                                            <option value="Colombia">Colombia</option>
                                            <option value="Comoros">Comoros</option>
                                            <option value="Congo">Congo</option>
                                            <option value="Congo Democratic Republic of the">Congo Democratic Republic of the</option>
                                            <option value="Cook Islands">Cook Islands</option>
                                            <option value="Costa Rica">Costa Rica</option>
                                            <option value="Cote d'Ivoire">Cote d'Ivoire</option>
                                            <option value="Croatia">Croatia</option>
                                            <option value="Cuba">Cuba</option>
                                            <option value="Curacao">Curacao</option>
                                            <option value="Cyprus">Cyprus</option>
                                            <option value="Czech Republic">Czech Republic</option>
                                            <option value="Denmark">Denmark</option>
                                            <option value="Djibouti">Djibouti</option>
                                            <option value="Dominica">Dominica</option>
                                            <option value="Dominican Republic">Dominican Republic</option>
                                            <option value="East Timor">East Timor</option>
                                            <option value="Ecuador">Ecuador</option>
                                            <option value="Egypt">Egypt</option>
                                            <option value="El Salvador">El Salvador</option>
                                            <option value="Equatorial Guinea">Equatorial Guinea</option>
                                            <option value="Eritrea">Eritrea</option>
                                            <option value="Estonia">Estonia</option>
                                            <option value="Ethiopia">Ethiopia</option>
                                            <option value="European Union">European Union</option>
                                            <option value="Falkland Islands">Falkland Islands</option>
                                            <option value="Faroe Islands">Faroe Islands</option>
                                            <option value="Federated States of Micronesia">Federated States of Micronesia</option>
                                            <option value="Fiji">Fiji</option>
                                            <option value="Finland">Finland</option>
                                            <option value="France">France</option>
                                            <option value="Gabon">Gabon</option>
                                            <option value="Gambia">Gambia</option>
                                            <option value="Gambia The">Gambia The</option>
                                            <option value="Georgia">Georgia</option>
                                            <option value="Germany">Germany</option>
                                            <option value="Ghana">Ghana</option>
                                            <option value="Gibraltar">Gibraltar</option>
                                            <option value="Greece">Greece</option>
                                            <option value="Greenland">Greenland</option>
                                            <option value="Grenada">Grenada</option>
                                            <option value="Guadeloupe">Guadeloupe</option>
                                            <option value="Guam">Guam</option>
                                            <option value="Guatemala">Guatemala</option>
                                            <option value="Guernsey">Guernsey</option>
                                            <option value="Guinea">Guinea</option>
                                            <option value="Guinea Bissau">Guinea Bissau</option>
                                            <option value="Guyana">Guyana</option>
                                            <option value="Haiti">Haiti</option>
                                            <option value="Herzegovina">Herzegovina</option>
                                            <option value="Honduras">Honduras</option>
                                            <option value="Hong Kong">Hong Kong</option>
                                            <option value="Hungary">Hungary</option>
                                            <option value="Iceland">Iceland</option>
                                            <option value="India">India</option>
                                            <option value="Indonesia">Indonesia</option>
                                            <option value="Iran">Iran</option>
                                            <option value="Iraq">Iraq</option>
                                            <option value="Ireland">Ireland</option>
                                            <option value="Isle Of Man">Isle Of Man</option>
                                            <option value="Israel">Israel</option>
                                            <option value="Italy">Italy</option>
                                            <option value="Jamaica">Jamaica</option>
                                            <option value="Japan">Japan</option>
                                            <option value="Jersey">Jersey</option>
                                            <option value="Jordan">Jordan</option>
                                            <option value="Kazakhstan">Kazakhstan</option>
                                            <option value="Kenya">Kenya</option>
                                            <option value="Kiribati">Kiribati</option>
                                            <option value="KoreaSouth">KoreaSouth</option>
                                            <option value="Kosovo">Kosovo</option>
                                            <option value="Kuwait">Kuwait</option>
                                            <option value="Kyrgyzstan">Kyrgyzstan</option>
                                            <option value="Laos">Laos</option>
                                            <option value="Latvia">Latvia</option>
                                            <option value="Lebanon">Lebanon</option>
                                            <option value="Lesotho">Lesotho</option>
                                            <option value="Liberia">Liberia</option>
                                            <option value="Libya">Libya</option>
                                            <option value="Liechtenstein">Liechtenstein</option>
                                            <option value="Lithuania">Lithuania</option>
                                            <option value="Luxembourg">Luxembourg</option>
                                            <option value="Macau">Macau</option>
                                            <option value="Macedonia">Macedonia</option>
                                            <option value="Macedonia Republic of">Macedonia Republic of</option>
                                            <option value="Madagascar">Madagascar</option>
                                            <option value="Maianmar">Maianmar</option>
                                            <option value="Malawi">Malawi</option>
                                            <option value="Malaysia">Malaysia</option>
                                            <option value="Maldives">Maldives</option>
                                            <option value="Mali">Mali</option>
                                            <option value="Malta">Malta</option>
                                            <option value="Marshall Islands">Marshall Islands</option>
                                            <option value="Mauritania">Mauritania</option>
                                            <option value="Mauritius">Mauritius</option>
                                            <option value="Mexico">Mexico</option>
                                            <option value="Micronesia">Micronesia</option>
                                            <option value="Moldova">Moldova</option>
                                            <option value="Monaco">Monaco</option>
                                            <option value="Mongolia">Mongolia</option>
                                            <option value="Mont Serrat">Mont Serrat</option>
                                            <option value="Montenegro">Montenegro</option>
                                            <option value="Morocco">Morocco</option>
                                            <option value="Mozambique">Mozambique</option>
                                            <option value="Myanmar">Myanmar</option>
                                            <option value="Myanmar (Burma)">Myanmar (Burma)</option>
                                            <option value="Namibia">Namibia</option>
                                            <option value="Nauru">Nauru</option>
                                            <option value="Nepal">Nepal</option>
                                            <option value="Netherlands">Netherlands</option>
                                            <option value="New Caledonia">New Caledonia</option>
                                            <option value="New Zealand">New Zealand</option>
                                            <option value="Nicaragua">Nicaragua</option>
                                            <option value="Niger">Niger</option>
                                            <option value="Nigeria">Nigeria</option>
                                            <option value="Niue">Niue</option>
                                            <option value="Norfolk Island">Norfolk Island</option>
                                            <option value="North Korea">North Korea</option>
                                            <option value="Northern Cyprus">Northern Cyprus</option>
                                            <option value="Northern Mariana Islands">Northern Mariana Islands</option>
                                            <option value="Norway">Norway</option>
                                            <option value="Oman">Oman</option>
                                            <option value="Pakistan">Pakistan</option>
                                            <option value="Palau">Palau</option>
                                            <option value="Palestinian Territories">Palestinian Territories</option>
                                            <option value="Panama">Panama</option>
                                            <option value="Papua New Guinea">Papua New Guinea</option>
                                            <option value="Paraguay">Paraguay</option>
                                            <option value="Peru">Peru</option>
                                            <option value="Philippines">Philippines</option>
                                            <option value="Pitcairn Islands">Pitcairn Islands</option>
                                            <option value="Poland">Poland</option>
                                            <option value="Portugal">Portugal</option>
                                            <option value="Puerto Rico">Puerto Rico</option>
                                            <option value="Qatar">Qatar</option>
                                            <option value="Republic of Macedonia">Republic of Macedonia</option>
                                            <option value="Romania">Romania</option>
                                            <option value="Russia">Russia</option>
                                            <option value="Rwanda">Rwanda</option>
                                            <option value="Saint Helena Ascension Tristan Da Cunha">Saint Helena Ascension Tristan Da Cunha</option>
                                            <option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
                                            <option value="Saint Lucia">Saint Lucia</option>
                                            <option value="Saint Vincent and the Grenadines">Saint Vincent and the Grenadines</option>
                                            <option value="Samoa">Samoa</option>
                                            <option value="San Marino">San Marino</option>
                                            <option value="Sao Tone and Preincipe">Sao Tone and Preincipe</option>
                                            <option value="Saudi Arabia">Saudi Arabia</option>
                                            <option value="Senegal">Senegal</option>
                                            <option value="Serbia">Serbia</option>
                                            <option value="Seychelles">Seychelles</option>
                                            <option value="Sierra Leone">Sierra Leone</option>
                                            <option value="Singapore">Singapore</option>
                                            <option value="Sint Maarten">Sint Maarten</option>
                                            <option value="Slovakia">Slovakia</option>
                                            <option value="Slovenia">Slovenia</option>
                                            <option value="Solomon Islands">Solomon Islands</option>
                                            <option value="Somalia">Somalia</option>
                                            <option value="South Africa">South Africa</option>
                                            <option value="South Korea">South Korea</option>
                                            <option value="Spain">Spain</option>
                                            <option value="Sri Lanka">Sri Lanka</option>
                                            <option value="St Lucia">St Lucia</option>
                                            <option value="St. Helena">St. Helena</option>
                                            <option value="St. Pierre and Miquelon">St. Pierre and Miquelon</option>
                                            <option value="Sudan South Sudan">Sudan South Sudan</option>
                                            <option value="Suriname">Suriname</option>
                                            <option value="Swaziland">Swaziland</option>
                                            <option value="Sweden">Sweden</option>
                                            <option value="Switzerland">Switzerland</option>
                                            <option value="Syria">Syria</option>
                                            <option value="Taiwan">Taiwan</option>
                                            <option value="Tajikistan">Tajikistan</option>
                                            <option value="Tanzania">Tanzania</option>
                                            <option value="Tasmania">Tasmania</option>
                                            <option value="Thailand">Thailand</option>
                                            <option value="Tobago">Tobago</option>
                                            <option value="Togo">Togo</option>
                                            <option value="Tokelau">Tokelau</option>
                                            <option value="Tonga">Tonga</option>
                                            <option value="Trinidad">Trinidad</option>
                                            <option value="Trinidad and Tobago">Trinidad and Tobago</option>
                                            <option value="Tunisia">Tunisia</option>
                                            <option value="Turkey">Turkey</option>
                                            <option value="Turkmenistan">Turkmenistan</option>
                                            <option value="Turks and Caicos Islands">Turks and Caicos Islands</option>
                                            <option value="Tuvalu">Tuvalu</option>
                                            <option value="Tuvalu">Tuvalu</option>
                                            <option value="Uganda">Uganda</option>
                                            <option value="Ukraine">Ukraine</option>
                                            <option value="United Arab Emirates">United Arab Emirates</option>
                                            <option value="United Kingdom">United Kingdom</option>
                                            <option value="United States">United States</option>
                                            <option value="Uruguay">Uruguay</option>
                                            <option value="US Virgin Islands">US Virgin Islands</option>
                                            <option value="Uzbekistan">Uzbekistan</option>
                                            <option value="Vanuatu">Vanuatu</option>
                                            <option value="Vatican City">Vatican City</option>
                                            <option value="Venezuela">Venezuela</option>
                                            <option value="Vietnam">Vietnam</option>
                                            <option value="Virgin Islands">Virgin Islands</option>
                                            <option value="Wallis and Futuna Islands">Wallis and Futuna Islands</option>
                                            <option value="Western Sahara">Western Sahara</option>
                                            <option value="Yemen">Yemen</option>
                                            <option value="Zambia">Zambia</option>
                                            <option value="Zimbabwe">Zimbabwe</option>
                                        </select>
                                        </label></td>
                                </tr>
                                <tr>
                                    <td>What is your budget in US dollars?</td>
                                    <td><select name="PatientBudget" id="PatientBudget" type="text">
                                            <option value="$1,000 to $2,500">$1,000 to $2,500</option>
                                            <option value="$2,500 to $5,000">$2,500 to $5,000</option>
                                            <option value="$5,000 to $7,500">$5,000 to $7,500</option>
                                            <option value="$7,500 to $10,000">$7,500 to $10,000</option>
                                            <option value="$10,000 to $12,500">$10,000 to $12,500</option>
                                            <option value="$12,500 to $15,000">$12,500 to $15,000</option>
                                            <option value="$15,000 to $17,500">$15,000 to $17,500</option>
                                            <option value="$17,500 to $20,000">$17,500 to $20,000</option>
                                            <option value="$20,000 to $22,500">$20,000 to $22,500</option>
                                            <option value="$22,500 to $25,000">$22,500 to $25,000</option>
                                            <option value="$25,000 to $27,500">$25,000 to $27,500</option>
                                            <option value="$27,500 to $30,000">$27,500 to $30,000</option>
                                            <option value="$30,000 to $32,500">$30,000 to $32,500</option>
                                            <option value="$32,500 to $35,000">$32,500 to $35,000</option>
                                            <option value="$35,000 to $37,500">$35,000 to $37,500</option>
                                            <option value="$37,500 to $40,000">$37,500 to $40,000</option>
                                            <option value="$40,000 to $42,500">$40,000 to $42,500</option>
                                            <option value="$42,500 to $45,000">$42,500 to $45,000</option>
                                            <option value="$45,000 to $47,500">$45,000 to $47,500</option>
                                            <option value="$47,500 to $50,000">$47,500 to $50,000</option>
                                            <option value="$50,000 to $100,000">$50,000 to $100,000</option>
                                            <option value="$100,000 to $200,000">$100,000 to $200,000</option>
                                            <option value="$200,000 to $300,000">$200,000 to $300,000</option>
                                            <option value="$300,000 to $400,000">$300,000 to $400,000</option>
                                            <option value="$400,000 to $500,000">$400,000 to $500,000</option>
                                            <option value="$500,000 to $600,000">$500,000 to $600,000</option>
                                            <option value="$600,000 to $700,000">$600,000 to $700,000</option>
                                            <option value="$700,000 to $800,000">$700,000 to $800,000</option>
                                            <option value="$800,000 to $900,000">$800,000 to $900,000</option>
                                            <option value="$900,000 to $1,000,000">$900,000 to $1,000,000</option>
                                            <option value="$1,000,000+">$1,000,000+</option>
                                        </select></td>
                                </tr>
                                <tr>
                                    <td>Do you need finance?</td>
                                    <td><input type="radio" name="q1" value="Yes">
                                        Yes
                                        <input type="radio" name="q1" value="No">
                                        No


                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td>When would you like to have your surgery?</td>
                                    <td><br>
                                        <input type="text" id="datepicker" name="datepicker">
                                        &nbsp;</td>
                                </tr>
                            </table>
                            <p>
                                <label>
                                    <input type="submit" name="RequestProposal" id='signupsubmit' value="Request Proposal" />
                                </label>
                            </p>
                            <p>&nbsp;</p>
                        </form>



                        <script src="/proposals/date/pikaday.js"></script>
                        <script>

                            var picker = new Pikaday(
                                {
                                    field: document.getElementById('datepicker'),
                                    firstDay: 1,
                                    minDate: new Date('2000-01-01'),
                                    maxDate: new Date('2020-12-31'),
                                    yearRange: [2000,2020]
                                });

                        </script>
                    </div>










            <p>&nbsp;</p>



        </div>


			<p>&nbsp;</p>
			<p>&nbsp;</p>
			<?php include "includes/p_footer.php"; ?>
