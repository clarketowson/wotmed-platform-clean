<?php
	session_start();
	include "../includes/connect.php";
	include "../includes/functions.php";
	include "../classes/SimpleImage.php";
	$path="../";
	$row=getPractitionerDetail($conn,$_SESSION['id']);
	$rowSession=getParticipantDetail($conn,$_SESSION['id']);
	
	if($rowSession['PROFILEPHOTO']==""){
		$ppFileNameSession="blankSilhouetteMale.png";
	}else{
		$ppFileNameSession=$rowSession['PROFILEPHOTO'];
	}
	$id=$_SESSION['id'];
	if(isset($_POST["detail"])){
		msgBox("Thank you for submitting your criticism to the Wotmed team.  A Wotmed representative will be in contact with you shortly.");
		$query="SELECT MAX(IDEA_NUMBER) FROM IDEA WHERE PARTICIPANT_NUMBER='$id'";
		$result=mysqli_query($conn,$query);
		$imgId=mysqli_fetch_array($result);
		if(isset($_FILES) && $_FILES['flogo']['name']!=NULL)
		{
			$res=uploadImageIdea($conn,$_FILES,$id,$imgId[0]-1+2);
		}
		$query="INSERT INTO IDEA (PARTICIPANT_NUMBER,IMAGE,DESCRIPTION,TRANSCRIPT,SUBMITDATE,CRITIC) VALUES('$id','$res','{$_POST["detail"]}','{$_POST["transcript"]}',NOW(),1)";
		mysqli_query($conn,$query);
		
		if(isset($_SESSION['practitioner_id'])){
			$name=$row["PRACTITIONER_BUSINESSNAME"];
			$type=0;
		}
		else{
			$name=$rowSession["FIRSTNAME"] . " " . $rowSession["SURNAME"];
			$type=1;
		}
		
		teamNotification("criticism@wotmed.com",$name,str_replace('\\n', "<br>", $_POST["detail"]),$res,"a critic",$type);
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link href="<?php echo $path; ?>style/p_style.css" rel="stylesheet"></link>
<script type="text/javascript" src="<?php echo $path; ?>classes/jquery.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Client Criticism</title>
<link href="style/apple.css" rel="stylesheet" type="text/css" />
</head>
<body>
<?php include $path."includes/p_header.php"; ?>
<div>
<?php if(isset($_SESSION['practitioner_id'])){ ?>
	<h3>Practitioner Submit your criticism to Wotmed</h3>
	<div>
		<img src="../images/Medical_Symbol1.jpg" height="50px">
		<img src="../images/300px-Thumbs-down-icon.png" height="50px">
		<img src="../images/WotmedLogoMedium.jpg" height="30px">
		
	</div>
	<p>
	Dear Doctor <?php echo $row["PRACTITIONER_BUSINESSNAME"];?>,<br><bR>

	The Wotmed team is eager to hear your constructive criticism. If there is something that you do not like about the Wotmed platform please let us know.<br><br>

	We are most interested to hear if there is anything about the Wotmed platform that does not live up to your expectations and the reasons why with specific evidence to back up your arguments. Please feel free to supply screenshots or other images and/or video to help support your case.<br><br>

	As a highly educated and intelligent Practitioner we will listen to your informed, well thought out constructive criticism and use it to improve the Wotmed platform.<br><br>
	</p>
<?php } 
else {?>
	<h3>Patient Submit your Criticism to Wotmed</h3>
	<div>
		<img src="../images/PatientLogin.jpg" height="50px">
		<img src="../images/300px-Thumbs-down-icon.png" height="50px">
		<img src="../images/WotmedLogoMedium.jpg" height="30px">
		
	</div>
	<p>
	Dear <?php echo $rowSession["FIRSTNAME"] . " " . $rowSession["SURNAME"];?>,<br><br>

	The Wotmed team is eager to hear your constructive criticism. If there is something that you do not like about the Wotmed platform please let us know.<br><br>

	We are most interested to hear if there is anything about the Wotmed platform that does not live up to your expectations and the reasons why with specific evidence to back up your arguments. Please feel free to supply screenshots or other images and/or video to help support your case.<br><br>

	We will listen to your informed, well thought out constructive criticism and use it to improve the Wotmed platform.<br><br>
	</p>
<?php } ?>
	<form action="#" method="post" enctype="multipart/form-data">
	<table>
		<tr>
			<td>Enter your criticism here</td>
			<td><textarea id="detail" name="detail"></textarea></td>
		</tr>
		<tr>
			<td>Upload any images that you would like us to review as part of your criticism</td>
			<td><input type="file" name="flogo" id="flogo" value="Upload Image" /></td>
		</tr>
		<tr>
			<td>Upload any video that you would like us to review as part of your criticism</td>
			<td><input type="file" name="flogo1" id="flogo1" value="Upload Video" /></td>
		</tr>
		<tr>
			<td>If you submit a video please also enter a transcript in text of what you said in the video</td>
			<td><textarea id="transcript" name="transcript"></textarea></td>
		</tr>
		<tr>
			<td colspan="2" align="center"><input type="submit" value="Submit your criticism to Wotmed"></td>
		</tr>
	</table>
	</form>
</div>
<?php include $path."includes/p_footer.php"; ?>
</body>
</html>
