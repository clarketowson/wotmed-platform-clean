<?php
	session_start();
	include "../includes/connect.php";
	include "../includes/functions.php";
	include "../classes/SimpleImage.php";
	$path="../";
	$row=getPractitionerDetail($conn,$_SESSION['id']);
	$rowSession=getParticipantDetail($conn,$_SESSION['id']);
	
	if($rowSession['PROFILEPHOTO']==""){
		$ppFileNameSession="blankSilhouetteMale.png";
	}else{
		$ppFileNameSession=$rowSession['PROFILEPHOTO'];
	}
	$id=$_SESSION['id'];
	if(isset($_POST["detail"])){
		msgBox("Thank you for submitting your capital ideas to the Wotmed team.  A Wotmed representative will be in contact with you shortly.");
		$query="SELECT MAX(IDEA_NUMBER) FROM IDEA WHERE PARTICIPANT_NUMBER='$id'";
		$result=mysqli_query($conn,$query);
		$imgId=mysqli_fetch_array($result);
		if(isset($_FILES) && $_FILES['flogo']['name']!=NULL)
		{
			$res=uploadImageIdea($conn,$_FILES,$id,$imgId[0]-1+2);
		}
		$query="INSERT INTO IDEA (PARTICIPANT_NUMBER,IMAGE,DESCRIPTION,TRANSCRIPT,SUBMITDATE,CRITIC) VALUES('$id','$res','{$_POST["detail"]}','{$_POST["transcript"]}',NOW(),0)";
		mysqli_query($conn,$query);
		
		if(isset($_SESSION['practitioner_id'])){
			$name=$row["PRACTITIONER_BUSINESSNAME"];
			$type=0;
		}
		else{
			$name=$rowSession["FIRSTNAME"] . " " . $rowSession["SURNAME"];
			$type=1;
		}
		
		
		teamNotification("capitalideas@wotmed.com",$name,str_replace('\\n', "<br>", $_POST["detail"]),$res,"an idea",$type);
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link href="<?php echo $path; ?>style/p_style.css" rel="stylesheet"></link>
<script type="text/javascript" src="<?php echo $path; ?>classes/jquery.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Capital Ideas</title>
<link href="style/apple.css" rel="stylesheet" type="text/css" />
</head>
<body>
<?php include $path."includes/p_header.php"; ?>
<div>
<?php if(isset($_SESSION['practitioner_id'])){ ?>
	<h3>Practitioner Submit your Capital Ideas to Wotmed</h3>
	<div>
		<img src="../images/Medical_Symbol1.jpg" height="50px">
		<img src="../images/128px-Action_idea.svg.png" height="50px">
		<img src="../images/WotmedLogoMedium.jpg" height="30px">
		
	</div>
	<p>
	Dear Doctor <?php echo $row["PRACTITIONER_BUSINESSNAME"];?>,<br><bR>

	As a Practitioner on the Wotmed platform you are a highly intelligent and highly educated professional with a leadership role and position of responsibility in the community thatt you serve. We want to give you an opportunity to serve a much larger group of people than you otherwise would be able to.<br><br>

	You have the ability to influence the Wotmed team if you are able to provide us with good ideas that we act upon and integrate into the Wotmed platform. Your ideas and improvement suggestions are a form of capital. This capital has value. Your good ideas can be leveraged globally when integrated into the Wotmed platform and can help us in our mission of service to humanity.<br><br>

	As a Practitioner and a leader you have a number of different kinds of capital which is economically scace and particularly valuable to us at Wotmed:<br><br>

	<b>Intellectual Capital:</b> Your knowledge, insight and thinking<br>

	<b>Social Capital:</b> The relationships that you have built over your lifetime<br>

	<b>Experiential Capital:</b> Your evaluated experiences - the wisdom that you have accumulated over your lifetime<br>

	<b>Talent Capital:</b> Your abilities, your strengths and your weaknesses<br>

	<b>Creative Capital:</b> Your ability to merge ideas, communicate thoughts, get out of the box<br>

	<b>Passion Capital:</b> your ability to energise yourself individually and others collectively<br><br>

	We woud like to ask you as a Practitioner to belong to something bigger than yourself and your clinic. Work with us at Wotmed towards our common goal of service to humanity. Help us to improve the Wotmed platform through the power of your idea capital. Have the confidence to submit your ideas to us at Wotmed. Follow your heart and your intuition as a Practitioner and as a human being and help us to make a difference to the world.<br><br>

	Thinking about what you have just read above - What would you love to see on the Wotmed platform? Think creatively. Combine your thoughts and feelings. Speak to us from your heart. Inspire us.<br><br>

	What do you want us to feel? What do you want us to remember? What do you want us to do?<br><br>

	When love and skill come together expect a masterpiece:<br><br>
	</p>
<?php } 
else {?>
	<h3>Patient Submit your Capital Ideas to Wotmed</h3>
	<div>
		<img src="../images/PatientLogin.jpg" height="50px">
		<img src="../images/128px-Action_idea.svg.png" height="50px">
		<img src="../images/WotmedLogoMedium.jpg" height="30px">
		
	</div>
	<p>
	Dear <?php echo $rowSession["FIRSTNAME"] . " " . $rowSession["SURNAME"];?>,<br><br>

	As a Patient on the Wotmed platform you have the potential for great leadership. We want to give you an opportunity to help people like you from all around the world.<br><br>

	You have the ability to influence the Wotmed team if you are able to provide us with good ideas that we act upon and integrate into the Wotmed platform. Your ideas and improvement suggestions are a form of capital. This capital has value. Your good ideas can be leveraged globally when integrated into the Wotmed platform and can help us in our mission of service to humanity.<br><br>

	As a patient on the Wotmed platform you have a number of different kinds of capital which is economically scace and particularly valuable to us at Wotmed:<br><br>

	<b>Intellectual Capital:</b> Your knowledge, insight and thinking<br>

	<b>Social Capital:</b> The relationships that you have built over your lifetime<br>

	<b>Experiential Capital:</b> Your evaluated experiences - the wisdom that you have accumulated over your lifetime<br>

	<b>Talent Capital:</b> Your abilities, your strengths and your weaknesses<br>

	<b>Creative Capital:</b> Your ability to merge ideas, communicate thoughts, get out of the box<br>

	<b>Passion Capital:</b> your ability to energise yourself individually and others collectively<br><br>

	We woud like to ask you as a Patient to belong to something bigger than yourself. Work with us at Wotmed towards our common goal of service to humanity. Help us to improve the Wotmed platform through the power of your idea capital. Have the confidence to submit your ideas to us at Wotmed. Follow your heart and your intuition as a Patient and as a human being and help us to make a difference to the world.<br><br>

	Thinking about what you have just read above - What would you love to see on the Wotmed platform? Think creatively. Combine your thoughts and feelings. Speak to us from your heart. Inspire us.<br><br>

	What do you want us to feel? What do you want us to remember? What do you want us to do?<br><br>

	When love and skill come together expect a masterpiece:<br><br>
	</p>
<?php } ?>
	<form action="#" method="post" enctype="multipart/form-data">
	<table>
		<tr>
			<td>Enter your capital ideas here</td>
			<td><textarea id="detail" name="detail"></textarea></td>
		</tr>
		<tr>
			<td>Upload any images that you would like us to review as part of your capital ideas</td>
			<td><input type="file" name="flogo" id="flogo" /></td>
		</tr>
		<tr>
			<td>Upload any video that you would like us to review as part of your capital ideas</td>
			<td><input type="file" name="flogo1" id="flogo1"/></td>
		</tr>
		<tr>
			<td>If you submit a video please also enter a transcript in text of what you said in the video</td>
			<td><textarea id="transcript" name="transcript"></textarea></td>
		</tr>
		<tr>
			<td colspan="2" align="center"><input type="submit" value="Submit Your Capital Idea"></td>
		</tr>
	</table>
	</form>
</div>
<?php include $path."includes/p_footer.php"; ?>
</body>
</html>
