<?php

ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(-1);

session_start();

include_once("../../paypal/sitebanneradvertising/config.php");
include_once("../../paypal/sitebanneradvertising/paypal.class.php");

include('../../thumbgenerator/image.class.php');  // class for the image thumbnail generator


$paypalmode = ($PayPalMode=='sandbox') ? '.sandbox' : '';

if($_POST) //Post Data received from product list page.
{
    //Mainly we need 4 variables from product page Item Name, Item Price, Item Number and Item Quantity.

    //Please Note : People can manipulate hidden field amounts in form,
    //In practical world you must fetch actual price from database using item id. Eg:
    //$ItemPrice = $mysqli->query("SELECT item_price FROM products WHERE id = Product_Number");

    $ItemName       = $_POST["itemname"]; //Item Name

    $ItemPrice      = $_POST["AdvertisementFee"]; //Item Price

    list($packageName, $packagePriceTotal) = explode('$', $ItemPrice);

    $ItemPrice = $packagePriceTotal;


    $ItemNumber     = $_POST["itemnumber"]; //Item Number
    $ItemDesc       = $_POST["itemdesc"]; //Item description
    $ItemQty        = $_POST["itemQty"]; // Item Quantity
    $ItemTotalPrice = ($ItemPrice*$ItemQty); //(Item Price x Quantity = Total) Get total amount of product;

    //Other important variables like tax, shipping cost
    $TotalTaxAmount     = 0.00;  //Sum of tax for all items in this order.
    $HandalingCost      = 0.00;  //Handling cost for this order.
    $InsuranceCost      = 0.00;  //shipping insurance cost for this order.
    $ShippinDiscount    = 0.00; //Shipping discount for this order. Specify this as negative number.
    $ShippinCost        = 0.00; //Although you may change the value later, try to pass in a shipping amount that is reasonably accurate.

    //Grand total including all tax, insurance, shipping cost and discount
    $GrandTotal = ($ItemTotalPrice + $TotalTaxAmount + $HandalingCost + $InsuranceCost + $ShippinCost + $ShippinDiscount);

    //Parameters for SetExpressCheckout, which will be sent to PayPal
    $padata =   '&METHOD=SetExpressCheckout'.
        '&RETURNURL='.urlencode($PayPalReturnURL ).
        '&CANCELURL='.urlencode($PayPalCancelURL).
        '&PAYMENTREQUEST_0_PAYMENTACTION='.urlencode("SALE").

        '&L_PAYMENTREQUEST_0_NAME0='.urlencode($ItemName).
        '&L_PAYMENTREQUEST_0_NUMBER0='.urlencode($ItemNumber).
        '&L_PAYMENTREQUEST_0_DESC0='.urlencode($ItemDesc).
        '&L_PAYMENTREQUEST_0_AMT0='.urlencode($ItemPrice).
        '&L_PAYMENTREQUEST_0_QTY0='. urlencode($ItemQty).

        /*
        //Additional products (L_PAYMENTREQUEST_0_NAME0 becomes L_PAYMENTREQUEST_0_NAME1 and so on)
        '&L_PAYMENTREQUEST_0_NAME1='.urlencode($ItemName2).
        '&L_PAYMENTREQUEST_0_NUMBER1='.urlencode($ItemNumber2).
        '&L_PAYMENTREQUEST_0_DESC1='.urlencode($ItemDesc2).
        '&L_PAYMENTREQUEST_0_AMT1='.urlencode($ItemPrice2).
        '&L_PAYMENTREQUEST_0_QTY1='. urlencode($ItemQty2).
        */

        /*
        //Override the buyer's shipping address stored on PayPal, The buyer cannot edit the overridden address.
        '&ADDROVERRIDE=1'.
        '&PAYMENTREQUEST_0_SHIPTONAME=J Smith'.
        '&PAYMENTREQUEST_0_SHIPTOSTREET=1 Main St'.
        '&PAYMENTREQUEST_0_SHIPTOCITY=San Jose'.
        '&PAYMENTREQUEST_0_SHIPTOSTATE=CA'.
        '&PAYMENTREQUEST_0_SHIPTOCOUNTRYCODE=US'.
        '&PAYMENTREQUEST_0_SHIPTOZIP=95131'.
        '&PAYMENTREQUEST_0_SHIPTOPHONENUM=408-967-4444'.
        */

        '&NOSHIPPING=0'. //set 1 to hide buyer's shipping address, in-case products that do not require shipping

        '&PAYMENTREQUEST_0_ITEMAMT='.urlencode($ItemTotalPrice).
        '&PAYMENTREQUEST_0_TAXAMT='.urlencode($TotalTaxAmount).
        '&PAYMENTREQUEST_0_SHIPPINGAMT='.urlencode($ShippinCost).
        '&PAYMENTREQUEST_0_HANDLINGAMT='.urlencode($HandalingCost).
        '&PAYMENTREQUEST_0_SHIPDISCAMT='.urlencode($ShippinDiscount).
        '&PAYMENTREQUEST_0_INSURANCEAMT='.urlencode($InsuranceCost).
        '&PAYMENTREQUEST_0_AMT='.urlencode($GrandTotal).
        '&PAYMENTREQUEST_0_CURRENCYCODE='.urlencode($PayPalCurrencyCode).
        '&LOCALECODE=GB'. //PayPal pages to match the language on your website.
        '&LOGOIMG=http://platform.wotmed.com/images/WotmedLogoMediumNew3.png'. //site logo
        '&CARTBORDERCOLOR=FFFFFF'. //border color of cart
        '&ALLOWNOTE=1';

    ############# set session variable we need later for "DoExpressCheckoutPayment" #######
    $_SESSION['ItemName']           =  $ItemName; //Item Name
    $_SESSION['ItemPrice']          =  $ItemPrice; //Item Price
    $_SESSION['ItemNumber']         =  $ItemNumber; //Item Number
    $_SESSION['ItemDesc']           =  $ItemDesc; //Item description
    $_SESSION['ItemQty']            =  $ItemQty; // Item Quantity
    $_SESSION['ItemTotalPrice']     =  $ItemTotalPrice; //total amount of product;
    $_SESSION['TotalTaxAmount']     =  $TotalTaxAmount;  //Sum of tax for all items in this order.
    $_SESSION['HandalingCost']      =  $HandalingCost;  //Handling cost for this order.
    $_SESSION['InsuranceCost']      =  $InsuranceCost;  //shipping insurance cost for this order.
    $_SESSION['ShippinDiscount']    =  $ShippinDiscount; //Shipping discount for this order. Specify this as negative number.
    $_SESSION['ShippinCost']        =   $ShippinCost; //Although you may change the value later, try to pass in a shipping amount that is reasonably accurate.
    $_SESSION['GrandTotal']         =  $GrandTotal;


    //We need to execute the "SetExpressCheckOut" method to obtain paypal token
    $paypal= new MyPayPal();
    $httpParsedResponseAr = $paypal->PPHttpPost('SetExpressCheckout', $padata, $PayPalApiUsername, $PayPalApiPassword, $PayPalApiSignature, $PayPalMode);

    //Respond according to message we receive from Paypal
    if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"]))
    {

        //Redirect user to PayPal store with Token received.
        $paypalurl ='https://www'.$paypalmode.'.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token='.$httpParsedResponseAr["TOKEN"].'';
        header('Location: '.$paypalurl);

    }else{
        //Show error message
        echo '<div style="color:red"><b>Error : </b>'.urldecode($httpParsedResponseAr["L_LONGMESSAGE0"]).'</div>';
        echo '<pre>';
        print_r($httpParsedResponseAr);
        echo '</pre>';
    }

}

//Paypal redirects back to this page using ReturnURL, We should receive TOKEN and Payer ID
if(isset($_GET["token"]) && isset($_GET["PayerID"]))
{
    //we will be using these two variables to execute the "DoExpressCheckoutPayment"
    //Note: we haven't received any payment yet.

    $token = $_GET["token"];
    $payer_id = $_GET["PayerID"];

    //get session variables
    $ItemName           = $_SESSION['ItemName']; //Item Name
    $ItemPrice          = $_SESSION['ItemPrice'] ; //Item Price
    $ItemNumber         = $_SESSION['ItemNumber']; //Item Number
    $ItemDesc           = $_SESSION['ItemDesc']; //Item Number
    $ItemQty            = $_SESSION['ItemQty']; // Item Quantity
    $ItemTotalPrice     = $_SESSION['ItemTotalPrice']; //total amount of product;
    $TotalTaxAmount     = $_SESSION['TotalTaxAmount'] ;  //Sum of tax for all items in this order.
    $HandalingCost      = $_SESSION['HandalingCost'];  //Handling cost for this order.
    $InsuranceCost      = $_SESSION['InsuranceCost'];  //shipping insurance cost for this order.
    $ShippinDiscount    = $_SESSION['ShippinDiscount']; //Shipping discount for this order. Specify this as negative number.
    $ShippinCost        = $_SESSION['ShippinCost']; //Although you may change the value later, try to pass in a shipping amount that is reasonably accurate.
    $GrandTotal         = $_SESSION['GrandTotal'];

    $padata =   '&TOKEN='.urlencode($token).
        '&PAYERID='.urlencode($payer_id).
        '&PAYMENTREQUEST_0_PAYMENTACTION='.urlencode("SALE").

        //set item info here, otherwise we won't see product details later
        '&L_PAYMENTREQUEST_0_NAME0='.urlencode($ItemName).
        '&L_PAYMENTREQUEST_0_NUMBER0='.urlencode($ItemNumber).
        '&L_PAYMENTREQUEST_0_DESC0='.urlencode($ItemDesc).
        '&L_PAYMENTREQUEST_0_AMT0='.urlencode($ItemPrice).
        '&L_PAYMENTREQUEST_0_QTY0='. urlencode($ItemQty).

        /*
        //Additional products (L_PAYMENTREQUEST_0_NAME0 becomes L_PAYMENTREQUEST_0_NAME1 and so on)
        '&L_PAYMENTREQUEST_0_NAME1='.urlencode($ItemName2).
        '&L_PAYMENTREQUEST_0_NUMBER1='.urlencode($ItemNumber2).
        '&L_PAYMENTREQUEST_0_DESC1=Description text'.
        '&L_PAYMENTREQUEST_0_AMT1='.urlencode($ItemPrice2).
        '&L_PAYMENTREQUEST_0_QTY1='. urlencode($ItemQty2).
        */

        '&PAYMENTREQUEST_0_ITEMAMT='.urlencode($ItemTotalPrice).
        '&PAYMENTREQUEST_0_TAXAMT='.urlencode($TotalTaxAmount).
        '&PAYMENTREQUEST_0_SHIPPINGAMT='.urlencode($ShippinCost).
        '&PAYMENTREQUEST_0_HANDLINGAMT='.urlencode($HandalingCost).
        '&PAYMENTREQUEST_0_SHIPDISCAMT='.urlencode($ShippinDiscount).
        '&PAYMENTREQUEST_0_INSURANCEAMT='.urlencode($InsuranceCost).
        '&PAYMENTREQUEST_0_AMT='.urlencode($GrandTotal).
        '&PAYMENTREQUEST_0_CURRENCYCODE='.urlencode($PayPalCurrencyCode);

    //We need to execute the "DoExpressCheckoutPayment" at this point to Receive payment from user.
    $paypal= new MyPayPal();
    $httpParsedResponseAr = $paypal->PPHttpPost('DoExpressCheckoutPayment', $padata, $PayPalApiUsername, $PayPalApiPassword, $PayPalApiSignature, $PayPalMode);

    //Check if everything went ok..
    if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"]))
    {

        echo '<h2>Success</h2>';
        echo 'Your Transaction ID : '.urldecode($httpParsedResponseAr["PAYMENTINFO_0_TRANSACTIONID"]);

        /*
        //Sometimes Payment are kept pending even when transaction is complete.
        //hence we need to notify user about it and ask him manually approve the transiction
        */

        if('Completed' == $httpParsedResponseAr["PAYMENTINFO_0_PAYMENTSTATUS"])
        {
            echo '<div style="color:green">Payment Received! Your advertising banner will be uploaded to the Wotmed site shortly.</div>';
        }
        elseif('Pending' == $httpParsedResponseAr["PAYMENTINFO_0_PAYMENTSTATUS"])
        {
            echo '<div style="color:red">Transaction Complete, but payment is still pending! '.
                'You need to manually authorize this payment in your <a target="_new" href="http://www.paypal.com">Paypal Account</a></div>';
        }

        // we can retrive transection details using either GetTransactionDetails or GetExpressCheckoutDetails
        // GetTransactionDetails requires a Transaction ID, and GetExpressCheckoutDetails requires Token returned by SetExpressCheckOut
        $padata =   '&TOKEN='.urlencode($token);
        $paypal= new MyPayPal();
        $httpParsedResponseAr = $paypal->PPHttpPost('GetExpressCheckoutDetails', $padata, $PayPalApiUsername, $PayPalApiPassword, $PayPalApiSignature, $PayPalMode);

        if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"]))
        {

            echo '<br /><b>Thank You for your business!</b><br /><pre>';
            /*
            #### SAVE BUYER INFORMATION IN DATABASE ###
            //see (http://www.sanwebe.com/2013/03/basic-php-mysqli-usage) for mysqli usage

            $buyerName = $httpParsedResponseAr["FIRSTNAME"].' '.$httpParsedResponseAr["LASTNAME"];
            $buyerEmail = $httpParsedResponseAr["EMAIL"];

            //Open a new connection to the MySQL server
            $mysqli = new mysqli('host','username','password','database_name');

            //Output any connection error
            if ($mysqli->connect_error) {
                die('Error : ('. $mysqli->connect_errno .') '. $mysqli->connect_error);
            }

            $insert_row = $mysqli->query("INSERT INTO BuyerTable
            (BuyerName,BuyerEmail,TransactionID,ItemName,ItemNumber, ItemAmount,ItemQTY)
            VALUES ('$buyerName','$buyerEmail','$transactionID','$ItemName',$ItemNumber, $ItemTotalPrice,$ItemQTY)");

            if($insert_row){
                print 'Success! ID of last inserted record is : ' .$mysqli->insert_id .'<br />';
            }else{
                die('Error : ('. $mysqli->errno .') '. $mysqli->error);
            }

            */

         //   echo '<pre>';
         //   print_r($httpParsedResponseAr);
         //   echo '</pre>';
        } else  {
            echo '<div style="color:red"><b>GetTransactionDetails failed:</b>'.urldecode($httpParsedResponseAr["L_LONGMESSAGE0"]).'</div>';
            echo '<pre>';
            print_r($httpParsedResponseAr);
            echo '</pre>';

        }

    }else{
        echo '<div style="color:red"><b>Error : </b>'.urldecode($httpParsedResponseAr["L_LONGMESSAGE0"]).'</div>';
        echo '<pre>';
        print_r($httpParsedResponseAr);
        echo '</pre>';
    }
}

if(!$_POST) exit;

// Email address verification, do not edit.
function isEmail($email) {
	return(preg_match("/^[-_.[:alnum:]]+@((([[:alnum:]]|[[:alnum:]][[:alnum:]-]*[[:alnum:]])\.)+(ad|ae|aero|af|ag|ai|al|am|an|ao|aq|ar|arpa|as|at|au|aw|az|ba|bb|bd|be|bf|bg|bh|bi|biz|bj|bm|bn|bo|br|bs|bt|bv|bw|by|bz|ca|cc|cd|cf|cg|ch|ci|ck|cl|cm|cn|co|com|coop|cr|cs|cu|cv|cx|cy|cz|de|dj|dk|dm|do|dz|ec|edu|ee|eg|eh|er|es|et|eu|fi|fj|fk|fm|fo|fr|ga|gb|gd|ge|gf|gh|gi|gl|gm|gn|gov|gp|gq|gr|gs|gt|gu|gw|gy|hk|hm|hn|hr|ht|hu|id|ie|il|in|info|int|io|iq|ir|is|it|jm|jo|jp|ke|kg|kh|ki|km|kn|kp|kr|kw|ky|kz|la|lb|lc|li|lk|lr|ls|lt|lu|lv|ly|ma|mc|md|me|mg|mh|mil|mk|ml|mm|mn|mo|mp|mq|mr|ms|mt|mu|museum|mv|mw|mx|my|mz|na|name|nc|ne|net|nf|ng|ni|nl|no|np|nr|nt|nu|nz|om|org|pa|pe|pf|pg|ph|pk|pl|pm|pn|pr|pro|ps|pt|pw|py|qa|re|ro|ru|rw|sa|sb|sc|sd|se|sg|sh|si|sj|sk|sl|sm|sn|so|sr|st|su|sv|sy|sz|tc|td|tf|tg|th|tj|tk|tm|tn|to|tp|tr|tt|tv|tw|tz|ua|ug|uk|um|us|uy|uz|va|vc|ve|vg|vi|vn|vu|wf|ws|ye|yt|yu|za|zm|zw)$|(([0-9][0-9]?|[0-1][0-9][0-9]|[2][0-4][0-9]|[2][5][0-5])\.){3}([0-9][0-9]?|[0-1][0-9][0-9]|[2][0-4][0-9]|[2][5][0-5]))$/i",$email));
}

if (!defined("PHP_EOL")) define("PHP_EOL", "\r\n");


// make sure to escape the users post data for security purposes

$conn = new mysqli('liberty.wotmed.com','wotmedco_user','e5~1]qu1|&8)6wJ', 'wotmedco_db');

// $conn = new mysqli('localhost','wotmedco_user','Password1', 'WotmedDBLiveMirror');

// $conn = new mysqli('liberty.wotmed.com','wotmedco_user','e5~1]qu1|&8)6wJ', 'wotmedco_db');

// Create the Banner Ad HTML code

$myfile = fopen("../../HTMLEmailTemplate/WotmedEmailTemplate/BannerAdvertisementUploaded.html", "w") or die("Unable to open file!");


// FIND THE LATEST FILE IN THE ImageUpload/uploads DIRECTORY

$path = "../../ImageUpload/uploads/banneradvertisements/";

$latest_ctime = 0;
$latest_filename = '';

$d = dir($path);
while (false !== ($entry = $d->read())) {
    $filepath = "{$path}/{$entry}";
    // could do also other checks than just checking whether the entry is a file
    if (is_file($filepath) && filectime($filepath) > $latest_ctime) {
        $latest_ctime = filectime($filepath);
        $latest_filename = $entry;
    }
}


$advertisementThumb = "Thumb" . $latest_filename;


// **************GENERATE THUMBNAIL HERE *******************

###############################################################
# Thumbnail Image Generator 1.3
###############################################################
# Visit http://www.zubrag.com/scripts/ for updates
###############################################################

// REQUIREMENTS:
// PHP 4.0.6 and GD 2.0.1 or later
// May not work with GIFs if GD2 library installed on your server
// does not support GIF functions in full

// Parameters:
// src - path to source image
// dest - path to thumb (where to save it)
// x - max width
// y - max height
// q - quality (applicable only to JPG, 1 to 100, 100 - best)
// t - thumb type. "-1" - same as source, 1 = GIF, 2 = JPG, 3 = PNG
// f - save to file (1) or output to browser (0).

// Sample usage:
// 1. save thumb on server
// http://www.zubrag.com/thumb.php?src=test.jpg&dest=thumb.jpg&x=100&y=50
// 2. output thumb to browser
// http://www.zubrag.com/thumb.php?src=test.jpg&x=50&y=50&f=0


// Below are default values (if parameter is not passed)

// save to file (true) or output to browser (false)
$save_to_file = true;

// Quality for JPEG and PNG.
// 0 (worst quality, smaller file) to 100 (best quality, bigger file)
// Note: PNG quality is only supported starting PHP 5.1.2
$image_quality = 100;

// resulting image type (1 = GIF, 2 = JPG, 3 = PNG)
// enter code of the image type if you want override it
// or set it to -1 to determine automatically
$image_type = -1;

// maximum thumb side size
$max_x = 400;
$max_y = 400;

// cut image before resizing. Set to 0 to skip this.
$cut_x = 0;
$cut_y = 0;

// Folder where source images are stored (thumbnails will be generated from these images).
// MUST end with slash.
$images_folder = '../../ImageUpload/uploads/banneradvertisements/';

// Folder to save thumbnails, full path from the root folder, MUST end with slash.
// Only needed if you save generated thumbnails on the server.
// Sample for windows:     c:/wwwroot/thumbs/
// Sample for unix/linux:  /home/site.com/htdocs/thumbs/
$thumbs_folder = '../../ImageUpload/uploads/banneradvertisements/';


///////////////////////////////////////////////////
/////////////// DO NOT EDIT BELOW
///////////////////////////////////////////////////

$to_name = '';

if (isset($_REQUEST['f'])) {
    $save_to_file = intval($_REQUEST['f']) == 1;
}

if (isset($_REQUEST['q'])) {
    $image_quality = intval($_REQUEST['q']);
}

if (isset($_REQUEST['t'])) {
    $image_type = intval($_REQUEST['t']);
}

if (isset($_REQUEST['x'])) {
    $max_x = intval($_REQUEST['x']);
}

if (isset($_REQUEST['y'])) {
    $max_y = intval($_REQUEST['y']);
}

if (!file_exists($images_folder)) die('Images folder does not exist (update $images_folder in the script)');
if ($save_to_file && !file_exists($thumbs_folder)) die('Thumbnails folder does not exist (update $thumbs_folder in the script)');

// Allocate all necessary memory for the image.
// Special thanks to Alecos for providing the code.
ini_set('memory_limit', '-1');

// include image processing code
// include('image.class.php');

$img = new Zubrag_image;

// initialize
$img->max_x        = $max_x;
$img->max_y        = $max_y;
$img->cut_x        = $cut_x;
$img->cut_y        = $cut_y;
$img->quality      = $image_quality;
$img->save_to_file = $save_to_file;
$img->image_type   = $image_type;

// generate thumbnail
// $img->GenerateThumbFile($images_folder . $from_name, $thumbs_folder . $to_name);

$img->GenerateThumbFile($images_folder . $latest_filename, $thumbs_folder . $advertisementThumb);


// **************GENERATE THUMBNAIL HERE *******************


//

$bannerCode = "
<!-- 3 columns -->
<table width='100%' bgcolor='#f7f7f7' cellpadding='0' cellspacing='0' border='0' id='backgroundTable'>
    <tbody>
    <tr>
        <td>
            <table width='600' cellpadding='0' cellspacing='0' border='0' align='center' class='devicewidth'>
                <tbody>
                <tr>
                    <td width='100%'>
                        <table width='600' cellpadding='0' cellspacing='0' border='0' align='center' class='devicewidth'>
                            <tbody>
                            <tr>
                                <td>
                                        <tr>
                                            <td width='186' height='250' align='center' class='devicewidth'>
                                                <img src='http://platform.wotmed.com/ImageUpload/uploads/banneradvertisements/$advertisementThumb' alt='' border='0' style='display:block; border:none; outline:none; text-decoration:none;'>
                                            </td>
                                        </tr>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>
<!-- end of 3 columns -->

";

// close the file

fwrite($myfile, $bannerCode);
fclose($myfile);


$name     = mysqli_real_escape_string($conn, $_POST['AdvertiserSubmitterName']);
$advertiserPhone    = mysqli_real_escape_string($conn, $_POST['AdvertiserSubmitterPhoneNumber']);
$email    = mysqli_real_escape_string($conn, $_POST['AdvertiserSubmitterEmailAddress']);
$advertisementTitle    = mysqli_real_escape_string($conn, $_POST['AdvertisementTitle']);
$advertisementSubject = mysqli_real_escape_string($conn, $_POST['AdvertisementSubject']);
$advertiserType = mysqli_real_escape_string($conn, $_POST['AdvertiserType']);
$advertisementPlacement = mysqli_real_escape_string($conn, $_POST['AdvertisementPlacement']);
$advertisementSEOKeywords = mysqli_real_escape_string($conn, $_POST['AdvertiserSEOKeywords']);
$advertisementText = mysqli_real_escape_string($conn, $_POST['AdvertisementText']);
$advertiserCountry  = mysqli_real_escape_string($conn, $_POST['AdvertiserSubmitterCountry']);
$practiceOrBusinessName = mysqli_real_escape_string($conn, $_POST['PracticeOrBusinessName']);
$practiceOrBusinessSpeciality = mysqli_real_escape_string($conn, $_POST['PracticeOrBusinessSpeciality']);
$practiceOrBusinessPhone = mysqli_real_escape_string($conn, $_POST['PracticeOrBusinessPhone']);
$practiceOrBusinessAddress = mysqli_real_escape_string($conn, $_POST['PracticeOrBusinessAddress']);

$advertisementSizeAndFee = mysqli_real_escape_string($conn, $_POST['AdvertisementFee']);



$practiceOrBusinessIndependentWebsiteHyperlink = mysqli_real_escape_string($conn, $_POST['PracticeOrBusinessIndependentWebsiteHyperlink']);
$practiceOrBusinessMarketingBlurb = mysqli_real_escape_string($conn, $_POST['PracticeOrBusinessMarketingBlurb']);


$practiceLogo = mysqli_real_escape_string($conn, $_POST['fileToUpload']);


$randomNumber = mt_rand(0, 100000);

// $storyHyperlink = date("Y-m-d") . $randomNumber . '.php';

$articleHyperlink = date("Y-m-d");



if(trim($name) == '') 
{
	echo '<div class="error_message">You must enter your name (we wont share this if you dont want us to).</div>';
	exit();
} 

else if(trim($email) == '') 
{
	echo '<div class="error_message">Please enter a valid email address (we wont share this if you dont want us to).</div>';
	exit();
	
}

else if(trim($advertiserPhone) == '')
{
	echo '<div class="error_message">Please enter a valid phone number (we wont share this).</div>';
	exit();
	
}

else if(!isEmail($email)) 
{
   echo '<div class="error_message">You have entered an invalid e-mail address, please try again (we wont share this if you dont want us to).</div>';
   exit();
}


if(get_magic_quotes_gpc()) 
{
    $articleText = stripslashes($articleText);
}

if ($conn->connect_error) 
{
header('Location: /DatabaseError.shtml');
}

// store the advertisers details

$result = $conn->query("INSERT INTO SITEBASEDADVERTISINGREQUEST(ADVERTISEMENTSUBMISSIONDATE, ADVERTISERNAME, ADVERTISERPHONE, ADVERTISEREMAIL, ADVERTISEMENTTITLE, ADVERTISEMENTSUBJECT, ADVERTISERTYPE, ADVERTISEMENTPLACEMENT, ADVERTISEMENTSEOKEYWORDS, ADVERTISEMENTTEXT, ADVERTISEMENTCOUNTRY, PRACTICEORBUSINESSNAME, PRACTICEORBUSINESSSPECIALITY, PRACTICEORBUSINESSPHONE, PRACTICEORBUSINESSADDRESS, PRACTICEORBUSINESSINDEPENDENTWEBSITEHYPERLINK, PRACTICEORBUSINESSMARKETINGBLURB, ADVERTISEMENTSIZEANDFEE, ADVERTISEMENTFILENAME) VALUES (NOW(), '$name', '$advertiserPhone ', '$email', '$advertisementTitle', '$advertisementSubject', '$advertiserType', '$advertisementPlacement', '$advertisementSEOKeywords', '$advertisementText', '$advertiserCountry', '$practiceOrBusinessName', '$practiceOrBusinessSpeciality', '$practiceOrBusinessPhone', '$practiceOrBusinessAddress', '$practiceOrBusinessIndependentWebsiteHyperlink', '$practiceOrBusinessMarketingBlurb', '$advertisementSizeAndFee', '$latest_filename')");

$conn->close();


// Configuration option.
// Enter the email address that you want to emails to be sent to.
// Example $address = "joe.doe@yourdomain.com";

//$address = "example@themeforest.net";

$address = "sitebanneradvertisingrequests@wotmed.com";

// $address = "patientstorysubmissions@wotmed.com";

// Configuration option.
// i.e. The standard subject will appear as, "You've been contacted by John Doe."

// Example, $e_subject = '$name . ' has contacted you via Your Website.';

//$e_subject = 'You\'ve been contacted by ' . $name . '.';


// Configuration option.
// You can change this if you feel that you need to.
// Developers, you may wish to add more fields to the form, in which case you must be sure to add them here.

$e_body = "$name has submitted a site banner advertising request";
$e_content = "\"$comments\"" . PHP_EOL . PHP_EOL;
$e_reply = "You can contact $name via email, $email";

$msg = wordwrap( $e_body . $e_content . $e_reply, 70 );


$headers = 'From: sitebanneradvertisingrequests@wotmed.com' . "\r\n" .
    'Reply-To: sitebanneradvertisingrequests@wotmed.com' . "\r\n" .
    'X-Mailer: PHP/' . phpversion();
$headers .= "MIME-Version: 1.0\r\n";
$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";


$subject = 'An advertiser: ' . $name . ' with email address: ' . $email . ' has just submitted a site banner advertisement request';

$message = file_get_contents('../../HTMLEmailTemplate/WotmedEmailTemplate/RequestforProposalHeader.html');
$message .= file_get_contents('../../HTMLEmailTemplate/WotmedEmailTemplate/AdvertisementBodyHeader.html');


$message .= 'Name: ' . $name . "<BR>" . 'Email: ' . $email . "<BR>" . 'Phone: ' . $advertiserPhone . "<BR>" . 'Advertiser Country: ' . $advertiserCountry . "<BR>" . 'Advertisement Title: ' . $advertisementTitle  . "<BR>";
$message .= 'Advertisement Subject: ' . $advertisementSubject . "<br>" . 'Advertisement Keywords: ' . $advertisementSEOKeywords  . "<BR>";
$message .= 'Advertisement: ' . $advertisementText . "<BR>";
$message .= 'Practice or Business Name: ' . $practiceOrBusinessName . "<BR>";
$message .= 'Practice or BusinessSpeciality: ' . $practiceOrBusinessSpeciality . "<BR>";
$message .= 'Practice or Business Phone: ' . $practiceOrBusinessPhone . "<BR>";
$message .= 'Practice or Business Address: ' . $practiceOrBusinessAddress . "<BR>";
$message .= 'Practice or Business Website: ' . $practiceOrBusinessIndependentWebsiteHyperlink . "<BR>";
$message .= 'Practice or Business Marketing Blurb: ' . $practiceOrBusinessMarketingBlurb . "<BR>";

$message .= 'Advertiser Type: ' . $advertiserType . "<BR>";
$message .= 'Advertisement Placement: ' . $advertisementPlacement . "<BR>";
$message .= 'Advertisement Size and Fee: ' . $advertisementSizeAndFee . "<BR>";

$message .= 'Uploaded Advertisement:' . "<BR>";
$message .= file_get_contents('../../HTMLEmailTemplate/WotmedEmailTemplate/BannerAdvertisementUploaded.html');

$message .= file_get_contents('../../HTMLEmailTemplate/WotmedEmailTemplate/RequestforProposalBodyFooter.html');

$message .= file_get_contents('../../HTMLEmailTemplate/WotmedEmailTemplate/RequestForProposalFooter.html');


if(mail($address, $subject, $message, $headers)) 
{

	include ("reg-advertiserSubmissionSuccess.php");

}


// send the practitioner themselves an email thanking them for submitting an article

$address2 = $email;	// set the email address to the practitioners email now

// $address2 = "practitionerarticlesubmissions@wotmed.com";

//	$address = "clarketowson@wotmed.com";

$headers2 = 'From: sitebanneradvertisingrequests@wotmed.com' . "\r\n" .
    'Reply-To: sitebanneradvertisingrequests@wotmed.com' . "\r\n" .
    'X-Mailer: PHP/' . phpversion();
$headers2 .= "MIME-Version: 1.0\r\n";
$headers2 .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

$subject2 = 'Wotmed - Thank you for submitting a site banner advertising request ' . $name;

$message2 = file_get_contents('../../HTMLEmailTemplate/WotmedEmailTemplate/RequestforProposalHeaderClient.html');
$message2 .= file_get_contents('../../HTMLEmailTemplate/WotmedEmailTemplate/AdvertiserBodyHeaderThankYou.html');

$message2 .= "<BR>" . 'Dear ' . $name . ',' . "<BR><BR>" . 'Thank you for submitting an advertising request.  A Wotmed representative will contact you shortly prior to your advertisement going live.' . "<BR><BR>";
$message2 .= 'If you have not already done so - Please sign up for a Wotmed Practitioner or Surgery Facilitator account now by going here: http://platform.wotmed.com/reg-practitioner.php or here http://platform.wotmed.com/reg-facilitator.php';
$message2 .= ' so that you can connect with Wotmed Practitioners and Surgery Facilitators globally';
$message2 .= "<BR><BR>" . 'Kind Regards,' . "<BR><BR>" . 'The Wotmed Team' . "<BR><BR>";

$message2 .= 'Your advertisement submission details:' . "<BR><BR>";

$message2 .= 'Name: ' . $name . "<BR>" . 'Email: ' . $email . "<BR>" . 'Phone: ' . $advertiserPhone . "<BR>" . 'Advertiser Country: ' . $advertiserCountry . "<BR>" . 'Advertisement Title: ' . $advertisementTitle  . "<BR>";
$message2 .= 'Advertisement Subject: ' . $advertisementSubject . "<br>" . 'Advertisement Keywords: ' . $advertisementSEOKeywords  . "<BR>";
$message2 .= 'Advertisement: ' . $advertisementText . "<BR>";
$message2 .= 'Practice or Business Name: ' . $practiceOrBusinessName . "<BR>";
$message2 .= 'Practice or BusinessSpeciality: ' . $practiceOrBusinessSpeciality . "<BR>";
$message2 .= 'Practice or Business Phone: ' . $practiceOrBusinessPhone . "<BR>";
$message2 .= 'Practice or Business Address: ' . $practiceOrBusinessAddress . "<BR>";
$message2 .= 'Practice or Business Website: ' . $practiceOrBusinessIndependentWebsiteHyperlink . "<BR>";
$message2 .= 'Practice or Business Marketing Blurb: ' . $practiceOrBusinessMarketingBlurb . "<BR>";

$message2 .= 'Advertiser Type: ' . $advertiserType . "<BR>";
$message2 .= 'Advertisement Placement: ' . $advertisementPlacement . "<BR>";
$message2 .= 'Advertisement Size and Fee: ' . $advertisementSizeAndFee . "<BR>";

$message2 .= 'Uploaded Advertisement:' . "<BR>";
$message2 .= file_get_contents('../../HTMLEmailTemplate/WotmedEmailTemplate/BannerAdvertisementUploaded.html');

$message2 .= file_get_contents('../../HTMLEmailTemplate/WotmedEmailTemplate/RequestforProposalBodyFooterClient.html');

$message2 .= file_get_contents('../../HTMLEmailTemplate/WotmedEmailTemplate/RequestForProposalFooterClient.html');

if(mail($address2, $subject2, $message2, $headers2)) 
{

	// Email has sent successfully, echo a success page.

//	echo "<fieldset>";
//	echo "<div id='success_page'>";
//	echo "<h1>Your request for proposal has been submitted successfully</h1>";
//	echo "<p>Thank you <strong>$name</strong>, you have successfully submitted your request for proposal.  A Wotmed team member will be in touch with you shortly</p>";
//	echo "</div>";
//	echo "</fieldset>";
	
// Update the Wotmed database and add the prospective customers details to the PROSPECTIVECUSTOMER TABLE

//$query="INSERT INTO PROSPECTIVECUSTOMER ('NAME', 'EMAILADDRESS', 'PHONENUMBER', 'QUESTIONS', 'PROCEDUREWANTED', 'COUNTRYPREFERENCE', 'EMAILADDRESS', 'BUDGET', //'FINANCEREQUIRED', 'PROPOSEDDATE') VALUES (  '$name', '$email', '$phone', '$questions', '$procedure', '$countries', '$budget', '$finance', '$date');";

//$query="INSERT INTO PROSPECTIVECUSTOMER ('NAME') VALUES ( '$name')";
		
//		mysqli_query($conn,$query);
	

// now email the client a confirmation email that Wotmed has received their request for proposal
	
// include ('contactClient.php');		

}


else 

{

	echo 'ERROR!';

}




