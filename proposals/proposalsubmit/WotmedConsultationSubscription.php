<?php

//ini_set('display_errors',1);
//ini_set('display_startup_errors',1);
//error_reporting(-1);

session_start();

$id=$_SESSION['practitioner_id'];

$conn = new mysqli('liberty.wotmed.com','wotmedco_user','e5~1]qu1|&8)6wJ', 'wotmedco_db');

// get the practitioners details

$query="SELECT PARTICIPANT_NUMBER, PRACTITIONER_BUSINESSNAME, PRACTITIONER_BUSINESSNUMBER, PRACTITIONER_BUSINESSADDRESS, PRACTITIONER_BUSINESSSUBURB, PRACTITIONER_BUSINESSPOSTCODE, PRACTITIONER_BUSINESSCITY, PRACTITIONER_BUSINESSSTATE, PRACTITIONER_BUSINESSLOGO FROM PRACTITIONER WHERE PRACTITIONER_NUMBER = $id";
$result = $conn->query($query);

while($row = $result->fetch_assoc())
{
    $participantNumber = $row["PARTICIPANT_NUMBER"];
    $practitionerBusinessName = $row["PRACTITIONER_BUSINESSNAME"];
    $practitionerBusinessNumber = $row["PRACTITIONER_BUSINESSNUMBER"];
    $practitionerBusinessAddress = $row["PRACTITIONER_BUSINESSADDRESS"];
    $practitionerBusinessSuburb = $row["PRACTITIONER_BUSINESSSUBURB"];
    $practitionerBusinessCity = $row["PRACTITIONER_BUSINESSCITY"];
    $practitionerBusinessState = $row["PRACTITIONER_BUSINESSSTATE"];
    $practitionerBusinessPostCode = $row["PRACTITIONER_BUSINESSPOSTCODE"];
    $practitionerBusinessLogo = $row["PRACTITIONER_BUSINESSLOGO"];

}


$query="SELECT PARTICIPANT_NUMBER, FIRSTNAME, SURNAME, EMAILADDRESS, PROFILEPHOTO FROM PARTICIPANT WHERE PARTICIPANT_NUMBER = $participantNumber";
$result = $conn->query($query);

while($row = $result->fetch_assoc())
{
    $practitionerEmailAddress = $row["EMAILADDRESS"];
    $practitionerProfilePhoto = $row["PROFILEPHOTO"];
    $practitionerFirstName = $row["FIRSTNAME"];
    $practitionerSurName = $row["SURNAME"];

}

// get the practitioners number

$query ="SELECT PRACTITIONER_NUMBER FROM PRACTITIONER WHERE PARTICIPANT_NUMBER = $participantNumber";
$result = $conn->query($query);
while($row = $result->fetch_assoc())
{
    $practitionerNumber = $row["PRACTITIONER_NUMBER"];
}


//if(!$_POST) exit;

if (!defined("PHP_EOL")) define("PHP_EOL", "\r\n");


// store the prospective users information in the Wotmed Database in the PATIENTSTORIES table

// make sure to escape the users post data for security purposes

$conn = new mysqli('liberty.wotmed.com','wotmedco_user','e5~1]qu1|&8)6wJ', 'wotmedco_db');

// $conn = new mysqli('localhost','wotmedco_user','Password1', 'WotmedDBLiveMirror');

// $conn = new mysqli('liberty.wotmed.com','wotmedco_user','e5~1]qu1|&8)6wJ', 'wotmedco_db');

$practitionerName     = mysqli_real_escape_string($conn, $_POST['YourPractitionerName']);
$hourlyRate = mysqli_real_escape_string($conn, $_POST['HourlyRate']);

$servicesOfferedTelephone = mysqli_real_escape_string($conn, $_POST['ConsultingServicesOfferedTelephone']);
$servicesOfferedSkypeVoice = mysqli_real_escape_string($conn, $_POST['ConsultingServicesOfferedSkypeVoice']);
$servicesOfferedSkypeVideo = mysqli_real_escape_string($conn, $_POST['ConsultingServicesOfferedSkypeVideo']);
$servicesOfferedEmail = mysqli_real_escape_string($conn, $_POST['ConsultingServicesOfferedEmail']);
$servicesOfferedInPerson = mysqli_real_escape_string($conn, $_POST['ConsultingServicesOfferedInPerson']);


$phone    = mysqli_real_escape_string($conn, $_POST['PractitionerPhoneNumberConsulting']);
$paypalEmailAddress = mysqli_real_escape_string($conn, $_POST['PractitionerPayPalEmailAddress']);
$skypeID = mysqli_real_escape_string($conn, $_POST['PractitionerSkypeUserID']);

$practiceName = mysqli_real_escape_string($conn, $_POST['PracticeOrBusinessName']);
$practiceSpeciality = mysqli_real_escape_string($conn, $_POST['PracticeOrBusinessSpeciality']);
$practiceAddress = mysqli_real_escape_string($conn, $_POST['PracticeOrBusinessAddress']);


if ($conn->connect_error) 
{
header('Location: /DatabaseError.shtml');
}

// store the practitioner or facilitators consultation details


$result = $conn->query("INSERT IGNORE INTO WOTMEDPAIDCONSULTING(PARTICIPANT_NUMBER, HOURLYRATE, PHONENUMBER, EMAILADDRESS, SKYPEUSERID, PRACTICEORBUSINESSNAME, PRACTICEORBUSINESSSPECIALITY, PRACTICEORBUSINESSADDRESS, TELEPHONECONSULT, SKYPEVOICECONSULT, SKYPEVIDEOCONSULT, EMAILCONSULT, INPERSONCONSULT) VALUES ('$practitionerNumber', '$hourlyRate', '$phone', '$paypalEmailAddress', '$skypeID', '$practiceName', '$practiceSpeciality', '$practiceAddress', '$servicesOfferedTelephone', '$servicesOfferedSkypeVoice', '$servicesOfferedSkypeVideo', '$servicesOfferedEmail', '$servicesOfferedInPerson')");

$conn->close();


// Configuration option.
// Enter the email address that you want to emails to be sent to.
// Example $address = "joe.doe@yourdomain.com";

//$address = "example@themeforest.net";

$address = "wotmedconsultations@wotmed.com";

// $address = "patientstorysubmissions@wotmed.com";

// Configuration option.
// i.e. The standard subject will appear as, "You've been contacted by John Doe."

// Example, $e_subject = '$name . ' has contacted you via Your Website.';

//$e_subject = 'You\'ve been contacted by ' . $name . '.';


// Configuration option.
// You can change this if you feel that you need to.
// Developers, you may wish to add more fields to the form, in which case you must be sure to add them here.

$e_body = "$practitionerName has signed up to the Wotmed Paid Consultation Program";
$e_content = "\"$comments\"" . PHP_EOL . PHP_EOL;
$e_reply = "You can contact $practitionerName via email, $paypalEmailAddress";

$msg = wordwrap( $e_body . $e_content . $e_reply, 70 );


$headers = 'From: wotmedconsultations@wotmed.com' . "\r\n" .
    'Reply-To: wotmedconsultations@wotmed.com' . "\r\n" .
    'X-Mailer: PHP/' . phpversion();
$headers .= "MIME-Version: 1.0\r\n";
$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

$subject = 'A practitioner: ' . $practitionerName . ' with PayPal email address: ' . $paypalEmailAddress . ' has just subscribed to the Wotmed Paid Consultation program';

$message = file_get_contents('../../HTMLEmailTemplate/WotmedEmailTemplate/RequestforProposalHeader.html');
$message .= file_get_contents('../../HTMLEmailTemplate/WotmedEmailTemplate/PaidConsultationsBodyHeader.html');

$message .= 'Practitioner Name: ' . $practitionerName . "<BR>" . 'Practitioner PayPal Email Address: ' . $paypalEmailAddress . '<BR><BR>';

$message .= 'Consulting Services Telephone: ' . $servicesOfferedTelephone . "<BR>";
$message .= 'Consulting Services Skype Voice: ' . $servicesOfferedSkypeVoice . "<BR>";
$message .= 'Consulting Services Skype Video: ' . $servicesOfferedSkypeVideo . "<BR>";
$message .= 'Consulting Services Email: ' . $servicesOfferedEmail . "<BR>";
$message .= 'Consulting Services In Person: ' . $servicesOfferedInPerson . "<BR><BR>";

$message .= 'Hourly Rate: $' . $hourlyRate . "<BR>" . 'Skype ID: ' . $skypeID . "<BR>" . 'Consulting Phone Number: ' . $phone . "<BR>" ;
$message .= 'Practice Name: ' . $practiceName . "<BR>";
$message .= 'Speciality: ' . $practiceSpeciality . "<BR>";
$message .= 'Practice Address: ' . $practiceAddress . "<BR>";

$message .= file_get_contents('../../HTMLEmailTemplate/WotmedEmailTemplate/RequestforProposalBodyFooter.html');

$message .= file_get_contents('../../HTMLEmailTemplate/WotmedEmailTemplate/RequestForProposalFooter.html');



if(mail($address, $subject, $message, $headers)) 
{

   include("http://platform.wotmed.com/proposals/proposalsubmit/reg-consultingSuccess.php");

  //  header("/proposals/proposalsubmit/reg-consultingSuccess.php");

}


// send the practitioner themselves an email thanking them for signing up to Wotmed Paid Consulting

$address2 = $paypalEmailAddress;	// set the email address to the practitioners email now

$headers2 = 'From: wotmedconsultations@wotmed.com' . "\r\n" .
    'Reply-To: wotmedconsultations@wotmed.com' . "\r\n" .
    'X-Mailer: PHP/' . phpversion();
$headers2 .= "MIME-Version: 1.0\r\n";
$headers2 .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

$subject2 = 'Wotmed - Thank you for signing up to the Wotmed Consulting Program ' . $practitionerName;

$message2 = file_get_contents('../../HTMLEmailTemplate/WotmedEmailTemplate/RequestforProposalHeaderClient.html');
$message2 .= file_get_contents('../../HTMLEmailTemplate/WotmedEmailTemplate/PaidConsultationsBodyHeaderThankYouPatient.html');

$message2 .= "<BR>" . 'Dear ' . $practitionerName . ',' . "<BR><BR>" . 'Thank you for subscribing to the Wotmed Paid Consulting Program.  An icon has been addd to your Wotmed profile so patients can contact you for paid consultations.' . "<BR><BR>";
$message2 .= "<BR><BR>" . 'Kind Regards,' . "<BR><BR>" . 'The Wotmed Team' . "<BR><BR>";

$message2 .= 'Your sign up details:' . "<BR><BR>";

$message2 .= 'Practitioner Name: ' . $practitionerName . "<BR>" . 'Practitioner PayPal Email Address: ' . $paypalEmailAddress . "<BR><BR>";

$message2 .= 'Consulting Services Telephone: ' . $servicesOfferedTelephone . "<BR>";
$message2 .= 'Consulting Services Skype Voice: ' . $servicesOfferedSkypeVoice . "<BR>";
$message2 .= 'Consulting Services Skype Video: ' . $servicesOfferedSkypeVideo . "<BR>";
$message2 .= 'Consulting Services Email: ' . $servicesOfferedEmail . "<BR>";
$message2 .= 'Consulting Services In Person: ' . $servicesOfferedInPerson . "<BR><BR>";

$message2 .= 'Hourly Rate: $' . $hourlyRate . "<BR>" . 'Skype ID: ' . $skypeID . "<BR>" . 'Consulting Phone Number: ' . $phone . "<BR>";
$message2 .= 'Practice Name: ' . $practiceName . "<BR>";
$message2 .= 'Speciality: ' . $practiceSpeciality . "<BR>";
$message2 .= 'Practice Address: ' . $practiceAddress . "<BR>";

$message2 .= file_get_contents('../../HTMLEmailTemplate/WotmedEmailTemplate/RequestforProposalBodyFooterClient.html');

$message2 .= file_get_contents('../../HTMLEmailTemplate/WotmedEmailTemplate/RequestForProposalFooterClient.html');



if(mail($address2, $subject2, $message2, $headers2)) 
{

}

else 

{
	echo 'ERROR!';
}

echo '<script language="javascript">';
echo 'alert("Thank you for electing to take part in Wotmed Paid Consulting.  An icon has been added to your Wotmed profile.  Wotmed Patients can now click this icon to arrange a date and time to consult with you.  You will receive an automated email from the Wotmed Platform when a patient books a consultation with you as well as an email from PayPal indicating that you have received payment for a Wotmed Consultation.")';
echo '</script>';

echo "<script> location.replace('http://platform.wotmed.com/practitioner_profile.php'); </script>";


