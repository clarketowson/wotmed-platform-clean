<?php


session_start();


if(!$_POST) exit;

// Email address verification, do not edit.
function isEmail($email) {
	return(preg_match("/^[-_.[:alnum:]]+@((([[:alnum:]]|[[:alnum:]][[:alnum:]-]*[[:alnum:]])\.)+(ad|ae|aero|af|ag|ai|al|am|an|ao|aq|ar|arpa|as|at|au|aw|az|ba|bb|bd|be|bf|bg|bh|bi|biz|bj|bm|bn|bo|br|bs|bt|bv|bw|by|bz|ca|cc|cd|cf|cg|ch|ci|ck|cl|cm|cn|co|com|coop|cr|cs|cu|cv|cx|cy|cz|de|dj|dk|dm|do|dz|ec|edu|ee|eg|eh|er|es|et|eu|fi|fj|fk|fm|fo|fr|ga|gb|gd|ge|gf|gh|gi|gl|gm|gn|gov|gp|gq|gr|gs|gt|gu|gw|gy|hk|hm|hn|hr|ht|hu|id|ie|il|in|info|int|io|iq|ir|is|it|jm|jo|jp|ke|kg|kh|ki|km|kn|kp|kr|kw|ky|kz|la|lb|lc|li|lk|lr|ls|lt|lu|lv|ly|ma|mc|md|me|mg|mh|mil|mk|ml|mm|mn|mo|mp|mq|mr|ms|mt|mu|museum|mv|mw|mx|my|mz|na|name|nc|ne|net|nf|ng|ni|nl|no|np|nr|nt|nu|nz|om|org|pa|pe|pf|pg|ph|pk|pl|pm|pn|pr|pro|ps|pt|pw|py|qa|re|ro|ru|rw|sa|sb|sc|sd|se|sg|sh|si|sj|sk|sl|sm|sn|so|sr|st|su|sv|sy|sz|tc|td|tf|tg|th|tj|tk|tm|tn|to|tp|tr|tt|tv|tw|tz|ua|ug|uk|um|us|uy|uz|va|vc|ve|vg|vi|vn|vu|wf|ws|ye|yt|yu|za|zm|zw)$|(([0-9][0-9]?|[0-1][0-9][0-9]|[2][0-4][0-9]|[2][5][0-5])\.){3}([0-9][0-9]?|[0-1][0-9][0-9]|[2][0-4][0-9]|[2][5][0-5]))$/i",$email));
}

if (!defined("PHP_EOL")) define("PHP_EOL", "\r\n");


// store the prospective users information in the Wotmed Database in the PATIENTSTORIES table

// make sure to escape the users post data for security purposes

$conn = new mysqli('liberty.wotmed.com','wotmedco_user','e5~1]qu1|&8)6wJ', 'wotmedco_db');

// $conn = new mysqli('localhost','wotmedco_user','Password1', 'WotmedDBLiveMirror');

// $conn = new mysqli('liberty.wotmed.com','wotmedco_user','e5~1]qu1|&8)6wJ', 'wotmedco_db');

$name     = mysqli_real_escape_string($conn, $_POST['ArticleSubmitterName']);
$phone    = mysqli_real_escape_string($conn, $_POST['ArticleSubmitterPhoneNumber']);
$email    = mysqli_real_escape_string($conn, $_POST['ArticleSubmitterEmailAddress']);
$articleTitle    = mysqli_real_escape_string($conn, $_POST['ArticleTitle']);
$articleSubject = mysqli_real_escape_string($conn, $_POST['ArticleSubject']);
$anonymous = mysqli_real_escape_string($conn, $_POST['ArticleSubmitterAnonymous']);
$articlePenName = mysqli_real_escape_string($conn, $_POST['ArticlePenName']);
$articleSEOKeywords = mysqli_real_escape_string($conn, $_POST['ArticleSEOKeywords']);
$articleText = mysqli_real_escape_string($conn, $_POST['ArticleText']);
$articleCountry  = mysqli_real_escape_string($conn, $_POST['ArticleSubmitterCountry']);
$isAuthorContactable = mysqli_real_escape_string($conn, $_POST['IsAuthorContactable']);

$practiceName = mysqli_real_escape_string($conn, $_POST['PracticeName']);
$practiceSpeciality = mysqli_real_escape_string($conn, $_POST['PracticeSpeciality']);
$practicePhone = mysqli_real_escape_string($conn, $_POST['PracticePhone']);
$practiceAddress = mysqli_real_escape_string($conn, $_POST['PracticeAddress']);

$practiceIndependentWebsiteHyperlink = mysqli_real_escape_string($conn, $_POST['PracticeIndependentWebsiteHyperlink']);
$practiceMarketingBlurb = mysqli_real_escape_string($conn, $_POST['PracticeMarketingBlurb']);
$practiceMarketingImage = mysqli_real_escape_string($conn, $_POST['PracticeMarketingImage']);


$practiceLogo = mysqli_real_escape_string($conn, $_POST['fileToUpload']);


$randomNumber = mt_rand(0, 100000);

// $storyHyperlink = date("Y-m-d") . $randomNumber . '.php';

$articleHyperlink = date("Y-m-d");



if(trim($name) == '') 
{
	echo '<div class="error_message">You must enter your name (we wont share this if you dont want us to).</div>';
	exit();
} 

else if(trim($email) == '') 
{
	echo '<div class="error_message">Please enter a valid email address (we wont share this if you dont want us to).</div>';
	exit();
	
}

else if(trim($phone) == '') 
{
	echo '<div class="error_message">Please enter a valid phone number (we wont share this).</div>';
	exit();
	
}

else if(!isEmail($email)) 
{
   echo '<div class="error_message">You have entered an invalid e-mail address, please try again (we wont share this if you dont want us to).</div>';
   exit();
}


if(get_magic_quotes_gpc()) 
{
    $articleText = stripslashes($articleText);
}

if ($conn->connect_error) 
{
header('Location: /DatabaseError.shtml');
}

// store the practitioners article details

$result = $conn->query("INSERT INTO PRACTITIONERARTICLES(ARTICLESUBMISSIONDATE, ARTICLESUBMITTERNAME, ARTICLESUBMITTERCOUNTRY, ARTICLESUBMITTERPHONENUMBER, ARTICLESUBMITTEREMAILADDRESS, ARTICLETITLE, ARTICLEHYPERLINK, ARTICLESUBJECT, ARTICLETEXT, ARTICLESEOKEYWORDS, ARTICLESUBMITTERANONYMOUS, ARTICLEPENNAME, PRACTICESPECIALITY, PRACTICEPHONE, PRACTICEADDRESS, PRACTICELOGO, PRACTICEINDEPENDENTWEBSITEHYPERLINK, PRACTICEMARKETINGBLURB, PRACTICEMARKETINGBLURBIMAGE, ISAUTHORCONTACTABLE, PRACTICENAME) VALUES (NOW(), '$name', '$articleCountry', '$phone', '$email', '$articleTitle', '$articleHyperlink', '$articleSubject', '$articleText', '$articleSEOKeywords', '$anonymous', '$articlePenName', '$practiceSpeciality', '$practicePhone', '$practiceAddress', '$practiceLogo', '$practiceIndependentWebsiteHyperlink', '$practiceMarketingBlurb', '$practiceMarketingImage', '$isAuthorContactable', '$practiceName')");

$conn->close();


// Configuration option.
// Enter the email address that you want to emails to be sent to.
// Example $address = "joe.doe@yourdomain.com";

//$address = "example@themeforest.net";

$address = "practitioneremailsubmissions@wotmed.com";

// $address = "patientstorysubmissions@wotmed.com";

// Configuration option.
// i.e. The standard subject will appear as, "You've been contacted by John Doe."

// Example, $e_subject = '$name . ' has contacted you via Your Website.';

//$e_subject = 'You\'ve been contacted by ' . $name . '.';


// Configuration option.
// You can change this if you feel that you need to.
// Developers, you may wish to add more fields to the form, in which case you must be sure to add them here.

$e_body = "$name has submitted a Practitioner Article";
$e_content = "\"$comments\"" . PHP_EOL . PHP_EOL;
$e_reply = "You can contact $name via email, $email";

$msg = wordwrap( $e_body . $e_content . $e_reply, 70 );


$headers = 'From: practitioneremailsubmissions@wotmed.com' . "\r\n" .
    'Reply-To: practitioneremailsubmissions@wotmed.com' . "\r\n" .
    'X-Mailer: PHP/' . phpversion();
$headers .= "MIME-Version: 1.0\r\n";
$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";


$subject = 'A practitioner: ' . $name . ' with email address: ' . $email . ' has just submitted a Practitioner Article';

$message = file_get_contents('../../HTMLEmailTemplate/WotmedEmailTemplate/RequestforProposalHeader.html');
$message .= file_get_contents('../../HTMLEmailTemplate/WotmedEmailTemplate/PractitionerArticleBodyHeader.html');


$message .= 'Name: ' . $name . "<BR>" . 'Email: ' . $email . "<BR>" . 'Phone: ' . $phone . "<BR>" . 'Practitioner Country: ' . $articleCountry . "<BR>" . 'Article Title: ' . $articleTitle  . "<BR>";
$message .= 'Article Subject: ' . $articleSubject . "<BR>" . 'Requested Anonymity? ' . $anonymous . "<BR>" . 'Is Author Contactable? ' . $isAuthorContactable . "<BR>" . 'Pen Name: ' . $articlePenName . "<BR>" . 'Story SEO Keywords: ' . $articleSEOKeywords  . "<BR>";
$message .= 'Article: ' . $articleText . "<BR>";
$message .= 'Practice Name: ' . $practiceName . "<BR>";
$message .= 'Practice Speciality: ' . $practiceSpeciality . "<BR>";
$message .= 'Practice Phone: ' . $practicePhone . "<BR>";
$message .= 'Practice Address: ' . $practiceAddress . "<BR>";
$message .= 'Practice Website: ' . $practiceIndependentWebsiteHyperlink . "<BR>";
$message .= 'Practice Marketing Blurb: ' . $practiceMarketingBlurb . "<BR>";

$message .= 'Publish article under this hyperlink: ' . $articleHyperlink . "<BR>";


$message .= file_get_contents('../../HTMLEmailTemplate/WotmedEmailTemplate/RequestforProposalBodyFooter.html');

$message .= file_get_contents('../../HTMLEmailTemplate/WotmedEmailTemplate/RequestForProposalFooter.html');


if(mail($address, $subject, $message, $headers)) 
{

	include ("reg-practitionerArticleSuccess.php");

}


// send the practitioner themselves an email thanking them for submitting an article

$address2 = $email;	// set the email address to the practitioners email now

// $address2 = "practitionerarticlesubmissions@wotmed.com";

//	$address = "clarketowson@wotmed.com";

$headers2 = 'From: practitioneremailsubmissions@wotmed.com' . "\r\n" .
    'Reply-To: practitioneremailsubmissions@wotmed.com' . "\r\n" .
    'X-Mailer: PHP/' . phpversion();
$headers2 .= "MIME-Version: 1.0\r\n";
$headers2 .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

$subject2 = 'Wotmed - Thank you for submitting your practitioner story ' . $name;

$message2 = file_get_contents('../../HTMLEmailTemplate/WotmedEmailTemplate/RequestforProposalHeaderClient.html');
$message2 .= file_get_contents('../../HTMLEmailTemplate/WotmedEmailTemplate/PractitionerArticleBodyHeaderThankYou.html');

$message2 .= "<BR>" . 'Dear ' . $name . ',' . "<BR><BR>" . 'Thank you for submitting your practitioner article.  A Wotmed representative will contact you shortly prior to publication.' . "<BR><BR>";
$message2 .= 'If you have not already done so - Please sign up for a Wotmed Practitioner account now by going here: http://platform.wotmed.com/reg-practitioner.php';
$message2 .= ' so that you can connect with Wotmed Practitioners globally';
$message2 .= "<BR><BR>" . 'Kind Regards,' . "<BR><BR>" . 'The Wotmed Team' . "<BR><BR>";

$message2 .= 'Your practitioner article details:' . "<BR><BR>";

$message2 .= 'Name: ' . $name . "<BR>" . 'Email: ' . $email . "<BR>" . 'Phone: ' . $phone . "<BR>" . 'Practitioner Country: ' . $articleCountry . "<BR>" . 'Article Title: ' . $articleTitle  . "<BR>";
$message2 .= 'Article Subject: ' . $articleSubject . "<BR>" . 'Requested Anonymity? ' . $anonymous . "<BR>" . 'Is Author Contactable? ' . $isAuthorContactable . "<BR>" . 'Pen Name: ' . $articlePenName . "<BR>" . 'Story SEO Keywords: ' . $articleSEOKeywords  . "<BR>";
$message2 .= 'Article: ' . $articleText . "<BR>";
$message2 .= 'Practice Name: ' . $practiceName . "<BR>";
$message2 .= 'Practice Speciality: ' . $practiceSpeciality . "<BR>";
$message2 .= 'Practice Phone: ' . $practicePhone . "<BR>";
$message2 .= 'Practice Address: ' . $practiceAddress . "<BR>";
$message2 .= 'Practice Website: ' . $practiceIndependentWebsiteHyperlink . "<BR>";
$message2 .= 'Practice Marketing Blurb: ' . $practiceMarketingBlurb . "<BR>";

$message2 .= file_get_contents('../../HTMLEmailTemplate/WotmedEmailTemplate/RequestforProposalBodyFooterClient.html');

$message2 .= file_get_contents('../../HTMLEmailTemplate/WotmedEmailTemplate/RequestForProposalFooterClient.html');

if(mail($address2, $subject2, $message2, $headers2)) 
{

	// Email has sent successfully, echo a success page.

//	echo "<fieldset>";
//	echo "<div id='success_page'>";
//	echo "<h1>Your request for proposal has been submitted successfully</h1>";
//	echo "<p>Thank you <strong>$name</strong>, you have successfully submitted your request for proposal.  A Wotmed team member will be in touch with you shortly</p>";
//	echo "</div>";
//	echo "</fieldset>";
	
// Update the Wotmed database and add the prospective customers details to the PROSPECTIVECUSTOMER TABLE

//$query="INSERT INTO PROSPECTIVECUSTOMER ('NAME', 'EMAILADDRESS', 'PHONENUMBER', 'QUESTIONS', 'PROCEDUREWANTED', 'COUNTRYPREFERENCE', 'EMAILADDRESS', 'BUDGET', //'FINANCEREQUIRED', 'PROPOSEDDATE') VALUES (  '$name', '$email', '$phone', '$questions', '$procedure', '$countries', '$budget', '$finance', '$date');";

//$query="INSERT INTO PROSPECTIVECUSTOMER ('NAME') VALUES ( '$name')";
		
//		mysqli_query($conn,$query);
	

// now email the client a confirmation email that Wotmed has received their request for proposal
	
// include ('contactClient.php');		

}


else 

{

	echo 'ERROR!';

}




