<?php


session_start();


if(!$_POST) exit;

// Email address verification, do not edit.
function isEmail($email) {
	return(preg_match("/^[-_.[:alnum:]]+@((([[:alnum:]]|[[:alnum:]][[:alnum:]-]*[[:alnum:]])\.)+(ad|ae|aero|af|ag|ai|al|am|an|ao|aq|ar|arpa|as|at|au|aw|az|ba|bb|bd|be|bf|bg|bh|bi|biz|bj|bm|bn|bo|br|bs|bt|bv|bw|by|bz|ca|cc|cd|cf|cg|ch|ci|ck|cl|cm|cn|co|com|coop|cr|cs|cu|cv|cx|cy|cz|de|dj|dk|dm|do|dz|ec|edu|ee|eg|eh|er|es|et|eu|fi|fj|fk|fm|fo|fr|ga|gb|gd|ge|gf|gh|gi|gl|gm|gn|gov|gp|gq|gr|gs|gt|gu|gw|gy|hk|hm|hn|hr|ht|hu|id|ie|il|in|info|int|io|iq|ir|is|it|jm|jo|jp|ke|kg|kh|ki|km|kn|kp|kr|kw|ky|kz|la|lb|lc|li|lk|lr|ls|lt|lu|lv|ly|ma|mc|md|me|mg|mh|mil|mk|ml|mm|mn|mo|mp|mq|mr|ms|mt|mu|museum|mv|mw|mx|my|mz|na|name|nc|ne|net|nf|ng|ni|nl|no|np|nr|nt|nu|nz|om|org|pa|pe|pf|pg|ph|pk|pl|pm|pn|pr|pro|ps|pt|pw|py|qa|re|ro|ru|rw|sa|sb|sc|sd|se|sg|sh|si|sj|sk|sl|sm|sn|so|sr|st|su|sv|sy|sz|tc|td|tf|tg|th|tj|tk|tm|tn|to|tp|tr|tt|tv|tw|tz|ua|ug|uk|um|us|uy|uz|va|vc|ve|vg|vi|vn|vu|wf|ws|ye|yt|yu|za|zm|zw)$|(([0-9][0-9]?|[0-1][0-9][0-9]|[2][0-4][0-9]|[2][5][0-5])\.){3}([0-9][0-9]?|[0-1][0-9][0-9]|[2][0-4][0-9]|[2][5][0-5]))$/i",$email));
}

if (!defined("PHP_EOL")) define("PHP_EOL", "\r\n");


// store the prospective users information in the Wotmed Database in the PATIENTSTORIES table

// make sure to escape the users post data for security purposes

// $conn = new mysqli('liberty.wotmed.com','wotmedco_user','e5~1]qu1|&8)6wJ', 'wotmedco_db');

// $conn = new mysqli('localhost','wotmedco_user','Password1', 'WotmedDBLiveMirror');

$conn = new mysqli('liberty.wotmed.com','wotmedco_user','e5~1]qu1|&8)6wJ', 'wotmedco_db');

$name     = mysqli_real_escape_string($conn, $_POST['StorySubmitterName']);
$phone    = mysqli_real_escape_string($conn, $_POST['StorySubmitterPhoneNumber']);
$email    = mysqli_real_escape_string($conn, $_POST['StorySubmitterEmailAddress']);
$storyTitle    = mysqli_real_escape_string($conn, $_POST['StoryTitle']);
$storySubject = mysqli_real_escape_string($conn, $_POST['StorySubject']);
$anonymous = mysqli_real_escape_string($conn, $_POST['StorySubmitterAnonymous']);
$storyPenName = mysqli_real_escape_string($conn, $_POST['StoryPenName']);
$storySEOKeywords = mysqli_real_escape_string($conn, $_POST['StorySEOKeywords']);
$storyText = mysqli_real_escape_string($conn, $_POST['StoryText']);
$procedure    = mysqli_real_escape_string($conn, $_POST['SurgeryName']);
$countries    = mysqli_real_escape_string($conn, $_POST['CountrySurgeryPerformed']);
$date    = mysqli_real_escape_string($conn, $_POST['datepicker']);
$isAuthorContactable = mysqli_real_escape_string($conn, $_POST['IsAuthorContactable']);

$randomNumber = mt_rand(0, 100000);

// $storyHyperlink = date("Y-m-d") . $randomNumber . '.php';

$storyHyperlink = date("Y-m-d");



if(trim($name) == '') 
{
	echo '<div class="error_message">You must enter your name (we wont share this if you dont want us to).</div>';
	exit();
} 

else if(trim($email) == '') 
{
	echo '<div class="error_message">Please enter a valid email address (we wont share this if you dont want us to).</div>';
	exit();
	
}

else if(trim($phone) == '') 
{
	echo '<div class="error_message">Please enter a valid phone number (we wont share this).</div>';
	exit();
	
}

else if(!isEmail($email)) 
{
   echo '<div class="error_message">You have entered an invalid e-mail address, please try again (we wont share this if you dont want us to).</div>';
   exit();
}


if(get_magic_quotes_gpc()) 
{
    $storyText = stripslashes($storyText);
}

if ($conn->connect_error) 
{
header('Location: /DatabaseError.shtml');
}

// store the users data
$result = $conn->query("INSERT INTO PATIENTSTORIES(STORYSUBMISSIONDATETIME, STORYSUBMITTERNAME, STORYSUBMITTERPHONENUMBER, STORYSUBMITTEREMAILADDRESS, STORYTITLE, STORYHYPERLINK, STORYSUBJECT, STORYTEXT, STORYSEOKEYWORDS, STORYSUBMITTERANONYMOUS, STORYPENNAME, SURGERYNAME, COUNTRYSURGERYPERFORMED, DATEOFSURGERY, EMAILSENTFLAG, ISAUTHORCONTACTABLE) VALUES (NOW(),'$name', '$phone', '$email', '$storyTitle', '$storyHyperlink', '$storySubject', '$storyText', '$storySEOKeywords', '$anonymous', '$storyPenName', '$procedure', '$countries', '$date', '0', '$isAuthorContactable')");

$conn->close();


// Configuration option.
// Enter the email address that you want to emails to be sent to.
// Example $address = "joe.doe@yourdomain.com";

//$address = "example@themeforest.net";

$address = "patientstorysubmissions@wotmed.com";


// Configuration option.
// i.e. The standard subject will appear as, "You've been contacted by John Doe."

// Example, $e_subject = '$name . ' has contacted you via Your Website.';

//$e_subject = 'You\'ve been contacted by ' . $name . '.';


// Configuration option.
// You can change this if you feel that you need to.
// Developers, you may wish to add more fields to the form, in which case you must be sure to add them here.

$e_body = "$name has submitted a patient story";
$e_content = "\"$comments\"" . PHP_EOL . PHP_EOL;
$e_reply = "You can contact $name via email, $email";

$msg = wordwrap( $e_body . $e_content . $e_reply, 70 );


$headers = 'From: patientstorysubmissions@wotmed.com' . "\r\n" .
    'Reply-To: patientstorysubmissions@wotmed.com' . "\r\n" .
    'X-Mailer: PHP/' . phpversion();
$headers .= "MIME-Version: 1.0\r\n";
$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";


$subject = 'A patient: ' . $name . ' with email address: ' . $email . ' has just submitted a Patient Story';

$message = file_get_contents('../../HTMLEmailTemplate/WotmedEmailTemplate/RequestforProposalHeader.html');
$message .= file_get_contents('../../HTMLEmailTemplate/WotmedEmailTemplate/PatientStoryBodyHeader.html');


$message .= 'Name: ' . $name . "<BR>" . 'Email: ' . $email . "<BR>" . 'Phone: ' . $phone . "<BR>" . 'Story Title: ' . $storyTitle  . "<BR>";
$message .= 'Story Subject: ' . $storySubject . "<BR>" . 'Requested Anonymity? ' . $anonymous . "<BR>" . 'Is Author Contactable? ' . $isAuthorContactable . "<BR>" . 'Pen Name: ' . $storyPenName . "<BR>" . 'Story SEO Keywords: ' . $storySEOKeywords  . "<BR>";
$message .= 'Story: ' . $storyText . "<BR>" . 'Surgery Undergone: ' . $procedure  . "<BR>" . 'Country Surgery Performed In: ' . $countries . "<BR>" . 'Date of Surgery: ' . $date  . "<BR>";
$message .= 'Publish story under this hyperlink: ' . $storyHyperlink . "<BR>";


$message .= file_get_contents('../../HTMLEmailTemplate/WotmedEmailTemplate/RequestforProposalBodyFooter.html');

$message .= file_get_contents('../../HTMLEmailTemplate/WotmedEmailTemplate/RequestForProposalFooter.html');


if(mail($address, $subject, $message, $headers)) 
{

	include ("reg-participantStorySuccess.php");

}


//if(mail($address, $subject, $message, $headers)) 
//{

	// Email has sent successfully, echo a success page.

//	echo "<fieldset>";
//	echo "<div id='success_page'>";
//	echo "<h1>Your request for proposal has been submitted successfully</h1>";
//	echo "<p>Thank you <strong>$name</strong>, you have successfully submitted your request for proposal.  A Wotmed team member will be in touch with you shortly</p>";
//	echo "</div>";
//	echo "</fieldset>";
	
	// redirect patient to the front page of the Wotmed site
	
//	header( "refresh:5;url=http://www.wotmed.com" );	

//}




// send the patient themselves an email thanking them for submitting a story

	$address2 = $email;	// set the email address to the patients email now
	
//	$address = "clarketowson@wotmed.com";

$headers2 = 'From: patientstorysubmissions@wotmed.com' . "\r\n" .
    'Reply-To: patientstorysubmissions@wotmed.com' . "\r\n" .
    'X-Mailer: PHP/' . phpversion();
$headers2 .= "MIME-Version: 1.0\r\n";
$headers2 .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

$subject2 = 'Wotmed - Thank you for submitting your patient story ' . $name;

$message2 = file_get_contents('../../HTMLEmailTemplate/WotmedEmailTemplate/RequestforProposalHeaderClient.html');
$message2 .= file_get_contents('../../HTMLEmailTemplate/WotmedEmailTemplate/PatientStoryBodyHeaderThankYou.html');

$message2 .= "<BR>" . 'Dear ' . $name . ',' . "<BR><BR>" . 'Thank you for submitting your patient story.  A Wotmed representative will contact you shortly prior to publication.' . "<BR><BR>";
$message2 .= 'If you have not already done so - Please sign up for a Wotmed account now by going here: http://platform.wotmed.com/reg-participant.php';
$message2 .= ' so that you can connect with Wotmed Practitioners globally';
$message2 .= "<BR><BR>" . 'Kind Regards,' . "<BR><BR>" . 'The Wotmed Team' . "<BR><BR>";

$message2 .= 'Your patient story details:' . "<BR><BR>";

$message2 .= 'Name: ' . $name . "<BR>" . 'Email: ' . $email . "<BR>" . 'Phone: ' . $phone . "<BR>" . 'Story Title: ' . $storyTitle  . "<BR>";
$message2 .= 'Story Subject: ' . $storySubject . "<BR>" . 'Requested Anonymity? ' . $anonymous . "<BR>" . 'Is Author Contactable? ' . $isAuthorContactable . "<BR>" . 'Pen Name: ' . $storyPenName . "<BR>" . 'Story SEO Keywords: ' . $storySEOKeywords  . "<BR>";
$message2 .= 'Story: ' . $storyText . "<BR>" . 'Surgery Undergone: ' . $procedure  . "<BR>" . 'Country Surgery Performed In: ' . $countries . "<BR>" . 'Date of Surgery: ' . $date  . "<BR>";

$message2 .= file_get_contents('../../HTMLEmailTemplate/WotmedEmailTemplate/RequestforProposalBodyFooterClient.html');

$message2 .= file_get_contents('../../HTMLEmailTemplate/WotmedEmailTemplate/RequestForProposalFooterClient.html');

if(mail($address2, $subject2, $message2, $headers2)) 
{

	// Email has sent successfully, echo a success page.

//	echo "<fieldset>";
//	echo "<div id='success_page'>";
//	echo "<h1>Your request for proposal has been submitted successfully</h1>";
//	echo "<p>Thank you <strong>$name</strong>, you have successfully submitted your request for proposal.  A Wotmed team member will be in touch with you shortly</p>";
//	echo "</div>";
//	echo "</fieldset>";
	
// Update the Wotmed database and add the prospective customers details to the PROSPECTIVECUSTOMER TABLE

//$query="INSERT INTO PROSPECTIVECUSTOMER ('NAME', 'EMAILADDRESS', 'PHONENUMBER', 'QUESTIONS', 'PROCEDUREWANTED', 'COUNTRYPREFERENCE', 'EMAILADDRESS', 'BUDGET', //'FINANCEREQUIRED', 'PROPOSEDDATE') VALUES (  '$name', '$email', '$phone', '$questions', '$procedure', '$countries', '$budget', '$finance', '$date');";

//$query="INSERT INTO PROSPECTIVECUSTOMER ('NAME') VALUES ( '$name')";
		
//		mysqli_query($conn,$query);
	

// now email the client a confirmation email that Wotmed has received their request for proposal
	
// include ('contactClient.php');		

}


else 

{

	echo 'ERROR!';

}


