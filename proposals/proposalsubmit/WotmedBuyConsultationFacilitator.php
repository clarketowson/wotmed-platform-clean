<?php

ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(-1);

include('../../thumbgenerator/image.class.php');  // class for the image thumbnail generator


session_start();

ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(-1);


//$participantNumber = $_SESSION['id'];
//$practitionerParticipantNumber = $_SESSION['PractitionerNumber'];

$participantNumber = $_SESSION['id'];
$patientNumber = $_SESSION['id'];

$practitionerParticipantNumber = $_SESSION['SurgeryFacilitatorNumber'];

//echo $participantNumber;                    // 2132 Clarke Towson
//echo "<br>";
//echo $practitionerParticipantNumber;        // 2267 Practitioner James Smith

//if(!$_POST) exit;

if (!defined("PHP_EOL")) define("PHP_EOL", "\r\n");


// store the prospective users information in the Wotmed Database in the PATIENTSTORIES table

// make sure to escape the users post data for security purposes

$conn = new mysqli('liberty.wotmed.com','wotmedco_user','e5~1]qu1|&8)6wJ', 'wotmedco_db');

// $conn = new mysqli('localhost','wotmedco_user','Password1', 'WotmedDBLiveMirror');

// $conn = new mysqli('liberty.wotmed.com','wotmedco_user','e5~1]qu1|&8)6wJ', 'wotmedco_db');


// get the practitioners number

$query ="SELECT PRACTITIONER_NUMBER FROM PRACTITIONER WHERE PARTICIPANT_NUMBER = $practitionerParticipantNumber";
$result = $conn->query($query);
while($row = $result->fetch_assoc())
{
    $practitionerNumber = $row["PRACTITIONER_NUMBER"];

  //  echo $practitionerNumber;
}

// get the practitioners Skype details

$query ="SELECT PARTICIPANT_NUMBER, SKYPEUSERID FROM WOTMEDPAIDCONSULTING WHERE PARTICIPANT_NUMBER = 206";
$result = $conn->query($query);
while($row = $result->fetch_assoc())
{

    $skypeUserID = $row["SKYPEUSERID"];

    //  echo $practitionerNumber;
}


// get the practitioners details

$query="SELECT PARTICIPANT_NUMBER, PRACTITIONER_BUSINESSNAME, PRACTITIONER_BUSINESSNUMBER, PRACTITIONER_BUSINESSADDRESS, PRACTITIONER_BUSINESSSUBURB, PRACTITIONER_BUSINESSPOSTCODE, PRACTITIONER_BUSINESSCITY, PRACTITIONER_BUSINESSSTATE, PRACTITIONER_BUSINESSLOGO FROM PRACTITIONER WHERE PRACTITIONER_NUMBER = $practitionerNumber";
$result = $conn->query($query);

while($row = $result->fetch_assoc())
{
    $participantNumber = $row["PARTICIPANT_NUMBER"];
    $practitionerBusinessName = $row["PRACTITIONER_BUSINESSNAME"];
    $practitionerBusinessNumber = $row["PRACTITIONER_BUSINESSNUMBER"];
    $practitionerBusinessAddress = $row["PRACTITIONER_BUSINESSADDRESS"];
    $practitionerBusinessSuburb = $row["PRACTITIONER_BUSINESSSUBURB"];
    $practitionerBusinessCity = $row["PRACTITIONER_BUSINESSCITY"];
    $practitionerBusinessState = $row["PRACTITIONER_BUSINESSSTATE"];
    $practitionerBusinessPostCode = $row["PRACTITIONER_BUSINESSPOSTCODE"];
    $practitionerBusinessLogo = $row["PRACTITIONER_BUSINESSLOGO"];



}


$query="SELECT PARTICIPANT_NUMBER, FIRSTNAME, SURNAME, EMAILADDRESS, PROFILEPHOTO FROM PARTICIPANT WHERE PARTICIPANT_NUMBER = $participantNumber";
$result = $conn->query($query);

while($row = $result->fetch_assoc())
{
    $practitionerEmailAddress = $row["EMAILADDRESS"];
    $practitionerProfilePhoto = $row["PROFILEPHOTO"];
    $practitionerFirstName = $row["FIRSTNAME"];
    $practitionerSurName = $row["SURNAME"];

}







//$participantNumber = $_SESSION['id'];
//$practitionerParticipantNumber = $_SESSION['PractitionerNumber'];


$patientFirstName = mysqli_real_escape_string($conn, $_POST['PatientFirstName']);
$patientSurname = mysqli_real_escape_string($conn, $_POST['PatientSurName']);

$patientName = $patientFirstName . ' ' . $patientSurname;


$wotmedCommission = "TBA";

$hourlyRate = mysqli_real_escape_string($conn, $_POST['PractitionersHourlyRate']);
$consultingHoursPurchased = mysqli_real_escape_string($conn, $_POST['HoursPurchased']);

$hourlyRateInt = (int)$hourlyRate;
$consultingHoursPurchasedInt = (int)$consultingHoursPurchased;


$totalPrice = $hourlyRateInt * $consultingHoursPurchasedInt;

$_SESSION['TOTALPRICE'] = $totalPrice;

$wotmedCommission = $totalPrice * 0.10;

$requestedBookingDate = mysqli_real_escape_string($conn, $_POST['datepicker']);
$requestedBookingTime = mysqli_real_escape_string($conn, $_POST['AppointmentTime']);
$practitionerName     = mysqli_real_escape_string($conn, $_POST['YourPractitionerName']);

// $servicesOfferedTelephone = mysqli_real_escape_string($conn, $_POST['ConsultingServicesOfferedTelephone']);

if(isset($_POST['ConsultingServicesOfferedTelephone']))
{
    $servicesOfferedTelephone = mysqli_real_escape_string($conn, $_POST['ConsultingServicesOfferedTelephone']);
}
else
{
    $servicesOfferedTelephone = "Not Selected";
}

if(isset($_POST['ConsultingServicesOfferedSkypeVoice']))
{
    $servicesOfferedSkypeVoice = mysqli_real_escape_string($conn, $_POST['ConsultingServicesOfferedSkypeVoice']);
}
else
{
    $servicesOfferedSkypeVoice = "Not Selected";
}

if(isset($_POST['ConsultingServicesOfferedSkypeVideo']))
{
    $servicesOfferedSkypeVideo = mysqli_real_escape_string($conn, $_POST['ConsultingServicesOfferedSkypeVideo']);
}
else
{
    $servicesOfferedSkypeVideo = "Not Selected";
}

if(isset($_POST['ConsultingServicesOfferedEmail']))
{
    $servicesOfferedEmail = mysqli_real_escape_string($conn, $_POST['ConsultingServicesOfferedEmail']);
}
else
{
    $servicesOfferedEmail  = "Not Selected";
}

if(isset($_POST['ConsultingServicesOfferedInPerson']))
{
    $servicesOfferedInPerson = mysqli_real_escape_string($conn, $_POST['ConsultingServicesOfferedInPerson']);

}
else
{
    $servicesOfferedInPerson = "Not Selected";
}




$phone    = mysqli_real_escape_string($conn, $_POST['PatientPhoneNumberConsulting']);
$paypalEmailAddress = mysqli_real_escape_string($conn, $_POST['PatientPayPalEmailAddress']);
$paypalEmailAddressPatient = $paypalEmailAddress;

$skypeID = mysqli_real_escape_string($conn, $_POST['PractitionerSkypeUserID']);

//$practiceName = mysqli_real_escape_string($conn, $_POST['PracticeOrBusinessName']);
//$practiceSpeciality = mysqli_real_escape_string($conn, $_POST['PracticeOrBusinessSpeciality']);
//$practiceAddress = mysqli_real_escape_string($conn, $_POST['PracticeOrBusinessAddress']);


if ($conn->connect_error) 
{
header('Location: /DatabaseError.shtml');
}

// record the booking

//$result = $conn->query("INSERT INTO WOTMEDCONSULTATIONSBOOKED(PARTICIPANT_NUMBER, PRACTITIONER_NUMBER, PATIENTNAME, PRACTITIONERNAME, PRACTITIONERCONSULTATIONHOURLYRATE, CONSULTINGHOURSPURCHASED, TOTALPATIENTSPEND, APPOINTMENTDATE, APPOINTMENTTIME, PATIENTPHONENUMBER, PATIENTSKYPEID, PATIENTPAYPALEMAILADDRESS, BOOKINGDATE) VALUES ()

$result = $conn->query("INSERT INTO WOTMEDPAIDCONSULTATIONSBOOKED(PARTICIPANT_NUMBER, PRACTITIONER_NUMBER, PATIENTNAME, PRACTITIONERNAME, PRACTITIONERCONSULTATIONHOURLYRATE, CONSULTINGHOURSPURCHASED, TOTALPATIENTSPEND, APPOINTMENTDATE, APPOINTMENTTIME, PATIENTPHONENUMBER, PATIENTSKYPEID, PATIENTPAYPALEMAILADDRESS, BOOKINGDATE, WOTMEDCOMMISSION) VALUES ('$patientNumber', '$practitionerParticipantNumber', '$patientName', '$practitionerName', '$hourlyRateInt', '$consultingHoursPurchased', '$totalPrice', '$requestedBookingDate', '$requestedBookingTime', '$phone', '$skypeID', '$paypalEmailAddress', NOW(), '$wotmedCommission')");

$conn->close();




$advertisementThumb = "Thumb" . $practitionerBusinessLogo;

$practitionerProfileThumb = "Thumb" . $practitionerProfilePhoto;

$paidConsultationsGraphic = "WotmedPaidConsultationsGraphicMediumFacilitator.jpg";

$paidConsultationsGraphicPractitioner = "WotmedPaidConsultationsGraphicMediumFacilitator.jpg";

$bannerCodeGraphic = "
<!-- 3 columns -->
<table width='100%' bgcolor='#f7f7f7' cellpadding='0' cellspacing='0' border='0' id='backgroundTable'>
    <tbody>
    <tr>
        <td>
            <table width='600' cellpadding='0' cellspacing='0' border='0' align='center' class='devicewidth'>
                <tbody>
                <tr>
                    <td width='100%'>
                        <table width='600' cellpadding='0' cellspacing='0' border='0' align='center' class='devicewidth'>
                            <tbody>
                            <tr>
                                <td>
                                        <tr>
                                            <td width='186' height='250' align='center' class='devicewidth'>
                                                <img src='http://platform.wotmed.com/ImageUpload/uploads/consultations/$paidConsultationsGraphic' alt='' border='0' style='display:block; border:none; outline:none; text-decoration:none;'>
                                            </td>
                                        </tr>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>
<!-- end of 3 columns -->

";

$bannerCodeGraphicPractitioner = "
<!-- 3 columns -->
<table width='100%' bgcolor='#f7f7f7' cellpadding='0' cellspacing='0' border='0' id='backgroundTable'>
    <tbody>
    <tr>
        <td>
            <table width='600' cellpadding='0' cellspacing='0' border='0' align='center' class='devicewidth'>
                <tbody>
                <tr>
                    <td width='100%'>
                        <table width='600' cellpadding='0' cellspacing='0' border='0' align='center' class='devicewidth'>
                            <tbody>
                            <tr>
                                <td>
                                        <tr>
                                            <td width='186' height='250' align='center' class='devicewidth'>
                                                <img src='http://platform.wotmed.com/ImageUpload/uploads/consultations/$paidConsultationsGraphicPractitioner' alt='' border='0' style='display:block; border:none; outline:none; text-decoration:none;'>
                                            </td>
                                        </tr>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>
<!-- end of 3 columns -->

";


// **************GENERATE THUMBNAIL HERE *******************

###############################################################
# Thumbnail Image Generator 1.3
###############################################################
# Visit http://www.zubrag.com/scripts/ for updates
###############################################################

// REQUIREMENTS:
// PHP 4.0.6 and GD 2.0.1 or later
// May not work with GIFs if GD2 library installed on your server
// does not support GIF functions in full

// Parameters:
// src - path to source image
// dest - path to thumb (where to save it)
// x - max width
// y - max height
// q - quality (applicable only to JPG, 1 to 100, 100 - best)
// t - thumb type. "-1" - same as source, 1 = GIF, 2 = JPG, 3 = PNG
// f - save to file (1) or output to browser (0).

// Sample usage:
// 1. save thumb on server
// http://www.zubrag.com/thumb.php?src=test.jpg&dest=thumb.jpg&x=100&y=50
// 2. output thumb to browser
// http://www.zubrag.com/thumb.php?src=test.jpg&x=50&y=50&f=0


// Below are default values (if parameter is not passed)

// save to file (true) or output to browser (false)
$save_to_file = true;

// Quality for JPEG and PNG.
// 0 (worst quality, smaller file) to 100 (best quality, bigger file)
// Note: PNG quality is only supported starting PHP 5.1.2
$image_quality = 100;

// resulting image type (1 = GIF, 2 = JPG, 3 = PNG)
// enter code of the image type if you want override it
// or set it to -1 to determine automatically
$image_type = -1;

// maximum thumb side size
$max_x = 400;
$max_y = 400;

// cut image before resizing. Set to 0 to skip this.
$cut_x = 0;
$cut_y = 0;

// Folder where source images are stored (thumbnails will be generated from these images).
// MUST end with slash.
$images_folder = '../../ImageUpload/uploads/consultations/';

// Folder to save thumbnails, full path from the root folder, MUST end with slash.
// Only needed if you save generated thumbnails on the server.
// Sample for windows:     c:/wwwroot/thumbs/
// Sample for unix/linux:  /home/site.com/htdocs/thumbs/
$thumbs_folder = '../../ImageUpload/uploads/consultations/';


///////////////////////////////////////////////////
/////////////// DO NOT EDIT BELOW
///////////////////////////////////////////////////

$to_name = '';

if (isset($_REQUEST['f'])) {
    $save_to_file = intval($_REQUEST['f']) == 1;
}

if (isset($_REQUEST['q'])) {
    $image_quality = intval($_REQUEST['q']);
}

if (isset($_REQUEST['t'])) {
    $image_type = intval($_REQUEST['t']);
}

if (isset($_REQUEST['x'])) {
    $max_x = intval($_REQUEST['x']);
}

if (isset($_REQUEST['y'])) {
    $max_y = intval($_REQUEST['y']);
}

if (!file_exists($images_folder)) die('Images folder does not exist (update $images_folder in the script)');
if ($save_to_file && !file_exists($thumbs_folder)) die('Thumbnails folder does not exist (update $thumbs_folder in the script)');

// Allocate all necessary memory for the image.
// Special thanks to Alecos for providing the code.
ini_set('memory_limit', '-1');

// include image processing code
// include('image.class.php');

$img = new Zubrag_image;

// initialize
$img->max_x        = $max_x;
$img->max_y        = $max_y;
$img->cut_x        = $cut_x;
$img->cut_y        = $cut_y;
$img->quality      = $image_quality;
$img->save_to_file = $save_to_file;
$img->image_type   = $image_type;

// generate thumbnail
// $img->GenerateThumbFile($images_folder . $from_name, $thumbs_folder . $to_name);

if (!empty($practitionerBusinessLogo))
{
 //   $img->GenerateThumbFile($images_folder . $practitionerBusinessLogo, $thumbs_folder . $advertisementThumb);

    // **************GENERATE THUMBNAIL HERE *******************


//

    $bannerCode = "
<!-- 3 columns -->
<table width='100%' bgcolor='#f7f7f7' cellpadding='0' cellspacing='0' border='0' id='backgroundTable'>
    <tbody>
    <tr>
        <td>
            <table width='600' cellpadding='0' cellspacing='0' border='0' align='center' class='devicewidth'>
                <tbody>
                <tr>
                    <td width='100%'>
                        <table width='600' cellpadding='0' cellspacing='0' border='0' align='center' class='devicewidth'>
                            <tbody>
                            <tr>
                                <td>
                                        <tr>
                                            <td width='186' height='250' align='center' class='devicewidth'>
                                                <img src='http://platform.wotmed.com/photos/originals/$practitionerBusinessLogo' alt='' border='0' style='display:block; border:none; outline:none; text-decoration:none;'>
                                            </td>
                                        </tr>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>
<!-- end of 3 columns -->

";
}

else
{

    $practitionerBusinessLogo = "default_logo.jpg";

    $img->GenerateThumbFile($images_folder . $practitionerBusinessLogo, $thumbs_folder . $advertisementThumb);

    // **************GENERATE THUMBNAIL HERE *******************


    $bannerCode = "
<!-- 3 columns -->
<table width='100%' bgcolor='#f7f7f7' cellpadding='0' cellspacing='0' border='0' id='backgroundTable'>
    <tbody>
    <tr>
        <td>
            <table width='600' cellpadding='0' cellspacing='0' border='0' align='center' class='devicewidth'>
                <tbody>
                <tr>
                    <td width='100%'>
                        <table width='600' cellpadding='0' cellspacing='0' border='0' align='center' class='devicewidth'>
                            <tbody>
                            <tr>
                                <td>
                                        <tr>
                                            <td width='186' height='250' align='center' class='devicewidth'>
                                                <img src='http://platform.wotmed.com/ImageUpload/uploads/consultations/$advertisementThumb' alt='' border='0' style='display:block; border:none; outline:none; text-decoration:none;'>
                                            </td>
                                        </tr>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>
<!-- end of 3 columns -->

";
}





if (!empty($practitionerProfilePhoto))
{
 //   $img->GenerateThumbFile($images_folder . $practitionerProfilePhoto, $thumbs_folder . $practitionerProfileThumb);

    // **************GENERATE THUMBNAIL HERE *******************


//

    $bannerCode2 = "
<!-- 3 columns -->
<table width='100%' bgcolor='#f7f7f7' cellpadding='0' cellspacing='0' border='0' id='backgroundTable'>
    <tbody>
    <tr>
        <td>
            <table width='600' cellpadding='0' cellspacing='0' border='0' align='center' class='devicewidth'>
                <tbody>
                <tr>
                    <td width='100%'>
                        <table width='600' cellpadding='0' cellspacing='0' border='0' align='center' class='devicewidth'>
                            <tbody>
                            <tr>
                                <td>
                                        <tr>
                                            <td width='186' height='250' align='center' class='devicewidth'>
                                                <img src='http://platform.wotmed.com/photos/originals/$practitionerProfilePhoto' alt='' border='0' style='display:block; border:none; outline:none; text-decoration:none;'>
                                            </td>
                                        </tr>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>
<!-- end of 3 columns -->

";
}

else
{

    $practitionerProfilePhoto = "blankSilhouetteMale.png";

//    $img->GenerateThumbFile($images_folder . $practitionerProfilePhoto, $thumbs_folder . $practitionerProfileThumb);

    // **************GENERATE THUMBNAIL HERE *******************


    $bannerCode2 = "
<!-- 3 columns -->
<table width='100%' bgcolor='#f7f7f7' cellpadding='0' cellspacing='0' border='0' id='backgroundTable'>
    <tbody>
    <tr>
        <td>
            <table width='600' cellpadding='0' cellspacing='0' border='0' align='center' class='devicewidth'>
                <tbody>
                <tr>
                    <td width='100%'>
                        <table width='600' cellpadding='0' cellspacing='0' border='0' align='center' class='devicewidth'>
                            <tbody>
                            <tr>
                                <td>
                                        <tr>
                                            <td width='186' height='250' align='center' class='devicewidth'>
                                                <img src='http://platform.wotmed.com/ImageUpload/uploads/consultations/blankSilhouetteMale.png' alt='' border='0' style='display:block; border:none; outline:none; text-decoration:none;'>
                                            </td>
                                        </tr>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>
<!-- end of 3 columns -->

";
}













// Configuration option.
// Enter the email address that you want to emails to be sent to.
// Example $address = "joe.doe@yourdomain.com";

//$address = "example@themeforest.net";

$address = "wotmedconsultations@wotmed.com";

// $address = "patientstorysubmissions@wotmed.com";

// Configuration option.
// i.e. The standard subject will appear as, "You've been contacted by John Doe."

// Example, $e_subject = '$name . ' has contacted you via Your Website.';

//$e_subject = 'You\'ve been contacted by ' . $name . '.';


// Configuration option.
// You can change this if you feel that you need to.
// Developers, you may wish to add more fields to the form, in which case you must be sure to add them here.

$comments = '';

$e_body = "$patientName has has just purchased a Wotmed Paid Consultation from $practitionerName";
$e_content = "\"$comments\"" . PHP_EOL . PHP_EOL;
$e_reply = "You can contact $patientName via email, $paypalEmailAddress";

$msg = wordwrap( $e_body . $e_content . $e_reply, 70 );


$headers = 'From: wotmedconsultations@wotmed.com' . "\r\n" .
    'Reply-To: wotmedconsultations@wotmed.com' . "\r\n" .
    'X-Mailer: PHP/' . phpversion();
$headers .= "MIME-Version: 1.0\r\n";
$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

$subject = 'A patient: ' . $patientFirstName . ' ' . $patientSurname . ' has just purchased a Wotmed Paid Consultation from ' . $practitionerName;

$message = file_get_contents('../../HTMLEmailTemplate/WotmedEmailTemplate/RequestforProposalHeader.html');
$message .= file_get_contents('../../HTMLEmailTemplate/WotmedEmailTemplate/PaidConsultationPurchasedBodyHeaderFacilitator.html');

$message .= $bannerCodeGraphic . "<BR><BR>";

$message .= 'Patient Name: ' . $patientFirstName . ' ' . $patientSurname . "<BR>" . 'Patient PayPal Email Address: ' . $paypalEmailAddressPatient . '<BR><BR>';
$message .= 'Practitioner Name: ' . $practitionerName . "<BR>" . 'Practitioner PayPal Email Address: ' . $practitionerEmailAddress . '<BR><BR>';
$message .= 'Practitioner Skype ID: ' . $skypeUserID . "<BR><BR>";

$message .= 'Patients requested paid consultation details:' . "<BR><BR>";

$message .= 'Requested Booking Date: ' . $requestedBookingDate . "<BR>";
$message .= 'Requested Booking Time: ' . $requestedBookingTime . "<BR><BR>";

$message .= 'Total Consulting Hours Purchased: ' . $consultingHoursPurchasedInt . "<BR>" . 'Total Price: $' . $totalPrice . '<BR><BR>';
$message .= 'Wotmed Commission: $' . $wotmedCommission . "<BR><BR>";

$message .= 'Consulting Services Telephone: ' . $servicesOfferedTelephone . "<BR>";
$message .= 'Consulting Services Skype Voice: ' . $servicesOfferedSkypeVoice . "<BR>";
$message .= 'Consulting Services Skype Video: ' . $servicesOfferedSkypeVideo . "<BR>";
$message .= 'Consulting Services Email: ' . $servicesOfferedEmail . "<BR>";
$message .= 'Consulting Services In Person: ' . $servicesOfferedInPerson . "<BR><BR>";

$message .= 'Hourly Rate: USD$' . $hourlyRateInt . "<BR>" . 'Patients Skype ID: ' . $skypeID . "<BR>" . 'Consulting Phone Number: ' . $phone . "<BR>" ;
$message .= 'Practice Name: ' . $practitionerBusinessName . "<BR>";
$message .= 'Practice Address: ' . $practitionerBusinessAddress . ' ' . $practitionerBusinessSuburb . ' ' . $practitionerBusinessCity . ' ' . $practitionerBusinessState . ' ' . $practitionerBusinessPostCode . "<BR>";

$message .= 'Practitioner Photo: ' . $bannerCode2 . "<BR>";

$message .= 'Practitioner Business Logo: ' . $bannerCode . "<BR>";

$message .= 'Practitioner First Name: ' . $practitionerFirstName . "<BR>";;
$message .= 'Practitioner Surname: ' . $practitionerSurName . "<BR>";;


$message .= file_get_contents('../../HTMLEmailTemplate/WotmedEmailTemplate/RequestforProposalBodyFooterClient.html');

$message .= file_get_contents('../../HTMLEmailTemplate/WotmedEmailTemplate/RequestForProposalFooterClient.html');



if(mail($address, $subject, $message, $headers)) 
{

 //  include("reg-consultingSuccessBuy.php");

  //  header("/proposals/proposalsubmit/reg-consultingSuccess.php");

}



$address2 = $paypalEmailAddress;	// set the email address to the practitioners email now

$headers2 = 'From: wotmedconsultations@wotmed.com' . "\r\n" .
    'Reply-To: wotmedconsultations@wotmed.com' . "\r\n" .
    'X-Mailer: PHP/' . phpversion();
$headers2 .= "MIME-Version: 1.0\r\n";
$headers2 .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

$subject2 = 'Wotmed - Thank you for purchasing a Wotmed Paid Consultation ' . $patientFirstName;

$message2 = file_get_contents('../../HTMLEmailTemplate/WotmedEmailTemplate/RequestforProposalHeaderClient.html');
$message2 .= file_get_contents('../../HTMLEmailTemplate/WotmedEmailTemplate/PaidConsultationsBodyHeaderThankYouPatientBuy.html');

$message2 .= $bannerCodeGraphic . "<BR><BR>";

$message2 .= "<BR>" . 'Dear ' . $patientFirstName . ',' . "<BR><BR>" . 'Thank you for purchasing a consultation from a Wotmed Surgery Facilitator.' . "<BR><BR>";
$message2 .= "Please ensure that you have paid the Surgery Facilitator via PayPal first before initiating first contact" . "<BR>";
$message2 .= "The Surgery Facilitator contact details are below.  You should add the Surgery Facilitator to Skype as a first step" . "<BR><BR>";
$message2 .= "<BR><BR>" . 'Kind Regards,' . "<BR><BR>" . 'The Wotmed Team' . "<BR><BR>";

$message2 .= 'Your paid consultation details:' . "<BR><BR>";

$message2 .= 'Requested Booking Date: ' . $requestedBookingDate . "<BR>";
$message2 .= 'Requested Booking Time: ' . $requestedBookingTime . "<BR><BR>";
$message2 .= 'Total Consulting Hours Purchased: ' . $consultingHoursPurchasedInt . "<BR>" . 'Total Price: $' . $totalPrice . '<BR><BR>';

$message2 .= 'Surgery Facilitator Name: ' . $practitionerName . "<BR>" . 'Surgery Facilitator PayPal Email Address: ' . $practitionerEmailAddress  . "<BR><BR>";
$message2 .= 'Surgery Facilitator Skype ID: ' . $skypeUserID . "<BR><BR>";

$message2 .= 'Consulting Services Telephone?: ' . $servicesOfferedTelephone . "<BR>";
$message2 .= 'Consulting Services Skype Voice?: ' . $servicesOfferedSkypeVoice . "<BR>";
$message2 .= 'Consulting Services Skype Video?: ' . $servicesOfferedSkypeVideo . "<BR>";
$message2 .= 'Consulting Services Email?: ' . $servicesOfferedEmail . "<BR>";
$message2 .= 'Consulting Services In Person?: ' . $servicesOfferedInPerson . "<BR><BR>";

$message2 .= 'Hourly Rate: USD$' . $hourlyRateInt . "<BR>" . 'Consulting Phone Number: ' . $phone . "<BR>";
$message2 .= 'Business Name: ' . $practitionerBusinessName . "<BR>";
$message2 .= 'Business Address: ' . $practitionerBusinessAddress . ' ' . $practitionerBusinessSuburb . ' ' . $practitionerBusinessCity . ' ' . $practitionerBusinessState . ' ' . $practitionerBusinessPostCode . "<BR>";

$message2 .= 'Business Photo: ' . $bannerCode2 . "<BR>";
$message2 .= 'Surgery Facilitator Business Logo: ' . $bannerCode . "<BR>";

$message2 .= 'Surgery Facilitator First Name: ' . $practitionerFirstName . "<BR>";
$message2 .= 'Surgery Facilitator Surname: ' . $practitionerSurName . "<BR>";




$message2 .= file_get_contents('../../HTMLEmailTemplate/WotmedEmailTemplate/RequestforProposalBodyFooterClient.html');

$message2 .= file_get_contents('../../HTMLEmailTemplate/WotmedEmailTemplate/RequestForProposalFooterClient.html');



if(mail($address2, $subject2, $message2, $headers2)) 
{

}

else 

{
	echo 'ERROR!';
}


// email the practitioner themselves now and notify them that a patient has requested a paid consultation

// $address3 = $practitionerEmailAddress;


$address3 = $paypalEmailAddress;

$e_body = "$patientName has has just purchased a Wotmed Paid Consultation from you!";
$e_content = "\"$comments\"" . PHP_EOL . PHP_EOL;
$e_reply = "You can contact $patientName via email, $paypalEmailAddress";

$msg = wordwrap( $e_body . $e_content . $e_reply, 70 );


$headers3 = 'From: wotmedconsultations@wotmed.com' . "\r\n" .
    'Reply-To: wotmedconsultations@wotmed.com' . "\r\n" .
    'X-Mailer: PHP/' . phpversion();
$headers3 .= "MIME-Version: 1.0\r\n";
$headers3 .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

$subject3 = 'A patient: ' . $patientFirstName . ' ' . $patientSurname . ' has just purchased a Wotmed Paid Consultation from you ' . $practitionerName . '!';

$message3 = file_get_contents('../../HTMLEmailTemplate/WotmedEmailTemplate/RequestforProposalHeader.html');
$message3 .= file_get_contents('../../HTMLEmailTemplate/WotmedEmailTemplate/PaidConsultationPurchasedBodyHeaderPractitioner.html');

$message3 .= $bannerCodeGraphicPractitioner . "<BR><BR>";

$message3 .= "You are receiving this email because you have elected to take part in the Wotmed Paid Consultations program and you have just received a paid consultation request from a Wotmed Patient" . "<BR><BR>";
$message3 .= "Please ensure that you the patient has paid you via PayPal first before initiating first contact" . "<BR>";
$message3 .= "The Patients contact details are below.  You should add the Patient to Skype as a first step" . "<BR><BR>";

$message3 .= 'Patient Name: ' . $patientFirstName . ' ' . $patientSurname . "<BR>" . 'Patient PayPal Email Address: ' . $paypalEmailAddressPatient . '<BR><BR>';
$message3 .= 'Your PayPal Email Address the patient is sending payment to via PayPal: ' . $practitionerEmailAddress . '<BR><BR>';
$message3 .= 'Your Skype ID the patient will be contacting you via: ' . $skypeUserID . "<BR><BR>";

$message3 .= 'Patients requested paid consultation details:' . "<BR><BR>";

$message3 .= 'Requested Booking Date: ' . $requestedBookingDate . "<BR>";
$message3 .= 'Requested Booking Time: ' . $requestedBookingTime . "<BR><BR>";

$message3 .= 'Total Consulting Hours Purchased: ' . $consultingHoursPurchasedInt . "<BR>" . 'Total Price: $' . $totalPrice . '<BR><BR>';
$message3 .= 'Wotmed Commission for facilitating this consultation: $' . $wotmedCommission . "<BR><BR>";

$message3 .= 'Consulting Services Telephone: ' . $servicesOfferedTelephone . "<BR>";
$message3 .= 'Consulting Services Skype Voice: ' . $servicesOfferedSkypeVoice . "<BR>";
$message3 .= 'Consulting Services Skype Video: ' . $servicesOfferedSkypeVideo . "<BR>";
$message3 .= 'Consulting Services Email: ' . $servicesOfferedEmail . "<BR>";
$message3 .= 'Consulting Services In Person: ' . $servicesOfferedInPerson . "<BR><BR>";

$message3 .= 'Your Hourly Rate: USD$' . $hourlyRateInt . "<BR>" . 'Patients Skype ID: ' . $skypeID . "<BR>" . 'Your Consulting Phone Number: ' . $phone . "<BR>" ;
$message3 .= 'Your Business Name: ' . $practitionerBusinessName . "<BR>";
$message3 .= 'Your Business Address: ' . $practitionerBusinessAddress . ' ' . $practitionerBusinessSuburb . ' ' . $practitionerBusinessCity . ' ' . $practitionerBusinessState . ' ' . $practitionerBusinessPostCode . "<BR>";


$message3 .= file_get_contents('../../HTMLEmailTemplate/WotmedEmailTemplate/RequestforProposalBodyFooterClient.html');

$message3 .= file_get_contents('../../HTMLEmailTemplate/WotmedEmailTemplate/RequestForProposalFooterClient.html');



if(mail($address3, $subject3, $message3, $headers3))
{

    //  include("reg-consultingSuccessBuy.php");

    //  header("/proposals/proposalsubmit/reg-consultingSuccess.php");

}

else

{
    echo 'ERROR!';
}

$wotmedPaymentsEmailAddress = 'paypalpayments@wotmed.com';
$wotmedPatientNumber = $_SESSION['id'];				                                        //	2132
$wotmedPatientEmailAddress = $_SESSION['PATIENTEMAILADDRESS'];								// clarketowson@wotmed.com
$wotmedPractitionerBusinessName = $_SESSION['PRACTITIONERBUSINESSNAME'];					// James Smith
$wotmedPractitionerNumber = $_SESSION['PRACTITIONER_NUMBER'];								//  211
$wotmedPractitionerBusinessEmailAddress = $_SESSION['PRACTITIONEREMAILADDRESS'];			//  jamessmith19791@yahoo.com.au









// TowsonC August 2nd 2012 - Updated July 2015
// The Wotmed.com Transaction Percentage Fee. This fee is 3 percent of the transacted amount which the patient pays
// so in the example - the buyer pays $12,500 for a Rhinoplasty and Wotmed.com gets 3 percent of this amount which is $375
//
// This code interfaces with Paypal.  It will connect to Paypal and allow a Parallel transaction to occur with the following details:
//
// 1) Practitioner receives a payment for an Advertised Surgery after the Patient clicks the Buy Now Paypal button on the Practitioners Profile
// 2) Wotmed.com receives a 3 percent fee off the transaction.  This fee is for being a net weaver and for putting the Practitioner and patient together on the Wotmed.com Platform
// 3) The Practitioner is the PRIMARYRECEIVER and therefore is responsible for paying the PayPal transaction fee.  This is like how on ebay that the sellers are charged when selling
// 4) http://wwww.wotmed.com/PreapprovalCancelHandler.html 		This page has been created for when a Patient cancels a transaction
// 5) http://wwww.wotmed.com/PreapprovalReturnHandler.html		This page has been created for the return url
// There are a number of variables in this code which need to be set programatically when this code is in live use:
//
// $buyerTransactionAmount		will be set to the value of each PayPal button on the Practitioners Profiles
// $receiverInvoiceIdArray		will be set to the patient and practitioners IDs from the database
//
//

$wotmedPercentageFee = "0.10";

// $buyerTransactionAmount = "800";

// get the amount from the shopping cart from chart.php which has the value stored is $value


// $buyerTransactionAmount = $_SESSION['CARTTOTAL'];

// $buyerTransactionAmount = $_POST["PractitionersHourlyRate"]; // Practitioners Hourly Consulting Rate

$buyerTransactionAmount = $totalPrice;

//$buyerTransactionAmount = $_SESSION['TOTALPRICE']; // Practitioners Hourly Consulting Rate



//$buyerTransactionAmount = 50;

// calculate the Wotmed.com Buyer Transaction Fee.  This is the amount that Wotmed.com earns from each Buyer who pays for surgery purchased from
// a Practitioner

$wotmedFinalPercentageDollarCharge = ($buyerTransactionAmount * $wotmedPercentageFee);

// set the patient to pay the paypal transaction fees
// perhaps it should be the PRACTITIONER who pays this?

// $feesPayable = PRIMARYRECEIVER;

// calculate remainder amount for Practitioner

$practitionerTransactionRemainderCharge = ($buyerTransactionAmount - $wotmedFinalPercentageDollarCharge);

//-------------------------------------------------
// When you integrate this code
// look for TODO as an indication
// that you may need to provide a value or take action
// before executing this code
//-------------------------------------------------

/********************************************
PayPal Adaptive Payments API Module

Defines all the global variables and the wrapper functions
 ********************************************/
$PROXY_HOST = '127.0.0.1';
$PROXY_PORT = '808';

$Env = "live";

//------------------------------------
// PayPal API Credentials
// Replace <API_USERNAME> with your API Username
// Replace <API_PASSWORD> with your API Password
// Replace <API_SIGNATURE> with your Signature
//------------------------------------





$API_UserName = "clarketowson_api1.yahoo.com.au";
$API_Password = "Z8YKFCN7886P9NKA";
$API_Signature = "Ap2.L4xlU4gvbnxwY6ZzduB7qvIFAIWDUjFoNbF22qXTuEomO0Y002kR";

// AppID is preset for sandbox use
//   If your application goes live, you will be assigned a value for the live environment by PayPal as part of the live onboarding process
$API_AppID = "APP-4YG51019LB127002L";
$API_Endpoint = "";

if ($Env == "sandbox")
{
    $API_Endpoint = "https://svcs.sandbox.paypal.com/AdaptivePayments";
}
else
{
    $API_Endpoint = "https://svcs.paypal.com/AdaptivePayments";
}

$USE_PROXY = false;

if (session_id() == "")
    session_start();

function generateCharacter () {
    $possible = "1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    $char = substr($possible, mt_rand(0, strlen($possible)-1), 1);
    return $char;
}

function generateTrackingID () {
    $GUID = generateCharacter().generateCharacter().generateCharacter().generateCharacter().generateCharacter();
    $GUID .= generateCharacter().generateCharacter().generateCharacter().generateCharacter();
    return $GUID;
}

/*
'-------------------------------------------------------------------------------------------------------------------------------------------
' Purpose: 	Prepares the parameters for the Refund API Call.
'			The API credentials used in a Pay call can make the Refund call
'			against a payKey, or a tracking id, or to specific receivers of a payKey or a tracking id
'			that resulted from the Pay call
'
'			A receiver itself with its own API credentials can make a Refund call against the transactionId corresponding to their transaction.
'			The API credentials used in a Pay call cannot use transactionId to issue a refund
'			for a transaction for which they themselves were not the receiver
'
'			If you do specify specific receivers, keep in mind that you must provide the amounts as well
'			If you specify a transactionId, then only the receiver of that transactionId is affected therefore
'			the receiverEmailArray and receiverAmountArray should have 1 entry each if you do want to give a partial refund
' Inputs:
'
' Conditionally Required:
'		One of the following:  payKey or trackingId or trasactionId or
'                              (payKey and receiverEmailArray and receiverAmountArray) or
'                              (trackingId and receiverEmailArray and receiverAmountArray) or
'                              (transactionId and receiverEmailArray and receiverAmountArray)
' Returns:
'		The NVP Collection object of the Refund call response.
'--------------------------------------------------------------------------------------------------------------------------------------------
*/
function CallRefund( $payKey, $transactionId, $trackingId, $receiverEmailArray, $receiverAmountArray )
{
    /* Gather the information to make the Refund call.
        The variable nvpstr holds the name value pairs
    */

    $nvpstr = "";

    // conditionally required fields
    if ("" != $payKey)
    {
        $nvpstr = "payKey=" . urlencode($payKey);
        if (0 != count($receiverEmailArray))
        {
            reset($receiverEmailArray);
            while (list($key, $value) = each($receiverEmailArray))
            {
                if ("" != $value)
                {
                    $nvpstr .= "&receiverList.receiver(" . $key . ").email=" . urlencode($value);
                }
            }
        }
        if (0 != count($receiverAmountArray))
        {
            reset($receiverAmountArray);
            while (list($key, $value) = each($receiverAmountArray))
            {
                if ("" != $value)
                {
                    $nvpstr .= "&receiverList.receiver(" . $key . ").amount=" . urlencode($value);
                }
            }
        }
    }
    elseif ("" != $trackingId)
    {
        $nvpstr = "trackingId=" . urlencode($trackingId);
        if (0 != count($receiverEmailArray))
        {
            reset($receiverEmailArray);
            while (list($key, $value) = each($receiverEmailArray))
            {
                if ("" != $value)
                {
                    $nvpstr .= "&receiverList.receiver(" . $key . ").email=" . urlencode($value);
                }
            }
        }
        if (0 != count($receiverAmountArray))
        {
            reset($receiverAmountArray);
            while (list($key, $value) = each($receiverAmountArray))
            {
                if ("" != $value)
                {
                    $nvpstr .= "&receiverList.receiver(" . $key . ").amount=" . urlencode($value);
                }
            }
        }
    }
    elseif ("" != $transactionId)
    {
        $nvpstr = "transactionId=" . urlencode($transactionId);
        // the caller should only have 1 entry in the email and amount arrays
        if (0 != count($receiverEmailArray))
        {
            reset($receiverEmailArray);
            while (list($key, $value) = each($receiverEmailArray))
            {
                if ("" != $value)
                {
                    $nvpstr .= "&receiverList.receiver(" . $key . ").email=" . urlencode($value);
                }
            }
        }
        if (0 != count($receiverAmountArray))
        {
            reset($receiverAmountArray);
            while (list($key, $value) = each($receiverAmountArray))
            {
                if ("" != $value)
                {
                    $nvpstr .= "&receiverList.receiver(" . $key . ").amount=" . urlencode($value);
                }
            }
        }
    }

    /* Make the Refund call to PayPal */
    $resArray = hash_call("Refund", $nvpstr);

    /* Return the response array */
    return $resArray;
}

/*
'-------------------------------------------------------------------------------------------------------------------------------------------
' Purpose: 	Prepares the parameters for the PaymentDetails API Call.
'			The PaymentDetails call can be made with either
'			a payKey, a trackingId, or a transactionId of a previously successful Pay call.
' Inputs:
'
' Conditionally Required:
'		One of the following:  payKey or transactionId or trackingId
' Returns:
'		The NVP Collection object of the PaymentDetails call response.
'--------------------------------------------------------------------------------------------------------------------------------------------
*/
function CallPaymentDetails( $payKey, $transactionId, $trackingId )
{
    /* Gather the information to make the PaymentDetails call.
        The variable nvpstr holds the name value pairs
    */

    $nvpstr = "";

    // conditionally required fields
    if ("" != $payKey)
    {
        $nvpstr = "payKey=" . urlencode($payKey);
    }
    elseif ("" != $transactionId)
    {
        $nvpstr = "transactionId=" . urlencode($transactionId);
    }
    elseif ("" != $trackingId)
    {
        $nvpstr = "trackingId=" . urlencode($trackingId);
    }

    /* Make the PaymentDetails call to PayPal */
    $resArray = hash_call("PaymentDetails", $nvpstr);

    /* Return the response array */
    return $resArray;
}

/*
'-------------------------------------------------------------------------------------------------------------------------------------------
' Purpose: 	Prepares the parameters for the Pay API Call.
' Inputs:
'
' Required:
'
' Optional:
'
'
' Returns:
'		The NVP Collection object of the Pay call response.
'--------------------------------------------------------------------------------------------------------------------------------------------
*/
function CallPay( $actionType, $cancelUrl, $returnUrl, $currencyCode, $receiverEmailArray, $receiverAmountArray,
                  $receiverPrimaryArray, $receiverInvoiceIdArray, $feesPayer, $ipnNotificationUrl,
                  $memo, $pin, $preapprovalKey, $reverseAllParallelPaymentsOnError, $senderEmail, $trackingId )
{
    /* Gather the information to make the Pay call.
        The variable nvpstr holds the name value pairs
    */

    // required fields
    $nvpstr = "actionType=" . urlencode($actionType) . "&currencyCode=" . urlencode($currencyCode);
    $nvpstr .= "&returnUrl=" . urlencode($returnUrl) . "&cancelUrl=" . urlencode($cancelUrl);

    if (0 != count($receiverAmountArray))
    {
        reset($receiverAmountArray);
        while (list($key, $value) = each($receiverAmountArray))
        {
            if ("" != $value)
            {
                $nvpstr .= "&receiverList.receiver(" . $key . ").amount=" . urlencode($value);
            }
        }
    }

    if (0 != count($receiverEmailArray))
    {
        reset($receiverEmailArray);
        while (list($key, $value) = each($receiverEmailArray))
        {
            if ("" != $value)
            {
                $nvpstr .= "&receiverList.receiver(" . $key . ").email=" . urlencode($value);
            }
        }
    }

    if (0 != count($receiverPrimaryArray))
    {
        reset($receiverPrimaryArray);
        while (list($key, $value) = each($receiverPrimaryArray))
        {
            if ("" != $value)
            {
                $nvpstr = $nvpstr . "&receiverList.receiver(" . $key . ").primary=" . urlencode($value);
            }
        }
    }

    if (0 != count($receiverInvoiceIdArray))
    {
        reset($receiverInvoiceIdArray);
        while (list($key, $value) = each($receiverInvoiceIdArray))
        {
            if ("" != $value)
            {
                $nvpstr = $nvpstr . "&receiverList.receiver(" . $key . ").invoiceId=" . urlencode($value);
            }
        }
    }

    // optional fields
    if ("" != $feesPayer)
    {
        $nvpstr .= "&feesPayer=" . urlencode($feesPayer);
    }

    if ("" != $ipnNotificationUrl)
    {
        $nvpstr .= "&ipnNotificationUrl=" . urlencode($ipnNotificationUrl);
    }

    if ("" != $memo)
    {
        $nvpstr .= "&memo=" . urlencode($memo);
    }

    if ("" != $pin)
    {
        $nvpstr .= "&pin=" . urlencode($pin);
    }

    if ("" != $preapprovalKey)
    {
        $nvpstr .= "&preapprovalKey=" . urlencode($preapprovalKey);
    }

    if ("" != $reverseAllParallelPaymentsOnError)
    {
        $nvpstr .= "&reverseAllParallelPaymentsOnError=" . urlencode($reverseAllParallelPaymentsOnError);
    }

    if ("" != $senderEmail)
    {
        $nvpstr .= "&senderEmail=" . urlencode($senderEmail);
    }

    if ("" != $trackingId)
    {
        $nvpstr .= "&trackingId=" . urlencode($trackingId);
    }

    /* Make the Pay call to PayPal */
    $resArray = hash_call("Pay", $nvpstr);

    /* Return the response array */
    return $resArray;
}

/*
'-------------------------------------------------------------------------------------------------------------------------------------------
' Purpose: 	Prepares the parameters for the PreapprovalDetails API Call.
' Inputs:
'
' Required:
'		preapprovalKey:		A preapproval key that identifies the agreement resulting from a previously successful Preapproval call.
' Returns:
'		The NVP Collection object of the PreapprovalDetails call response.
'--------------------------------------------------------------------------------------------------------------------------------------------
*/
function CallPreapprovalDetails( $preapprovalKey )
{
    /* Gather the information to make the PreapprovalDetails call.
        The variable nvpstr holds the name value pairs
    */

    // required fields
    $nvpstr = "preapprovalKey=" . urlencode($preapprovalKey);

    /* Make the PreapprovalDetails call to PayPal */
    $resArray = hash_call("PreapprovalDetails", $nvpstr);

    /* Return the response array */
    return $resArray;
}

/*
'-------------------------------------------------------------------------------------------------------------------------------------------
' Purpose: 	Prepares the parameters for the Preapproval API Call.
' Inputs:
'
' Required:
'
' Optional:
'
'
' Returns:
'		The NVP Collection object of the Preapproval call response.
'--------------------------------------------------------------------------------------------------------------------------------------------
*/
function CallPreapproval( $returnUrl, $cancelUrl, $currencyCode, $startingDate, $endingDate, $maxTotalAmountOfAllPayments,
                          $senderEmail, $maxNumberOfPayments, $paymentPeriod, $dateOfMonth, $dayOfWeek,
                          $maxAmountPerPayment, $maxNumberOfPaymentsPerPeriod, $pinType )
{
    /* Gather the information to make the Preapproval call.
        The variable nvpstr holds the name value pairs
    */

    // required fields
    $nvpstr = "returnUrl=" . urlencode($returnUrl) . "&cancelUrl=" . urlencode($cancelUrl);
    $nvpstr .= "&currencyCode=" . urlencode($currencyCode) . "&startingDate=" . urlencode($startingDate);
    $nvpstr .= "&endingDate=" . urlencode($endingDate);
    $nvpstr .= "&maxTotalAmountOfAllPayments=" . urlencode($maxTotalAmountOfAllPayments);

    // optional fields
    if ("" != $senderEmail)
    {
        $nvpstr .= "&senderEmail=" . urlencode($senderEmail);
    }

    if ("" != $maxNumberOfPayments)
    {
        $nvpstr .= "&maxNumberOfPayments=" . urlencode($maxNumberOfPayments);
    }

    if ("" != $paymentPeriod)
    {
        $nvpstr .= "&paymentPeriod=" . urlencode($paymentPeriod);
    }

    if ("" != $dateOfMonth)
    {
        $nvpstr .= "&dateOfMonth=" . urlencode($dateOfMonth);
    }

    if ("" != $dayOfWeek)
    {
        $nvpstr .= "&dayOfWeek=" . urlencode($dayOfWeek);
    }

    if ("" != $maxAmountPerPayment)
    {
        $nvpstr .= "&maxAmountPerPayment=" . urlencode($maxAmountPerPayment);
    }

    if ("" != $maxNumberOfPaymentsPerPeriod)
    {
        $nvpstr .= "&maxNumberOfPaymentsPerPeriod=" . urlencode($maxNumberOfPaymentsPerPeriod);
    }

    if ("" != $pinType)
    {
        $nvpstr .= "&pinType=" . urlencode($pinType);
    }

    /* Make the Preapproval call to PayPal */
    $resArray = hash_call("Preapproval", $nvpstr);

    /* Return the response array */
    return $resArray;
}

/**
'-------------------------------------------------------------------------------------------------------------------------------------------
 * hash_call: Function to perform the API call to PayPal using API signature
 * @methodName is name of API method.
 * @nvpStr is nvp string.
 * returns an associative array containing the response from the server.
'-------------------------------------------------------------------------------------------------------------------------------------------
 */
function hash_call($methodName, $nvpStr)
{
    //declaring of global variables
    global $API_Endpoint, $API_UserName, $API_Password, $API_Signature, $API_AppID;
    global $USE_PROXY, $PROXY_HOST, $PROXY_PORT;

    $API_Endpoint .= "/" . $methodName;
    //setting the curl parameters.
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL,$API_Endpoint);
    curl_setopt($ch, CURLOPT_VERBOSE, 1);

    //turning off the server and peer verification(TrustManager Concept).
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);

    curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
    curl_setopt($ch, CURLOPT_POST, 1);

    // Set the HTTP Headers
    curl_setopt($ch, CURLOPT_HTTPHEADER,  array(
        'X-PAYPAL-REQUEST-DATA-FORMAT: NV',
        'X-PAYPAL-RESPONSE-DATA-FORMAT: NV',
        'X-PAYPAL-SECURITY-USERID: ' . $API_UserName,
        'X-PAYPAL-SECURITY-PASSWORD: ' .$API_Password,
        'X-PAYPAL-SECURITY-SIGNATURE: ' . $API_Signature,
        'X-PAYPAL-SERVICE-VERSION: 1.3.0',
        'X-PAYPAL-APPLICATION-ID: ' . $API_AppID
    ));

    //if USE_PROXY constant set to TRUE in Constants.php, then only proxy will be enabled.
    //Set proxy name to PROXY_HOST and port number to PROXY_PORT in constants.php
    if($USE_PROXY)
        curl_setopt ($ch, CURLOPT_PROXY, $PROXY_HOST. ":" . $PROXY_PORT);

    // RequestEnvelope fields
    $detailLevel	= urlencode("ReturnAll");	// See DetailLevelCode in the WSDL for valid enumerations
    $errorLanguage	= urlencode("en_US");		// This should be the standard RFC 3066 language identification tag, e.g., en_US

    // NVPRequest for submitting to server
    $nvpreq = "requestEnvelope.errorLanguage=$errorLanguage&requestEnvelope.detailLevel=$detailLevel";
    $nvpreq .= "&$nvpStr";

    //setting the nvpreq as POST FIELD to curl
    curl_setopt($ch, CURLOPT_POSTFIELDS, $nvpreq);

    //getting response from server
    $response = curl_exec($ch);

    //converting NVPResponse to an Associative Array
    $nvpResArray=deformatNVP($response);
    $nvpReqArray=deformatNVP($nvpreq);
    $_SESSION['nvpReqArray']=$nvpReqArray;

    if (curl_errno($ch))
    {
        // moving to display page to display curl errors
        $_SESSION['curl_error_no']=curl_errno($ch) ;
        $_SESSION['curl_error_msg']=curl_error($ch);

        //Execute the Error handling module to display errors.
    }
    else
    {
        //closing the curl
        curl_close($ch);
    }

    return $nvpResArray;
}

/*'----------------------------------------------------------------------------------
 Purpose: Redirects to PayPal.com site.
 Inputs:  $cmd is the querystring
 Returns:
----------------------------------------------------------------------------------
*/
function RedirectToPayPal ( $cmd )
{
    // Redirect to paypal.com here
    global $Env;

    $payPalURL = "";

    if ($Env == "sandbox")
    {
        $payPalURL = "https://www.sandbox.paypal.com/webscr?" . $cmd;
    }
    else
    {
        $payPalURL = "https://www.paypal.com/webscr?" . $cmd;
    }

    header("Location: ".$payPalURL);
}


/*'----------------------------------------------------------------------------------
 * This function will take NVPString and convert it to an Associative Array and it will decode the response.
  * It is usefull to search for a particular key and displaying arrays.
  * @nvpstr is NVPString.
  * @nvpArray is Associative Array.
   ----------------------------------------------------------------------------------
  */
function deformatNVP($nvpstr)
{
    $intial=0;
    $nvpArray = array();

    while(strlen($nvpstr))
    {
        //postion of Key
        $keypos= strpos($nvpstr,'=');
        //position of value
        $valuepos = strpos($nvpstr,'&') ? strpos($nvpstr,'&'): strlen($nvpstr);

        /*getting the Key and Value values and storing in a Associative Array*/
        $keyval=substr($nvpstr,$intial,$keypos);
        $valval=substr($nvpstr,$keypos+1,$valuepos-$keypos-1);
        //decoding the respose
        $nvpArray[urldecode($keyval)] =urldecode( $valval);
        $nvpstr=substr($nvpstr,$valuepos+1,strlen($nvpstr));
    }
    return $nvpArray;
}



// ==================================
// PayPal Platform Parallel Payment Module
// ==================================

// Request specific required fields
$actionType			= "PAY";
$cancelUrl			= "http://www.platform.wotmed.com/profileConsultingPaymentFailure.php";	// TODO - If you are not executing the Pay call for a preapproval,
//        then you must set a valid cancelUrl for the web approval flow
//        that immediately follows this Pay call
$returnUrl			= "http://www.platform.wotmed.com/profileConsultationPaymentSuccess.php";	// TODO - If you are not executing the Pay call for a preapproval,
//        then you must set a valid returnUrl for the web approval flow
//        that immediately follows this Pay call
$currencyCode		= "USD";

// A parallel payment can be made among two to six receivers
// TODO - specify the receiver emails
//        remove or set to an empty string the array entries for receivers that you do not have

// $receiverEmailArray	= array('doctor_1343877949_per@wotmed.com','wot_1343877381_biz@wotmed.com','','','','');

$receiverEmailArray	= array($wotmedPractitionerBusinessEmailAddress, $wotmedPaymentsEmailAddress,'','','','');


// TODO - specify the receiver amounts as the amount of money, for example, '5' or '5.55'
//        remove or set to an empty string the array entries for receivers that you do not have
// $receiverAmountArray = array(
//		'375',
//		'12500',
//		'',
//		'',
//	'',
//		''
//		);


// This array lists the payment shares to be distributed to each receiver as
// specified within the $receiverEmailArray

$receiverAmountArray = array($practitionerTransactionRemainderCharge, $wotmedFinalPercentageDollarCharge,'','','','');


// for parallel payment, no primary indicators are needed, so set empty array
$receiverPrimaryArray = array();

// TODO - Set invoiceId to uniquely identify the transaction associated with each receiver
//        set the array entries with value for receivers that you have
//		  each of the array values must be unique
$receiverInvoiceIdArray = array(
    $wotmedPatientNumber,
    $wotmedPractitionerNumber,
    '',
    '',
    '',
    ''
);

// Request specific optional fields
//   Provide a value for each field that you want to include in the request, if left as an empty string the field will not be passed in the request
$senderEmail					= "";		// TODO - If you are executing the Pay call against a preapprovalKey, you should set senderEmail
//        It is not required if the web approval flow immediately follows this Pay call
$feesPayer						= "";
$ipnNotificationUrl				= "";
$memo							= "You have received a payment for consulting from this patient via the Wotmed Platform";		// maxlength is 1000 characters
$pin							= "";		// TODO - If you are executing the Pay call against an existing preapproval
//        the requires a pin, then you must set this
$preapprovalKey					= "";		// TODO - If you are executing the Pay call against an existing preapproval, set the preapprovalKey here
$reverseAllParallelPaymentsOnError	= "";	// TODO - Set this to "true" if you would like each parallel payment to be reversed if an error occurs
//        defaults to "false" if you don't specify
$trackingId						= generateTrackingID();	// generateTrackingID function is found in paypalplatform.php

//-------------------------------------------------
// Make the Pay API call
//
// The CallPay function is defined in the paypalplatform.php file,
// which is included at the top of this file.
//-------------------------------------------------
$resArray = CallPay ($actionType, $cancelUrl, $returnUrl, $currencyCode, $receiverEmailArray,
    $receiverAmountArray, $receiverPrimaryArray, $receiverInvoiceIdArray,
    $feesPayer, $ipnNotificationUrl, $memo, $pin, $preapprovalKey,
    $reverseAllParallelPaymentsOnError, $senderEmail, $trackingId
);

$ack = strtoupper($resArray["responseEnvelope.ack"]);
if($ack=="SUCCESS")
{
    if ("" == $preapprovalKey)
    {
        // redirect for web approval flow
        $cmd = "cmd=_ap-payment&paykey=" . urldecode($resArray["payKey"]);
        RedirectToPayPal ( $cmd );
    }
    else
    {
        // payKey is the key that you can use to identify the result from this Pay call
        $payKey = urldecode($resArray["payKey"]);
        // paymentExecStatus is the status of the payment
        $paymentExecStatus = urldecode($resArray["paymentExecStatus"]);
    }
}
else
{
    //Display a user friendly Error on the page using any of the following error information returned by PayPal
    //TODO - There can be more than 1 error, so check for "error(1).errorId", then "error(2).errorId", and so on until you find no more errors.
    $ErrorCode = urldecode($resArray["error(0).errorId"]);
    $ErrorMsg = urldecode($resArray["error(0).message"]);
    $ErrorDomain = urldecode($resArray["error(0).domain"]);
    $ErrorSeverity = urldecode($resArray["error(0).severity"]);
    $ErrorCategory = urldecode($resArray["error(0).category"]);

    echo "Preapproval API call failed. ";
    echo "Detailed Error Message: " . $ErrorMsg;
    echo "Error Code: " . $ErrorCode;
    echo "Error Severity: " . $ErrorSeverity;
    echo "Error Domain: " . $ErrorDomain;
    echo "Error Category: " . $ErrorCategory;


    //
    //  Error testing code for amounts
    //

//	echo "<br>";
//	echo "<br>";
//	echo "Practitioner Transaction Remainder is: " . $practitionerTransactionRemainderCharge;
//	echo "<br>";
//	echo "Wotmed Final percentage Dollar Amount Charge is: " . $wotmedFinalPercentageDollarCharge;
//	echo "<br>";
//	echo "Wotmed Final percentage Dollar Charge is: " . $wotmedFinalPercentageDollarCharge;

}



