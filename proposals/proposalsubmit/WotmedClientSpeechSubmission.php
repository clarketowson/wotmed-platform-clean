<?php


session_start();


if(!$_POST) exit;

// Email address verification, do not edit.
function isEmail($email) {
	return(preg_match("/^[-_.[:alnum:]]+@((([[:alnum:]]|[[:alnum:]][[:alnum:]-]*[[:alnum:]])\.)+(ad|ae|aero|af|ag|ai|al|am|an|ao|aq|ar|arpa|as|at|au|aw|az|ba|bb|bd|be|bf|bg|bh|bi|biz|bj|bm|bn|bo|br|bs|bt|bv|bw|by|bz|ca|cc|cd|cf|cg|ch|ci|ck|cl|cm|cn|co|com|coop|cr|cs|cu|cv|cx|cy|cz|de|dj|dk|dm|do|dz|ec|edu|ee|eg|eh|er|es|et|eu|fi|fj|fk|fm|fo|fr|ga|gb|gd|ge|gf|gh|gi|gl|gm|gn|gov|gp|gq|gr|gs|gt|gu|gw|gy|hk|hm|hn|hr|ht|hu|id|ie|il|in|info|int|io|iq|ir|is|it|jm|jo|jp|ke|kg|kh|ki|km|kn|kp|kr|kw|ky|kz|la|lb|lc|li|lk|lr|ls|lt|lu|lv|ly|ma|mc|md|me|mg|mh|mil|mk|ml|mm|mn|mo|mp|mq|mr|ms|mt|mu|museum|mv|mw|mx|my|mz|na|name|nc|ne|net|nf|ng|ni|nl|no|np|nr|nt|nu|nz|om|org|pa|pe|pf|pg|ph|pk|pl|pm|pn|pr|pro|ps|pt|pw|py|qa|re|ro|ru|rw|sa|sb|sc|sd|se|sg|sh|si|sj|sk|sl|sm|sn|so|sr|st|su|sv|sy|sz|tc|td|tf|tg|th|tj|tk|tm|tn|to|tp|tr|tt|tv|tw|tz|ua|ug|uk|um|us|uy|uz|va|vc|ve|vg|vi|vn|vu|wf|ws|ye|yt|yu|za|zm|zw)$|(([0-9][0-9]?|[0-1][0-9][0-9]|[2][0-4][0-9]|[2][5][0-5])\.){3}([0-9][0-9]?|[0-1][0-9][0-9]|[2][0-4][0-9]|[2][5][0-5]))$/i",$email));
}

if (!defined("PHP_EOL")) define("PHP_EOL", "\r\n");


// store the prospective users information in the Wotmed Database in the PATIENTSTORIES table

// make sure to escape the users post data for security purposes

 $conn = new mysqli('liberty.wotmed.com','wotmedco_user','e5~1]qu1|&8)6wJ', 'wotmedco_db');

// $conn = new mysqli('localhost','wotmedco_user','Password1', 'WotmedDBLiveMirror');

// $conn = new mysqli('liberty.wotmed.com','wotmedco_user','e5~1]qu1|&8)6wJ', 'wotmedco_db');

// Create the Images in HTML code

$myfile = fopen("../../HTMLEmailTemplate/WotmedEmailTemplate/ClientSpeechUploaded.html", "w") or die("Unable to open file!");



// FIND THE LATEST FILE IN THE ImageUpload/uploads/clientspeeches DIRECTORY

$path = "../../ImageUpload/uploads/clientspeeches/";

$latest_ctime = 0;
$latest_filename = '';

$d = dir($path);
while (false !== ($entry = $d->read())) {
    $filepath = "{$path}/{$entry}";
    // could do also other checks than just checking whether the entry is a file
    if (is_file($filepath) && filectime($filepath) > $latest_ctime) {
        $latest_ctime = filectime($filepath);
        $latest_filename = $entry;
    }
}


$name     = mysqli_real_escape_string($conn, $_POST['SpeechSubmitterName']);
$speakerPosition    = mysqli_real_escape_string($conn, $_POST['SpeechSubmitterPosition']);
$speakerShortPhraseToRemember = mysqli_real_escape_string($conn, $_POST['SpeechShortPhraseToRemember']);
$phone    = mysqli_real_escape_string($conn, $_POST['SpeechSubmitterPhoneNumber']);
$email    = mysqli_real_escape_string($conn, $_POST['SpeechSubmitterEmailAddress']);
$speechTitle    = mysqli_real_escape_string($conn, $_POST['SpeechTitle']);
$speechSubject = mysqli_real_escape_string($conn, $_POST['SpeechSubject']);
$speechSEOKeywords = mysqli_real_escape_string($conn, $_POST['SpeechSEOKeywords']);
$speechTranscript = mysqli_real_escape_string($conn, $_POST['SpeechTranscript']);
$speakerCountry  = mysqli_real_escape_string($conn, $_POST['SpeechSubmitterCountry']);

$isSpeakerContactable = mysqli_real_escape_string($conn, $_POST['IsSpeakerContactable']);
$practiceOrBusinessName = mysqli_real_escape_string($conn, $_POST['PracticeOrBusinessName']);
$practiceOrBusinessSpeciality = mysqli_real_escape_string($conn, $_POST['PracticeOrBusinessSpeciality']);
$practiceOrBusinessPhone = mysqli_real_escape_string($conn, $_POST['PracticeOrBusinessPhone']);
$practiceOrBusinessAddress = mysqli_real_escape_string($conn, $_POST['PracticeOrBusinessAddress']);
$practiceOrBusinessIndependentWebsiteHyperlink = mysqli_real_escape_string($conn, $_POST['PracticeOrBusinessIndependentWebsiteHyperlink']);
$practiceOrBusinessMarketingBlurb = mysqli_real_escape_string($conn, $_POST['PracticeOrBusinessMarketingBlurb']);
// $practiceOrBusinessMarketingBlurbImage = mysqli_real_escape_string($conn, $_POST['PracticeOrBusinessMarketingBlurbImage']);
// $practiceOrBusinessLogo = mysqli_real_escape_string($conn, $_POST['fileToUpload']);

$clientVideoThumb = "TBA";

$speechPublishedHyperlink = "TBA";


$videoFileName = $_SESSION['VideoFileNameIs'];
$photoFileName = $_SESSION['PhotoFileNameIs'];
$practiceOrBusinessLogoFileName = $_SESSION['PracticeOrBusinessFileNameIs'];
$practiceOrBusinessMarketingImage = $_SESSION['PracticeOrBusinessMarketingImageFileNameIs'];

$practiceOrBusinessThumbImage = $_SESSION["PracticeOrBusinessThumbImageFileNameIs"];


$randomNumber = mt_rand(0, 100000);

// $storyHyperlink = date("Y-m-d") . $randomNumber . '.php';

$articleHyperlink = date("Y-m-d");



if(trim($name) == '')
{
	echo '<div class="error_message">You must enter your name (we wont share this if you dont want us to).</div>';
	exit();
}

else if(trim($email) == '')
{
	echo '<div class="error_message">Please enter a valid email address (we wont share this if you dont want us to).</div>';
	exit();

}

else if(trim($phone) == '')
{
	echo '<div class="error_message">Please enter a valid phone number (we wont share this).</div>';
	exit();

}

else if(!isEmail($email))
{
   echo '<div class="error_message">You have entered an invalid e-mail address, please try again (we wont share this if you dont want us to).</div>';
   exit();
}


if(get_magic_quotes_gpc())
{
    $articleText = stripslashes($articleText);
}

if ($conn->connect_error)
{
header('Location: /DatabaseError.shtml');
}

$videoFileName = $_SESSION['VideoFileNameIs'];
$videoFileDirectory = "/home/wotmedco/public_html/ImageUpload/uploads/clientspeeches/";

$finalVideoName = $videoFileDirectory . $videoFileName;

// remove the file if it is already there

exec("rm /home/wotmedco/public_html/ImageUpload/uploads/clientspeeches/UploadedSpeechVideo.txt");

exec("ffprobe -sexagesimal -pretty -show_format -show_streams $finalVideoName >/home/wotmedco/public_html/ImageUpload/uploads/clientspeeches/UploadedSpeechVideo.txt");

$myVideofile = fopen("../../ImageUpload/uploads/clientspeeches/UploadedSpeechVideo.txt", "r") or die("Unable to open file!");
while(!feof($myVideofile))
{
    // echo fgets($myfile) . "<br>";
    $videoTechnicalInformation .= fgets($myVideofile) . "<br>";
    //  $videoTechnicalInformation .= fgets($myVideofile);
}
fclose($myVideofile);


//

$bannerCode = "
<!-- 3 columns -->
<table width='100%' bgcolor='#f7f7f7' cellpadding='0' cellspacing='0' border='0' id='backgroundTable'>
    <tbody>
    <tr>
        <td>
            <table width='600' cellpadding='0' cellspacing='0' border='0' align='center' class='devicewidth'>
                <tbody>
                <tr>
                    <td width='100%'>
                        <table width='600' cellpadding='0' cellspacing='0' border='0' align='center' class='devicewidth'>
                            <tbody>
                            <tr>
                                <td>
                                        <tr>
                                            <td width='186' height='250' align='center' class='devicewidth'>
                                            <p>Speech Profile Photo</p><BR>
                                    <img src='http://platform.wotmed.com/ImageUpload/uploads/clientspeeches/$photoFileName' alt='Photo' border='0' style='display:block; border:none; outline:none; text-decoration:none;'><BR><BR>
                                    <p>Practice or Business Logo</p><BR>
                                    <img src='http://platform.wotmed.com/ImageUpload/uploads/clientspeeches/$practiceOrBusinessLogoFileName' alt='Practice or Business Logo' border='0' style='display:block; border:none; outline:none; text-decoration:none;'><BR><BR>
                                    <p>Practice or Business Marketing Image</p><BR>
                                    <img src='http://platform.wotmed.com/ImageUpload/uploads/clientspeeches/$practiceOrBusinessMarketingImage' alt='Practice or Business Marketing Image' border='0' style='display:block; border:none; outline:none; text-decoration:none;'><BR><BR>
                                    <p>Practice or Business Marketing Video Thumbnail</p><BR>
                                    <img src='http://platform.wotmed.com/ImageUpload/uploads/clientspeeches/$practiceOrBusinessThumbImage' alt='Practice or Business Thumb Image' border='0' style='display:block; border:none; outline:none; text-decoration:none;'><BR><BR>

                                            </td>
                                        </tr>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>
<!-- end of 3 columns -->

";

// close the file

fwrite($myfile, $bannerCode);
fclose($myfile);



// store the practitioners article details

$result = $conn->query("INSERT INTO CLIENTSPEECHES(SPEECHSUBMISSIONDATE, SPEAKERNAME, SPEAKERPOSITION, SPEAKERSHORTPHRASETOREMEMBER, SPEECHSUBMITTERCOUNTRY, SPEECHSUBMITTEREMAILADDRESS, SPEECHTITLE, SPEECHSUBJECT, SPEECHTRANSCRIPT, SPEECHSEOKEYWORDS, PRACTICEORBUSINESSNAME, PRACTICEORBUSINESSSPECIALITY, PRACTICEORBUSINESSPHONE, PRACTICEORBUSINESSADDRESS, PRACTICEORBUSINESSLOGO, PRACTICEORBUSINESSINDEPENDENTWEBSITEHYPERLINK, PRACTICEORBUSINESSMARKETINGBLURB, PRACTICEORBUSINESSBLURBIMAGE, PRACTICEORBUSINESSISSPEAKERCONTACTABLE, SPEECHPUBLISHEDHYPERLINK, CLIENTPHOTOTHUMB, CLIENTVIDEOTHUMB, VIDEOTECHNICALINFORMATION) VALUES (NOW(), '$name', '$speakerPosition', '$speakerShortPhraseToRemember', '$speakerCountry', '$email', '$speechTitle', '$speechSubject', '$speechTranscript', '$speechSEOKeywords', '$practiceOrBusinessName', '$practiceOrBusinessSpeciality', '$practiceOrBusinessPhone', '$practiceOrBusinessAddress', '$practiceOrBusinessLogoFileName', '$practiceOrBusinessIndependentWebsiteHyperlink', '$practiceOrBusinessMarketingBlurb', '$practiceOrBusinessMarketingImage', '$isSpeakerContactable', '$speechPublishedHyperlink', '$photoFileName', '$practiceOrBusinessThumbImage', '$videoTechnicalInformation')");

$conn->close();


// Configuration option.
// Enter the email address that you want to emails to be sent to.
// Example $address = "joe.doe@yourdomain.com";

//$address = "example@themeforest.net";

$address = "clientspeechsubmissions@wotmed.com";

// $address = "patientstorysubmissions@wotmed.com";

// Configuration option.
// i.e. The standard subject will appear as, "You've been contacted by John Doe."

// Example, $e_subject = '$name . ' has contacted you via Your Website.';

//$e_subject = 'You\'ve been contacted by ' . $name . '.';


// Configuration option.
// You can change this if you feel that you need to.
// Developers, you may wish to add more fields to the form, in which case you must be sure to add them here.

$e_body = "$name has submitted a Video Speech";
$e_content = "\"$comments\"" . PHP_EOL . PHP_EOL;
$e_reply = "You can contact $name via email, $email";

$msg = wordwrap( $e_body . $e_content . $e_reply, 70 );


$headers = 'From: clientspeechsubmissions@wotmed.com' . "\r\n" .
    'Reply-To: clientspeechsubmissions@wotmed.com' . "\r\n" .
    'X-Mailer: PHP/' . phpversion();
$headers .= "MIME-Version: 1.0\r\n";
$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";


$subject = 'A client: ' . $name . ' with email address: ' . $email . ' has just submitted a video speech';

$message = file_get_contents('../../HTMLEmailTemplate/WotmedEmailTemplate/RequestforProposalHeader.html');
$message .= file_get_contents('../../HTMLEmailTemplate/WotmedEmailTemplate/ClientSpeechBodyHeader.html');

$message .= 'Name: ' . $name . "<BR>" . 'Position: ' . $speakerPosition . "<BR>" . 'Email: ' . $email . "<BR>" . 'Phone: ' . $phone . "<BR>" . 'Country: ' . $speakerCountry . "<BR>" . 'Speech Title: ' . $speechTitle  . "<BR>";
$message .= 'Speech Subject: ' . $speechSubject . "<BR>" . 'Is Speaker Contactable? ' . $isSpeakerContactable . "<BR>" . 'Speech SEO Keywords: ' . $speechSEOKeywords  . "<BR>";
$message .= 'Speech: ' . $speechTranscript . "<BR>";
$message .= 'Speech Short Blurb: ' . $speakerShortPhraseToRemember . "<BR>";

$message .= 'Practice or Business Name: ' . $practiceOrBusinessName . "<BR>";
$message .= 'Practice or Business Speciality: ' . $practiceOrBusinessSpeciality . "<BR>";
$message .= 'Practice or Business Phone: ' . $phone . "<BR>";
$message .= 'Practice or Business Address: ' . $practiceOrBusinessAddress . "<BR>";
$message .= 'Practice or Business Website: ' . $practiceOrBusinessIndependentWebsiteHyperlink . "<BR>";
$message .= 'Practice or Business Marketing Blurb: ' . $practiceOrBusinessMarketingBlurb . "<BR>";

$message .= 'Speaker Photo: ' . $photoFileName  . "<BR>";
$message .= 'Practice or Business Logo: ' . $practiceOrBusinessLogoFileName . "<BR>";
$message .= 'Practice or Business Marketing Image: ' . $practiceOrBusinessMarketingImage . "<BR>";
$message .= 'Speech Video Thumbnail: ' . $practiceOrBusinessThumbImage . "<BR>";
$message .= 'Speech Video Filename: ' . $videoFileName . "<BR>";

$message .= 'Uploaded Speech Video is stored here: http://platform.wotmed.com/ImageUpload/uploads/clientspeeches/' . $videoFileName . "<BR><BR>";
$message .= 'Video Technical Information: ' . $videoTechnicalInformation;

$message .= 'Publish article under this hyperlink: ' . $articleHyperlink . "<BR>";

$message .= 'Uploaded Speech Images:' . "<BR>";
$message .= file_get_contents('../../HTMLEmailTemplate/WotmedEmailTemplate/ClientSpeechUploaded.html');

$message .= file_get_contents('../../HTMLEmailTemplate/WotmedEmailTemplate/RequestforProposalBodyFooter.html');

$message .= file_get_contents('../../HTMLEmailTemplate/WotmedEmailTemplate/RequestForProposalFooter.html');





if(mail($address, $subject, $message, $headers))
{

	include ("reg-clientSpeechSuccess.php");

}


// send the practitioner themselves an email thanking them for submitting an article

$address2 = $email;	// set the email address to the practitioners email now

// $address2 = "practitionerarticlesubmissions@wotmed.com";

//	$address = "clarketowson@wotmed.com";

$headers2 = 'From: clientspeechsubmissions@wotmed.com' . "\r\n" .
    'Reply-To: clientspeechsubmissions@wotmed.com' . "\r\n" .
    'X-Mailer: PHP/' . phpversion();
$headers2 .= "MIME-Version: 1.0\r\n";
$headers2 .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

$subject2 = 'Wotmed - Thank you for submitting a video speech ' . $name;

$message2 = file_get_contents('../../HTMLEmailTemplate/WotmedEmailTemplate/RequestforProposalHeaderClient.html');
$message2 .= file_get_contents('../../HTMLEmailTemplate/WotmedEmailTemplate/ClientSpeechBodyHeaderThankYou.html');

$message2 .= "<BR>" . 'Dear ' . $name . ',' . "<BR><BR>" . 'Thank you for submitting your video speech.  A Wotmed representative will contact you shortly prior to publication.' . "<BR><BR>";
$message2 .= 'If you have not already done so - Please sign up for a Wotmed Practitioner or Surgery Facilitator account now by going here: http://platform.wotmed.com/reg-practitioner.php or here: http://platform.wotmed.com/reg-facilitator.php';
$message2 .= ' so that you can connect with Wotmed Practitioners and Surgery Facilitators globally';
$message2 .= "<BR><BR>" . 'Kind Regards,' . "<BR><BR>" . 'The Wotmed Team' . "<BR><BR>";

$message2 .= 'Your video speech submission details:' . "<BR><BR>";

$message2 .= 'Name: ' . $name . "<BR>" . 'Position: ' . $speakerPosition . "<BR>" . 'Email: ' . $email . "<BR>" . 'Phone: ' . $phone . "<BR>" . 'Country: ' . $speakerCountry . "<BR>" . 'Speech Title: ' . $speechTitle  . "<BR>";
$message2 .= 'Speech Subject: ' . $speechSubject . "<BR>" . 'Is Speaker Contactable? ' . $isSpeakerContactable . "<BR>" . 'Speech SEO Keywords: ' . $speechSEOKeywords  . "<BR>";
$message2 .= 'Speech: ' . $speechTranscript . "<BR>";
$message2 .= 'Speech Short Blurb: ' . $speakerShortPhraseToRemember . "<BR>";
$message2 .= 'Practice or Business Name: ' . $practiceOrBusinessName . "<BR>";
$message2 .= 'Practice or Business Speciality: ' . $practiceOrBusinessSpeciality . "<BR>";
$message2 .= 'Practice or Business Phone: ' . $phone . "<BR>";
$message2 .= 'Practice or Business Address: ' . $practiceOrBusinessAddress . "<BR>";
$message2 .= 'Practice or Business Website: ' . $practiceOrBusinessIndependentWebsiteHyperlink . "<BR>";
$message2 .= 'Practice or Business Marketing Blurb: ' . $practiceOrBusinessMarketingBlurb . "<BR>";

$message2 .= 'Your Photo: ' . $photoFileName  . "<BR>";
$message2 .= 'Your Practice or Business Logo: ' . $practiceOrBusinessLogoFileName . "<BR>";
$message2 .= 'Your Practice or Business Marketing Image: ' . $practiceOrBusinessMarketingImage . "<BR>";
$message2 .= 'Your Speech Video Thumbnail: ' . $practiceOrBusinessThumbImage . "<BR><BR>";

$message2 .= 'Your uploaded speech and images have been saved on the Wotmed server and will be verified by a member of the Wotmed Team shortly prior to your speech going live on the site' . "<BR><BR>";

$message2 .= 'Uploaded Speech Images:' . "<BR>";
$message2 .= file_get_contents('../../HTMLEmailTemplate/WotmedEmailTemplate/ClientSpeechUploaded.html');

$message2 .= file_get_contents('../../HTMLEmailTemplate/WotmedEmailTemplate/RequestforProposalBodyFooterClient.html');

$message2 .= file_get_contents('../../HTMLEmailTemplate/WotmedEmailTemplate/RequestForProposalFooterClient.html');

if(mail($address2, $subject2, $message2, $headers2)) 
{

	// Email has sent successfully, echo a success page.

//	echo "<fieldset>";
//	echo "<div id='success_page'>";
//	echo "<h1>Your request for proposal has been submitted successfully</h1>";
//	echo "<p>Thank you <strong>$name</strong>, you have successfully submitted your request for proposal.  A Wotmed team member will be in touch with you shortly</p>";
//	echo "</div>";
//	echo "</fieldset>";
	
// Update the Wotmed database and add the prospective customers details to the PROSPECTIVECUSTOMER TABLE

//$query="INSERT INTO PROSPECTIVECUSTOMER ('NAME', 'EMAILADDRESS', 'PHONENUMBER', 'QUESTIONS', 'PROCEDUREWANTED', 'COUNTRYPREFERENCE', 'EMAILADDRESS', 'BUDGET', //'FINANCEREQUIRED', 'PROPOSEDDATE') VALUES (  '$name', '$email', '$phone', '$questions', '$procedure', '$countries', '$budget', '$finance', '$date');";

//$query="INSERT INTO PROSPECTIVECUSTOMER ('NAME') VALUES ( '$name')";
		
//		mysqli_query($conn,$query);
	

// now email the client a confirmation email that Wotmed has received their request for proposal
	
// include ('contactClient.php');		

}


else 

{

	echo 'ERROR!';

}




