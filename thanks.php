<?php
	require_once "includes/functions.php";
	if(isset($_POST['pracId'])){
		require_once "includes/connect.php";
		//msgBox($_POST['pracId'].", ".$_POST['reason']);
		session_start();
		$comment=$_POST['reason'];
		$id=$_SESSION['id'];
		$practitionerId=$_POST['pracId'];
		$query="INSERT INTO THANKS (PRACTITIONER_NUMBER,PARTICIPANT_NUMBER,COMMENT,THANKSDATE) VALUES ('$practitionerId','$id','$comment',NOW())";
		//msgBox($query);
		mysqli_query($conn,$query);
		?>
		<script language="javascript"> 
			<?php echo "window.location = 'profile.php'";?>
		</script> <?php
	}
?>
		
		<script language="javascript"> 
			function showThanksForm(){
				// scroll to top
				document.body.style.overflow= "hidden";
				$('html, body').animate({scrollTop:0}, 'fast');

				// before showing the modal window, reset the form incase of previous use.
				$('.success, .error').hide();
				$('form#thanksForm').show();
				/* Reset all the default values in the form fields
				$('#name').val('Your name');
				$('#email').val('Your email address');
				$('#comment').val('Enter your comment or query...');*/

				//show the mask and thanks divs
				$('#mask').show().fadeTo('', 0.7);
				$('div#thanks').fadeIn();
				
				// close the modal window is close div or mask div are clicked.
				$('div#close, div#mask').click(function() {
					$('div#thanks, div#mask').stop().fadeOut('slow');
					document.body.style.overflow= "visible";
				});
			}
		</script>
		<div id="thanks">
			<div id="close">Close</div>
			<div id="thanks_header" class="lfloat">
			<p class="success">Thanks! Your message has been sent.</p>

			<div class="lfloat" style="width:320px;">
				<div style='width:100%;background-color:#325199;'>
					<img height="30px" src="images/WotMedLogoMedium.jpg">
				</div>
			  <form action="thanks.php" method="post" name="thanksForm" id="thanksForm">
				<div style='width:300px;'>
					<div>
						<input type='hidden' id='pracId' name='pracId' value='' />
						Would you like to say more about why you have Thanked this practitioner?<br>
						<textarea id='reason' name='reason' style='width:100%'></textarea>
						<div style='width:50%;float:left'>
							<input type='button' value='Another time' />
						</div>
						<div style='width:50%;float:left'>
							<input type='submit' value='Submit' />
						</div>
					</div>
				</div>
			  </form>
			</div>
			</div>
		</div>
		
		
<?php
		if(isset($_GET['thanks']))
		{
			echo "
			<script language='javaScript'>
				$(function() {
					$('input#pracId').val('{$row['PRACTITIONER_NUMBER']}');	
					showThanksForm();
				});
			</script>
			";
		}
?>
