<?php
	if(isset($_POST['reason'])){
		session_start();
		include "includes/connect.php";
		include "includes/functions.php";
		$comment=$_POST['reason'];
		$status=$_POST['status'];
		$id=$_SESSION['id'];
		$practitionerId=$_POST['pracId'];
		$query="UPDATE PATIENTPRACTITIONERRELATIONSHIP SET RECOMMENDED='1', COMMENT='$comment', RECOMMENDDATE=NOW()";
		if($status=='true'){
			$query.=", RECOMMENDEDAS='1'";
		}
		if(isset($_POST['chBoxContact'])){
			$query.=", FUTURECONTACT='1'";
		}
		$query.=" WHERE PARTICIPANT_NUMBER='$id' AND PRACTITIONER_NUMBER='$practitionerId'";
		mysqli_query($conn,$query);
		?>
		<script language="javascript"> 
			<?php echo "window.location = 'profile.php'";?>
		</script> <?php
	}
?>
		<script type="text/javascript" src="<?php echo $path; ?>classes/recommend.js"></script>
	
			<div id="recommend">
				<div id="close">Close</div>
				<div id="recommend_header" class="lfloat">
				<p class="success">Thanks! Your message has been sent.</p>

				<div class="lfloat" style="width:320px;">
					<div style='width:100%;background-color:#325199;'>
						<img height="30px" src="images/WotMedLogoMedium.jpg">
					</div>
				  <form action="recommend.php" method="post" name="recommendForm" id="recommendForm">
				  <div style='width:100%;float:left;'>
					Would you recommend this practitioner to someone in your family?
				</div><br>
				<input type='hidden' id='status' name='status' value='false' />
				<input type='hidden' id='pracId' class='pracId' name='pracId' value='' />
				<div style='width:50%;float:left;'>
					<input type='button' value='No' onclick='notRecommend();' />
				</div>
				<div style='width:50%;float:right;'>
					<input type='button' value='Yes' onclick='recommend();' />
				</div>
				<div>
					<label id='info' name='info' style='width:100%;display:none'>
					Negative feedback will be displayed on this practitioner profile as no feedback.<br><br>
					If there is little to no feedback on a practitioner profile, he or she will eventually not be able to practice any further.<br><br>
					Medical practitioners however, do make mistakes, they are only human within profession, a margin of error should be accounted for and viewed as longer picture.<br><br>
					We are collecting this feedback along with others and will relay this back to the practitioner in time, if nessesary.
					</label><br>
					<div id='botFields' style='display:none'>
						<textarea id='reason' name='reason' style='width:100%'></textarea>
						<br>
						<input type='checkbox' id='chBoxContact' name='chBoxContact' value='true' />I am happy for future pateints to contact me to learn more<br>
						<input type='submit' value='Submit' />
					</div>
				</div>
				 </form>
				</div>
				</div>
			</div>
