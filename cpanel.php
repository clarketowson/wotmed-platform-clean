<?php
	session_start();




	
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");


	include "includes/connect.php";
	include "includes/functions.php";
	include "classes/SimpleImage.php";
	
	$ppFileNameSession="blankSilhouetteMale.png";
	$ppFileName="blankSilhouetteMale.png";

	
	
	$days=array("Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday");
	if(isset($_GET['delete']))
	{	
		$serviceId=$_GET['delete'];
		$id=$rowSession['PARTICIPANT_NUMBER'];
		$query="DELETE FROM SERVICES WHERE PRACTITIONER_NUMBER='" . $id . "' AND MASTERSERVICE_NUMBER='" . $serviceId . "'";
		mysqli_query($conn,$query);
	}
	if(isset($_POST['serviceId']))
	{
		$serviceId=$_POST['serviceId'];
		$newPrice=$_POST['newPrice'];
		$id=$rowSession['PARTICIPANT_NUMBER'];
		$query="UPDATE SERVICES SET PRICE='" . $newPrice . "' WHERE PRACTITIONER_NUMBER='" . $id . "' AND MASTERSERVICE_NUMBER='" . $serviceId . "'";
		mysqli_query($conn,$query);
	}
	if(isset($_POST['price']))
	{
		$service=$_POST['service'];
		$price=$_POST['price'];
		$id=$rowSession['PARTICIPANT_NUMBER'];
		$query="INSERT INTO SERVICES (PRACTITIONER_NUMBER,MASTERSERVICE_NUMBER,PRICE) VALUES ('" . $id . "','" . $service . "','" . $price . "');";
		if($service!=-1)
			mysqli_query($conn,$query);
	}
	if(!isset($_SESSION['id'])){
		header("location:login.php");
	}
	if(isset($_SESSION['id'])){
		$rowSession=getParticipantDetail($conn,$_SESSION['id']);
		$row=getPractitionerDetail($conn,$_SESSION['id']);
		$servicesList=getServices($conn,$rowSession['PARTICIPANT_NUMBER']);
		$query="SELECT * FROM AVAILABILITY WHERE PRACTITIONER_NUMBER ='" . $rowSession['PARTICIPANT_NUMBER'] . "' ORDER BY TIMESTART";
		$schedules=mysqli_query($conn,$query);
		$query="SELECT COUNT(*) FROM PATIENTPRACTITIONERRELATIONSHIP WHERE PRACTITIONER_NUMBER = '" . $rowSession['PARTICIPANT_NUMBER'] . "'";
		$tempOfPatients=mysqli_query($conn,$query);
		if(mysqli_num_rows($tempOfPatients)!=0)
			$numbOfPatients=mysqli_fetch_array($tempOfPatients);
		else
			$numbOfPatients[]=0;
		//echo "<br><br><BR><BR><BR>".$query;
		$query="SELECT COUNT(*) FROM PATIENTPRACTITIONERRELATIONSHIP WHERE PRACTITIONER_NUMBER = '" . $_SESSION['practitioner_id'] . "' AND RECOMMENDEDAS='1'";
		$tempOfRecommend=mysqli_query($conn,$query);
		if(mysqli_num_rows($tempOfRecommend)!=0)
			$numbOfRecommend=mysqli_fetch_array($tempOfRecommend);
		else
			$numbOfRecommend[]=0;
		$passport="";
		
		if($row['B_COUNTRY_NUMBER']!=$rowSession['COUNTRY_NUMBER'])
			$passport="<img src='images/passport_icon.png' height='20px' align='left'/>";
		$subject3 = (isset($_GET['id']))?"This practitioner is":"You are";



	}


	
		
//	if($rowSession['PROFILEPHOTO']==""){
//		$ppFileNameSession="default_logo.jpg";
//	}else{
//		$ppFileNameSession=$rowSession['PROFILEPHOTO'];
//	}


	// if the logged in user is a surgery facilitator then we want to show the profile photo at left as the business logo
// otherwise the user is a practitioner and we show the profile photo on the left as the practitioner photo

if($row['ISFACILITATOR'] == 1)
{
	if($row['PRACTITIONER_BUSINESSLOGO']=="")
	{
			$ppFileNameSession="blankSilhouetteMale.png";
	}else
	
		{
			$ppFileNameSession=$row['PRACTITIONER_BUSINESSLOGO'];
		}
		
	}

else
{
	if($row['PRACTITIONER_BUSINESSLOGO']!="")
	{
		$ppFileNameSession=$row['PRACTITIONER_BUSINESSLOGO'];
		$ppFileName=$row['PRACTITIONER_BUSINESSLOGO'];
	}
	if($rowSession['PROFILEPHOTO']!="")
	{
		$ppFileNameSession=$rowSession['PROFILEPHOTO'];
	}
	
}





?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<link href="<?php echo $path; ?>style/p_style.css" rel="stylesheet"></link>
		<script type="text/javascript" src="<?php echo $path; ?>classes/jquery.js"></script>
		<script src="classes/hover.js" type="text/javascript"></script>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title><?php echo $row['PRACTITIONER_BUSINESSNAME']; ?>'s Wotmed.com Profile</title>
		<link href="style/apple.css" rel="stylesheet" type="text/css" />
		<script language="javascript"> 
		function updateServiceList(categoryNumber,subCategoryNumber)
		{
			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp=new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp.onreadystatechange=function()
			{
				if (xmlhttp.readyState==4 && xmlhttp.status==200)
				{
					document.getElementById("service").innerHTML=xmlhttp.responseText;
				}
			}
			xmlhttp.open("GET","ajax/updateServiceList.php?catNumber="+categoryNumber+"&subCatNumber="+subCategoryNumber,true);
			xmlhttp.send();
		}
		function updateAvailableSchedule(id,order)
		{
			
			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp=new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp.onreadystatechange=function()
			{
				if (xmlhttp.readyState==4 && xmlhttp.status==200)
				{
					document.getElementById("scheduleTable").innerHTML=xmlhttp.responseText;
				}
			}
			xmlhttp.open("GET","ajax/updateAvailableSchedule.php?id="+id+"&order="+order,true);
			xmlhttp.send();
		}
		function toggle(selected_div) {
			var ele = document.getElementById(selected_div);
			if(ele.style.display == "block") {
					ele.style.display = "none";
			}
			else {
				ele.style.display = "block";
			}
		}
		function back()
		{
			window.location = "practitioner_profile.php"
		}
		function showVisaReq(origURL,destURL){
			window.open(origURL,'_blank');
			window.open(destURL,'_blank');
		}
		</script>
	</head>

	<body>
		<?php include $path."includes/p_header.php"; ?>
		<?php
			$backLink = "
				<a href='practitioner_profile.php'>See your public facing Wotmed Practitioner Profile (exactly how your patients will see it)</a> <a href='practitioner_profile.php'><img src='images/BackToPublicProfile.png' title='Click here to view your Wotmed Practitioner Profile (exactly how your patients will see it)' /> </a>
				
				
			
				
			";
			include $path."cpanelHeader.php"; 
		?>
		<!--
		<div class='lfloat'>
			<table width="548" border="0">
			<!--
			  <tr>
				<td width="293" rowspan="10"><div class="PractitionerPhotoWrapper">
				  <p class="PractitionerHeadings"><img src="<?php echo "photos/originals/" . $ppFileNameSession; ?>" alt="alexPhoto" width="167" height="141" align="top" /></p>
				</div></td>
				<td height="5">&nbsp;</td>
				<td height="5" class="hyperlinks">&nbsp;</td>
			  </tr>
			  
			  <tr>
				<td height="5">&nbsp;</td>
				<td height="5" class="hyperlinks">&nbsp;</td>
			  </tr>
			  <tr>
				<td height="5">&nbsp;</td>
				<td height="5" class="hyperlinks">&nbsp;</td>
			  </tr>
			  <tr>
				<td height="5">&nbsp;</td>
				<td height="5" class="hyperlinks">&nbsp;</td>
			  </tr>
			  <tr>
				<td width="201" height="5" bgcolor="#FFFFFF"><span class="PractitionerMainText">Patient <em>Thanks</em></span></td>
				<td width="40" height="5" bgcolor="#FFFFFF" class="hyperlinks"><?php echo getNumberOfThanks($conn,$_SESSION['id']); ?></td>
			  </tr>
			  <tr>
				<td height="5" bgcolor="#FFFFFF" class="PractitionerMainText">Patient Recommendations</td>
				<td height="5" bgcolor="#FFFFFF" class="hyperlinks"><?php echo $numbOfRecommend[0]; ?></td>
			  </tr>
			  <tr>
				<td height="5" bgcolor="#FFFFFF"><span class="PractitionerMainText">Patient Connections</span></td>
				<td height="5" bgcolor="#FFFFFF"><span class="hyperlinks"><?php echo $numbOfPatients[0]; ?></span></td>
			  </tr>
			  <tr>
				<td height="5">&nbsp;</td>
				<td height="5">&nbsp;</td>
			  </tr>
			  <tr>
				<td height="5" bgcolor="#FFFFFF"><span class="PractitionerMainText">Practitioner Connections</span></td>
				<td height="5" bgcolor="#FFFFFF"><span class="hyperlinks">-</span></td>
			  </tr>
			  <tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			</table>
			<table width="536" border="0">
			  <tr>
				<td width="203" valign="top"><div class="PractitionerNameWrapper">
	
				</div></td>
				<td width="323" valign="bottom" class="PractitionerMainText"><?php //echo $row['PRACTITIONER_BUSINESSADDRESS'] . ", " . $row['PRACTITIONER_BUSINESSSUBURB'] . ", " . $row['PRACTITIONER_BUSINESSSUBURB']; ?></a>
			  </tr>
			  <tr>
				<td valign="top"><div class="PractitionerHyperlinkWrapper">
				  <p class="hyperlinks"><?php echo $row['WEBSITEURL']; ?></p>
				</div></td>
				<td class="hyperlinks"><span class="PractitionerMainText"><?php //echo $row['PRACTITIONER_BUSINESSNUMBER']; ?></span></td>
			  </tr>
			  <tr>
				<td colspan="2"><span class="PractitionerMainText"><?php //echo $row['EDUCATION']; ?></td>
			  </tr>
			  <tr>
				<td colspan="2"><a href="qualifications.php" class="PractitionerMainText"><?php echo $row['QUALIFICATIONS']; ?></a></td>
			  </tr>
			</table>
			<table width="100%" border="0">
			  <tr>
				<td width="273" valign="top"><div class="PractitionerPayPalWrapper">
				  <p><?php //include "paypalSubscribeFee.php";?></p>
				</div></td>
				<td width="259"></td>
			  </tr>
			</table>
		</div>
		-->
		<!--
		<div class="PractitionerSubHeadingWrapper">
		  <p class="PractitionerSubHeadings">Serving <?php echo $row['PRACTITIONER_BUSINESSSUBURB']; ?>, VIC (Victoria)</p>
		</div>
		<div class="PractitionerBodyWrapper">
		  <p class="PractitionerBody"><span class="PractitionerMainText"><span class="PractitionerMainText">Dr. Alex Yusupov is your orthodontic specialist offering Herbst appliances, Incognito, Invisalign, and braces for children, teens, and adults.<br />
			<br />
			Since 1993 we have been your greater Melbourne, VIC orthodontist specialist of choice. We help you achieve beautiful smiles and prepare you to keep them for a lifetime. Being a specialist clinic means that our orthodontist, Dr. Alex Yusupov, is often called on to do orthodontic work others cannot provide. He fills this role by using the best available oral health technology and skills available. </span></span></p>
		  <p class="PractitionerBody"><span class="PractitionerMainText"><span class="PractitionerMainText">Our qualified staff’s support allows Dr. Yusupov to provide the best quality orthodontic treatments. They are all fully trained and accredited in the services they provide. You will love both the time spent and the smile you achieve with us.</span></span><span class="PractitionerMainText"></span></p>
		</div>-->
		<p>&nbsp;</p>
		<div class="PractitionerPublicFacingTabWrapper">
		  <p><span class="PractitionerSubHeadings">
		  <img src="images/publicfacing.png" height="81" /></span>
		  <span class="PractitionerSubHeadings" style=' font-family: "Futura Medium";font-size:20px;font-weight:bold;'>Your public facing profile information</span></p>
		<p class="PractitionerMainText">The functions below are setup so that you can add, update and delete information on your public facing profile page. You can preview all profile information before you make it public.  </p>
		<p class="PractitionerMainText"><strong>Please note:</strong> You may need to hold down the shift or command key and then reload the browser page if you find that the images are not refreshing correcty in each section of this website update panel.</p>

            <p class="PractitionerMainText"><strong>Profile Setup Wizard:</strong><a href="signupWizard/index.htm"></a> Please click here to view a quick overview of how to setup your Wotmed Profile</p>

            <p class="PractitionerMainText"><strong></strong> Please note that it takes 24 hours before your profile will be visible by Patients via Wotmed Search.</p>

            <p class="PractitionerMainText">&nbsp;</p>

            <div class="stdWrapper narrowWrapper" onmouseover="this.style.background='white';"onmouseout="this.style.background='#F0F0F0';" style="cursor: pointer" onclick="location.href='WotmedConsultations.php';">
                <p><img src="WotmedPaidConsultationsGraphicCP.jpg" alt="Wotmed Paid Consultations" width="210" height="225" border="0" /></a></span> <a href="WotmedConsultations.php" class="hyperlinks">Get paid to consult with Wotmed Patients</a></p>

            </div>


        
        <div class="stdWrapper narrowWrapper" onmouseover="this.style.background='white';"onmouseout="this.style.background='#F0F0F0';" style="cursor: pointer" onclick="location.href='UpdateMyProfilePhotoPractitioner.php';">
				<p><span class="PractitionerBody"><a href="UpdateMyProfilePhotoPractitioner.php"><img src="images/blankSilhouetteMale.png" alt="a3" width="72" height="72" /></a></span> <a href="UpdateMyProfilePhotoPractitioner.php" class="hyperlinks">My Profile Photo</a></p>
				<p><span class="PractitionerMainText">
				Upload your profile photo</span></p>
			  </div>
        
			<div class="stdWrapper narrowWrapper" onmouseover="this.style.background='white';"onmouseout="this.style.background='#F0F0F0';" style="cursor: pointer" onclick="location.href='mySchedule.php';">
				<p><span class="PractitionerBody"><a href="mySchedule.php"><img src="images/schedule-icon.png" alt="a3" width="72" height="72" /></a></span> <a href="mySchedule.php" class="hyperlinks">My Schedule</a></p>
				<p><span class="PractitionerMainText">
				Create, update and delete your patient scheduling information</span></p>
			  </div>
              
			  <div class="stdWrapper narrowWrapper" onmouseover="this.style.background='white';"onmouseout="this.style.background='#F0F0F0';" style="cursor: pointer" onclick="location.href='myServices.php';">
				<p><span class="PractitionerBody"><a href="myServices.php"><img src="images/Service-icon.png" alt="a4" width="72" height="72" /></a></span><a href="myServices.php" class="hyperlinks">My Services &amp; Price Lists</a></p>
				<p><span class="PractitionerBody"><span class="PractitionerMainText">
				Add a list of the professional services that you provide to your patients together with a corresponding list of prices</span></span></p>
			  </div>
			
              <div class="stdWrapper narrowWrapper" onmouseover="this.style.background='white';"onmouseout="this.style.background='#F0F0F0';" style="cursor: pointer" onclick="location.href='myAdvertisements.php';">
				<p><span class="PractitionerBody"><a href="myAdvertisements.php"><img src="images/Advertising-icon.png" alt="a4" width="72" height="72" /></a></span><a href="myAdvertisements.php" class="hyperlinks">My Advertisements</a></p>
				<p><span class="PractitionerBody"><span class="PractitionerMainText">
				Advertisements can be shown here which can be shown on your profile page</span></span></p>
			  </div>
			
            
            
            
            	  <div class="stdWrapper narrowWrapper" onmouseover="this.style.background='white';"onmouseout="this.style.background='#F0F0F0';" style="cursor: pointer" onclick="location.href='myPromotions.php';">
				<p><span class="PractitionerBody"><a href="myPromotions.php"><img src="images/promotions.png" alt="sale" width="72" height="72" /></a></span><a href="myPromotions.php" class="hyperlinks">My Promotions</a></p>
				<p><span class="PractitionerMainText">Promotions can be added here which will be shown prominantly on your profile page</span></p>
			  </div>
			  <div class="stdWrapper narrowWrapper" onmouseover="this.style.background='white';"onmouseout="this.style.background='#F0F0F0';" style="cursor: pointer" onclick="location.href='myAbout.php';">
				<p><span class="PractitionerBody"><a href="myAbout.php"><img src="images/Actions-help-about-icon.png" alt="a2" width="72" height="72" /></a></span><a href="myAbout.php" class="hyperlinks">About Me / My Practice</a></p>
				<p><span class="PractitionerBody"><span class="PractitionerMainText">Update details about your practice hours, google street view maps, practice phone number, practice website link, photographs, videos, contact details, social networking links and PDF brochures</span></span></p>
			  </div>
			  <div class="stdWrapper narrowWrapper" onmouseover="this.style.background='white';"onmouseout="this.style.background='#F0F0F0';" style="cursor: pointer" onclick="location.href='myBlog.php';">
				<p><span class="PractitionerBody"><a href="myBlog.php"><img src="images/blog-icon.png" alt="a10" width="72" height="72" /></a></span><a href="myBlog.php" class="hyperlinks">My Blog</a></p>
				<p><span class="PractitionerBody"><span class="PractitionerMainText">Update your Professional Blog here</span></span></p>
			  </div>
			  <div class="stdWrapper narrowWrapper" onmouseover="this.style.background='white';"onmouseout="this.style.background='#F0F0F0';" style="cursor: pointer" onclick="location.href='myClientCaseStudies.php';">
				<p class="copyrightText"><a href="myClientCaseStudies.php"><img src="images/case-icon.png" alt="" width="72" height="72" /></a><a href="myClientCaseStudies.php" class="copyrightText"><span class="hyperlinks">My Client Case Studies</span></a></p>
				<p class="copyrightText">Client case studies can be added here which can be shown prominantly on your profile page</p>
			  </div>
			  <div class="stdWrapper narrowWrapper" onmouseover="this.style.background='white';"onmouseout="this.style.background='#F0F0F0';" style="cursor: pointer" onclick="location.href='myFinance.php';">
				<p class="copyrightText"><a href="myFinance.php"><img src="images/Money-icon.png" alt="" width="72" height="72" /></a><a href="myFinance.php" class="hyperlinks">Financing</a></p>
				<p class="copyrightText">Add financing that you offer to to your patients here</p>
			  </div>
			  <div class="stdWrapper narrowWrapper" onmouseover="this.style.background='white';"onmouseout="this.style.background='#F0F0F0';" style="cursor: pointer" onclick="location.href='myWebsites.php';">
				<p class="copyrightText"><a href="myWebsites.php"><img src="images/websites-folder-icon.png" alt="" width="72" height="72" /></a><a href="myWebsites.php" class="hyperlinks">Independent Website Details</a></p>
				<p class="copyrightText">Update details about your independent professional website here</p>
			  </div>
			  <div class="stdWrapper narrowWrapper" onmouseover="this.style.background='white';"onmouseout="this.style.background='#F0F0F0';" style="cursor: pointer" onclick="location.href='myPress.php';">
				<p class="copyrightText"><a href="myPress.php"><img src="images/PressIndependent.png" alt="" width="72" height="72" /></a><a href="myPress.php" class="hyperlinks">Independent Press Articles</a></p>
				<p class="copyrightText">Add information here about any independent press articles that have featured you or your practice</p>
			  </div>
			  <div class="stdWrapper narrowWrapper" onmouseover="this.style.background='white';"onmouseout="this.style.background='#F0F0F0';" style="cursor: pointer" onclick="location.href='myNews.php';">
				<p class="copyrightText"><a href="myNews.php"><img src="images/PractitionerNews.png" alt="" width="72" height="72" /></a><a href="myNews.php" class="hyperlinks">My News Articles</a></p>
				<p class="copyrightText">Add News Articles to your profile and keep your patients up to date with information about you or your practice</p>
			  </div>
		
			  <div class="stdWrapper narrowWrapper" onmouseover="this.style.background='white';"onmouseout="this.style.background='#F0F0F0';" style="cursor: pointer" onclick="location.href='myLogo.php';">
				<p class="copyrightText"><a href="myLogo.php"><img src="images/default_logo.jpg" alt="" width="99" height="59" /></a><a href="myLogo.php" class="hyperlinks">My Logos</a></p>
				<p class="copyrightText">Add your practice logos here</p>
			  </div>
			  <div class="stdWrapper narrowWrapper" onmouseover="this.style.background='white';"onmouseout="this.style.background='#F0F0F0';" style="cursor: pointer" onclick="location.href='mySlogan.php';">
				<p class="copyrightText"><a href="mySlogan.php"><img src="images/stock-vector-service-plus-quality-retro-ad-art-banner-80117605.jpg" alt="" width="85" height="71" /></a><a href="mySlogan.php" class="hyperlinks">My Slogan</a></p>
				<p class="copyrightText">Add your practice slogan here</p>
			  </div>
			  <div class="stdWrapper narrowWrapper" onmouseover="this.style.background='white';"onmouseout="this.style.background='#F0F0F0';" style="cursor: pointer" onclick="location.href='myPhilosophy.php';">
				<p><a href="myPhilosophy.php"><img src="images/stock-photo-stack-of-open-books-with-apple-isolated-on-white-108879077.jpg" alt="" width="114" height="86" /></a><a href="myPhilosophy.php" class="hyperlinks">My Philosophy</a></p>
				<p class="PractitionerMainText">Add your practice philosophy here</p>
			  </div>
			  <div class="stdWrapper narrowWrapper" onmouseover="this.style.background='white';"onmouseout="this.style.background='#F0F0F0';" style="cursor: pointer" onclick="location.href='myCustomerCare.php';">
				<p><a href="myCustomerCare.php"><img src="images/User-Group-icon.png" alt="" width="72" height="72" /></a><a href="myCustomerCare.php" class="hyperlinks">Customer Care Policy</a></p>
				<p class="PractitionerMainText">Add your practice customer care policy </p>
			  </div>
			  <div class="stdWrapper narrowWrapper" onmouseover="this.style.background='white';"onmouseout="this.style.background='#F0F0F0';" style="cursor: pointer" onclick="location.href='myTeam.php';">
				<p><a href="myTeam.php"><img src="images/Groups-Meeting-Dark-icon.png" alt="" width="72" height="72" /></a><a href="myTeam.php" class="hyperlinks">My Team</a></p>
				<p class="PractitionerMainText">Add information about your team of professionals here</p>
			  </div>
			  <div class="stdWrapper narrowWrapper" onmouseover="this.style.background='white';"onmouseout="this.style.background='#F0F0F0';" style="cursor: pointer" onclick="location.href='myTechnology.php';">
				<p><a href="myTechnology.php"><img src="images/Motherboard-icon.png" alt="" width="72" height="72" /></a><a href="myTechnology.php" class="hyperlinks">My Technology</a></p>
				<p class="PractitionerMainText">Add information here about the technology that you use in your practice </p>
			  </div>
			  <div class="stdWrapper narrowWrapper" onmouseover="this.style.background='white';"onmouseout="this.style.background='#F0F0F0';" style="cursor: pointer" onclick="location.href='myFAQ.php';"> <a href="myFAQ.php"><img src="images/help-icon.png" alt="" width="72" height="72" /></a><a href="myFAQ.php" class="hyperlinks">My FAQ's</a>
				<p class="PractitionerMainText">Add a list of FAQ's to your profile</p>
			  </div>
			  <div class="stdWrapper narrowWrapper" onmouseover="this.style.background='white';"onmouseout="this.style.background='#F0F0F0';" style="cursor: pointer" onclick="location.href='myGlossary.php';">
				<p><a href="myGlossary.php"><img src="images/glossary-icon.png" alt="" width="72" height="72" /></a><a href="myGlossary.php" class="hyperlinks">Independent Glossary</a></p>
				<p class="PractitionerMainText">Update your glossary here</p>
			  </div>
			  <div class="stdWrapper narrowWrapper" onmouseover="this.style.background='white';"onmouseout="this.style.background='#F0F0F0';" style="cursor: pointer" onclick="location.href='myPrivacy.php';">
				<p><a href="myPrivacy.php"><img src="images/Keys-icon.png" alt="" width="72" height="72" /></a><a href="myPrivacy.php" class="hyperlinks">My Privacy Statement</a></p>
				<p class="PractitionerMainText">Update your Practice Privacy Statement here</p>
			  </div>
			  <div class="stdWrapper narrowWrapper" onmouseover="this.style.background='white';"onmouseout="this.style.background='#F0F0F0';" style="cursor: pointer" onclick="location.href='myCopyright.php';">
				<p><a href="myCopyright.php"><img src="images/copyright-icon.png" alt="" width="72" height="72" /></a><a href="myCopyright.php" class="hyperlinks">My Copyright Statement</a></p>
				<p class="PractitionerMainText">Update your Copyright Statement here</p>
			  </div>
			  <div class="stdWrapper narrowWrapper" onmouseover="this.style.background='white';"onmouseout="this.style.background='#F0F0F0';" style="cursor: pointer" onclick="location.href='myDisclaimer.php';">
				<p><a href="myDisclaimer.php"><img src="images/justice-balance-icon.png" alt="" width="72" height="72" /></a><a href="myDisclaimer.php" class="hyperlinks">My Disclaimer Statement</a></p>
				<p class="PractitionerMainText">Update your Disclaimer Statement here</p>
			  </div>
			  <div class="stdWrapper narrowWrapper" onmouseover="this.style.background='white';"onmouseout="this.style.background='#F0F0F0';" style="cursor: pointer" onclick="location.href='myTerms.php';">
				<p><a href="myTerms.php"><img src="images/desktop-icon.png" alt="" width="72" height="72" /></a><a href="myTerms.php" class="hyperlinks">Terms and Conditions</a></p>
				<p class="PractitionerMainText">Add your terms and conditions here</p>
			  </div>
			  <div class="stdWrapper narrowWrapper" onmouseover="this.style.background='white';"onmouseout="this.style.background='#F0F0F0';" style="cursor: pointer" onclick="location.href='myAcceptedInsurance.php';">
				<p><a href="myAcceptedInsurance.php"><img src="images/desktop-icon.png" alt="" width="72" height="72" /></a><a href="myAcceptedInsurance.php" class="hyperlinks">Accepted Insurance</a></p>
				<p class="PractitionerMainText">Add insurance you accept here</p>
			  </div>
              
              <div class="stdWrapper narrowWrapper" onmouseover="this.style.background='white';"onmouseout="this.style.background='#F0F0F0';" style="cursor: pointer" onclick="location.href='EmailYourPatients.php';">
				<p><img src="images/ContactsImportIcon.png" alt="" width="72" height="72" border="0" /></p><a href="EmailYourPatients.php" class="hyperlinks">Bulk Import and Bulk Email all your Patients</a></p>
				<p class="PractitionerMainText">Bulk Import and Bulk Email all your Patients.  </p>
				<p class="PractitionerMainText">Send an automated email to all your past and current patients encouraging them to join Wotmed to recommend and review you</p>
			  </div>
              
          <div class="stdWrapper narrowWrapper" onmouseover="this.style.background='white';"onmouseout="this.style.background='#F0F0F0';" style="cursor: pointer" onclick="location.href='EmailYourPeers.php';">
			<p><img src="images/ContactsImportIcon.png" alt="" width="72" height="72" border="0" /></p><a href="EmailYourPeers.php" class="hyperlinks">Bulk Import and Bulk Email all your Peers</a></p>
			<p class="PractitionerMainText">Bulk Import and Bulk Email all your Peers.  </p>
				<p class="PractitionerMainText">Send an automated email to all your peers encouraging them to join Wotmed and establish a relationship with you</p>
		    </div>
            
          <div class="stdWrapper narrowWrapper" onmouseover="this.style.background='white';"onmouseout="this.style.background='#F0F0F0';" style="cursor: pointer" onclick="location.href='myWotmedFees.php';">
                <p><img src="WotmedFees.jpg" alt="" width="80" height="63" border="0" /><a href="myWotmedFees.php">Wotmed Schedule of Fees</a></p>
                <p class="PractitionerMainText">&nbsp;</p>
                <p class="PractitionerMainText">Please click here to see a schedule of Wotmed fees and charges </p>
            </div>

            <div class="stdWrapper narrowWrapper" onmouseover="this.style.background='white';"onmouseout="this.style.background='#F0F0F0';" style="cursor: pointer" onclick="location.href='';">
                <p><img src="DeleteProfile.jpg" alt="" width="70" height="70" border="0" /><a href="DeleteMyWotmedProfile.php">Delete my Wotmed Profile</a></p>
                <p class="PractitionerMainText">&nbsp;</p>
                <p class="PractitionerMainText">Click here to delete your Wotmed Profile </p>
            </div>
		
		</div>
		<!--div class="PractitionerPublicFacingTabWrapper">
		  <p><span class="PractitionerSubHeadings" style='font-size:20px;font-weight:bold;'><img src="images/privacy.jpg" width="91" height="59" />Your strictly private information</span></p>
		  <p class="PractitionerMainText">The functions below are setup so that you can add, update and delete strictly private information. This information is provided by you simply to make it easier for you to work on the wotmed.com platform.</p>
		  <p class="PractitionerMainText">&nbsp;</p>
			<div class="stdWrapper narrowWrapper" onmouseover="this.style.background='white';"onmouseout="this.style.background='#F0F0F0';" style="cursor: pointer" onclick="location.href='myBanking.php';">
			<p><span class="PractitionerBody"><a href="myBanking.php"><img src="images/Bank-icon.png" alt="a9" width="72" height="72" /></a></span><a href="myBanking.php" class="hyperlinks">My Banking and Paypal Details</a></p>
			<p><span class="PractitionerBody"><span class="PractitionerMainText">Update details about your personal and business banking </span></span></p>
			</div>
			<div class="stdWrapper narrowWrapper" onmouseover="this.style.background='white';"onmouseout="this.style.background='#F0F0F0';" style="cursor: pointer" onclick="location.href='myInsurance.php';">
			<p><a href="myInsurance.php"><img src="images/insurance-icon.png" alt="" width="65" height="76" /></a> <span class="hyperlinks"><a href="myInsurance.php" class="hyperlinks">My Professional Insurance Details</a></span></p>
			<p class="PractitionerMainText">Update details about your professional insurance details here</p>
			</div>
			<div class="stdWrapper narrowWrapper" onmouseover="this.style.background='white';"onmouseout="this.style.background='#F0F0F0';" style="cursor: pointer;" onclick="location.href='mySales.php';">
			<p><img src="images/salesPhoto.jpg" alt="" width="91" height="93" /><span class="hyperlinks"><a href="myInsurance.php" class="hyperlinks">Your Wotmed Sales</a></span></p>
			<p class="PractitionerMainText">View details about all sales you have made through the Wotmed.com platform</p>
			</div>
		</div-->
		<p class="copyrightText">&nbsp;</p><!--
		<div class="CopyrightTextWrapper">
		  <p class="copyrightText">Copyright (c) Wotmed.com 2012 All rights reserved. </p>
		</div>-->
			<?php include $path."includes/p_footer.php"; ?>
	</body>

</html>
