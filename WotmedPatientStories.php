

<script type="application/ld+json">
{
"@context": "http://schema.org",
"@type": "Service",
"name": "Wotmed Patient Stories",
"description": "Features patient stories about undergoing surgery overseas",
"serviceType": "Story Publication Service",
"url": "http://platform.wotmed.com/WotmedPatientStories.php"
}
}
</script>

<?php
	/*if ($_SERVER['HTTPS'] != "on") {
		$url = "https://". $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
		header("location: $url");
	}  */

	session_start();
	include("includes/connect.php");
	include("includes/functions.php");
	
	if(isset($_SESSION['id']))
	{
		$redirect="profile.php";
		if(isset($_SESSION['practitioner_id']))
			$redirect='practitioner_profile.php';
		redirect($redirect);
	}
	elseif(isset($_POST['email']))
	{
		$inputtext=$_POST['inputtext'];
	$inputpassword=$_POST['inputpassword'];
		login($conn,$inputtext,$inputpassword);
	}
?>



<!DOCTYPE html PUBLIC
"-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
		<title>Wotmed Patient Stories</title>
		<meta http-equiv="Content-Type" content="'text'/html; charset=utf-8" />


    <!-- ADDTHIS BUTTON BEGIN -->
    <script type="text/javascript">
        var addthis_config = {
            pubid: "ra-556a5489192d5a6e"
        }
    </script>

    <script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js"></script>
    <!-- ADDTHIS BUTTON END -->

 <script type="text/javascript">



  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-38343871-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-49954656-1', 'wotmed.com');
		  ga('send', 'pageview');

		</script>

       
		<script type="text/javascript" src="classes/jquery.js"></script>
		<script type="text/javascript" src="classes/hover.js"></script>
		<script src="classes/jquery.validation.js"></script>
		<script src="classes/dateSelectBoxes.js"></script>

    <!-- Wotmed Chat System NOTE - MUST BE KEPT EXACTLY HERE-->
    <script type="text/javascript" src="http://platform.wotmed.com/WotmedChat/livechat/php/app.php?widget-init.js"></script>
    <!-- Wotmed Chat System -->

    <link href="style/i_style.css" rel="stylesheet"></link>

    <link rel="stylesheet" type="text/css" href="signup.css">

    <link rel="stylesheet" href="/proposals/date/css/pikaday.css">

 <!--   <link rel="stylesheet" href="/proposals/date/css/site.css"> -->


 <!--   <style type="text/css">
        <!--
        #PatientBudget {	font-size:14px;
        }
        #PatientCountries {	font-size:14px;
        }
        #ProcedureList {
            font-size:14px;
        }
        #Submit {	background-color:#125c5d;
            -moz-border-radius:1px;
            -webkit-border-radius:1px;
            border-radius:1px;
            display:inline-block;
            cursor:pointer;
            color:#ffffff;
            font-family:arial;
            font-size:22px;
            padding:12px 50px;
            text-decoration:none;
            text-shadow:0px 1px 0px #125c5d;
        }

    </style> -->



    <meta name="description" content="Submit a story to Wotmed Now.  Write for Wotmed.  Audience of Doctors, Dentists and Patients globally.  Wotmed.com – Facilitating Medical Travel">
    <meta name="keywords" content="Wotmed.com, Write Stories, Stories, Submit Patient Stories, Read Stories, Audience of Dentists, Patients and Doctors globally, Facilitating Medical Travel">

    <meta property="og:locale" content="en_US">
    <meta property="og:type" content="website">
    <meta property="og:title" content="Welcome to Wotmed">
    <meta property="og:description" content="Wotmed.com - Packaging Medical Tourism">
    <meta property="og:site_name" content="Wotmed">
    <meta property="og:url" content="http://platform.wotmed.com/WotmedPatientStories.php">
    <meta property="og:image" content="http://platform.wotmed.com/WotmedFacebookLogo2015.png">
        
	</head>
	<body onload="searchUsers()">
		<div class="topBar" >
			<div class="bar_frame">
				<div class="w_logo"><img src="images/WotmedLogoTransparent.png" height="45" alt="wotmedLogo" style='position:relative; left:-30px; padding:0px;padding-top:10px;' /></div>
				<div class="div_login">
					<form id="loginForm" method="post" action="login.php">
					<table>
						<tr>
							<td><label>Email</label></td>
							<td><label>Password</label></td>
						</tr>
						<tr>
							<td width="150"><input type="text" class="inputtext required email" name="email" id="email"/></td>
							<td width="150"><input type="password" class="inputpassword required" name="password" id="password"/></td>
							<td><input type="submit" value="Login"/></td>
						</tr>
						<tr>
							<td><input name="persistent" type="checkbox" style="float:left; margin: 0px; padding: 0px;"/> <label>Keep me logged in</label></td>
							<td><a href="resetPassword.php"><label style="cursor:pointer;text-decoration:underline;">Forget your password?</label></a></td>
                            
                           
						</tr>
                        
					</table>
					</form>
				<div id="errMsg">
				</div>
				</div>
				<div>
					<?php //include "fbConnect.php"; ?>
				</div>
				<div>
					<?php //include "googleConnect.php"; ?>
				</div>
			</div>
		</div>



		<div id="container">


            <h3><a href="#" onclick="return false;" class="about">Wotmed is the medical network that connects Patients with Practitioners and Surgery Facilitators globally</a></h3>
            <p>
                &nbsp;</p>

            <div id="system">


                <article class="item">




                    <div class="content clearfix">


                        <h1><strong>Wotmed Patient Stories</strong></h1>

                        <a href="http://www.addthis.com/bookmark.php?v=250"
                           class="addthis_button"><img
                                src="http://s7.addthis.com/static/btn/v2/lg-share-en.gif"
                                width="125" height="16" border="0" alt="Share" /></a>

                        <p> This section of the site features patient stories about undergoing surgery.</p>
                        <p> It is dedicated to empowering you as a patient to post your stories which other patients can read and benefit from.</p>
                        <p> Your experience may be of great value to other patients around the world who are considering the exact same surgery.</p>
                        <p> Please note that the Wotmed Team moderates all patient story submissions before publishing.</p>
                        <br>

                        <p> You can submit your patient story to the Wotmed Team by

                            <a href="WotmedPatientStorySubmissions.php"><label style="cursor:pointer;text-decoration:underline;">clicking here</label></a>
<br><br>
                       <style>
                        .column-left{ float: left; width: 20%; }
                        .column-right{ float: right; width: 30%; }
                        .column-center{ display: inline-block; width: 50%; }
                       </style>

                        <div class="StoryContainer">
                            <div class="column-center"><b>Story Title</b>
                                <BR> <BR>


                                <BR>

                                <a href='WotmedPatientStories/2015-05-20.php'>A personal recommendation of Dr Daniel Andrews of Hobsons Bay Dental</a>


                            </div>

                            <div class="column-right"><b>Author</b>
                                <BR> <BR>



                                <BR>

                                Clarke Towson

                            </div>

                            <div class="column-left"><b>Date Published</b>
                                <BR> <BR>


                                <BR>

                                Wednesday May 5th 2015
                            </div>

                        </div>

                    </div>










            <p>&nbsp;</p>



        </div>


			<p>&nbsp;</p>
			<p>&nbsp;</p>
			<?php include "includes/p_footer.php"; ?>
