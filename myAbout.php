<?php
	session_start();
	
	header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");


	include "includes/connect.php";
	include "includes/functions.php";
	include "classes/SimpleImage.php";
	if(isset($_SESSION['id'])){
		$rowSession=getParticipantDetail($conn,$_SESSION['id']);
		$row=getPractitionerDetail($conn,$_SESSION['id']);
		$webLink="";
		//msgBox($row['PRACTITIONER_NUMBER']);
		$temp=getDetails($conn,$row['PRACTITIONER_NUMBER'],10);
		if($temp!=null){
			$detailResult= mysqli_fetch_array($temp);
			$webLink.=$detailResult['DETAIL_TITLE'];
		}
	$ppFileNameSession="blankSilhouetteMale.png";
	$ppFileName="blankSilhouetteMale.png";
	
	
	// if the logged in user is a surgery facilitator then we want to show the profile photo at left as the business logo
// otherwise the user is a practitioner and we show the profile photo on the left as the practitioner photo

if($row['ISFACILITATOR'] == 1)
{
	if($row['PRACTITIONER_BUSINESSLOGO']=="")
	{
			$ppFileNameSession="blankSilhouetteMale.png";
	}else
	
		{
			$ppFileNameSession=$row['PRACTITIONER_BUSINESSLOGO'];
		}
		
	}

else
{
	if($row['PRACTITIONER_BUSINESSLOGO']!="")
	{
		$ppFileNameSession=$row['PRACTITIONER_BUSINESSLOGO'];
		$ppFileName=$row['PRACTITIONER_BUSINESSLOGO'];
	}
	if($rowSession['PROFILEPHOTO']!="")
	{
		$ppFileNameSession=$rowSession['PROFILEPHOTO'];
	}
	
}


	
//	if($row['PRACTITIONER_BUSINESSLOGO']!=""){
//		$ppFileNameSession=$row['PRACTITIONER_BUSINESSLOGO'];
//		$ppFileName=$row['PRACTITIONER_BUSINESSLOGO'];
//	}
//	if($rowSession['PROFILEPHOTO']!=""){
//		$ppFileNameSession=$rowSession['PROFILEPHOTO'];
//	}
	
		$query="SELECT COUNT(*) FROM PATIENTPRACTITIONERRELATIONSHIP WHERE PRACTITIONER_NUMBER = '" . $_SESSION['practitioner_id'] . "' AND RECOMMENDEDAS='1'";
		$tempOfRecommend=mysqli_query($conn,$query);
		if(mysqli_num_rows($tempOfRecommend)!=0)
			$numbOfRecommend=mysqli_fetch_array($tempOfRecommend);
		else
			$numbOfRecommend[]=0;
		if(isset($_POST) && $_POST!=NULL)
		{
			//$query="UPDATE PRACTITIONERDETAIL SET DETAIL_TITLE ='{$_POST['WEBSITEURL']}' WHERE PRACTITIONER_NUMBER ={$row['PRACTITIONER_NUMBER']} AND MASTERDETAILTYPE_ID =10";
			$query="UPDATE PRACTITIONER SET PRACTITIONER_BUSINESSNAME ='{$_POST['PRACTITIONER_BUSINESSNAME']}' WHERE PRACTITIONER_NUMBER ={$row['PRACTITIONER_NUMBER']}";
			//msgBox($query);
	
	
	
	
			$result = mysqli_query($conn,$query);
	
			
			updateDetails($conn,mysqli_real_escape_string($conn,$_POST['detail_id']),$_POST);
			if(isset($_FILES) && $_FILES["flogo"]["name"]!=NULL)
			{
				uploadImage($conn,$_FILES,$row["PRACTITIONER_NUMBER"],mysqli_real_escape_string($conn,$_POST['detail_id']));
			}
			$_POST=null;
			?>
			<script language="javascript"> 
				<?php echo "window.location = 'myAbout.php'";?>
			</script> <?php
		}
		
		$result=getDetails($conn,$row["PRACTITIONER_NUMBER"],1);
		if($result==null){
			insertDetails($conn,$row["PRACTITIONER_NUMBER"],null,1);
			$result=getDetails($conn,$row["PRACTITIONER_NUMBER"],1);
	
			$data=mysqli_fetch_array($result);
			
			
	
		}else{
			$data=mysqli_fetch_array($result);
		}
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link href="<?php echo $path; ?>style/p_style.css" rel="stylesheet"></link>
<script type="text/javascript" src="<?php echo $path; ?>classes/jquery.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>About Me</title>
<link href="style/apple.css" rel="stylesheet" type="text/css" />
</head>

<body>
<?php include $path."includes/p_header.php"; ?>
<?php
if($row['ISFACILITATOR'] == 1){
	$backLink = "
		<a href='facilitator/cpanel.php'>Back to Your Control Panel</a>
	";
	include $path."facilitator/cpanelHeader.php"; 
}
else{
	$backLink = "
		<a href='cpanel.php'>Back to Your Control Panel</a>
	";
	include $path."cpanelHeader.php"; 
}
?>
<!--
<div class="PractitionerPhotoWrapper">
  <p class="PractitionerHeadings"><img src="<?php echo "photos/originals/" . $ppFileNameSession; ?>" alt="alexPhoto" width="167" height="141" align="top" /></p>
</div>
<div class='rfloat'>
	<a href='cpanel.php'>
		Back to Your Control Panel
	</a>
</div>
<div class="PractitionerNameWrapper">
  <p class="PractitionerName"><?php echo $row['PRACTITIONER_BUSINESSNAME']; ?></p>
</div>
<div class="PractitionerHyperlinkWrapper">
  <p class="hyperlinks">www.alexyusupov.com.au</p>
</div>
<div class="PractitionerHeadingWrapper">
  <p class="hyperlinks"><span class="PractitionerHeadings">Welcome to <?php echo $row['PRACTITIONER_BUSINESSNAME']; ?>'s Profile</span></p>
</div>
<div class="PractitionerSubHeadingWrapper">
  <p class="PractitionerSubHeadings">Serving Caulfield, Balaclava, and St Kilda, VIC (Victoria)</p>
</div>
-->
<div class='lfloat' style='width:98%'>
	<div class="stdWrapper" onMouseOver="this.style.background='white';"onmouseout="this.style.background='#F0F0F0';">
	  <p><span class="PractitionerBody">
	  <?php
	  
	  header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

	  if($data['DETAIL_IMAGE']!="")
		echo "<img src='photos/thumbs/" . $data['DETAIL_IMAGE'] . "' alt='' height='72' />";
	  else
		echo "<img src='images/Actions-help-about-icon.png' alt='' width='72' height='72' />";
	  ?>
	  <h3 class="hyperlinks">Update About Me</h3></span></p>
	  <?php
	  if($row['ISFACILITATOR'] == 1){
	  	echo "
	  <p class='PractitionerMainText'>Update details about your business hours, google street view maps, practice phone number, practice website link, photographs, videos, contact details, social networking links and PDF brochures</p>
	  ";
	  } else{
	  	echo "
	  <p class='PractitionerMainText'>Update details about your practice hours, google street view maps, practice phone number, practice website link, photographs, videos, contact details, social networking links and PDF brochures</p>";
		}
		?>
	  <form action="myAbout.php" method="post" enctype="multipart/form-data" name="UpdatePractitionerPromotion" id="UpdatePractitionerPromotion">
		<table width="789" border="0">
		  <tr>
			<td><span class="PractitionerMainText">Business Name</span></td>
			<td><span class="PractitionerMainText">
			  <input name="PRACTITIONER_BUSINESSNAME" type="text" id="AboutHeadline2" size="80" maxlength="80" value="<?php echo $row['PRACTITIONER_BUSINESSNAME']; ?>" />
			  <input name="detail_id" type="hidden" id="AboutHeadline2" value="<?php echo $data['PRACTITIONERDETAIL_ID']; ?>" />
			</span></td>
		  </tr>
		  <tr>
			<td><span class="PractitionerMainText">Sub Heading</span></td>
			<td><span class="PractitionerMainText">
			  <input name="DETAIL_SUBTITLE" type="text" id="AboutName2" size="40" maxlength="40" value="<?php echo $data['DETAIL_SUBTITLE']; ?>" />
			</span></td>
		  </tr>
		  <tr>
			<td><span class="PractitionerMainText">About Me Details</span></td>
			<td><span class="PractitionerMainText">
			  <label>
				<textarea name="DETAIL_MAINBODY" id="ServiceDescription2" cols="80" rows="20"><?php echo $data['DETAIL_MAINBODY']; ?></textarea>
			  </label>
			</span></td>
		  </tr>
		  <!-- <tr>
			<td><span class="PractitionerMainText">Practice Hours</span></td>
			<td><span class="PractitionerMainText">
			  <input type="text" name="DETAIL_PRACTICEHOUR" id="AboutPrice2" value="<?php echo $data['DETAIL_PRACTICEHOUR']; ?>" />
			</span></td>
		  </tr> -->
		  <?php
			if($data["DETAIL_IMAGE"]!="")
				echo "<input type='hidden' name='DETAIL_IMAGE' value='" . $data['DETAIL_IMAGE'] . "'>";
		  ?>
		  <tr>
			<td><span class="PractitionerMainText">Images</span></td>
			<td><input type="file" name="flogo" id="flogo" value="Upload Logo" /></td>
		  </tr>
		  <tr>
			<td><span class="PractitionerMainText">Videos</span></td>
			<td><span class="PractitionerMainText">
			  <input type="file" name="fDETAIL_VIDEO" id="AboutVideos2" />
			</span></td>
		  </tr>
		  <tr>
			<td><span class="PractitionerMainText">PDF</span></td>
			<td><span class="PractitionerMainText">
			  <input type="file" name="fDETAIL_PDF" id="AboutVideos2" />
			</span></td>
		  </tr>
		  <tr>
			<td class="PractitionerMainText">Independent Website Link</td>
			<td><span class="PractitionerMainText">
			  <input name="WEBSITEURL" type="text" id="AboutWebsite2" size="80" maxlength="80" value="<?php echo $webLink; ?>" />
			</span></td>
		  </tr>
		  <tr>
			<td><span class="PractitionerMainText">Social Network / Send Link</span></td>
			<td><span class="PractitionerMainText">
			  <input name="DETAIL_SOCIALNETWORK" type="text" id="AboutSocialLinks5" size="80" maxlength="80" value="<?php echo $data['DETAIL_SOCIALNETWORK']; ?>" />
			</span></td>
		  </tr>
		  <!--<tr>
			<td class="PractitionerMainText">Practice Phone Number</td>
			<td><span class="PractitionerMainText">
			  <input name="PRACTITIONER_BUSINESSNUMBER" disabled type="text" id="AboutPhone" size="40" maxlength="40" value="<?php echo $data['PRACTITIONER_BUSINESSNUMBER']; ?>" />
			</span></td>
		  </tr>
		  <tr>
			<td class="PractitionerMainText">My Contact Details</td>
			<td><span class="PractitionerMainText">
			  <input name="PRACTITIONER_BUSINESSADDRESS" type="text" id="AboutContact" size="80" maxlength="80" />
			</span></td>
		  </tr>
		  <tr>
			<td class="PractitionerMainText">Google Street View Map</td>
			<td><input name="Google Street View Map" type="text" id="AboutContact" size="80" maxlength="80" /></td>
		  </tr>
		  <tr>
			<td class="PractitionerMainText">Google Map</td>
			<td>&nbsp;</td>
		  </tr>
		  <tr>
			<td class="PractitionerMainText">Practice Address Details</td>
			<td><span class="PractitionerMainText">
			  <input name="PRACTITIONER_BUSINESSADDRESS" type="text" id="AboutAddress" disabled size="80" maxlength="80" value="<?php echo $data['PRACTITIONER_BUSINESSADDRESS'] . ", " . $data['PRACTITIONER_BUSINESSSUBURB'] . " " . $data['PRACTITIONER_BUSINESSPOSTCODE']; ?>"/>
			</span></td>
		  </tr>-->
		</table>
		<p class="PractitionerMainText">
		  <label>
			<input type="button" name="PreviewAbout2" id="PreviewAbout2" value="Preview About" />
		  </label>
		  <input type="submit" name="UploadAbout2" id="UploadAbout2" value="Update About" />
		</p>
	  </form>
	</div>
</div>
<p>&nbsp;</p>
<?php include $path."includes/p_footer.php"; ?>
</body>

</html>
