<?php
	session_start();
	include "includes/connect.php";
	include "includes/functions.php";
	include "classes/SimpleImage.php";
	$rowSession=getParticipantDetail($conn,$_SESSION['id']);
	$row=getPractitionerDetail($conn,$_SESSION['id']);
	$ppFileNameSession="blankSilhouetteMale.png";
	$ppFileName="blankSilhouetteMale.png";
	
	// if the logged in user is a surgery facilitator then we want to show the profile photo at left as the business logo
// otherwise the user is a practitioner and we show the profile photo on the left as the practitioner photo

if($row['ISFACILITATOR'] == 1)
{
	if($row['PRACTITIONER_BUSINESSLOGO']=="")
	{
			$ppFileNameSession="blankSilhouetteMale.png";
	}else
	
		{
			$ppFileNameSession=$row['PRACTITIONER_BUSINESSLOGO'];
		}
		
	}

else
{
	if($row['PRACTITIONER_BUSINESSLOGO']!="")
	{
		$ppFileNameSession=$row['PRACTITIONER_BUSINESSLOGO'];
		$ppFileName=$row['PRACTITIONER_BUSINESSLOGO'];
	}
	if($rowSession['PROFILEPHOTO']!="")
	{
		$ppFileNameSession=$rowSession['PROFILEPHOTO'];
	}
	
}


//	if($row['PRACTITIONER_BUSINESSLOGO']!=""){
//		$ppFileNameSession=$row['PRACTITIONER_BUSINESSLOGO'];
//		$ppFileName=$row['PRACTITIONER_BUSINESSLOGO'];
//	}
//	if($rowSession['PROFILEPHOTO']!=""){
//		$ppFileNameSession=$rowSession['PROFILEPHOTO'];
//	}

	$query="SELECT COUNT(*) FROM PATIENTPRACTITIONERRELATIONSHIP WHERE PRACTITIONER_NUMBER = '" . $_SESSION['practitioner_id'] . "' AND RECOMMENDEDAS='1'";
	$tempOfRecommend=mysqli_query($conn,$query);
	if(mysqli_num_rows($tempOfRecommend)!=0)
		$numbOfRecommend=mysqli_fetch_array($tempOfRecommend);
	else
		$numbOfRecommend[]=0;
	if(isset($_POST) && $_POST!=NULL)
	{		
		if($_POST['status']=="insert"){
			insertAcceptedInsurance($conn,$row["PRACTITIONER_NUMBER"],mysqli_real_escape_string($conn,$_POST['insuranceId']));
		}
		if($_POST['status']=="delete")
			deleteAcceptedInsurance($conn,$row["PRACTITIONER_NUMBER"],mysqli_real_escape_string($conn,$_POST['insuranceId']));
		?>
		<script language="javascript"> 
			<?php echo "window.location = 'myAcceptedInsurance.php'";?>
		</script>
		<?php
	}
	$details=getAcceptedInsurance($conn,$row["PRACTITIONER_NUMBER"]);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link href="<?php echo $path; ?>style/p_style.css" rel="stylesheet"></link>
<script type="text/javascript" src="<?php echo $path; ?>classes/jquery.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>My Accepted Insurance</title>
<link href="style/apple.css" rel="stylesheet" type="text/css" />
</head>

<body>
<?php include $path."includes/p_header.php"; ?>
<?php
if($row['ISFACILITATOR'] == 1){
	$backLink = "
		<a href='facilitator/cpanel.php'>Back to Your Control Panel</a>
	";
	include $path."facilitator/cpanelHeader.php"; 
}
else{
	$backLink = "
		<a href='cpanel.php'>Back to Your Control Panel</a>
	";
	include $path."cpanelHeader.php"; 
}
?>
<div class='lfloat' style='width:98%'>
	<div class="stdWrapper" onMouseOver="this.style.background='white';"onmouseout="this.style.background='#F0F0F0';">
	  <p><span class="PractitionerBody"><a href="myPromotions.php"><img src="images/promotions.png" width="72" height="72" /></a><a href="myPromotions.php" class="hyperlinks">Accepted Insurance List</a></span></p>
	  <p class="PractitionerMainText">Use this form to add a new insurance you accept. </p>
	  
		<table width="789" border="0">
		<?php 
		if($details!=NULL)
		{
			while($data=mysqli_fetch_array($details)){
				echo "<tr>";
				echo "<td>&nbsp;</td><td>$data[2]</td><td><form action='' method='post'><input type='hidden' name='insuranceId' value='$data[1]'/><input type='hidden' name='status' value='delete'/><input type='submit' value='Delete'/></form></td>";
				echo "</tr>";
			}
		} ?>
		  <form action="" method="post" enctype="multipart/form-data" name="CreatePractitionerPromotion" id="form1">
			<tr>
			<td><span class="PractitionerMainText">I am accepting</span></td>
			<td><span class="PractitionerMainText">
			<input type="hidden" name="status" value="insert">
			<select name='insuranceId' >
				<option value='0'>Select Health Insurance Provider</option>
				<?php
				$insuranceList=getInsuranceList($conn);
				while($insurance=mysqli_fetch_array($insuranceList)){
					echo "<option value='$insurance[0]'>$insurance[1]</option>";
				}
				?>
			</select>
			</span></td><td><input type="submit" name="UploadPromotion" id="UploadPromotion" value="Add Insurance" /></td>
		  </tr>
			</form>
		</table>
		<p>
		  
		</p>
	</div>
</div>
<p>&nbsp;</p>
<?php include $path."includes/p_footer.php"; ?>
</body>

</html>
