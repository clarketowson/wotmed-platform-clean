
<!-- GOOGLE STRUCTURED DATA DETAILS -->

<script type="application/ld+json">
{
"@context": "http://schema.org",
"@type": "Service",
"name": "Wotmed Patient Registration",
"description": "Sign up today as a Wotmed Patient and connect with Practitioners and Surgery Facilitators globally",
"serviceType": "Patient Registration Service",
"url": "http://platform.wotmed.com/reg-participant.php"
}
}
</script>

<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "VideoObject",
  "name": "Wotmed Explained",
  "description": "Video description",
  "thumbnailUrl": "WotmedExplainedSplash3.jpg",
  "uploadDate": "2015-01-01",
  "duration": "PT3M04S",
  "contentUrl": "http://platform.wotmed.com/WotmedFirstAdvertisementVer2.flv"
}
</script>


<script type="application/ld+json">
    {  "@context" : "http://schema.org",
       "@type" : "WebSite",
       "name" : "Wotmed Corporation",
       "url" : "http://www.wotmed.com"
    }
</script>

<script type="application/ld+json">
    {  "@context" : "http://schema.org",
       "@type" : "WebSite",
       "name" : "Wotmed Platform",
       "url" : "http://platform.wotmed.com"
    }
</script>

<script type="application/ld+json">
    {  "@context" : "http://schema.org",
       "@type" : "WebSite",
       "name" : "Wotmed News",
       "url" : "http://platform.wotmed.com/WotmedNews"
    }
</script>

<script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "WebSite",
        "url": "http://platform.wotmed.com/",
        "author": "Wotmed Corporation",
        "audience": "Patients, Practitioners, Surgery Facilitators Globally",
        "creator": "Clarke Towson, Christian Fletcher-Walker, Stefanus Kurniawan, Christian Felix Susanto",
        "publisher": "Wotmed Corporation",
        "version": "1"
    }
</script>


<script type="application/ld+json">
    {
    "@context": "http://schema.org",
    "@type" : "Organization",
    "name" : "The Collins St Group Investment Banking Services",
     "logo": "http://www.collinsgroup.com.au/images/CSG_logo.gif",
    "url" : "http://www.collinsgroup.com.au",
	"contactPoint" : [
	{ "@type" : "ContactPoint",
	"productSupported": "Wotmed Investment Banking Enquiries",
	"contactType": "Customer Support",
	"hoursAvailable": "9:00 am to 5:00 pm Monday to Friday",
    "telephone": "+61 3 9670 9030",
     "availableLanguage" : ["English"]
    } ] }
</script>

<script type="application/ld+json">
    {
    "@context": "http://schema.org",
    "@type" : "Organization",
    "name" : "Joseph Palmer & Sons Investment Managers",
    "logo": "http://www.jpalmer.com.au/Portals/0/Logo_wEst_smal.png",
    "url" : "http://www.jpalmer.com.au",
	"contactPoint" : [
	{ "@type" : "ContactPoint",
	"productSupported": "Wotmed Investor Relations",
	"contactType": "Customer Support",
	"hoursAvailable": "9:00 am to 5:00 pm Monday to Friday",
    "telephone": "+61 3 9601 6800",
     "availableLanguage" : ["English"]
    } ] }
</script>


<script type="application/ld+json">
    {
    "@context": "http://schema.org",
    "@type" : "Organization",
    "name" : "The Wotmed Platform",
    "sameAs" : [ "http://www.facebook.com/wotmed" ],
    "logo": "http://platform.wotmed.com/images/email/LogoEmail.jpg",
    "url" : " http://platform.wotmed.com",
	"contactPoint" : [
	{ "@type" : "ContactPoint",
	"productSupported": "Wotmed Platform Technical Support",
	"contactType": "Technical Support",
	"hoursAvailable": "9:00 am to 5:00 pm Saturday & Sunday",
    "telephone": "+61 432 359 166",
    "availableLanguage" : ["English","Thai"]


    } ] }
</script>

<script type="application/ld+json">
    {
    "@context": "http://schema.org",
    "@type" : "Organization",
     "name" : "Wotmed Corporation",
      "sameAs" : [ "http://www.facebook.com/wotmed" ],
     "logo": "http://platform.wotmed.com/images/email/LogoEmail.jpg",
    "url" : "http://www.wotmed.com",
	"contactPoint" : [
	{ "@type" : "ContactPoint",
	"productSupported": "Wotmed Sales & Business Enquiries",
	"contactType": "Sales",
	"hoursAvailable": "9:00 am to 5:00 pm Friday",
    "telephone": "+61 1300 693 201",
    "availableLanguage" : ["English"]
    } ] }
</script>

<script type="application/ld+json">
    {
	"@context": "http://schema.org",
	"@type": "PostalAddress",
	"streetAddress": "7/330 Collins St",
	"postalCode": "3000",
	"addressLocality": "Melbourne",
    "addressRegion": "Victoria",
    "addressCountry": "Australia"
    }
</script>

<script type="application/ld+json">
    {
      "@context": "http://schema.org",
      "@type": "Person",
      "address": {
        "@type": "PostalAddress",
        "addressLocality": "Melbourne",
        "addressRegion": "Victoria",
        "postalCode": "3000",
        "streetAddress": "7/330 Collins St"
      },
      "colleague": [
        "http://platform.wotmed.com/homeimages/ClarkeTowsonPhoto.jpg"
      ],
      "email": "mailto:christianfletcherwalker@wotmed.com",
      "image": "http://platform.wotmed.com/homeimages/ChristianPhoto.jpg",
      "jobTitle": "Wotmed Chief Executive Officer",
      "name": "Mr Christian Fletcher Walker",
      "telephone": "+61 425 465 493",
      "url": "http://www.wotmed.com",
      "gender": "Male"
    }
</script>

<script type="application/ld+json">
    {
      "@type": "Person",
      "address": {
        "@type": "PostalAddress",
        "addressLocality": "Melbourne",
        "addressRegion": "Victoria",
        "postalCode": "3000",
        "streetAddress": "7/330 Collins St"
      },
      "colleague": [
        "http://platform.wotmed.com/homeimages/ChristianPhoto.jpg"
      ],
      "email": "mailto:clarketowson@wotmed.com",
      "image": "http://platform.wotmed.com/homeimages/ClarkeTowsonPhoto.jpg",
      "jobTitle": "Wotmed Chief Technology Officer",
      "name": "Mr Clarke William Towson",
      "telephone": "+61 432 359 166",
      "url": "http://www.wotmed.com",
      "gender": "Male"
    }
</script>

<script type="application/ld+json">
    {
      "@type": "Person",
      "address": {
        "@type": "PostalAddress",
        "addressLocality": "Melbourne",
        "addressRegion": "Victoria",
        "postalCode": "3000",
        "streetAddress": "7/330 Collins St"
      },
      "colleague": [
        "http://platform.wotmed.com/homeimages/ClarkeTowsonPhoto.jpg"
      ],
      "email": "mailto:AMoffatt@jpalmer.com.au",
      "image": "http://www.jpalmer.com.au/portals/0/Images/staff/alexMoffatt.jpg",
      "jobTitle": "Wotmed Board Member & Director",
      "name": "Mr Alex Moffatt",
      "telephone": "+61 3 9601 6800",
      "url": "http://www.jpalmer.com.au",
      "gender": "Male"
    }
</script>

<script type="application/ld+json">
    {
      "@type": "Person",
      "address": {
        "@type": "PostalAddress",
        "addressLocality": "Melbourne",
        "addressRegion": "Victoria",
        "postalCode": "3000",
        "streetAddress": "13/350 Collins St"
      },
      "colleague": [
        "http://platform.wotmed.com/homeimages/ClarkeTowsonPhoto.jpg"
      ],
      "email": "mailto:ilmars@collinsgroup.com.au",
      "image": "http://www.collinsgroup.com.au/images/bio_id.jpg",
      "jobTitle": "Wotmed Board Member & Director",
      "name": "Mr Ilmars Draudins",
      "telephone": "+61 3 9670 9030",
      "url": "http://www.collinsgroup.com.au",
      "gender": "Male"
    }
</script>

<!-- GOOGLE STRUCTURED DATA DETAILS -->

<!-- SPECIFIC GOOGLE STRUCTURED DATA DETAILS FOR THIS PAGE -->




<!-- SPECIFIC GOOGLE STRUCTURED DATA DETAILS FOR THIS PAGE -->
<?php

session_start();
include "includes/connect.php";
include "includes/functions.php";

?>


<?php

// if a user is logged in then logout
// log user in as guestsearch@wotmed.com
// bypass sign up wizard
// Password1

// http://platform.wotmed.com/WotmedOpenSearchGuest.php

// remove ability to show search hyperlink at top right hand side of screen
// remove bottom footer

// modify all search links to redirect to sign up page


// loginGuestSearch.php
// logoutGuestSearch.php
// profileGuestSearch.php

?>





<!DOCTYPE html PUBLIC
    '-//W3C//DTD XHTML 1.0 Strict//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd'>
<html xmlns='http://www.w3.org/1999/xhtml'><head>
    <title>Wotmed Guest Search</title>
    <meta http-equiv='Content-Type' content=''text'/html; charset=utf-8' />


    <script type='text/javascript'>

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-38343871-1']);
        _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();

    </script>

    <script type="text/javascript">
        function back()
        {
            window.location = "WotmedOpenSearchGuest.php"
        }
    </script>


    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-49954656-1', 'wotmed.com');
        ga('send', 'pageview');

    </script>

    <link rel="stylesheet" type="text/css" href="advanceSearch3.css">

    <script type='text/javascript' src='classes/jquery.js'></script>
    <script type='text/javascript' src='classes/hover.js'></script>
    <script src='/classes/jquery.validation.js'></script>
    <script src='/classes/dateSelectBoxes.js'></script>

    <link href='/style/i_style.css' rel='stylesheet'></link>

    <!-- Wotmed Chat System NOTE - MUST BE KEPT EXACTLY HERE-->
    <script type="text/javascript" src="http://platform.wotmed.com/WotmedChat/livechat/php/app.php?widget-init.js"></script>
    <!-- Wotmed Chat System -->

    <link rel='stylesheet' type='text/css' href='../signup.css'>

    <meta name='description' content='Wotmed - Connect with Practitioners and Surgery Facilitators Globally.  Wotmed.com – Facilitating Medical Travel'>
    <meta name='keywords' content='Wotmed.com, Wotmed Guest Search'>

    <meta property="og:locale" content="en_US">
    <meta property="og:type" content="website">
    <meta property="og:title" content="Welcome to Wotmed">
    <meta property="og:description" content="Wotmed.com - Packaging Medical Tourism">
    <meta property="og:site_name" content="Wotmed">
    <meta property="og:url" content="http://platform.wotmed.com/reg-participant.php">
    <meta property="og:image" content="http://platform.wotmed.com/WotmedFacebookLogo2015.png">


</head>
<body onload='searchUsers()' >
<div class='topBar' >
    <div class='bar_frame'>
        <div class='w_logo'><img src='images/WotmedLogoTransparent.png' height='45' alt='wotmedLogo' style='position:relative; left:-30px; padding:0px;padding-top:10px;' /></div>
        <div class='div_login'>
            <form id='loginForm' method='post' action='/login.php'>
                <table>
                    <tr>
                        <td><label>Email</label></td>
                        <td><label>Password</label></td>
                    </tr>
                    <tr>
                        <td width='150'><input type='text' class='inputtext required email' name='email' id='email'/></td>
                        <td width='150'><input type='password' class='inputpassword required' name='password' id='password'/></td>
                        <td><input type='submit' value='Login'/></td>
                    </tr>
                    <tr>
                        <td><input name='persistent' type='checkbox' style='float:left; margin: 0px; padding: 0px;'/> <label>Keep me logged in</label></td>
                        <td><a href='/resetPassword.php'><label style='cursor:pointer;text-decoration:underline;'>Forget your password?</label></a></td>


                    </tr>

                </table>
            </form>
            <div id='errMsg'>
            </div>
        </div>
        <div>
            <?php //include 'fbConnect.php'; ?>
        </div>
        <div>
            <?php //include 'googleConnect.php'; ?>
        </div>
    </div>
</div>



<div id='container'>


    <h3><a href="#" onclick="return false;" class="about">Wotmed is the medical network that connects Patients with Practitioners and Surgery Facilitators globally</a></h3>
    <p>
        &nbsp;</p>

    <div id='system'>


        <article class='item'>


            <div class='content clearfix'>




                <div class='gborder marBotBig fMid' style="width:99%" title='Search for Wotmed Practitioners and Surgery Facilitators'>

                    <table style="width:100%">

                        <?php

                        //	echo "<p><b>Search and connect with Practitioners and Surgery Facilitators globally who have an account on Wotmed</b>";

                        echo "<img src='images/WotmedSearchIcon.png' style='width:30px;'>";

                        echo "<b style='color:#195297;font-size:16px;'>Search and connect with Practitioners and Surgery Facilitators globally</b>";

                        echo "<BR><BR>";
                        echo "<form name='WotmedOpenSearch' method='GET' action='WotmedOpenSearchAllGuest.php'>";
                        echo "<input type='text' name='searchTerm' id='searchTerm'/>";
                        echo "<input type='radio' name='includeFacilitators' value='Facilitator' unchecked>Include Surgery Facilitators in your search<BR><BR>";
                        echo "<input type='submit' name='searchsubmit' value='Wotmed Search' class='form-submit' id='searchsubmit' />";
                        echo "</form>";

                        ?>

                    </table>
                </div>



            <BR><BR>

                <p> <a href='../WotmedExplained.php'>Click here to watch the Wotmed explained video</a></p>
                <p> <a href='http://www.wotmed.com'>Click here to go to the Wotmed landing pages</a></p>
                <p> <a href='../WotmedPractitionerArticles.php'>Click here to read or write articles for Wotmed</a></p>
                <p> <a href='../WotmedAdvertisingRequest.php'>Click here to buy advertising on Wotmed</a></p>



                <br>

                <p>&nbsp;</p>


            </div>

            <span>Copyright &copy; Wotmed.com Page Last Updated: <?php	echo date('d-m-Y H:i:s'); ?> All rights reserved.  </span>


            <p>&nbsp;</p>
            <p>&nbsp;</p>




