<?php
	session_start();
	include "includes/connect.php";
	include "includes/functions.php";





	if(isset($_SESSION['id'])){
		$rowSession=getParticipantDetail($conn,$_SESSION['id']);

	}
	if($rowSession['PROFILEPHOTO']==""){
		$ppFileNameSession="blankSilhouetteMale.png";
	}else{
		$ppFileNameSession=$rowSession['PROFILEPHOTO'];
	}




?>


<html>
<head>

    <script src="formvalidate/dist/parsley.min.js"></script>


    <link rel="stylesheet" href="proposals/date/css/pikaday.css">


    <meta name="description" content="Book an Appointment with a Wotmed Practitioner.  Wotmed.com – Facilitating Medical Travel">
    <meta name="keywords" content="Wotmed.com, Book an Appointment with a Wotmed Practitioner, Facilitating Medical Travel">

    <meta property="og:locale" content="en_US">
    <meta property="og:type" content="website">
    <meta property="og:title" content="Book an Appointment with a Wotmed Practitioner">
    <meta property="og:description" content="Book an Appointment with a Wotmed Practitioner">
    <meta property="og:site_name" content="Wotmed">
    <meta property="og:url" content="http://platform.wotmed.com/booking.php">
    <meta property="og:image" content="http://platform.wotmed.com/BookingRequest2.jpg">


    <title>
        Book a Wotmed Practitioner
    </title>
	<link href="style/p_style.css" rel="stylesheet"></link>

    <link rel="stylesheet" type="text/css" href="signup.css">

</head>
<body>



	<?php include "includes/p_header.php"; ?>
	<div class="center">
		<form method="post" action="booking.php">
		<?php

        echo "<div class='Booking' align='left'>";


        echo  "<h1><strong>Book an appointment with this Wotmed Practitioner</strong></h1>";

        echo "<p> Simply click the Book Now button below to book an appointment with this Wotmed Practitioner</p>";
        echo "<p> Both the Practitioner and yourself will receive an email confirmation of your booking.</p>";

        echo "<img src='BookingRequest2.jpg'>";
        echo "<BR><BR>";


        echo "<div>";
        echo "<BR><BR>";

        echo "</div>";


			if(isset($_POST['reason']))
			{
				$id=mysqli_real_escape_string($conn,$_SESSION['id']);
				$pracId=mysqli_real_escape_string($conn,$_POST['pracId']);
				$date=mysqli_real_escape_string($conn,$_POST['date']);
				$time=mysqli_real_escape_string($conn,$_POST['time']);
				$reason=mysqli_real_escape_string($conn,$_POST['reason']);$query="INSERT INTO BOOKING (PARTICIPANT_NUMBER,PRACTITIONER_NUMBER,DATE,TIME,REASON,STATUS) VALUES ('" . $id . "','" . $pracId . "','" . $date . "','" . $time . "','" . $reason . "','PENDING');";


				mysqli_query($conn,$query);

                $patientFirstName = $rowSession["FIRSTNAME"];
                $patientSurname = $rowSession["SURNAME"];

                $patientEmailAddress = $rowSession["EMAILADDRESS"];

                $appointmentDate    = mysqli_real_escape_string($conn, $_POST['datepicker']);
                $appointmentTime = mysqli_real_escape_string($conn, $_POST['AppointmentTime']);


                // get the practitioners details

                $query="SELECT PARTICIPANT_NUMBER, PRACTITIONER_BUSINESSNAME, PRACTITIONER_BUSINESSNUMBER, PRACTITIONER_BUSINESSADDRESS, PRACTITIONER_BUSINESSSUBURB, PRACTITIONER_BUSINESSPOSTCODE, PRACTITIONER_BUSINESSCITY, PRACTITIONER_BUSINESSSTATE, PRACTITIONER_BUSINESSLOGO FROM PRACTITIONER WHERE PRACTITIONER_NUMBER = $pracId";
                $result = $conn->query($query);

                while($row = $result->fetch_assoc())
                {
                    $participantNumber = $row["PARTICIPANT_NUMBER"];
                    $practitionerBusinessName = $row["PRACTITIONER_BUSINESSNAME"];
                    $practitionerBusinessNumber = $row["PRACTITIONER_BUSINESSNUMBER"];
                    $practitionerBusinessAddress = $row["PRACTITIONER_BUSINESSADDRESS"];
                    $practitionerBusinessSuburb = $row["PRACTITIONER_BUSINESSSUBURB"];
                    $practitionerBusinessCity = $row["PRACTITIONER_BUSINESSCITY"];
                    $practitionerBusinessState = $row["PRACTITIONER_BUSINESSSTATE"];
                    $practitionerBusinessPostCode = $row["PRACTITIONER_BUSINESSPOSTCODE"];
                    $practitionerBusinessLogo = $row["PRACTITIONER_BUSINESSLOGO"];


                //    echo $participantNumber;
                //    echo $practitionerBusinessName;

                }


                $query="SELECT PARTICIPANT_NUMBER, EMAILADDRESS, PROFILEPHOTO FROM PARTICIPANT WHERE PARTICIPANT_NUMBER = $participantNumber";
                $result = $conn->query($query);

        while($row = $result->fetch_assoc())
        {
            $practitionerEmailAddress = $row["EMAILADDRESS"];
            $practitionerProfilePhoto = $row["PROFILEPHOTO"];

        //    echo $practitionerEmailAddress;

        }


                $bannerCode = "
<!-- 3 columns -->
<table width='100%' bgcolor='#f7f7f7' cellpadding='0' cellspacing='0' border='0' id='backgroundTable'>
    <tbody>
    <tr>
        <td>
            <table width='600' cellpadding='0' cellspacing='0' border='0' align='center' class='devicewidth'>
                <tbody>
                <tr>
                    <td width='100%'>
                        <table width='600' cellpadding='0' cellspacing='0' border='0' align='center' class='devicewidth'>
                            <tbody>
                            <tr>
                                <td>
                                        <tr>
                                            <td width='186' height='250' align='center' class='devicewidth'>
                                                <img src='http://platform.wotmed.com/photos/thumbs/$practitionerBusinessLogo' alt='Practitioner Business Logo' border='0' style='display:block; border:none; outline:none; text-decoration:none;'>
                                            </td>
                                        </tr>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>
<!-- end of 3 columns -->

";

                $bannerCode2 = "
<!-- 3 columns -->
<table width='100%' bgcolor='#f7f7f7' cellpadding='0' cellspacing='0' border='0' id='backgroundTable'>
    <tbody>
    <tr>
        <td>
            <table width='600' cellpadding='0' cellspacing='0' border='0' align='center' class='devicewidth'>
                <tbody>
                <tr>
                    <td width='100%'>
                        <table width='600' cellpadding='0' cellspacing='0' border='0' align='center' class='devicewidth'>
                            <tbody>
                            <tr>
                                <td>
                                        <tr>
                                            <td width='186' height='250' align='center' class='devicewidth'>
                                                <img src='http://platform.wotmed.com/photos/thumbs/$practitionerProfilePhoto' alt='Practitioner Profile Photo' border='0' style='display:block; border:none; outline:none; text-decoration:none;'>
                                            </td>
                                        </tr>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>
<!-- end of 3 columns -->

";



                if (!defined("PHP_EOL")) define("PHP_EOL", "\r\n");


            //    $address = "bookings@wotmed.com";

                $comments = "none";

                $address = $practitionerEmailAddress;


                $e_body = "$patientFirstName has booked an appointment with you via Wotmed!";
                $e_content = "\"$comments\"" . PHP_EOL . PHP_EOL;
                $e_reply = "You can contact $patientFirstName via email, $patientEmailAddress";

                $msg = wordwrap( $e_body . $e_content . $e_reply, 70 );

                $headers = 'From: bookings@wotmed.com' . "\r\n" .
                    'Reply-To: bookings@wotmed.com' . "\r\n" .
                    'X-Mailer: PHP/' . phpversion();
                $headers .= "MIME-Version: 1.0\r\n";
                $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

                $subject = 'A patient: ' . $patientFirstName . ' ' . $patientSurname . ' with email address: ' . $patientEmailAddress . ' has just booked an appointment with you via Wotmed!';

                $message = file_get_contents('HTMLEmailTemplate/WotmedEmailTemplate/RequestforProposalHeader.html');
                $message .= file_get_contents('HTMLEmailTemplate/WotmedEmailTemplate/PatientBookingBodyHeaderThankYou.html');


                $message .= 'Patient Name: ' . $patientFirstName . ' ' . $patientSurname . "<BR>" . 'Patient Email: ' . $patientEmailAddress . "<BR>" . 'Booking Date: ' . $appointmentDate . "<BR>" . 'Booking Time: ' . $appointmentTime  . "<BR>";

                $message .= file_get_contents('HTMLEmailTemplate/WotmedEmailTemplate/PractitionerBookingBodyFooterClient.html');

                $message .= file_get_contents('HTMLEmailTemplate/WotmedEmailTemplate/PractitionerBookingFooterClient.html');


                if(mail($address, $subject, $message, $headers))
                {

                //	include ("reg-participantStorySuccess.php");



                    echo '<script language="javascript">';
                    echo 'alert("Your booking request has been successfully made with this Wotmed Practitioner.  You should receive an email confirmation shortly.")';
                    echo '</script>';

                }

                $address2 = $patientEmailAddress;	// set the email address to the patients email now

                $headers2 = 'From: bookings@wotmed.com' . "\r\n" .
                    'Reply-To: bookings@wotmed.com' . "\r\n" .
                    'X-Mailer: PHP/' . phpversion();
                $headers2 .= "MIME-Version: 1.0\r\n";
                $headers2 .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

                $subject2 = 'Your Wotmed Practitioner Booking ' . $patientFirstName . ' ' . $patientSurname;

                $message2 = file_get_contents('HTMLEmailTemplate/WotmedEmailTemplate/RequestforProposalHeader.html');
                $message2 .= file_get_contents('HTMLEmailTemplate/WotmedEmailTemplate/PractitionerBookingBodyHeaderThankYou.html');

                $message2 .= "<BR>" . 'Dear ' . $patientFirstName . ',' . "<BR><BR>" . 'Thank you so much for booking an appointment with a Wotmed Practitioner' . "<BR><BR>";
                $message2 .= 'Your chosen Practitioner has been emailed and advised of your booking.';
                $message2 .= "<BR><BR>" . 'Kind Regards,' . "<BR><BR>" . 'The Wotmed Team' . "<BR><BR>";

                $message2 .= '<b>Your booking details:</b>' . "<BR><BR>";

                $message2 .= 'Booking Date: ' . $appointmentDate . "<BR>";
                $message2 .= 'Booking Time: ' . $appointmentTime . "<BR>";
                $message2 .= 'Practitioner Name: Dr ' . $practitionerBusinessName . "<BR>";
                $message2 .= 'Clinic Phone Number: ' .  $practitionerBusinessNumber . "<BR>";
                $message2 .= 'Clinic Address: ' .  $practitionerBusinessAddress . ' ' . $practitionerBusinessSuburb . ' ' . $practitionerBusinessCity . ' ' . $practitionerBusinessState . "<BR>";

                $message2 .= $bannerCode . "<BR>";
                $message2 .= $bannerCode2 . "<BR>";

                $message2 .= file_get_contents('HTMLEmailTemplate/WotmedEmailTemplate/PractitionerBookingBodyFooterClient.html');

                $message2 .= file_get_contents('HTMLEmailTemplate/WotmedEmailTemplate/PractitionerBookingFooterClient.html');



                if(mail($address2, $subject2, $message2, $headers2))
                {

                }

                else

                {
                    echo 'ERROR!';
                }


				echo "<script language='javascript'>";
					echo "window.location = 'profile.php';";
				echo "</script>";
			}
			else
			{
				echo "<input type='hidden' name='pracId' value='" . $_GET['practitionerId'] . "'>";
				echo "<input type='hidden' name='date' value='" . $_GET['date'] . "'>";
				echo "<input type='hidden' name='time' value='" . $_GET['time'] . "'>";
			}
		?>
            Requested Appointment Date:
                <input type="text" id="datepicker" name="datepicker" value="Select desired date">

            <BR><BR>

            Requested Appointment Time:
            <select name='AppointmentTime' id='AppointmentTime'>
                <option value='Please select your desired appointment time'>Please select your desired appointment time</option>
                <option value='8:30 am'>8:30 am</option>
                <option value='9:00 am'>9:00 am</option>
                <option value='9:30 am'>9:30 am</option>
                <option value='10:00 am'>10:00 am</option>
                <option value='10:30 am'>10:30 am</option>
                <option value='11:00 am'>11:00 am</option>
                <option value='11:30 am'>11:30 am</option>
                <option value='12:00 noon'>12:00 noon</option>
                <option value='12:30 pm'>12:30 pm</option>
                <option value='1:00 pm'>1:00 pm</option>
                <option value='1:30 pm'>1:30 pm</option>
                <option value='2:00 pm'>2:00 pm</option>
                <option value='2:30 pm'>2:30 pm</option>
                <option value='3:00 pm'>3:00 pm</option>
                <option value='3:30 pm'>3:30 pm</option>
                <option value='4:00 pm'>4:00 pm</option>
                <option value='4:30 pm'>4:30 pm</option>
                <option value='5:00 pm'>5:00 pm</option>
                <option value='5:30 pm'>5:30 pm</option>

            </select>

            <BR><BR>

		Reason :
			<select name='reason' id='reason'>
				<option value='Consultation'>Consultation</option>
			</select>
			<br><br>
			<input type="submit" id='signupsubmit' value="Book Now">
		</form>
	</div>

    <script src="/proposals/date/pikaday.js"></script>
    <script>

        var picker = new Pikaday(
            {
                field: document.getElementById('datepicker'),
                firstDay: 1,
                minDate: new Date('2000-01-01'),
                maxDate: new Date('2020-12-31'),
                yearRange: [2000,2020]
            });

    </script>

	<?php

    echo "<BR><BR>";

    include "includes/p_footer.php"; ?>
</body>
</html>
