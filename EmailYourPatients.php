<?php
	session_start();
	include "includes/connect.php";
	include "includes/functions.php";
	include "classes/SimpleImage.php";
	$rowSession=getParticipantDetail($conn,$_SESSION['id']);
	$row=getPractitionerDetail($conn,$_SESSION['id']);
	$ppFileNameSession="blankSilhouetteMale.png";
	$ppFileName="blankSilhouetteMale.png";
	
	// if the logged in user is a surgery facilitator then we want to show the profile photo at left as the business logo
// otherwise the user is a practitioner and we show the profile photo on the left as the practitioner photo

if($row['ISFACILITATOR'] == 1)
{
	if($row['PRACTITIONER_BUSINESSLOGO']=="")
	{
			$ppFileNameSession="blankSilhouetteMale.png";
	}else
	
		{
			$ppFileNameSession=$row['PRACTITIONER_BUSINESSLOGO'];
		}
		
	}

else
{
	if($row['PRACTITIONER_BUSINESSLOGO']!="")
	{
		$ppFileNameSession=$row['PRACTITIONER_BUSINESSLOGO'];
		$ppFileName=$row['PRACTITIONER_BUSINESSLOGO'];
	}
	if($rowSession['PROFILEPHOTO']!="")
	{
		$ppFileNameSession=$rowSession['PROFILEPHOTO'];
	}
	
}


//	if($row['PRACTITIONER_BUSINESSLOGO']!=""){
//		$ppFileNameSession=$row['PRACTITIONER_BUSINESSLOGO'];
//		$ppFileName=$row['PRACTITIONER_BUSINESSLOGO'];
//	}
//	if($rowSession['PROFILEPHOTO']!=""){
//		$ppFileNameSession=$rowSession['PROFILEPHOTO'];
//	}

	$query="SELECT COUNT(*) FROM PATIENTPRACTITIONERRELATIONSHIP WHERE PRACTITIONER_NUMBER = '" . $_SESSION['practitioner_id'] . "' AND RECOMMENDEDAS='1'";
	$tempOfRecommend=mysqli_query($conn,$query);
	if(mysqli_num_rows($tempOfRecommend)!=0)
		$numbOfRecommend=mysqli_fetch_array($tempOfRecommend);
	else
		$numbOfRecommend[]=0;
	if(isset($_POST) && $_POST!=NULL)
	{		
		if($_POST['status']=="insert"){
			insertAcceptedInsurance($conn,$row["PRACTITIONER_NUMBER"],mysqli_real_escape_string($conn,$_POST['insuranceId']));
		}
		if($_POST['status']=="delete")
			deleteAcceptedInsurance($conn,$row["PRACTITIONER_NUMBER"],mysqli_real_escape_string($conn,$_POST['insuranceId']));
		?>
		<script language="javascript"> 
			<?php echo "window.location = 'myAcceptedInsurance.php'";?>
		</script>
		<?php
	}
	$details=getAcceptedInsurance($conn,$row["PRACTITIONER_NUMBER"]);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link href="<?php echo $path; ?>style/p_style.css" rel="stylesheet"></link>
<script type="text/javascript" src="<?php echo $path; ?>classes/jquery.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Practitioner: Upload your patient list and email your patients</title>


<link href="style/apple.css" rel="stylesheet" type="text/css" />
<link href="WotmedSkyeStyle.css" rel="stylesheet" type="text/css" />



</head>

<body>
<?php include $path."includes/p_header.php"; ?>
<?php
if($row['ISFACILITATOR'] == 1){
	$backLink = "
		<a href='facilitator/cpanel.php'>Back to Your Control Panel</a>
	";
	include $path."facilitator/cpanelHeader.php"; 
}
else{
	$backLink = "
		<a href='cpanel.php'>Back to Your Control Panel</a>
	";
	include $path."cpanelHeader.php"; 
}
?>
<div class='lfloat' style='width:98%'>
	<div class="stdWrapper" onMouseOver="this.style.background='white';"onmouseout="this.style.background='#F0F0F0';">
	  <p><span class="PractitionerBody"><a href="EmailYourPatients.php"><img src="images/ContactsImportIcon.png" alt="" width="72" height="72" /></a><a href="EmailYourPatients.php" class="hyperlinks">Bulk Import and Email all your Patients</a></span></p>
	  <h1 class="PractitionerMainText">Send an automated email to all your past and current patients encouraging them to join Wotmed to recommend and review you</h1>
	  <p>&nbsp;</p>
	  <h1 class="PractitionerMainText">It is in your best interest as a Wotmed Practitioner to ensure that you have as many recommendations and reviews by your past patients as possible. To help facilitate this process Wotmed has developed this form by which you can send an automated email to all your past and current patients encouraging them to join Wotmed to recommend and review you. </h1>
<p class="PractitionerMainText">&nbsp;</p>

<form action="EmailMyPatients.php" method="post" enctype="multipart/form-data" name="form1" id="form1">
  <p>
    <label for="textarea">Paste your patients email addresses here.  <BR>Each email address should be on a new line eg:<BR><BR>patient@gmail.com<BR>patient2@gmail.com
        <br />
      <br />
    </label>
    <textarea name="patientEmailAddresses" id="patientEmailAddresses" cols="45" rows="15"></textarea>
  </p>
  <p>
   <!-- <input type="submit" name="emailpatients" id="emailpatients" value="Load Patient Email Addresses" /> -->
    
     <input type='submit' name='emailpatients' value='Load Patient Email Addresses' class='form-submit' id='browsesubmit' />

    
  </p>
</form>
<p>&nbsp;</p>
<p class="PractitionerMainText"><strong>Please Note: </strong>After completing this form your patients will receive an email from Wotmed that looks similar to the graphic below.</p>
<p class="PractitionerMainText">

<img src="social/PractitionerToPatientEmailGraphicPreview.png" width="639" height="1277" alt="Practitioner To Patient Email Preview" />

&nbsp;</p>
      
      &nbsp;</p>
    </div>
</div>
<p>&nbsp;</p>
<?php include $path."includes/p_footer.php"; ?>
</body>

</html>
