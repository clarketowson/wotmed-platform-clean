<style type="text/css">
    .centeredImage
    {
        text-align:center;
        margin-top:0px;
        margin-bottom:0px;
        padding:0px;
    }
</style>

<div id="contentMain" class="lfloat">
	<div id="noticeDiv">
		<div id="uploadPhotoDiv" class="fMid fcg">
			<form method='POST' enctype="multipart/form-data" action= 'uploadProfilePicture.php'>
			Update your profile picture:<br/>
			<input type="file" name="pp"/>
			<input type="submit" value="Upload"/>
			</form>
		</div>
	</div>
	<div class="marBotBig marTopBig">
		<div class="rfloat" style="padding-top:15px;color:rgb(170, 170, 170)">
			<span>Surgery Facilitator No: <?php echo $row['PRACTITIONER_NUMBER']; ?></span><br>
			<span>Profile Views: <?php echo $row['VIEWCOUNT']; ?></span>
		</div>
		<div class="bold marBotBig subtitle fMid fColor" style="padding-top:7px;">
			<span style="font-size:20px" class="lfloat"><?php echo $row['PRACTITIONER_BUSINESSNAME'];?></span>
			<?php echo $trusted; ?><?php echo $trustIndex; ?>
			<span style="font-size:10px;margin-left:5px;margin-top:10px" class="">
			<?php
			if(isset($_GET['id'])){
				$temp=explode("'",$_GET['id']);
				$id=$temp[0];

				if($id!=$_SESSION['id']){
					$relation=true; //-- true if there is no relationship between practitioner and participant
					$doctors=getDoctor($conn,$_SESSION['id']);
					if($doctors!=Null)
					{
						while($listOfDoctors=mysqli_fetch_array($doctors)){
							if($row['PRACTITIONER_NUMBER']==$listOfDoctors[0])
							{
								$relation=false;
								//echo "This doctor is already in your list";
							}
						}
						if($relation)
						{
							echo "<a href='{$path}addDoctorToAccount.php?isId=" . $_SESSION['id'] . "&ofId=" . $row['PRACTITIONER_NUMBER'] . "'>Establish a relationship with this Surgery Facilitator</a>";
						}
					}
					else
					{
						echo "<a href='{$path}addDoctorToAccount.php?isId=" . $_SESSION['id'] . "&ofId=" . $row['PRACTITIONER_NUMBER'] . "'>Establish a relationship with this Surgery Facilitator</a>";
					}
				}
			}
			?>
			</span>
			<div style='font-size:14px;padding-top:10px;'>
			<span><?php 
				$result=getDetails($conn,$row["PRACTITIONER_NUMBER"],17);
				if($result!=null){
					$data=mysqli_fetch_array($result);
					echo $data['DETAIL_MAINBODY'];	
				}
			?></span>
			</div>
			<?php
				echo "<div style='font-size:12px;font-weight:normal;padding-left:20px;padding-top:10px;'>";
				if(isset($_GET['id'])){
					echo "
					This Surgery Facilitator has been <div id='trustText' style='display:inline-block;padding-top:3px;'><a href='#' style='color:#325199;font-style:italic;'>Trusted</a><div id='trustTextPopUp' class='Indices'>This is the total number of Wotmed patients that trust this Surgery Facilitator</div></div> by: " . $numTrust . " Wotmed patient(s)<BR>";
					echo "
					This Surgery Facilitator has been <div id='recomText' style='display:inline-block;padding-top:3px;'><a href='#' style='color:#325199;font-style:italic;'>Recommended</a><div id='recomTextPopUp' class='Indices'>This is the total number of Wotmed patients that have recommended this Surgery Facilitator</div></div> by: " . $numRecom . " Wotmed patient(s)<BR>";
					echo "
					This Surgery Facilitator has been <div id='thankText' style='display:inline-block;padding-top:3px;'><a href='#' style='color:#325199;font-style:italic;'>Thanked</a><div id='thankTextPopUp' class='Indices'>This is the total number of Wotmed patients that have thanked this Surgery Facilitator</div></div> by: ". $numThank ." Wotmed patient(s)<br><br>";
					
					$temp=explode("'",$_GET['id']);
					$id=$temp[0];

					echo "
					This Surgery Facilitator is <div id='connectedToText' style='display:inline-block;padding-top:3px;'><a href='#' style='color:#325199;font-style:italic;'>Connected</a><div id='connectedToTextPopUp' class='Indices'>This is the total number of Wotmed patients that have established relationships with this Surgery Facilitator</div></div> to: " . getNumbOfParticipant($conn,$_SESSION['id']) . " Wotmed patient(s)<br>
					This Surgery Facilitator is <div id='connectedTo2Text' style='display:inline-block;padding-top:3px;'><a href='#' style='color:#325199;font-style:italic;'>Connected</a><div id='connectedTo2TextPopUp' class='Indices'>This is the total number of Wotmed practitioners that this Surgery Facilitator has established a relationship with</div></div> to: 0 Wotmed practitioners(s)<br>
					";
					echo "<br>";
					echo "This Surgery Facilitator has facilitated surgery for: 0 Wotmed patient(s)"."<br>";
				}
				else{
					echo "
					You have been <div id='trustText' style='display:inline-block;padding-top:3px;'><a href='#' style='color:#325199;font-style:italic;'>Trusted</a><div id='trustTextPopUp' class='Indices'>This is the total number of Wotmed patients that trust you</div></div> by: " . $numTrust . " Wotmed patient(s)<BR>
					You have been <div id='recomText' style='display:inline-block;padding-top:3px;'><a href='#' style='color:#325199;font-style:italic;'>Recommended</a><div id='recomTextPopUp' class='Indices'>This is the total number of Wotmed patients that have recommended you</div></div> by: " . $numRecom . " Wotmed patient(s)<BR>
					You have been <div id='thankText' style='display:inline-block;padding-top:3px;'><a href='#' style='color:#325199;font-style:italic;'>Thanked</a><div id='thankTextPopUp' class='Indices'>This is the total number of Wotmed patients that have thanked you</div></div> by: ". $numThank ." Wotmed patient(s)<br><br>";
					
					echo "
					You are <div id='connectedToText' style='display:inline-block;padding-top:3px;'><a href='#' style='color:#325199;font-style:italic;'>Connected</a><div id='connectedToTextPopUp' class='Indices'>This is the total number of Wotmed patients that have established relationships with you</div></div> to: " . getNumbOfParticipant($conn,$_SESSION['id']) . " Wotmed patient(s)<br>
					You are <div id='connectedTo2Text' style='display:inline-block;padding-top:3px;'><a href='#' style='color:#325199;font-style:italic;'>Connected</a><div id='connectedTo2TextPopUp' class='Indices'>This is the total number of Wotmed practitioners that you have established a relationship with</div></div> to: 0 Wotmed practitioners(s)<br>
					";
					echo "<br>";
					echo "You have facilitated surgery for: 0 Wotmed patient(s)"."<br>";
					
				}
				echo "</div>";
				//echo "This practitioner recommended by : " . $numbOfRecommend[0] . " / " . $numbOfPatients[0];
			?>
		</div>
	</div>
	<div>
		
		<?php
			if(isset($_POST['businessNumb']))
			{	
				$businessNumb=mysqli_real_escape_string($conn,$_POST['businessNumb']);
				$address=mysqli_real_escape_string($conn,$_POST['address']);
				$suburb=mysqli_real_escape_string($conn,$_POST['suburb']);
				$postCode=mysqli_real_escape_string($conn,$_POST['postCode']);
				$country=mysqli_real_escape_string($conn,$_POST['country']);
				$profStatement=mysqli_real_escape_string($conn,$_POST['profStatement']);
				$speciality=mysqli_real_escape_string($conn,$_POST['speciality']);
				$education=mysqli_real_escape_string($conn,$_POST['education']);
				$qualification=mysqli_real_escape_string($conn,$_POST['qualifications']);
				$boardCertification=mysqli_real_escape_string($conn,$_POST['boardCertification']);
				$practitioner_professionalmembership=mysqli_real_escape_string($conn,$_POST['practitioner_professionalmembership']);
				$awardsandpublications=mysqli_real_escape_string($conn,$_POST['awardsandpublications']);
				$hospitalaffiliation=mysqli_real_escape_string($conn,$_POST['hospitalaffiliation']);
				$cdegree=mysqli_real_escape_string($conn,$_POST['cdegree']);
				$ctrained=mysqli_real_escape_string($conn,$_POST['ctrained']);
				$id=$row['PRACTITIONER_NUMBER'];
				$query="UPDATE PRACTITIONER SET PRACTITIONER_BUSINESSNUMBER='" . $businessNumb . "', COUNTRY_NUMBER='" . $country . "', PRACTITIONER_BUSINESSADDRESS='" . $address . "', PRACTITIONER_BUSINESSSUBURB='" . $suburb . "', PRACTITIONER_BUSINESSPOSTCODE='" . $postCode . "', PRACTITIONER_BUSINESSPROFESSIONALSTATEMENT='" . $profStatement . "', SPECIALITY_NUMBER='" . $speciality . "', EDUCATION='" . $education . "', QUALIFICATIONS='" . $qualification . "', BOARDCERTIFICATIONS='" . $boardCertification .  "', PRACTITIONER_PROFESSIONALMEMBERSHIP='" . $practitioner_professionalmembership .  "', AWARDSANDPUBLICATIONS='" . $awardsandpublications .  "', HOSPITALAFFILIATION='" . $hospitalaffiliation . "', C_DEGREE='$cdegree', C_TRAINED='$ctrained' WHERE PRACTITIONER_NUMBER=" . $id;
				mysqli_query($conn,$query);
				if($address != ""){
					$address = $address . ", " . $suburb . " " .$postCode;
					$tempLoc= convertAddressToLngLat($address) . ";";
					//echo $tempLoc . "<br>";
					@mysqli_query($conn,"UPDATE PRACTITIONER SET PRACTITIONER_BUSINESSGOOGLEMAPADDRESS='" . $tempLoc . "' WHERE PRACTITIONER_NUMBER='" . $row['PRACTITIONER_NUMBER'] . "'");
				}
				?>
				<script language="javascript"> 
					window.location = "";
				</script>
				<?php
			}
			?>
		<div style="display:none;">
		<?php
		if(isset($_GET['id'])){
			$temp=explode("'",$_GET['id']);
			$id=$temp[0];

			echo "<select id='currency' name='currency' onchange=\"updateCurrency(currency.value,$id)\">";
		}
		else
			echo "<select id='currency' name='currency' onchange=\"updateCurrency(currency.value,0)\">";
		foreach($items as $item){
			if($item[0]!=$_COOKIE["Currency"])
				echo "<option label='{$item[0]}'>{$item[0]}</option>";
			else
				echo "<option label='{$item[0]}' selected='selected'>{$item[0]}</option>";
			if($item[0]=="AUD")
				echo $AUD=$item[3];
		}
		echo "</select>";
		?>
		</div>
		<div class="gborder" style='height:50px;'>
		<table border=0>
			<tr>
				<!--<th>Number</th>-->
				<th colspan=2 style='width:170px;'>Surgery Facilitator Service(s)</th>
				<?php 
				if(isset($_GET['id'])) {
					echo "<th style='width:100px;text-align:center;'>Price</th>";
					echo "<th style='width:100px;text-align:center'><img src='{$path}images/cards_logo.png' height=12px /></th>";
				}
				else
					echo "<th colspan=3 style='width:100px;text-align:center'>Price</th>";
				?>
			</tr>
			</table>
			<?php if(!isset($_GET['id'])){ ?>
				<div style='overflow:auto;height:200px;'>
			<?php }else{ ?>
				<div style='overflow:auto;height:220px;'>
			<?php } ?>
			<table border=0>
			<?php
			$counter=0;
			
    		$servicesList=getServices($conn,$row['PRACTITIONER_NUMBER']);
			if($servicesList!=null){
				while ($list=mysqli_fetch_array($servicesList)){
					$counter++;
					echo "<tr>";
					//echo "<tr><td>" . $counter . "</td>";
					$query="SELECT TECHNICALPROCEDURENAME,WIKIPEDIALINK,WIKIPEDIAPDFGENERATORLINK,WIKIPEDIAFINALPDF FROM MASTERSERVICE WHERE MASTERSERVICE_NUMBER='" . $list['MASTERSERVICE_NUMBER'] . "'";
					$result=mysqli_query($conn,$query);
					if(mysqli_num_rows($result)!=0){
						$techName=mysqli_fetch_array($result);
						echo "<td style='width:150px;'> $techName[0] </td>";
						
					}
					if(isset($_GET['serviceId']))
					{
						if($_GET['serviceId']==$list[1])
						{
							echo "<form method='post' action='index.php'>";
							echo "<input type='hidden' name='serviceId' value='" . $list[1] . "'>";
							echo "<td><input type='text' name='newPrice' value='" . $list['PRICE'] . "'></td>";
							echo "<td><input type='submit' value='update'></td>";
							echo "</form>";
							echo "</tr>";
						}
						else
						{
							echo "<td>" . $list['PRICE'] . "</td></tr>";
						}
					}
					elseif(isset($_GET['deleteId']))
					{
						if($_GET['deleteId']==$list[1])
						{
							echo "<td>" . $list['PRICE'] . "</td>";
							echo "<td>Are you sure want to delete this service?</td>";
							echo "<td><a href='index.php?delete=" . $list[1] . "'>Yes</a></td>";
							echo "<td><a href='index.php'>No</a></td>";
						}
						else
						{
							echo "<td>" . $list['PRICE'] . "</td>";
						}
					}
					else
					{
						if($list['PRICE']>0){
							$aud=$_COOKIE["CurrencyAUD"];
							$usd=$_COOKIE["CurrencyUSD"];
							$cur=getCurSymbol($conn,$rowSession['COUNTRY_NUMBER']);
							$curPrice=($list['PRICE']*$aud)*$usd;
							echo "<td align='right' style='width:100px; text-align:right'>" . $cur . "" . number_format($curPrice , 0 , '.' , ',' ) . "</td>";
						}else
							echo "<td align='right' style='width:100px'>Please call for price</td>";
						if(!isset($_GET['id']))
						{
							//echo "<td><a href='practitioner_profile.php?serviceId=" . $list[1] . "'>Update</a></td>";
							//echo "<td><a href='practitioner_profile.php?deleteId=" . $list[1] . "'>Delete</a></td></tr>";
						}
						else
						{
							echo "<td align='center' style='width:100px;padding-left:15px'>";
							//echo "<a href='paypal/purchaseSurgery.php?amount=" . $list['PRICE'] . "&email=" . $row['EMAILADDRESS'] . "'><img src='images/buy_now_button.png' height=20px /></a>";
							if($list['PRICE']>0)
								echo "<span title='Coming Soon - purchase and pay for surgery facilitator'><img src='{$path}images/addToCartButton.jpg' style='height:17px'></span>";
								// echo "<div class='addToCartDiv'><a href='' onclick='addToChart({$list['SERVICES_NUMBER']},\"".$techName[1]."\",{$list['PRICE']});return false'><img src='{$path}images/addToCartButton.jpg' style='height:17px'></a><div class='addToCartDivPopUp Indices' style='text-align:left;'>Add this Practitioner Service to your Wotmed Services Cart.  When you are finished adding services you can book your flights, hotel and car hire and also pay for your medical surgery travel package quickly and easily</div></div>";
							echo "</td></tr>";
						}
					}
				}
			}
			else
				echo "<tr><td colspan='4' align='center'>No service is provided</td></tr>";
		?>
		</table>
		</div>
		</div>
		<br><br>
		<?php
			if(!isset($_GET['id']))
			{ ?>
		<div>
		<?php /*
			<div><strong>Add New Service</strong></div>
			<div>
				<form method="post" action="practitioner_profile.php">
					<?php
					$query="SELECT * FROM MASTERCATEGORY";
					$result=mysqli_query($conn,$query);
					echo "<select id='category' onchange=\"updateServiceList(this.value,document.getElementById('subcategory').value)\">";
					echo "<option value=''>Any</option>";
					while($catList=mysqli_fetch_array($result)){
						echo "<option value='" . $catList['CATEGORY_NUMBER'] . "'>" . $catList['CATEGORY'] . "</option>";
					}
					echo "</select>";

					$query="SELECT * FROM MASTERSUBCATEGORY";
					$result=mysqli_query($conn,$query);
					echo "<select id='subcategory' onchange=\"updateServiceList(document.getElementById('category').value,this.value)\">";
					echo "<option value=''>Any</option>";
					while($subList=mysqli_fetch_array($result)){
						echo "<option value='" . $subList['MASTERSUBCATEGORY_NUMBER'] . "'>" . $subList['SUBCATEGORY'] . "</option>";
					}
					echo "</select>";
					?>
					<br/>
					<select name="service" id="service" style="width:280px;">
						<?php
						$query="SELECT MASTERSERVICE_NUMBER,TECHNICALPROCEDURENAME FROM MASTERSERVICE";
						$result=mysqli_query($conn,$query);
						while($listOfServie=mysqli_fetch_array($result)){
							echo "<option value='" . $listOfServie['MASTERSERVICE_NUMBER'] . "'>" . $listOfServie['TECHNICALPROCEDURENAME'] . "</option>";
						}
						?>
					</select>
					Price : $<input type="text" name="price"/>
					<input type="submit" value="Add"/>
				</form>
			</div>*/?>
			
			
		</div>
		<div class="gborder">
			<div style="border-bottom:1px solid #AAA;padding-bottom:5px;margin-bottom:5px;">
				<img src="<?php echo $path; ?>images/PatientRecommendation.png" style="width:30px;"> 
				<b style='color:#325199;font-size:16px'>Patients who have Recommended you</b><br>
				<span><?php echo (isset($_GET['id']))?"This practitioner has":"You have"; ?> been recommended by these Wotmed Patient(s):</span>
			</div>
			<div id="feedback" style="overflow:auto;max-height:300px;width:100%;">
				<table border="0" style="width:100%;" cellspacing="0">
				<?php
					$limit=5;
					$feedback = getFeedback($conn,$_SESSION['id'],$limit);
					if($feedback!=null){
						while($userComments=mysqli_fetch_array($feedback)){
							$temp = getParticipantDetail($conn,$userComments['PARTICIPANT_NUMBER']);
							if($temp['PROFILEPHOTO']=="" || $userComments['FUTURECONTACT']!=1){
								$pp="blankSilhouetteMale.png";
							}else{
								$pp=$temp['PROFILEPHOTO'];
							}
							echo "<tr>
									<td rowspan='2' style='width:90px;border-bottom:1px solid #AAA;'><img src='{$path}photos/thumbs/{$pp}' width='75px' /></td>";
							$name="Private User";
							if($userComments['FUTURECONTACT']!=1){
								echo "<td style='width:250px;padding-top:10px;'><b>$name</b></td>";
							}
							else{
								$name=$temp['FIRSTNAME']." ".$temp['SURNAME'];
								if($_SESSION['id']==$userComments['PARTICIPANT_NUMBER'])
									$name.=" (You)";
								echo "<td 	style='width:250px;padding-top:10px;'><b><a href='{$path}profile.php?id={$temp['PARTICIPANT_NUMBER']}'>$name</a></b></td>";
							}
							if($userComments['FUTURECONTACT']==1){
								echo "<td rowspan='2' align='center' style='border-bottom:1px solid #AAA;'><a href='{$path}profile.php?id={$temp['PARTICIPANT_NUMBER']}'><img src='{$path}images/viewProfile.png' height='30' width='40px'/><br/>View this Wotmed Patients Profile</a></td>";
								//echo "<td rowspan='2' align='center' style='border-bottom:1px solid #AAA;'><a href='mailto:{$temp['EMAILADDRESS']}'><img src='images/contactRecommend.png' height='30' width='40px'/><br/>Email Patient</a></td>";
							}else{
								echo "<td rowspan='2' colspan='2' style='border-bottom:1px solid #AAA;'>&nbsp;</td>";
							}
							echo "</tr>";
							$review=$userComments['COMMENT'];
							if($review=="")
								$review="$name has recommended {$row['PRACTITIONER_BUSINESSNAME']} but has chosen not to provide a writen review";
							echo "<tr>
									<td style='border-bottom:1px solid #AAA;padding-bottom:10px;width:250px;'>$review</td>
								</tr>";
						}
						if($limit<getNumberOfFeedback($conn,$_SESSION['id']))
							echo "<tr>
								<td colspan='3'><a href='' onclick='loadMore(5,{$_SESSION['id']});return false;'>Load more</a></td>
								</tr>";
					}
					else{
						echo "<tr>
							<td  colspan='3'>No recommendations</td>
							</tr>";
					}
				?>
				</table>
			</div>
		</div>
		<BR/>
		<div class="gborder">
			<div style="border-bottom:1px solid #AAA;padding-bottom:5px;margin-bottom:5px;">
				<img src="<?php echo $path; ?>images/PatientRecommendation.png" style="width:30px;"> 
				<b style='color:##325199;font-size:16px'>Patients who have Thanked You</b><br>
				<span>You have been thanked by these Wotmed patient(s):</span>
			</div>
			<div id="thankFeedback" style="overflow:auto;max-height:300px;width:100%;">
				<table border="0" style="width:100%;" cellspacing="0">
				<?php
					$limit=5;
					$feedback = getThankFeedback($conn,$_SESSION['id'],$limit);
					if($feedback!=null){
						while($userComments=mysqli_fetch_array($feedback)){
							$temp = getParticipantDetail($conn,$userComments['PARTICIPANT_NUMBER']);
							if($temp['PROFILEPHOTO']==""){
								$pp="blankSilhouetteMale.png";
							}else{
								$pp=$temp['PROFILEPHOTO'];
							}
							echo "<tr>
									<td rowspan='2' style='width:90px;border-bottom:1px solid #AAA;'><img src='{$path}photos/thumbs/{$pp}' width='75px' /></td>";
							$name=$temp['FIRSTNAME']." ".$temp['SURNAME'];
							if($_SESSION['id']==$userComments['PARTICIPANT_NUMBER'])
								$name.=" (You)";
							echo "<td style='width:250px;padding-top:10px;'><b><a href='{$path}profile.php?id={$temp['PARTICIPANT_NUMBER']}'>$name</a></b></td>";
							echo "<td rowspan='2' align='center' style='border-bottom:1px solid #AAA;'><a href='{$path}profile.php?id={$temp['PARTICIPANT_NUMBER']}'><img src='{$path}images/viewProfile.png' height='30' width='40px'/><br/>View Your Patient Profile</a></td>";
							//echo "<td rowspan='2' align='center' style='border-bottom:1px solid #AAA;'><a href='mailto:{$temp['EMAILADDRESS']}'><img src='images/contactRecommend.png' height='30' width='40px'/><br/>Email Patient</a></td>";
							
							
							
							
							
							
							
							
								echo "<td rowspan='2' align='center' style='border-bottom:1px solid #AAA;'><a href='#' onclick='displayContactUser({$temp['PARTICIPANT_NUMBER']})'><img src='{$path}images/contactRecommend.png' height='30' width='40px'/><br/>Email Patient</a></td>";
							
							
							
							
							
							echo "</tr>";
							$review=$userComments['COMMENT'];
							if($review=="")
								$review="$name has recommended {$row['PRACTITIONER_BUSINESSNAME']} but has chosen not to provide a writen review";
							echo "<tr>
									<td style='border-bottom:1px solid #AAA;padding-bottom:10px;width:250px;'>$review</td>
								</tr>";
						}
						if($limit<getNumberOfThanks($conn,$_SESSION['id']))
							echo "<tr>
								<td  colspan='3'><a href='#' onclick='loadMoreThank(5,{$_SESSION['id']});return false;'>Load more</a></td>
								</tr>";
					}
					else{
						echo "<tr>
							<td  colspan='3'>No patient has thanked this Surgery Facilitator yet</td>
							</tr>";
					}
				?>
				</table>
			</div>
		</div>
		<?php
		}
		else
		{ ?>

            <?php

            $facilitatorNumber = $row['PRACTITIONER_NUMBER'];


            echo " <div style='border-bottom:0px solid #AAA;padding-bottom:5px;margin-bottom:20px;'>";

            //     echo"<a href='booking.php?practitionerId=$practitionerNumber&date=2015-06-26&time=09:00'><img src='BookPractitionerButton6.jpg'  /><br/></a>";

            echo"<a href='booking.php?practitionerId=$facilitatorNumber&date=2015-06-26&time=09:00'> <p class='centeredImage'><img src='BookSurgeryFacilitator.jpg' alt='Book Surgery Facilitator'></p><br/></a>";



            echo "</div>";


            ?>



		<div class="gborder">
			<div style="border-bottom:1px solid #AAA;padding-bottom:5px;margin-bottom:5px;">
				<img src="<?php echo $path; ?>images/PatientRecommendation.png" style="width:30px;"> 
				<b style='color:#325199;font-size:16px'>Patients who have Recommended this Surgery Facilitator</b><br>
				<span>This Surgery Facilitator has been recommended by these Wotmed patient(s):</span>
			</div>
			<div id="feedback" style="overflow:auto;max-height:300px;width:100%;">
				<table border="0" style="width:100%;" cellspacing="0">
				<?php
					$limit=5;
					$feedback = getFeedback($conn,$_GET['id'],$limit);
					if($feedback!=null){
						while($userComments=mysqli_fetch_array($feedback)){
							$temp = getParticipantDetail($conn,$userComments['PARTICIPANT_NUMBER']);
							if($temp['PROFILEPHOTO']=="" || $userComments['FUTURECONTACT']!=1){
								$pp="blankSilhouetteMale.png";
							}else{
								$pp=$temp['PROFILEPHOTO'];
							}
							echo "<tr>
									<td rowspan='2' style='width:90px;border-bottom:1px solid #AAA;'><img src='{$path}photos/thumbs/{$pp}' width='75px' /></td>";
							$name="Private User";
							if($userComments['FUTURECONTACT']!=1){
								echo "<td style='width:250px;padding-top:10px;'><b>$name</b></td>";
							}
							else{
								$name=$temp['FIRSTNAME']." ".$temp['SURNAME'];
								if($_SESSION['id']==$userComments['PARTICIPANT_NUMBER'])
									$name.=" (You)";
								echo "<td 	style='width:250px;padding-top:10px;'><b><a href='{$path}profile.php?id={$temp['PARTICIPANT_NUMBER']}'>$name</a></b></td>";
							}
							if($userComments['FUTURECONTACT']==1){
								echo "<td rowspan='2' align='center' style='border-bottom:1px solid #AAA;'><a href='{$path}profile.php?id={$temp['PARTICIPANT_NUMBER']}'><img src='{$path}images/viewProfile.png' height='30' width='40px'/><br/>View Your Patient Profile</a></td>";
								echo "<td rowspan='2' align='center' style='border-bottom:1px solid #AAA;'><a href='#' onclick='displayContactUser({$temp['PARTICIPANT_NUMBER']})'><img src='{$path}images/contactRecommend.png' height='30' width='40px'/><br/>Email Patient</a></td>";
							}else{
								echo "<td rowspan='2' colspan='2' style='border-bottom:1px solid #AAA;'>&nbsp;</td>";
							}
							echo "</tr>";
							$review=$userComments['COMMENT'];
							if($review=="")
								$review="$name has recommended {$row['PRACTITIONER_BUSINESSNAME']} but has chosen not to provide a writen review";
							echo "<tr>
									<td style='border-bottom:1px solid #AAA;padding-bottom:10px;width:250px;'>$review</td>
								</tr>";
						}
						if($limit<getNumberOfFeedback($conn,$_GET['id']))
							echo "<tr>
								<td  colspan='3'><a href='' onclick='loadMore(5,{$_GET['id']});return false;'>Load more</a></td>
								</tr>";
					}
					else{
						echo "<tr>
							<td  colspan='3'>No recommendations have yet been made for this Surgery Facilitator</td>
							</tr>";
					}
				?>
				</table>
			</div>
		</div>
		<BR/>
		<div class="gborder">
			<div style="border-bottom:1px solid #AAA;padding-bottom:5px;margin-bottom:5px;">
				<img src="<?php echo $path; ?>images/PatientRecommendation.png" style="width:30px;"> 
				<b style='color:#325199;font-size:16px'>Patients who have Thanked this Surgery Facilitator</b><br>
				<span>This Surgery Facilitator has been thanked by these Wotmed patient(s):</span>
			</div>
			<div id="thankFeedback" style="overflow:auto;max-height:300px;width:100%;">
				<table border="0" style="width:100%;" cellspacing="0">
				<?php
					$temp=explode("'",$_GET['id']);
					$id=$temp[0];

					$limit=5;
					$feedback = getThankFeedback($conn,$id,$limit);
					if($feedback!=null){
						while($userComments=mysqli_fetch_array($feedback)){
							$temp = getParticipantDetail($conn,$userComments['PARTICIPANT_NUMBER']);
							if($temp['PROFILEPHOTO']==""){
								$pp="blankSilhouetteMale.png";
							}else{
								$pp=$temp['PROFILEPHOTO'];
							}
							echo "<tr>
									<td rowspan='2' style='width:90px;border-bottom:1px solid #AAA;'><img src='{$path}photos/thumbs/{$pp}' width='75px' /></td>";
							$name=$temp['FIRSTNAME']." ".$temp['SURNAME'];
							if($_SESSION['id']==$userComments['PARTICIPANT_NUMBER'])
								$name.=" (You)";
							echo "<td style='width:250px;padding-top:10px;'><b><a href='{$path}profile.php?id={$temp['PARTICIPANT_NUMBER']}'>$name</a></b></td>";
							echo "<td rowspan='2' align='center' style='border-bottom:1px solid #AAA;'><a href='{$path}profile.php?id={$temp['PARTICIPANT_NUMBER']}'><img src='{$path}images/viewProfile.png' height='30' width='40px'/><br/>View Your Patient Profile</a></td>";
							echo "<td rowspan='2' align='center' style='border-bottom:1px solid #AAA;'><a href='#' onclick='displayContactUser({$temp['PARTICIPANT_NUMBER']})'><img src='{$path}images/contactRecommend.png' height='30' width='40px'/><br/>Email Patient</a></td>";
							
							echo "</tr>";
							$review=$userComments['COMMENT'];
							if($review=="")
								$review="$name has recommended {$row['PRACTITIONER_BUSINESSNAME']} but has chosen not to provide a writen review";
							echo "<tr>
									<td style='border-bottom:1px solid #AAA;padding-bottom:10px;width:250px;'>$review</td>
								</tr>";
						}
						if($limit<getNumberOfThanks($conn,$_GET['id']))
							echo "<tr>
								<td  colspan='3'><a href='#' onclick='loadMoreThank(5,{$_GET['id']});return false;'>Load more</a></td>
								</tr>";
					}
					else{
						echo "<tr>
							<td  colspan='3'>No thanks available for this practitioner</td>
							</tr>";
					}
				?>



				</table>
			</div>
		</div>
		<?php
		}?>
	</div>
</div>
