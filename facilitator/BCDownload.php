<?php 
session_start();
$path="../";
include $path."includes/connect.php";
include $path."includes/functions.php";
$path="../";
include($path."classes/MPDF57/mpdf.php");
if(isset($_GET['target']))
	$target = $_GET['target'];
else
	$target = "en";
if(isset($_GET['id']))
	$id=$_GET['id'];
else
	$id=$_SESSION['id'];
$header= Array( 'BUSINESSCARD_NUMBER','PRACTITIONER_NUMBER','BUSINESSNAME','BUSINESSNUMBER','BUSINESSEMAIL','BUSINESSWEBSITEURL','BUSINESSADDRESS','BUSINESSSUBURB','BUSINESSPOSTCODE','BUSINESSCITY','COUNTRY_NUMBER','FRONT','BACK','STATUS');
$rowHeader= Array( '','PRACTITIONER_NUMBER','PRACTITIONER_BUSINESSNAME','PRACTITIONER_BUSINESSNUMBER','EMAILADDRESS','WEBSITEURL','PRACTITIONER_BUSINESSADDRESS','PRACTITIONER_BUSINESSSUBURB','PRACTITIONER_BUSINESSPOSTCODE','','B_COUNTRY_NUMBER','','','');

$row=getPractitionerDetail($conn,$id);
$practitionerId=$row['PRACTITIONER_NUMBER'];
$query="SELECT * FROM BUSINESSCARD WHERE PRACTITIONER_NUMBER='$practitionerId'";
$result=mysqli_query($conn,$query);

if(mysqli_num_rows($result)==0){
	$temp=getPractitionerDetail($conn,$id);
	for($i=0;$i<count($header);$i++){
		if($i>0 && $i<count($header)-3 && $i!=count($header)-5)
			$data[$header[$i]]=$temp[$rowHeader[$i]];
		else
			$data[$header[$i]]="";
	}
	$data['UNITNUMBER']="";
	$countryName=getCountryName($conn,$data['COUNTRY_NUMBER']);
	$address=$temp["PRACTITIONER_BUSINESSADDRESS"] . ", {$data['BUSINESSSUBURB']} {$temp['PRACTITIONER_BUSINESSPOSTCODE']}, {$data['BUSINESSCITY']}, {$countryName} ";
	$longlat=explode(",",convertAddressToLngLat($address));
}
else{
	$c=0;
	while($details=mysqli_fetch_array($result)){
		for($i=0;$i<count($header);$i++){
			if($header[$i]=="BUSINESSADDRESS"){
				$temp=explode(";",$details[$i]);
				$data["UNITNUMBER"]=$temp[0];
				$data[$header[$i]]=$temp[1];
			}	
			else
				$data[$header[$i]]=$details[$i];
		}
	}
	$countryName=getCountryName($conn,$data['COUNTRY_NUMBER']);
	$address=implode(" ",$temp) . ", {$data['BUSINESSSUBURB']}, {$data['BUSINESSCITY']}, {$countryName} ";
	$longlat=explode(",",convertAddressToLngLat($address));
}

$mpdf = new mPDF($target);

$html ="<div>";

if($data['FRONT']!=""){
	$html .= "<p>Practitioner original front business card</p>
	<img src='{$path}BusinessCard/{$data['FRONT']}' style='max-width:100px;'/><br>";
}
if($data['BACK']!=""){
	$html .= "<p>Practitioner original back business card</p>
	<img src='{$path}BusinessCard/{$data['BACK']}' style='max-width:100px;'/><br>";
}
$bcTitle = Array('Independent Website Address','Wotmed Profile Link','Email Address','Phone Number','Business Address');
if($target!='en'){
	for($i=0;$i<count($bcTitle);$i++){
    	$bcTitle[$i] =  transTo($target,$bcTitle[$i]);
	}
}
	$html .="<br>
		<div>
		<table style='width:100%'>
			<tr>
				<td style='width:20%'><img src='{$path}images/WotMedLogoMedium.jpg' style='width:100px;'></td>
				<td style='width:40%;text-align:center'><div>{$data['BUSINESSNAME']}</div></td>
				<td style='width:40%;' align=right><img src='{$path}photos/thumbs/{$row['PRACTITIONER_BUSINESSLOGO']}' style='max-width:100px;' ></td>
			</tr>
		</table>
		<table style='width:100%'>
			<tr >
				<td style='padding-right: 25px;'>{$bcTitle[0]}</td>
				<td>{$data['BUSINESSWEBSITEURL']}</td>
			</tr>
			<tr>
				<td style='padding-right: 25px;'>{$bcTitle[1]}</td>
				<td>http://platform.wotmed.com/practitioner_profile.php?id=$id</td>
			</tr>
			<tr>
				<td style='padding-right: 25px;'>{$bcTitle[2]}</td>
				<td>{$data['BUSINESSEMAIL']}</td>
			</tr>
			<tr>
				<td style='padding-right: 25px;'>{$bcTitle[3]}</td>
				<td>{$data['BUSINESSNUMBER']}</td>
			</tr>
			<tr>
				<td style='padding-right: 25px;'>{$bcTitle[4]}</td>
				<td>$address</td>
			</tr>
		</table>
	</div>";
	if($target!='en')
		$html .= "<p>".transTo($target,"Wotmed is the medical network that connects Patients with Practitioners and Surgery Facilitators globally")."</p>";
	else
		$html .= "<p>Wotmed is the medical network that connects Patients with Practitioners and Surgery Facilitators globally</p>";
$html .= "</div>";
$mpdf->WriteHTML($html);
$mpdf->Output();
?>
