<?php

//ini_set('display_errors',1);
//ini_set('display_startup_errors',1);
//error_reporting(-1);

	session_start();
	
	header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
	
	$path="../";
	include $path."includes/connect.php";
	include $path."includes/functions.php";
	$days=array("Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday");
	$path="../";
	$items=readFromXML($path);
	if(isset($_GET['delete']))
	{	
		$serviceId=mysqli_real_escape_string($conn,$_GET['delete']);
		$id=$_SESSION['practitioner_id'];
		$query="DELETE FROM SERVICES WHERE PRACTITIONER_NUMBER='" . $id . "' AND MASTERSERVICE_NUMBER='" . $serviceId . "'";
		mysqli_query($conn,$query);
	}
	if(isset($_POST['serviceId']))
	{
		$serviceId=mysqli_real_escape_string($conn,$_POST['serviceId']);
		$newPrice=mysqli_real_escape_string($conn,$_POST['newPrice']);
		$id=$_SESSION['practitioner_id'];
		$query="UPDATE SERVICES SET PRICE='" . $newPrice . "' WHERE PRACTITIONER_NUMBER='" . $id . "' AND MASTERSERVICE_NUMBER='" . $serviceId . "'";
		mysqli_query($conn,$query);
	}
	if(isset($_POST['price']))
	{
		$service=mysqli_real_escape_string($conn,$_POST['service']);
		$price=mysqli_real_escape_string($conn,$_POST['price']);
		$id=$_SESSION['practitioner_id'];
		$query="INSERT INTO SERVICES (PRACTITIONER_NUMBER,MASTERSERVICE_NUMBER,PRICE) VALUES ('" . $id . "','" . $service . "','" . $price . "');";
		if($service!=-1)
			mysqli_query($conn,$query);
	}
	if(isset($_SESSION['practitioner_id'])){
		$row=getPractitionerDetail($conn,$_SESSION['id']);
		$servicesList=getServices($conn,$_SESSION['practitioner_id']);
		$query="SELECT * FROM AVAILABILITY WHERE PRACTITIONER_NUMBER ='" . $_SESSION['practitioner_id'] . "' ORDER BY TIMESTART";
		//msgBox($query);
		$schedules=mysqli_query($conn,$query);
		$query="SELECT COUNT(*) FROM PATIENTPRACTITIONERRELATIONSHIP WHERE PRACTITIONER_NUMBER = '" . $_SESSION['practitioner_id'] . "'";
		$tempOfPatients=mysqli_query($conn,$query);
		if(mysqli_num_rows($tempOfPatients)!=0)
			$numbOfPatients=mysqli_fetch_array($tempOfPatients);
		else
			$numbOfPatients[]=0;
		//echo "<br><br><BR><BR><BR>".$query;
		$query="SELECT COUNT(*) FROM PATIENTPRACTITIONERRELATIONSHIP WHERE PRACTITIONER_NUMBER = '" . $_SESSION['practitioner_id'] . "' AND RECOMMENDEDAS='1'";
		$tempOfRecommend=mysqli_query($conn,$query);
		if(mysqli_num_rows($tempOfRecommend)!=0)
			$numbOfRecommend=mysqli_fetch_array($tempOfRecommend);
		else
			$numbOfRecommend[]=0;
	}
	if(!isset($_SESSION['id'])){
		?>
		<script language="javascript"> 
			<?php echo "window.location = '../login.php'";?>
		</script> <?php
		?><?php
	}
	if(isset($_SESSION['id'])){
		$rowSession=getParticipantDetail($conn,$_SESSION['id']);
		$row=getPractitionerDetail($conn,$_SESSION['id']);
		$query="SELECT CURRENCY_CODE FROM COUNTRY WHERE COUNTRY_NUMBER = '" . $rowSession['COUNTRY_NUMBER'] ."'";
		$localCur=mysqli_query($conn,$query);
		if(mysqli_num_rows($localCur)!=0)
			$curCurrency=mysqli_fetch_array($localCur);
			
		if(!isset($_COOKIE["Currency"])){
			setcookie("Currency", $curCurrency[0], time()+36000000,"/");
		}else{
			//setcookie("Currency", $_COOKIE['Currency'], time()+3600,"/");	
		}
		
		if(!isset($_COOKIE["CurrencyAUD"]) || !isset($_COOKIE["CurrencyUSD"])){			
			foreach($items as $item){
				if($item[0]=="AUD"){
					$curValue=$item[3];
					setcookie("CurrencyAUD", $curValue, time()+36000000,"/");
				}
				if($item[0]==$curCurrency[0]){
					$curValue=$item[2];
					setcookie("CurrencyUSD", $curValue, time()+36000000,"/");
				}
			}/*
			?>
			<script language="javascript"> 
				<?php //echo "window.location = 'practitioner_profile.php'";?>
			</script> <?php
			?><?php*/
		}
	}
	if(isset($_GET['id'])){
		$temp=explode("'",$_GET['id']);
		$id=$temp[0];

		$query="SELECT CURRENCY_CODE FROM COUNTRY WHERE COUNTRY_NUMBER = '" . $rowSession['COUNTRY_NUMBER'] ."'";
		$localCur=mysqli_query($conn,$query);
		if(mysqli_num_rows($localCur)!=0)
			$curCurrency=mysqli_fetch_array($localCur);
			
		if(!isset($_COOKIE["Currency"])){
			setcookie("Currency", $curCurrency[0], time()+36000000,"/");
		}else{
			//setcookie("Currency", $_COOKIE['Currency'], time()+3600,"/");	
		}
		
		if(!isset($_COOKIE["CurrencyAUD"]) || !isset($_COOKIE["CurrencyUSD"])){			
			foreach($items as $item){
				if($item[0]=="AUD"){
					$curValue=$item[3];
					setcookie("CurrencyAUD", $curValue, time()+36000000,"/");
				}
				if($item[0]==$curCurrency[0]){
					$curValue=$item[2];
					setcookie("CurrencyUSD", $curValue, time()+36000000,"/");
				}
			}
			?>
			<script language="javascript"> 
				<?php echo "window.location = 'practitioner_profile.php?id=$id'";?>
			</script> <?php
			?><?php
		}
		
		$row=getPractitionerDetail($conn,$id);
		increaseViewCount($conn,$id);
		$servicesList=getServices($conn,$row['PRACTITIONER_NUMBER']);
		$query="SELECT * FROM AVAILABILITY WHERE PRACTITIONER_NUMBER ='" . $row['PRACTITIONER_NUMBER'] . "'";
		$schedules=mysqli_query($conn,$query);
		$query="SELECT COUNT(*) FROM PATIENTPRACTITIONERRELATIONSHIP WHERE PRACTITIONER_NUMBER = '" . $row['PRACTITIONER_NUMBER'] . "'";
		$tempOfPatients=mysqli_query($conn,$query);
		if(mysqli_num_rows($tempOfPatients)!=0)
			$numbOfPatients=mysqli_fetch_array($tempOfPatients);
		else
			$numbOfPatients[]=0;
		$query="SELECT COUNT(*) FROM PATIENTPRACTITIONERRELATIONSHIP WHERE PRACTITIONER_NUMBER = '" . $row['PRACTITIONER_NUMBER'] . "' AND RECOMMENDED='1'";
		$tempOfRecommend=mysqli_query($conn,$query);
		if(mysqli_num_rows($tempOfRecommend)!=0)
			$numbOfRecommend=mysqli_fetch_array($tempOfRecommend);
		else
			$numbOfRecommend[]=0;
		$query="SELECT SKYPEUSERNAME FROM PARTICIPANT WHERE PARTICIPANT_NUMBER IN (SELECT PARTICIPANT_NUMBER FROM PRACTITIONER WHERE PRACTITIONER_NUMBER = '" . $row['PRACTITIONER_NUMBER'] . "')";
		$result=mysqli_query($conn,$query);
		if(mysqli_num_rows($result)!=0)
			$skypeName=mysqli_fetch_array($result);
		else
			$skypeName=NULL;
		$query="SELECT EMAILADDRESS FROM PARTICIPANT WHERE PARTICIPANT_NUMBER IN (SELECT PARTICIPANT_NUMBER FROM PRACTITIONER WHERE PRACTITIONER_NUMBER = '" . $row['PRACTITIONER_NUMBER'] . "')";
		$result=mysqli_query($conn,$query);
		if(mysqli_num_rows($result)!=0)
			$email=mysqli_fetch_array($result);
		else
			$email=NULL;
	}

// IMPORTANT:
// Save the Surgery Facilitators Number in the session variable
// so as to pass it onto the next page for the shopping cart

// $_SESSION['SurgeryFacilitatorNumber'] = $row['PRACTITIONER_NUMBER'];

$_SESSION['SurgeryFacilitatorNumber'] = $id;

?>
<!DOCTYPE html PUBLIC
"-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
    
    






		<title><?php echo $row['PRACTITIONER_BUSINESSNAME']; ?>'s Wotmed Surgery Facilitator Profile</title> 
		<?php include $path."meta_facilitator.php"; ?>
		<meta http-equiv="Content-Type" content="'text'/html; charset=utf-8" />
		<link href="<?php echo $path; ?>style/p_style.css" rel="stylesheet"></link>
		<link href="<?php echo $path; ?>style/apple.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" type="text/css" href="<?php echo $path; ?>gallery/style.css" />
		<link href="<?php echo $path; ?>style/jquery.autocomplete.css" rel="stylesheet"></link>
		<script type="text/javascript" src="<?php echo $path; ?>classes/jquery.js"></script>
		<script type="text/javascript" src="<?php echo $path; ?>classes/jquery.autocomplete.js"></script>
		<script type="text/javascript" src="<?php echo $path; ?>classes/autocomplete.js"></script>
		<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>
		
		<link href="<?php echo $path; ?>style/drawer.css" rel="stylesheet"></link>
		<script src="<?php echo $path; ?>classes/jquery.dimensions.js" type="text/javascript"></script>
		<script src="<?php echo $path; ?>classes/jquery.accordion.js" type="text/javascript"></script>
		<script src="<?php echo $path; ?>classes/hover.js" type="text/javascript"></script>
		<script type="text/javascript" src="<?php echo $path; ?>classes/userInfo.js"></script>
		<script type="text/javascript"
		  src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCtaXJ8ECL-qmxpuKQc5HCmXK9VxF8uiW4&sensor=true">
		</script>
		<script type="text/javascript" src="<?php echo $path; ?>classes/maps.js">
		</script>
		<script type="text/javascript">
		function addToChart(item_id,item_name,item_price){
			$('#addItem').css('display','none');
			$.ajax({
			type: "POST",
			url: "shoppingChart/addToChart.php",
			data: { item_id : item_id, item_name : item_name, item_price : item_price}
			}).done(function ( msg ) {
				result=msg.split("|");
				$('#SCQty').html(result[0]);
				$('#addItem').stop().css('display','block').css('opacity','1');
				$('#addItem').html(result[1]);
				setTimeout(function() { $('#addItem').fadeOut(500); }, 1500);
				// load the modal window
				//alert(msg);
				return false;
			});
		}
		$(function () {
			$('ul.drawers').accordion({
				header: 'H2.drawer-handle',
				selectedClass: 'open',
				event: 'mouseover'
			});
			$(".detailWrapper").css("display","none");
			$("#AboutWrapper").css("display","block");
			
			$("#trustText").mouseenter(function() {
				if(!trustLoaded)
					loadTrustList(<?php echo isset($_GET['id'])?$id:$_SESSION['id']; ?>);
				trustLoaded=true;
			});
		}); 
		function displayBlock(blockName){
			$(".detailWrapper").css("display","none");
			$("#"+blockName).css("display","block");
		}
		</script>
		
		<script language="javascript"> 
		trustLoaded=false;
		function showVisaReq(origURL,destURL){
			window.open(origURL,'_blank');
			window.open(destURL,'_blank');
		}
		function loadMore(limit,pracId){
			$.ajax({
			type: "POST",
			url: "ajax/loadMoreFeedback.php",
			data: { limit: limit, pracId: pracId }
		}).done(function ( msg ) {
			$('#feedback').html(msg);
			//alert(msg);
			});
		}
		function loadTrustList(id){
			$.ajax({
			type: "GET",
			url: "ajax/trustList.php?id="+id
		}).done(function ( msg ) {
			//$('#trustTextPopUp').html(msg);
			//alert(msg);
			});
		}
		function loadMoreThank(limit,pracId){
			$.ajax({
			type: "POST",
			url: "ajax/loadMoreThanks.php",
			data: { limit: limit, pracId: pracId }
		}).done(function ( msg ) {
			$('#thankFeedback').html(msg);
			
			});
		}
		function updateCurrency(target,id)
		{
			//alert(target);
			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp=new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp.onreadystatechange=function()
			{
				if (xmlhttp.readyState==4 && xmlhttp.status==200)
				{
					//document.getElementById('res').innerHTML=xmlhttp.responseText;
					if(id==0)
						window.location = "practitioner_profile.php";
					else
						window.location = "practitioner_profile.php?id="+id;
				}
			}
			xmlhttp.open("GET","ajax/updateCurrency.php?target="+encodeURIComponent(target),true);
			xmlhttp.send();
		}
		function updateServiceList(categoryNumber,subCategoryNumber)
		{
			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp=new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp.onreadystatechange=function()
			{
				if (xmlhttp.readyState==4 && xmlhttp.status==200)
				{
					document.getElementById("service").innerHTML=xmlhttp.responseText;
				}
			}
			xmlhttp.open("GET","ajax/updateServiceList.php?catNumber="+categoryNumber+"&subCatNumber="+subCategoryNumber,true);
			xmlhttp.send();
		}
		function updateAvailableSchedule(id,order)
		{
			
			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp=new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp.onreadystatechange=function()
			{
				if (xmlhttp.readyState==4 && xmlhttp.status==200)
				{
					document.getElementById("scheduleTable").innerHTML=xmlhttp.responseText;
				}
			}
			xmlhttp.open("GET","ajax/updateAvailableSchedule.php?id="+id+"&order="+order,true);
			xmlhttp.send();
		}
		function updateSearchList(name)
		{
			if(name.length==0){
				document.getElementById("listOfPatients").innerHTML="";
				return;
			}
			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp=new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp.onreadystatechange=function()
			{
				if (xmlhttp.readyState==4 && xmlhttp.status==200)
				{
					document.getElementById("listOfPatients").innerHTML=xmlhttp.responseText;
				}
			}
			xmlhttp.open("GET","ajax/updateSearchList.php?name="+name,true);
			xmlhttp.send();
		}
		function sleep(milliseconds) {
		  var start = new Date().getTime();
		  for (var i = 0; i < 1e7; i++) {
			if ((new Date().getTime() - start) > milliseconds){
			  break;
			}
		  }
		}
		function getPdf(web1,web2)
		{
			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp=new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp.onreadystatechange=function()
			{
				if (xmlhttp.readyState==4)
				{
					//sleep(10000);
					//window.location="http://en.wikipedia.org/w/index.php?title=Special:Book&bookcmd=download&collection_id=" + web2;
				}
			}
			xmlhttp.open("GET","http://en.wikipedia.org/w/index.php?title=Special:Book&bookcmd=render_article&arttitle=" + web1,true);
			xmlhttp.send();
		}
		function toggle(selected_div) {
			var ele = document.getElementById(selected_div);
			if(ele.style.display == "block") {
					ele.style.display = "none";
			}
			else {
				ele.style.display = "block";
			}
		}
		function back()
		{
			window.location = "index.php"
		}
		</script>
		<?php
		$ppFileNameSession="blankSilhouetteMale.png";
		$ppFileName="blankSilhouetteMale.png";
		if($row['PRACTITIONER_BUSINESSLOGO']!=""){
			$ppFileNameSession=$row['PRACTITIONER_BUSINESSLOGO'];
			$ppFileName=$row['PRACTITIONER_BUSINESSLOGO'];
		}
		if(isset($_GET['id']))
			$ppFileNameSession=$rowSession['PROFILEPHOTO'];
		?>
		<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-49954656-1', 'wotmed.com');
  ga('send', 'pageview');

</script>
	</head>

    <SCRIPT TYPE="text/javascript">
        <!--
        function popup(mylink, windowname)
        {
            if (! window.focus)return true;
            var href;
            if (typeof(mylink) == '../signupWizardFacilitator/index.htm')
                href=mylink;
            else
                href=mylink.href;
            window.open(href, windowname, 'width=1200,height=550,scrollbars=yes');
            return false;
        }

        function load()
        {
            window.location.href = "../signupWizardFacilitator/index.htm";

        }

        //-->
    </SCRIPT>

    <body>

    <?php




    $coatOfArms = $row['COUNTRYCOATOFARMS'];

    if(empty($coatOfArms))

 //   if ($coatOfArms === '/blankcoatofarms.gif')

    {
        // surgery facilitator has not yet set their coat of arms so we know that they have logged on most likely
        // for the first time
        // display the wizard
        // test to see if the surgery facilitator has logged on for the first time - if so - display the wizard

        // $coatOfArms = '/blankcoatofarms.gif';
        //  echo $coatOfArms;

        // check to see if we have already run the wizard - if we have - dont run it again
        // otherwise - run it again


        // whoami?
        // this code should only run if the surgery facilitator is logged on as themselves
        // not if a patient is viewing the surgery facilitators profile


        if (!isset($_SESSION['practitioner_id']))
        {
            // we are logged on as the patient so do nothing
        }
        else
        {


        //    if (isset($_SESSION['SurgeryFacilitatorNumber']))  // only run the wizard if the surgery facilitator is logged in as themselves - not if a patient is viewing the surgery facilitator profile
        //    {


                // check if we are logged in as a patient
                // dont run the wizard if we are


                session_start();
                if (isset($_SESSION['SurgeryFacilitatorViews']))
                    $_SESSION['SurgeryFacilitatorViews'] = $_SESSION['SurgeryFacilitatorViews'] + 1;
                else {
                    echo "<body onload='load()'>";
                    $_SESSION['SurgeryFacilitatorViews'] = 1;
                }
                //   echo "views = ". $_SESSION['views'];


       //     }


        }
    }

    ?>



    
		<?php 
	
		$path = "../";
		include $path."includes/p_header.php"; ?>
		<?php
		include "leftColum.php";
		include "midColum.php";
		include "rightColum.php";
		include "botColum.php";
		
		//include "contactSurgeryFacilitator.php";
		
		include "contactUser.php";
		
		
				//	include $path."contactUser.php";
		
		// above code for contact user resulted in broken contact user for Surgery Facilitator
		// php for contact user is not in the includes directory it is in the root of the facilitator directory
		
	//	include "contactUser.php";
		
		
		include $path."thanks.php";
		
		
		
		
	//	include "help3.html";
		?>		
		<?php include $path."facilitator/p_footer.php";



    //    echo $_SESSION['SurgeryFacilitatorNumber'];

     //   echo "test";

     //   echo $coatOfArms;

 //       echo "views = ". $_SESSION['views'];

  //      echo $_SESSION['practitioner_id'];

        ?>


        
        
		<div id="overlayPhoto">
			<div id="overlayContent">
			</div>
		</div>
	</body>
</html>
