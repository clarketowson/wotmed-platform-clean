<?php
	session_start();

ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(-1);


$path="../";
include $path."includes/connect.php";
include $path."includes/functions.php";





	if(isset($_SESSION['id'])){
		$rowSession=getParticipantDetail($conn,$_SESSION['id']);
        $row=getPractitionerDetail($conn,$_SESSION['id']);



	}
	if($rowSession['PROFILEPHOTO']==""){
		$ppFileNameSession="blankSilhouetteMale.png";
	}else{
		$ppFileNameSession=$rowSession['PROFILEPHOTO'];
	}

$ppFileNameSession="blankSilhouetteMale.png";
$ppFileName="blankSilhouetteMale.png";
if($row['PRACTITIONER_BUSINESSLOGO']!=""){
    $ppFileNameSession=$row['PRACTITIONER_BUSINESSLOGO'];
    $ppFileName=$row['PRACTITIONER_BUSINESSLOGO'];
}
if(isset($_GET['id']))
    $ppFileNameSession=$rowSession['PROFILEPHOTO'];



$facilitatorNumber = $_SESSION['SurgeryFacilitatorNumber'];


?>




<html>
<head>

    <script src="../formvalidate/dist/parsley.min.js"></script>


    <link rel="stylesheet" href="../proposals/date/css/pikaday.css">


    <meta name="description" content="Book an Appointment with a Wotmed Surgery Facilitator.  Wotmed.com – Facilitating Medical Travel">
    <meta name="keywords" content="Wotmed.com, Book an Appointment with a Wotmed Surgery Facilitator, Facilitating Medical Travel">

    <meta property="og:locale" content="en_US">
    <meta property="og:type" content="website">
    <meta property="og:title" content="Book an Appointment with a Wotmed Surgery Facilitator">
    <meta property="og:description" content="Book an Appointment with a Wotmed Surgery Facilitator">
    <meta property="og:site_name" content="Wotmed">
    <meta property="og:url" content="http://platform.wotmed.com/facilitator/booking.php">
    <meta property="og:image" content="http://platform.wotmed.com/BookingRequest2.jpg">


    <title>
        Book a Wotmed Surgery Facilitator
    </title>
	<link href="../style/p_style.css" rel="stylesheet"></link>

    <link rel="stylesheet" type="text/css" href="../signup.css">

</head>
<body>



<?php
if(!isset($_SESSION['id'])){
    redirect($path."login.php");
}

?>
<div class="topBar" >
    <div class="bar_frame">

        <a href="http://platform.wotmed.com"><div class="lfloat"><img src="<?php echo $path; ?>../images/WotmedLogoTransparent.png" height="30px"/></a></div>

    <!--   <div class="w_logo"><img src="images/WotmedLogoTransparent.png" height="45" alt="wotmedLogo" style='position:relative; left:-30px; padding:0px;padding-top:10px;' /></div> -->


    <!--<form id="navSearch">
        <div class="uiTypeahead">
            <input type="text" size="50" />
        </div>
    </form>-->
    <?php
    if(isset($_SESSION['CART']))
        $sesItem=count($_SESSION['CART']). " item(s)";
    else
        $sesItem="0 Item(s)";
    ?>
    <div id="nav" class="rfloat">
        <ul>
            <li class="lfloat">
                <?php
                if(isset($_SESSION['practitioner_id'])){
                    echo "<a href='" . $path ."practitioner_profile.php'>";
                }
                else
                    if(isset($_SESSION['id'])){
                        echo "<a href='" . $path ."profile.php'>";
                    }
                ?>

                <div class="tinyDiv"><img class="tinyPhoto" src="<?php echo $path . "../photos/thumbs/" . $ppFileNameSession; ?>"/></div><span class="smallName"><?php echo $rowSession["FIRSTNAME"]; ?></span></a>
            </li>
            <?php
            if(isset($_GET['id'])){ ?>
                <li class='lfloat'>
                    <div id="shopChart" style="width:auto;margin-right:7px;">
                        <a href="<?php echo $path;?>../shoppingChart/chart.php" style="position:relative;">
                            <div style="float:left"><img src="<?php echo $path;?>../images/WotmedShoppingCartGraphic.png" style='height:25px;padding-top:3px;' /></div>
                            <div id="SCQty" style='float:left'><?php echo $sesItem; ?></div>
                        </a>
                        <div id="addItem" style="position: absolute;
									top: 40px;
									background-color: rgb(66,143,137);
									padding: 10px 10px;
									border:2px solid black;
									-moz-border-radius: 10px;
									-webkit-border-radius: 10px;
									-khtml-border-radius: 10px;
									border-radius: 10px;
									display:none;
									width:100px;">item added</div>
                    </div>
                </li>
            <?php } ?>
            <li class="lfloat">
                <a href="<?php echo $path; ?>logout.php"><span class="smallName tinyPhoto">Log Out</span></a>
            </li>
        </ul>
    </div>
</div>
</div>

<div id="container">
    <div id="subcontainer">




	<div class="center">
		<form method="post" action="booking.php">
		<?php

        echo "<div class='Booking' align='left'>";


        echo  "<h1><strong>Book an appointment with this Wotmed Surgery Facilitator</strong></h1>";

        echo "<p> Simply click the Book Now button below to book an appointment with this Wotmed Surgery Facilitator</p>";
        echo "<p> Both the Surgery Facilitator and yourself will receive an email confirmation of your booking.</p>";

        echo "<img src='../BookingRequest2.jpg'>";
        echo "<BR><BR>";


        echo "<div>";
        echo "<BR><BR>";

        echo "</div>";


			if(isset($_POST['reason']))
			{
				$id=mysqli_real_escape_string($conn,$_SESSION['id']);
				$pracId=mysqli_real_escape_string($conn,$_POST['pracId']);
				$date=mysqli_real_escape_string($conn,$_POST['date']);
				$time=mysqli_real_escape_string($conn,$_POST['time']);
				$reason=mysqli_real_escape_string($conn,$_POST['reason']);$query="INSERT INTO BOOKING (PARTICIPANT_NUMBER,PRACTITIONER_NUMBER,DATE,TIME,REASON,STATUS) VALUES ('" . $id . "','" . $pracId . "','" . $date . "','" . $time . "','" . $reason . "','PENDING');";


				mysqli_query($conn,$query);

                $patientFirstName = $rowSession["FIRSTNAME"];
                $patientSurname = $rowSession["SURNAME"];

                $patientEmailAddress = $rowSession["EMAILADDRESS"];

                $appointmentDate    = mysqli_real_escape_string($conn, $_POST['datepicker']);
                $appointmentTime = mysqli_real_escape_string($conn, $_POST['AppointmentTime']);


                // get the practitioners details

                $query="SELECT PARTICIPANT_NUMBER, PRACTITIONER_BUSINESSNAME, PRACTITIONER_BUSINESSNUMBER, PRACTITIONER_BUSINESSADDRESS, PRACTITIONER_BUSINESSSUBURB, PRACTITIONER_BUSINESSPOSTCODE, PRACTITIONER_BUSINESSCITY, PRACTITIONER_BUSINESSSTATE, PRACTITIONER_BUSINESSLOGO FROM PRACTITIONER WHERE PRACTITIONER_NUMBER = $pracId";
                $result = $conn->query($query);

                while($row = $result->fetch_assoc())
                {
                    $participantNumber = $row["PARTICIPANT_NUMBER"];
                    $practitionerBusinessName = $row["PRACTITIONER_BUSINESSNAME"];
                    $practitionerBusinessNumber = $row["PRACTITIONER_BUSINESSNUMBER"];
                    $practitionerBusinessAddress = $row["PRACTITIONER_BUSINESSADDRESS"];
                    $practitionerBusinessSuburb = $row["PRACTITIONER_BUSINESSSUBURB"];
                    $practitionerBusinessCity = $row["PRACTITIONER_BUSINESSCITY"];
                    $practitionerBusinessState = $row["PRACTITIONER_BUSINESSSTATE"];
                    $practitionerBusinessPostCode = $row["PRACTITIONER_BUSINESSPOSTCODE"];
                    $practitionerBusinessLogo = $row["PRACTITIONER_BUSINESSLOGO"];


                //    echo $participantNumber;
                //    echo $practitionerBusinessName;

                }


                $query="SELECT PARTICIPANT_NUMBER, EMAILADDRESS, PROFILEPHOTO FROM PARTICIPANT WHERE PARTICIPANT_NUMBER = $participantNumber";
                $result = $conn->query($query);

        while($row = $result->fetch_assoc())
        {
            $practitionerEmailAddress = $row["EMAILADDRESS"];
            $practitionerProfilePhoto = $row["PROFILEPHOTO"];

        //    echo $practitionerEmailAddress;

        }


                $bannerCode = "
<!-- 3 columns -->
<table width='100%' bgcolor='#f7f7f7' cellpadding='0' cellspacing='0' border='0' id='backgroundTable'>
    <tbody>
    <tr>
        <td>
            <table width='600' cellpadding='0' cellspacing='0' border='0' align='center' class='devicewidth'>
                <tbody>
                <tr>
                    <td width='100%'>
                        <table width='600' cellpadding='0' cellspacing='0' border='0' align='center' class='devicewidth'>
                            <tbody>
                            <tr>
                                <td>
                                        <tr>
                                            <td width='186' height='250' align='center' class='devicewidth'>
                                                <img src='http://platform.wotmed.com/photos/thumbs/$practitionerBusinessLogo' alt='Surgery Facilitator Business Logo' border='0' style='display:block; border:none; outline:none; text-decoration:none;'>
                                            </td>
                                        </tr>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>
<!-- end of 3 columns -->

";

                $bannerCode2 = "
<!-- 3 columns -->
<table width='100%' bgcolor='#f7f7f7' cellpadding='0' cellspacing='0' border='0' id='backgroundTable'>
    <tbody>
    <tr>
        <td>
            <table width='600' cellpadding='0' cellspacing='0' border='0' align='center' class='devicewidth'>
                <tbody>
                <tr>
                    <td width='100%'>
                        <table width='600' cellpadding='0' cellspacing='0' border='0' align='center' class='devicewidth'>
                            <tbody>
                            <tr>
                                <td>
                                        <tr>
                                            <td width='186' height='250' align='center' class='devicewidth'>
                                                <img src='http://platform.wotmed.com/photos/thumbs/$practitionerProfilePhoto' alt='Surgery Facilitator Profile Photo' border='0' style='display:block; border:none; outline:none; text-decoration:none;'>
                                            </td>
                                        </tr>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>
<!-- end of 3 columns -->

";



                if (!defined("PHP_EOL")) define("PHP_EOL", "\r\n");


            //    $address = "bookings@wotmed.com";

                $comments = "none";

                $address = $practitionerEmailAddress;


                $e_body = "$patientFirstName has booked an appointment with you via Wotmed!";
                $e_content = "\"$comments\"" . PHP_EOL . PHP_EOL;
                $e_reply = "You can contact $patientFirstName via email, $patientEmailAddress";

                $msg = wordwrap( $e_body . $e_content . $e_reply, 70 );

                $headers = 'From: bookings@wotmed.com' . "\r\n" .
                    'Reply-To: bookings@wotmed.com' . "\r\n" .
                    'X-Mailer: PHP/' . phpversion();
                $headers .= "MIME-Version: 1.0\r\n";
                $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

                $subject = 'A patient: ' . $patientFirstName . ' ' . $patientSurname . ' with email address: ' . $patientEmailAddress . ' has just booked an appointment with you via Wotmed!';

                $message = file_get_contents('../HTMLEmailTemplate/WotmedEmailTemplate/RequestforProposalHeader.html');
                $message .= file_get_contents('../HTMLEmailTemplate/WotmedEmailTemplate/PatientBookingBodyHeaderThankYou.html');


                $message .= 'Patient Name: ' . $patientFirstName . ' ' . $patientSurname . "<BR>" . 'Patient Email: ' . $patientEmailAddress . "<BR>" . 'Booking Date: ' . $appointmentDate . "<BR>" . 'Booking Time: ' . $appointmentTime  . "<BR>";

                $message .= file_get_contents('../HTMLEmailTemplate/WotmedEmailTemplate/PractitionerBookingBodyFooterClient.html');

                $message .= file_get_contents('../HTMLEmailTemplate/WotmedEmailTemplate/PractitionerBookingFooterClient.html');


                if(mail($address, $subject, $message, $headers))
                {

                //	include ("reg-participantStorySuccess.php");



                    echo '<script language="javascript">';
                    echo 'alert("Your booking request has been successfully made with this Wotmed Surgery Facilitator.  You should receive an email confirmation shortly.")';
                    echo '</script>';

                }

                $address2 = $patientEmailAddress;	// set the email address to the patients email now

                $headers2 = 'From: bookings@wotmed.com' . "\r\n" .
                    'Reply-To: bookings@wotmed.com' . "\r\n" .
                    'X-Mailer: PHP/' . phpversion();
                $headers2 .= "MIME-Version: 1.0\r\n";
                $headers2 .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

                $subject2 = 'Your Wotmed Surgery Facilitator Booking ' . $patientFirstName . ' ' . $patientSurname;

                $message2 = file_get_contents('../HTMLEmailTemplate/WotmedEmailTemplate/RequestforProposalHeader.html');
                $message2 .= file_get_contents('../HTMLEmailTemplate/WotmedEmailTemplate/PractitionerBookingBodyHeaderThankYou.html');

                $message2 .= "<BR>" . 'Dear ' . $patientFirstName . ',' . "<BR><BR>" . 'Thank you so much for booking an appointment with a Wotmed Surgery Facilitator' . "<BR><BR>";
                $message2 .= 'Your chosen Surgery Facilitator has been emailed and advised of your booking.';
                $message2 .= "<BR><BR>" . 'Kind Regards,' . "<BR><BR>" . 'The Wotmed Team' . "<BR><BR>";

                $message2 .= '<b>Your booking details:</b>' . "<BR><BR>";

                $message2 .= 'Booking Date: ' . $appointmentDate . "<BR>";
                $message2 .= 'Booking Time: ' . $appointmentTime . "<BR>";
                $message2 .= 'Surgery Facilitator Name: ' . $practitionerBusinessName . "<BR>";
                $message2 .= 'Business Phone Number: ' .  $practitionerBusinessNumber . "<BR>";
                $message2 .= 'Business Address: ' .  $practitionerBusinessAddress . ' ' . $practitionerBusinessSuburb . ' ' . $practitionerBusinessCity . ' ' . $practitionerBusinessState . "<BR>";

                $message2 .= $bannerCode . "<BR>";
                $message2 .= $bannerCode2 . "<BR>";

                $message2 .= file_get_contents('../HTMLEmailTemplate/WotmedEmailTemplate/PractitionerBookingBodyFooterClient.html');

                $message2 .= file_get_contents('../HTMLEmailTemplate/WotmedEmailTemplate/PractitionerBookingFooterClient.html');



                if(mail($address2, $subject2, $message2, $headers2))
                {

                }

                else

                {
                    echo 'ERROR!';
                }


				echo "<script language='javascript'>";
					echo "window.location = '/facilitator/index.php?id=$facilitatorNumber';";
				echo "</script>";
			}
			else
			{
				echo "<input type='hidden' name='pracId' value='" . $_GET['practitionerId'] . "'>";
				echo "<input type='hidden' name='date' value='" . $_GET['date'] . "'>";
				echo "<input type='hidden' name='time' value='" . $_GET['time'] . "'>";
			}
		?>
            Requested Appointment Date:
                <input type="text" id="datepicker" name="datepicker" value="Select desired date">

            <BR><BR>

            Requested Appointment Time:
            <select name='AppointmentTime' id='AppointmentTime'>
                <option value='Please select your desired appointment time'>Please select your desired appointment time</option>
                <option value='8:30 am'>8:30 am</option>
                <option value='9:00 am'>9:00 am</option>
                <option value='9:30 am'>9:30 am</option>
                <option value='10:00 am'>10:00 am</option>
                <option value='10:30 am'>10:30 am</option>
                <option value='11:00 am'>11:00 am</option>
                <option value='11:30 am'>11:30 am</option>
                <option value='12:00 noon'>12:00 noon</option>
                <option value='12:30 pm'>12:30 pm</option>
                <option value='1:00 pm'>1:00 pm</option>
                <option value='1:30 pm'>1:30 pm</option>
                <option value='2:00 pm'>2:00 pm</option>
                <option value='2:30 pm'>2:30 pm</option>
                <option value='3:00 pm'>3:00 pm</option>
                <option value='3:30 pm'>3:30 pm</option>
                <option value='4:00 pm'>4:00 pm</option>
                <option value='4:30 pm'>4:30 pm</option>
                <option value='5:00 pm'>5:00 pm</option>
                <option value='5:30 pm'>5:30 pm</option>

            </select>

            <BR><BR>

		Reason :
			<select name='reason' id='reason'>
				<option value='Surgery Facilitation Consultation'>Surgery Facilitation Consultation</option>
			</select>
			<br><br>
			<input type="submit" id='signupsubmit' value="Book Now">
		</form>
	</div>

    <script src="/proposals/date/pikaday.js"></script>
    <script>

        var picker = new Pikaday(
            {
                field: document.getElementById('datepicker'),
                firstDay: 1,
                minDate: new Date('2000-01-01'),
                maxDate: new Date('2020-12-31'),
                yearRange: [2000,2020]
            });

    </script>

	<?php

    echo "<BR><BR>";

    include "../includes/p_footer.php"; ?>
</body>
</html>
