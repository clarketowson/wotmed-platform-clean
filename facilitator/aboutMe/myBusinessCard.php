<?php
	session_start();
	$path="../../";
	include $path."includes/connect.php";
	include $path."includes/functions.php";
	$path="../../";
	include $path."classes/SimpleImage.php";
	$header= Array( 'BUSINESSCARD_NUMBER','PRACTITIONER_NUMBER','BUSINESSNAME','BUSINESSNUMBER','BUSINESSEMAIL','BUSINESSWEBSITEURL','BUSINESSADDRESS','BUSINESSSUBURB','BUSINESSPOSTCODE','BUSINESSCITY','COUNTRY_NUMBER','FRONT','BACK','STATUS');
	$rowHeader= Array( '','PRACTITIONER_NUMBER','PRACTITIONER_BUSINESSNAME','PRACTITIONER_BUSINESSNUMBER','EMAILADDRESS','WEBSITEURL','PRACTITIONER_BUSINESSADDRESS','PRACTITIONER_BUSINESSSUBURB','PRACTITIONER_BUSINESSPOSTCODE','','B_COUNTRY_NUMBER','','','');
	$langs = array("Afrikaans", "Albanian", "Arabic", "Azerbaijani", "Basque", "Bengali", "Belarusian", "Bulgarian", "Catalan", "Chinese Simplified", "Chinese Traditional", "Croatian", "Czech", "Danish", "Dutch", "English", "Esperanto", "Estonian", "Filipino", "Finnish", "French", "Galician", "Georgian", "German", "Greek", "Gujarati", "Haitian Creole", "Hebrew", "Hindi", "Hungarian", "Icelandic", "Indonesian", "Irish", "Italian", "Japanese", "Kannada", "Korean", "Latin", "Latvian", "Lithuanian", "Macedonian", "Malay", "Maltese", "Norwegian", "Persian", "Polish", "Portuguese", "Romanian", "Russian", "Serbian", "Slovak", "Slovenian", "Spanish", "Swahili", "Swedish", "Tamil", "Telugu", "Thai", "Turkish", "Ukrainian", "Urdu", "Vietnamese", "Welsh", "Yiddish");

	//google language target
	$glangs = array("af", "sq", "ar", "az", "eu", "bn", "be", "bg", "ca", "zh-CN", "zh-TW", "hr", "cs", "da", "nl", "en", "eo", "et", "tl", "fi", "fr", "gl", "ka", "de", "el", "gu", "ht", "iw", "hi", "hu", "is", "id", "ga", "it", "ja", "kn", "ko", "la", "lv", "lt", "mk", "ms", "mt", "no", "fa", "pl", "pt", "ro", "ru", "sr", "sk", "sl", "es", "sw", "sv", "ta", "te", "th", "tr", "uk", "ur", "vi", "cy", "yi");
	if(isset($_SESSION['id'])){
			$rowSession=getParticipantDetail($conn,$_SESSION['id']);
	}
	
	if(isset($_SESSION['id'])){
			$rowSessionFacilitator=getPractitionerDetail($conn,$_SESSION['id']);
	}
	
	// REMOVED BUG BELOW WHICH DISPLAYED BLANK SILHOUETTE OF SURGERY FACILITATOR AS PROFILE PHOTO IN TOP RIGHT HAND SIDE OF SCREEN and in profile photo
	
//	if($rowSession['PROFILEPHOTO']==""){
//			$ppFileNameSession="blankSilhouetteMale.png";
//	}else{
//			$ppFileNameSession=$rowSession['PROFILEPHOTO'];
//	}

// display the surgery facilitators business logo as profile image and as top right hand side small photo

if($rowSessionFacilitator['PRACTITIONER_BUSINESSLOGO']=="")
	{
		$ppFileNameSession="blankSilhouetteMale.png";
	}
	else
	{
		$ppFileNameSession=$rowSessionFacilitator['PRACTITIONER_BUSINESSLOGO'];
	}
	
	
	
	if(isset($_SESSION['practitioner_id'])){
		$row=getPractitionerDetail($conn,$_SESSION['id']);
		$practitionerId=$row['PRACTITIONER_NUMBER'];
		
		$query="SELECT * FROM BUSINESSCARD WHERE PRACTITIONER_NUMBER='{$practitionerId}'";
		$result=mysqli_query($conn,$query);
	
		$query="SELECT COUNT(*) FROM PATIENTPRACTITIONERRELATIONSHIP WHERE PRACTITIONER_NUMBER = '" . $_SESSION['practitioner_id'] . "' AND RECOMMENDEDAS='1'";
		$tempOfRecommend=mysqli_query($conn,$query);
		if(mysqli_num_rows($tempOfRecommend)!=0)
			$numbOfRecommend=mysqli_fetch_array($tempOfRecommend);
		else
			$numbOfRecommend[]=0;
			
		if(mysqli_num_rows($result)==0){
			$temp=getPractitionerDetail($conn,$_SESSION['id']);
			for($i=0;$i<count($header);$i++){
				if($i>0 && $i<count($header)-3 && $i!=count($header)-5)
					$data[$header[$i]]=$temp[$rowHeader[$i]];
				else
					$data[$header[$i]]="";
			}
			$data['UNITNUMBER']="";
			$countryName=getCountryName($conn,$data['COUNTRY_NUMBER']);
			$address=$temp["PRACTITIONER_BUSINESSADDRESS"] . ", {$data['BUSINESSSUBURB']} {$temp['PRACTITIONER_BUSINESSPOSTCODE']}, {$data['BUSINESSCITY']}, {$countryName} ";
			//$address="99 Whiteman Street, SOUTHBANK VIC 3006";
			$longlat=explode(",",convertAddressToLngLat($address));
		}
		else{
			$c=0;
			while($details=mysqli_fetch_array($result)){
				for($i=0;$i<count($header);$i++){
					if($header[$i]=="BUSINESSADDRESS"){
						$temp=explode(";",$details[$i]);
						$data["UNITNUMBER"]=$temp[0];
						$data[$header[$i]]=$temp[1];
					}	
					else
						$data[$header[$i]]=$details[$i];
				}
			}
			$countryName=getCountryName($conn,$data['COUNTRY_NUMBER']);
			$address=implode(" ",$temp) . ", {$data['BUSINESSSUBURB']}, {$data['BUSINESSCITY']}, {$countryName} ";
			$longlat=explode(",",convertAddressToLngLat($address));
		}
	}
	 
	//Parser for Add Photo Form
	if (isset($_POST['BUSINESSNAME'])){
		$_POST['BUSINESSADDRESS']=$_POST['UNITNUMBER']. ";" . $_POST['BUSINESSADDRESS'];
		if($data['BUSINESSCARD_NUMBER']==""){
			$textValue=$_POST;
			$query="INSERT INTO BUSINESSCARD (PRACTITIONER_NUMBER";
			for($i=2;$i<count($header);$i++)
				$query.=", ".$header[$i];
			$query.=") VALUES ('" . $practitionerId . "'";
			for($i=2;$i<count($header);$i++){
				//file upload
				if(($i==11)||($i==12)){
					if(!isset($_FILES[$header[$i]]['name'])){
						$textValue[$header[$i]]="";
					}else{
						if (($_FILES[$header[$i]]["type"] == "image/jpeg") || ($_FILES[$header[$i]]["type"] == "image/png") || ($_FILES[$header[$i]]["type"] == "image/gif") || ($_FILES[$header[$i]]["type"] == "image/bmp"))
						{
							$type="";
							switch ($_FILES[$header[$i]]["type"])
							{
								case "image/jpeg":
									$type=".jpg";
									break;
								case "image/png":
									$type=".png";
									break;
								case "image/gif":
									$type=".gif";
									break;
								case "image/bmp":
									$type=".bmp";
									break;
							}
							$name=$header[$i]."_".$practitionerId;
							$image = new SimpleImage();
							$image->load($_FILES[$header[$i]]['tmp_name']);
							if($image->getWidth()>$image->getHeight()){
								if($image->getWidth()>1400)
									$image->resizeToWidth(1400);
							}else{
								if($image->getHeight()>1400)
									$image->resizeToHeight(1400);
							}
							$image->save($path.'BusinessCard/' . $name . $type);
							$textValue[$header[$i]]=$name . $type;
						}
						else{
							$textValue[$header[$i]]="";
						}
					}
				}
				if($i==10)
					$query.=", '". getCountryNumber($conn,$textValue[$header[$i]]) . "'";
				else
					$query.=", '". $textValue[$header[$i]] . "'";
			}
			$query.=")";
			//msgBox($query);
			mysqli_query($conn,$query);
			//header("location:index.php");
			//echo "<BR><BR><BR>" . $query;
		}
		else{
			$textValue=$_POST;
			$query="UPDATE BUSINESSCARD SET ";
			for($i=2;$i<count($header);$i++){
				//file upload
				if(($i==11)||($i==12)){
					if(!isset($_FILES[$header[$i]]['name'])){
						$textValue[$header[$i]]=$data[$header[$i]];
					}else{
						if (($_FILES[$header[$i]]["type"] == "image/jpeg") || ($_FILES[$header[$i]]["type"] == "image/png") || ($_FILES[$header[$i]]["type"] == "image/gif") || ($_FILES[$header[$i]]["type"] == "image/bmp"))
						{
							$type="";
							switch ($_FILES[$header[$i]]["type"])
							{
								case "image/jpeg":
									$type=".jpg";
									break;
								case "image/png":
									$type=".png";
									break;
								case "image/gif":
									$type=".gif";
									break;
								case "image/bmp":
									$type=".bmp";
									break;
							}
							$name=$header[$i]."_".$practitionerId;
							$image = new SimpleImage();
							$image->load($_FILES[$header[$i]]['tmp_name']);
							if($image->getWidth()>$image->getHeight()){
								if($image->getWidth()>1400)
									$image->resizeToWidth(1400);
							}else{
								if($image->getHeight()>1400)
									$image->resizeToHeight(1400);
							}
							$image->save($path.'BusinessCard/' . $name . $type);
							$textValue[$header[$i]]=$name . $type;
						}
						else{
							$textValue[$header[$i]]=$data[$header[$i]];
						}
					}
				}
				if($i<=12)
					if($i==10)
						$query.=$header[$i] . " = " . getCountryNumber($conn,$textValue[$header[$i]]) . ", " ;
					else
						$query.=$header[$i] . " = '{$textValue[$header[$i]]}', " ;
				else
					$query.=$header[$i] . " = '{$textValue[$header[$i]]}' " ;
			}
			$query.="WHERE PRACTITIONER_NUMBER = '". $textValue['PRACTITIONER_NUMBER'] . "'";
			//msgBox($query);
			mysqli_query($conn,$query);
			//header("location:index.php");
			//echo "<BR><BR><BR>" . $query;
		}
		header("location:myBusinessCard.php");
	}
	 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link href="<?php echo $path; ?>style/p_style.css" rel="stylesheet"></link>
<script type="text/javascript" src="<?php echo $path; ?>classes/jquery.js"></script>
<script type="text/javascript">
	function bcdownload(){
		//alert("BCDownload.php?target="+$('#langs').val());
		window.open("../BCDownload.php?target="+$('#langs').val());
	}
</script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Wotmed Surgery Facilitator Business Card</title>
<link href="<?php echo $path; ?>style/apple.css" rel="stylesheet" type="text/css" />
</head>

<body>
<?php include $path."includes/p_header.php"; ?>
<?php
	$backLink = "
		<a href='../'>Back to Your Surgery Facilitator Profile</a>
	";
	include "../cpanelHeader.php"; 
?>
<div class='lfloat' style='width:98%'>
	<div class="stdWrapper" onmouseover="this.style.background='white';"onmouseout="this.style.background='#F0F0F0';">
	<div id="google_translate_element" style='float:right'></div><BR>
	<script type="text/javascript">
		function googleTranslateElementInit() {
		  new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
		}
	</script>
	<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
	  <p><span class="PractitionerBody">
	  <?php
	  /*if($data['DETAIL_IMAGE']!="")
		echo "<img src='photos/thumbs/" . $data['DETAIL_IMAGE'] . "' alt='' width='72' height='72' />";
	  else*/
		echo "<img src='{$path}images/Actions-help-about-icon.png' alt='' width='72' height='72' />";
	  ?>
	  <h3 class="hyperlinks">Update Business Card</h3></span></p>
	  <p class="PractitionerMainText">Update details about your Business Card</p>
	
	  <form action="" method="post" enctype="multipart/form-data" name="UpdatePractitionerPromotion" id="UpdatePractitionerPromotion">
		<table width="789" border="0">
		  <tr>
			<td><span class="PractitionerMainText">Business Card (Front) </span></td><td>
			<?php
			if($data["FRONT"]!=""){
				//echo "<input type='hidden' name='FRONT' value='" . $data['FRONT'] . "'>";
				echo "<img src='{$path}BusinessCard/{$data['FRONT']}'><br>";
			}
			?>
			<input type="file" name="FRONT" id="FRONT" value="Browse Image" /></td>
		  </tr>
		  <tr>
			<td><span class="PractitionerMainText">Business Card (Back) </span></td><td>
			<?php
			if($data["BACK"]!=""){
				//echo "<input type='hidden' name='BACK' value='" . $data['BACK'] . "'>";
				echo "<img src='{$path}BusinessCard/{$data['BACK']}'><br>";
				}
		  ?>
			<input type="file" name="BACK" id="BACK" value="Browse Image" /></td>
		  </tr>
		  <tr>
			<td colspan="2">
				<span class="PractitionerMainText"><br>Please enter all the details of your medical surgery facilitation that you have written on your printed business card in the form below.<br></span>
			</td>
		  </tr>
		  <tr>
			<td><span class="PractitionerMainText">Your Surgery Facilitation Business Name</span></td>
			<td><span class="PractitionerMainText">
			  <input name="BUSINESSNAME" type="text" id="BUSINESSNAME" size="80" maxlength="80" value="<?php echo $data['BUSINESSNAME']; ?>" />
			  <input name="BUSINESSCARD_NUMBER" type="hidden" id="BUSINESSCARD_NUMBER" value="<?php echo $data['BUSINESSCARD_NUMBER']; ?>" />
			  <input name="PRACTITIONER_NUMBER" type="hidden" id="PRACTITIONER_NUMBER" value="<?php echo $data['PRACTITIONER_NUMBER']; ?>" />
			  <input name="STATUS" type="hidden" id="STATUS" value="0" />
			</span></td>
		  </tr>
		  <tr>
			<td><span class="PractitionerMainText">Your Surgery Facilitation Phone Number</span></td>
			<td><span class="PractitionerMainText">
			  <input name="BUSINESSNUMBER" type="text" id="BUSINESSNUMBER" size="80" maxlength="80" value="<?php echo $data['BUSINESSNUMBER']; ?>" />
			</span></td>
		  </tr>
		  <tr>
			<td><span class="PractitionerMainText">Your Surgery Facilitation Email Address</span></td>
			<td><span class="PractitionerMainText">
			  <input name="BUSINESSEMAIL" type="text" id="BUSINESSEMAIL" size="80" maxlength="80" value="<?php echo $data['BUSINESSEMAIL']; ?>" />
			</span></td>
		  </tr>
		  <tr>
			<td><span class="PractitionerMainText">Your Surgery Facilitation Independent Website Address</span></td>
			<td><span class="PractitionerMainText">
			  <input name="BUSINESSWEBSITEURL" type="text" id="BUSINESSWEBSITEURL" size="80" maxlength="80" value="<?php echo $data['BUSINESSWEBSITEURL']; ?>" />
			</span></td>
		  </tr>
		  <tr>
			<td><span class="PractitionerMainText">Unit or Street Number</span></td>
			<td><span class="PractitionerMainText">
			  <input name="UNITNUMBER" type="text" id="UNITNUMBER" size="80" maxlength="80" value="<?php echo $data['UNITNUMBER']; ?>" />
			</span></td>
		  </tr>
		  <tr>
			<td><span class="PractitionerMainText">Street Name</span></td>
			<td><span class="PractitionerMainText">
			  <input name="BUSINESSADDRESS" type="text" id="BUSINESSADDRESS" size="80" maxlength="80" value="<?php echo $data['BUSINESSADDRESS']; ?>" />
			</span></td>
		  </tr>
		  <tr>
			<td><span class="PractitionerMainText">Suburb</span></td>
			<td><span class="PractitionerMainText">
			  <input name="BUSINESSSUBURB" type="text" id="BUSINESSSUBURB" size="80" maxlength="80" value="<?php echo $data['BUSINESSSUBURB']; ?>" />
			</span></td>
		  </tr>
		  <tr>
			<td><span class="PractitionerMainText">Post Code</span></td>
			<td><span class="PractitionerMainText">
			  <input name="BUSINESSPOSTCODE" type="text" id="BUSINESSPOSTCODE" size="80" maxlength="80" value="<?php echo $data['BUSINESSPOSTCODE']; ?>" />
			</span></td>
		  </tr>
		  <tr>
			<td><span class="PractitionerMainText">City</span></td>
			<td><span class="PractitionerMainText">
			  <input name="BUSINESSCITY" type="text" id="BUSINESSCITY" size="80" maxlength="80" value="<?php echo $data['BUSINESSCITY']; ?>" />
			</span></td>
		  </tr>
		  <tr>
			<td><span class="PractitionerMainText">Country</span></td>
			<td><span class="PractitionerMainText">
			  <input name="COUNTRY_NUMBER" type="text" id="COUNTRY_NUMBER" size="80" maxlength="80" value="<?php echo getCountryName($conn,$data['COUNTRY_NUMBER']); ?>" />
			</span></td>
		  </tr>
		</table>
		<p class="PractitionerMainText">
		  <label>
			<!--<input type="button" name="PreviewAbout2" id="PreviewAbout2" value="Preview About" />-->
		  </label>
		  <input type="submit" name="UploadAbout2" id="UploadAbout2" value="Update Business Card" />
		</p>
	  </form>
	  
	  <div align="center">
		<table>
			<tr>
				<td><img src="<?php echo $path; ?>images/WotMedLogoMedium.jpg" height="40px"></td>
				<td align="center"><H2><?php echo $data['BUSINESSNAME']; ?></H2></td>
				<td align="right"><img src="<?php echo $path; ?>photos/thumbs/<?php echo $row['PRACTITIONER_BUSINESSLOGO']; ?>" height="50px"> 	</td>
			</tr>
			<tr>
				<td>Independent Website Address</td>
				<td colspan="2"><a href="//<?php echo $data['BUSINESSWEBSITEURL']; ?>"><?php echo $data['BUSINESSWEBSITEURL']; ?></a></td>
			</tr>
			<tr>
				<td>Wotmed Profile Address</td>
				<td colspan="2"><a href="practitioner_profile.php?id=<?php echo $row['PARTICIPANT_NUMBER']; ?>"><?php echo $data['BUSINESSNAME']; ?>
			</tr>
			<tr>
				<td>Email Address</td>
				<td colspan="2"><a href="mailto:<?php echo $data['BUSINESSEMAIL']; ?>"><?php echo $data['BUSINESSEMAIL']; ?></a></td>
			</tr>
			<tr>
				<td>Phone Number</td>
				<td colspan="2"><?php echo $data['BUSINESSNUMBER']; ?></td>
			</tr>
			<tr>
				<td>Address</td>
				<td colspan="2"><a href="http://maps.google.com/maps?q=<?php echo $longlat[0]; ?>,<?php echo $longlat[1]; ?>&z=17"><?php echo $address; ?></td>
			</tr>
		</table>
	  </div>
	 <div>
		<select id='langs'>
		<?php
			foreach($langs as $key => $val){
				if($val == "English")
					echo "<option value='{$glangs[$key]}' selected='selected'>$val</option>";
				else
					echo "<option value='{$glangs[$key]}'>$val</option>";
			}
		?>
		</select>
		<br>
		<a href="#" onclick="bcdownload()">Download PDF File Here</a>
	  </div>
	  <div>
		  <span><H3>
		Note: Your electronic business card will look similar to below except that it will contain your own surgery facilitator information. The reason that we ask you to input your surgery facilitator information separately on this form is so that you have 100% control over the information that you want presented to your clients as well as making it easy for Wotmed to use your information to create clickable hyperlinks and to translate your business card information into different languages. 
		  </H3><br></span>
		</div>
		<div align="center">
		  <img src="<?php echo $path; ?>images/ExampleOfBusinessCard1.jpg"><br><br>
		  <img src="<?php echo $path; ?>images/ExampleOfBusinessCard2.jpg"><br>
		</div>
	</div>
</div>
<p>&nbsp;</p>
<?php include $path."includes/p_footer.php"; ?>
</body>

</html>
