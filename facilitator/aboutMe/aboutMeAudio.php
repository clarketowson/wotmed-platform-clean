<?php
	session_start();
	$path="../../";
	include $path."includes/connect.php";
	include $path."includes/functions.php";
	$path="../../";
	require_once $path."classes/phpuploader/include_phpuploader.php";
	if(isset($_GET['id']))
		$row=getPractitionerDetail($conn,$_GET['id']);
	else
		$row=getPractitionerDetail($conn,$_SESSION['id']);
	$rowSession=getParticipantDetail($conn,$_SESSION['id']);
	$ppFileNameSession="blankSilhouetteMale.png";
	$ppFileName="blankSilhouetteMale.png";
	if($row['PRACTITIONER_BUSINESSLOGO']!=""){
		$ppFileNameSession=$row['PRACTITIONER_BUSINESSLOGO'];
		$ppFileName=$row['PRACTITIONER_BUSINESSLOGO'];
	}
	/*
	if(isset($_POST) && $_POST!=NULL)
	{		
		if(isset($_FILES) && $_FILES["flogo"]["name"]!=NULL)
		{
			$name=$_FILES["flogo"]["name"];
			$tmpname=$_FILES["flogo"]["tmp_name"];
			$size=$_FILES["flogo"]["size"];
			if (($_FILES["flogo"]["type"] == "audio/mp3") || ($_FILES["flogo"]["type"] == "audio/acc"))
			{
				$type="";
				switch ($_FILES["flogo"]["type"])
				{
					case "audio/mp3":
						$type=".mp3";
						break;
					case "audio/acc":
						$type=".acc";
						break;
				}
				$id=$row['PRACTITIONER_NUMBER'];
				$title="audio_".$id."_1".$type;
				$targetName="media/" . $title;
				$desc=$_POST['transcript'];
				if(move_uploaded_file($_FILES["flogo"]["tmp_name"],$targetName))
				{
					$query="UPDATE PRACTITIONER SET AUDIO='$title', AUDIOTRANSCRIPT='$desc' WHERE PRACTITIONER_NUMBER=$id";
					//echo $query."<BR>".$targetName."<BR>".$_FILES['flogo']['tmp_name']."<BR>".$_FILES['flogo']['error']."<BR>";
					@mysqli_query($conn,$query);
					echo "<script type='text/javascript'>
							window.location = 'aboutMeAudio.php'
					</script>";
				}
			}
			else
				echo "Error";
			
			echo "Success";
		}
	}*/
	$uploader=new PhpUploader();
	$uploader->MaxSizeKB=20000;
	$uploader->Name="myuploader";
	$uploader->InsertText="Select Audio (Max 20M)";
	$uploader->AllowedFileExtensions="*.mp3,*.acc";	
	$uploader->MultipleFilesUpload=false;
	$uploader->ManualStartUpload=true;
	$uploader->UploadUrl="uploadAudio.php";
?>
<html>
<head>
	<script src="<?php echo $path; ?>includes/flowplayer/flowplayer-3.2.11.min.js"></script>
	<script type="text/javascript" src="<?php echo $path; ?>classes/jquery.js"></script>
	<link href="<?php echo $path; ?>style/p_style.css" rel="stylesheet"></link>
	<script type="text/javascript">
		function doStartUpload(transcript)
		{
			//alert(transcript);
			var uploadobj = document.getElementById('myuploader');
			if (uploadobj.getqueuecount() > 0)
			{
				uploadobj.startupload();
				document.getElementById("buttonupload").disabled=true;
			}
			else
			{
				alert("Please browse files for upload");
			}
		}
		function CuteWebUI_AjaxUploader_OnStop()
		{
			document.getElementById("buttonupload").disabled=false;
		}
		function CuteWebUI_AjaxUploader_OnPostback()
		{
			$.ajax({
			  type: "POST",
			  url: "updateTranscript.php",
			  data: { type: "a", transcript: $('#transcript').val() }
			}).done(function( msg ) {
				location.reload();
			});
			//var hidden=this;
			//hidden.internalobject.insertBtn.style.display='';
			document.getElementById("buttonupload").disabled=false;
			//thie files are prepaired
			//return false to cancel it..
		}
		function del()
		{
			$.ajax({
			  type: "POST",
			  url: "deleteAboutMe.php",
			  data: { type: "audio"}
			}).done(function( msg ) {
				location.reload();
			});
		}
	</script>
</head>
<body>
<?php include $path."includes/p_header.php"; ?>
<h3>WotMed.com Surgery Facilitator About Me Audio</h3>
<?php 
	if($row['AUDIO']==null){
		if(!isset($_GET['id'])){ ?>
			<p>You currently do not have an About Audio on your Wotmed profile. You should seriously consider creating and uploading an About Audio to your Wotmed profile. <BR><BR>

			Your Surgery Facilitator About Audio is an important part of your personal brand and your image as a Surgery Facilitator on the Wotmed platform. A corporate audio message is a powerful way to promote yourself and your business and is a great way to engage with Patients on the Wotmed platform. You will find that adding an About Audio to your Wotmed profile will improve the trust that Patients have for you as a Surgery Facilitator and is a great way to market yourself and your business.  <BR><BR>

			This is your chance to showcase your talents to patients globally so upload an About Audio to your Wotmed profile today! You will find that it will dramatically improve the volume and quality of Patient enquiries.<BR><BR>

			Please also use the form below to create a transcript of your About Audio. This is useful for Patients who may have difficulty understanding your language. We have integrated the Google Translator into the Wotmed Platform so your patients can simply click the translate button, turn the sound down and yet still understand the message of your audio.<BR><BR>

			Your Surgery Facilitator About Audio is limited to 10 minutes in length.<BR><BR>

			</p><BR>
			<form method='post' action="" enctype="multipart/form-data">
			<table>
				<tr>
					<td>Surgery Facilitator Name :</td>
					<td><input type='text' value='<?php echo $row['PRACTITIONER_BUSINESSNAME']; ?>' id='name' value='name'></td>
				</tr>
				<tr>
					<td>File audio (MP3 or ACC) :</td>
					<td><?php $uploader->Render(); ?></td>
				</tr>
				<tr>
					<td>About Me Audio Transcript :</td>
					<td><textarea id='transcript' name='transcript'></textarea></td>
				</tr>
				<tr>
					<td colspan=2><input type='button' value='Upload Audio & Transcript' onclick="doStartUpload(this.transcript); return false;" id="buttonupload"></td>
				</tr>
			</table>
			</form>
	<?php } 
	}?>
	<div>
		<?php
		if($row['AUDIO']!=null){?>
		<p>Your Practitioner About Audio is below.  Please click the Play button to listen your audio. </p>
		<?php 
			$detail=explode(".",$row['AUDIO']);
			echo "<a id='mb' style='display:block;width:648px;height:30px;' href='media/".$row['AUDIO']."'></a>"; ?>
			<script language="JavaScript">
				$f("mb", "<?php echo $path; ?>includes/flowplayer/flowplayer.commercial-3.2.12.swf", {
 
					// fullscreen button not needed here
					plugins: {
						controls: {
							fullscreen: false,
							height: 30,
							autoHide: false
						},
						audio: {
							url: '<?php echo $path; ?>includes/flowplayer/flowplayer.audio-3.2.10.swf'
						}
					},
				 
					clip: {
						autoPlay: false,
						// optional: when playback starts close the first audio playback
						onBeforeBegin: function() {
							$f("player").close();
						}
				 
					}
				 
				});
		</script>
			<BR>
		<div id="google_translate_element"></div><BR>
		<script type="text/javascript">
			function googleTranslateElementInit() {
			  new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
			}
		</script>
		<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
			<?php
			echo "<p>".$row['AUDIOTRANSCRIPT']."</p>";
			if(!isset($_GET['id']))
				echo "<p>Please ensure that you are happy with your About Audio.  If you are not happy with your audio you can delete it via the link below.</p>
				<a href='#' onclick='del();return false;'><p>Click here to delete your Practitioner About Audio and Audio Transcript</p></a>";
			else	
				echo "<a href='media/{$row['AUDIO']}'>Download this audio</a>";
		}
		else{
			if(isset($_GET['id'])){
				echo "No audio uploaded for this practitioner";
			}
		}
		?>	
	</div>
<?php include $path."includes/p_footer.php"; ?>
</body>
</html>
