<div id="leftCol" class="lfloat">
	<div id="leftProfPic" class="gborder lfloat" style="width:100%; margin-bottom:10px;">
		<div class="profPicDiv">
			<?php
			$subject = (isset($_GET['id']))?"This Surgery Facilitator":"Your";
			$subject2 = (isset($_GET['id']))?"This Surgery Facilitator":"You have";
			$subject3 = (isset($_GET['id']))?"This Surgery Facilitator is":"You are";
			if(isset($_GET['id'])){ ?>
				<div class="profPicDivClass"><img src="<?php echo $path."photos/originals/" . $ppFileName; ?>"/>
				<div class="profPicDivClassPopUp Indices" style="text-align:left;">

                    <?php echo $subject3; ?> located in

                    <?php

                  //  echo $row['PRACTITIONER_BUSINESSCITY'] . " " . $row['COUNTRYNAME'];

                    //  echo $row['PRACTITIONER_BUSINESSCITY'] . " " . $row['COUNTRYNAME'];

                    // advise the facilitator to set country of origin

                    $countryName = $row['COUNTRYNAME'];

                    if(empty($countryName))
                    {
                        if(!isset($_GET['id']))
                        {

                            echo "(Please set your country name by clicking the Update your Business Information at the right hand side of your profile)";
                        }
                        else
                        {
                            echo "(This Surgery Facilitator has not yet set their country of origin)";
                        }
                    }

                    else
                    {
                        echo $row['PRACTITIONER_BUSINESSCITY'] . " " . $row['COUNTRYNAME'];
                    }


                    ?>




                    <?php

                    if(empty($countryName))
                    {

                    }
                    else
                    {
                        if (isset($_GET['id']))
                            if ($row['B_COUNTRY_NUMBER'] != $rowSession['COUNTRY_NUMBER'])
                                echo " You will need to travel overseas to visit this Surgery Facilitator.  You will soon be able to book your flights, accommodation and car hire all via Wotmed";
                            else if ($row['PRACTITIONER_BUSINESSCITY'] != $rowSession['CITY'])
                                echo " You may need to travel interstate to visit this Surgery Facilitator.  You will soon be able to book your flights, accommodation and car hire all via Wotmed";
                    }


                    ?>.
				<?php/*
					if(isset($_GET['id']))
						if($row['B_COUNTRY_NUMBER']!=$rowSession['COUNTRY_NUMBER'])
							echo " You will need to travel overseas to visit this Practitioner.  You can book your flights, accommodation and car hire all via Wotmed";
						else if($row['PRACTITIONER_BUSINESSCITY']!=$rowSession['CITY'])
							echo " You will need to travel interstate to visit this practitioner.  You can book your flights, accommodation and car hire all via Wotmed";*/
				?>
				</div>
				</div>
			<?php }else{ ?>
				<img src="<?php echo $path."photos/originals/" . $ppFileName; ?>"/>
			<?php } ?>
		</div>
		<?php
			if(isset($_SESSION['id']) && !isset($_GET['id'])){
		?>
		<!--<a href="#" onClick="toggle('noticeDiv')">Upload Photo</a>-->
		<?php
			}
		?>
	</div>
	<div class="lfloat" style="width:100%; margin-top:0px;">
    
    
  
    
    
    
	<?php
		$passport="";
		$trusted="";
		$numTrust=0;
		$numRecom=0;
		$numThank=0;
		if(isset($_GET['id'])){
			$temp=explode("'",$_GET['id']);
			$id=$temp[0];

			$numTrust=pracGetTrust($conn,getPractitionerId($conn,$id));
			$numRecom=$numbOfRecommend[0];
			$numThank=getNumberOfThanks($conn,$id);
		}
		else{
			$numTrust=pracGetTrust($conn,getPractitionerId($conn,$_SESSION['id']));
			$numRecom=$numbOfRecommend[0];
			$numThank=getNumberOfThanks($conn,$_SESSION['id']);

		}
		$trustIndex="<div id='trustIndex' class='lfloat' style='display:inline-block;padding-top:3px;'><img src='{$path}images/t_red.png' height='20px' align='left'/>
			<div id='trustIndexPopUp' class='Indices'>RED</div>
			</div>";
		if($row['B_COUNTRY_NUMBER']!=$rowSession['COUNTRY_NUMBER'])
			$passport="<img src='{$path}images/passport_icon.png' height='20px' align='left'/>";
		if($row['TRUSTED']==1)
			$trusted="<div id='trust' class='lfloat' style='display:inline-block;padding-top:3px;'><img src='{$path}images/TrustedPractitionerIcon.png' height='20px' align='left'/>
			<div id='trustPopUp' class='Indices'>$subject credentials have been verified by Wotmed<br/>$subject3 a Wotmed Trusted Surgery Facilitators</div>
			</div>";

		/* -----------------------TRUST CALCULATION------------------------- */
		$sumIndex=0;
		$sumIndex+=$numTrust;
		$sumIndex+=$numRecom;
		$sumIndex+=$numThank;
		if($sumIndex==0){
			$trustIndex="<div id='trustIndex' class='rfloat' style='display:inline-block;padding-top:3px;'>
			<img src='{$path}images/t_red.png' height='20px' align='left'/>
			<img src='{$path}images/t_black.png' height='20px' align='left'/>
			<img src='{$path}images/t_black.png' height='20px' align='left'/>
			<div id='trustIndexPopUp' class='Indices' style='font-weight:normal;'>{$row['PRACTITIONER_BUSINESSNAME']} has achieved an overall Wotmed Trust Index rating of <span style='color:red;font-weight:bold;'>RED</span><br/>
This means that {$row['PRACTITIONER_BUSINESSNAME']} has not yet facilitated surgery for a number of Wotmed patients and has not yet earned enough of your fellow Wotmed Patients Trust, Recommendations and Thanks to earn a Green light</div>
			</div>";
		}else if(($sumIndex>0) && ($sumIndex<=49)){
			$trustIndex="<div id='trustIndex' class='rfloat' style='display:inline-block;padding-top:3px;'>
			<img src='{$path}images/t_black.png' height='20px' align='left'/>
			<img src='{$path}images/t_amber.png' height='20px' align='left'/>
			<img src='{$path}images/t_black.png' height='20px' align='left'/>
			<div id='trustIndexPopUp' class='Indices' style='font-weight:normal;'>{$row['PRACTITIONER_BUSINESSNAME']} has achieved an overall Wotmed Trust Index rating of <span style='color:rgb(255, 102, 0);font-weight:bold;'>AMBER</span><br/>
This means that {$row['PRACTITIONER_BUSINESSNAME']} has not yet facilitated enough surgery for Wotmed patients to earn enough of your fellow Wotmed Patients Trust, Recommendations and Thanks to earn a Green light</div>
			</div>";
		}else if($sumIndex>=50){
			$trustIndex="<div id='trustIndex' class='rfloat' style='display:inline-block;padding-top:3px;'>
			<img src='{$path}images/t_black.png' height='20px' align='left'/>
			<img src='{$path}images/t_black.png' height='20px' align='left'/>
			<img src='{$path}images/t_green.png' height='20px' align='left'/>
			<div id='trustIndexPopUp' class='Indices' style='font-weight:normal;'>{$row['PRACTITIONER_BUSINESSNAME']} has achieved an overall Wotmed Trust Index rating of <span style='color:rgb(0, 127, 0);font-weight:bold;'>GREEN</span><br/>
This means that {$row['PRACTITIONER_BUSINESSNAME']} has facilitated surgery for a number of Wotmed patients and has earned enough of your fellow Wotmed Patients Trust, Recommendations and Thanks to earn a Green light</div>
			</div>";
		}
		/* ---------------------TRUST CALCULATION END------------------------- */
			?>
            
            
           
        
             
             
		<div class="gborder lfloat" style="width:100%;">
        
        
        
       
        
        
			<div id="flagDiv"><img id='flag' src='<?php echo $path; ?>images/flag

			<?php


                // test to see if the facilitators country flag has been set - if it hasn't we are probably logged in for the first time
                // display the oneworld flag until the facilitator has set their flag

                $flag = $row['COUNTRYFLAG'];

                if(empty($flag))
                {
                    // display the divercity flag
                    $flag = '/OneWorldFlag.gif';
                   echo $flag;

                }

                else
                {
                    echo $row['COUNTRYFLAG'];
                }



                ?>' height='20px' align='left'/>

            
			<div id="flagDivPopUp" class="Indices"><?php echo $subject3; ?> located in

                <?php



                // advise the surgery facilitator to set country of origin

                $countryName = $row['COUNTRYNAME'];

                if(empty($countryName))
                {
                    if(!isset($_GET['id']))
                    {

                        echo "(Please set your country name by clicking the Update your Business Information at the right hand side of your profile)";
                    }
                    else
                    {
                        echo "(This Surgery facilitator has not yet set their country of origin)";
                    }
                }

                else
                {
                    echo $row['PRACTITIONER_BUSINESSCITY'] . " " . $row['COUNTRYNAME'];
                }


                ?>.

                <?php
                if(empty($countryName))
                {

                }
                else
                {
                    if (isset($_GET['id']))
                        if ($row['B_COUNTRY_NUMBER'] != $rowSession['COUNTRY_NUMBER'])
                            echo " You will need to travel overseas to visit this Surgery Facilitator.  You will soon be able to book your flights, accommodation and car hire all via Wotmed";
                        else if ($row['PRACTITIONER_BUSINESSCITY'] != $rowSession['CITY'])
                            echo " You may need to travel interstate to visit this Surgery Facilitator.  You will soon be able to book your flights, accommodation and car hire all via Wotmed";
                }
                ?>

			</div>
			</div>
			<img src='<?php echo $path; ?>images/coatofarms

			<?php

        //    echo $row['COUNTRYCOATOFARMS'];

            // test to see if the facilitators country coat of arms has been set - if it hasn't we are probably logged in for the first time
            // display no coat of arms until the facilitator has set their country

           $coatOfArms = $row['COUNTRYCOATOFARMS'];

            if(empty($coatOfArms))
            {
                // blank the coat of arms
                $coatOfArms = '/blankcoatofarms.gif';
                echo $coatOfArms;


            }
            else
            {
                echo $row['COUNTRYCOATOFARMS'];


            }

            ?>' height='20px' align='left'/><a href="#" onclick="showVisaReq('http://en.wikipedia.org/wiki/Visa_requirements_for_<?php echo $row['WIKIVR']; ?>','http://en.wikipedia.org/wiki/Visa_requirements_for_<?php echo $rowSession['WIKIVR']; ?>')"><?php echo $passport ?></a>
			<div class="rfloat" id="dipMissionIcon">
			<?php echo "<a href='http://en.wikipedia.org/wiki/List_of_diplomatic_missions_of_" . $row['DIPLOMATIC'] . "' target=_blank><img src='{$path}images/Embassy.jpg' height='20px' align='left'></a>"; ?>

                <div id="dipMissionIconPopUp" class="Indices">
                    <?php

                   // echo "<a href='http://en.wikipedia.org/wiki/List_of_diplomatic_missions_of_" . $row['DIPLOMATIC'] . "' target=_blank>List the Diplomatic Missions in the country of {$row['COUNTRYNAME']}</a>";

                    // if the patient is logged in and viewing the practitioners profile change the wording to in this Practitioners country of
                    // otherwise use the wording in your country of

                    if(isset($_GET['id']))
                    {
                        if(empty($countryName))
                        {
                            echo "Cannot display the list of diplomatic missions because this Surgery Facilitator has not yet set their country of origin";
                        }
                        else
                        {
                            echo "<a href='http://en.wikipedia.org/wiki/List_of_diplomatic_missions_of_" . $row['DIPLOMATIC'] . "' target=_blank>List the Diplomatic Missions in this Surgery Facilitators country of {$row['COUNTRYNAME']}</a>";
                        }
                    }
                    else
                    {

                        if(empty($countryName))
                        {
                            echo "Please set your country name by clicking the Update your Business Information at the right hand side of your profile in order to see the list of Diplomatic Missions";
                        }
                        else
                        {
                            echo "<a href='http://en.wikipedia.org/wiki/List_of_diplomatic_missions_of_" . $row['DIPLOMATIC'] . "' target=_blank>List the Diplomatic Missions in your country of {$row['COUNTRYNAME']}</a>";
                        }
                    }


                    ?></div>
			</div>
		</div>
			<br>
			<br><?php
		if(isset($_GET['id'])){
		?>
		<div class="gborder lfloat" style="width:100%; margin-top:10px;">
			<div class="lfloat" id="aboutMePractitionerVideo"><a href="aboutMe/aboutMePractitionerVideo.php?id=<?php echo $_GET['id']; ?>"><img src="<?php echo $path; ?>images/video.jpg" height='28' alt="Surgery Facilitator About Me Video"></a><div id="aboutMePractitionerVideoPopUp" class="Indices"><a href="aboutMe/aboutMePractitionerVideo.php?id=<?php echo $_GET['id']; ?>">Watch Surgery Facilitators About Video</a></div></div>
			<div class="lfloat" id="aboutMeAudio"><a href="aboutMe/aboutMeAudio.php?id=<?php echo $_GET['id']; ?>"><img src="<?php echo $path; ?>images/audio.png" height='28' alt="Surgery Facilitator About Me Audio"></a><div id="aboutMeAudioPopUp" class="Indices">
				<a href="aboutMe/aboutMeAudio.php?id=<?php echo $_GET['id']; ?>">Listen to Surgery Facilitators About Audio</a>
			</div></div>
			<?php
					$query="SELECT COUNT(*) FROM BUSINESSCARD WHERE PRACTITIONER_NUMBER=".$row["PRACTITIONER_NUMBER"];
					$result=mysqli_query($conn,$query);
					$res=mysqli_fetch_array($result);
					if($res[0]!=0){
                        echo "<div class='lfloat' style='padding:3px;' id='cardIcon'><a href='BCDownload.php?id={$_GET['id']}''><img src='../images/BusinessCardIcon.jpg' height='23' alt='Surgery Facilitator Business Card'></a><div id='cardIconPopUp' class='Indices'><a href='myBusinessCard.php'>Download Surgery Facilitators Business Card</a></div></div>";

                    }
			?>
		</div>
		<?php
		}else{
		?>
		<div class="gborder lfloat" style="width:100%; margin-top:10px;">
			<div class="lfloat" id="aboutMePractitionerVideo"><a href="aboutMe/aboutMePractitionerVideo.php"><img src="<?php echo $path; ?>images/video.jpg" height='28' alt="Practitioner About Me Video"></a><div id="aboutMePractitionerVideoPopUp" class="Indices">
			<?php if($row['VIDEO']!=null){?>
				<a href="aboutMe/aboutMePractitionerVideo.php">Please click here to play or update your Surgery Facilitators About Video</a>
			<?php }else{ ?>
				You have not yet uploaded your surgery facilitation business About Video - <a href="aboutMe/aboutMePractitionerVideo.php">please click here to upload it</a>
			<?php } ?>
			</div></div>
			<div class="lfloat" id="aboutMeAudio"><a href="aboutMe/aboutMeAudio.php"><img src="<?php echo $path; ?>images/audio.png" height='28' alt="Practitioner About Me Audio"></a><div id="aboutMeAudioPopUp" class="Indices">
			<?php if($row['AUDIO']!=null){?>
				<a href="aboutMe/aboutMeAudio.php">Please click here to play or update your Surgery Facilitators About Audio</a>
			<?php }else{ ?>
				You have not yet uploaded your surgery facilitation business  About Audio - <a href="aboutMe/aboutMeAudio.php">Please click here to upload it</a>
			<?php } ?>
			</div></div><!--
			<div class="lfloat" style="padding:3px;"><a href="aboutMe/resume.php"><img src="images/resume.png" height='20' alt="Practitioner Resume"></a></div>-->
            
            <?php 
			
			if($row['AUDIO']=null)
			{
				echo "You have not yet set country of origin.  To do this please click the Update your Business Information link on the right hand side of your Wotmed Profile under the Business Information heading.  Once you have set your country of origin your country flag will appear here";
			}
?>
           
			
            
            
			<?php
				$query="SELECT COUNT(*) FROM BUSINESSCARD WHERE PRACTITIONER_NUMBER=".$row["PRACTITIONER_NUMBER"];
				$result=mysqli_query($conn,$query);
				$res=mysqli_fetch_array($result);
				if($res[0]==0)
					$bcText="Please click here to create your Wotmed surgery facilitation business card";
				else
					$bcText="Please click here to update your Wotmed surgery facilitation business card";
			?>
			<div class="lfloat" style="padding:3px;" id="cardIcon"><a href="aboutMe/myBusinessCard.php"><img src="<?php echo $path; ?>images/BusinessCardIcon.jpg" height='20' alt="Surgery Facilitators Business Card"></a><div id="cardIconPopUp" class="Indices"><a href="aboutMe/myBusinessCard.php"><?php echo $bcText; ?></a></div></div>
			<?php
				//echo "<a href='myBusinessCard.php'><img src='images/BusinessCardIcon.jpg' height='25px'></a>";
				/*
				echo "
					<div id='businessCard' class='lfloat'>
								<a href='myBusinessCard.php'>
									<img src='images/BusinessCardIcon.jpg' height='25px'>
									<div id='galleryPopUp' class='Indices'>
										Please click here to create or update your Wotmed Business Card
									</div>
								</a>
							</div>					
				";*/
			if($row['TRUSTED']==0){
			?>
			<!-- <div class="lfloat" style="padding:3px;" id="trustIcon"><a href="<?php echo $path; ?>trustapplication"><img src="<?php echo $path; ?>images/Ok-icon.png" height='20' alt="Trust Application"></a><div id="trustIconPopUp" class="Indices"><a href="<?php echo $path; ?>trustapplication">Trust Application</a></div></div> -->
			<?php } ?>
		</div>
		<?php
		}
			?>
		</div><?php
		if(isset($_GET['id']))
		{ 
			if($skypeName[0]!=NULL)
			{
			?>
	<div class="lfloat gborder" style="width:100%; margin-top:10px;">
		 <!--
		Skype 'Add me to Skype' button
		http://www.skype.com/go/skypebuttons
		-->
		<script type="text/javascript" src="http://download.skype.com/share/skypebuttons/js/skypeCheck.js"></script>
		<a href="skype:<?php echo $skypeName[0]; ?>?add"><img src="<?php echo $path; ?>images/add_green_white_194x52.png" style="border: none;" width="100" height="30" alt="Add me to Skype" /></a>

		<!--
		Skype 'Chat with me' button
		http://www.skype.com/go/skypebuttons
		-->
		<script type="text/javascript" src="http://download.skype.com/share/skypebuttons/js/skypeCheck.js"></script>
		<a href="skype:<?php echo $skypeName[0]; ?>?chat"><img src="<?php echo $path; ?>images/chat_green_white_164x52.png" style="border: none;" width="100" height="30" alt="Chat with me" /></a>

		<!--
		Skype 'View my profile' button
		http://www.skype.com/go/skypebuttons
		-->
		<script type="text/javascript" src="http://download.skype.com/share/skypebuttons/js/skypeCheck.js"></script>
		<a href="skype:<?php echo $skypeName[0]; ?>?userinfo"><img src="<?php echo $path; ?>images/userinfo_green_white_180x52.png" style="border: none;" width="100" height="30" alt="View my profile" /></a>

		<!--
		Skype 'Leave me voicemail' button
		http://www.skype.com/go/skypebuttons
		-->
		<script type="text/javascript" src="http://download.skype.com/share/skypebuttons/js/skypeCheck.js"></script>
		<a href="skype:<?php echo $skypeName[0]; ?>?voicemail"><img src="<?php echo $path; ?>images/voicemail_green_white_213x52.png" style="border: none;" width="100" height="30" alt="Leave me voicemail" /></a>

		<!--
		Skype 'Send me a file' button
		http://www.skype.com/go/skypebuttons
		-->
		<script type="text/javascript" src="http://download.skype.com/share/skypebuttons/js/skypeCheck.js"></script>
		<a href="skype:<?php echo $skypeName[0]; ?>?sendfile"><img src="<?php echo $path; ?>images/sendfile_green_white_164x52.png" style="border: none;" width="100" height="30" alt="Send me a file" /></a>
	</div>
	<?php } ?>
	<br>
	<div class="lfloat gborder" style="width:100%; margin-top:10px;">
	<table>
		<tr>
			<!-- td style="width:50%;">
				<?php $text= "{$rowSession['FIRSTNAME']} {$rowSession['SURNAME']} from Wotmed.com has requested a diagnosis"; ?>
				<a href="mailto:<?php echo $email[0]; ?>?Subject=<?php echo $text ;?>"><img src="<?php echo $path; ?>images/Request.jpg" style="width:50px;" title="Click here to email <?php echo $row['PRACTITIONER_BUSINESSNAME']; ?> directly and request a diagnosis.<br>You may like to include photographs from your Wotmed profile."></a>
				<br>
				<a href="mailto:<?php echo $email[0]; ?>?Subject=<?php echo $text ;?>">Request Diagnosis</a>
			</td> -->
			<td>
			<?php $text = "{$rowSession['FIRSTNAME']} {$rowSession['SURNAME']} from Wotmed.com has sent you an attachment"; ?>
			<a href="mailto:<?php echo $email[0]; ?>?Subject=<?php echo $text ;?>"><img src="<?php echo $path; ?>images/EmailPractitioner.jpg" style="width:50px;" title="Click here to email <?php echo $row['PRACTITIONER_BUSINESSNAME']; ?>.  Include any file attachment you would like <?php echo $row['PRACTITIONER_BUSINESSNAME']; ?> to view."></a>
			<br>
			<a href="mailto:<?php echo $email[0]; ?>?Subject=<?php echo $text ;?>">Send files</a>
			</td>
		</tr>
	</table>
	</div>
	<?php 
	}
	if(!isset($_GET['id'])){ ?>
	<div class="lfloat">
		
        
        <div class="gborder lfloat" style="width:100%; margin-top:5px;">
        
		<div align="center" id="cpanelIcon"><a href='cpanel.php'><img src='<?php echo $path; ?>facilitator/images/SurgeryFacilitatorWebsite.jpg' width='80' height='80' alt='Update your Surgery Facilitator Website'><br>Update your Surgery Facilitator Website</a><div id="cpanelIconPopUp" class="Indices"><a href="cpanel.php">Please click here to update all aspects of your Wotmed Website</a></div></div>
        
</div>
        
        
		<!--<div align="center"><br><br><a href='<?php echo $path; ?>profile.php'>
		<!--<img src='images/profile_logo.png' width='94' height='94' alt='Health Profile'>-->
		<!--<br>Health Profile</a></div><br>-->
	</div>
	<?php } ?>


    <?php

    // check to see if this practitioner has subscribed to the Wotmed Paid Consultation System
    // if so - display the icon on their profile
    // otherwise - dont

    $conn2 = new mysqli('liberty.wotmed.com','wotmedco_user','e5~1]qu1|&8)6wJ', 'wotmedco_db');
    $conn3 = new mysqli('liberty.wotmed.com','wotmedco_user','e5~1]qu1|&8)6wJ', 'wotmedco_db');

    // get the practitioners details
    $practitionerNumber = $row["PRACTITIONER_NUMBER"];
    $query3="SELECT PARTICIPANT_NUMBER, PRACTITIONER_BUSINESSNAME, PRACTITIONER_BUSINESSNUMBER, PRACTITIONER_BUSINESSADDRESS, PRACTITIONER_BUSINESSSUBURB, PRACTITIONER_BUSINESSPOSTCODE, PRACTITIONER_BUSINESSCITY, PRACTITIONER_BUSINESSSTATE, PRACTITIONER_BUSINESSLOGO FROM PRACTITIONER WHERE PRACTITIONER_NUMBER = $practitionerNumber";
    $result3 = $conn3->query($query3);
    while($row3 = $result3->fetch_assoc())
    {
        $practitionerParticipantNumber = $row3["PARTICIPANT_NUMBER"];
    }
    $query2 ="SELECT PARTICIPANT_NUMBER FROM WOTMEDPAIDCONSULTING WHERE PARTICIPANT_NUMBER = $practitionerNumber";
    $result2 = $conn2->query($query2);
    while($row2 = $result2->fetch_assoc())
    {
        $IsEnrolled = $row2["PARTICIPANT_NUMBER"];
        //    echo $IsEnrolled;
    }



        if (!empty($IsEnrolled))
        {

            if (!isset($_GET['id']))
            {
                echo "   <div class='lfloat gborder' style='width:100%; margin-top:10px;'>
                <table>
                    <tr>
                        <td style='width:50%;'>

                            <a href='WotmedConsultations.php'><img src='../BuyWotmedConsultationNow.png' title='You offer Wotmed Paid Consultations'</a>
                            <br>
                            <a href='WotmedConsultations.php'>You offer Wotmed Paid Consultations</a>
                        </td>

                    </tr>
                </table>
            </div>";

            }

            else {
                echo "   <div class='lfloat gborder' style='width:100%; margin-top:10px;'>
                <table>
                    <tr>
                        <td style='width:50%;'>

                            <a href='BuyWotmedPaidConsultation.php'><img src='../BuyWotmedConsultationNow.png' title='Click here to request a paid consultation with this Practitioner'</a>
                            <br>
                            <a href='BuyWotmedPaidConsultation.php'>Wotmed Paid Consultation</a>
                        </td>

                    </tr>
                </table>
            </div>";
            }
        }




    ?>
    
    
    
</div>
