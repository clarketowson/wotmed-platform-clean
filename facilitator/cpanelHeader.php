<div id='headInfo' class='' style='margin:10px;margin-bottom:20px;width:97%'>
	<div class="PractitionerPhotoWrapper leftProfPic lfloat" style='height:200px;'>
		<div class="profPicDiv"><img src="<?php echo $path."photos/originals/" . $ppFileNameSession; ?>"/></div>
		<div style="margin-left:20px;">



            <div id="flagDiv"><img id='flag' src='../../images/flag<?php

                // set the practitioners coat of arms to the one divercity flag if they have not yet set their country of origin

                $flag = $row['COUNTRYFLAG'];

                if(empty($flag))
                {
                    // display the divercity flag
                    $flag = '/OneWorldFlag.gif';
                    echo $flag;

                }

                else
                {
                    echo $row['COUNTRYFLAG'];
                }


                ?>' height='20px' align='left'/>

                <div id="flagDivPopUp" class="Indices"><?php echo $subject3; ?> located in

                    <?php


                    // advise the practitioner to set country or origin

                    $countryName = $row['COUNTRYNAME'];

                    if(empty($countryName))
                    {
                        echo "(Please set your country name by clicking the Update your Business Information at the right hand side of your profile)";
                    }

                    else
                    {
                        echo $row['PRACTITIONER_BUSINESSCITY'] . " " . $row['COUNTRYNAME'];
                    }


                    ?></div>
            </div>
            <img src='../../images/coatofarms<?php

            // blank the coat of arms if the country has not been set

            $coatOfArms = $row['COUNTRYCOATOFARMS'];

            if(empty($coatOfArms))
            {
                // blank the coat of arms
                $coatOfArms = '/blankcoatofarms.gif';
                echo $coatOfArms;


            }
            else
            {
                echo $row['COUNTRYCOATOFARMS'];


            }


            ?>' height='20px' align='left'/>


            <!--<a href="#" onclick="showVisaReq('http://en.wikipedia.org/wiki/Visa_requirements_for_<?php echo $row['WIKIVR']; ?>','http://en.wikipedia.org/wiki/Visa_requirements_for_<?php echo $rowSession['WIKIVR']; ?>')"><?php echo $passport ?></a>-->
		</div>
	</div>
	<?php
	$trusted="";/*
	if($row['TRUSTED']==1){
		$trusted="<div id='trustCPanel' class='' style='display:inline-block;padding-top:3px;font-size:12px'><img src='{$path}images/TrustedPractitionerIcon.png' height='20px' align='left'/>
		<div id='trustCPanelPopUp' class='Indices'>Your credentials have been verified by Wotmed<br/>You are a Wotmed Trusted Practitioner</div>
		</div>";
	}else if($row['TRUSTED']==0){
		$trusted="<div class='' style='padding:3px;' id='trustIcon'><a href='{$path}trustapplication'>Apply for Wotmed Trusted Practitioner Status</a><div id='trustIconPopUpa' class='Indices'><a href='{$path}trustapplication'>Trust Application</a></div></div>";
	}*/
	?>
	<div class="lfloat">
	  <span class="PractitionerHeadings"><?php echo $row['PRACTITIONER_BUSINESSNAME']; ?>'s Website Update Panel </span> <?php echo $trusted; ?>
	</div>
	<div style='float:right;margin:10px'>
		<img src="<?php echo $path; ?>facilitator/images/SurgeryFacilitatorWebsite.jpg" width="80" height="80"/>
	</div>
	<div class="rfloat" style="padding-top:15px;color:rgb(170, 170, 170)">
		<span>Surgery Facilitator No: <?php echo $row['PRACTITIONER_NUMBER']; ?></span><br>
		<!--<span>Profile Views: 1</span>-->
	</div>
	<span class='lfloat' style='margin-top:-150px;margin-left:198px;width:65%;font-size:12px'>
	Welcome to your Wotmed Surgery Facilitator Control Panel.  You control most aspects of your Wotmed profile via this control panel.<br><br>
	For more information about how to use this control panel please visit the Wotmed FAQ page <a href='http://platform.wotmed.com/newFAQ.html' target='_blank'>here</a>
	</span>
	<span class='lfloat' style='margin-top:-80px;margin-left:198px;width:80%;font-weight:bold'>
	Your Patient Statistics:
	</span>
	<span class='lfloat' style='margin-top:-60px;margin-left:198px;width:80%'>
	<?php
		echo "You have been <a href='#' style='color:#325199;font-style:italic;'>Trusted</a> by: 0 Wotmed patient(s)<BR>";
		echo "You have been been <a href='#' style='color:#325199;font-style:italic;'>Recommended</a> by: " . $numbOfRecommend[0] . " Wotmed patient(s)<BR>";
		echo "You have been been <a href='#' style='color:#325199;font-style:italic;'>Thanked</a> by: ". getNumberOfThanks($conn,$_SESSION['id']) ." Wotmed patient(s)";
	?>
	</span>
	<span class='lfloat' style='margin-top:5px;margin-left:198px;width:80%'>
		You are <a href='#' style='color:#325199;font-style:italic;'>Connected</a> to: <?php echo getNumbOfParticipant($conn,$_SESSION['id']);?> Wotmed patient(s)<br>
		You are <a href='#' style='color:#325199;font-style:italic;'>Connected</a> to: 0 Wotmed practitioners(s)
		<br>
		<br>
		You have facilitated surgery for: 0 Wotmed patient(s)
	</span>
	<div class='rfloat' style='margin-bottom:10px'>
		<?php echo $backLink; ?>
	</div>
</div>
