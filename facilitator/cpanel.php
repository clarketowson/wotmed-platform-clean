<?php
	session_start();
	
	header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

	
	$path="../";
	include $path."includes/connect.php";
	include $path."includes/functions.php";
	$path="../";

	$days=array("Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday");
	if(isset($_GET['delete']))
	{	
		$serviceId=$_GET['delete'];
		$id=$rowSession['PARTICIPANT_NUMBER'];
		$query="DELETE FROM SERVICES WHERE PRACTITIONER_NUMBER='" . $id . "' AND MASTERSERVICE_NUMBER='" . $serviceId . "'";
		mysqli_query($conn,$query);
	}
	if(isset($_POST['serviceId']))
	{
		$serviceId=$_POST['serviceId'];
		$newPrice=$_POST['newPrice'];
		$id=$rowSession['PARTICIPANT_NUMBER'];
		$query="UPDATE SERVICES SET PRICE='" . $newPrice . "' WHERE PRACTITIONER_NUMBER='" . $id . "' AND MASTERSERVICE_NUMBER='" . $serviceId . "'";
		mysqli_query($conn,$query);
	}
	if(isset($_POST['price']))
	{
		$service=$_POST['service'];
		$price=$_POST['price'];
		$id=$rowSession['PARTICIPANT_NUMBER'];
		$query="INSERT INTO SERVICES (PRACTITIONER_NUMBER,MASTERSERVICE_NUMBER,PRICE) VALUES ('" . $id . "','" . $service . "','" . $price . "');";
		if($service!=-1)
			mysqli_query($conn,$query);
	}
	if(!isset($_SESSION['id'])){
		header("location:login.php");
	}
	if(isset($_SESSION['id'])){
		$rowSession=getParticipantDetail($conn,$_SESSION['id']);
		$row=getPractitionerDetail($conn,$_SESSION['id']);
		$servicesList=getServices($conn,$rowSession['PARTICIPANT_NUMBER']);
		$query="SELECT * FROM AVAILABILITY WHERE PRACTITIONER_NUMBER ='" . $rowSession['PARTICIPANT_NUMBER'] . "' ORDER BY TIMESTART";
		$schedules=mysqli_query($conn,$query);
		$query="SELECT COUNT(*) FROM PATIENTPRACTITIONERRELATIONSHIP WHERE PRACTITIONER_NUMBER = '" . $rowSession['PARTICIPANT_NUMBER'] . "'";
		$tempOfPatients=mysqli_query($conn,$query);
		if(mysqli_num_rows($tempOfPatients)!=0)
			$numbOfPatients=mysqli_fetch_array($tempOfPatients);
		else
			$numbOfPatients[]=0;
		//echo "<br><br><BR><BR><BR>".$query;
		$query="SELECT COUNT(*) FROM PATIENTPRACTITIONERRELATIONSHIP WHERE PRACTITIONER_NUMBER = '" . $_SESSION['practitioner_id'] . "' AND RECOMMENDEDAS='1'";
		$tempOfRecommend=mysqli_query($conn,$query);
		if(mysqli_num_rows($tempOfRecommend)!=0)
			$numbOfRecommend=mysqli_fetch_array($tempOfRecommend);
		else
			$numbOfRecommend[]=0;
		$passport="";
		if($row['B_COUNTRY_NUMBER']!=$rowSession['COUNTRY_NUMBER'])
			$passport="<img src='images/passport_icon.png' height='20px' align='left'/>";
		$subject3 = (isset($_GET['id']))?"This practitioner is":"You are";
	}
	
		
	$ppFileNameSession="blankSilhouetteMale.png";
	$ppFileName="blankSilhouetteMale.png";
	if($row['PRACTITIONER_BUSINESSLOGO']!=""){
		$ppFileNameSession=$row['PRACTITIONER_BUSINESSLOGO'];
		$ppFileName=$row['PRACTITIONER_BUSINESSLOGO'];
	}
	
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<link href="<?php echo $path; ?>style/p_style.css" rel="stylesheet"></link>
		<script type="text/javascript" src="<?php echo $path; ?>classes/jquery.js"></script>
		<script src="<?php echo $path; ?>classes/hover.js" type="text/javascript"></script>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title><?php echo $row['PRACTITIONER_BUSINESSNAME']; ?>'s Wotmed.com Profile</title>
		<link href="<?php echo $path; ?>style/apple.css" rel="stylesheet" type="text/css" />
		<script language="javascript"> 
		function updateServiceList(categoryNumber,subCategoryNumber)
		{
			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp=new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp.onreadystatechange=function()
			{
				if (xmlhttp.readyState==4 && xmlhttp.status==200)
				{
					document.getElementById("service").innerHTML=xmlhttp.responseText;
				}
			}
			xmlhttp.open("GET","<?php echo $path; ?>ajax/updateServiceList.php?catNumber="+categoryNumber+"&subCatNumber="+subCategoryNumber,true);
			xmlhttp.send();
		}
		function updateAvailableSchedule(id,order)
		{
			
			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp=new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp.onreadystatechange=function()
			{
				if (xmlhttp.readyState==4 && xmlhttp.status==200)
				{
					document.getElementById("scheduleTable").innerHTML=xmlhttp.responseText;
				}
			}
			xmlhttp.open("GET","<?php echo $path; ?>ajax/updateAvailableSchedule.php?id="+id+"&order="+order,true);
			xmlhttp.send();
		}
		function toggle(selected_div) {
			var ele = document.getElementById(selected_div);
			if(ele.style.display == "block") {
					ele.style.display = "none";
			}
			else {
				ele.style.display = "block";
			}
		}
		function back()
		{
			window.location = "practitioner_profile.php"
		}
		function showVisaReq(origURL,destURL){
			window.open(origURL,'_blank');
			window.open(destURL,'_blank');
		}
		</script>
	</head>

	<body>
		<?php include $path."includes/p_header.php"; ?>
		<?php
			$backLink = "
				
				<a href='http://wotmed.com/facilitator/index.php'>See your public facing Wotmed Surgery Facilitator Profile (exactly how your patients will see it)</a> <a href='http://wotmed.com/facilitator/index.php'><img src='images/BackToPublicProfile.png' title='Click here to view your Wotmed Surgery Facilitator Profile (exactly how your patients will see it)' /> </a>
				
				
				
				
			";
			include "cpanelHeader.php";
		?>
		<!--
		<div class='lfloat'>
			<table width="548" border="0">
			<!--
			  <tr>
				<td width="293" rowspan="10"><div class="PractitionerPhotoWrapper">
				  <p class="PractitionerHeadings"><img src="<?php echo "photos/originals/" . $ppFileNameSession; ?>" alt="alexPhoto" width="167" height="141" align="top" /></p>
				</div></td>
				<td height="5">&nbsp;</td>
				<td height="5" class="hyperlinks">&nbsp;</td>
			  </tr>
			  
			  <tr>
				<td height="5">&nbsp;</td>
				<td height="5" class="hyperlinks">&nbsp;</td>
			  </tr>
			  <tr>
				<td height="5">&nbsp;</td>
				<td height="5" class="hyperlinks">&nbsp;</td>
			  </tr>
			  <tr>
				<td height="5">&nbsp;</td>
				<td height="5" class="hyperlinks">&nbsp;</td>
			  </tr>
			  <tr>
				<td width="201" height="5" bgcolor="#FFFFFF"><span class="PractitionerMainText">Patient <em>Thanks</em></span></td>
				<td width="40" height="5" bgcolor="#FFFFFF" class="hyperlinks"><?php echo getNumberOfThanks($conn,$_SESSION['id']); ?></td>
			  </tr>
			  <tr>
				<td height="5" bgcolor="#FFFFFF" class="PractitionerMainText">Patient Recommendations</td>
				<td height="5" bgcolor="#FFFFFF" class="hyperlinks"><?php echo $numbOfRecommend[0]; ?></td>
			  </tr>
			  <tr>
				<td height="5" bgcolor="#FFFFFF"><span class="PractitionerMainText">Patient Connections</span></td>
				<td height="5" bgcolor="#FFFFFF"><span class="hyperlinks"><?php echo $numbOfPatients[0]; ?></span></td>
			  </tr>
			  <tr>
				<td height="5">&nbsp;</td>
				<td height="5">&nbsp;</td>
			  </tr>
			  <tr>
				<td height="5" bgcolor="#FFFFFF"><span class="PractitionerMainText">Practitioner Connections</span></td>
				<td height="5" bgcolor="#FFFFFF"><span class="hyperlinks">-</span></td>
			  </tr>
			  <tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			</table>
			<table width="536" border="0">
			  <tr>
				<td width="203" valign="top"><div class="PractitionerNameWrapper">
				  <p class="PractitionerName"><?php //echo $row['PRACTITIONER_BUSINESSNAME']; ?>&nbsp; <img src="images/flag<?php echo $rowSession['COUNTRYFLAG']; ?>" width="31" height="15" alt="australiaFlag" /></p>
				</div></td>
				<td width="323" valign="bottom" class="PractitionerMainText"><?php //echo $row['PRACTITIONER_BUSINESSADDRESS'] . ", " . $row['PRACTITIONER_BUSINESSSUBURB'] . ", " . $row['PRACTITIONER_BUSINESSSUBURB']; ?></a>
			  </tr>
			  <tr>
				<td valign="top"><div class="PractitionerHyperlinkWrapper">
				  <p class="hyperlinks"><?php echo $row['WEBSITEURL']; ?></p>
				</div></td>
				<td class="hyperlinks"><span class="PractitionerMainText"><?php //echo $row['PRACTITIONER_BUSINESSNUMBER']; ?></span></td>
			  </tr>
			  <tr>
				<td colspan="2"><span class="PractitionerMainText"><?php //echo $row['EDUCATION']; ?></td>
			  </tr>
			  <tr>
				<td colspan="2"><a href="qualifications.php" class="PractitionerMainText"><?php echo $row['QUALIFICATIONS']; ?></a></td>
			  </tr>
			</table>
			<table width="100%" border="0">
			  <tr>
				<td width="273" valign="top"><div class="PractitionerPayPalWrapper">
				  <p><?php //include "paypalSubscribeFee.php";?></p>
				</div></td>
				<td width="259"></td>
			  </tr>
			</table>
		</div>
		-->
		<!--
		<div class="PractitionerSubHeadingWrapper">
		  <p class="PractitionerSubHeadings">Serving <?php echo $row['PRACTITIONER_BUSINESSSUBURB']; ?>, VIC (Victoria)</p>
		</div>
		<div class="PractitionerBodyWrapper">
		  <p class="PractitionerBody"><span class="PractitionerMainText"><span class="PractitionerMainText">Dr. Alex Yusupov is your orthodontic specialist offering Herbst appliances, Incognito, Invisalign, and braces for children, teens, and adults.<br />
			<br />
			Since 1993 we have been your greater Melbourne, VIC orthodontist specialist of choice. We help you achieve beautiful smiles and prepare you to keep them for a lifetime. Being a specialist clinic means that our orthodontist, Dr. Alex Yusupov, is often called on to do orthodontic work others cannot provide. He fills this role by using the best available oral health technology and skills available. </span></span></p>
		  <p class="PractitionerBody"><span class="PractitionerMainText"><span class="PractitionerMainText">Our qualified staff’s support allows Dr. Yusupov to provide the best quality orthodontic treatments. They are all fully trained and accredited in the services they provide. You will love both the time spent and the smile you achieve with us.</span></span><span class="PractitionerMainText"></span></p>
		</div>-->
		<p>&nbsp;</p>
		<div class="PractitionerPublicFacingTabWrapper">
		  <p><span class="PractitionerSubHeadings">
		  <img src="<?php echo $path; ?>images/publicfacing.png" height="81" /></span>
		  <span class="PractitionerSubHeadings" style='font-size:20px;font-weight:bold;'>Your public facing profile information</span></p>
		<p class="PractitionerMainText">The functions below are setup so that you can add, update and delete information on your public facing profile page. You can preview all profile information before you make it public.  </p>
		<p class="PractitionerMainText"><strong>Please note:</strong> You may need to hold down the shift or command key and then reload the browser page if you find that the images are not refreshing correcty in each section of this website update panel.</p>

            <p class="PractitionerMainText"><strong>Profile Setup Wizard:</strong><a href="../signupWizardFacilitator/index.htm"></a>  Please click here to view a quick overview of how to setup your Wotmed Profile</p>

            <p class="PractitionerMainText"><strong>Wotmed Search:</strong> Please note that it takes 24 hours before your profile will be visible by Patients via Wotmed Search.</p>


            <p class="PractitionerMainText">&nbsp;</p>

            <div class="stdWrapper narrowWrapper" onmouseover="this.style.background='white';"onmouseout="this.style.background='#F0F0F0';" style="cursor: pointer" onclick="location.href='WotmedConsultations.php';">
                <p><img src="../WotmedPaidConsultationsGraphicCP.jpg" alt="Wotmed Paid Consultations" width="210" height="225" border="0" /></a></span> <a href="WotmedConsultations.php" class="hyperlinks">Get paid to consult with Wotmed Patients</a></p>

            </div>

        <div class="stdWrapper narrowWrapper" onmouseover="this.style.background='white';"onmouseout="this.style.background='#F0F0F0';" style="cursor: pointer" onclick="location.href='../UpdateMyProfilePhotoFacilitator.php';">
				<p><span class="PractitionerBody"><a href="../UpdateMyProfilePhotoFacilitator.php"><img src="images/blankSilhouetteMale.png" alt="a3" width="72" height="72" /></a></span> <a href="../UpdateMyProfilePhotoFacilitator.php" class="hyperlinks">My Business Profile Logo</a></p>
				<p><span class="PractitionerMainText">
				Upload your profile photo</span> (which is shown next to your business information at the top right hand side of your profile)</p>
			  </div>
        
			<div class="stdWrapper narrowWrapper" onmouseover="this.style.background='white';"onmouseout="this.style.background='#F0F0F0';" style="cursor: pointer" onclick="location.href='<?php echo $path; ?>mySchedule.php';">
				<p><span class="PractitionerBody"><a href="<?php echo $path; ?>mySchedule.php"><img src="<?php echo $path; ?>images/schedule-icon.png" alt="a3" width="72" height="72" /></a></span> <a href="<?php echo $path; ?>mySchedule.php" class="hyperlinks">My Schedule</a></p>
				<p><span class="PractitionerMainText">
				Create, update and delete your surgery facilitation business scheduling information</span></p>
			  </div>
			  <div class="stdWrapper narrowWrapper" onmouseover="this.style.background='white';"onmouseout="this.style.background='#F0F0F0';" style="cursor: pointer" onclick="location.href='<?php echo $path; ?>myServices.php';">
				<p><span class="PractitionerBody"><a href="<?php echo $path; ?>myServices.php"><img src="<?php echo $path; ?>images/Service-icon.png" alt="a4" width="72" height="72" /></a></span><a href="<?php echo $path; ?>myServices.php" class="hyperlinks">My Services &amp; Price Lists</a></p>
				<p><span class="PractitionerBody"><span class="PractitionerMainText">
				Update the price of your surgery facilitation services here</span></span></p>
			  </div>
			  <div class="stdWrapper narrowWrapper" onmouseover="this.style.background='white';"onmouseout="this.style.background='#F0F0F0';" style="cursor: pointer" onclick="location.href='<?php echo $path; ?>myAdvertisements.php';" >
				<p class="PractitionerBody"><a href="<?php echo $path; ?>myAdvertisements.php"><img src="<?php echo $path; ?>images/Advertising-icon.png" alt="ad" width="72" height="72" /></a><a href="<?php echo $path; ?>myAdvertisements.php" class="hyperlinks">My Advertisements</a></p>
				<p class="PractitionerBody"><span class="PractitionerMainText">Advertisements can be added here which can be shown prominantly on your profile page</span></p>
			  </div>
			  <div class="stdWrapper narrowWrapper" onmouseover="this.style.background='white';"onmouseout="this.style.background='#F0F0F0';" style="cursor: pointer" onclick="location.href='<?php echo $path; ?>myPromotions.php';">
				<p><span class="PractitionerBody"><a href="<?php echo $path; ?>myPromotions.php"><img src="<?php echo $path; ?>images/promotions.png" alt="sale" width="72" height="72" /></a></span><a href="<?php echo $path; ?>myPromotions.php" class="hyperlinks">My Promotions</a></p>
				<p><span class="PractitionerMainText">Promotions can be added here which will be shown prominantly on your profile page</span></p>
			  </div>
			  <div class="stdWrapper narrowWrapper" onmouseover="this.style.background='white';"onmouseout="this.style.background='#F0F0F0';" style="cursor: pointer" onclick="location.href='<?php echo $path; ?>myAbout.php';">
				<p><span class="PractitionerBody"><a href="<?php echo $path; ?>myAbout.php"><img src="<?php echo $path; ?>images/Actions-help-about-icon.png" alt="a2" width="72" height="72" /></a></span><a href="<?php echo $path; ?>myAbout.php" class="hyperlinks">About Me / My Practice</a></p>
				<p><span class="PractitionerBody"><span class="PractitionerMainText">Update details about your surgery facilitation business hours, google street view maps, business phone number, business website link, photographs, videos, contact details, social networking links and PDF brochures</span></span></p>
			  </div>
			  <div class="stdWrapper narrowWrapper" onmouseover="this.style.background='white';"onmouseout="this.style.background='#F0F0F0';" style="cursor: pointer" onclick="location.href='<?php echo $path; ?>myBlog.php';">
				<p><span class="PractitionerBody"><a href="<?php echo $path; ?>myBlog.php"><img src="<?php echo $path; ?>images/blog-icon.png" alt="a10" width="72" height="72" /></a></span><a href="<?php echo $path; ?>myBlog.php" class="hyperlinks">My Blog</a></p>
				<p><span class="PractitionerBody"><span class="PractitionerMainText">Update your Professional Blog here</span></span></p>
			  </div>
			  <div class="stdWrapper narrowWrapper" onmouseover="this.style.background='white';"onmouseout="this.style.background='#F0F0F0';" style="cursor: pointer" onclick="location.href='<?php echo $path; ?>myClientCaseStudies.php';">
				<p class="copyrightText"><a href="<?php echo $path; ?>myClientCaseStudies.php"><img src="<?php echo $path; ?>images/case-icon.png" alt="" width="72" height="72" /></a><a href="<?php echo $path; ?>myClientCaseStudies.php" class="copyrightText"><span class="hyperlinks">My Client Case Studies</span></a></p>
				<p class="copyrightText">Client case studies can be added here which can be shown prominantly on your profile page</p>
			  </div>
			  <div class="stdWrapper narrowWrapper" onmouseover="this.style.background='white';"onmouseout="this.style.background='#F0F0F0';" style="cursor: pointer" onclick="location.href='<?php echo $path; ?>myWebsites.php';">
				<p class="copyrightText"><a href="<?php echo $path; ?>myWebsites.php"><img src="<?php echo $path; ?>images/websites-folder-icon.png" alt="" width="72" height="72" /></a><a href="<?php echo $path; ?>myWebsites.php" class="hyperlinks">Independent Website Details</a></p>
				<p class="copyrightText">Add information here about any independent press articles that have featured your business</p>
			  </div>
			  <div class="stdWrapper narrowWrapper" onmouseover="this.style.background='white';"onmouseout="this.style.background='#F0F0F0';" style="cursor: pointer" onclick="location.href='<?php echo $path; ?>myPress.php';">
				<p class="copyrightText"><a href="<?php echo $path; ?>myPress.php"><img src="<?php echo $path; ?>images/PressIndependent.png" alt="" width="72" height="72" /></a><a href="<?php echo $path; ?>myPress.php" class="hyperlinks">Independent Press Articles</a></p>
				<p class="copyrightText">Add information here about any independent press articles that have featured you or your business</p>
			  </div>
			  <div class="stdWrapper narrowWrapper" onmouseover="this.style.background='white';"onmouseout="this.style.background='#F0F0F0';" style="cursor: pointer" onclick="location.href='<?php echo $path; ?>myNews.php';">
				<p class="copyrightText"><a href="<?php echo $path; ?>myNews.php"><img src="<?php echo $path; ?>images/PractitionerNews.png" alt="" width="72" height="72" /></a><a href="<?php echo $path; ?>myNews.php" class="hyperlinks">My News Articles</a></p>
				<p class="copyrightText">Add News Articles to your profile and keep your patients up to date with information about your business</p>
			  </div>
		
			  <div class="stdWrapper narrowWrapper" onmouseover="this.style.background='white';"onmouseout="this.style.background='#F0F0F0';" style="cursor: pointer" onclick="location.href='<?php echo $path; ?>myLogoFacilitator.php';">
				<p class="copyrightText"><a href="<?php echo $path; ?>myLogoFacilitator.php"><img src="<?php echo $path; ?>images/default_logo.jpg" alt="" width="99" height="59" /></a><a href="<?php echo $path; ?>myLogoFacilitator.php" class="hyperlinks">My Logos</a></p>
				<p class="copyrightText">Add your surgery facilitation business logo here
					<br><br>
					<b>Note:</b> This will update your business logo which is also your primary profile logo (shown at top left hand side of your profile)
				</p>
			  </div>
			  <div class="stdWrapper narrowWrapper" onmouseover="this.style.background='white';"onmouseout="this.style.background='#F0F0F0';" style="cursor: pointer" onclick="location.href='<?php echo $path; ?>mySlogan.php';">
				<p class="copyrightText"><a href="<?php echo $path; ?>mySlogan.php"><img src="<?php echo $path; ?>images/stock-vector-service-plus-quality-retro-ad-art-banner-80117605.jpg" alt="" width="85" height="71" /></a><a href="<?php echo $path; ?>mySlogan.php" class="hyperlinks">My Slogan</a></p>
				<p class="copyrightText">This section is where you can update your Surgery Facilitation website slogan heading</p>
			  </div>
			  <div class="stdWrapper narrowWrapper" onmouseover="this.style.background='white';"onmouseout="this.style.background='#F0F0F0';" style="cursor: pointer" onclick="location.href='<?php echo $path; ?>myPhilosophy.php';">
				<p><a href="<?php echo $path; ?>myPhilosophy.php"><img src="<?php echo $path; ?>images/stock-photo-stack-of-open-books-with-apple-isolated-on-white-108879077.jpg" alt="" width="114" height="86" /></a><a href="<?php echo $path; ?>myPhilosophy.php" class="hyperlinks">My Philosophy</a></p>
				<p class="PractitionerMainText">Add your surgery facilitation business philosophy here</p>
			  </div>
			  <div class="stdWrapper narrowWrapper" onmouseover="this.style.background='white';"onmouseout="this.style.background='#F0F0F0';" style="cursor: pointer" onclick="location.href='<?php echo $path; ?>myCustomerCare.php';">
				<p><a href="<?php echo $path; ?>myCustomerCare.php"><img src="<?php echo $path; ?>images/User-Group-icon.png" alt="" width="72" height="72" /></a><a href="<?php echo $path; ?>myCustomerCare.php" class="hyperlinks">Customer Care Policy</a></p>
				<p class="PractitionerMainText">Add your surgery facilitation business customer care policy </p>
			  </div>
			  <div class="stdWrapper narrowWrapper" onmouseover="this.style.background='white';"onmouseout="this.style.background='#F0F0F0';" style="cursor: pointer" onclick="location.href='<?php echo $path; ?>myTeam.php';">
				<p><a href="<?php echo $path; ?>myTeam.php"><img src="<?php echo $path; ?>images/Groups-Meeting-Dark-icon.png" alt="" width="72" height="72" /></a><a href="<?php echo $path; ?>myTeam.php" class="hyperlinks">My Team</a></p>
				<p class="PractitionerMainText">Add information about your team that facilitates surgery here</p>
			  </div>
			  <div class="stdWrapper narrowWrapper" onmouseover="this.style.background='white';"onmouseout="this.style.background='#F0F0F0';" style="cursor: pointer" onclick="location.href='<?php echo $path; ?>myTechnology.php';">
				<p><a href="<?php echo $path; ?>myTechnology.php"><img src="<?php echo $path; ?>images/Motherboard-icon.png" alt="" width="72" height="72" /></a><a href="<?php echo $path; ?>myTechnology.php" class="hyperlinks">My Technology</a></p>
				<p class="PractitionerMainText">Add information here about the technology that you use to facilitate surgery for patients</p>
			  </div>
			  <div class="stdWrapper narrowWrapper" onmouseover="this.style.background='white';"onmouseout="this.style.background='#F0F0F0';" style="cursor: pointer" onclick="location.href='<?php echo $path; ?>myFAQ.php';"> <a href="<?php echo $path; ?>myFAQ.php"><img src="<?php echo $path; ?>images/help-icon.png" alt="" width="72" height="72" /></a><a href="<?php echo $path; ?>myFAQ.php" class="hyperlinks">My FAQ's</a>
				<p class="PractitionerMainText">Add a list of FAQ's to your profile</p>
			  </div>
			  <div class="stdWrapper narrowWrapper" onmouseover="this.style.background='white';"onmouseout="this.style.background='#F0F0F0';" style="cursor: pointer" onclick="location.href='<?php echo $path; ?>myGlossary.php';">
				<p><a href="<?php echo $path; ?>myGlossary.php"><img src="<?php echo $path; ?>images/glossary-icon.png" alt="" width="72" height="72" /></a><a href="<?php echo $path; ?>myGlossary.php" class="hyperlinks">Independent Glossary</a></p>
				<p class="PractitionerMainText">Update your glossary here</p>
			  </div>
			  <div class="stdWrapper narrowWrapper" onmouseover="this.style.background='white';"onmouseout="this.style.background='#F0F0F0';" style="cursor: pointer" onclick="location.href='<?php echo $path; ?>myPrivacy.php';">
				<p><a href="<?php echo $path; ?>myPrivacy.php"><img src="<?php echo $path; ?>images/Keys-icon.png" alt="" width="72" height="72" /></a><a href="<?php echo $path; ?>myPrivacy.php" class="hyperlinks">My Privacy Statement</a></p>
				<p class="PractitionerMainText">Update your surgery facilitation business privacy statement here</p>
			  </div>
			  <div class="stdWrapper narrowWrapper" onmouseover="this.style.background='white';"onmouseout="this.style.background='#F0F0F0';" style="cursor: pointer" onclick="location.href='<?php echo $path; ?>myCopyright.php';">
				<p><a href="<?php echo $path; ?>myCopyright.php"><img src="<?php echo $path; ?>images/copyright-icon.png" alt="" width="72" height="72" /></a><a href="<?php echo $path; ?>myCopyright.php" class="hyperlinks">My Copyright Statement</a></p>
				<p class="PractitionerMainText">Update your Copyright Statement here</p>
			  </div>
			  <div class="stdWrapper narrowWrapper" onmouseover="this.style.background='white';"onmouseout="this.style.background='#F0F0F0';" style="cursor: pointer" onclick="location.href='<?php echo $path; ?>myDisclaimer.php';">
				<p><a href="<?php echo $path; ?>myDisclaimer.php"><img src="<?php echo $path; ?>images/justice-balance-icon.png" alt="" width="72" height="72" /></a><a href="<?php echo $path; ?>myDisclaimer.php" class="hyperlinks">My Disclaimer Statement</a></p>
				<p class="PractitionerMainText">Update your Disclaimer Statement here</p>
			  </div>
			  <div class="stdWrapper narrowWrapper" onmouseover="this.style.background='white';"onmouseout="this.style.background='#F0F0F0';" style="cursor: pointer" onclick="location.href='<?php echo $path; ?>myTerms.php';">
				<p><a href="<?php echo $path; ?>myTerms.php"><img src="<?php echo $path; ?>images/desktop-icon.png" alt="" width="72" height="72" /></a><a href="<?php echo $path; ?>myTerms.php" class="hyperlinks">Terms and Conditions</a></p>
				<p class="PractitionerMainText">Add your terms and conditions here</p>
			  </div>
			  <div class="stdWrapper narrowWrapper" onmouseover="this.style.background='white';"onmouseout="this.style.background='#F0F0F0';" style="cursor: pointer" onclick="location.href='<?php echo $path; ?>myAcceptedInsurance.php';">
				<p><a href="<?php echo $path; ?>myAcceptedInsurance.php"><img src="<?php echo $path; ?>images/desktop-icon.png" alt="" width="72" height="72" /></a><a href="<?php echo $path; ?>myAcceptedInsurance.php" class="hyperlinks">Accepted Insurance</a></p>
				<p class="PractitionerMainText">Add insurance you accept here</p>
			  </div>

            <div class="stdWrapper narrowWrapper" onmouseover="this.style.background='white';"onmouseout="this.style.background='#F0F0F0';" style="cursor: pointer" onclick="location.href='';">
                <p><img src="DeleteProfile.jpg" alt="" width="70" height="70" border="0" /><a href="../DeleteMyWotmedProfile.php">Delete my Wotmed Profile</a></p>
                <p class="PractitionerMainText">&nbsp;</p>
                <p class="PractitionerMainText">Click here to delete your Wotmed Profile </p>
            </div>


		
		</div>
		<!--div class="PractitionerPublicFacingTabWrapper">
		  <p><span class="PractitionerSubHeadings" style='font-size:20px;font-weight:bold;'><img src="images/privacy.jpg" width="91" height="59" />Your strictly private information</span></p>
		  <p class="PractitionerMainText">The functions below are setup so that you can add, update and delete strictly private information. This information is provided by you simply to make it easier for you to work on the wotmed.com platform.</p>
		  <p class="PractitionerMainText">&nbsp;</p>
			<div class="stdWrapper narrowWrapper" onmouseover="this.style.background='white';"onmouseout="this.style.background='#F0F0F0';" style="cursor: pointer" onclick="location.href='myBanking.php';">
			<p><span class="PractitionerBody"><a href="myBanking.php"><img src="images/Bank-icon.png" alt="a9" width="72" height="72" /></a></span><a href="myBanking.php" class="hyperlinks">My Banking and Paypal Details</a></p>
			<p><span class="PractitionerBody"><span class="PractitionerMainText">Update details about your personal and business banking </span></span></p>
			</div>
			<div class="stdWrapper narrowWrapper" onmouseover="this.style.background='white';"onmouseout="this.style.background='#F0F0F0';" style="cursor: pointer" onclick="location.href='myInsurance.php';">
			<p><a href="myInsurance.php"><img src="images/insurance-icon.png" alt="" width="65" height="76" /></a> <span class="hyperlinks"><a href="myInsurance.php" class="hyperlinks">My Professional Insurance Details</a></span></p>
			<p class="PractitionerMainText">Update details about your professional insurance details here</p>
			</div>
			<div class="stdWrapper narrowWrapper" onmouseover="this.style.background='white';"onmouseout="this.style.background='#F0F0F0';" style="cursor: pointer;" onclick="location.href='mySales.php';">
			<p><img src="images/salesPhoto.jpg" alt="" width="91" height="93" /><span class="hyperlinks"><a href="myInsurance.php" class="hyperlinks">Your Wotmed Sales</a></span></p>
			<p class="PractitionerMainText">View details about all sales you have made through the Wotmed.com platform</p>
			</div>
		</div-->
		<p class="copyrightText">&nbsp;</p><!--
		<div class="CopyrightTextWrapper">
		  <p class="copyrightText">Copyright (c) Wotmed.com 2012 All rights reserved. </p>
		</div>-->
			<?php include $path."includes/p_footer.php"; ?>
	</body>

</html>
