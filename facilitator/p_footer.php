
			<div id="footer">
				<div id="contentCurve"></div>
				<div class="clearfix" id="footerContainer">
					<div class="mrl lfloat" role="contentinfo">
						<div class="fSma fcg">

                             <span>

                                 Wotmed Platform Status: <?php

                                 $myfile = fopen("../WotmedPlatformStatus.txt", "r") or die("Undetermined");
                                 echo fread($myfile,filesize("../WotmedPlatformStatus.txt"));
                                 fclose($myfile);

                                 ?>

                            </span>
                            <br>

							<span>Copyright &copy; Wotmed.com <?php	echo date('Y'); ?> All rights reserved.</span>
						</div>
					</div>
					<div class="navigation fSma fcg" role="navigation" style='display:none'><a class="contactForm" href=
						"<?php echo $path; ?>contactus.php" accesskey="9" title=
						"Review our terms and policies.">Contact Us</a> &middot; <a class="about" href=
						"<?php echo $path; ?>#" accesskey="0" title=
						"About us">About Us</a>
					</div>
				</div>
			</div>
		</div>
		</div>
		<?php include $path."about/about.php" ;?>
		<!-- contact us section -->
		<script type="text/javascript" src="<?php echo $path; ?>classes/contact.js"></script>
		<script type="text/javascript">
			$(function() {
				$("a").css('pointer-events','auto');
				if($("#subcontainer").height()<$(window).height()){
					$("#contentMain").height($(window).height()-120);
				}
				//alert('subcontainer: '+$("#subcontainer").height()+'\n'+
					//'window: '+$(window).height());
			});
		</script>
		
		<!--contact form-->
		
		<div id='faqPage' style=''>
			<div id="faqClose" style="">Close</div>
			<div id="faq_header" class="lfloat"><br/>FAQ<br></div>
			<div class="lfloat" style="width:80%;">
				<?php include $path."newFAQ.html";



                ?>
			</div>
		</div>
		
		<div id="contact">
			<div id="close">Close</div>
			<div id="contact_header" class="lfloat"><br/>Feedback</div>
			<p class="success">Thanks! Your message has been sent.</p>



			<div class="lfloat" style="width:320px;">
			  <form action="sendFeedback.php" method="post" name="contactForm" id="contactForm">
			  <p><input name="name" id="name" type="text" size="30" value="<?php echo $rowSession['FIRSTNAME'] . " " . $rowSession['SURNAME']; ?>" /></p>
			  <p><input name="email" id="email" type="text" size="30" value="<?php echo $rowSession['EMAILADDRESS']; ?>" /></p>
			  <p>Enter your comment or query...<br/><textarea name="comment" id="comment" rows="5" cols="40"></textarea></p>
			  <p><input type="submit" id="submit" name="submit" value="Send" /></p>
			 </form>
			</div>
			<div class="lfloat" style="width:240px;margin-left:30px;margin-right:10px;">
			If your feedback includes photographs or screenshots please email those to:<br/><a href="mailto:multimediafeedback@wotmed.com?Subject=Wotmed Multimedia Feedback">
multimediafeedback@wotmed.com</a><br/>
			<br/>
			The Wotmed team is dedicated to our mission of service to humanity by connecting
patients with medical professionals globally. Your feedback is extremely valuable to us to ensure that we are putting you, our users first and building something that you want.<br/><br/>We built this platform from scratch for you.<br/><br/>We welcome your ideas on how we may improve Wotmed.  We are here to listen and to learn from you our valued clients.<br/><br/>Clarke Towson<br/>
Wotmed Chief Technology Officer
<br/><br/><br/><br/><br/>"Most startups fail because they don't make something people want"<br/>"The companies that win are the ones that put users first"<br/><br/>Paul Graham <br/>Computer Scientist</div>
		</div>

		<div id="mask"></div>

		<!--end contact form-->
		<!-- end contact us section -->
		<?php
		mysqli_close($conn);
		/*
		if(($row['PRACTITIONER_BUSINESSLOGO']=="") && isset($_SESSION['practitioner_id'])){
			?>
			<script> document.getElementById("noticeDiv").style.display = "block"; </script>
			<?php
		}*/?>
		
<div id="contactCorner">

<a class='help' href="<?php echo $path; ?>trainingvideos.html" accesskey="9" title="Wotmed Documentation and profile setup help"><img src="<?php echo $path; ?>images/WotmedDocumentation.png" width="40px"/></a>

	<a class='faq' href="<?php echo $path; ?>newFAQ.html" onclick="return false;" accesskey="9" title="Read the Wotmed Frequently Asked Questions"><img src="<?php echo $path; ?>images/WotmedFAQIconBottomRightScreen.png" width="30px"/></a>
	<a class="about" href="<?php echo $path; ?>#" accesskey="9" onclick="return false;" title="Help spread the word about Wotmed"><img src="<?php echo $path; ?>images/WordOfMouth.png" width="30px"/></a>
	<a href="<?php echo $path; ?>ideas/idea.php" accesskey="9" title="Submit your great ideas to Wotmed"><img src="<?php echo $path; ?>images/128px-Action_idea.svg.png" width="40px"/></a>
	<a href="<?php echo $path; ?>ideas/critic.php" accesskey="9" title="Submit your criticism to Wotmed"><img src="<?php echo $path; ?>images/critical-notifications-icon.png" width="40px"/></a>
	<a class="contactForm" href="<?php echo $path; ?>#" accesskey="9" title="Contact us at Wotmed" onclick="return false;"><img src="<?php echo $path; ?>images/contact.png" width="40px"/></a>
	<div>
