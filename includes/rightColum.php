<div id="rightCol" class="rfloat">


				<div id="userInfo">
					<div class="lfloat">
						<div class="bold marBotBig subtitle fMid fColor lfloat" style='padding-top:20px;padding-left:10px;'>
						<img src='images/PractitionerBusinessInformation.png' style='height:23px;'>



						<?php //echo $row['PRACTITIONER_BUSINESSNAME']; ?> Business Information </div>
						<?php
						
										
						
						if(isset($_SESSION['id']) && !isset($_GET['update']) && !isset($_GET['id']))
						echo "<div  class='rfloat'><a href='practitioner_profile.php?update=true'>Update your Business Information</a></div>";
						//else
							//echo "<a href='profile.php'>Back to Profile</a>";
						?>
					</div>
				<?php




				
				if(!isset($_GET['update']) || $_GET['update']=="profile"){ ?>
					<div class="gborder lfloat marBotBig" style="width:95%">
					 <div style='max-width:95%;float:left;padding:5px;'>
						<img src="photos/thumbs/<?php




                        // test to see if the practitioners business logo has been set - if it hasn't we are probably logged in for the first time
                        // display the blank logo until the practitioner has set it specifically

                        $logo = $row['PRACTITIONER_BUSINESSLOGO'];

                        if(empty($logo))
                        {
                            // display the blank logo
                            $logo = 'blanklogo.gif';
                            echo $logo;

                        }

                        else
                        {
                            echo $row['PRACTITIONER_BUSINESSLOGO'];


                        }




                        ?>" style='max-height:40px;'/>
					</div>
					<table border='0' style="width:100%">






						<tr>
                            <td style="width:20px;"><img src="images/PhoneFinal.png" style="height:25px;padding-top:3px;"></td>
							<th style='width:60px;vertical-align:bottom;'>Phone</th>
							<td style="vertical-align:bottom;"><?php echo $row['PRACTITIONER_BUSINESSNUMBER'] ?></td>
						</tr>
						<tr>
							<td><img src="images/PractitionerAddress.png" style="height:25px;padding-top:3px;"></td>
							<th style="vertical-align:bottom;">Address</th>
							<td style="vertical-align:bottom;"><?php echo $row['PRACTITIONER_BUSINESSADDRESS'] ?></td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<th>&nbsp;</th>
							<td><?php echo $row['PRACTITIONER_BUSINESSSUBURB'] ?></td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<th>&nbsp;</th>
							<td><?php echo $row['PRACTITIONER_BUSINESSPOSTCODE'] ?></td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<th>&nbsp;</th>
							<td><a href="http://www.google.com.au/#gs_rn=15&gs_ri=psy-ab&suggest=p&cp=9&gs_id=ao&xhr=t&q=<?php echo str_replace(" ","+",$row['PRACTITIONER_BUSINESSCITY']." ".strtoupper($row['COUNTRYNAME'])); ?>" target="_blank"><?php echo $row['PRACTITIONER_BUSINESSCITY']." ".strtoupper($row['COUNTRYNAME']); ?></a></td>
						</tr>
						<tr>
							<td><img src="images/EmailIcon.png" style="height:25px;padding-top:3px;padding-left:2px;"></td>
							<th style="vertical-align:bottom;">Email</th>
							<td style="vertical-align:bottom;"><div style="overflow-horizontal:auto;"><?php 
							
							
							
							
							
							
							//$srtEmail=substr($row['EMAILADDRESS'],0,20);
							$srtEmail="Send Email";
							echo "<a href='#' onclick='displayContactUser({$row['PARTICIPANT_NUMBER']})'><span>". $srtEmail ."</span></a>"; 
							?></div></td>
						</tr>
						<?php
							$webLink="";
							//msgBox($row['PRACTITIONER_NUMBER']);
							$temp=getDetails($conn,$row['PRACTITIONER_NUMBER'],10);
							if($temp!=null)
							{
								$detailResult= mysqli_fetch_array($temp);
								$webLink.=$detailResult['DETAIL_TITLE'];
								
							
							$input = $webLink;

							// in case scheme relative URI is passed, e.g., //www.google.com/
							$input = trim($input, '/');

							// If scheme not included, prepend it
						if (!preg_match('#^http(s)?://#', $input)) 
						{
   							 $input = 'http://' . $input;
						}

							$urlParts = parse_url($input);

							// remove www
							$domain = preg_replace('/^www\./', '', $urlParts['host']);

						//	echo $domain;
							
							$webLink = $domain;

						// output without www or http:// or final /
							
							
							
								
							}
						?>
						<tr>
							<td><img src="images/My-Websites-icon.png" style="height:21px;padding-left:3px"></td>
							<th>Independent Website</th>
							<td style="vertical-align:text-top;"><?php 
							$srtEmail=substr($webLink,0,20);
							
						//	 echo "<a href='http://$webLink' target='_blank'><span title='$webLink'>". $srtEmail ."</span></a>"; 
							
							echo "<a href='http://$webLink' target='_blank'><span title='$webLink'>". $srtEmail ."</span></a>"; 
							
							
							?>
							</td></td>
						</tr>



						<tr><td colspan='3'>&nbsp;</td></tr>




                    </table>
					</div>




					<div class="gborder lfloat marBotBig" style="width:95%">
						<div class="lfloat">
							<div class="bold marBotBig subtitle fMid fColor lfloat" style='padding-top:5px;'>
							<img src='images/PastedGraphic-1.png' style='width:20px;'>
							<?php //echo $row['PRACTITIONER_BUSINESSNAME']; ?> Opening Hours </div>
						</div>









                        <div class='lfloat' style='width:95%'>
						<?php
							$tempDay="<table>";
							$days = array('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday',);
							$dayQuery = "SELECT * FROM AVAILABILITY WHERE PRACTITIONER_NUMBER=".$row['PRACTITIONER_NUMBER'];$daysResult=mysqli_query($conn,$dayQuery);
							if(mysqli_num_rows($daysResult)!=0){
								while($daysFound=mysqli_fetch_array($daysResult)){
									$tempDay.="
										<tr>
											<td>{$days[$daysFound['DAY']]}</td>
											<td>{$daysFound['TIMESTART']}</td>
											<td>-</td>
											<td>{$daysFound['TIMEEND']}</td>
										</tr>
									";
								}
							}
							$tempDay.="</table>";
							echo $tempDay;
						?>
						</div>
					</div>
					<?php
					$ads1="";$ads2="";$ads3="";$ads4="";
					$adsQuery="SELECT * FROM ADVERTS WHERE PRACTITIONER_NUMBER = {$row['PARTICIPANT_NUMBER']} LIMIT 0,1";
					$adsResult=mysqli_query($conn,$adsQuery);
					if(mysqli_num_rows($adsResult)!=0){
						$data=mysqli_fetch_array($adsResult);/*
						$link=urlencode($data["ADVERTSHYPERLINK"]);
						$adsTable = "
						<tr style='background: rgb(253,249,228);'>
						<input type='hidden' id='adsID' name='adsID' value='{$data["ADVERTS_NUMBER"]}'>
						<td colspan='2' onclick='advertisment()' onmouseover='ShowText(\'Message\'); return true;' onmouseout='HideText(\'Message\'); return true;'>
						<h3>{$data["ADVERTSTITLE"]}</h3>
						{$data["ADVERTSRTEXT"]}<br>
						<a href='ajax/addAdsClick.php?id={$data["ADVERTS_NUMBER"]}'>{$data["ADVERTSHYPERLINK"]}</a></td></tr>";*/
						$ads1=$data["ADVERTS_NUMBER"];
						$ads2=$data["ADVERTSTITLE"];
						$ads3=$data["ADVERTSRTEXT"];
						$ads4=$data["ADVERTSHYPERLINK"];
					}
					$add_str = $row["PRACTITIONER_BUSINESSGOOGLEMAPADDRESS"];
					$prac_str = $row["PRACTITIONER_BUSINESSLOGO"] . "|" . $row["PRACTITIONER_BUSINESSNAME"] . "|" . $row["PRACTITIONER_BUSINESSADDRESS"] . "," . $row["PRACTITIONER_BUSINESSSUBURB"] . " " . $row["PRACTITIONER_BUSINESSPOSTCODE"] . "|" . $ads1 . "|" . $ads2 . "|" . $ads3 . "|" . $ads4 . "|" . getSpeciality($conn,$row["SPECIALITY_NUMBER"]) . ";";
					?>
					<div id="map_canvas" style="width:98%; height:300px; margin-bottom:10px" class='lfloat' \></div>











                    <script type="text/javascript">
							setPlace('<?php echo $add_str; ?>','<?php echo $prac_str; ?>');
						</script>
					<div class="gborder lfloat" style="width:95%;">
						<table border='0' style="width:100%;">
						<!--
						<tr>
							<td style="width:auto;"><img src="images/WotmedMembershipIcon.png" style="height:25px;padding-top:3px;"></td>
							<th style='width:90px;'>Professional Statement</th>
							<td><?php echo $row['PRACTITIONER_BUSINESSPROFESSIONALSTATEMENT'] ?></td>
						</tr>
						-->
						<tr>
							<td style="width:20px;"><img src="images/WotmedPuzzleIconTransparent.png" style="height:25px;padding-top:3px;padding-left:2px;"></td>
							<th style='width:80px;'>Speciality</th>
							<td>
								<?php
									$query="SELECT SPECIALITY FROM SPECIALITY WHERE SPECIALITY_NUMBER ='" . $row['SPECIALITY_NUMBER'] . "'";
									$speciality=mysqli_query($conn,$query);
									if(mysqli_num_rows($speciality)!=0){
										$spec=mysqli_fetch_array($speciality);
										echo $spec[0];
									}
								?>
							</td>
						</tr>






						<tr>
							<td><img src="images/WotmedEducation_icon.png" style="height:20px;padding-top:3px;"></td>
							<th>Qualification</th>
							<td><?php echo $row['QUALIFICATIONS'] ?></td>
						</tr>
						<tr>
							<td><img src="images/WotmedEducationIcon.png" style="height:20px;padding-top:3px;"></td>
							<th>Education</th>
							<td><?php echo $row['EDUCATION'] ?></td>
						</tr>
						<tr>
							<td style="padding-left:4px;"><img src="images/gr-med-icon2.png" style="height:25px;padding-top:3px;"></td>
							<th>Board Certifications</th>
							<td><?php echo $row['BOARDCERTIFICATIONS'] ?></td>
						</tr>
						<tr>
							<td><img src="images/WotmedMembershipIcon.png" style="height:23px;padding-top:3px;padding-left:3px;"></td>
							<th>Professional Membership</th>
							<td><?php echo $row['PRACTITIONER_PROFESSIONALMEMBERSHIP'] ?></td>
						</tr>
						<tr>
							<td><img src="images/WotmedAwardIcon.png" style="height:25px;padding-top:3px;padding-left:7px;"></td>
							<th>Awards and Publications</th>
							<td><?php echo $row['AWARDSANDPUBLICATIONS'] ?></td>
						</tr>
						<tr>
							<td><img src="images/WotmedHospitalIconTransparent.png" style="height:23px;padding-top:3px;padding-left:2px;"></td>
							<th>Hospital Affiliation</th>
							<td><?php echo $row['HOSPITALAFFILIATION'] ?></td>
						</tr>
						<tr>
							<td><img src="images/CountryIcon.jpg" style="height:23px;padding-top:3px;padding-left:2px;"></td>
							<th>Country Trained</th>
							<td><?php echo $row['C_DEGREE'] ?></td>
						</tr>
						<tr>
							<td><img src="images/Training-512.png" style="height:23px;padding-top:3px;padding-left:2px;"></td>
							<th>Country Degree Obtained</th>
							<td><?php echo $row['C_TRAINED'] ?></td>
						</tr>
					</table>
					</div>
						<?php }
						else
						{
						if($_GET['update']=="true")
						{ ?>
							<table>
							<form method="post" action="practitioner_profile.php">
							<tr>
								<th>Phone</th>
								<td><input type="text" name="businessNumb" value="<?php echo $row['PRACTITIONER_BUSINESSNUMBER'] ?>"></td>
							</tr>
							<tr>
								<th>Address</th>
								<td><input type="text" name="address" value="<?php echo $row['PRACTITIONER_BUSINESSADDRESS'] ?>"></td>
							</tr>
							<tr>
								<th>Suburb</th>
								<td><input type="text" name="suburb" value="<?php echo $row['PRACTITIONER_BUSINESSSUBURB'] ?>"></td>
							</tr>
							<tr>
								<th>Post Code</th>
								<td><input type="text" name="postCode" value="<?php echo $row['PRACTITIONER_BUSINESSPOSTCODE'] ?>"></td>
							</tr>
							<tr>
								<th>Country</th>
								<td>
								<select name='country' >
									<option value='0'>Select Country</option>
									<?php
									$countryList=getCountryList($conn);
									while($country=mysqli_fetch_array($countryList)){
										if($row['B_COUNTRY_NUMBER']==$country[0])
											$selected=" selected=true ";
										else
											$selected="";
										echo "<option value='$country[0]'$selected>$country[1]</option>";
									}
									?>
								</select>
								</td>
							</tr>
							<tr>
								<th>Professional Statement</th>
								<td><input type="text" name="profStatement" value="<?php echo $row['PRACTITIONER_BUSINESSPROFESSIONALSTATEMENT'] ?>"></td>
							</tr>
							<tr>
								<th>Speciality</th>
								<td>
									<?php
										$query="SELECT SPECIALITY FROM SPECIALITY WHERE SPECIALITY_NUMBER ='" . $row['SPECIALITY_NUMBER'] . "'";
										$currentSpeciality=mysqli_query($conn,$query);
										if(mysqli_num_rows($currentSpeciality)!=0){
											$currentSpec=mysqli_fetch_array($currentSpeciality);
										}
										$query="SELECT * FROM SPECIALITY ORDER BY SPECIALITY";
										$speciality=mysqli_query($conn,$query);
										if(mysqli_num_rows($speciality)!=0){
										?> <select name='speciality' id='speciality'> <?php
											while($spec=mysqli_fetch_array($speciality)){
											if(mysqli_num_rows($currentSpeciality)!=0){
												if($currentSpec[0]==$spec[1])
													echo "<option value='" . $spec[0] . "' selected>" . $spec[1] . "</option>";
												else
													echo "<option value='" . $spec[0] . "'>" . $spec[1] . "</option>";
											}	
											else
												echo "<option value='" . $spec[0] . "'>" . $spec[1] . "</option>";
											}
										?> </select> <?php
										}
									?>
								</td>
							</tr>
							<tr>
							<tr>
								<th>Qualification</th>
								<td><textarea rows="1" cols="15" name="qualifications" wrap="physical"><?php echo $row['QUALIFICATIONS'] ?></textarea></td>
							</tr>
								<th>Education</th>
								<td><textarea rows="1" cols="15" name="education" wrap="physical"><?php echo $row['EDUCATION'] ?></textarea></td>
							</tr>
							<tr>
								<th>Board Certifications</th>
								<td><textarea rows="1" cols="15" name="boardCertification" wrap="physical"><?php echo $row['BOARDCERTIFICATIONS'] ?></textarea></td>
							</tr>
							<tr>
								<th>Professional Membership</th>
								<td><textarea rows="1" cols="15" name="practitioner_professionalmembership" wrap="physical"><?php echo $row['PRACTITIONER_PROFESSIONALMEMBERSHIP'] ?></textarea></td>
							</tr>
							<tr>
								<th>Awards and Publications</th>
								<td><textarea rows="1" cols="15" name="awardsandpublications" wrap="physical"><?php echo $row['AWARDSANDPUBLICATIONS'] ?></textarea></td>
							</tr>
							<tr>
								<th>Hospital Affiliation</th>
								<td><textarea rows="1" cols="15" name="hospitalaffiliation" wrap="physical"><?php echo $row['HOSPITALAFFILIATION'] ?></textarea></td>
							</tr>
							<tr>
								<th>Country Trained</th>
								<td><textarea rows="1" cols="15" name="cdegree" wrap="physical"><?php echo $row['C_DEGREE'] ?></textarea></td>
							</tr>
							<tr>
								<th>Country Degree Obtained</th>
								<td><textarea rows="1" cols="15" name="ctrained" wrap="physical"><?php echo $row['C_TRAINED'] ?></textarea></td>
							</tr>



							<tr>
								<td><input type="submit" value="Update"></td>
								<td><input type="button" value="Cancel" onclick="back()"></td>
							<tr>
							</form>
						<?php 
							}
						} ?>
					</table>
				</div>
				<?php //include "includes/profile_details.php"; ?>
				<?php
				//echo "Search <input type='text' name='name' id='name' onkeyup=\"updateSearchList(this.value)\" autocomplete='off'>";
				?>
				<div id="listOfPatients"></div>
				<!--
				<input type="button" onclick="$.ajax({type:'GET',url:'toolKit/toolKit.php',data:{practitioner_id:'<?php echo $row['PRACTITIONER_NUMBER']; ?>'}}).done(function(msg){$('#embedCode').append(msg.replace(/</g,'\&\lt\;').replace(/>/g,'\&\gt\;').replace(/\t/g,''));$('#embedCode').css('display','inline-block');});this.style.visible=false;" value="Show Embed Code"/>
				<textarea col="20" rows="5" id="embedCode" style="display:none;"></textarea>-->
			</div>