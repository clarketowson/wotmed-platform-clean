<?php
	if(!isset($_SESSION['id'])){
		redirect($path."login.php");
	}

?>
		<div class="topBar" >
			<div class="bar_frame">

				 <a href="http://platform.wotmed.com"><div class="lfloat"><img src="<?php echo $path; ?>images/WotmedLogoTransparent.png" height="30px"/></a></div>

         <!--   <div class="w_logo"><img src="images/WotmedLogoTransparent.png" height="45" alt="wotmedLogo" style='position:relative; left:-30px; padding:0px;padding-top:10px;' /></div> -->
                 
                
				<!--<form id="navSearch">
					<div class="uiTypeahead">
						<input type="text" size="50" />
					</div>
				</form>-->
				<?php 
					if(isset($_SESSION['CART']))
						$sesItem=count($_SESSION['CART']). " item(s)";
					else
						$sesItem="0 Item(s)";
				?>
				<div id="nav" class="rfloat">
					<ul>
						<li class="lfloat">
						<?php
							if(isset($_SESSION['practitioner_id'])){
								echo "<a href='" . $path ."practitioner_profile.php'>";
							}
							else
								if(isset($_SESSION['id'])){
									echo "<a href='" . $path ."profile.php'>";
								}
							?>
							
							<div class="tinyDiv"><img class="tinyPhoto" src="<?php echo $path . "photos/thumbs/" . $ppFileNameSession; ?>"/></div><span class="smallName"><?php echo $rowSession["FIRSTNAME"]; ?></span></a>
						</li>
						<?php 
						if(isset($_GET['id'])){ ?>
						<li class='lfloat'>
							<div id="shopChart" style="width:auto;margin-right:7px;">
								<a href="<?php echo $path;?>shoppingChart/chart.php" style="position:relative;">
									<div style="float:left"><img src="<?php echo $path;?>images/WotmedShoppingCartGraphic.png" style='height:25px;padding-top:3px;' /></div>
									<div id="SCQty" style='float:left'><?php echo $sesItem; ?></div>
								</a>
								<div id="addItem" style="position: absolute;
									top: 40px;
									background-color: rgb(66,143,137);
									padding: 10px 10px;
									border:2px solid black;
									-moz-border-radius: 10px;
									-webkit-border-radius: 10px;
									-khtml-border-radius: 10px;
									border-radius: 10px;
									display:none;
									width:100px;">item added</div>
							</div>
						</li>
						<?php } ?>
						<li class="lfloat">
							<a href="<?php echo $path; ?>logout.php"><span class="smallName tinyPhoto">Log Out</span></a>
						</li>
					</ul>
				</div>
			</div>
		</div>

		<div id="container">
			<div id="subcontainer">
