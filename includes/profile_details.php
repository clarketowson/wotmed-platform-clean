<?php 
	$PIHeader=array("Name","-","Email Address","Home Phone","Work Phone","Mobile Phone","Home Address","-","-","-","Date of Birth","Gender","Ethnicity","Religion");
	$PIResult=array("FIRSTNAME","SURNAME","EMAILADDRESS","HOMEPHONENUMBER","WORKPHONENUMBER","MOBILEPHONENUMBER","ADDRESS","SUBURB","STATE","POSTCODE","DOB","GENDER","ETHNIC","RELIGION");
	
	$FIHeader=array("Bank Name","Bank Branch","Account Name","BSB Number","Account Number","Internet Payment Method");
	$FIResult=array("BANKNAME","BANKBRANCH","ACCOUNTNAME","BSBNUMBER","ACCOUNTNUMBER","INTERNETPAYMENTMETHOD");
	
	$header=array( 1=>"Ethnic","Religion",7=>"Birthday","Gender","Email","Bank Name", "Branch", "BSB Number","Account Name", "Account Number","Internet Payment Method", 18=> "Home Phone Number","Mobile Phone Number","Work Phone Number","Preferred Phone Number","Skype Username","Health Insurance Provider","Address","Suburb","Postcode","State","City","Country");
	$rowPatientSessionHeader=array("PARTICIPANT_NUMBER","ETHNIC","RELIGION","USERNAME","PASSWORD","FIRSTNAME","SURNAME","DOB","GENDER","EMAILADDRESS","BANKNAME","BANKBRANCH","BSBNUMBER","ACCOUNTNAME","ACCOUNTNUMBER","INTERNETPAYMENTMETHOD","PROFILEPHOTO","GOOGLEMAPADDRESS","HOMEPHONENUMBER","MOBILEPHONENUMBER","WORKPHONENUMBER","PREFERREDPHONENUMBER","SKYPEUSERNAME","HEALTHINSURANCEPROVIDER_NUMBER","ADDRESS","SUBURB","POSTCODE","STATE","CITY","COUNTRY_NUMBER");
	
	if(!isset($_SESSION['id'])){
		header("location:login.php");
	}
	if(isset($_SESSION['id'])){
		$rowPatientSession=getParticipantDetail($conn,$_SESSION['id']);
		$rowPatientSession['DOB']=formatDOB($rowPatientSession['DOB']);
	}
	if(isset($_GET['id'])){
		$temp=explode("'",$_GET['id']);
		$id=$temp[0];
		$rowPatient=getParticipantDetail($conn,$id);
	}
	?>
	<div id="userInfo">
		<?php 
		/*
			if(isset($_SESSION['practitioner_id']) && !isset($_GET['id'])){
				if(!isset($_GET['update']) && !isset($_GET['practitioner_id']))
					echo "<div class='rfloat'><a href='practitioner_profile.php?update=profile'>Edit Profile</a></div>";
			}
			else
				if(isset($_SESSION['id']) && !isset($_GET['id'])){
					if(!isset($_GET['update']) && !isset($_GET['id']))
							echo "<div class='rfloat'><a href='profile.php?update=profile'>Edit Profile</a></div>";
				}
		*/
		?>	
		<div class='lfloat'>
			<?php
			if(!isset($_GET['id']))
			{
				if(!isset($_GET['update']) || $_GET['update']=="true")
				{
				/*
					$rowPatientSessionHeader[28]="COUNTRYNAME";
					$rowPatientSessionHeader[23]="HEALTHINSURANCEPROVIDER_NAME";
					for($i=1;$i<28;$i++){
						if($i==3)
							$i=7;
						if($i==16)
							$i=18;
						
						if($i==21)
						{
							echo "<tr><th>" . $header[$i] . "</th>";
							Switch($rowPatientSession[$rowPatientSessionHeader[$i]])
							{
								case 0:
									echo "<td>Home Phone Number</td></tr>";
									break;
								case 1:
									echo "<td>Mobile Phone Number</td></tr>";
									break;
								case 2:
									echo "<td>Work Phone Number</td></tr>";
									break;
							}
						}
						else
						{
							if($rowPatientSession[$rowPatientSessionHeader[$i]]!=""  && $i!=21)
							{
								echo "<tr><th>" . $header[$i] . "</th>";
								echo "<td>" . $rowPatientSession[$rowPatientSessionHeader[$i]] . "</td></tr>";
							}
						//}
					}
					echo "<tr><th>" . $header[$i] . "</th>";
					echo "<td><img src='images/flag" . $rowPatientSession['COUNTRYFLAG'] . "' height='8px' align='left'/> <img src='images/coatofarms" . $rowPatientSession['COUNTRYCOATOFARMS'] . "' height='16px' align='left'/> " . $rowPatientSession[$rowPatientSessionHeader[$i]] . "</td></tr>";
					$index=0;
					if($rowPatientSession['LANGUAGES']!=null)
						while($fetch=mysqli_fetch_array($rowPatientSession['LANGUAGES'])){
							$index++;
							$header=$index==1 ? "Language" : "";
							echo "<tr><th>" . $header . "</th><td>" . $fetch['LANGUAGE'];
						}
				*/

                    echo "
					<div class='marBotBig headerTextContainer lfloat' style='width:96%;padding-top:0px;'>
						<div class='bold marBotBig subtitle fMid fColor' style='padding-top:20px;'>
							<img src='HowToPatientIcon.png' style='width:25px;'>

							<strong>Profile Setup Wizard</strong><br><a href='../signupWizardPatient/index.htm'> <br>Please click here to view a quick overview of how to setup your Wotmed Profile</a></p>

						</div>
						<div class='rfloat'>
							<!--<a href='profile.php?update=profile'>Update your financial information</a>-->
						</div>
					</div>
				";

				echo "
					<div class='marBotBig headerTextContainer lfloat' style='width:96%'>
						<div class='bold marBotBig subtitle fMid fColor' style='padding-top:20px;'>
							<img src='images/Personal information.png' style='width:25px;'>
							Your Personal Information
						</div>
						<div class='rfloat'>
							<a href='profile.php?update=profile'>Update your personal information</a>
						</div>
					</div>
				";
				echo "<div class='gborder lfloat' style='width:96%;margin-top:-8px;'>";
				echo "<table>";
					for($i=0;$i<count($PIHeader);$i++){
						if($i==0){
							echo "
							<tr>
								<th>$PIHeader[$i] </th>
								<td>{$rowPatientSession[$PIResult[$i]]} {$rowPatientSession[$PIResult[$i+1]]}</td>
							</tr>
							";
							echo "
							<tr>
								<th>{$PIHeader[$i+2]} </th>
								<td><a href='mailto:{$rowPatientSession[$PIResult[$i+2]]}'>{$rowPatientSession[$PIResult[$i+2]]}</a></td>
							</tr>
							";
							$i+=3;
						}
						if($i==6){
							echo "
							<tr>
								<th style='vertical-align:text-top;'>$PIHeader[$i] </th>
								<td>{$rowPatientSession[$PIResult[$i]]}<BR>{$rowPatientSession[$PIResult[$i+1]]} <BR>{$rowPatientSession[$PIResult[$i+2]]} {$rowPatientSession[$PIResult[$i+3]]}
								<BR>{$rowPatientSession['CITY']} ".strtoupper($rowPatientSession['COUNTRYNAME'])."</td>
							</tr>
							";
							$i+=4;
						}
						echo "
						<tr>
							<th style='width:90px;'>$PIHeader[$i] </th>
							<td>{$rowPatientSession[$PIResult[$i]]}</td>
						</tr>
						";
					}
				echo "</table>";
				echo "</div>";

				echo "
					<div class='marBotBig headerTextContainer lfloat' style='width:96%;padding-top:0px;'>
						<div class='bold marBotBig subtitle fMid fColor' style='padding-top:20px;'>
							<img src='images/FinancialInformation.png' style='width:25px;'>
							Your Financial Information
						</div>
						<div class='rfloat'>
							<!--<a href='profile.php?update=profile'>Update your financial information</a>-->
						</div>
					</div>
				";




				echo "<div class='gborder lfloat' style='width:96%;margin-bottom:10px;'>";
				echo "<table>";
					for($i=0;$i<count($FIHeader);$i++){
						echo "
						<tr>
							<th style='width:90px;'>$FIHeader[$i] </th>
							<td>{$rowPatientSession[$FIResult[$i]]}</td>
						</tr>
						";
					}
				echo "</table>";
				echo "</div>";
				}else{
				echo "
					<table>
					<form method='post' action='profile.php'>
				";
					if($_GET['update']=="profile")
					{
						for($i=1;$i<=29;$i++){
							if($i==3)
								$i=10;
							if($i==16)
								$i=18;
							if($i==10)
							{
								echo "<tr><th>" . $header[$i] . "</th>";
								$query="SELECT BANKNAME FROM MASTERBANK";
								echo "<td><select id='{$rowPatientSessionHeader[$i]}' name='{$rowPatientSessionHeader[$i]}' style='width:200px;'>";
								$nameOfBank=mysqli_query($conn,$query);
								if(mysqli_num_rows($nameOfBank)>0){								
									while($bankName=mysqli_fetch_array($nameOfBank)){
										if($bankName[0]==$rowPatientSession[$rowPatientSessionHeader[$i]])
											echo "<option label='{$bankName[0]}' selected='selected'>{$bankName[0]}</option>";
										else
											echo "<option label='{$bankName[0]}'>{$bankName[0]}</option>";
									}
								}
								echo "</td>";
							}
							if($i==21)
							{
								echo "<tr><th>" . $header[$i] . "</th>";
								?>
								<td>
								<select name='PREFERREDPHONENUMBER' >
									<option value='0'>Home Phone Number</option>
									<option value='1'>Mobile Phone Number</option>
									<option value='2'>Work Phone Number</option>
								</select>
								</td></tr>
								<?php
							}
							elseif($i==23)
							{
								echo "<tr><th>" . $header[$i] . "</th>";
								?>
								<td>
								<select name='<?php echo $rowPatientSessionHeader[$i]; ?>' >
									<option value='0'>Select Health Insurance Provider</option>
									<?php
									$insuranceList=getInsuranceList($conn);
									while($insurance=mysqli_fetch_array($insuranceList)){
										if($rowPatientSession[$rowPatientSessionHeader[$i]]==$insurance[0])
											$selected=" selected=true ";
										else
											$selected="";
										echo "<option value='$insurance[0]'$selected>$insurance[1]</option>";
									}
									?>
								</select>
								</td></tr>
								<?php
							}
							elseif($i==29)
							{
								echo "<tr><th>" . $header[$i] . "</th>";
								?>
								<td>
								<select name='<?php echo $rowPatientSessionHeader[$i]; ?>' >
									<option value='0'>Select Country</option>
									<?php
									$countryList=getCountryList($conn);
									while($country=mysqli_fetch_array($countryList)){
										if($rowPatientSession[$rowPatientSessionHeader[$i]]==$country[0])
											$selected=" selected=true ";
										else
											$selected="";
										echo "<option value='$country[0]'$selected>$country[1]</option>";
									}
									?>
								</select>
								</td></tr>
								<?php
							}
							elseif($i!=10)
							{
								echo "<tr><th>" . $header[$i] . "</th>";
								echo "<td><input type='text' name='". $rowPatientSessionHeader[$i] . "' id='". $rowPatientSessionHeader[$i] . "' value='" . $rowPatientSession[$rowPatientSessionHeader[$i]] . "'></td></tr>";
							}
						}
						$index=0;
						if($rowPatientSession['LANGUAGES']!=null)
							while($fetch=mysqli_fetch_array($rowPatientSession['LANGUAGES'])){
								$index++;
								$header=$index==1 ? "Language" : "";
								echo "<tr><th>" . $header . "</th><td><input type='text' class='LANGUAGE' id='LANGUAGE" . $index . "' name='LANGUAGE" . $index . "' value='" . $fetch['LANGUAGE'] . "' />";
								echo "<input type='hidden' name='LANGUAGESPOKEN_NUMBER[]' value='" . $fetch['LANGUAGESPOKEN_NUMBER'] . "'/>";
								echo "<input type='hidden' name='LANGUAGE[]' value='" . $fetch['LANGUAGE_NUMBER'] . "' id='LANGUAGE" . $index . "_NUMBER'/></td></tr>\n";
							}
						$index++;
						$header=$index==1 ? "Language" : "";
						echo "<tr><th>$header</th><td><input type='text' class='LANGUAGE' id='LANGUAGE" . $index . "' value='' />";
						echo "<input type='hidden' name='LANGUAGE[]' value='' id='LANGUAGE" . $index . "_NUMBER'/></td></tr>\n";
						?>
						<input type='hidden' name='RELIGION_NUMBER' id='RELIGION_NUMBER' value='<?php echo $rowPatientSession['RELIGION_NUMBER']; ?>'/>
						<input type='hidden' name='ETHNIC_NUMBER' id='ETHNIC_NUMBER' value='<?php echo $rowPatientSession['ETHNIC_NUMBER']; ?>'/>
						<input type='hidden' name='status' value='update'/>
						<tr><td><input type='submit' value='Save'/></td>
						<td><input type='button' value='Cancel' onclick='back()'/></td></tr>
						<?php
					}
					echo"
					</form>
					</table>
					";
				}
			}
			else
			{
				$temp=explode("'",$_GET['id']);
				$id=$temp[0];
				$userProfile=getParticipantDetail($conn,$id);
				echo "
					<div class='marBotBig headerTextContainer lfloat' style='width:96%'>
						<div class='bold marBotBig subtitle fMid fColor' style='padding-top:20px;'>
							<img src='images/Personal information.png' style='width:20px;'>
							{$userProfile[$rowPatientSessionHeader[5]]} 's Personal Information
						</div>
					</div>
				";
				echo "<div class='gborder lfloat' style='width:96%;'>";
				echo "<table>";
				//for($i=7;$i<10;$i++){
					echo "<tr><th>Name: </th>";
					echo "<td>" . $userProfile[$rowPatientSessionHeader[5]] .  " " . $userProfile[$rowPatientSessionHeader[6]] . "</td></tr>";
					echo "<tr><th>" . $header[9] . "</th>";
					echo "<td><a href='#' onclick='displayContactUser($id)'>Send Email</a></td></tr>";
				echo "</table>";
				echo "</div>";
				//}
			}
			?>
		</div>
	</div>
