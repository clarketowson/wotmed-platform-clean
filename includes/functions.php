<?php

function substrwords($text, $maxchar, $end='...') {
    if (strlen($text) > $maxchar || $text == '') {
        $words = preg_split('/\s/', $text);      
        $output = '';
        $i      = 0;
        while (1) {
            $length = strlen($output)+strlen($words[$i]);
            if ($length > $maxchar) {
                break;
            } 
            else {
                $output .= " " . $words[$i];
                ++$i;
            }
        }
        $output .= $end;
    } 
    else {
        $output = $text;
    }
    return $output;
}


//debugging
function msgBox($msg){
	echo "<script>alert((\"$msg\"));</script>";
}
function redirect($page){
	echo "<script language='javascript'> 
			window.location = '$page'
		</script>";
}
$path="";
//Takes a password and returns the salted hash
//$password - the password to hash
//returns - the hash of the password (128 hex characters)
function HashPassword($password){
    $salt = bin2hex(mcrypt_create_iv(32, MCRYPT_DEV_URANDOM)); //get 256 random bits in hex
    $hash = hash("sha256", $salt . $password); //prepend the salt, then hash
    //store the salt and hash in the same string, so only 1 DB column is needed
    $final = $salt . $hash;
    return $final;
}

//Validates a password
//returns true if hash is the correct hash for that password
//$hash - the hash created by HashPassword (stored in your DB)
//$password - the password to verify
//returns - true if the password is valid, false otherwise.
function ValidatePassword($password, $correctHash){
    $salt = substr($correctHash, 0, 64); //get the salt from the front of the hash
    $validHash = substr($correctHash, 64, 64); //the SHA256

    $testHash = hash("sha256", $salt . $password); //hash the password being tested
    
    //if the hashes are exactly the same, the password is valid
    return $testHash === $validHash;
} 
function login($conn,$email,$password,$fbLogin,$path){
	$query="SELECT pa.PASSWORD,pa.PARTICIPANT_NUMBER, pr.PRACTITIONER_NUMBER FROM PARTICIPANT pa LEFT JOIN PRACTITIONER pr ON (pa.PARTICIPANT_NUMBER=pr.PARTICIPANT_NUMBER) WHERE EMAILADDRESS='$email'";
	$result=mysqli_query($conn,$query);
	if(mysqli_num_rows($result)==0){
		return "User not registered";
	}
	else{
		$row=mysqli_fetch_array($result);
		if(ValidatePassword($password, $row[0]) || $fbLogin=="1"){
			$_SESSION['id']=$row[1];
			$_SESSION['practitioner_id']=$row[2];
			setCurrency($conn,$path);
			return "";
		}
		else{
			return "Incorrect Login";
		}
	}
}
function setCurrency($conn,$path){
	$data=getParticipantDetail($conn,$_SESSION['id']);
	$items=readFromXML($path);
	if(!is_array($items)) echo "null";
	if($data['COUNTRY_NUMBER']!=null){
		$query="SELECT CURRENCY_CODE FROM COUNTRY WHERE COUNTRY_NUMBER = '" . $data['COUNTRY_NUMBER'] ."'";
		$localCur=mysqli_query($conn,$query);
		if(mysqli_num_rows($localCur)!=0){
			$curCurrency=mysqli_fetch_array($localCur);
			
			setcookie("Currency", $curCurrency[0], time()+36000000,"/");
		
			
			foreach( $items as $item){
				if($item[0]=="AUD"){
					$curValue=$item[3];
					setcookie("CurrencyAUD", $curValue, time()+36000000,"/");
				}
				if($item[0]==$curCurrency[0]){
					$curValue=$item[2];
					setcookie("CurrencyUSD", $curValue, time()+36000000,"/");
				}
			}
		}else{
		}
	}
	else{
		setcookie("Currency", "AUD", time()+36000000,"/");
	
		foreach( $items as $item){
			if($item[0]=="AUD"){
				$curValue=$item[3];
				setcookie("CurrencyAUD", $curValue, time()+36000000,"/");
			}
			if($item[0]=="AUD"){
				$curValue=$item[2];
				setcookie("CurrencyUSD", $curValue, time()+36000000,"/");
			}
		}
	}
}
function sendEmailVisitor($email,$subject,$body){
	$to      = $email;
	$message = "
	<html>
	<head>
		<title>$subject</title>
	<head>
	<body>
		<div>
			$body
		<div>
	</body>
	</html>
	";
	
	$headers = "From: nvr@.wotmed.com\r\n";
	$headers .= "MIME-Version: 1.0\r\n";
	$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

	mail($to, $subject, $message, $headers);
}

function sendEmailContact($email,$subject,$body,$sender){
	$to      = $email;
	$message = "
	<html>
	<head>
		<title>$subject</title>
	<head>
	<body>
		<div>
			$body
		<div>
	</body>
	</html>
	";
	
	$headers = "From: contact@.wotmed.com\r\n";
	$headers .= "Reply-To: " . $sender . "\r\n";
	$headers .= "MIME-Version: 1.0\r\n";
	$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

	mail($to, $subject, $message, $headers);
}

function sendEmail($email,$subject,$image,$body){
	$to      = $email;
	$message = "
	<html>
	<head>
		<title>$subject</title>
	<head>
	<body>
		<div style='width:622px; margin:20px;'>
			<div id='header'>
				<img src='$image' alt='' width='618' height='258'>
			</div>
			<div id='body' style='margin-top:30px;'>
				$body
			</div>
			<div id='footer' style='width:622px;float:left;height:25px;margin-top:50px;'>
				<a href='http://platform.wotmed.com/'>
					<div style='float:left;'><img src='http://platform.wotmed.com/images/email/LogoEmail.jpg' alt='' width='100' height='25'></div>
					<div style='height:25px;float:left;padding:4px;'>Make Wotmed your home</div>
				</a>
			</div>
			<div style='width='100%'>
			Wotmed is a medical network that connects patients with medical, dental and other specialist health professionals globally
			</div>
	</body>
	</html>
	";
	
	$headers = "From: noreply@.wotmed.com\r\n";
	$headers .= "MIME-Version: 1.0\r\n";
	$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

	mail($to, $subject, $message, $headers);
}
function welcomePatient($email){
	$subject="Welcome to Wotmed";
	$body="
	<div id='left' style='width:63%; float:left;'>
				<table>
					<tr>
						<td>
							<b>Here are 3 things you should know to get you started</b>
						</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td>
							You can establish relationships with Practitioners anywhere in the world using this platform
						</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td>
							You will soon be able to book flights, accommodation and car hire right here on Wotmed
						</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td>
							You can save a lot of money by becoming a medical tourist and flying overseas to visit doctors, dentists and other health professionals
						</td>
					</tr>
				</table>
			</div>
			<div id='right' style='width:33%;float:left;background:#F0F0F0;margin:-10px;padding:10px;'>
				<table>
					<tr>
						<td>
							<b>My Info</b>
						</td>
					</tr>
					<tr>
						<td>
							Wotmed ID and Email Address:
						</td>
					</tr>
					<tr>
						<td>
							$email
						</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td>
							<a href='http://platform.wotmed.com/'>
								My Account
							</a>
						</td>
					</tr>
					<tr>
						<td>Keep your account up to date</td>
					</tr>
					
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td>
							<a href='http://platform.wotmed.com/terms'>
								Wotmed Terms of Service
							</a>
						</td>
					</tr>
					<tr>
						<td>
							<a href='http://.platform.wotmed.com/privacy'>
								Wotmed Privacy Policy
							</a>
						</td>
					</tr>
				</table>
			</div>
	";
	$image="http://platform.wotmed.com/images/email/WotmedWelcomePatient.jpg";
	sendEmail($email,$subject,$image,$body);
}

function welcomePractitioner($email){
	$subject="Welcome to Wotmed";
	$body="
	<div id='left' style='width:63%; float:left;'>
				<table>
					<tr>
						<td>
							<b>Here are 3 things you should know to get you started</b>
						</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td>
							You can establish relationships with Patients anywhere in the world using this platform
						</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td>
							Patients will soon be able to book flights, accommodation and car hire right here on Wotmed
						</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td>
							Patients can save a lot of money by becoming a medical tourist and flying overseas to your country for surgery
						</td>
					</tr>
				</table>
			</div>
			<div id='right' style='width:33%;float:left;background:#F0F0F0;margin:-10px;padding:10px;'>
				<table>
					<tr>
						<td>
							<b>My Info</b>
						</td>
					</tr>
					<tr>
						<td>
							Wotmed ID and Email Address:
						</td>
					</tr>
					<tr>
						<td>
							$email
						</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td>
							<a href='http://platform.wotmed.com/'>
								My Account
							</a>
						</td>
					</tr>
					<tr>
						<td>Keep your account up to date</td>
					</tr>
					
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td>
							<a href='http://platform.wotmed.com/terms'>
								Wotmed Terms of Service
							</a>
						</td>
					</tr>
					<tr>
						<td>
							<a href='http://platform.wotmed.com/privacy'>
								Wotmed Privacy Policy
							</a>
						</td>
					</tr>
				</table>
			</div>
	";
	$image="http://platform.wotmed.com/images/email/WotmedWelcomePractitioner.jpg";
	sendEmail($email,$subject,$image,$body);
}

function register($conn,$firstName,$lastName,$email,$password,$gender,$genderB,$birthDate){
	$query="SELECT COUNT(*) FROM PARTICIPANT WHERE EMAILADDRESS ='" . $email . "'";
	$res=mysqli_query($conn,$query);
	$result=mysqli_fetch_array($res);
	if($result[0]==0){
		switch($gender){
			case 1:
				$gender='F';
				break;
			case 2:
				$gender='M';
				break;
			case 3:
				$gender='T';
				break;
		}
		switch($genderB){
			case 1:
				$genderB='F';
				break;
			case 2:
				$genderB='M';
				break;
		}
		$password=HashPassword($password);
		$verCode = bin2hex(mcrypt_create_iv(32, MCRYPT_DEV_URANDOM));
		$verCode .= bin2hex(mcrypt_create_iv(32, MCRYPT_DEV_URANDOM));
		$query="INSERT INTO PARTICIPANT (  `PASSWORD`, `FIRSTNAME`, `SURNAME`, `DOB`, `GENDER`, `GENDERATBIRTH`, `EMAILADDRESS`, VERIFICATIONCODE, DELETED) VALUES (  '$password', '$firstName', '$lastName', '$birthDate', '" . $gender . "', '" . $genderB . "', '$email','$verCode',0);";
		mysqli_query($conn,$query);
		//pre populate condition table
		$query="INSERT INTO CONDITIONCARD (PARTICIPANT_NUMBER) SELECT PARTICIPANT_NUMBER FROM PARTICIPANT WHERE EMAILADDRESS='$email';";
		mysqli_query($conn,$query);
		
		$subject="Please verify your email address";
		$body="Please confirm your email by clicking this link <a href='http://platform.wotmed.com/verify.php?email=$email&verificationCode=$verCode'>The Link</a>";
		$image="http://platform.wotmed.com/images/email/WotmedVerifyEmail.jpg";
		sendEmail($email,$subject,$image,$body);
		
		return "success";
	}else{
		return "failed";
		msgBox('This email has already registered');
	}
}

function registerApi($conn,$firstName,$lastName,$email,$password,$gender,$birthDate){
	switch($gender){
		case 1:
			$gender='F';
			break;
		case 2:
			$gender='M';
			break;
		case 3:
			$gender='T';
			break;
	}
	$password=HashPassword($password);
	$verCode = bin2hex(mcrypt_create_iv(32, MCRYPT_DEV_URANDOM));
	$verCode .= bin2hex(mcrypt_create_iv(32, MCRYPT_DEV_URANDOM));
	$query="INSERT INTO PARTICIPANT (  `PASSWORD`, `FIRSTNAME`, `SURNAME`, `DOB`, `GENDER`, `EMAILADDRESS`, VERIFICATIONCODE) VALUES (  '$password', '$firstName', '$lastName', '$birthDate', '" . $gender . "', '$email','$verCode');";
	mysqli_query($conn,$query);
	
	$subject="Please verify your email address";
	$body="Please confirm your email by clicking this link <a href='http://platform.wotmed.com/verify.php?email=$email&verificationCode=$verCode'>The Link</a>";
	$image="http://platform.wotmed.com/images/email/WotmedVerifyEmail.jpg";
	sendEmail($email,$subject,$image,$body);
}

function shareEmail($to,$photoId, $name){


	$subject="Wotmed patient $name has shared a photograph with you";
	$body="
		You are receiving this email because Wotmed patient $name wants to share a photograph with you.<br><br>   

		Please ensure that you treat this photograph with a high degree of privacy as $name has placed a high degree of trust in you by sharing this photograph with you.<br><br>

		Please keep in mind that photographs shared by Wotmed patients may be of a very private nature and may not be suitable for viewing in the workplace.<br><br>

		Please click <a href='http://platform.wotmed.com/gallery/photoPreview.php?id=" . $photoId . "'>here</a> to see the photograph that $name has shared with you.<br><br>
	";
	$image="http://platform.wotmed.com/images/email/WotmedSharePhoto.png";
	sendEmail($to,$subject,$image,$body);
}
function shareEmailVideo($to,$videoId, $name){
	$subject="Wotmed patient $name has shared a video with you";
	$body="
		You are receiving this email because Wotmed patient $name wants to share a video with you.<br><br>   

		Please ensure that you treat this video with a high degree of privacy as $name has placed a high degree of trust in you by sharing this video with you.<br><br>

		Please keep in mind that videos shared by Wotmed patients may be of a very private nature and may not be suitable for viewing in the workplace.<br><br>

		Please click <a href='http://platform.wotmed.com/gallery/videoPreview.php?fileName=" . $videoId . "'>here</a> to see the video that $name has shared with you.<br><br>
	";
	$image="http://platform.wotmed.com/images/email/WotmedShareVideo.png";
	sendEmail($to,$subject,$image,$body);
}
function shareCondReport($to,$link){
    
    $subject="Wotmed condition report sharing";
    $body="Hi, <br>
    <p>
        You just received a Wotmed Condition Report <BR>
        Please check it <a href=$link>here</a>
    </p>";
    $image="http://platform.wotmed.com/images/email/WotmedVerifyEmail.jpg";
    //echo $to . "<br>" . $body;
    sendEmail($to,$subject,$image,$body);
}
function reminderHealthProfile($to){
	$subject = "platform.wotmed.com";
	$message = "
	<html>
	<head>
	  <title>Wotmed Reminder</title>
	  <meta name='description' content='free website template' />
	  <meta name='keywords' content='enter your keywords here' />
	  <meta http-equiv='content-type' content='text/html; charset=utf-8' />
	  <link rel='stylesheet' type='text/css' href='http://platform.wotmed.com/images/email/estyle.css' />
	</head>
	<body style='font: normal 80% Arial, Helvetica, sans-serif; background: #CBCBCB url(http://platform.wotmed.com/images/email/background.jpg) repeat-x; color: #000;'>

		<div id='slideshow' style='width: 920px; height: 35px; margin: 0 auto; background: #FFF;'><img src='http://platform.wotmed.com/images/email/LogoEmail.jpg' alt='' width='120' height='35ssss' /> </div><!--close slidesho-->		  

		<div id='header' style='width: 920px; height: 70px; background: transparent url(http://platform.wotmed.com/images/email/header.jpg) no-repeat;'>
		  <div id='banner' style='width: 320px; position: relative; text-align: center; height: 40px; padding: 15px 0 0 0; background: transparent;'>
			<div id='welcome' style='width: 880px; float: left; height: 40px; margin: 0 auto; padding-left: 20px; background: transparent;'>
			<h1 style='font: normal 250% Arial, Helvetica, sans-serif; letter-spacing: -3px; color: #FFF;'>Reminder</h1>
			</div><!--close welcome-->
		  </div><!--close banner-->
		</div><!--close header-->

		<div id='site_content' style='width: 900px; overflow: hidden; margin: 0 auto; padding: 0 0px 0 20px; background: #FFF;'><!--close sidebar_container-->		 
		 
		  <div id='content' style='width: 640px; padding-left: 10px; margin-bottom: 20px; float: left;'>
			<div class='content_item' style='width: 280px; padding: 5px; margin-right: 10px; float: left;'>
			  <p style='padding: 0 0 20px 0; line-height: 1.7em; font-size: 100%'>Hi, </p>
			  <p style='padding: 0 0 20px 0; line-height: 1.7em; font-size: 100%'>We find that you need to update your health profile. Please login into <a href='platform.wotmed.com/'>platform.wotmed.com</a></p>
			  <p style='padding: 0 0 20px 0; line-height: 1.7em; font-size: 100%'>&nbsp;</p>
			  <p style='padding: 0 0 20px 0; line-height: 1.7em; font-size: 100%'>Thank You</p>
			  <p style='padding: 0 0 20px 0; line-height: 1.7em; font-size: 100%'>&nbsp;</p>
			  <p style='padding: 0 0 20px 0; line-height: 1.7em; font-size: 100%'>The Wotmed Team</p>
			  <p style='padding: 0 0 20px 0; line-height: 1.7em; font-size: 100%'><br style='clear:both'/>
			  </p>
			  <!--close content_container--><!--close content_container-->			  
			</div><!--close content_item-->
		  </div><!--close content-->   
		</div><!--close site_content--> 

		<div id='content_grey' style='width: 920px; height: 150px; text-align: center; background: transparent url(http://platform.wotmed.com/images/email/content_grey.jpg) repeat; color: #FFF;'><!--close content_grey_container_box--><!--close content_grey_container_box--><!--close content_grey_container_box1-->
		<br style='clear:both'/>
		</div><!--close content_grey-->   

		</div><!--close main-->

		<div id='footer' style='width: 920px; height: 20px; padding-top: 20px; text-align: center;  background: transparent; color: #1D1D1D;'>
		  <a href='http://www.wotmed.com'>Copyright (c) 2015 Wotmed.com</a><a href='http://www.wotmed.com'></a>
		</div><!--close footer-->  
	</body>
	</html>";
	
	$headers = "From: noreply@.wotmed.com\r\n";
	$headers .= "MIME-Version: 1.0\r\n";
	$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

	mail($to, $subject, $message, $headers);
}
function broadcastMsg($to,$msg){
	
	$subject="Wotmed broadcast message";
	$body="Hi, <br>
		  <p>{$msg}</p>";
	$image="http://platform.wotmed.com/images/email/BroadcastMessage.jpg";
	sendEmail($to,$subject,$image,$body);
}
function getParticipantDetail($conn,$id){
	$query="SELECT PARTICIPANT_NUMBER, ETHNIC, e.ETHNIC_NUMBER, r.RELIGION_NUMBER, RELIGION, USERNAME, PASSWORD, FIRSTNAME, SURNAME, DOB, GENDER, GENDERATBIRTH, EMAILADDRESS, BANKNAME, BANKBRANCH, BSBNUMBER, ACCOUNTNAME, ACCOUNTNUMBER, INTERNETPAYMENTMETHOD, PROFILEPHOTO, GOOGLEMAPADDRESS, HOMEPHONENUMBER, MOBILEPHONENUMBER, WORKPHONENUMBER, PREFERREDPHONENUMBER, SKYPEUSERNAME, HEALTHINSURANCEPROVIDER_NAME, h.HEALTHINSURANCEPROVIDER_NUMBER, ADDRESS, SUBURB, POSTCODE, STATE, STEP, CITY, VERIFICATIONCODE, COUNTRYNAME, c.COUNTRY_NUMBER,COUNTRYFLAG,COUNTRYCOATOFARMS,WIKIVR,p.VIDEO,p.VIDEOTRANSCRIPT,p.VIDEOLENGTH FROM PARTICIPANT p LEFT JOIN RELIGION r ON (p.RELIGION_NUMBER=r.RELIGION_NUMBER) LEFT JOIN ETHNIC e ON (p.ETHNIC_NUMBER=e.ETHNIC_NUMBER) LEFT JOIN COUNTRY c ON (p.COUNTRY_NUMBER=c.COUNTRY_NUMBER) LEFT JOIN HEALTHINSURANCEPROVIDER h ON (h.HEALTHINSURANCEPROVIDER_NUMBER=p.HEALTHINSURANCEPROVIDER_NUMBER) WHERE PARTICIPANT_NUMBER='$id'";
	$result=mysqli_query($conn,$query);
	if(mysqli_num_rows($result)!=0){
		$row=mysqli_fetch_array($result);
		$query="SELECT ls.LANGUAGESPOKEN_NUMBER,ls.LANGUAGE_NUMBER,ml.LANGUAGE FROM LANGUAGESPOKEN ls INNER JOIN MASTERLANGUAGE ml ON(ml.LANGUAGE_NUMBER=ls.LANGUAGE_NUMBER) WHERE PARTICIPANT_NUMBER='$id'";
		$result=mysqli_query($conn,$query);
		if(mysqli_num_rows($result)!=0)
			$row['LANGUAGES']=$result;
		else
			$row['LANGUAGES']=null;
		return $row;
	}
	else{
		return null;
	}
}
function getInsuranceList($conn){
	$query="SELECT HEALTHINSURANCEPROVIDER_NUMBER,HEALTHINSURANCEPROVIDER_NAME FROM HEALTHINSURANCEPROVIDER ORDER BY HEALTHINSURANCEPROVIDER_NAME";
	$result=mysqli_query($conn,$query);
	return $result;
}
function getCountryList($conn){
	$query="SELECT COUNTRY_NUMBER,COUNTRYNAME FROM COUNTRY ORDER BY COUNTRYNAME";
	$result=mysqli_query($conn,$query);
	return $result;
}

function loginPractitioner($conn,$id,$isFacilitator){
	$participantDetail=getParticipantDetail($conn,$id);
	$query="SELECT PRACTITIONER_NUMBER FROM PRACTITIONER WHERE PARTICIPANT_NUMBER=" . $id;
	$result=mysqli_query($conn,$query);
	if(mysqli_num_rows($result)==0){
		$query="INSERT INTO PRACTITIONER(PARTICIPANT_NUMBER,PRACTITIONER_BUSINESSNAME,ISFACILITATOR) VALUES('$id','" . $participantDetail['FIRSTNAME'] . " " . $participantDetail['SURNAME'] . "','$isFacilitator')";
		$result=mysqli_query($conn,$query);
		
		$query="SELECT PRACTITIONER_NUMBER FROM PRACTITIONER WHERE PARTICIPANT_NUMBER='$id'";
		$result=mysqli_query($conn,$query);
		$row=mysqli_fetch_array($result);
		$_SESSION['practitioner_id']=$row[0];
	}else{
		$row=mysqli_fetch_array($result);
		$_SESSION['practitioner_id']=$row[0];
	}/*
	?>
	<script language="javascript"> 
		<?php echo "window.location = 'practitioner_profile.php'";?>
	</script> <?php
	?><?php*/
}
function getPractitionerDetail($conn,$participantId){
	$query="SELECT pr.PRACTITIONER_NUMBER, SPECIALITY_NUMBER, PRACTITIONER_BUSINESSNUMBER, PRACTITIONER_BUSINESSNAME, PRACTITIONER_BUSINESSLOGO, PRACTITIONER_BUSINESSADDRESS, PRACTITIONER_BUSINESSSUBURB, PRACTITIONER_BUSINESSPOSTCODE, PRACTITIONER_BUSINESSCITY, PRACTITIONER_BUSINESSGOOGLEMAPADDRESS,PRACTITIONER_BUSINESSPROFESSIONALSTATEMENT, AVERAGERECOMMENDATION, AVERAGEBEDSIDEMANNER, AVERAGEWAITTIME, PATIENTOVERALLREVIEW, EDUCATION, QUALIFICATIONS, BOARDCERTIFICATIONS, PRACTITIONER_PROFESSIONALMEMBERSHIP, AWARDSANDPUBLICATIONS, HOSPITALAFFILIATION, PRACTITIONER_BUSINESSSTATE, YEARSOFPRACTICE, MARITALSTATUS, WEBSITEURL, CLINICALINTEREST, EMERGENCYPHONENUMBER, PUBLISHEDARTICLES, SCRAPBOOK, PROFILEHITS,REGISTERDATE,pa.PARTICIPANT_NUMBER, ETHNIC_NUMBER, RELIGION_NUMBER, USERNAME, PASSWORD, FIRSTNAME, SURNAME, DOB, GENDER, EMAILADDRESS, BANKNAME, BANKBRANCH, BSBNUMBER, ACCOUNTNAME, ACCOUNTNUMBER, INTERNETPAYMENTMETHOD, PROFILEPHOTO, GOOGLEMAPADDRESS, HOMEPHONENUMBER, MOBILEPHONENUMBER, WORKPHONENUMBER, PREFERREDPHONENUMBER, SKYPEUSERNAME, HEALTHINSURANCEPROVIDER_NUMBER, ADDRESS, SUBURB, POSTCODE, STATE, CITY, VERIFICATIONCODE, STEP, pr.COUNTRY_NUMBER as B_COUNTRY_NUMBER,COUNTRYNAME,COUNTRYFLAG,COUNTRYCOATOFARMS,WIKIVR,c.FREEDOM,c.ECONOMIC,c.PRESS,c.DEMOCRACY,c.HDI,TRUSTED,c.DIPLOMATIC,pr.AUDIO,pr.AUDIOTRANSCRIPT,pr.VIDEO,pr.VIDEOTRANSCRIPT,pr.VIEWCOUNT,pr.VIDEOLENGTH,pr.C_TRAINED,pr.C_DEGREE, ISFACILITATOR FROM PRACTITIONER pr LEFT JOIN COUNTRY c ON(pr.COUNTRY_NUMBER=c.COUNTRY_NUMBER), PARTICIPANT pa WHERE pr.PARTICIPANT_NUMBER='$participantId' AND pr.PARTICIPANT_NUMBER=pa.PARTICIPANT_NUMBER";
	$result=mysqli_query($conn,$query);
	if(mysqli_num_rows($result)!=0){
		$row=mysqli_fetch_array($result);
		return $row;
	}
	else{
		return null;
	}
}
function getPractitionerDetailByPractitionerId($conn,$practitionerId){
	$query="SELECT pr.PRACTITIONER_NUMBER, SPECIALITY_NUMBER, PRACTITIONER_BUSINESSNUMBER, PRACTITIONER_BUSINESSNAME, PRACTITIONER_BUSINESSLOGO, PRACTITIONER_BUSINESSADDRESS, PRACTITIONER_BUSINESSSUBURB, PRACTITIONER_BUSINESSPOSTCODE, PRACTITIONER_BUSINESSPROFESSIONALSTATEMENT, AVERAGERECOMMENDATION, AVERAGEBEDSIDEMANNER, AVERAGEWAITTIME, PATIENTOVERALLREVIEW, EDUCATION, QUALIFICATIONS, BOARDCERTIFICATIONS, PRACTITIONER_PROFESSIONALMEMBERSHIP, AWARDSANDPUBLICATIONS, HOSPITALAFFILIATION, PRACTITIONER_BUSINESSSTATE, YEARSOFPRACTICE, MARITALSTATUS, WEBSITEURL, CLINICALINTEREST, PRACTITIONER_BUSINESSCITY,EMERGENCYPHONENUMBER, PUBLISHEDARTICLES, SCRAPBOOK, PROFILEHITS, REGISTERDATE,pa.PARTICIPANT_NUMBER, ETHNIC_NUMBER, RELIGION_NUMBER, USERNAME, PASSWORD, FIRSTNAME, SURNAME, DOB, GENDER, EMAILADDRESS, BANKNAME, BANKBRANCH, BSBNUMBER, ACCOUNTNAME, ACCOUNTNUMBER, INTERNETPAYMENTMETHOD, PROFILEPHOTO, GOOGLEMAPADDRESS, HOMEPHONENUMBER, MOBILEPHONENUMBER, WORKPHONENUMBER, PREFERREDPHONENUMBER, SKYPEUSERNAME, HEALTHINSURANCEPROVIDER_NUMBER, ADDRESS, SUBURB, POSTCODE, STATE, CITY, VERIFICATIONCODE, pr.COUNTRY_NUMBER,COUNTRYFLAG,FREEDOM,ECONOMIC,PRESS,DEMOCRACY,HDI, ISFACILITATOR FROM PRACTITIONER pr LEFT JOIN COUNTRY c ON(pr.COUNTRY_NUMBER=c.COUNTRY_NUMBER), PARTICIPANT pa WHERE pr.PRACTITIONER_NUMBER='$practitionerId' AND pr.PARTICIPANT_NUMBER=pa.PARTICIPANT_NUMBER";
	$result=mysqli_query($conn,$query);
	if(mysqli_num_rows($result)!=0){
		$row=mysqli_fetch_array($result);
		return $row;
	}
	else{
		return null;
	}
}
function age_from_dob($dob) {
	$dob = strtotime($dob);
	$y = date('Y', $dob);
	if (($m = (date('m') - date('m', $dob))) < 0) {
		$y++;
	} elseif ($m == 0 && date('d') - date('d', $dob) < 0) {
		$y++;
	}
	return date('Y') - $y;
}
function getCheckup($conn,$dateOfBirth,$sex){
	$age=age_from_dob($dateOfBirth);
	$query="SELECT CHECKUP_NUMBER, CHECKUP_NAME, FREQUENCY, AGEFROM, AGETO, CHECKUPGENDER, CHECKUPREASON FROM CHECKUP WHERE (AGEFROM<='$age' and AGETO>='$age') and (CHECKUPGENDER='$sex' or CHECKUPGENDER='A')";
	$result=mysqli_query($conn,$query);
	if(mysqli_num_rows($result)!=0){
		//while ($row=mysqli_fetch_array($result)){ 
		//	echo "<tr><td>" . $row['CHECKUP_NAME'] . "</td></tr>";
		//}
		return $result;
	}
	else{
		return null;
	}
}
function getHealtProfile($conn,$id){
	$query="SELECT * FROM (SELECT * FROM HEALTHPROFILE WHERE PARTICIPANT_NUMBER=$id order by UPDATETIME DESC) AS A group by CHECKUP_NUMBER";
	$result=mysqli_query($conn,$query);
	if(mysqli_num_rows($result)!=0){
		 //while ($row=mysqli_fetch_array($result)){ 
			//echo "<tr><td>" . $row['CHECKUP_NUMBER'] . "</td></tr>";
		//}
		return $result;
	}
	else{
		return null;
	}
}

function checkCheckupExpiry($updateTime,$frequency)
{
	$today = getdate();
	$currentDate=$today['yday']-1+1;
	$currentYear=$today['year']-1+1;
	//$test = date_create(date('Y-m-d', strtotime($healthRow['UPDATETIME'])));  -- for timestamp data type
	//$lastTest = getdate(date_timestamp_get($test));
	if (!function_exists('date_parse_from_format')) {
		function date_parse_from_format($format, $date) {
			$i=0;
			$pos=0;
			$output=array();
			while ($i< strlen($format)) {
			  $pat = substr($format, $i, 1);
			  $i++;
			  switch ($pat) {
				case 'd': //    Day of the month, 2 digits with leading zeros    01 to 31
				  $output['day'] = substr($date, $pos, 2);
				  $pos+=2;
				break;
				case 'D': // A textual representation of a day: three letters    Mon through Sun
				  //TODO
				break;
				case 'j': //    Day of the month without leading zeros    1 to 31
				  $output['day'] = substr($date, $pos, 2);
				  if (!is_numeric($output['day']) || ($output['day']>31)) {
					$output['day'] = substr($date, $pos, 1);
					$pos--;
				  }
				  $pos+=2;
				break;
				case 'm': //    Numeric representation of a month: with leading zeros    01 through 12
				  $output['month'] = (int)substr($date, $pos, 2);
				  $pos+=2;
				break;
				case 'n': //    Numeric representation of a month: without leading zeros    1 through 12
				  $output['month'] = substr($date, $pos, 2);
				  if (!is_numeric($output['month']) || ($output['month']>12)) {
					$output['month'] = substr($date, $pos, 1);
					$pos--;
				  }
				  $pos+=2;
				break;
				case 'Y': //    A full numeric representation of a year: 4 digits    Examples: 1999 or 2003
				  $output['year'] = (int)substr($date, $pos, 4);
				  $pos+=4;
				break;
				case 'y': //    A two digit representation of a year    Examples: 99 or 03
				  $output['year'] = (int)substr($date, $pos, 2);
				  $pos+=2;
				break;
				case 'g': //    12-hour format of an hour without leading zeros    1 through 12
				  $output['hour'] = substr($date, $pos, 2);
				  if (!is_numeric($output['day']) || ($output['hour']>12)) {
					$output['hour'] = substr($date, $pos, 1);
					$pos--;
				  }
				  $pos+=2;
				break;
				case 'G': //    24-hour format of an hour without leading zeros    0 through 23
				  $output['hour'] = substr($date, $pos, 2);
				  if (!is_numeric($output['day']) || ($output['hour']>23)) {
					$output['hour'] = substr($date, $pos, 1);
					$pos--;
				  }
				  $pos+=2;
				break;
				case 'h': //    12-hour format of an hour with leading zeros    01 through 12
				  $output['hour'] = (int)substr($date, $pos, 2);
				  $pos+=2;
				break;
				case 'H': //    24-hour format of an hour with leading zeros    00 through 23
				  $output['hour'] = (int)substr($date, $pos, 2);
				  $pos+=2;
				break;
				case 'i': //    Minutes with leading zeros    00 to 59
				  $output['minute'] = (int)substr($date, $pos, 2);
				  $pos+=2;
				break;
				case 's': //    Seconds: with leading zeros    00 through 59
				  $output['second'] = (int)substr($date, $pos, 2);
				  $pos+=2;
				break;
				case 'l': // (lowercase 'L')    A full textual representation of the day of the week    Sunday through Saturday
				case 'N': //    ISO-8601 numeric representation of the day of the week (added in PHP 5.1.0)    1 (for Monday) through 7 (for Sunday)
				case 'S': //    English ordinal suffix for the day of the month: 2 characters    st: nd: rd or th. Works well with j
				case 'w': //    Numeric representation of the day of the week    0 (for Sunday) through 6 (for Saturday)
				case 'z': //    The day of the year (starting from 0)    0 through 365
				case 'W': //    ISO-8601 week number of year: weeks starting on Monday (added in PHP 4.1.0)    Example: 42 (the 42nd week in the year)
				case 'F': //    A full textual representation of a month: such as January or March    January through December
				case 'u': //    Microseconds (added in PHP 5.2.2)    Example: 654321
				case 't': //    Number of days in the given month    28 through 31
				case 'L': //    Whether it's a leap year    1 if it is a leap year: 0 otherwise.
				case 'o': //    ISO-8601 year number. This has the same value as Y: except that if the ISO week number (W) belongs to the previous or next year: that year is used instead. (added in PHP 5.1.0)    Examples: 1999 or 2003
				case 'e': //    Timezone identifier (added in PHP 5.1.0)    Examples: UTC: GMT: Atlantic/Azores
				case 'I': // (capital i)    Whether or not the date is in daylight saving time    1 if Daylight Saving Time: 0 otherwise.
				case 'O': //    Difference to Greenwich time (GMT) in hours    Example: +0200
				case 'P': //    Difference to Greenwich time (GMT) with colon between hours and minutes (added in PHP 5.1.3)    Example: +02:00
				case 'T': //    Timezone abbreviation    Examples: EST: MDT ...
				case 'Z': //    Timezone offset in seconds. The offset for timezones west of UTC is always negative: and for those east of UTC is always positive.    -43200 through 50400
				case 'a': //    Lowercase Ante meridiem and Post meridiem    am or pm
				case 'A': //    Uppercase Ante meridiem and Post meridiem    AM or PM
				case 'B': //    Swatch Internet time    000 through 999
				case 'M': //    A short textual representation of a month: three letters    Jan through Dec
				default:
				  $pos++;
			  }
			}
		return  $output;
		  }
	}
	$date = date_parse_from_format('Y-m-d', $updateTime);
	$timestamp = mktime(0, 0, 0, $date['month'], (int)$date['day'], (int)$date['year']);
	$lastTest = getdate($timestamp);
	$testDate=$lastTest['yday']-1+1;
	$testYear=$lastTest['year']-1+1;
	$lenghtOfTest=$currentDate - $testDate + (($currentYear-$testYear)*365);
	if($lenghtOfTest>$frequency)
	{
		return true;
	}
}
function getRelationship($conn,$isId,$ofId)
{
	$query="SELECT ISPARTICIPANT, OFPARTICIPANT, RELATIONSHIP FROM RELATIONSHIP WHERE ISPARTICIPANT=$isId AND OFPARTICIPANT=$ofId";
	$result=mysqli_query($conn,$query);
	if(mysqli_num_rows($result)!=0){
		$row=mysqli_fetch_array($result);
		return $row[2];
	}
	else{
		return null;
	}
}
function getFamily($conn,$id)
{
	$query="SELECT ISPARTICIPANT, OFPARTICIPANT, RELATIONSHIP FROM RELATIONSHIP WHERE ISPARTICIPANT=$id";
	$result=mysqli_query($conn,$query);
	if(mysqli_num_rows($result)!=0){
		return $result;
	}
	else{
		return null;
	}
}
function getServices($conn,$id){
	$query="SELECT PRACTITIONER_NUMBER,MASTERSERVICE_NUMBER,PRICE,SERVICES_NUMBER FROM SERVICES WHERE PRACTITIONER_NUMBER='$id'";
	$result=mysqli_query($conn,$query);
	if(mysqli_num_rows($result)!=0){
		return $result;
	}
	else{
		return null;
	}
}
function countServices($conn,$id){
	$query="SELECT COUNT(*) FROM SERVICES WHERE PRACTITIONER_NUMBER='$id'";
	$result=mysqli_query($conn,$query);
	if(mysqli_num_rows($result)!=0){
		return $result;
	}
	else{
		return null;
	}
}
function getRelation($relation,$gender)
{
	if($relation=="Grand Parent")
	{
		if($gender=="M")
			$relation="Grand Father";
		else
			$relation="Grand Mother";
	}
	if($relation=="Parent")
	{
		if($gender=="M")
			$relation="Father";
		else
			$relation="Mother";
	}
	if($relation=="Grand Child")
	{
		if($gender=="M")
			$relation="Grand Son";
		else
			$relation="Grand Daughter";
	}
	if($relation=="Child")
	{
		if($gender=="M")
			$relation="Son";
		else
			$relation="Daughter";
	}
	if($relation=="Sibling")
	{
		if($gender=="M")
			$relation="Brother";
		else
			$relation="Sister";
	}
	return $relation;
}
function getDoctor($conn,$id)
{
	$query="SELECT PRACTITIONER_NUMBER,PARTICIPANT_NUMBER,RECOMMENDED,TRUSTED FROM PATIENTPRACTITIONERRELATIONSHIP WHERE PARTICIPANT_NUMBER=$id";
	$result=mysqli_query($conn,$query);
	if(mysqli_num_rows($result)!=0){
		return $result;
	}
	else{
		return null;
	}
}
function getCheckupResultValues($conn,$checkUpId)
{
	$query="SELECT HEADER,VALUE FROM CHECKUPRESULT WHERE CHECKUP_NUMBER=$checkUpId";
	$result=mysqli_query($conn,$query);
	if(mysqli_num_rows($result)!=0){
		return $result;
	}
	else{
		return null;
	}
}
function timeToMin($time)
{
	$temp=explode(":",$time);
	return ($temp[0]*60)+$temp[1];
}
function addedTime($time,$duration)
{
	$temp=timeToMin($time)+$duration;
	if($temp%60==0)
		$result=floor(($temp/60)) . ":00";
	else
		$result=floor(($temp/60)) . ":" . ($temp%60);
	return $result;
}
function get_last_day($year, $month){

    $timestamp = strtotime("$year-$month-01");
    $number_of_days = date('t',$timestamp);
    return $number_of_days;
}
function countYear($numbOfDays,$year)
{
	$limit = 0;
	while ($numbOfDays > 0)
	{
		$temp=$year-1+1+$limit;
		$numbOfDays=$numbOfDays+date('L', strtotime($temp."-01-01"))-(365+date('L', strtotime($temp."-01-01")));
		$limit += 1;
	}
	return $limit;
}


function insertAcceptedInsurance($conn,$practitioner_id,$insuranceId)
{
	$query="INSERT INTO ACCEPTEDHEALTHINSURANCE";
	$query .= " VALUES ('" . $practitioner_id . "','$insuranceId')";
	mysqli_query($conn,$query);
	//return $query;
}

function deleteAcceptedInsurance($conn,$practitioner_id,$insuranceId)
{
	$query="DELETE FROM ACCEPTEDHEALTHINSURANCE";
	$query .= " WHERE PRACTITIONER_NUMBER='" . $practitioner_id . "' AND HEALTHINSURANCEPROVIDER_NUMBER='$insuranceId'";
	mysqli_query($conn,$query);
	//return $query;
}
function getAcceptedInsurance($conn,$practitioner_id)
{
	$query="SELECT PRACTITIONER_NUMBER,a.HEALTHINSURANCEPROVIDER_NUMBER,HEALTHINSURANCEPROVIDER_NAME FROM ACCEPTEDHEALTHINSURANCE a, HEALTHINSURANCEPROVIDER h WHERE a.HEALTHINSURANCEPROVIDER_NUMBER=h.HEALTHINSURANCEPROVIDER_NUMBER AND PRACTITIONER_NUMBER='" . $practitioner_id . "'";
	$result = mysqli_query($conn,$query);
	if(mysqli_num_rows($result)!=0){
		return $result;
	}
	else{
		return null;
	}
}
function getWishlist($conn,$id)
{
	$query="SELECT s.SERVICES_NUMBER,p.PRACTITIONER_BUSINESSNAME,m.TECHNICALPROCEDURENAME FROM WISHLIST w, PRACTITIONER p, SERVICES s, MASTERSERVICE m WHERE p.PRACTITIONER_NUMBER=s.PRACTITIONER_NUMBER AND s.SERVICES_NUMBER=w.SERVICES_NUMBER AND m.MASTERSERVICE_NUMBER=s.MASTERSERVICE_NUMBER AND w.PARTICIPANT_NUMBER='" . $id . "'";
	$result = mysqli_query($conn,$query);
	if(mysqli_num_rows($result)!=0){
		return $result;
	}
	else{
		return null;
	}
}
function getTrustApplication($conn,$id,$pending)
{
	$query="SELECT * FROM TRUSTAPPLICATION WHERE PRACTITIONER_NUMBER='" . $id . "'";
	if($pending){
		$query.=" AND (STATUS!='5' AND STATUS!='-1')";
	}
	$result = mysqli_query($conn,$query);
	if(mysqli_num_rows($result)!=0){
		return $result;
	}
	else{
		return null;
	}
}
function insertDetails($conn,$practitioner_id,$details,$type)
{
	$header=array("PRACTITIONER_NUMBER","PRACTITIONERDETAIL_ID","DETAIL_TITLE","DETAIL_SUBTITLE","DETAIL_PRICE","DETAIL_MAINBODY","DETAIL_IMAGE","DETAIL_VIDEO","DETAIL_PDF","DETAIL_TERMS","DETAIL_SOCIALNETWORK","DETAIL_PRACTICEHOUR","DETAIL_DATE","MASTERDETAILTYPE_ID");
	$query="INSERT INTO PRACTITIONERDETAIL (";
	for($i=0;$i<13;$i++)
	{
		$query.= $header[$i] . ",";
	}
	$query .= $header[$i] . ") VALUES ('" . $practitioner_id . "',NULL,";
	for($i=2;$i<13;$i++)
	{
		if(isset($details[$header[$i]]))
			$query.= "'" . mysqli_real_escape_string($conn,$details[$header[$i]]) . "', ";
		else
			$query.= "NULL, ";
	}
	$query .= "'" . $type . "');";
	mysqli_query($conn,$query);
	//return $query;
}
function updateDetails($conn,$detail_id,$details)
{
	$header=array("PRACTITIONER_NUMBER","PRACTITIONERDETAIL_ID","DETAIL_TITLE","DETAIL_SUBTITLE","DETAIL_PRICE","DETAIL_MAINBODY","DETAIL_IMAGE","DETAIL_VIDEO","DETAIL_PDF","DETAIL_TERMS","DETAIL_SOCIALNETWORK","DETAIL_PRACTICEHOUR","DETAIL_DATE","MASTERDETAILTYPE_ID");
	//$data=explode(",",$details);
	$query="UPDATE PRACTITIONERDETAIL SET ";
	for($i=2;$i<12;$i++)
	{
		if(isset($details[$header[$i]]))
			$query.= $header[$i] . "='" . mysqli_real_escape_string($conn,$details[$header[$i]]) . "', ";
		else
			$query.= $header[$i] . "=" . "NULL, ";
	}
	if(isset($details[$header[$i]]))
		$query .= $header[$i] . "='" . mysqli_real_escape_string($conn,$details[$header[$i]]) . "' WHERE PRACTITIONERDETAIL_ID=" . $detail_id;
	else
		$query .= $header[$i] . "=" . "NULL " . " WHERE PRACTITIONERDETAIL_ID=" . $detail_id;
	mysqli_query($conn,$query);
	return $query;
}
function deleteDetails($conn,$detail_id)
{
	$query="DELETE FROM PRACTITIONERDETAIL WHERE PRACTITIONERDETAIL_ID=" . $detail_id;
	mysqli_query($conn,$query);
	//return $query;
}
function getDetails($conn,$practitioner_id,$type)
{
	$query="SELECT PRACTITIONERDETAIL_ID,MASTERDETAILTYPE_ID,DETAIL_TITLE,DETAIL_SUBTITLE,DETAIL_PRICE,REPLACE(DETAIL_MAINBODY, '\n', '<br/>') as DETAIL_MAINBODY,DETAIL_IMAGE,DETAIL_VIDEO,DETAIL_PDF,DETAIL_TERMS,DETAIL_SOCIALNETWORK,DETAIL_PRACTICEHOUR,DETAIL_DATE, PRACTITIONER_BUSINESSNUMBER, PRACTITIONER_BUSINESSADDRESS, PRACTITIONER_BUSINESSSUBURB, PRACTITIONER_BUSINESSPOSTCODE, WEBSITEURL FROM PRACTITIONERDETAIL pd LEFT JOIN PRACTITIONER pr ON(pr.PRACTITIONER_NUMBER=pd.PRACTITIONER_NUMBER) WHERE pd.PRACTITIONER_NUMBER='" . $practitioner_id . "' AND MASTERDETAILTYPE_ID = " . $type . " ORDER BY PRACTITIONERDETAIL_ID DESC";
	$result = mysqli_query($conn,$query);
	if(mysqli_num_rows($result)!=0){
		return $result;
	}
	else{
		return null;
	}
}
function countDetails($conn,$practitioner_id,$type)
{
	$query="SELECT COUNT(*) FROM PRACTITIONERDETAIL WHERE PRACTITIONER_NUMBER=" . $practitioner_id . " AND MASTERDETAILTYPE_ID = " . $type;
	$result = mysqli_query($conn,$query);
	if(mysqli_num_rows($result)!=0){
		return $result;
	}
	else{
		return null;
	}
}
function getMaxId($conn,$practitioner_id,$type)
{
	$query="SELECT PRACTITIONERDETAIL_ID FROM PRACTITIONERDETAIL WHERE PRACTITIONER_NUMBER=" . $practitioner_id . " AND MASTERDETAILTYPE_ID = " . $type . " ORDER BY PRACTITIONERDETAIL_ID DESC  LIMIT 0 , 1";
	//echo "<br><br><br><br>" . $query;
	$result = mysqli_query($conn,$query);
	if(mysqli_num_rows($result)!=0){
		$total = mysqli_fetch_array($result);
		return $total[0];
	}
	else{
		return 0;
	}
}
function uploadImage($conn,$details,$id,$detailId){
	$name=$details["flogo"]["name"];
	$tmpname=$details["flogo"]["tmp_name"];
	$size=$details["flogo"]["size"];
	if (($details["flogo"]["type"] == "image/jpeg") || ($details["flogo"]["type"] == "image/png") || ($details["flogo"]["type"] == "image/gif"))
	{
		$type="";
		switch ($details["flogo"]["type"])
		{
			case "image/jpeg":
				$type=".jpg";
				break;
			case "image/png":
				$type=".png";
				break;
			case "image/gif":
				$type=".gif";
				break;
		}
		$image = new SimpleImage();
		$image->load($details['flogo']['tmp_name']);
		$image->save('photos/originals/d_' . $detailId . '_' . $id . $type);
		$image->resizeToHeight(175);
		$image->save('photos/thumbs/d_' . $detailId . '_' . $id . $type);
		
		@mysqli_query($conn,"UPDATE PRACTITIONERDETAIL SET DETAIL_IMAGE='d_" . $detailId . "_" . $id . $type . "' WHERE PRACTITIONERDETAIL_ID='" . $detailId . "'");
	}
	else
		echo "<br><br><br>Upload Image Failed";
}
function convertAddressToLngLat($address){
	$address = str_replace(" ", "+", $address);

	$json = file_get_contents("http://maps.google.com/maps/api/geocode/json?address=$address&sensor=false");
//echo $json;

	$decoded = json_decode($json);
	$lat = $decoded->{'results'}[0]->{'geometry'}->{'location'}->{'lat'};
	$long = $decoded->{'results'}[0]->{'geometry'}->{'location'}->{'lng'};
	return $lat.",".$long;
}
function readFromXML($path){
	$oldDoc = new DOMDocument('1.0', 'ISO-8859-1');
	$oldDoc->load($path."includes/currency.xml");
	$items = $oldDoc->getElementsByTagName( "currency" );
	$result;
	$c=0;
	foreach( $items as $item )
	{
		$temp = $item->getElementsByTagName( "csymbol" ); 
		$result[$c][0]=$temp->item(0)->nodeValue;
		
		$temp = $item->getElementsByTagName( "cname" ); 
		$result[$c][1]=$temp->item(0)->nodeValue;
		
		$temp = $item->getElementsByTagName( "crate" ); 
		$result[$c][2]=$temp->item(0)->nodeValue;
		
		$temp = $item->getElementsByTagName( "cinverse" ); 
		$result[$c][3]=$temp->item(0)->nodeValue;
		$c++;
	} 
	return $result;

}
function getCountryNumber($conn,$country){
	$query = "SELECT COUNTRY_NUMBER  FROM COUNTRY WHERE COUNTRYNAME='{$country}'";
	$result=mysqli_query($conn,$query);
	if(mysqli_num_rows($result)!=0){
		$total = mysqli_fetch_array($result);
		return $total[0];
	}
	else{
		return 0;
	}
}
function getCountryName($conn,$country){
	$query = "SELECT COUNTRYNAME  FROM COUNTRY WHERE COUNTRY_NUMBER='{$country}'";
	$result=mysqli_query($conn,$query);
	if(mysqli_num_rows($result)!=0){
		$total = mysqli_fetch_array($result);
		return $total[0];
	}
	else{
		return "";
	}
}
function uploadImageIdea($conn,$details,$id,$detailId){
	$name=$details["flogo"]["name"];
	$tmpname=$details["flogo"]["tmp_name"];
	$size=$details["flogo"]["size"];
	if (($details["flogo"]["type"] == "image/jpeg") || ($details["flogo"]["type"] == "image/png") || ($details["flogo"]["type"] == "image/gif"))
	{
		$type="";
		switch ($details["flogo"]["type"])
		{
			case "image/jpeg":
				$type=".jpg";
				break;
			case "image/png":
				$type=".png";
				break;
			case "image/gif":
				$type=".gif";
				break;
		}
		$title="idea_".$id."_".$detailId.$type;
		$image = new SimpleImage();
		$image->load($details['flogo']['tmp_name']);
		$image->save('media/originals/'.$title);
		$image->resizeToHeight(175);
		$image->save('media/thumbs/'.$title);
	}
	else
		$title="";
		
	return $title;
}
function teamNotification($to,$name,$detail,$image,$header,$type){ //for critic or idea
	
	$subject="We have got ".$header." from client";
	$body="
	Hi, team<br>
	We have got $header from $name<br><br>
	The detail is:<br>
	$detail<br><br>
	$image
	";
	$image="";
	if($type==0){
		if($header=="a critic")
			$image="http://platform.wotmed.com/images/email/PractitionerCriticismImage.jpg";
		else
			$image="http://platform.wotmed.com/images/email/PractitionerIdeaSuggestionImage.jpg";
	}
	else{
		if($header=="a critic")
			$image="http://platform.wotmed.com/images/email/PatientCriticismImage.jpg";
		else
			$image="http://platform.wotmed.com/images/email/PatientIdeaSuggestion.jpg";
	}
	sendEmail($to,$subject,$image,$body);
}
function emailNewPassword($to,$password){ //for critic or idea
	
	$subject="Reset password";
	$body="Dear, $to <br>
		Your new password is : $password";
	$image="http://platform.wotmed.com/images/email/PasswordReset.jpg";
	sendEmail($to,$subject,$image,$body);
}
function SharePrincipleEmail($coon,$to,$id){ //for critic or idea
	$query = "SELECT PRACTITIONER_BUSINESSNAME FROM PRACTITIONER WHERE PARTICIPANT_NUMBER=$id";
	$result=mysqli_query($conn,$query);
	if(mysqli_num_rows($result)!=0){
		$temp = mysqli_fetch_array($result);
		$name = $temp[0];
		$image="http://platform.wotmed.com/images/email/WotmedPractitionerWordOfMouth.jpg";
	}
	else{
		$query = "SELECT FIRSTNAME,SURNAME FROM PARTICIPANT WHERE PARTICIPANT_NUMBER=$id";
		$result=mysqli_query($conn,$query);
		$temp = mysqli_fetch_array($result);
		$name = $temp[0] . " " . $temp[1];
		$image="http://platform.wotmed.com/images/email/WotmedPatientWordOfMouthEmail.jpg";
	}
	$subject="Our users share our declaration of principle";
	$body="Hi team, <br>
		{$name} has shared our declaration of principle via email";
	sendEmail($to,$subject,$image,$body);
}
function SharePrincipleSocialMedia($coon,$to,$id){ //for critic or idea
	$query = "SELECT PRACTITIONER_BUSINESSNAME FROM PRACTITIONER WHERE PARTICIPANT_NUMBER=$id";
	$result=mysqli_query($conn,$query);
	if(mysqli_num_rows($result)!=0){
		$temp = mysqli_fetch_array($result);
		$name = $temp[0];
		$image="http://platform.wotmed.com/images/email/WotmedPractitionerWordOfMouthSocialMedia.jpg";
	}
	else{
		$query = "SELECT FIRSTNAME,SURNAME FROM PARTICIPANT WHERE PARTICIPANT_NUMBER=$id";
		$result=mysqli_query($conn,$query);
		$temp = mysqli_fetch_array($result);
		$name = $temp[0] . " " . $temp[1];
		$image="http://platform.wotmed.com/images/email/WotmedPatientWordOfMouthSocialMedia.jpg";
	}
	$subject="Our users share our declaration of principle";
	$body="Hi team, <br>
		{$name} has shared our declaration of principle via social media";
	sendEmail($to,$subject,$image,$body);
}
function getPlayer($link,$cover,$length){
	$getID3 = new getID3;
	$file = $getID3->analyze($link);
	$length=$file['playtime_string'];
	if($length==""){
		$file = $getID3->analyze("../gallery/".$link);
		$length=$file['playtime_string'];
	}
	return "<div class='lfloat gborder videoItem' style='margin-bottom:5px; position:relative;'><a class='myPlayer' href='{$link}' style='background-image:url({$cover});background-size:100%;display:block;width:520px;height:330px;'><img src='../images/play_button.png' style='padding-left:135px;padding-top:70px;width:250px;'/></a><div class='videoLength'>$length</div></div><br clear='all' />";
}
function getPlayerFacilitator($link,$cover,$length){
	$getID3 = new getID3;
	$file = $getID3->analyze($link);
	$length=$file['playtime_string'];
	if($length==""){
		$file = $getID3->analyze($link);
		$length=$file['playtime_string'];
	}
	return "<div class='lfloat gborder videoItem' style='margin-bottom:5px; position:relative;'><a class='myPlayer' href='{$link}' style='background-image:url({$cover});background-size:100%;display:block;width:520px;height:330px;'><img src='../../images/play_button.png' style='padding-left:135px;padding-top:70px;width:250px;'/></a><div class='videoLength'>$length</div></div><br clear='all' />";
}
function getSpeciality($conn,$speciality){
	$countryQuery="SELECT SPECIALITY FROM SPECIALITY WHERE SPECIALITY_NUMBER = '$speciality' LIMIT 0 , 1";
	//echo $countryQuery;
	$result=mysqli_query($conn,$countryQuery);
	$temp = mysqli_fetch_array($result);
	return $temp[0];
}
function getPP($conn,$partNumber){
	$countryQuery="SELECT PROFILEPHOTO FROM PARTICIPANT WHERE PARTICIPANT_NUMBER = '$partNumber' LIMIT 0 , 1";
	//echo $countryQuery;
	$result=mysqli_query($conn,$countryQuery);
	$temp = mysqli_fetch_array($result);
	return $temp[0];
}
function getRedirect(){
	$result="profile.php";
	if(isset($_SESSION['practitioner_id']))
		$result="practitioner_profile.php";
	return $result;
}
function getPractitionerId($conn,$id){
	$query="SELECT PRACTITIONER_NUMBER FROM PRACTITIONER WHERE PARTICIPANT_NUMBER = $id";
	$result = mysqli_query($conn,$query);
	$temp = mysqli_fetch_array($result);
	return $temp[0];
}
function getNumberOfFeedback($conn,$id){
	$query=$query="SELECT COUNT(*) FROM PATIENTPRACTITIONERRELATIONSHIP WHERE PRACTITIONER_NUMBER = ". getPractitionerId($conn,$id) ." and RECOMMENDEDAS = 1";
	$result = mysqli_query($conn,$query);
	$temp = mysqli_fetch_array($result);
	return $temp[0];
}
function getFeedback($conn,$id,$limit){
	$query="SELECT * FROM PATIENTPRACTITIONERRELATIONSHIP WHERE PRACTITIONER_NUMBER = ". getPractitionerId($conn,$id) ." and RECOMMENDEDAS = 1 ORDER BY RECOMMENDDATE DESC LIMIT 0,$limit";
	$result = mysqli_query($conn,$query);
	if(mysqli_num_rows($result)!=0){
		return $result;
	}
	else{
		return null;
	}
}
function getThankFeedback($conn,$id,$limit){
	$query="SELECT * FROM THANKS WHERE PRACTITIONER_NUMBER = ". getPractitionerId($conn,$id) ." ORDER BY THANKSDATE DESC LIMIT 0,$limit";
	$result = mysqli_query($conn,$query);
	if(mysqli_num_rows($result)!=0){
		return $result;
	}
	else{
		return null;
	}
}
function getNumberOfThanks($conn,$id){
	$query=$query="SELECT COUNT(*) FROM THANKS WHERE PRACTITIONER_NUMBER = ". getPractitionerId($conn,$id);
	//echo $query;
	$result = mysqli_query($conn,$query);
	$temp = mysqli_fetch_array($result);
	return $temp[0];
}
function getServiceDetails($conn,$id){
	$query="SELECT * FROM SERVICES WHERE SERVICES_NUMBER = $id";
	$result = mysqli_query($conn,$query);
	$temp = mysqli_fetch_array($result);
	if(mysqli_num_rows($result)!=0){
		return $temp;
	}
	else{
		return null;
	}
}
function getServiceName($conn,$id){
	$query=$query="SELECT TECHNICALPROCEDURENAME FROM MASTERSERVICE WHERE MASTERSERVICE_NUMBER = $id";
	//echo $query;
	$result = mysqli_query($conn,$query);
	$temp = mysqli_fetch_array($result);
	return $temp[0];
}
function display_cart($conn,$items,$qtys){
	$result="<TABLE BORDER=1 style='width:100%'>";
	if(count($items)>0){
		$result.= "
			<tr>
				<th>NO.</th>
				<th>DETAILS</th>
				<th>PRICE</th>
				<th>QTY</th>
				<th>SUBTOTOAL</th>
				<th>OPTION</th>
			</tr>
		";
		global $total;		// changed to global variable 
							// by Clarke Towson 27th July 2014 so as to pass the value to paypal button via parallel2.php
							
		$total=0;
		for($i=0;$i<count($items);$i++){
			$index=$i+1;
			$item_id=$items[$i];
			$temp=getServiceDetails($conn,$item_id);
			$price=$temp['PRICE'];
			$qty=$qtys[$i];
			$subTot=$price*$qty;
			$total+=$subTot;
			$result .= "
				<tr>
					<td>$index</td>
					<td>".getServiceName($conn,$temp['MASTERSERVICE_NUMBER'])." ".$item_id."</td>
					<td align='right'>$ ".number_format($price, 2, '.', ',')."</td>
					<td align='center'>{$qty}</td>
					<td align='right'>$ ".number_format($subTot, 2, '.', ',')."</td>
					<td align='center'>
						<a href='' onclick='removeItem($item_id);return false'>Delete</a>
					</td>
				</tr>
			";
		}
		$result .= "
			<tr>
				<td colspan='6' align='right'>Total : $ ".number_format($total, 2, '.', ',')."</td>
			</tr>
		";
	}else{
		$result .= "
			<tr>
				<td><b>No item(s) in your Wotmed Services Cart</b></td>
			<tr>
		";
	}
	$result .="</TABLE>";
	return $result;
}
function getNumberOfFam($conn,$id){
	$query=$query="SELECT COUNT(*) FROM RELATIONSHIP WHERE ISPARTICIPANT = ". $id;
	$result = mysqli_query($conn,$query);
	$temp = mysqli_fetch_array($result);
	return $temp[0];
}
function getNumberOfConnection($conn,$id){
	$query=$query="SELECT COUNT(*) FROM PATIENTPRACTITIONERRELATIONSHIP WHERE PARTICIPANT_NUMBER = ". $id;
	$result = mysqli_query($conn,$query);
	$temp = mysqli_fetch_array($result);
	return $temp[0];
}
function getNumberOfThanksParticipant($conn,$id){
	$query=$query="SELECT COUNT(*) FROM THANKS WHERE PARTICIPANT_NUMBER = ". $id;
	//echo $query;
	$result = mysqli_query($conn,$query);
	$temp = mysqli_fetch_array($result);
	return $temp[0];
}
function getNumberOfRecommended($conn,$id){
	$query=$query="SELECT COUNT(*) FROM PATIENTPRACTITIONERRELATIONSHIP WHERE PARTICIPANT_NUMBER = ". $id ." and RECOMMENDED = 1";
	$result = mysqli_query($conn,$query);
	$temp = mysqli_fetch_array($result);
	return $temp[0];
}
function getCurSymbol($conn,$id){
	if($id!=null){
		$query="SELECT CURRENCY_SYMBOL FROM COUNTRY WHERE COUNTRY_NUMBER = '$id'";
		$localCur=mysqli_query($conn,$query);
		$curCurrency=mysqli_fetch_array($localCur);
		$cur="";
		$pieces = explode(", ",$curCurrency[0]);
		foreach ($pieces as $value){
			$cur.="&#x$value;";
		}
		return $cur;
	}else{
		return "$";
	}
}
function getNumbOfParticipant($conn,$id){
	$query="SELECT COUNT(*) FROM PATIENTPRACTITIONERRELATIONSHIP WHERE PRACTITIONER_NUMBER = '".getPractitionerId($conn,$id)."'";
	$res=mysqli_query($conn,$query);
	$temp = mysqli_fetch_array($res);
	return $temp[0];
}
function formatDOB($dob){
	$temp=explode("-",$dob);
	return $temp[2]."-".$temp[1]."-".$temp[0];
}

function increaseViewCount($conn,$participantId){
	$query="UPDATE PRACTITIONER SET VIEWCOUNT=VIEWCOUNT+1 WHERE PARTICIPANT_NUMBER='$participantId'";
	$result=mysqli_query($conn,$query);
}

function participantGetTrust($conn,$id)
{
    $query="SELECT COUNT(*) FROM PATIENTPRACTITIONERRELATIONSHIP WHERE PARTICIPANT_NUMBER = '$id' AND TRUSTED = '1'";
    $res=mysqli_query($conn,$query);
    $temp = mysqli_fetch_array($res);
    return $temp[0];
}


// SURGERY FACILITATOR FUNCTIONS 
// For Trust, Thanked, Recommended, Connected to

// NOTE CAREFULLY - THIS NEEDS TO BE SLIGHLY RECODED DUE TO THE CHANGE IN THE DATABASE FOR SURGERY FACILITATOR

// Table name: PATIENTSURGERYFACILITATORRELATIONSHIP

// Use these for now and recode slighly later to ensure it is the surgery facilitator 
// Clarke towson May 24th 2014

function participantGetTrustSurgeryFacilitator($conn,$id)
{
$query="SELECT COUNT(*) FROM PATIENTPRACTITIONERRELATIONSHIP WHERE PRACTITIONER_NUMBER = '$id' AND TRUSTED = '1'";
    $res=mysqli_query($conn,$query);
    $temp = mysqli_fetch_array($res);
    return $temp[0];
}

function getNumberOfThanksParticipantSurgeryFacilitator($conn,$id)
{
	$query=$query="SELECT COUNT(*) FROM THANKS WHERE PRACTITIONER_NUMBER = ". getPractitionerId($conn,$id);
	//echo $query;
	$result = mysqli_query($conn,$query);
	$temp = mysqli_fetch_array($result);
	return $temp[0];
}

function getNumberOfRecommendedSurgeryFacilitator($conn,$id)
{
	$query=$query="SELECT COUNT(*) FROM PATIENTPRACTITIONERRELATIONSHIP WHERE PARTICIPANT_NUMBER = ". $id ." and RECOMMENDED = 1";
	$result = mysqli_query($conn,$query);
	$temp = mysqli_fetch_array($result);
	return $temp[0];
	
}

function getNumberOfFamSurgeryFacilitator($conn,$id)
{
	$query=$query="SELECT COUNT(*) FROM RELATIONSHIP WHERE ISPARTICIPANT = ". $id;
	$result = mysqli_query($conn,$query);
	$temp = mysqli_fetch_array($result);
	return $temp[0];
	
}

function getNumberOfConnectionSurgeryFacilitator($conn,$id)
{
	$query=$query="SELECT COUNT(*) FROM PATIENTPRACTITIONERRELATIONSHIP WHERE PARTICIPANT_NUMBER = ". $id;
	$result = mysqli_query($conn,$query);
	$temp = mysqli_fetch_array($result);
	return $temp[0];
}





function pracGetTrust($conn,$id){
    $query="SELECT COUNT(*) FROM PATIENTPRACTITIONERRELATIONSHIP WHERE PRACTITIONER_NUMBER = '$id' AND TRUSTED = '1'";
    $res=mysqli_query($conn,$query);
    $temp = mysqli_fetch_array($res);
    return $temp[0];
}
function isPractitioner($conn, $id){
	$is=false;
	$query="SELECT COUNT(*) FROM PRACTITIONER WHERE PARTICIPANT_NUMBER = '$id'";
    $res=mysqli_query($conn,$query);
    $temp = mysqli_fetch_array($res);
    if($temp[0]>0)
    	$is=true;
    return $is;
}
function isAvailableToUser($conn, $email, $mediaId){
	$is=false;
	$query="SELECT COUNT(*) FROM PRIVACYMEDIA pm, PARTICIPANT p WHERE p.EMAILADDRESS=pm.EMAIL AND p.EMAILADDRESS = '$email' AND pm.IMAGEANDVIDEO_NUMBER = '$mediaId';";
    $res=mysqli_query($conn,$query);
    $temp = mysqli_fetch_array($res);
    if($temp[0]>0){
    	$is=true;
    }else{
		$query="SELECT COUNT(*) FROM IMAGEANDVIDEO i, ALBUM a, PARTICIPANT p WHERE a.ALBUM_NUMBER=i.ALBUM_NUMBER AND p.PARTICIPANT_NUMBER=a.PARTICIPANT_NUMBER AND p.EMAILADDRESS = '$email' AND i.IMAGEANDVIDEO_NUMBER = '$mediaId'";
	    $res=mysqli_query($conn,$query);
	    $temp1 = mysqli_fetch_array($res);
	    if($temp1[0]>0){
	    	$is=true;
	    }
    }
    return $is;
}
function grantAccess($conn,$email,$mediaId){
	$query="INSERT INTO PRIVACYMEDIA(IMAGEANDVIDEO_NUMBER,EMAIL) VALUES('$mediaId','$email')";
    $res=mysqli_query($conn,$query);
}


function getTodaysPhysician2($conn)
{
	
	$today = date("Y/m/d");
	$query = "SELECT * FROM PHYSICIANS WHERE LASTDISPLAYED = '$today'";
	$result = mysqli_query($conn, $query);
	
	if(mysqli_num_rows($result) > 0 )
	{
		return mysqli_fetch_array($result);
		
	}
	
	else
	
	{
		$query = "SELECT COUNT(*) FROM PHYSICIANS WHERE LASTDISPLAYED is null";
		$result = mysqli_query($conn, $query);
		
	}

}


function getTodaysPhysician($conn){
	$today = date("Y/m/d");
	$query = "SELECT * FROM PHYSICIANS WHERE LASTDISPLAYED = '$today'";
	$result = mysqli_query($conn, $query);
	if(mysqli_num_rows($result) > 0 ){
		return mysqli_fetch_array($result);
	}else{
		$query = "SELECT COUNT(*) FROM PHYSICIANS WHERE LASTDISPLAYED is null";
		$result = mysqli_query($conn, $query);
		$numbOfAvailablePhycisian = mysqli_fetch_array($result);
		if($numbOfAvailablePhycisian[0] == 0){
			$query = "UPDATE PHYSICIANS SET LASTDISPLAYED = null";
			mysql_query($conn, $query);
		}
		$query = "SELECT COUNT(*) FROM PHYSICIANS";
		$result = mysqli_query($conn, $query);
		$numbOfPhycisian = mysqli_fetch_array($result);
		while(true){
			$rand = rand(0,$numbOfPhycisian[0]);
			$query = "SELECT * FROM PHYSICIANS WHERE PHYSICIANS_NUMBER = '$rand' AND LASTDISPLAYED is null";
			$result = mysqli_query($conn, $query);
			if(mysqli_num_rows($result) > 0){
				$query = "UPDATE PHYSICIANS SET LASTDISPLAYED = '$today' WHERE PHYSICIANS_NUMBER = '$rand'";
				mysqli_query($conn, $query);
				return mysqli_fetch_array($result);
			}
		}
	}
}

function transTo($target, $text){
		$apiKey = 'AIzaSyCtaXJ8ECL-qmxpuKQc5HCmXK9VxF8uiW4';
	    //$text = 'Hello world!';
	    $url = 'https://www.googleapis.com/language/translate/v2?key=' . $apiKey . '&q=' . rawurlencode($text) . '&source=en&target='.$target;

	    $handle = curl_init($url);
	    curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
	    $response = curl_exec($handle);                 
	    $responseDecoded = json_decode($response, true);
	    curl_close($handle);
	    
	    return $responseDecoded['data']['translations'][0]['translatedText'];
	}
?>
