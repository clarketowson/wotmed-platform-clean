<?php
	if(isset($_GET['id']))
		$row=getPractitionerDetail($conn,$_GET['id']);
	else
		$row=getPractitionerDetail($conn,$_SESSION['id'])
?>
<div id="botColumn" class="lfloat" style="width:100%;margin-top:10px;">
	<div class="lfloat" style="width:100%;">
		<div id='HeadSectionWrapper' style='margin: 0 auto; text-align:center;margin-top:15px;min-width:50px;;' class=''>
			<div id='HeadSection' style='margin: 0 auto; text-align:center;border-top: 1px solid rgb(170, 170, 170);border-bottom: 1px solid rgb(170, 170, 170);min-width:50px;display:inline-block;' class=''>
				<div class="" style='margin: 0 auto;min-width:50px;display:inline-block;'>
				  <span style='font-size: 24px;font-weight: bold;'><?php 
				  $query="SELECT SPECIALITY FROM SPECIALITY WHERE SPECIALITY_NUMBER = '" . $row['SPECIALITY_NUMBER'] . "'";
				  $speciality=mysqli_query($conn,$query);
				  if($speciality!=NULL){
					$specialityName=mysqli_fetch_array($speciality);
					echo $row['PRACTITIONER_BUSINESSNAME'] . " Specialist " . $specialityName[0]; 
				  }
				  ?></span>
				</div><br>
				<div class="" style='margin: 0 auto;min-width:50px;display:inline-block;'>
				  <span style='font-size: 20px;font-weight: bold;'>
				  <?php
					$slogan=getDetails($conn,$row['PRACTITIONER_NUMBER'],17);
					if($slogan!=NULL){
						$sloganText=mysqli_fetch_array($slogan);
						echo $sloganText['DETAIL_MAINBODY'];
					}
				  ?>
				  </span>
				</div><br>
				<div class="" style='margin: 0 auto;min-width:50px;display:inline-block;'>
				  <span class=""><?php echo $row['PRACTITIONER_BUSINESSPROFESSIONALSTATEMENT'];?></span>
				</div><!--
				<div class="PractitionerProfilePatientPerspectiveIndependentWebsiteHeadlineWrapper" style='margin:auto; text-align:center;'>
				  <p class="PractitionerProfilePatientPerspectiveIndependentWebsiteHeadlineText">
				  <?php/*
				  $web=getDetails($conn,$row['PRACTITIONER_NUMBER'],10);
				  if($web!=null){
					  $webDetail=mysqli_fetch_array($web);
					  echo "<a target='_blank' href='http://" . $webDetail['DETAIL_TITLE'] . "'>" . $webDetail['DETAIL_TITLE'] . "</a>";
				  }*/
				  ?>
				  </p>
				</div>-->
			</div>
			<div class='lfloat' style='width:100%'>
			</div>
		</div>
		<!-- DRAWER -->
		<div class="drawers-wrapper lfloat">
			<div class="boxcap captop"></div>
			<ul class="drawers">
				<li class="drawer">
					<h2 class="drawer-handle open">Practitioner Detail</h2>
					<ul>
						<li id="liAbout"><a title="" href="#" onclick="displayBlock('AboutWrapper'); return false;">About Us</a></li>
						<li id="liPhilosophy"><a title="" href="#" onclick="displayBlock('PhilosophyWrapper'); return false;">Our Philosophy</a></li>
						<li id="liCustomerCare"><a title="" href="#" onclick="displayBlock('CustomerCareWrapper'); return false;">Customer Care Policy</a></li>
						<li id="liAcceptedInsurance"><a title="" href="#" onclick="displayBlock('AcceptedInsuranceWrapper'); return false;">Accepted Insurance</a></li>
						<li id="liAdvertisements"><a title="" href="#" onclick="displayBlock('AdvertisementsWrapper'); return false;">Advertisements</a></li>
						<li id="liPromotions"><a title="" href="#" onclick="displayBlock('PromotionsWrapper'); return false;">Promotions</a></li>
						<li id="liTeam"><a title="" href="#" onclick="displayBlock('TeamWrapper'); return false;">Teams</a></li>
						<li id="liClientCase"><a title="" href="#" onclick="displayBlock('ClientCaseWrapper'); return false;">Client Case Studies</a></li>
						<li id="liClientFinancing"><a title="" href="#" onclick="displayBlock('ClientFinancingWrapper'); return false;">Client Financing</a></li>
						<li id="liNews"><a title="" href="#" onclick="displayBlock('NewsWrapper'); return false;">News</a></li>
						<li id="liPress"><a title="" href="#" onclick="displayBlock('PressWrapper'); return false;">Press</a></li>
						<li id="liBlog"><a title="" href="#" onclick="displayBlock('BlogWrapper'); return false;">Blog</a></li>
						<li id="liTechnology"><a title="" href="#" onclick="displayBlock('TechnologyWrapper'); return false;">Technology</a></li>
						<li id="liGlossary"><a title="" href="#" onclick="displayBlock('GlossaryWrapper'); return false;">Independent Glossary</a></li>
						<li id="liPrivacy"><a title="" href="#" onclick="displayBlock('PrivacyWrapper'); return false;">Privacy Policy</a></li>
						<li id="liCopyright"><a title="" href="#" onclick="displayBlock('CopyrightWrapper'); return false;">Copyright Statement</a></li>
						<li id="liDisclaimer"><a title="" href="#" onclick="displayBlock('DisclaimerWrapper'); return false;">Disclaimer Statement</a></li>
						<li id="liTerms"><a title="" href="#" onclick="displayBlock('TermsWrapper'); return false;">Terms and Conditions</a></li>
						<li id="liFAQ"><a title="" href="#" onclick="displayBlock('FAQWrapper'); return false;">F.A.Q. (Frequently Asked Questions)</a></li>
					</ul>
				</li>
				<!--<li class="drawer">
					<h2 class="drawer-handle">Blog</h2>
					<ul>
						<li><a title="" href="#" onclick="return false;">An Entry Blog</a></li>
						<li><a title="" href="#" onclick="return false;">An Entry Blog</a></li>
						<li><a title="" href="#" onclick="return false;">An Entry Blog</a></li>
						<li><a title="" href="#" onclick="return false;">An Entry Blog</a></li>
						<li><a title="" href="#" onclick="return false;">An Entry Blog</a></li>
						<li><a title="" href="#" onclick="return false;">An Entry Blog</a></li>
						<li><a title="" href="#" onclick="return false;">An Entry Blog</a></li>
					</ul>
				</li>
				<li class="drawer last">
					<h2 class="drawer-handle">Advertisments</h2>
					<ul>
						<li><a title="" href="#" onclick="return false;">An Entry Adverts</a></li>
						<li><a title="" href="#" onclick="return false;">An Entry Adverts</a></li>
						<li><a title="" href="#" onclick="return false;">An Entry Adverts</a></li>
						<li><a title="" href="#" onclick="return false;">An Entry Adverts</a></li>
						<li><a title="" href="#" onclick="return false;">An Entry Adverts</a></li>
						<li><a title="" href="#" onclick="return false;">An Entry Adverts</a></li>
						<li><a title="" href="#" onclick="return false;">An Entry Adverts</a></li>
						<li><a title="" href="#" onclick="return false;">An Entry Adverts</a></li>
					</ul>
				</li>-->
			</ul>
			<div class="boxcap"></div>
		</div>
		
		
		<!-- DETAIL CONTENT -->
		<div class="lfloat" style="width:79%;">
			<?php
				$about=getDetails($conn,$row['PRACTITIONER_NUMBER'],1);
				if($about!=null){
			?>
			<script>$('#liAbout').css('display','block');</script>
			<div class="detailWrapper" id="AboutWrapper">
				<div class="PractitionerProfilePatientPerspectiveAboutMainTextHeadlineWrapper">
				  <p class="PractitionerProfilePatientPerspectiveAboutMainTextHeadline"><H3>About</H3></p>
				</div>
				<div class="stdWrapper" onmouseover="this.style.background='white';"onmouseout="this.style.background='#F0F0F0';" style="cursor: pointer">
				<?php
						$aboutDetails=mysqli_fetch_array($about);
						echo "<p class='PractitionerProfilePatientPerspectiveAboutMainText'><H2><div class=\"headerDetDiv\"><img src='photos/thumbs/" . $aboutDetails['DETAIL_IMAGE'] . "' alt='' /></div> " . $aboutDetails['DETAIL_TITLE'] . "</H2>";
						echo "<H3>" . $aboutDetails['DETAIL_SUBTITLE'] . "</H3><br>";
						echo $aboutDetails['DETAIL_MAINBODY'] . "<br><br>";
						echo $aboutDetails['DETAIL_PRACTICEHOUR'] . "<br><br>";
						echo $aboutDetails['WEBSITEURL'] . "<br><br>";
						echo $aboutDetails['DETAIL_SOCIALNETWORK'] . "<br><br>";
					//	echo $aboutDetails['PRACTITIONER_BUSINESSNUMBER'] . "<br><br>";
					//	echo $aboutDetails['PRACTITIONER_BUSINESSADDRESS'] . "<br><br>";
						echo "</p>";
				?>
				</div>
			</div>
			<?php
				}
				$philosophy=getDetails($conn,$row['PRACTITIONER_NUMBER'],14);
				if($philosophy!=null){
			?>
			<script>$('#liPhilosophy').css('display','block');</script>
			<div class="detailWrapper" id="PhilosophyWrapper">
				<div class="PractitionerProfilePatientPerspectivePhilosophyHeadlineWrapper">
				  <p class="PractitionerProfilePatientPerspectivePhilosophyHeadlineText"><H3>Our Philosophy</H3></p>
				</div>
				<div class="stdWrapper" onmouseover="this.style.background='white';"onmouseout="this.style.background='#F0F0F0';" style="cursor: pointer">
				<?php
						$philosophyDetails=mysqli_fetch_array($philosophy);
						echo "<p class='PractitionerProfilePatientPerspectiveAboutMainText'><H2><div class=\"headerDetDiv\"><img src='photos/thumbs/" . $philosophyDetails['DETAIL_IMAGE'] . "' alt='' /></div> " . $philosophyDetails['DETAIL_SUBTITLE'] . "</H2>";
						echo $philosophyDetails['DETAIL_MAINBODY'] . "<br><br>";
						echo $philosophyDetails['DETAIL_SOCIALNETWORK'] . "<br><br>";
						echo "</p>";
				?>
				</div>
			</div>
			<?php
				}
				$customerCare=getDetails($conn,$row['PRACTITIONER_NUMBER'],5);
				if($customerCare!=null){
			?>
			<script>$('#liCustomerCare').css('display','block');</script>
			<div class="detailWrapper" id="CustomerCareWrapper">
				<div class="PractitionerProfilePatientPerspectiveCustomerCarePolicyHeadlineWrapper">
				  <p class="PractitionerProfilePatientPerspectiveCustomerCarePolicyHeadlineText"><H3>Customer Care Policy	</H3></p>
				</div>
				<div class="stdWrapper" onmouseover="this.style.background='white';"onmouseout="this.style.background='#F0F0F0';" style="cursor: pointer">
				<?php
						$customerCareDetails=mysqli_fetch_array($customerCare);
						echo "<p class='PractitionerProfilePatientPerspectiveAboutMainText'><H2><div class=\"headerDetDiv\"><img src='photos/thumbs/" . $customerCareDetails['DETAIL_IMAGE'] . "' alt='' /></div> " . $customerCareDetails['DETAIL_SUBTITLE'] . "</H2>";
						echo $customerCareDetails['DETAIL_MAINBODY'] . "<br><br>";
						echo $customerCareDetails['DETAIL_SOCIALNETWORK'] . "<br><br>";
						echo "</p>";
				?>
				</div>
			</div>
			<?php
				}
				$advertiment=getDetails($conn,$row['PRACTITIONER_NUMBER'],2);
				if($advertiment!=null){
			?>
			<script>$('#liAdvertisements').css('display','block');</script>
			<div class="detailWrapper" id="AdvertisementsWrapper">
				<div class="PractitionerProfilePatientPerspectiveAdvertisementsHeadlineWrapper">
				  <p class="PractitionerProfilePatientPerspectiveAdvertisementsHeadlineText"><H3>Advertisements</H3></p>
				</div>
				<div class="stdWrapper" onmouseover="this.style.background='white';"onmouseout="this.style.background='#F0F0F0';" style="cursor: pointer">
				<?php
						while($advertimentDetails=mysqli_fetch_array($advertiment)){
							echo "<p class='PractitionerProfilePatientPerspectiveAboutMainText'><H2><div class=\"headerDetDiv\"><img src='photos/thumbs/" . $advertimentDetails['DETAIL_IMAGE'] . "' alt='' /></div> " . $advertimentDetails['DETAIL_TITLE'] . "</H2>";
							echo "<H3>" . $advertimentDetails['DETAIL_SUBTITLE'] . "</H3><br>";
							echo $advertimentDetails['DETAIL_MAINBODY'] . "<br><br>";
							if($advertimentDetails['DETAIL_PRICE']>0)
								echo "$ " . $advertimentDetails['DETAIL_PRICE'] . "<br><br>";
							else
								echo "Please call for price<br><br>";
							echo "</p>";
						}
				?>
				</div>
			</div>
			<?php
				}
				$promotion=getDetails($conn,$row['PRACTITIONER_NUMBER'],16);
				if($promotion!=null){
			?>
			<script>$('#liPromotions').css('display','block');</script>
			<div class="detailWrapper" id="PromotionsWrapper">
				<div class="PractitionerProfilePatientPerspectivePromotionsHeadlineWrapper">
				  <p class="PractitionerProfilePatientPerspectivePromotionsHeadlineText"><H3>Promotions</H3></p>
				</div>
				<div class="stdWrapper" onmouseover="this.style.background='white';"onmouseout="this.style.background='#F0F0F0';" style="cursor: pointer">
				<?php
						while($promotionDetails=mysqli_fetch_array($promotion)){
							echo "<p class='PractitionerProfilePatientPerspectiveAboutMainText'><H2><div class=\"headerDetDiv\"><img src='photos/thumbs/" . $promotionDetails['DETAIL_IMAGE'] . "' alt='' /></div> " . $promotionDetails['DETAIL_TITLE'] . "</H2>";
							echo "<H3>" . $promotionDetails['DETAIL_SUBTITLE'] . "</H3><br>";
							echo $promotionDetails['DETAIL_MAINBODY'] . "<br><br>";
							if($promotionDetails['DETAIL_PRICE']>0)
								echo "$ " . $promotionDetails['DETAIL_PRICE'] . "<br><br>";
							else
								echo "Please call for price<br><br>";
							echo $promotionDetails['DETAIL_TERMS'] . "<br><br>";
							echo "<a href=\"//{$promotionDetails['DETAIL_SOCIALNETWORK']}\">".$promotionDetails['DETAIL_SOCIALNETWORK'] . "</a><br><br>";
							echo "</p>";
						}
				?>
				</div>
			</div>
			<?php
				}
			?>
			<?php
				$team=getDetails($conn,$row['PRACTITIONER_NUMBER'],11);
				if($team!=null){
			?>
			<script>$('#liTeam').css('display','block');</script>
			<div class="detailWrapper" id="TeamWrapper">
				<div class="PractitionerProfilePatientPerspectiveTeamBioHeadlineWrapper">
				  <p class="PractitionerProfilePatientPerspectiveTeamBioHeadlineText"><H3>My Team</H3></p>
				</div>
				<div class="stdWrapper" onmouseover="this.style.background='white';"onmouseout="this.style.background='#F0F0F0';" style="cursor: pointer">
				<div id="overlayPhoto">
					<div id="overlayContent">
					</div>
				</div>
				<script>
					function updateOverlayPhoto(fileName) {
						if (window.XMLHttpRequest)
						{// code for IE7+, Firefox, Chrome, Opera, Safari
							xmlhttp=new XMLHttpRequest();
						}
						else
						{// code for IE6, IE5
							xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
						}
						xmlhttp.onreadystatechange=function()
						{
							if (xmlhttp.readyState==4 && xmlhttp.status==200)
							{
								el = document.getElementById("overlayPhoto");
								document.getElementById("overlayContent").innerHTML=xmlhttp.responseText;
								el.style.visibility = "visible";
								el.style.top=$(document).scrollTop()+"px";
								document.body.style.overflow= "hidden";
								$("a").css('pointer-events','auto');
								popUpIcon("galleryBack");
								popUpIcon("share");
							}
						}
						xmlhttp.open("GET","photos/overlayTeamPhoto.php?file=" + fileName,true);
						xmlhttp.send();
					}
					function closePhoto(){
						el = document.getElementById("overlayPhoto");
						el.style.visibility = "hidden";
						document.body.style.overflow= "visible";
					}
				</script>
				<?php
						while($teamDetails=mysqli_fetch_array($team)){
							echo "<p class='PractitionerProfilePatientPerspectiveAboutMainText'><H2><div class=\"headerDetDiv\" onclick=\"updateOverlayPhoto('" . $teamDetails['DETAIL_IMAGE'] . "'); return false;\"><img src='photos/thumbs/" . $teamDetails['DETAIL_IMAGE'] . "' alt='' /></div> " . $teamDetails['DETAIL_TITLE'] . "</H2>";
							echo "<H3>" . $teamDetails['DETAIL_SUBTITLE'] . "</H3><br>";
							echo $teamDetails['DETAIL_MAINBODY'] . "<br><br>";
							echo $teamDetails['DETAIL_SOCIALNETWORK'] . "<br><br>";
							echo "</p>";
						}
				?>
				</div>
			</div>
			<?php
				}
				$studyCase=getDetails($conn,$row['PRACTITIONER_NUMBER'],20);
				if($studyCase!=null){
			?>
			<script>$('#liClientCase').css('display','block');</script>
			<div class="detailWrapper" id="ClientCaseWrapper">
				<div class="PractitionerProfilePatientPerspectiveClientCaseStudiesHeadlineWrapper">
				  <p class="PractitionerProfilePatientPerspectiveClientCaseStudiesHeadlineText"><H3>Client Case Studies</H3></p>
				</div>
				<div class="stdWrapper" onmouseover="this.style.background='white';"onmouseout="this.style.background='#F0F0F0';" style="cursor: pointer">
				<?php
						while($studyCaseDetails=mysqli_fetch_array($studyCase)){
							echo "<p class='PractitionerProfilePatientPerspectiveAboutMainText'><H2><div class=\"headerDetDiv\"><img src='photos/thumbs/" . $studyCaseDetails['DETAIL_IMAGE'] . "' alt='' /></div> " . $studyCaseDetails['DETAIL_TITLE'] . "</H2>";
							echo "<H3>" . $studyCaseDetails['DETAIL_SUBTITLE'] . "</H3><br>";
							echo $studyCaseDetails['DETAIL_MAINBODY'] . "<br><br>";
							echo $studyCaseDetails['DETAIL_PRICE'] . "<br><br>";
							echo $studyCaseDetails['DETAIL_TERMS'] . "<br><br>";
							echo $studyCaseDetails['DETAIL_SOCIALNETWORK'] . "<br><br>";
							echo "</p>";
						}
				?>
				</div>
			</div>
			<?php
				}
				$finace=getDetails($conn,$row['PRACTITIONER_NUMBER'],8);
				if($finace!=null){
			?>
			<script>$('#liClientFinancing').css('display','block');</script>
			<div class="detailWrapper" id="ClientFinancingWrapper">
				<div class="PractitionerProfilePatientPerspectiveFinancingHeadlineWrapper">
				  <p class="PractitionerProfilePatientPerspectiveFinancingHeadlineText"><H3>Client Financing</H3></p>
				</div>
				<div class="stdWrapper" onmouseover="this.style.background='white';"onmouseout="this.style.background='#F0F0F0';" style="cursor: pointer">
				<?php
						while($finaceDetails=mysqli_fetch_array($finace)){
							echo "<p class='PractitionerProfilePatientPerspectiveAboutMainText'><H2><div class=\"headerDetDiv\"><img src='photos/thumbs/" . $finaceDetails['DETAIL_IMAGE'] . "' alt='' /></div> " . $finaceDetails['DETAIL_TITLE'] . "</H2>";
							echo "<H3>" . $finaceDetails['DETAIL_SUBTITLE'] . "</H3><br>";
							echo $finaceDetails['DETAIL_MAINBODY'] . "<br><br>";
							echo $finaceDetails['DETAIL_SOCIALNETWORK'] . "<br><br>";
							echo "</p>";
						}
				?>
				</div>
			</div>
			<?php
				}
				$news=getDetails($conn,$row['PRACTITIONER_NUMBER'],13);
				if($news!=null){
			?>
			<script>$('#liNews').css('display','block');</script>
			<div class="detailWrapper" id="NewsWrapper">
				<div class="PractitionerProfilePatientPerspectiveNewsHeadlineWrapper">
				  <p class="PractitionerProfilePatientPerspectiveNewsHeadlineText"><H3>News</H3></p>
				</div>
				<div class="stdWrapper" onmouseover="this.style.background='white';"onmouseout="this.style.background='#F0F0F0';" style="cursor: pointer">
				<?php
						while($newsDetails=mysqli_fetch_array($news)){
							echo "<p class='PractitionerProfilePatientPerspectiveAboutMainText'><H2><div class=\"headerDetDiv\"><img src='photos/thumbs/" . $newsDetails['DETAIL_IMAGE'] . "' alt='' /></div> " . $newsDetails['DETAIL_TITLE'] . "</H2> ";
							echo substr($newsDetails['DETAIL_DATE'],0,10) . "<br><br>";
							echo $newsDetails['DETAIL_MAINBODY'] . "<br><br>";
							echo $newsDetails['DETAIL_SOCIALNETWORK'] . "<br><br>";
							echo "</p>";
						}
				?>
				</div>
			</div>
			<?php
				}
				$blog=getDetails($conn,$row['PRACTITIONER_NUMBER'],3);
				if($blog!=null){
			?>
			<script>$('#liBlog').css('display','block');</script>
			<div class="detailWrapper" id="BlogWrapper">
				<div class="PractitionerProfilePatientPerspectiveBlogHeadlineWrapper">
				  <p class="PractitionerProfilePatientPerspectiveBlogHeadlineText"><H3>Blog</H3></p>
				</div>
				<div class="stdWrapper" onmouseover="this.style.background='white';"onmouseout="this.style.background='#F0F0F0';" style="cursor: pointer">
				<?php
						while($blogDetails=mysqli_fetch_array($blog)){
							echo "<p class='PractitionerProfilePatientPerspectiveAboutMainText'><H2><div class=\"headerDetDiv\"><img src='photos/thumbs/" . $blogDetails['DETAIL_IMAGE'] . "' alt='' /></div> " . $blogDetails['DETAIL_TITLE'] . "</H2> ";
							echo substr($blogDetails['DETAIL_DATE'],0,10) . "<br><br>";
							echo $blogDetails['DETAIL_MAINBODY'] . "<br><br>";
							echo $blogDetails['DETAIL_SOCIALNETWORK'] . "<br><br>";
							echo "</p>";
						}
				?>
				</div>
			</div>
			<?php
				}
				$press=getDetails($conn,$row['PRACTITIONER_NUMBER'],19);
				if($press!=null){
			?>
			<script>$('#liPress').css('display','block');</script>
			<div class="detailWrapper" id="PressWrapper">
				<div class="PractitionerProfilePatientPerspectivePressHeadlineWrapper">
				  <p class="PractitionerProfilePatientPerspectivePressHeadlineText"><H3>Press</H3></p>
				</div>
				<div class="stdWrapper" onmouseover="this.style.background='white';"onmouseout="this.style.background='#F0F0F0';" style="cursor: pointer">
				<?php
						while($pressDetails=mysqli_fetch_array($press)){
							echo "<p class='PractitionerProfilePatientPerspectiveAboutMainText'><H2><div class=\"headerDetDiv\"><img src='photos/thumbs/" . $pressDetails['DETAIL_IMAGE'] . "' alt='' /></div> " . $pressDetails['DETAIL_SUBTITLE'] . "</H2> ";
							echo substr($pressDetails['DETAIL_DATE'],0,10) . "<br><br>";
							echo $pressDetails['DETAIL_MAINBODY'] . "<br><br>";
							echo $pressDetails['DETAIL_SOCIALNETWORK'] . "<br><br>";
							echo "</p>";
						}
				?>
				</div>
			</div>
			<?php
				}
				$tech=getDetails($conn,$row['PRACTITIONER_NUMBER'],12);
				if($tech!=null){
			?>
			<script>$('#liTechnology').css('display','block');</script>
			<div class="detailWrapper" id="TechnologyWrapper">
				<div class="PractitionerProfilePatientPerspectiveTechnologyHeadlineWrapper">
				  <p class="PractitionerProfilePatientPerspectiveTechnologyHeadlineText"><H3>Technology</H3></p>
				</div>
				<div class="stdWrapper" onmouseover="this.style.background='white';"onmouseout="this.style.background='#F0F0F0';" style="cursor: pointer">
				<?php
						while($techDetails=mysqli_fetch_array($tech)){
							echo "<p class='PractitionerProfilePatientPerspectiveAboutMainText'><H2><div class=\"headerDetDiv\"><img src='photos/thumbs/" . $techDetails['DETAIL_IMAGE'] . "' alt='' /></div> " . $techDetails['DETAIL_TITLE'] . "</H2> ";
							echo "<H3>" . $techDetails['DETAIL_SUBTITLE'] . "</H3><br>";
							echo $techDetails['DETAIL_MAINBODY'] . "<br><br>";
							echo $techDetails['DETAIL_SOCIALNETWORK'] . "<br><br>";
							echo "</p>";
						}
				?>
				</div>
			</div>
			<?php
				}
				$faq=getDetails($conn,$row['PRACTITIONER_NUMBER'],7);
				if($faq!=null){
			?>
			<script>$('#liFAQ').css('display','block');</script>
			<div class="detailWrapper" id="FAQWrapper">
				<div class="PractitionerProfilePatientPerspectiveFAQHeadlineWrapper">
				  <p class="PractitionerProfilePatientPerspectiveFAQHeadlineText"><H3>FAQ</H3></p>
				</div>
				<div class="stdWrapper" onmouseover="this.style.background='white';"onmouseout="this.style.background='#F0F0F0';" style="cursor: pointer">
				<?php
						while($faqDetails=mysqli_fetch_array($faq)){
							echo "<p class='PractitionerProfilePatientPerspectiveAboutMainText'><H2><div class=\"headerDetDiv\"><img src='photos/thumbs/" . $faqDetails['DETAIL_IMAGE'] . "' alt='' /></div> " . $faqDetails['DETAIL_SUBTITLE'] . "</H2> ";
							
							echo substr($faqDetails['DETAIL_DATE'],0,10) . "<br><br>";
							echo $faqDetails['DETAIL_MAINBODY'] . "<br><br>";
							echo $faqDetails['DETAIL_SOCIALNETWORK'] . "<br><br>";
							echo "</p>";
						}
				?>
				</div>
			</div>
			<?php
				}
				$glossary=getDetails($conn,$row['PRACTITIONER_NUMBER'],9);
				if($glossary!=null){
			?>
			<script>$('#liGlossary').css('display','block');</script>
			<div class="detailWrapper" id="GlossaryWrapper">
				<div class="PractitionerProfilePatientPerspectiveFAQHeadlineWrapper">
				  <p class="PractitionerProfilePatientPerspectiveFAQHeadlineText"><H3>Glossary</H3></p>
				</div>
				<div class="stdWrapper" onmouseover="this.style.background='white';"onmouseout="this.style.background='#F0F0F0';" style="cursor: pointer">
				<?php
						while($glossaryDetails=mysqli_fetch_array($glossary)){
							echo "<p class='PractitionerProfilePatientPerspectiveAboutMainText'><H2>" . $glossaryDetails['DETAIL_SUBTITLE'] . "</H2> ";
							echo substr($glossaryDetails['DETAIL_DATE'],0,10) . "<br><br>";
							echo $glossaryDetails['DETAIL_MAINBODY'] . "<br><br>";
							echo $glossaryDetails['DETAIL_SOCIALNETWORK'] . "<br><br>";
							echo "</p>";
						}
				?>
				</div>
			</div>
			<?php
				}
				$privacy=getDetails($conn,$row['PRACTITIONER_NUMBER'],15);
				if($privacy!=null){
			?>
			<script>$('#liPrivacy').css('display','block');</script>
			<div class="detailWrapper" id="PrivacyWrapper">
				<div class="PractitionerProfilePatientPerspectiveFAQHeadlineWrapper">
				  <p class="PractitionerProfilePatientPerspectiveFAQHeadlineText"><H3>Privacy</H3></p>
				</div>
				<div class="stdWrapper" onmouseover="this.style.background='white';"onmouseout="this.style.background='#F0F0F0';" style="cursor: pointer">
				<?php
						while($privacyDetails=mysqli_fetch_array($privacy)){
							echo "<p class='PractitionerProfilePatientPerspectiveAboutMainText'><H2>" . $privacyDetails['DETAIL_SUBTITLE'] . "</H2> ";
							echo substr($privacyDetails['DETAIL_DATE'],0,10) . "<br><br>";
							echo $privacyDetails['DETAIL_MAINBODY'] . "<br><br>";
							echo $privacyDetails['DETAIL_SOCIALNETWORK'] . "<br><br>";
							echo "</p>";
						}
				?>
				</div>
			</div>
			<?php
				}
				$copyright=getDetails($conn,$row['PRACTITIONER_NUMBER'],4);
				if($copyright!=null){
			?>
			<script>$('#liCopyright').css('display','block');</script>
			<div class="detailWrapper" id="CopyrightWrapper">
				<div class="PractitionerProfilePatientPerspectiveFAQHeadlineWrapper">
				  <p class="PractitionerProfilePatientPerspectiveFAQHeadlineText"><H3>Copyright</H3></p>
				</div>
				<div class="stdWrapper" onmouseover="this.style.background='white';"onmouseout="this.style.background='#F0F0F0';" style="cursor: pointer">
				<?php
						while($copyrightDetails=mysqli_fetch_array($copyright)){
							echo "<p class='PractitionerProfilePatientPerspectiveAboutMainText'><H2>" . $copyrightDetails['DETAIL_SUBTITLE'] . "</H2> ";
							echo substr($copyrightDetails['DETAIL_DATE'],0,10) . "<br><br>";
							echo $copyrightDetails['DETAIL_MAINBODY'] . "<br><br>";
							echo $copyrightDetails['DETAIL_SOCIALNETWORK'] . "<br><br>";
							echo "</p>";
						}
				?>
				</div>
			</div>
			<?php
				}
				$disclaimer=getDetails($conn,$row['PRACTITIONER_NUMBER'],6);
				if($disclaimer!=null){
			?>
			<script>$('#liDisclaimer').css('display','block');</script>
			<div class="detailWrapper" id="DisclaimerWrapper">
				<div class="PractitionerProfilePatientPerspectiveFAQHeadlineWrapper">
				  <p class="PractitionerProfilePatientPerspectiveFAQHeadlineText"><H3>Disclaimer</H3></p>
				</div>
				<div class="stdWrapper" onmouseover="this.style.background='white';"onmouseout="this.style.background='#F0F0F0';" style="cursor: pointer">
				<?php
						while($disclaimerDetails=mysqli_fetch_array($disclaimer)){
							echo "<p class='PractitionerProfilePatientPerspectiveAboutMainText'><H2>" . $disclaimerDetails['DETAIL_SUBTITLE'] . "</H2> ";
							echo substr($disclaimerDetails['DETAIL_DATE'],0,10) . "<br><br>";
							echo $disclaimerDetails['DETAIL_MAINBODY'] . "<br><br>";
							echo $disclaimerDetails['DETAIL_SOCIALNETWORK'] . "<br><br>";
							echo "</p>";
						}
				?>
				</div>
			</div>
			<?php
				}
				$insurances=getAcceptedInsurance($conn,$row['PRACTITIONER_NUMBER']);
				if($insurances!=null){
			?>
			<script>$('#liAcceptedInsurance').css('display','block');</script>
			<div class="detailWrapper" id="AcceptedInsuranceWrapper">
				<div class="PractitionerProfilePatientPerspectiveFAQHeadlineWrapper">
				  <p class="PractitionerProfilePatientPerspectiveFAQHeadlineText"><H3>Accepted Insurance</H3></p>
				</div>
				<div class="stdWrapper" onmouseover="this.style.background='white';"onmouseout="this.style.background='#F0F0F0';" style="cursor: pointer"><ul>
				<?php
						while($insuranceDetails=mysqli_fetch_array($insurances)){
							echo "<li><p class='PractitionerProfilePatientPerspectiveAboutMainText'><H2>" . $insuranceDetails['HEALTHINSURANCEPROVIDER_NAME'] . "</H2> ";
							echo "</p></li>";
						}
				?></ul>
				</div>
			</div>
			<?php
				}
				$terms=getDetails($conn,$row['PRACTITIONER_NUMBER'],18);
				if($terms!=null){
			?>
			<script>$('#liTerms').css('display','block');</script>
			<div class="detailWrapper" id="TermsWrapper">
				<div class="PractitionerProfilePatientPerspectiveTechnologyHeadlineWrapper">
				  <p class="PractitionerProfilePatientPerspectiveTechnologyHeadlineText"><H3>Terms and Conditions</H3></p>
				</div>
				<div class="stdWrapper" onmouseover="this.style.background='white';"onmouseout="this.style.background='#F0F0F0';" style="cursor: pointer">
				<?php
						while($termDetails=mysqli_fetch_array($terms)){
							echo "<p class='PractitionerProfilePatientPerspectiveAboutMainText'><H2><div class=\"headerDetDiv\"><img src='photos/thumbs/" . $termDetails['DETAIL_IMAGE'] . "' alt='' /></div> " . $termDetails['DETAIL_TITLE'] . "</H2> ";
							
							
							
							echo "<H3>" . $termDetails['DETAIL_SUBTITLE'] . "</H3><br>";
							echo $termDetails['DETAIL_MAINBODY'] . "<br><br>";
							echo $termDetails['DETAIL_SOCIALNETWORK'] . "<br><br>";
							echo "</p>";
						}
				?>
				</div>
			</div>
			<?php
			
			
			
				}
			?>
		</div>
		<div class="lfloat" style="width:100%;">
			<p>&nbsp;</p>
			<div class="PractitionerProfilePatientPerspectivePrivacyMainTextWrapper" style='margin:auto; text-align:center;'>
			  <p class="PractitionerProfilePatientPerspectivePrivacyMainText"><H3>All client information is kept strictly confidential</H3></p>
			</div>
			<div class="PractitionerProfilePatientPerspectiveCopyrightMainTextWrapper" style='margin:auto; text-align:center;'>
			  <p class="PractitionerProfilePatientPerspectiveCopyrightMainText"><H3>Copyright</H3> &copy; <?php	echo date('Y'); ?> <?php echo $row['PRACTITIONER_BUSINESSNAME']; ?></p>
			</div>
			<div class="PractitionerProfilePatientPerspectiveTermsConditionsMainTextWrapper" style='margin:auto; text-align:center;'>
<!--			  <p class="PractitionerProfilePatientPerspectiveTermsConditionsMainText"><H3>Terms &amp; Conditions</H3></p> -->
			</div>
			<p>&nbsp;</p>
			<p>&nbsp;</p>
			<p class="copyrightText">&nbsp;</p>
			<p class="copyrightText">&nbsp;</p>
		</div>
	</div>
</div>
