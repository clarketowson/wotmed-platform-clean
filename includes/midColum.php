<style type="text/css">
    .centeredImage
    {
        text-align:center;
        margin-top:0px;
        margin-bottom:0px;
        padding:0px;
    }
</style>

<div id="contentMain" class="lfloat">
	<div id="noticeDiv">
		<div id="uploadPhotoDiv" class="fMid fcg">
			<form method='POST' enctype="multipart/form-data" action= 'uploadProfilePicture.php'>
			Update your profile picture:<br/>
			<input type="file" name="pp"/>
			<input type="submit" value="Upload"/>
			</form>
		</div>
	</div>
	<div class="marBotBig marTopBig">
		<div class="rfloat" style="padding-top:15px;color:rgb(170, 170, 170)">
			<span>Practitioner No: <?php echo $row['PRACTITIONER_NUMBER']; ?></span><br>
			<span>Profile Views: <?php echo $row['VIEWCOUNT'];  ?></span>
		</div>



		<div class="bold marBotBig subtitle fMid fColor" style="padding-top:7px;">
			<span style="font-size:20px" class="lfloat">Dr <?php echo $row['PRACTITIONER_BUSINESSNAME']; ?>  
            </span><?php echo $trusted; ?><?php echo $trustIndex; ?>
			<span style="font-size:10px;margin-left:5px;margin-top:10px" class="">
			<?php
			if(isset($_GET['id'])){
				$temp=explode("'",$_GET['id']);
				$id=$temp[0];

				if($id!=$_SESSION['id']){
					$relation=true; //-- true if there is no relationship between practitioner and participant
					$doctors=getDoctor($conn,$_SESSION['id']);
					if($doctors!=Null)
					{
						while($listOfDoctors=mysqli_fetch_array($doctors)){
							if($row['PRACTITIONER_NUMBER']==$listOfDoctors[0])
							{
								$relation=false;
								//echo "This doctor is already in your list";
							}
						}
						if($relation)
						{
							echo "<a href='addDoctorToAccount.php?isId=" . $_SESSION['id'] . "&ofId=" . $row['PRACTITIONER_NUMBER'] . "'>Establish a relationship with this Practitioner</a>";
						}
					}
					else
					{
						echo "<a href='addDoctorToAccount.php?isId=" . $_SESSION['id'] . "&ofId=" . $row['PRACTITIONER_NUMBER'] . "'>Establish a relationship with this Practitioner</a>";
					}
				}
			}
			?>
			</span>
			<BR><BR>
			<?php
				echo "<div style='font-size:12px;font-weight:normal;padding-left:20px;padding-top:5px;'>";
				if(isset($_GET['id'])){
					echo "
					This practitioner has been <div id='trustText' style='display:inline-block;padding-top:3px;'><a href='#' style='color:#195297;font-style:italic;'>Trusted</a><div id='trustTextPopUp' class='Indices'>This is the total number of Wotmed patients that trust this practitioner</div></div> by: " . $numTrust . " Wotmed Patient(s)<BR>";
					echo "
					This practitioner has been <a href='#' style='color:#195297;font-style:italic;'>Recommended</a> by: " . $numRecom . " Wotmed Patient(s)<BR>";
					echo "
					This practitioner has been <a href='#' style='color:#195297;font-style:italic;'>Thanked</a> by: ". $numThank ." Wotmed Patient(s)<br><br>";
					
					echo "
					This practitioner has been <div id='trustText' style='display:inline-block;padding-top:3px;'><a href='#' style='color:rgb(16, 93, 87);font-style:italic;'>Trusted</a><div id='trustTextPopUp' class='Indices'>This is the total number of Wotmed Surgery Facilitators that trust this Practitioner</div></div> by: " . $numTrust . " Wotmed Patient(s)<BR>";
					echo "
					This practitioner has been <a href='#' style='color:#195297;font-style:italic;'>Recommended</a> by: " . $numRecom . " Wotmed Surgery Facilitator(s)<BR>";
					echo "
					This practitioner has been <a href='#' style='color:#195297;font-style:italic;'>Thanked</a> by: ". $numThank ." Wotmed Surgery Facilitator(s)<br><br>";
					
					
					
					
					
					$temp=explode("'",$_GET['id']);
					$id=$temp[0];

					echo "
					This practitioner is <a href='#' style='color:#195297;font-style:italic;'>Connected</a> to: " . getNumbOfParticipant($conn,$id) . " Wotmed Practitioners<br>
					This practitioner is <a href='#' style='color:#195297;font-style:italic;'>Connected</a> to: 0 Wotmed Surgery Facilitator(s)<br>

					

					
					";
				}
				else{
					echo "You have been <div id='trustText' style='display:inline-block;padding-top:3px;'><a href='#' style='color:#195297;font-style:italic;'>Trusted</a><div id='trustTextPopUp' class='Indices'>This is the total number of Wotmed patients that trust you</div></div> by: " . $numTrust . " Wotmed Patient(s)<BR>
				
					
					You have been <a href='#' style='color:#195297;font-style:italic;'>Recommended</a> by: " . $numRecom . " Wotmed Patient(s)<BR>
					
					You have been <a href='#' style='color:#195297;font-style:italic;'>Thanked</a> by: ". $numThank ." Wotmed Patient(s)<br><br>
					
					
					You have been <div id='trustText' style='display:inline-block;padding-top:3px;'><a href='#' style='color:rgb(16, 93, 87);font-style:italic;'>Trusted</a><div id='trustTextPopUp' class='Indices'>This is the total number of Wotmed Surgery Facilitators that trust you</div></div> by: " . $numTrust . " Wotmed Surgery Facilitator(s)<BR>
					
					You have been <a href='#' style='color:#195297;font-style:italic;'>Recommended</a> by: " . $numRecom . " Wotmed Surgery Facilitator(s)<BR>
					
					You have been <a href='#' style='color:#195297;font-style:italic;'>Thanked</a> by: ". $numThank ." Wotmed Surgery Facilitator(s)<br><br>
					
					";
					
					
					
					echo "
					You are <a href='#' style='color:#195297;font-style:italic;'>Connected</a> to: " . getNumbOfParticipant($conn,$_SESSION['id']) . " Wotmed Patient(s)<br>
					You are <a href='#' style='color:#195297;font-style:italic;'>Connected</a> to: 0 Wotmed Practitioners(s)<br>
					You are <a href='#' style='color:#195297;font-style:italic;'>Connected</a> to: 0 Wotmed Surgery Facilitators(s)<br>
					
					
					
					";
					
				}
				echo "</div>";
				//echo "This practitioner recommended by : " . $numbOfRecommend[0] . " / " . $numbOfPatients[0];
			?>
		</div>
	</div>
	<div>
		
		<?php
			if(isset($_POST['businessNumb']))
			{	
				$businessNumb=mysqli_real_escape_string($conn,$_POST['businessNumb']);
				$address=mysqli_real_escape_string($conn,$_POST['address']);
				$suburb=mysqli_real_escape_string($conn,$_POST['suburb']);
				$postCode=mysqli_real_escape_string($conn,$_POST['postCode']);
				$country=mysqli_real_escape_string($conn,$_POST['country']);
				$profStatement=mysqli_real_escape_string($conn,$_POST['profStatement']);
				$speciality=mysqli_real_escape_string($conn,$_POST['speciality']);
				$education=mysqli_real_escape_string($conn,$_POST['education']);
				$qualification=mysqli_real_escape_string($conn,$_POST['qualifications']);
				$boardCertification=mysqli_real_escape_string($conn,$_POST['boardCertification']);
				$practitioner_professionalmembership=mysqli_real_escape_string($conn,$_POST['practitioner_professionalmembership']);
				$awardsandpublications=mysqli_real_escape_string($conn,$_POST['awardsandpublications']);
				$hospitalaffiliation=mysqli_real_escape_string($conn,$_POST['hospitalaffiliation']);
				$cdegree=mysqli_real_escape_string($conn,$_POST['cdegree']);
				$ctrained=mysqli_real_escape_string($conn,$_POST['ctrained']);
				$id=$row['PRACTITIONER_NUMBER'];
				$query="UPDATE PRACTITIONER SET PRACTITIONER_BUSINESSNUMBER='" . $businessNumb . "', COUNTRY_NUMBER='" . $country . "', PRACTITIONER_BUSINESSADDRESS='" . $address . "', PRACTITIONER_BUSINESSSUBURB='" . $suburb . "', PRACTITIONER_BUSINESSPOSTCODE='" . $postCode . "', PRACTITIONER_BUSINESSPROFESSIONALSTATEMENT='" . $profStatement . "', SPECIALITY_NUMBER='" . $speciality . "', EDUCATION='" . $education . "', QUALIFICATIONS='" . $qualification . "', BOARDCERTIFICATIONS='" . $boardCertification .  "', PRACTITIONER_PROFESSIONALMEMBERSHIP='" . $practitioner_professionalmembership .  "', AWARDSANDPUBLICATIONS='" . $awardsandpublications .  "', HOSPITALAFFILIATION='" . $hospitalaffiliation . "', C_DEGREE='$cdegree', C_TRAINED='$ctrained' WHERE PRACTITIONER_NUMBER=" . $id;
				mysqli_query($conn,$query);
				if($address != ""){
					$address = $address . ", " . $suburb . " " .$postCode;
					$tempLoc= convertAddressToLngLat($address) . ";";
					//echo $tempLoc . "<br>";
					@mysqli_query($conn,"UPDATE PRACTITIONER SET PRACTITIONER_BUSINESSGOOGLEMAPADDRESS='" . $tempLoc . "' WHERE PRACTITIONER_NUMBER='" . $row['PRACTITIONER_NUMBER'] . "'");
				}
				?>
				<script language="javascript"> 
					window.location = "practitioner_profile.php";
				</script>
				<?php
			}
			?>
		<div style="display:none;">
		<?php
		if(isset($_GET['id'])){
			$temp=explode("'",$_GET['id']);
			$id=$temp[0];

			echo "<select id='currency' name='currency' onchange=\"updateCurrency(currency.value,$id)\">";
		}
		else
			echo "<select id='currency' name='currency' onchange=\"updateCurrency(currency.value,0)\">";
		foreach($items as $item){
			if($item[0]!=$_COOKIE["Currency"])
				echo "<option label='{$item[0]}'>{$item[0]}</option>";
			else
				echo "<option label='{$item[0]}' selected='selected'>{$item[0]}</option>";
			if($item[0]=="AUD")
				echo $AUD=$item[3];
		}
		echo "</select>";
		?>
		</div>
		<div class="gborder" style='height:250px;'>
		<?php if(!isset($_GET['id'])){ ?>
		<span style='margin:5px;'>
			You can update your list of services by clicking on your Website Update Panel<br/>
		</span>
		<?php } ?>
		<table border=0>
			<tr>
				<!--<th>Number</th>-->
				<th colspan=2 style='width:170px;'>Practitioner Service(s)</th>
				<?php 
				if(isset($_GET['id'])) {
					echo "<th style='width:100px;text-align:center;'>Price</th>";
					echo "<th style='width:100px;text-align:center'><img src='images/cards_logo.png' height=12px /></th>";
				}
				else
					echo "<th colspan=3 style='width:100px;text-align:center'>Price</th>";
				?>
			</tr>
			</table>
			<?php if(!isset($_GET['id'])){ ?>
				<div style='overflow:auto;height:200px;'>
			<?php }else{ ?>
				<div style='overflow:auto;height:220px;'>
			<?php } ?>
			<table border=0>
			<?php
			$counter=0;
			
    		$servicesList=getServices($conn,$row['PRACTITIONER_NUMBER']);
			if($servicesList!=null){
				while ($list=mysqli_fetch_array($servicesList)){
					$counter++;
					echo "<tr>";
					//echo "<tr><td>" . $counter . "</td>";
					$query="SELECT TECHNICALPROCEDURENAME,WIKIPEDIALINK,WIKIPEDIAPDFGENERATORLINK,WIKIPEDIAFINALPDF FROM MASTERSERVICE WHERE MASTERSERVICE_NUMBER='" . $list['MASTERSERVICE_NUMBER'] . "'";
					$result=mysqli_query($conn,$query);
					if(mysqli_num_rows($result)!=0){
						$techName=mysqli_fetch_array($result);
					
					// Clarke Towson - code updated April 5th 2015	
					// removed hyperlinks to wiki pages and PDF's as this can't work correctly
					// due to a design flaw with the names of the practitioners services being totally self made by the practitioner
					// we can update this later to link directly to Wotmed Branded Procedure names
					// which we can work with the practitioners to create
					// take it as a branding opportunity!
					
						
						
				//		echo "<td><a target='_blank' href='http://en.wikipedia.org/w/index.php?title=Special:Book&bookcmd=render_article&arttitle=" . $techName[2] . "' onclick=\"getPdf('" . $techName[2]. "','" . $techName[3] . "'); \"><img src='images/pdf_logo.jpg' height='20' width='20'></a></td>";
				//		echo "<td style='width:150px;'><a target='_blank' href='http://en.wikipedia.org/wiki/" . $techName[1] . "'>" . $techName[0] . "</a></td>";
				
						echo "<td><img src='images/pdf_logo.jpg' height='20' width='20'></td>";
						echo "<td style='width:150px;'><x target='_blank' href='http://en.wikipedia.org/wiki/" . $techName[1] . "'>" . $techName[0] . "</x></td>";
			
					
				
				
						
					}
					if(isset($_GET['serviceId']))
					{
						if($_GET['serviceId']==$list[1])
						{
							echo "<form method='post' action='practitioner_profile.php'>";
							echo "<input type='hidden' name='serviceId' value='" . $list[1] . "'>";
							echo "<td><input type='text' name='newPrice' value='" . $list['PRICE'] . "'></td>";
							echo "<td><input type='submit' value='update'></td>";
							echo "</form>";
							echo "</tr>";
						}
						else
						{
							echo "<td>" . $list['PRICE'] . "</td></tr>";
						}
					}
					elseif(isset($_GET['deleteId']))
					{
						if($_GET['deleteId']==$list[1])
						{
							echo "<td>" . $list['PRICE'] . "</td>";
							echo "<td>Are you sure want to delete this service?</td>";
							echo "<td><a href='practitioner_profile.php?delete=" . $list[1] . "'>Yes</a></td>";
							echo "<td><a href='practitioner_profile.php'>No</a></td>";
						}
						else
						{
							echo "<td>" . $list['PRICE'] . "</td>";
						}
					}
					else
					{
						if($list['PRICE']>0){
							$aud=$_COOKIE["CurrencyAUD"];
							$usd=$_COOKIE["CurrencyUSD"];
							$cur=getCurSymbol($conn,$rowSession['COUNTRY_NUMBER']);
							$curPrice=($list['PRICE']*$aud)*$usd;
							echo "<td align='right' style='width:100px'>" . $cur . "" . number_format($curPrice , 0 , '.' , ',' ) . "</td>";
						}else
							echo "<td align='right' style='width:100px'>Please call for price</td>";
						if(!isset($_GET['id']))
						{
							//echo "<td><a href='practitioner_profile.php?serviceId=" . $list[1] . "'>Update</a></td>";
							//echo "<td><a href='practitioner_profile.php?deleteId=" . $list[1] . "'>Delete</a></td></tr>";
						}
						else
						{
							echo "<td align='center' style='width:100px;'>";
							
							//echo "<a href='paypal/purchaseSurgery.php?amount=" . $list['PRICE'] . "&email=" . $row['EMAILADDRESS'] . "'><img src='images/buy_now_button.png' height=20px /></a>";
							
							if($list['PRICE']>0)
								
								echo "<a href='' onclick='addToChart({$list['SERVICES_NUMBER']},\"".$techName[1]."\",{$list['PRICE']});return false'><img src='images/addToCartButton3.jpg' style='height:17px'></a>";
								
							//	echo "<span title='Coming Soon - purchase and pay for medical procedures'><img src='images/addToCartButton.jpg' style='height:17px'></span>";
							
							echo "</td><td style='width:100px;'>";
						//	echo "<a href='wishlist/addToWishlist.php?servicesNumber=" . $list['SERVICES_NUMBER'] . "' style='color:#105D57'>Add to Wishlist</a>";
							
							echo "<b='wishlist/addToWishlist.php?servicesNumber=" . $list['SERVICES_NUMBER'] . "' style='color:#195297'>Add to Wishlist</b>";
								
							echo "</td></tr>";
						}
					}
				}
			}
			else
				echo "<tr><td colspan='4' align='center'>No service is provided</td></tr>";
		?>
		</table>
		</div>
		</div>
		<br><br>
		<?php
			if(!isset($_GET['id']))
			{ ?>
		<div>
		<?php /*
			<div><strong>Add New Service</strong></div>
			<div>
				<form method="post" action="practitioner_profile.php">
					<?php
					$query="SELECT * FROM MASTERCATEGORY";
					$result=mysqli_query($conn,$query);
					echo "<select id='category' onchange=\"updateServiceList(this.value,document.getElementById('subcategory').value)\">";
					echo "<option value=''>Any</option>";
					while($catList=mysqli_fetch_array($result)){
						echo "<option value='" . $catList['CATEGORY_NUMBER'] . "'>" . $catList['CATEGORY'] . "</option>";
					}
					echo "</select>";

					$query="SELECT * FROM MASTERSUBCATEGORY";
					$result=mysqli_query($conn,$query);
					echo "<select id='subcategory' onchange=\"updateServiceList(document.getElementById('category').value,this.value)\">";
					echo "<option value=''>Any</option>";
					while($subList=mysqli_fetch_array($result)){
						echo "<option value='" . $subList['MASTERSUBCATEGORY_NUMBER'] . "'>" . $subList['SUBCATEGORY'] . "</option>";
					}
					echo "</select>";
					?>
					<br/>
					<select name="service" id="service" style="width:280px;">
						<?php
						$query="SELECT MASTERSERVICE_NUMBER,TECHNICALPROCEDURENAME FROM MASTERSERVICE";
						$result=mysqli_query($conn,$query);
						while($listOfServie=mysqli_fetch_array($result)){
							echo "<option value='" . $listOfServie['MASTERSERVICE_NUMBER'] . "'>" . $listOfServie['TECHNICALPROCEDURENAME'] . "</option>";
						}
						?>
					</select>
					Price : $<input type="text" name="price"/>
					<input type="submit" value="Add"/>
				</form>
			</div>*/?>
			<div class="gborder" style='background:rgb(240, 240, 240);height:190px;overflow:auto;'>		
				<div style="border-bottom:1px solid #AAA;padding-bottom:5px;margin-bottom:5px;">
					<img src="images/WotmedPractitionerScheduleIcon.png" style="width:30px;"> 
					<b style='color:#195297;font-size:16px'>Your Availability</b>
				</div>
				<!--<table border="1" width="20">-->
				<?php 
					$today = getdate();
					echo "<table border='0'>";
					echo "<tr>";
					for($i=0;$i<7;$i++)
					{
						$differentDays=$i-($today['wday']-1+1);
						$scheduleDate=($today['mday']-1+1)+$differentDays."-".$today['mon']."-".$today['year'];
						echo "<th width='80' align='center'>" . $days[$i] . "</th>";
					}
					echo "</tr>";
					
					if(mysqli_num_rows($schedules)!=0){
						echo "<tr style='vertical-align:text-top;'>";							
						for($i=0;$i<7;$i++)
						{
							echo "<td align='center'>";
							$availability=mysqli_data_seek($schedules,0);
							while($availability=mysqli_fetch_array($schedules)){
								if($availability[2]==$i)
								{
									$times=timeToMin($availability[4])-timeToMin($availability[3]);
									$totalOfSchedules=$times/$availability[5];
									$totalOfSchedules=floor($totalOfSchedules);
									$newSchedule=$availability[3];
									for($j=0;$j<=$totalOfSchedules;$j++)
									{
										echo $newSchedule . "<br>";
										$newSchedule=addedTime($newSchedule,$availability[5]);
									}
								}
							}
							echo "</td>";
						}
						echo "</tr>";
					}
					else
					{
						echo "<tr><td colspan='7' align='center'>No schedule is available</td></tr>";
					}
				?>
				</table><?php /*
				<a href='addSchedule.php'>Add Schedule</a>
				<a href='addSchedule.php?update=true'>Update Schedule</a> */ ?>
			</div>
			<br><br>
			<div style='height:170px;overflow:auto'>
				Your Appointments with Wotmed Patients
				<table border=1>
				<tr align=center>
					<th width=150>Name of Patients</th>
					<th width=80>Reason</th>
					<th width=80>Date</th>
					<th width=80>Time</th>
					<th width=150 colspan="2">Confirmation</th>
				</tr>
				<?php
					$query="SELECT * FROM BOOKING WHERE PRACTITIONER_NUMBER ='" . $_SESSION['id'] . "'";
					$books=mysqli_query($conn,$query);
					if(mysqli_num_rows($books)!=0){
						while($listOfBooking=mysqli_fetch_array($books)){
							$temp="SELECT CONCAT(FIRSTNAME,' ',SURNAME) FROM PARTICIPANT WHERE PARTICIPANT_NUMBER ='" . $listOfBooking[1] . "'";
							$name=mysqli_query($conn,$temp);
							$fullName=mysqli_fetch_array($name);
							echo "<tr><td>" . $fullName[0] . "</td>";
							echo "<td>" . $listOfBooking[5] . "</td>";
							echo "<td>" . $listOfBooking[3] . "</td>";
							echo "<td>" . $listOfBooking[4] . "</td>";
							if($listOfBooking[8]=='PENDING')
							{
								echo "<td><a href='bookRespon.php?bookId=" . $listOfBooking[0] . "&status=confirmed'>Confirm</a></td>";
								echo "<td><a href='bookRespon.php?bookId=" . $listOfBooking[0] . "&status=canceled&pateintId=" . $listOfBooking[1] . "&reason=" . $listOfBooking[5] . "'>Cancel</a></td></tr>";
							}
							else
							{
								echo "<td colspan='2' align=center>" . $listOfBooking[8] . "</td></tr>";
							}
						}
					}
					else
					{
						echo "<tr><td colspan='6' align=center>No appointment scheduled</td></tr>";
					}
				?>
				</table>
			</div>
		</div>
		<BR>
		<div class="gborder">
			<div style="border-bottom:1px solid #AAA;padding-bottom:5px;margin-bottom:5px;">
				<img src="images/PatientRecommendation.png" style="width:30px;"> 
				<b style='color:#195297;font-size:16px'>Patients who have Recommended you</b><br>
				<span><?php echo (isset($_GET['id']))?"This practitioner has":"You have"; ?> been recommended by these Wotmed patient(s):</span>
			</div>
			<div id="feedback" style="overflow:auto;height:300px;width:100%;">
				<table border="0" style="width:100%;" cellspacing="0">
				<?php
					$limit=5;
					$feedback = getFeedback($conn,$_SESSION['id'],$limit);
					if($feedback!=null){
						while($userComments=mysqli_fetch_array($feedback)){
							$temp = getParticipantDetail($conn,$userComments['PARTICIPANT_NUMBER']);
							if($temp['PROFILEPHOTO']=="" || $userComments['FUTURECONTACT']!=1){
								$pp="blankSilhouetteMale.png";
							}else{
								$pp=$temp['PROFILEPHOTO'];
							}
							echo "<tr>
									<td rowspan='2' style='width:90px;border-bottom:1px solid #AAA;'><img src='photos/thumbs/{$pp}' width='75px' /></td>";
							$name="Private User";
							if($userComments['FUTURECONTACT']!=1){
								echo "<td style='width:250px;padding-top:10px;'><b>$name</b></td>";
							}
							else{
								$name=$temp['FIRSTNAME']." ".$temp['SURNAME'];
								if($_SESSION['id']==$userComments['PARTICIPANT_NUMBER'])
									$name.=" (You)";
								echo "<td 	style='width:250px;padding-top:10px;'><b><a href='profile.php?id={$temp['PARTICIPANT_NUMBER']}'>$name</a></b></td>";
							}
							if($userComments['FUTURECONTACT']==1){
								echo "<td rowspan='2' align='center' style='border-bottom:1px solid #AAA;'><a href='profile.php?id={$temp['PARTICIPANT_NUMBER']}'><img src='images/viewProfile.png' height='30' width='40px'/><br/>View this Wotmed Patients Profile</a></td>";
								//echo "<td rowspan='2' align='center' style='border-bottom:1px solid #AAA;'><a href='mailto:{$temp['EMAILADDRESS']}'><img src='images/contactRecommend.png' height='30' width='40px'/><br/>Email Patient</a></td>";
							}else{
								echo "<td rowspan='2' colspan='2' style='border-bottom:1px solid #AAA;'>&nbsp;</td>";
							}
							echo "</tr>";
							$review=$userComments['COMMENT'];
							if($review=="")
								$review="$name has recommended {$row['PRACTITIONER_BUSINESSNAME']} but has chosen not to provide a writen review";
							echo "<tr>
									<td style='border-bottom:1px solid #AAA;padding-bottom:10px;width:250px;'>$review</td>
								</tr>";
						}
						if($limit<getNumberOfFeedback($conn,$_SESSION['id']))
							echo "<tr>
								<td colspan='3'><a href='' onclick='loadMore(5,{$_SESSION['id']});return false;'>Load more</a></td>
								</tr>";
					}
					else{
						echo "<tr>
							<td  colspan='3'>No recommendations</td>
							</tr>";
					}
				?>
				</table>
			</div>
		</div>
		<BR/>
		<div class="gborder">
			<div style="border-bottom:1px solid #AAA;padding-bottom:5px;margin-bottom:5px;">
				<img src="images/PatientRecommendation.png" style="width:30px;"> 
				<b style='color:#195297;font-size:16px'>Patients who have Thanked this Practitioner</b><br>
				<span>This practitioner has been thanked by these Wotmed patient(s):</span>
			</div>
			<div id="thankFeedback" style="overflow:auto;height:300px;width:100%;">
				<table border="0" style="width:100%;" cellspacing="0">
				<?php
					$limit=5;
					$feedback = getThankFeedback($conn,$_SESSION['id'],$limit);
					if($feedback!=null){
						while($userComments=mysqli_fetch_array($feedback)){
							$temp = getParticipantDetail($conn,$userComments['PARTICIPANT_NUMBER']);
							if($temp['PROFILEPHOTO']==""){
								$pp="blankSilhouetteMale.png";
							}else{
								$pp=$temp['PROFILEPHOTO'];
							}
							echo "<tr>
									<td rowspan='2' style='width:90px;border-bottom:1px solid #AAA;'><img src='photos/thumbs/{$pp}' width='75px' /></td>";
							$name=$temp['FIRSTNAME']." ".$temp['SURNAME'];
							if($_SESSION['id']==$userComments['PARTICIPANT_NUMBER'])
								$name.=" (You)";
							echo "<td style='width:250px;padding-top:10px;'><b><a href='profile.php?id={$temp['PARTICIPANT_NUMBER']}'>$name</a></b></td>";
							echo "<td rowspan='2' align='center' style='border-bottom:1px solid #AAA;'><a href='profile.php?id={$temp['PARTICIPANT_NUMBER']}'><img src='images/viewProfile.png' height='30' width='40px'/><br/>View Your Patient Profile</a></td>";
							//echo "<td rowspan='2' align='center' style='border-bottom:1px solid #AAA;'><a href='mailto:{$temp['EMAILADDRESS']}'><img src='images/contactRecommend.png' height='30' width='40px'/><br/>Email Patient</a></td>";
							echo "<td rowspan='2' align='center' style='border-bottom:1px solid #AAA;'><a href='#' onclick='displayContactUser({$temp['PARTICIPANT_NUMBER']})'><img src='images/contactRecommend.png' height='30' width='40px'/><br/>Email Patient</a></td>";
							
							echo "</tr>";
							$review=$userComments['COMMENT'];
							if($review=="")
								$review="$name has recommended {$row['PRACTITIONER_BUSINESSNAME']} but has chosen not to provide a writen review";
							echo "<tr>
									<td style='border-bottom:1px solid #AAA;padding-bottom:10px;width:250px;'>$review</td>
								</tr>";
						}
						if($limit<getNumberOfThanks($conn,$_SESSION['id']))
							echo "<tr>
								<td  colspan='3'><a href='#' onclick='loadMoreThank(5,{$_SESSION['id']});return false;'>Load more</a></td>
								</tr>";
					}
					else{
						echo "<tr>
							<td  colspan='3'>No thanks available for this practitioner</td>
							</tr>";
					}
				?>
				</table>
			</div>
		</div>
		<?php
		}
		else

		{


            $practitionerNumber = $row['PRACTITIONER_NUMBER'];


            echo " <div style='border-bottom:0px solid #AAA;padding-bottom:5px;margin-bottom:20px;'>";

   //     echo"<a href='booking.php?practitionerId=$practitionerNumber&date=2015-06-26&time=09:00'><img src='BookPractitionerButton6.jpg'  /><br/></a>";

            echo"<a href='booking.php?practitionerId=$practitionerNumber&date=2015-06-26&time=09:00'> <p class='centeredImage'><img src='BookPractitionerButton6.jpg' alt='Book Practitioner'></p><br/></a>";



echo "</div>";


			echo "<div name='scheduleTable' id='scheduleTable' class='gborder' style='background:rgb(240, 240, 240);height:250px;overflow:auto;'>
				<div style='border-bottom:1px solid #AAA;padding-bottom:5px;margin-bottom:5px;'>
					<img src='images/WotmedPractitionerScheduleIcon.png' style='width:30px;'> 
					<b style='color:#195297;font-size:16px'>Practitioner's Availability</b>
				</div>";
			$listOfSchedules = "";
			$today = getdate();
			$listOfSchedules .= "<table border='0'>";
			$listOfSchedules .= "<tr style='font-size:10px;'>";
			for($i=0;$i<7;$i++)
			{
				$differentDays=$i-($today['wday']-1+1);
				$thisYear=date("Y")-1+countYear(($today['yday']+$differentDays),date("Y"));
				$thisDate=date('d', strtotime($thisYear . '-01-01 +' . (($today['yday']+$differentDays-1+date('L', strtotime($thisYear."-01-01")))%(365+date('L', strtotime($thisYear."-01-01")))) . 'days'));
				$thisMonth=date('m', strtotime($thisYear . '-01-01 +' . (($today['yday']+$differentDays-1+date('L', strtotime($thisYear."-01-01")))%(365+date('L', strtotime($thisYear."-01-01")))) . 'days'));
				$scheduleDate=$thisDate."-".$thisMonth."-".$thisYear;
				$listOfSchedules .= "<th width='80' align='center'>" . $days[$i] . "<br>" . $scheduleDate . "</th>";
			}
			$listOfSchedules .= "</tr>";
			if(mysqli_num_rows($schedules)!=0){
				$schedulesLeft="";
				$listOfSchedules .= "<tr style='vertical-align:text-top;'>";
				for($i=0;$i<7;$i++)
				{
					$numbOfSchedules=0;
					$schedulesLeft .= "<td align='center'>";
					$availability=mysqli_data_seek($schedules,0);
					while($availability=mysqli_fetch_array($schedules)){
						if($availability[2]==$i AND $i>=$today['wday'])
						{	
							$differentDays=$i-($today['wday']-1+1);
							$thisYear=date("Y")-1+countYear(($today['yday']+$differentDays),date("Y"));
							$thisDate=date('d', strtotime($thisYear . '-01-01 +' . (($today['yday']+$differentDays-1+date('L', strtotime($thisYear."-01-01")))%(365+date('L', strtotime($thisYear."-01-01")))) . 'days'));
							$thisMonth=date('m', strtotime($thisYear . '-01-01 +' . (($today['yday']+$differentDays-1+date('L', strtotime($thisYear."-01-01")))%(365+date('L', strtotime($thisYear."-01-01")))) . 'days'));
							$scheduleDate=$thisYear."-".$thisMonth."-".$thisDate;
							$found=true;
							$times=timeToMin($availability[4])-timeToMin($availability[3]);
							$totalOfSchedules=$times/$availability[5];
							$totalOfSchedules=floor($totalOfSchedules);
							$newSchedule=$availability[3];
							for($j=0;$j<=$totalOfSchedules;$j++)
							{
								//$DoctorDetail=getPractitionerDetail($conn,$_GET['id']);
								$schedulesLeft .= "<a href='booking.php?practitionerId=" . $row['PRACTITIONER_NUMBER'] . "&date=" . $scheduleDate . "&time=" . $newSchedule . "'>" . $newSchedule . "</a><br>";
								$newSchedule=addedTime($newSchedule,$availability[5]);
								$numbOfSchedules++;
							}
						}
					}
					$schedulesLeft .= "</td>";
				}
				if($numbOfSchedules==0)
						$schedulesLeft="<td colspan='7' align='center'>No schedule is available</td>";
				$listOfSchedules .= $schedulesLeft . "</tr>"; ?>
				<form>
					<input type='hidden' name='id' id='id' value='<?php echo $row['PRACTITIONER_NUMBER']; ?>'>
					<input type='hidden' name='datePrev' id='datePrev' value='<?php echo ($today['yday']-1+1-7); ?>'>
					<input type='hidden' name='dateNext' id='dateNext' value='<?php echo ($today['yday']-1+1+7); ?>'>
					<input type='button' name='prev' id='prev' value='Prev' disabled onclick="updateAvailableSchedule(document.getElementById('id').value,document.getElementById('datePrev').value)">
					<input type='button' name='next' id='next' value='next' onclick="updateAvailableSchedule(document.getElementById('id').value,document.getElementById('dateNext').value)">
				</form>
				<?php
			}
			else
			{
				$listOfSchedules .= "<tr><td colspan='7' align='center'>No schedule is available</td></tr>";
			}
			echo $listOfSchedules;


			echo "</table>";
			echo "</div>";
			?>
		<BR/>
		<div class="gborder">
			<div style="border-bottom:1px solid #AAA;padding-bottom:5px;margin-bottom:5px;">
				<img src="images/PatientRecommendation.png" style="width:30px;"> 
				<b style='color:#195297;font-size:16px'>Patients who have Recommended this Practitioner</b><br>
				<span>This practitioner has been recommended by these Wotmed patient(s):</span>
			</div>
			<div id="feedback" style="overflow:auto;height:300px;width:100%;">
				<table border="0" style="width:100%;" cellspacing="0">
				<?php
					$limit=5;
					$feedback = getFeedback($conn,$_GET['id'],$limit);
					if($feedback!=null){
						while($userComments=mysqli_fetch_array($feedback)){
							$temp = getParticipantDetail($conn,$userComments['PARTICIPANT_NUMBER']);
							if($temp['PROFILEPHOTO']=="" || $userComments['FUTURECONTACT']!=1){
								$pp="blankSilhouetteMale.png";
							}else{
								$pp=$temp['PROFILEPHOTO'];
							}
							echo "<tr>
									<td rowspan='2' style='width:90px;border-bottom:1px solid #AAA;'><img src='photos/thumbs/{$pp}' width='75px' /></td>";
							$name="Private User";
							if($userComments['FUTURECONTACT']!=1){
								echo "<td style='width:250px;padding-top:10px;'><b>$name</b></td>";
							}
							else{
								$name=$temp['FIRSTNAME']." ".$temp['SURNAME'];
								if($_SESSION['id']==$userComments['PARTICIPANT_NUMBER'])
									$name.=" (You)";
								echo "<td 	style='width:250px;padding-top:10px;'><b><a href='profile.php?id={$temp['PARTICIPANT_NUMBER']}'>$name</a></b></td>";
							}
							if($userComments['FUTURECONTACT']==1){
								echo "<td rowspan='2' align='center' style='border-bottom:1px solid #AAA;'><a href='profile.php?id={$temp['PARTICIPANT_NUMBER']}'><img src='images/viewProfile.png' height='30' width='40px'/><br/>View Your Patient Profile</a></td>";
								echo "<td rowspan='2' align='center' style='border-bottom:1px solid #AAA;'><a href='#' onclick='displayContactUser({$temp['PARTICIPANT_NUMBER']})'><img src='images/contactRecommend.png' height='30' width='40px'/><br/>Email Patient</a></td>";
							}else{
								echo "<td rowspan='2' colspan='2' style='border-bottom:1px solid #AAA;'>&nbsp;</td>";
							}
							echo "</tr>";
							$review=$userComments['COMMENT'];
							if($review=="")
								$review="$name has recommended {$row['PRACTITIONER_BUSINESSNAME']} but has chosen not to provide a writen review";
							echo "<tr>
									<td style='border-bottom:1px solid #AAA;padding-bottom:10px;width:250px;'>$review</td>
								</tr>";
						}
						if($limit<getNumberOfFeedback($conn,$_GET['id']))
							echo "<tr>
								<td  colspan='3'><a href='' onclick='loadMore(5,{$_GET['id']});return false;'>Load more</a></td>
								</tr>";
					}
					else{
						echo "<tr>
							<td  colspan='3'>No recommendations</td>
							</tr>";
					}
				?>
				</table>
			</div>
		</div>
		<BR/>
		<div class="gborder">
			<div style="border-bottom:1px solid #AAA;padding-bottom:5px;margin-bottom:5px;">
				<img src="images/PatientRecommendation.png" style="width:30px;"> 
				<b style='color:#195297;font-size:16px'>Patients who have Thanked this Practitioner</b><br>
				<span>This practitioner has been thanked by these Wotmed patient(s):</span>
			</div>
			<div id="thankFeedback" style="overflow:auto;height:300px;width:100%;">
				<table border="0" style="width:100%;" cellspacing="0">
				<?php
					$temp=explode("'",$_GET['id']);
					$id=$temp[0];

					$limit=5;
					$feedback = getThankFeedback($conn,$id,$limit);
					if($feedback!=null){
						while($userComments=mysqli_fetch_array($feedback)){
							$temp = getParticipantDetail($conn,$userComments['PARTICIPANT_NUMBER']);
							if($temp['PROFILEPHOTO']==""){
								$pp="blankSilhouetteMale.png";
							}else{
								$pp=$temp['PROFILEPHOTO'];
							}
							echo "<tr>
									<td rowspan='2' style='width:90px;border-bottom:1px solid #AAA;'><img src='photos/thumbs/{$pp}' width='75px' /></td>";
							$name=$temp['FIRSTNAME']." ".$temp['SURNAME'];
							if($_SESSION['id']==$userComments['PARTICIPANT_NUMBER'])
								$name.=" (You)";
							echo "<td style='width:250px;padding-top:10px;'><b><a href='profile.php?id={$temp['PARTICIPANT_NUMBER']}'>$name</a></b></td>";
							echo "<td rowspan='2' align='center' style='border-bottom:1px solid #AAA;'><a href='profile.php?id={$temp['PARTICIPANT_NUMBER']}'><img src='images/viewProfile.png' height='30' width='40px'/><br/>View Your Patient Profile</a></td>";
							echo "<td rowspan='2' align='center' style='border-bottom:1px solid #AAA;'><a href='#' onclick='displayContactUser({$temp['PARTICIPANT_NUMBER']})'><img src='images/contactRecommend.png' height='30' width='40px'/><br/>Email Patient</a></td>";
							
							echo "</tr>";
							$review=$userComments['COMMENT'];
							if($review=="")
								$review="$name has recommended {$row['PRACTITIONER_BUSINESSNAME']} but has chosen not to provide a writen review";
							echo "<tr>
									<td style='border-bottom:1px solid #AAA;padding-bottom:10px;width:250px;'>$review</td>
								</tr>";
						}
						if($limit<getNumberOfThanks($conn,$_GET['id']))
							echo "<tr>
								<td  colspan='3'><a href='#' onclick='loadMoreThank(5,{$_GET['id']});return false;'>Load more</a></td>
								</tr>";
					}
					else{
						echo "<tr>
							<td  colspan='3'>No thanks available for this practitioner</td>
							</tr>";
					}
				?>
                
                
		

                
				</table>
			</div>
		</div>
		<?php
		}?>
	</div>
</div>
