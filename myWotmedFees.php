<script type="application/ld+json">
{
"@context": "http://schema.org",
"@type": "Service",
"name": "Wotmed Practitioner Subscription",
"description": "Buy a Practitioner Subscription to Wotmed",
"serviceType": "Wotmed Practitioner Subscription",
"url": "http://platform.wotmed.com/WotmedNews"
}
}
</script>

<?php
	session_start();
	
	header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");


	include "includes/connect.php";
	include "includes/functions.php";
	include "classes/SimpleImage.php";
	if(isset($_SESSION['id'])){
		$rowSession=getParticipantDetail($conn,$_SESSION['id']);
		$row=getPractitionerDetail($conn,$_SESSION['id']);
		$webLink="";
		//msgBox($row['PRACTITIONER_NUMBER']);
		$temp=getDetails($conn,$row['PRACTITIONER_NUMBER'],10);
		if($temp!=null){
			$detailResult= mysqli_fetch_array($temp);
			$webLink.=$detailResult['DETAIL_TITLE'];
		}
	$ppFileNameSession="blankSilhouetteMale.png";
	$ppFileName="blankSilhouetteMale.png";
	
	
	// if the logged in user is a surgery facilitator then we want to show the profile photo at left as the business logo
// otherwise the user is a practitioner and we show the profile photo on the left as the practitioner photo

if($row['ISFACILITATOR'] == 1)
{
	if($row['PRACTITIONER_BUSINESSLOGO']=="")
	{
			$ppFileNameSession="blankSilhouetteMale.png";
	}else
	
		{
			$ppFileNameSession=$row['PRACTITIONER_BUSINESSLOGO'];
		}
		
	}

else
{
	if($row['PRACTITIONER_BUSINESSLOGO']!="")
	{
		$ppFileNameSession=$row['PRACTITIONER_BUSINESSLOGO'];
		$ppFileName=$row['PRACTITIONER_BUSINESSLOGO'];
	}
	if($rowSession['PROFILEPHOTO']!="")
	{
		$ppFileNameSession=$rowSession['PROFILEPHOTO'];
	}
	
}


	
//	if($row['PRACTITIONER_BUSINESSLOGO']!=""){
//		$ppFileNameSession=$row['PRACTITIONER_BUSINESSLOGO'];
//		$ppFileName=$row['PRACTITIONER_BUSINESSLOGO'];
//	}
//	if($rowSession['PROFILEPHOTO']!=""){
//		$ppFileNameSession=$rowSession['PROFILEPHOTO'];
//	}
	
		$query="SELECT COUNT(*) FROM PATIENTPRACTITIONERRELATIONSHIP WHERE PRACTITIONER_NUMBER = '" . $_SESSION['practitioner_id'] . "' AND RECOMMENDEDAS='1'";
		$tempOfRecommend=mysqli_query($conn,$query);
		if(mysqli_num_rows($tempOfRecommend)!=0)
			$numbOfRecommend=mysqli_fetch_array($tempOfRecommend);
		else
			$numbOfRecommend[]=0;
		if(isset($_POST) && $_POST!=NULL)
		{
			//$query="UPDATE PRACTITIONERDETAIL SET DETAIL_TITLE ='{$_POST['WEBSITEURL']}' WHERE PRACTITIONER_NUMBER ={$row['PRACTITIONER_NUMBER']} AND MASTERDETAILTYPE_ID =10";
			$query="UPDATE PRACTITIONER SET PRACTITIONER_BUSINESSNAME ='{$_POST['PRACTITIONER_BUSINESSNAME']}' WHERE PRACTITIONER_NUMBER ={$row['PRACTITIONER_NUMBER']}";
			//msgBox($query);
			mysqli_query($conn,$query);
			updateDetails($conn,mysqli_real_escape_string($conn,$_POST['detail_id']),$_POST);
			if(isset($_FILES) && $_FILES["flogo"]["name"]!=NULL)
			{
				uploadImage($conn,$_FILES,$row["PRACTITIONER_NUMBER"],mysqli_real_escape_string($conn,$_POST['detail_id']));
			}
			$_POST=null;
			?>
			<script language="javascript"> 
				<?php echo "window.location = 'myAbout.php'";?>
			</script> <?php
		}
		
		$result=getDetails($conn,$row["PRACTITIONER_NUMBER"],1);
		if($result==null){
			insertDetails($conn,$row["PRACTITIONER_NUMBER"],null,1);
			$result=getDetails($conn,$row["PRACTITIONER_NUMBER"],1);
			$data=mysqli_fetch_array($result);
		}else{
			$data=mysqli_fetch_array($result);
		}
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link href="<?php echo $path; ?>style/p_style.css" rel="stylesheet"></link>
<script type="text/javascript" src="<?php echo $path; ?>classes/jquery.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Schedule of Wotmed fees</title>
<link href="style/apple.css" rel="stylesheet" type="text/css" />
</head>

<body>
<?php include $path."includes/p_header.php"; ?>
<?php
if($row['ISFACILITATOR'] == 1){
	$backLink = "
		<a href='facilitator/cpanel.php'>Back to Your Control Panel</a>
	";
	include $path."facilitator/cpanelHeader.php"; 
}
else{
	$backLink = "
		<a href='cpanel.php'>Back to Your Control Panel</a>
	";
	include $path."cpanelHeader.php"; 
}
?>
<!--
<div class="PractitionerPhotoWrapper">
  <p class="PractitionerHeadings"><img src="<?php echo "photos/originals/" . $ppFileNameSession; ?>" alt="alexPhoto" width="167" height="141" align="top" /></p>
</div>
<div class='rfloat'>
	<a href='cpanel.php'>
		Back to Your Control Panel
	</a>
</div>
<div class="PractitionerNameWrapper">
  <p class="PractitionerName"><?php echo $row['PRACTITIONER_BUSINESSNAME']; ?></p>
</div>
<div class="PractitionerHyperlinkWrapper">
  <p class="hyperlinks">www.alexyusupov.com.au</p>
</div>
<div class="PractitionerHeadingWrapper">
  <p class="hyperlinks"><span class="PractitionerHeadings">Welcome to <?php echo $row['PRACTITIONER_BUSINESSNAME']; ?>'s Profile</span></p>
</div>
<div class="PractitionerSubHeadingWrapper">
  <p class="PractitionerSubHeadings">Serving Caulfield, Balaclava, and St Kilda, VIC (Victoria)</p>
</div>
-->
<div class='lfloat' style='width:98%'>
	<div class="stdWrapper" onMouseOver="this.style.background='white';"onmouseout="this.style.background='#F0F0F0';">
	  <p>
<h3><span class="PractitionerBody">
      <?php
	  
	  header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

	 
	  ?>
      Wotmed Schedule of fees	  </span>
      
</h3>
      <br />
	  <p></p>
<p> </p>


    
	  <p>Dear Dr <?php echo $row['PRACTITIONER_BUSINESSNAME']; ?>,      </p>
     

	<!--  <p>The table below lists the monthly Wotmed Subscription Fees for each country of the world for the 2015 financial year. </p> -->
          <p>By becoming a paid subscriber you are directly helping the Wotmed Team to establish a successful business which enables us improve our services to you our valued Practitioner.<p>
        <p>To make it fair for all Practitioners to be able to afford an account on Wotmed our monthly fee is based upon the purchasing power parity of your country of origin.</p>

        <p>The Wotmed Team very much appreciates your business Dr <?php echo $row['PRACTITIONER_BUSINESSNAME'] . '.'; ?></p>
      <p>Kind Regards,</p>
      <p>The Wotmed Team</p>
      <p>&nbsp;</p>


            <?php


        // if the practitioners country has not been set then dont display the paypal subscribe button

        if ($row['COUNTRYNAME'] != null)
        {
            echo "<img src='WotmedLogo2.png' height='28' alt='Subscribe to Wotmed'></a>";
            echo "<BR><BR>";

            include "paypalSubscribeFee.php";

            $query="SELECT PRACTITIONERSUBSCRIPTIONFEEYEARLY FROM COUNTRY WHERE COUNTRY_NUMBER = " . $row['B_COUNTRY_NUMBER'];
            $result=mysqli_query($conn,$query);
            $practitionerSubscriptionFee=mysqli_fetch_array($result);

        echo "<p>The fee for your country of ";

        echo $row['COUNTRYNAME'];

        echo " for FY";

         echo date("Y");

        echo " Dr ";

        echo $row['PRACTITIONER_BUSINESSNAME'];

        echo " is USD$";

        echo $practitionerSubscriptionFee[0];

        echo "</p>";

        echo "<p> Once you click the Paypal Subscribe button you will be charged this amount monthly for your account on Wotmed.</p>";
            echo "<BR><BR>";
            echo "<p> <p>";

            echo "<b>Digital Subscription Terms and Conditions</b>";
            echo "<BR>";

            echo "Updated 17/05/2015";
            echo "<BR><BR>";


echo "1.     About Digital Subscriptions<BR>

            A digital subscription provides you with unlimited access to The Wotmed Platform.<BR>

In these terms and conditions, “Wotmed” means Wotmed Pty Limited, creator of The Wotmed Platform and associated website Wotmed.com.<BR>

The login details associated with your digital membership and/or digital subscription including username and password must not be shared. If any misuse is detected your subscription may be terminated and you will not be eligible for a refund.<BR>

<BR>2.     Processing and Payment, including Grace Period<BR>

Your digital subscription purchase will be processed instantly via PayPal.<BR>

We reserve the right to reject any subscription order at any time.<BR>

Digital subscriptions renew automatically each month unless you cancel according to our cancellation policy (see clause 9 below). Payment for digital subscriptions will be from your nominated PayPal account every calendar month.<BR>

It is your responsibility to provide valid payment details, and ensure that your payment details are up to date.<BR>

If your credit card expires or your payment method is otherwise invalid, you are entitled to continue your free access to the Wotmed Platform and your account will remain active, and you can continue to use our services.<BR>

Unless specified otherwise in the Cancellation policy, all charges are non-refundable.<BR>


<BR>3.     Pricing<BR>

When you purchase a digital subscription, the price is made clear during the PayPal subscription process. You agree to pay the price stated at the time of your order. You also agree to the billing frequency stated at the time of your order.<BR>

Discount eligibility is determined at the time of order. Discounts cannot be applied retrospectively. All prices are in United States dollars (USD).<BR>

Wotmed reserves the right to change the prices and fees at any time.<BR>

Price changes will take effect from your next billing date after the notice period. If you do not wish to continue your Digital Subscription at the revised price, You may cancel Your subscription before the end of Your current direct debit pay cycle. See section 9.<BR>


<BR>4.     Third party fees and charges<BR>

If you have purchased your digital subscription through a third party (not directly through Wotmed), these Digital Subscription Terms and Conditions of Sale may not apply to you. Wotmed is not liable to you for any claims related to purchases made through third parties. Please contact the third party directly to address these claims.<BR>

When you use your digital subscription, you may incur other additional changes, such as telecommunications fees, data fees or service provider fees. You are responsible for paying any additional charges.<BR>


<BR>5.     Promotions<BR>

Wotmed may at times offer special promotions and offers. The specific terms and conditions of each offer will be stated at the time of the promotion and will apply in addition to these Digital Subscription Terms and Conditions. In the event of any inconsistency between the terms and conditions of a special offer and these Digital Subscription Terms and Conditions, these Digital Subscription Terms and Conditions will apply.<BR>

You may be required to provide your payment details when you sign up for a promotion. If this is a direct debit offer, your subscription will be automatically renewed after your initial payment at the standard subscription rate unless you cancel under the terms of the cancellation policy (see clause 9 below).<BR>

From time to time Wotmed offers promotional items with its digital & bundle subscriptions. From time to time and at its discretion, Wotmed will communicate with its subscribers about the availability of such items and how to redeem them.<BR>


<BR>6.     Software products<BR>

When you purchase a digital subscription that includes other Wotmed apps, your purchase and terms of use will also be subject to the Apple Terms of Service, available at www.apple.com/legal/terms.<BR>


<BR>7.     Managing your Digital Subscription<BR>

You can manage your account and personal details online through the Wotmed Website Update Panel.<BR>

You can change your subscription package as you need via this page.<BR>


<BR>8.     Cancellations<BR>

You can cancel your digital subscription at any time by cancelling your PayPal monthly subscription fee:<BR>

Digital subscriptions are non-refundable.<BR>

When you cancel, you cancel only future charges associated with your subscription. You may notify us of your intent to cancel at any time, but the cancellation will become effective at the end of your current billing period. You will continue to have the same access and benefits until the end of your current billing period.<BR>


<BR>9.     Wotmed right to change digital products<BR>

Wotmed reserves the right to modify the content, inclusions, type and availability of any digital product at any time. In the event your subscription package changes, you will be given 14 days notification prior to when you would need to make a decision to cancel and not be affected. Your package will change automatically take place from your next billing cycle.<BR>

We reserve the right under special circumstances to enable free access to our subscriber content for a limited period of time. During this time subscribers will not be eligible for a refund.<BR>

If any or all of our digital products are temporarily unavailable, you will not automatically be entitled to receive a refund. We reserve the right to issues refunds or credits at our sole discretion. If we issue a refund or credit, we are under no obligation to issue the same or similar refund in the future.<BR>


<BR>10.     Your Privacy<BR>

You agree that your name, address, email address, year of birth, postcode, gender and mobile/telephone number will be collected and stored by Wotmed and used for the purpose of managing Your subscription, communicating with You about Your subscription and to notify You of any associated customer offers or benefits or future subscriptions, unless otherwise notified by you.<BR>

Information on how we handle your personal information is explained in our Privacy Policy. You can obtain further information on the Wotmed Privacy Policy by contacting the Wotmed Team.<BR>


<BR>11.     Disclaimers<BR>

You agree that any errors made in entering your contact information and order details are your responsibility and Wotmed is not liable for any consequences that may arise as a result of such errors or incorrect information, including but not limited to sending the subscription to the address as notified by you.<BR>

To the extent permitted by law, Wotmed is not liable to you for any loss or damage incurred by you in connection with your subscription, whether direct, consequential, special, indirect or other loss or damage. In any event, Wotmed's maximum liability to you is limited to the value of the subscription fees paid within the previous twelve months.<BR>


<BR>12.     General<BR>

You agree that all subscriptions provided, benefits accepted or offers made by Wotmed shall be deemed to be provided in Australia, notwithstanding your location, and the terms of such subscriptions, benefits and offers shall be governed exclusively by Victorian law, and the Courts of Victoria shall have exclusive jurisdiction to determine any such matters that may arise involving or alleged to involve Wotmed.<BR>

You acknowledge that these Digital Subscription Terms and Conditions may be modified by Wotmed from time to time, including but not limited to terms relating to fees and charges, cancellation and modifying your subscription.<BR>

You will be notified of any changes to these Subscriptions Terms and Conditions by the Wotmed Team.<BR>


<BR>13.     Definitions<BR>

In these terms and conditions:<BR>

'Wotmed', 'we' or 'us' means Wotmed Pty Limited, creator of The Wotmed Platform and associated website Wotmed.com.<BR>

'Subscriber', 'Customer' or 'You' means a person who is a subscriber to The Wotmed Platform.";



        }
            else {
                echo "<BR>";
                echo "<b>Important: Please update your country of origin in order to make a monthly subscription payment to Wotmed</b>";
                echo "<BR>";
}

       //     echo "Practitioner ID is " . $row['PRACTITIONER_NUMBER'];
       //     echo "<br>";
       //     echo "Practitioner Name is " . $row['PRACTITIONER_BUSINESSNAME'];
       //     echo "<br>";



            ?>

            &nbsp;</p>
           
    <!--  <p><img src="PractitionerPriceList2014v2.jpg" width="546" height="7318" /> -->
      </p>
	  
      
	  </p>
     
      <p>&nbsp;</p>
	</div>
</div>

<p>&nbsp;</p>
<?php include $path."includes/p_footer.php"; ?>
</body>

</html>
