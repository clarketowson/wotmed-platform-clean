
<?php
	session_start();
	include "includes/connect.php";
	include "includes/functions.php";
	include "classes/SimpleImage.php";
	$rowSession=getParticipantDetail($conn,$_SESSION['id']);
	$row=getPractitionerDetail($conn,$_SESSION['id']);
	$ppFileNameSession="blankSilhouetteMale.png";
	$ppFileName="blankSilhouetteMale.png";
	
	// if the logged in user is a surgery facilitator then we want to show the profile photo at left as the business logo
// otherwise the user is a practitioner and we show the profile photo on the left as the practitioner photo

if($row['ISFACILITATOR'] == 1)
{
	if($row['PRACTITIONER_BUSINESSLOGO']=="")
	{
			$ppFileNameSession="blankSilhouetteMale.png";
	}else
	
		{
			$ppFileNameSession=$row['PRACTITIONER_BUSINESSLOGO'];
		}
		
	}

else
{
	if($row['PRACTITIONER_BUSINESSLOGO']!="")
	{
		$ppFileNameSession=$row['PRACTITIONER_BUSINESSLOGO'];
		$ppFileName=$row['PRACTITIONER_BUSINESSLOGO'];
	}
	if($rowSession['PROFILEPHOTO']!="")
	{
		$ppFileNameSession=$rowSession['PROFILEPHOTO'];
	}
	
}


//	if($row['PRACTITIONER_BUSINESSLOGO']!=""){
//		$ppFileNameSession=$row['PRACTITIONER_BUSINESSLOGO'];
//		$ppFileName=$row['PRACTITIONER_BUSINESSLOGO'];
//	}
//	if($rowSession['PROFILEPHOTO']!=""){
//		$ppFileNameSession=$rowSession['PROFILEPHOTO'];
//	}

	$query="SELECT COUNT(*) FROM PATIENTPRACTITIONERRELATIONSHIP WHERE PRACTITIONER_NUMBER = '" . $_SESSION['practitioner_id'] . "' AND RECOMMENDEDAS='1'";
	$tempOfRecommend=mysqli_query($conn,$query);
	if(mysqli_num_rows($tempOfRecommend)!=0)
		$numbOfRecommend=mysqli_fetch_array($tempOfRecommend);
	else
		$numbOfRecommend[]=0;
		
		
		?>
		
		
	}
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link href="<?php echo $path; ?>style/p_style.css" rel="stylesheet"></link>
<script type="text/javascript" src="<?php echo $path; ?>classes/jquery.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Practitioner: Upload your patient list and email your patients</title>

<link href="style/apple.css" rel="stylesheet" type="text/css" />
<link href="WotmedSkyeStyle.css" rel="stylesheet" type="text/css" />

</head>

<body>
<?php include $path."includes/p_header.php"; ?>
<?php
if($row['ISFACILITATOR'] == 1){
	$backLink = "
		<a href='facilitator/cpanel.php'>Back to Your Control Panel</a>
	";
	include $path."facilitator/cpanelHeader.php"; 
}
else{
	$backLink = "
		<a href='cpanel.php'>Back to Your Control Panel</a>
	";
	include $path."cpanelHeader.php"; 
}
?>
<div class='lfloat' style='width:98%'>
	<div class="stdWrapper" onMouseOver="this.style.background='white';"onmouseout="this.style.background='#F0F0F0';">
	  <p><span class="PractitionerBody"><a href="EmailYourPatients.php"><img src="images/ContactsImportIcon.png" alt="" width="72" height="72" /></a><a href="EmailYourPatients.php" class="hyperlinks">Bulk Import and Email all your Patients</a></span></p>
	  <h1 class="PractitionerMainText">Send an automated email to all your past and current patients encouraging them to join Wotmed to recommend and review you</h1>
	  <p class="PractitionerMainText">When you are ready please click to email your patients.</p>
	  <p>&nbsp;</p>
	  <p>
      
      <?php

      $participant_number = $row[0];
      $practitioner_number = $rowSession[0];


      // get the email addresses from the textarea from the last page

  //    echo ' ';
  //    echo $practitioner_number;
  //    echo ' ';
  //    echo $participant_number;

      $totalEmailAddresses = count($_POST);

      echo "Total Email Addresses Submitted: ";
      echo $totalEmailAddresses;
      echo "<BR><BR>";

      echo htmlspecialchars($_POST['patientEmailAddresses']);


      echo "<BR>";
      echo "<BR>";



      $text = trim($_POST['patientEmailAddresses']);
      $textAr = explode("\n", $text);
      $textAr = array_filter($textAr, 'trim'); // remove any extra \r characters left behind


      // check and make sure that the information submitted actually are email addresses


      // counter for bad email addresses
      $badEmails = 0;

          foreach ($textAr as $line)
          {

                // only add actual email addresses to the database

              // remove all illegal characters from email
              $email = filter_var($line, FILTER_SANITIZE_EMAIL);

              // validate email
      if (!filter_var($email, FILTER_VALIDATE_EMAIL) === false)
      {

          $query = "INSERT INTO CLIENTLIST(CLIENTLIST_NUMBER, PRACTITIONER_NUMBER, CLIENTEMAILADDRESS)
          VALUES (DEFAULT, '$practitioner_number','$email')";
          mysqli_query($conn, $query);
      }
              else
              {
                  $badEmails++;

              }

          }

      echo "<BR>";
      echo "Email Addresses successfully imported.    ";
      echo "<img src='GreenTick.gif' alt='Success' width='30' height='40' /></a>";

      echo "<BR>";
      echo "<BR>";

      echo "<BR>";

      if ($badEmails > 0)
      {
          echo "Please note that there were problems loading some of your clients email addresses";
          echo "<BR>";
          echo "Number of client email addresses that were not successfully imported: ";
          echo $badEmails;
          echo "<BR>";
          echo "<BR>";
      }



      echo "Click the button below to bulk email your patients";
      echo "<BR>";

      echo "<form action='EmailClientList.php' method='post' enctype='multipart/form-data' name='form1' id='form1'>";

//      echo "<input type='submit' name='Bulk email your Patients' id='Bulk email your Patients' value='Bulk email your Patients' />";
	  
	       echo "<input type='submit' name='Bulk email your Patients' value='Bulk email your Patients' class='form-submit' id='uploadsubmit' />";
 
		   
      echo "</form>";








?>
	  
      
      
      &nbsp;</p>
      
      &nbsp;</p>
    </div>
</div>
<p>&nbsp;</p>
<?php include $path."includes/p_footer.php"; ?>
</body>

</html>
