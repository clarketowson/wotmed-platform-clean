<?php
	
?>
		<script type="text/javascript" src="<?php echo $path; ?>about/about.js"></script>
<div id="about">
	<div id="close">Close</div>
	<h2 align="center"><strong>Our Declaration of Principles</strong></h2>
		  <div align="center"><a title="Help us spread the word" href="mailto:?Subject=Check out Wotmed.com&body=The team at Wotmed believe that all patients globally should have access to affordable health care services regardless of what country they are presently located in."><img src="<?php echo $path; ?>images/word.jpg" height="90"/></a></div>
		  <!-- AddThis Button BEGIN -->
			<div class="addthis_toolbox addthis_default_style " style="margin-left:120px;width:400px; text-align:center;">
			<a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
			<a class="addthis_button_tweet"></a>
			<a class="addthis_button_pinterest_pinit"></a>
			<a class="addthis_counter addthis_pill_style"></a>
			</div><br>
			<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=xa-50fa140e36ced01d"></script>
		  <!-- AddThis Button END -->
		  <strong><a href="mailto:?Subject=Check out Wotmed.com&body=The team at Wotmed believe that all patients globally should have access to affordable health care services regardless of what country they are presently located in.">We believe that all patients globally deserve access to affordable health care services regardless of what country they are presently located in.</a></strong>
          <p>Our highest leadership  principle is <strong>Service to Humanity</strong>. The Wotmed team work together toward a common goal of service to humanity.</p>
<p>The Wotmed team shall provide a global website platform via which patients  located anywhere in the world can connect with and establish relationships with practitioners located anywhere in the world  in a<em> libertarian spirit of freedom, individual liberty and voluntary association with little to no government interference.</em> </p>
          <p>Wotmed believes in the power of the free market. Wotmed shall be an open and transparent global marketplace where the prices of all medical services are published and known by all the participants on the platform.</p>
          <p>The Wotmed platform shall facilitate a global marketplace for the voluntarily establishment of relationships between patients and practitioners  guided by each parties mutual self interest.</p>
          <p>Wotmed   shall enable patients to buy medical services from practitioners globally including the ability to book flights, accommodation and car hire as part of this buying process.          </p>
          <p>Wotmed shall enable practitioners from every country in the world to sell medical services to patients located in every country in the world.</p>
          <p>We will always listen to you our valued users and put you first. Our goal is to make something that you want and value highly. </p>
          <p>&nbsp;</p>
          <table border="0" width="600">
            <tbody><tr>
              <td width="205">Christian Fletcher-Walker&nbsp;&nbsp;&nbsp;</td>
              <td width="385">&nbsp;Clarke Towson</td>
            </tr>
            <tr>
              <td><img src="<?php echo $path; ?>images/LouisSignature27thJan2013.jpg" alt="louissig" align="left" height="88" width="185"></td>
              <td><img src="<?php echo $path; ?>images/WotmedDeclarationOfPrinciples_clip_image004.png" alt="towsonsig" align="left" height="88" width="185"></td>
            </tr>
            <tr>
              <td>Chief Executive Officer&nbsp;&nbsp;</td>
              <td>&nbsp;Chief Technology Officer</td>
            </tr>
          </tbody></table>
          <p>&nbsp; </p>
</div>
