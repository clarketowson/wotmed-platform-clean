$(function() {
	// load the modal window
	$('a.about').click(function(){

		// scroll to top
		document.body.style.overflow= "hidden";
		$('html, body').animate({scrollTop:0}, 'fast');

		// before showing the modal window, reset the form incase of previous use.
		//$('.success, .error').hide();

		/* Reset all the default values in the form fields
		$('#name').val('Your name');
		$('#email').val('Your email address');
		$('#comment').val('Enter your comment or query...');*/

		//show the mask and about divs
		$('#mask').show().fadeTo('', 0.7);
		$('div#about').fadeIn();

		// stop the modal link from doing its default action
		return false;
	});

	// close the modal window is close div or mask div are clicked.
	$('div#close, div#mask').click(function() {
		$('div#about, div#mask').stop().fadeOut('slow');
		document.body.style.overflow= "visible";
	});

});