<?php

SESSION_START();		// Note - we need the session start function to ensure we can pass the value of our buyers shopping cart into buyerTransactionAmount
						// from the previous page


include "../../includes/connect.php";
include "../../includes/functions.php";



									
						
// get the practitioner practitioner_number					Dan Andrews = 1
// get the practitioners email address participant_email 	Dan Andrews = danandrewsdentist@yahoo.com.au

// get the patient participant_number						Clarke Towson = 2132
// get the patients email address							Clarke Towson = clarketowson@wotmed.com

// specify the Wotmed payments email address for our 3% fee
// ensure this is emailed onto the Wotmed Leadership Team

$wotmedPaymentsEmailAddress = 'paypalpayments@wotmed.com';

$wotmedPatientNumber = $_SESSION['id'];														//  this is the logged in patient session id 2132 in Clarke's case
$wotmedPatientEmailAddress = $_SESSION['EMAILADDRESS'];										//  patients email address clarketowson@wotmed.com in Clarke's case
$wotmedPractitionerBusinessName = $_SESSION['PRACTITIONERBUSINESSNAME'];					//  Daniel Andrews
$wotmedPractitionerNumber = $_SESSION['PRACTITIONERNUMBER'];								//  1 in Dan Andrews case
$wotmedPractitionerBusinessEmailAddress = $_SESSION['PRACTITIONEREMAILADDRESS'];			//  danandrewsdentist@yahoo.com.au

						


// TowsonC August 2nd 2012
// The Wotmed.com Transaction Percentage Fee. This fee is 3 percent of the transacted amount which the patient pays
// so in the example - the buyer pays $12,500 for a Rhinoplasty and Wotmed.com gets 3 percent of this amount which is $375
//
// This code interfaces with Paypal.  It will connect to Paypal and allow a Parallel transaction to occur with the following details:
// 
// 1) Practitioner receives a payment for an Advertised Surgery after the Patient clicks the Buy Now Paypal button on the Practitioners Profile
// 2) Wotmed.com receives a 3 percent fee off the transaction.  This fee is for being a net weaver and for putting the Practitioner and patient together on the Wotmed.com Platform
// 3) The Practitioner is the PRIMARYRECEIVER and therefore is responsible for paying the PayPal transaction fee.  This is like how on ebay that the sellers are charged when selling
// 4) http://wwww.wotmed.com/PreapprovalCancelHandler.html 		This page has been created for when a Patient cancels a transaction
// 5) http://wwww.wotmed.com/PreapprovalReturnHandler.html		This page has been created for the return url
// There are a number of variables in this code which need to be set programatically when this code is in live use:
// 
// $buyerTransactionAmount		will be set to the value of each PayPal button on the Practitioners Profiles
// $receiverInvoiceIdArray		will be set to the patient and practitioners IDs from the database
// 
// 

$wotmedPercentageFee = "0.03";
// $buyerTransactionAmount = "800";

// get the amount from the shopping cart from chart.php which has the value stored is $value


$buyerTransactionAmount = $_SESSION['CARTTOTAL'];
		
    



// calculate the Wotmed.com Buyer Transaction Fee.  This is the amount that Wotmed.com earns from each Buyer who pays for surgery purchased from
// a Practitioner

$wotmedFinalPercentageDollarCharge = ($buyerTransactionAmount * $wotmedPercentageFee);

// set the patient to pay the paypal transaction fees
// perhaps it should be the PRACTITIONER who pays this?

$feesPayable = PRIMARYRECEIVER;	

// calculate remainder amount for Practitioner

$practitionerTransactionRemainderCharge = ($buyerTransactionAmount - $wotmedFinalPercentageDollarCharge);

//-------------------------------------------------
// When you integrate this code
// look for TODO as an indication
// that you may need to provide a value or take action
// before executing this code
//-------------------------------------------------

require_once ("paypalplatform.php");


// ==================================
// PayPal Platform Parallel Payment Module
// ==================================

// Request specific required fields
$actionType			= "PAY";
$cancelUrl			= "http://www.platform.wotmed.com/profileSurgeryPaymentFailure.php";	// TODO - If you are not executing the Pay call for a preapproval,
												//        then you must set a valid cancelUrl for the web approval flow
												//        that immediately follows this Pay call
$returnUrl			= "http://www.platform.wotmed.com/profileSurgeryPaymentSuccess.php";	// TODO - If you are not executing the Pay call for a preapproval,
												//        then you must set a valid returnUrl for the web approval flow
												//        that immediately follows this Pay call
$currencyCode		= "USD";

// A parallel payment can be made among two to six receivers
// TODO - specify the receiver emails
//        remove or set to an empty string the array entries for receivers that you do not have

// $receiverEmailArray	= array('doctor_1343877949_per@wotmed.com','wot_1343877381_biz@wotmed.com','','','','');

$receiverEmailArray	= array($wotmedPractitionerBusinessEmailAddress, $wotmedPaymentsEmailAddress,'','','','');


// TODO - specify the receiver amounts as the amount of money, for example, '5' or '5.55'
//        remove or set to an empty string the array entries for receivers that you do not have
// $receiverAmountArray = array(
//		'375',
//		'12500',
//		'',
//		'',
//	'',
//		''
//		);


// This array lists the payment shares to be distributed to each receiver as
// specified within the $receiverEmailArray

$receiverAmountArray = array($practitionerTransactionRemainderCharge, $wotmedFinalPercentageDollarCharge,'','','','');


// for parallel payment, no primary indicators are needed, so set empty array
$receiverPrimaryArray = array();

// TODO - Set invoiceId to uniquely identify the transaction associated with each receiver
//        set the array entries with value for receivers that you have
//		  each of the array values must be unique
$receiverInvoiceIdArray = array(
		$wotmedPatientNumber,
		$wotmedPractitionerNumber,
		'',
		'',
		'',
		''
		);

// Request specific optional fields
//   Provide a value for each field that you want to include in the request, if left as an empty string the field will not be passed in the request
$senderEmail					= $wotmedPatientEmailAddress;		// TODO - If you are executing the Pay call against a preapprovalKey, you should set senderEmail
											//        It is not required if the web approval flow immediately follows this Pay call
$feesPayer						= "";
$ipnNotificationUrl				= "";
$memo							= "You have received a payment for surgery from this patient via the Wotmed Platform";		// maxlength is 1000 characters
$pin							= "";		// TODO - If you are executing the Pay call against an existing preapproval
											//        the requires a pin, then you must set this
$preapprovalKey					= "";		// TODO - If you are executing the Pay call against an existing preapproval, set the preapprovalKey here
$reverseAllParallelPaymentsOnError	= "";	// TODO - Set this to "true" if you would like each parallel payment to be reversed if an error occurs
											//        defaults to "false" if you don't specify
$trackingId						= generateTrackingID();	// generateTrackingID function is found in paypalplatform.php

//-------------------------------------------------
// Make the Pay API call
//
// The CallPay function is defined in the paypalplatform.php file,
// which is included at the top of this file.
//-------------------------------------------------
$resArray = CallPay ($actionType, $cancelUrl, $returnUrl, $currencyCode, $receiverEmailArray,
						$receiverAmountArray, $receiverPrimaryArray, $receiverInvoiceIdArray,
						$feesPayer, $ipnNotificationUrl, $memo, $pin, $preapprovalKey,
						$reverseAllParallelPaymentsOnError, $senderEmail, $trackingId
);

$ack = strtoupper($resArray["responseEnvelope.ack"]);
if($ack=="SUCCESS")
{
	if ("" == $preapprovalKey)
	{
		// redirect for web approval flow
		$cmd = "cmd=_ap-payment&paykey=" . urldecode($resArray["payKey"]);
		RedirectToPayPal ( $cmd );
	}
	else
	{
		// payKey is the key that you can use to identify the result from this Pay call
		$payKey = urldecode($resArray["payKey"]);
		// paymentExecStatus is the status of the payment
		$paymentExecStatus = urldecode($resArray["paymentExecStatus"]);
	}
} 
else  
{
	//Display a user friendly Error on the page using any of the following error information returned by PayPal
	//TODO - There can be more than 1 error, so check for "error(1).errorId", then "error(2).errorId", and so on until you find no more errors.
	$ErrorCode = urldecode($resArray["error(0).errorId"]);
	$ErrorMsg = urldecode($resArray["error(0).message"]);
	$ErrorDomain = urldecode($resArray["error(0).domain"]);
	$ErrorSeverity = urldecode($resArray["error(0).severity"]);
	$ErrorCategory = urldecode($resArray["error(0).category"]);
	
	echo "Preapproval API call failed. ";
	echo "Detailed Error Message: " . $ErrorMsg;
	echo "Error Code: " . $ErrorCode;
	echo "Error Severity: " . $ErrorSeverity;
	echo "Error Domain: " . $ErrorDomain;
	echo "Error Category: " . $ErrorCategory;
	
	
	//
	//  Error testing code for amounts
	// 
	
//	echo "<br>";
//	echo "<br>";
//	echo "Practitioner Transaction Remainder is: " . $practitionerTransactionRemainderCharge;
//	echo "<br>";
//	echo "Wotmed Final percentage Dollar Amount Charge is: " . $wotmedFinalPercentageDollarCharge;
//	echo "<br>";
//	echo "Wotmed Final percentage Dollar Charge is: " . $wotmedFinalPercentageDollarCharge;

}

?>
