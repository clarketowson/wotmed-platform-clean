<?php
	$indicesImage="";
	switch($row['FREEDOM']){
		case 1: $indicesImage="<img src='images/indicesGraphics/FreedomHouse/PartlyFree.jpg' alt='' width='128' height='52'>";
break;
		case 2: $indicesImage="<img src='images/indicesGraphics/FreedomHouse/Free.jpg' alt='' width='133' height='55'>";
break;
		default: $indicesImage="<img src='images/indicesGraphics/FreedomHouse/NotFree.jpg' alt='' width='139' height='55'>";
		break;
	}
?>
	<div id="freedomIcon" class="lfloat"><img src="images/indicesGraphics/FreedomHouse/FreedomHouseIcon.jpg" height='25px'>
		<div id="freedomIconPopUp" class="Indices">
		  <h2>
			Wotmed Indices
		  </h2>
		  <p><img src="images/indicesGraphics/FreedomHouse/FreedomHouseIcon.jpg" alt="" width="55" height="167"></p>
	<p><strong>Freedom in the World Index</strong></p>
	<p>The Freedom in the World is a yearly survey and report by U.S. based Freedom House that attempts to measure the degree of democracy&nbsp; and political freedom in every nation and significant disrupted territories around the world.</p>
	<p>The Freedom in the World index can have one of the following assessments:</p>
	<p><img src="images/indicesGraphics/FreedomHouse/Free.jpg" alt="" width="185" height="45"><img src="images/indicesGraphics/FreedomHouse/PartlyFree.jpg" alt="" width="197" height="48"><img src="images/indicesGraphics/FreedomHouse/NotFree.jpg" alt="" width="205" height="50"><br>
	</p>
	<p>The Freedom in the World ranking for <strong><?php echo $row['COUNTRYNAME']; ?></strong> where this Practitioner is located is listed as <strong><?php echo $indicesImage; ?></strong></p>
	<p>Should you travel to <strong><?php echo $row['COUNTRYNAME']; ?></strong> to visit this Practitioner for surgery you should be aware of this democracy and political freedom assessment.</p>
	<p>&nbsp;</p>
		</div>
    </div>
