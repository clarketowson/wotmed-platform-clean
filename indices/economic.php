<?php
	$indicesImage="";
	switch($row['ECONOMIC']){
		case 1: $indicesImage="<img src='images/indicesGraphics/EconomicFreedom/MostlyUnfree.jpg' alt='' width='128' height='52'>";
break;
		case 2: $indicesImage="<img src='images/indicesGraphics/ModeratelyFree.jpg' alt='' width='133' height='55'>";
break;
		case 3: $indicesImage="<img src='images/indicesGraphics/EconomicFreedom/MostlyFree.jpg' alt='' width='133' height='55'>";
break;
		case 4: $indicesImage="<img src='images/indicesGraphics/EconomicFreedom/Free.jpg' alt='' width='133' height='55'>";
break;
		default: $indicesImage="<img src='images/indicesGraphics/EconomicFreedom/Free.jpg' alt='' width='139' height='55'>";
		break;
	}
?>
	<div id="economicIcon" class="lfloat"><img src="images/indicesGraphics/EconomicFreedomIndexIcon.jpg" height='25px' width='25px'>
		<div id="economicIconPopUp" class="Indices">
		  <h2>
			Wotmed Indices
		  </h2>
		  <p><img src="images/indicesGraphics/EconomicFreedomIndexIcon.jpg" alt="" width="132" height="61"></p>
	<p><strong>Index of Economic Freedom</strong></p>
	<p>The Index of Economic Freedom is a series of 10 economic measurements created by The Heritage Foundation and The Wall Street Journal. The index measures the degree of economic freedom in the world's nations.</p>
	<p>"Basic institutions that protect the liberty of individuals to pursue their own economic interests result in greater prosperity for the larger society"</p>
	<p>The Index of Economic Freedom for a country has one of the following assessments:</p>
	<p><img src="images/indicesGraphics/EconomicFreedom/Free.jpg" alt="" width="127" height="50"><img src="images/indicesGraphics/EconomicFreedom/MostlyFree.jpg" alt="" width="127" height="50"><img src="images/indicesGraphics/ModeratelyFree.jpg" alt="" width="125" height="50"><img src="images/indicesGraphics/EconomicFreedom/MostlyUnfree.jpg" alt="" width="126" height="50"><img src="images/indicesGraphics/EconomicFreedom/Repressed.jpg" alt="" width="126" height="50"></p>
	<p>The Economic Freedom ranking for <strong><?php echo $row['COUNTRYNAME']; ?></strong> where this Practitioner is located is listed as <strong><?php echo $indicesImage; ?></strong></p>
	<p>Should you travel to <strong><?php echo $row['COUNTRYNAME']; ?></strong> to visit this Practitioner for surgery you should be aware of this Economic Freedom assessment.</p>
	<p>&nbsp;</p>
		</div>
    </div>
