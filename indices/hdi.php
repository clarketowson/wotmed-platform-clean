<?php
	$indicesImage="";
	$hdi=$row['HDI'];
	if($hdi>=0.793)
		$indicesImage="<img src='images/indicesGraphics/HDI/VeryHigh.jpg' alt='' width='128' height='52'>";
	elseif($hdi>=0.698)
		$indicesImage="<img src='images/indicesGraphics/HDI/High.jpg' alt='' width='128' height='52'>";
	elseif($hdi>=0.522)
		$indicesImage="<img src='images/indicesGraphics/HDI/Medium.jpg' alt='' width='128' height='52'>";
	elseif($hdi>=0.286)
		$indicesImage="<img src='images/indicesGraphics/HDI/Low.jpg' alt='' width='128' height='52'>";
	else
		$indicesImage="<img src='images/indicesGraphics/HDI/DataUnavailable.jpg' alt='' width='128' height='52'>";
?>
	<div id="hdiIcon" class="lfloat"><img src="images/indicesGraphics/HDI/UNLogo.jpg" height='25px'>
		<div id="hdiIconPopUp" class="Indices">
		  <h2>
			Wotmed Indices
		  </h2>
		  <p><img src="images/indicesGraphics/HDI/UNLogo.jpg" alt="" width="78" height="58" /></p>
	<p><strong>Human Development Index</strong></p>
	<p>The Human Development Index (HDI) is a comparative measure of life expectancy, literacy, education, standards of living, and quality of life for countries worldwide. It is a standard means of measuring well-being. It is used to distinguish whether the country is a developed, a developing or an underdeveloped country, and also to measure the impact of economic policies on quality of life.</p>
	<p>Countries fall into four broad human development categories as listed below:</p>
	<p><img src="images/indicesGraphics/HDI/VeryHigh.jpg" alt="" width="127" height="50" /><img src="images/indicesGraphics/HDI/High.jpg" alt="" width="127" height="50" /><img src="images/indicesGraphics/HDI/Medium.jpg" alt="" width="126" height="50" /><img src="images/indicesGraphics/HDI/Low.jpg" alt="" width="126" height="50" /></p>
	<p><img src="images/indicesGraphics/HDI/DataUnavailable.jpg" alt="" width="127" height="50" /></p>
	<p>The Human Development Index ranking for <strong><?php echo $row['COUNTRYNAME']; ?></strong> where this Practitioner is located is listed as 
	<?php echo $indicesImage; ?>
	</p>
	<p>Should you travel to <strong><?php echo $row['COUNTRYNAME']; ?></strong> to visit this Practitioner for surgery you should be aware of this Human Development assessment.</p>
		</div>
</div>
