<?php
	$indicesImage="";
	switch($row['DEMOCRACY']){
		case 1: $indicesImage="<img src='images/indicesGraphics/Democracy/tmp.jpg' alt='' width='128' height='52'>";
break;
		case 2: $indicesImage="<img src='images/indicesGraphics/Democracy/FlawedDemocracy.jpg' alt='' width='133' height='55'>";
break;
		case 3: $indicesImage="<img src='images/indicesGraphics/Democracy/Democracy.jpg' alt='' width='133' height='55'>";
break;
		default: $indicesImage="<img src='images/indicesGraphics/Democracy/AuthoritarianRegime.jpg' alt='' width='139' height='55'>";
		break;
	}
?>
	<div id="democracyIcon" class="lfloat"><img id="democracyIcon" src="images/indicesGraphics/Democracy/DemocracyIcon.jpg" height='25px' width='25px'>
		<div id="democracyIconPopUp" class="Indices">
		  <h2>
			Wotmed Indices
		  </h2>
		  <p><img src="images/indicesGraphics/Democracy/DemocracyIcon.jpg" alt="" width="103" height="52"></p>
	<p><strong>Democracy Index</strong></p>
	<p>The democracy index is an index compiled by the Economist Intelligence Unit that measures the state of democracy in 167 countries around the world. It is based on indicators grouped in five different categories: electoral process and pluralism, civil liberties, functioning of government, political participation and political culture.</p>
	<p>The Democracy Index for a country has one of the following assessments:</p>
	<p><img src="images/indicesGraphics/Democracy/Democracy.jpg" alt="" width="159" height="51"><img src="images/indicesGraphics/Democracy/FlawedDemocracy.jpg" alt="" width="158" height="51"><img src="images/indicesGraphics/Democracy/tmp.jpg" alt="" width="157" height="51"><img src="images/indicesGraphics/Democracy/AuthoritarianRegime.jpg" alt="" width="158" height="51"></p>
	<p>The Democracy Index  ranking for <strong><?php echo $row['COUNTRYNAME']; ?></strong> where this Practitioner is located is listed as <strong><?php echo $indicesImage; ?></strong></p>
	<p>Should you travel to <strong><?php echo $row['COUNTRYNAME']; ?></strong> to visit this Practitioner for surgery you should be aware of this democracy assessment.</p>
	<p>&nbsp;</p>
		</div>
    </div>
