<?php
	$indicesImage="";
	switch($row['PRESS']){
		case 1: $indicesImage="<img src='images/indicesGraphics/Press/Difficult.jpg' alt='' width='128' height='52'>";
break;
		case 2: $indicesImage="<img src='images/indicesGraphics/Press/Problems.jpg' alt='' width='133' height='55'>";
break;
		case 3: $indicesImage="<img src='images/indicesGraphics/Press/Satisfactory.jpg' alt='' width= '130' height='52'>";
break;
		case 4: $indicesImage="<img src='images/indicesGraphics/Press/Good.jpg' alt='' width='133' height= '52'>";
break;
		default: $indicesImage="<img src='images/indicesGraphics/Press/Serious.jpg' alt='' width='139' height='55'>";
		break;
	}
?>
	<div id="pressIcon" class="lfloat"><img src="images/indicesGraphics/Press/PressIcon.jpg" height='25px'>
		<div id="pressIconPopUp" class="Indices">
		  <h2>
			Wotmed Indices
		  </h2>
		  <p>
			<img src="images/indicesGraphics/Press/PressIcon.jpg" alt="" width="97"
			height="78">
		  </p>
		  <p>
			<strong>Press Freedom Index</strong>
		  </p>
		  <p>
			The Press Freedom Index is an annual ranking of countries compiled and
			published by Reporters Without Borders based upon the organisations
			assessment of the countries press freedom records in the previous year.
		  </p>
		  <p>
			The Press Freedom Index can have one of the following assessments:
		  </p>
		  <p>
			<img src="images/indicesGraphics/Press/Good.jpg" alt="" width="133" height=
			"52"><img src="images/indicesGraphics/Press/Satisfactory.jpg" alt="" width=
			"130" height="52"><img src="images/indicesGraphics/Press/Problems.jpg" alt=""
			width="133" height="55"><img src="images/indicesGraphics/Press/Difficult.jpg"
			alt="" width="128" height="52"><img src=
			"images/indicesGraphics/Press/Serious.jpg" alt="" width="139" height="55">
		  </p>
		  <p>
			The Press Freedom Index in <strong><?php echo $row['COUNTRYNAME']; ?></strong> where this Practitioner is located is listed as
			<strong><?php echo $indicesImage; ?></strong>
		  </p>
		  <p>
			Should you travel to <strong><?php echo $row['COUNTRYNAME']; ?></strong> to visit this Practitioner for surgery you should
			be aware of the status of the press
		  </p>
		</div>
	</div>
