<?php
session_start();
include("includes/connect.php");
include("includes/functions.php");
if(isset($_POST['firstname'])){
	$birthDate=mysqli_real_escape_string($conn,$_POST['birthYear'] . "-" . $_POST['birthMonth'] . "-" . $_POST['birthDay']);
	$status=register($conn,mysqli_real_escape_string($conn,$_POST['firstname']),mysqli_real_escape_string($conn,$_POST['lastname']),mysqli_real_escape_string($conn,$_POST['reg_email__']),mysqli_real_escape_string($conn,$_POST['reg_passwd__']),mysqli_real_escape_string($conn,$_POST['sex']),mysqli_real_escape_string($conn,$_POST['sexb']),$birthDate);
	if($status=='success'){
		$reg=$_POST['reg'];
		$redLink="";
		$query="SELECT PARTICIPANT_NUMBER FROM PARTICIPANT WHERE EMAILADDRESS='" . $_POST['reg_email__'] . "'";
		$row=mysqli_fetch_array(mysqli_query($conn,$query));
		$id=$row[0];
		$_SESSION['id']=$id;
		if($reg=="practitioner"){
			if(isset($_POST['facilitator']))
			{
				$isFacilitator=1;
				
				// Email the Wotmed Team and advise them we have a new SURGERY FACILITATOR
				// Clarke Towson code: May 23rd 2014
				//include('emailWotmedTeamSurgeryFacilitatorSignUp.php');
				// update for HTML Email June 10th 2014
				
				include('HTMLEmailTemplate/WotmedEmailTemplate/Facilitator.php');
				
				echo "<img src ='images/WotmedSurgeryFacilitatorEmailGraphic.jpg'>";
				$redLink="facilitator";
			}
			else{
				$isFacilitator=0;
				
				// Email the Wotmed Team and advise them we have a new PRACTITIONER
				// Clarke Towson code: May 23rd 2014
				// include('emailWotmedTeamPractitionerSignUp.php');
				// update for HTML Email June 10th 2014
				
				include('HTMLEmailTemplate/WotmedEmailTemplate/Practitioner.php');
				
				echo "<img src ='images/WotmedPractitionerAccountCreated.jpg'>";
				$redLink="practitioner_profile.php";
			}
			loginPractitioner($conn,$id,$isFacilitator);
			if($isFacilitator==1){
				$pracId = getPractitionerId($conn,$id);
				$query="INSERT INTO SERVICES (PRACTITIONER_NUMBER,MASTERSERVICE_NUMBER,PRICE) VALUES ('$pracId','380','0');";
				mysqli_query($conn,$query);
			}
		}else{
			
			// Email the Wotmed Team and advise them we have a new PATIENT
			// Clarke Towson code: May 23rd 2014
			// update for HTML Email June 10th 2014
			
			include('HTMLEmailTemplate/WotmedEmailTemplate/Patient.php');
			
			//	include('emailWotmedTeamPatientSignUp.php');
				
			echo "<img src ='images/WotmedParticipantAccountCreated.jpg'>";
			$redLink="profile.php";
		}
		?>
		<script language="javascript"> 
			setTimeout("location.href = '<?php echo $redLink; ?>';",5000);
		</script> <?php
		?><?php
	}else{
		redirect('index.php');
	}
}/*
if(isset($_SESSION['practitioner_id'])){
	?>
	<script language="javascript"> 
		<?php echo "window.location = 'practitioner_profile.php'";?>
	</script> <?php
	?><?php
}
else
	if(isset($_SESSION['id'])){
		?>
		<script language="javascript"> 
			<?php echo "window.location = 'profile.php'";?>
		</script> <?php
		?><?php
	}
?>*/
