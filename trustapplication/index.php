<?php
	session_start();
	include "../includes/connect.php";
	include "../includes/functions.php";
	$path="../";
	include $path."classes/SimpleImage.php";
	if(isset($_SESSION['id'])){
			$rowSession=getParticipantDetail($conn,$_SESSION['id']);
	}
	if($rowSession['PROFILEPHOTO']==""){
			$ppFileNameSession="blankSilhouetteMale.png";
	}else{
			$ppFileNameSession=$rowSession['PROFILEPHOTO'];
	}
	
	if(isset($_SESSION['practitioner_id'])){
		$row=getPractitionerDetail($conn,$_SESSION['id']);
	}
	 
	//Parser for Add Photo Form
	if (isset($_POST['FIRSTNAME']))
	{
		
		
		
	// Email the Wotmed Management Team and advise them that a Practitioner has applied for trusted practitioner status
		
	//	include('emailWotmedTeamPractitionerTrustApplicationSubmitted.php');
		
	
$to      = 'practitionertrustapplication@wotmed.com';
$subject = 'A Practitioner has applied for Wotmed Trusted Practitioner status.  Please review this application as soon as possible.';
$message = 'Dear Wotmed Management Team.  Please note that a Wotmed Practitioner has just applied for Wotmed Trusted Practitioner status.  Please review this application by clicking here:  www.wotmed.com/trustapplication/review.php';
$headers = 'From: practitionertrustapplication@wotmed.com' . "\r\n" .
    'Reply-To: practitionertrustapplication@wotmed.com' . "\r\n" .
    'X-Mailer: PHP/' . phpversion();

mail($to, $subject, $message, $headers);
		
		
		
		
			$header= Array( 'TRUSTAPPLICATION_NUMBER', 'PRACTITIONER_NUMBER', 'FIRSTNAME', 'SURNAME', 'DOB', 'GENDER', 'PASSPORTNUMBER', 'PASSPORTCOUNTRY', 'PASSPORTISSUEDATE', 'FULLNAME', 'SEX', 'PASSPORTSCAN', 'DRIVERSLICENSENUMBER', 'ADDRESS', 'DRIVERSLICENSEEXP', 'DRIVERSLICENSESCAN', 'UNINAME', 'UNICERTIFICATESCAN', 'BOARDCERTNAME', 'BOARDCERTSCAN', 'DETAIL', 'OTHERSCAN1', 'OTHERSCAN2');
			$practitionerId=$row['PRACTITIONER_NUMBER'];
			$textValue=$_POST;
			$query="INSERT INTO TRUSTAPPLICATION (PRACTITIONER_NUMBER";
			for($i=2;$i<count($header);$i++)
				$query.=", ".$header[$i];
			$query.=",STATUS) VALUES ('" . $practitionerId . "'";
			for($i=2;$i<count($header);$i++){
				//file upload
				if(($i==11)||($i==15)||($i==17)||($i==19)||($i==21)||($i==22)){
					if(!isset($_FILES[$header[$i]]['name'])){
						$textValue[$header[$i]]="";
					}else{
						if (($_FILES[$header[$i]]["type"] == "image/jpeg") || ($_FILES[$header[$i]]["type"] == "image/png") || ($_FILES[$header[$i]]["type"] == "image/gif") || ($_FILES[$header[$i]]["type"] == "image/bmp"))
						{
							$type="";
							switch ($_FILES[$header[$i]]["type"])
							{
								case "image/jpeg":
									$type=".jpg";
									break;
								case "image/png":
									$type=".png";
									break;
								case "image/gif":
									$type=".gif";
									break;
								case "image/bmp":
									$type=".bmp";
									break;
							}
							$query1="SELECT COUNT(*) FROM TRUSTAPPLICATION WHERE PRACTITIONER_NUMBER = '$practitionerId'";
							$data = mysqli_query($conn,$query1);
							$rows = mysqli_fetch_array($data);
							$name=$header[$i]."_".$practitionerId."_".($rows[0]+1);
							$image = new SimpleImage();
							$image->load($_FILES[$header[$i]]['tmp_name']);
							if($image->getWidth()>$image->getHeight()){
								if($image->getWidth()>1400)
									$image->resizeToWidth(1400);
							}else{
								if($image->getHeight()>1400)
									$image->resizeToHeight(1400);
							}
							$image->save('Uploaded Documents/' . $name . $type);
							$textValue[$header[$i]]=$name . $type;
						}
						else{
							$textValue[$header[$i]]="";
						}
					}
				}
				//gender
				if($i==5){
					$textValue[$header[$i]]=$textValue[$header[$i]][1];
				}
				$query.=", '".$textValue[$header[$i]] . "'";
			}
			$query.=",0)";
			
			
		//	msgBox($query);
		
			
			//check current application
			$pending=true;
			$trustapplication=getTrustApplication($conn,$practitionerId,$pending);
			if($trustapplication!=null){
				msgBox("Sorry, you cannot submit another application while waiting for your pending application.  A member of the Wotmed team will be in contact with you shortly.");
			}else{
				if($row['TRUSTED']!=1)
					mysqli_query($conn,$query);
			}
			
			//header("location:index.php");
			//echo $query;
	}
	 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
	<link href="<?php echo $path; ?>style/p_style.css"
	rel="stylesheet"></link>

      <link rel="stylesheet" type="text/css" href="../signup.css">

	<meta http-equiv="Content-Type"
	content="text/html; charset=utf-8" />
	<title>Wotmed Practitioner Trust Application</title>
	<script type="text/javascript" src="<?php echo $path; ?>classes/jquery.js"></script>
	<script type="text/javascript" src="<?php echo $path; ?>classes/jquery.validation.js"></script>
	<script type="text/javascript">function back() {
	window.location = "../profile.php" }
	
			$(document).ready(function(){
				$("#form1").validate();
			});
	</script>
  </head>
  <body>
	<?php include $path."includes/p_header.php"; ?>
	<div style="margin-left:100px;margin-right:100px;">
	<p>
       
      </p>
      <h2><strong>Wotmed Trusted Practitioner application form<img src="../images/GreenTick.jpg" width="38" height="43" alt="Trusted Practitioner Green Tick" /></strong>      </h2>
      <p>&nbsp;</p>
      <p>Please use this page to upload your identification, qualification and
        education details. Wotmed asks you for these details so that we can effectively verify your
        identity as a practitioner. This is important to give your patients and prospective patients a level of trust
        in you. </p>
      <p>
        Please provide as many of the detail below as possible.
      Once you submit these details a member of the Wotmed management team will review them. If your application is successful you will receive an email and your Wotmed profile will be updated to include a green tick next to your name.</p>
      <p>&nbsp;</p>
      <form action="" method="post" enctype="multipart/form-data" id="form1" name="form1">
        <table border="0">
          <tbody>
            <tr>
              <td width="293">
                Practitioner First Name
              </td>
              <td width="311">
                <label><input name="FIRSTNAME" size="40"
                type="text" /></label>
              </td>
            </tr>
            <tr>
              <td>
                Practitioner Surname
              </td>
              <td>
                <input name="SURNAME" size="40" type="text" />
              </td>
            </tr>
            <tr>
              <td>
                Date of Birth (YYYY-MM-DD)
              </td>
              <td>
                <input name="DOB" size="20" type="text" />
              </td>
            </tr>
            <tr>
              <td>
                Your Sex at birth*
              </td>
              <td>
                <input name="GENDER[]" size="20" type="text" />
              </td>
            </tr>
            <tr>
              <td>
                Your Sex now*
              </td>
              <td>
                <input name="GENDER[]" size="20" type="text" />
              </td>
            </tr>
            <tr>
              <td>
                Passport Document Number<img src=
                "<?php echo $path; ?>images/passport_icon.png" alt="" height="32" width=
                "32" />
              </td>
              <td>
                <input name="PASSPORTNUMBER" size="30" type="text" />
              </td>
            </tr>
            <tr>
              <td>
                Passport Country of Issue
              </td>
              <td>
                <input name="PASSPORTCOUNTRY" size="40" type=
                "text" />
              </td>
            </tr>
            <tr>
              <td>
                Passport Issue Date
              (YYYY-MM-DD)
              </td>
              <td>
                <input name="PASSPORTISSUEDATE" size="20" type="text" />
              </td>
            </tr>
            <tr>
              <td>
                Your full name as it appears on your passport
              </td>
              <td>
                <input name="FULLNAME" size="40" type=
                "text" />
              </td>
            </tr>
            <tr>
              <td>
                Your Sex as it appears on your passport
              </td>
              <td>
                <input name="SEX" size="20" type="text" />
              </td>
            </tr>
            <tr>
              <td>
                Passport Scanned Image
              </td>
              <td>
                <input name="PASSPORTSCAN" id="PASSPORTSCAN" type="file" />
              </td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td>
                Drivers Licence Number<img src=
                "<?php echo $path; ?>images/faq_picto_driving_licence.jpg" alt="" height=
                "35" width="35" />
              </td>
              <td>
                <input name="DRIVERSLICENSENUMBER" size="30" type="text" />
              </td>
            </tr>
            <tr>
              <td>
                Your address as it appears on your drivers licence
              </td>
              <td>
                <input name="ADDRESS" size="40" type=
                "text" />
              </td>
            </tr>
            <tr>
              <td>
                Drivers licence expiry date
              (YYYY-MM-DD)</td>
              <td>
                <input name="DRIVERSLICENSEEXP" size="20" type="text" />
              </td>
            </tr>
            <tr>
              <td>
                Drivers Licence Scanned Image
              </td>
              <td>
                <input name="DRIVERSLICENSESCAN" id="DRIVERSLICENSESCAN" type="file" />
              </td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td>
                University Qualification Name<img src=
                "<?php echo $path; ?>images/uniicon.jpg" alt="" height="41" width="41" />
              </td>
              <td>
                <input name="UNINAME" size="30" type=
                "text" />
              </td>
            </tr>
            <tr>
              <td>
                University Qualification Certificate Scanned Image
              </td>
              <td>
                <label><input name="UNICERTIFICATESCAN" id="UNICERTIFICATESCAN" type=
                "file" /></label>
              </td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td>
                Board Certification Name
              </td>
              <td>
                <input name="BOARDCERTNAME" size="30" type=
                "text" />
              </td>
            </tr>
            <tr>
              <td>
                Board Certification Certificate Scanned Image
              </td>
              <td>
                <input name="BOARDCERTSCAN" id="BOARDCERTSCAN" type="file" />
              </td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td>
                Any other details that you would like to provide to us which will help us to verify
                your identity?
              </td>
              <td>
                <label>
                <textarea name="DETAIL" id="DETAIL" cols="45" rows="10"></textarea></label>
              </td>
            </tr>
            <tr>
              <td>
                Other Scanned Image 1
              </td>
              <td>
                <input name="OTHERSCAN1" id="OTHERSCAN1" type="file" />
              </td>
            </tr>
            <tr>
              <td>
                Other Scanned Image 2
              </td>
              <td>
                <input name="OTHERSCAN2" id="OTHERSCAN2" type="file" />
              </td>
            </tr>
            <tr>
              <td colspan='2' align='center'>
                <textarea cols='40' rows='6' style="width:400px;">Wotmed Governance Disclaimer

Wotmed asks that you submit your identification, qualification and education details so that we can verify that you are a practitioner. 

You hereby agree that Wotmed may store your first name, surname, date of birth, sex, passport document number, country of issue and issue date, your full name as it appears on your passport, your sex as it appears on your passport, your address as it appears on your licence, your drivers licence expiry date, your university qualification name, your board certification name and any other details that you would like to provide us which helps us to verify your identity as a practitioner.

You herby agree that Wotmed may store a digital copy of your passport, drivers licence, university qualification, board certification and any other scanned images that you may upload.

Wotmed.com will not share the information or scanned documents that you provide us via our governance system with outside sources.</textarea><br/><input type="checkbox" name="agree" class="required"></input> I agree
              </td>
            </tr>
            <tr>
              <td colspan="2" align="center" height="115">
                <p><strong><img src="../images/GreenTick.jpg" width="38" height="43" alt="Trusted Practitioner Green Tick" /></strong></p>
                <p>
             <!--     <input name="Apply" id="Apply" value= -->
                   <input name="Apply" value="Apply for Wotmed Trusted Practitioner Status" type="submit" id='trustapplicationsubmit'/>





                </p>
              </td>
            </tr>
            <tr>
              <td colspan="2" align="left" height="43">
                * Wotmed asks questions about your sex at birth and your sex now due to the large
                number of transgendered participants who we anticipate will be members of our site
              </td>
            </tr>
          </tbody>
        </table>
      </form>
	  <p>
		Please limit the resolution of each document that you upload to Wotmed.  <br/><br/>

		Landscape oriented documents should be limited to a maximum resolution of 1400x1100.  Portrait oriented documents should be limited to a maximum of 1100x1400 resolution.  <br/><br/>

		You can change the orientation and resolution of your scanned documents in most photo editing applications like Photoshop, The Gimp etc.<br/><br/>

		Please also ensure that the size of each document that you upload is limited to 5 Megabytes or less.<br/><br/>

		Please ensure that each document you are uploading is one of the following file types: JPG, PNG, BMP or GIF.<br/><br/>

		It is appreciated if you also crop your documents to remove any excess whitespace / removal of the outer parts of the documents to improve framing and to accentuate the subject matter.<br/><br/>

		<img src="<?php echo $path; ?>images/Landscape1.jpg"/><br/>
		Maximum Resolution: 1400x1100<br/>
		Maximum Document Size: 5 MB <br/><br/>


		<img src="<?php echo $path; ?>images/Portrait1.jpg"/><br/>
		Maximum Resolution: 1100x1400<br/>
		Maximum Document Size: 5 MB<br/><br/>
		
		If you have a written reference or letter of recommendation which is in a document format such as a PDF, Microsoft Word or Text file then please take a "Print Screen" of the document and upload it as an "Other Scanned Image" using the buttons below.  Please ensure the images conform to the guidelines on resolution, document size and image format as explained earlier.<br/><br/>

		Please note that there is a maximum of two (2) written references or letter of recommendations that you may upload to Wotmed using this form.
	  </p>
	</div><?php include $path."includes/p_footer.php"; ?>
  </body>
</html>
