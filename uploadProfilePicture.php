<?php
session_start();
include "classes/SimpleImage.php";
if(isset($_FILES["pp"])){
	$name=$_FILES["pp"]["name"];
	$tmpname=$_FILES["pp"]["tmp_name"];
	$size=$_FILES["pp"]["size"];
	
	if (($_FILES["pp"]["type"] == "image/jpeg") || ($_FILES["pp"]["type"] == "image/png") || ($_FILES["pp"]["type"] == "image/gif"))
	{
		$type="";
		switch ($_FILES["pp"]["type"])
		{
			case "image/jpeg":
				$type=".jpg";
				break;
			case "image/png":
				$type=".png";
				break;
			case "image/gif":
				$type=".gif";
				break;
		}
		$image = new SimpleImage();
		$image->load($_FILES['pp']['tmp_name']);
		$image->save('photos/originals/' . $_SESSION['id'] . $type);
		$image->resizeToHeight(175);
		$image->save('photos/thumbs/' . $_SESSION['id'] . $type);
		
		include "includes/connect.php";
		@mysqli_query($conn,"UPDATE PARTICIPANT SET PROFILEPHOTO='" . $_SESSION['id'] . $type . "' WHERE PARTICIPANT_NUMBER='" . $_SESSION['id'] . "'");
		if(isset($_SESSION['practitioner_id'])){
			?>
			<script language="javascript"> 
				<?php echo "window.location = 'practitioner_profile.php'";?>
			</script>
			<?php
		}
		else
			if(isset($_SESSION['id'])){
				?>
				<script language="javascript"> 
					<?php echo "window.location = 'profile.php'";?>
				</script>
				<?php
			}
	}
	else
	echo "Upload Image Failed";
}

if(isset($_FILES["ppPractitioner"])){
	$name=$_FILES["ppPractitioner"]["name"];
	$tmpname=$_FILES["ppPractitioner"]["tmp_name"];
	$size=$_FILES["ppPractitioner"]["size"];
	
	if (($_FILES["ppPractitioner"]["type"] == "image/jpeg") || ($_FILES["ppPractitioner"]["type"] == "image/png") || ($_FILES["ppPractitioner"]["type"] == "image/gif"))
	{
		$type="";
		switch ($_FILES["pp"]["type"])
		{
			case "image/jpeg":
				$type=".jpg";
				break;
			case "image/png":
				$type=".png";
				break;
			case "image/gif":
				$type=".gif";
				break;
		}
		
		$image = new SimpleImage();
		$image->load($_FILES['ppPractitioner']['tmp_name']);
		$image->save('photos/originals/b_' . $_SESSION['id'] . $type);
		$image->resizeToHeight(175);
		$image->save('photos/thumbs/b_' . $_SESSION['id'] . $type);
		
		include "includes/connect.php";
		@mysqli_query($conn,"UPDATE PRACTITIONER SET PRACTITIONER_BUSINESSLOGO='b_" . $_SESSION['id'] . $type . "' WHERE PARTICIPANT_NUMBER='" . $_SESSION['id'] . "'");
		header("location:practitioner_profile.php");
	}
	else
	echo "Upload Image Failed";
}
?>
