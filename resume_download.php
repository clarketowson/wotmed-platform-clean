<?php
session_start();
include "includes/connect.php";
include "includes/functions.php";
$path="";
include($path."classes/MPDF57/mpdf.php");

if(isset($_GET['target']))
	$target = $_GET['target'];
else
	$target = "en";
if(isset($_GET['id']))
	$id=$_GET['id'];
else
	$id=$_SESSION['id'];

$row=getPractitionerDetail($conn,$id);
$headerDB=array("RESUME_NUMBER","PARTICIPANT_NUMBER","TITLEOBJECTVE","TITLEOBJECTIVESLOGAN","SPECIFICAREASOFEXPERTISE","CLINICALAFFILIATIONS","PE_COMPANYNAME1","PE_COMPANYADDRESS1","PE_YEARFROM1","PE_YEARTO1","PE_ROLEPOSITIONHELD1","PE_KEYACCOMPLISHMENTS1","PE_COMPANYNAME2","PE_COMPANYADDRESS2","PE_YEARFROM2","PE_YEARTO2","PE_ROLEPOSITIONHELD2","PE_KEYACCOMPLISHMENTS2","PE_COMPANYNAME3","PE_COMPANYADDRESS3","PE_YEARFROM3","PE_YEARTO3","PE_ROLEPOSITIONHELD3","PE_KEYACCOMPLISHMENTS3","PE_COMPANYNAME4","PE_COMPANYADDRESS4","PE_YEARFROM4","PE_YEARTO4","PE_ROLEPOSITIONHELD4","PE_KEYACCOMPLISHMENTS4","PE_COMPANYNAME5","PE_COMPANYADDRESS5","PE_YEARFROM5","PE_YEARTO5","PE_ROLEPOSITIONHELD5","PE_KEYACCOMPLISHMENTS5","ED_UNIVERSITYNAME1","ED_UNIVERSITYADDRESS1","ED_QUALIFICATIONEARNED1","ED_YEARFROM1","ED_YEARTO1","ED_UNIVERSITYNAME2","ED_UNIVERSITYADDRESS2","ED_QUALIFICATIONEARNED2","ED_YEARFROM2","ED_YEARTO2","ED_UNIVERSITYNAME3","ED_UNIVERSITYADDRESS3","ED_QUALIFICATIONEARNED3","ED_YEARFROM3","ED_YEARTO3","ED_UNIVERSITYNAME4","ED_UNIVERSITYADDRESS4","ED_QUALIFICATIONEARNED4","ED_YEARFROM4","ED_YEARTO4","ED_UNIVERSITYNAME5","ED_UNIVERSITYADDRESS5","ED_QUALIFICATIONEARNED5","ED_YEARFROM5","ED_YEARTO5","RI_RESEARCHINTERESTNAME1","RI_RESEARCHINTERESTDETAILS1","RI_RESEARCHINTERESTNAME2","RI_RESEARCHINTERESTDETAILS2","RI_RESEARCHINTERESTNAME3","RI_RESEARCHINTERESTDETAILS3","RI_RESEARCHINTERESTNAME4","RI_RESEARCHINTERESTDETAILS4","RI_RESEARCHINTERESTNAME5","RI_RESEARCHINTERESTDETAILS5","AW_AWARDNAME1","AW_AWARDYEAR1","AW_AWARDDETAILS1","AW_AWARDNAME2","AW_AWARDYEAR2","AW_AWARDDETAILS2","AW_AWARDNAME3","AW_AWARDYEAR3","AW_AWARDDETAILS3","AW_AWARDNAME4","AW_AWARDYEAR4","AW_AWARDDETAILS4","AW_AWARDNAME5","AW_AWARDYEAR5","AW_AWARDDETAILS5","STRENGTH1","STRENGTH2","STRENGTH3","SKILLSGIFTS","PASSIONS");
$headerPersonalDetail=array("","PRACTITIONER_BUSINESSNAME","PRACTITIONER_BUSINESSNUMBER","EMAILADDRESS","PRACTITIONER_BUSINESSADDRESS","PRACTITIONER_BUSINESSSUBURB","PRACTITIONER_BUSINESSPOSTCODE","PRACTITIONER_BUSINESSSTATE","COUNTRYNAME");
$header1=array("Title", "Business Name", "Business Number", "Email Address", "Business Address", "Suburb", "Post Code", "State", "Country");
$header2=array("Title/Objectve", "Title/Objective Slogan", "Specific Areas of Expertise", "Clinical Affiliations");
$header3=array("Company Name", "Company Address", "Year From", "Year To", "Role/Position Held", "Key Accomplishments");
$header4=array("University Name", "University Address", "Qualification Earned", "Year From", "Year To");
$header5=array("Research Interest Name","Research Interest Details");
$header6=array("Award Name","Award Year","Award Details");

$headerExtraInfo=array("Qualification", "Education", "Board Certifications", "Professional Membership", "Hospital Affiliation", "Country Trained", "Country Degree Obtained");
$tableExtraInfo=array("QUALIFICATIONS", "EDUCATION", "BOARDCERTIFICATIONS", "PRACTITIONER_PROFESSIONALMEMBERSHIP", "HOSPITALAFFILIATION", "C_TRAINED", "C_DEGREE");

if($target!='en'){
	for($i=0;$i<count($header1);$i++){
    	$header1[$i] =  transTo($target,$header1[$i]);
	}
	for($i=0;$i<count($header2);$i++){
    	$header2[$i] =  transTo($target,$header2[$i]);
	}
	for($i=0;$i<count($header3);$i++){
    	$header3[$i] =  transTo($target,$header3[$i]);
	}
	for($i=0;$i<count($header4);$i++){
    	$header4[$i] =  transTo($target,$header4[$i]);
	}
	for($i=0;$i<count($header5);$i++){
    	$header5[$i] =  transTo($target,$header5[$i]);
	}
	for($i=0;$i<count($header6);$i++){
    	$header6[$i] =  transTo($target,$header6[$i]);
	}
	for($i=0;$i<count($headerExtraInfo);$i++){
    	$headerExtraInfo[$i] =  transTo($target,$headerExtraInfo[$i]);
	}
}

$query="SELECT * FROM RESUME WHERE PARTICIPANT_NUMBER = $id";
//echo $query."<BR>";
$result=mysqli_query($conn,$query);
$available=false;
if(mysqli_num_rows($result)!=0){
	$details=mysqli_fetch_array($result);
	$available=true;
}

// initiate FPDI
$mpdf = new mPDF($target);

$html ="<div style='width:100%'>";
$html .= "<div style='width:100%; align:right;'>
			<img src='images/WotMedLogoMedium.jpg' style='width:150px;'><br>
			<b style='font-size:150%'>Dr {$row['FIRSTNAME']} {$row['SURNAME']}</b>";
			if($row['PRACTITIONER_BUSINESSLOGO']!=""){
				$html .= "<img src='photos/thumbs/{$row['PRACTITIONER_BUSINESSLOGO']}' style='width:150px;float:right'><br>";
			}
$html .= "</div>";


$html .= "<div style='width:100%'>
			<table width:100%>";
//basic info
$html .=		"<tr colspan='2'>";
if($target == "en")
	$html .= "<td><b style='font-size:120%'>Basic Information</b></td>";
else
	$html .= "<td><b style='font-size:120%'>".transTo($target, "Basic Information")."</b></td>";
$html .=		"</tr>";

$c=2;

$html .=		"<tr>";
if($target == "en")
	$html .= "<td>Practitioner Number</td>";
else
	$html .= "<td>" . transTo($target, "Practitioner Number")."</td>";
$html .= "<td>{$row["PRACTITIONER_NUMBER"]}</td>";
$html .=		"</tr>";
for($i=1;$i<count($header1);$i++){
	$html .= 	"<tr>
					<td>{$header1[$i]}</td>
					<td>{$row[$headerPersonalDetail[$i]]}</td>
				</tr>";
}

//extra info
$webLink="";
$temp=getDetails($conn,$row['PRACTITIONER_NUMBER'],10);
if($temp!=null){
	$detailResult= mysqli_fetch_array($temp);
	$webLink.=$detailResult['DETAIL_TITLE'];
}
$html .= "<tr>";
if($target == "en")
	$html .= "<td>Independent Website</td>";
else
	$html .= "<td>".transTo($target, "Independent Website")."</td>";
$html .= "<td><a href='$webLink'>$webLink</a></td>
		</tr>";

$specName="";
$query="SELECT SPECIALITY FROM SPECIALITY WHERE SPECIALITY_NUMBER ='" . $row['SPECIALITY_NUMBER'] . "'";
$speciality=mysqli_query($conn,$query);
if(mysqli_num_rows($speciality)!=0){
	$spec=mysqli_fetch_array($speciality);
	$specName = $spec[0];
}
$html .= "<tr>";
if($target == "en")
	$html .= "<td>Speciality</td>";
else
	$html .= "<td>".transTo($target, "Speciality")."</td>";
$html .= "<td>$specName</td>
		</tr>";				

for($i=0; $i<count($headerExtraInfo);$i++){
	$html .= 	"<tr>
					<td>{$headerExtraInfo[$i]}</td>
					<td>{$row[$tableExtraInfo[$i]]}</td>
				</tr>";
}
$html .= "<tr coslpan='2'><td>&nbsp;</td></tr>";

//about practitioner
$html .=		"<tr colspan='2'>";
if($target == "en")
	$html .= "<td><b style='font-size:120%'>About Practitioner</b></td>";
else
	$html .= "<td><b style='font-size:120%'>".transTo($target, "About Practitioner")."</b></td>";
$html .=		"</tr>";

for($i=0;$i<count($header2);$i++){
	$html .= 	"<tr>
					<td>{$header2[$i]}</td>";
	if(isset($details))
		$html .= "<td>{$details[$c]}</td>
				</tr>";
	else
		$html .= "<td>&nbsp;</td>
				</tr>";
	$c++;
}

//professional experiences
for($i=0;$i<5;$i++){
	if($i<=4){
		if(isset($details)){
			if($details[$c+1]==""){
				$cal=(5-$i)*(count($header3));
				$c+=$cal;
				$i=5;
			}else{
				$cc=$i+1;
				$html .= "<tr coslpan='2'><td>&nbsp;</td></tr>";
				$html .=		"<tr colspan='2'>";
				if($target == "en")
					$html .= "<td><b style='font-size:120%'>Professional Experience #$cc</b></td>";
				else
					$html .= "<td><b style='font-size:120%'>".transTo($target, "Professional Experience #$cc")."</b></td>";
				$html .=		"</tr>";
				for($j=0;$j<count($header3);$j++){
					$html .= 	"<tr>
									<td>{$header3[$j]}</td>
									<td>{$details[$c]}</td>
								</tr>";
					$c++;	
				}
			}
		}
	}
}

//educations
for($i=0;$i<5;$i++){
	if($i<=4){
		if(isset($details)){
			if($details[$c+1]==""){
				$cal=(5-$i)*(count($header4));
				$c+=$cal;
				$i=5;
			}else{
				$cc=$i+1;
				$html .= "<tr coslpan='2'><td>&nbsp;</td></tr>";
				$html .=		"<tr colspan='2'>";
				if($target == "en")
					$html .= "<td><b style='font-size:120%'>Education #$cc</b></td>";
				else
					$html .= "<td><b style='font-size:120%'>".transTo($target, "Education #$cc")."</b></td>";
				$html .=		"</tr>";
				for($j=0;$j<count($header4);$j++){
					$html .= 	"<tr>
									<td>{$header4[$j]}</td>
									<td>{$details[$c]}</td>
								</tr>";
					$c++;	
				}
			}	
		}
	}
}

//researches
for($i=0;$i<5;$i++){
	if($i<=4){
		if(isset($details)){
			if($details[$c+1]==""){
				$cal=(5-$i)*(count($header5));
				$c+=$cal;
				$i=5;
			}else{
				$cc=$i+1;
				$html .= "<tr coslpan='2'><td>&nbsp;</td></tr>";
				$html .=		"<tr colspan='2'>";
				if($target == "en")
					$html .= "<td><b style='font-size:120%'>Research Interests #$cc</b></td>";
				else
					$html .= "<td><b style='font-size:120%'>".transTo($target, "Research Interests #$cc")."</b></td>";
				$html .=		"</tr>";
				for($j=0;$j<count($header5);$j++){
					$html .= 	"<tr>
									<td>{$header5[$j]}</td>
									<td>{$details[$c]}</td>
								</tr>";
					$c++;	
				}
			}
		}
	}
}

//awards
for($i=0;$i<5;$i++){
	if($i<=4){
		if(isset($details)){
			if($details[$c+1]==""){
				$cal=(5-$i)*(count($header6));
				$c+=$cal;
				$i=5;
			}else{
				$cc=$i+1;
				$html .= "<tr coslpan='2'><td>&nbsp;</td></tr>";
				$html .=		"<tr colspan='2'>";
				if($target == "en")
					$html .= "<td><b style='font-size:120%'>Awards #$cc</b></td>";
				else
					$html .= "<td><b style='font-size:120%'>".transTo($target, "Awards #$cc")."</b></td>";
				$html .=		"</tr>";
				for($j=0;$j<count($header6);$j++){
					$html .= 	"<tr>
									<td>{$header6[$j]}</td>
									<td>{$details[$c]}</td>
								</tr>";
					$c++;	
				}
			}
		}
	}
}

//strengths
$html .= "<tr coslpan='2'><td>&nbsp;</td></tr>";
$html .=		"<tr colspan='2'>";
if($target == "en")
	$html .= "<td><b style='font-size:120%'>Your Top Three Strengths</b></td>";
else
	$html .= "<td><b style='font-size:120%'>".transTo($target, "Your Top Three Strengths")."</b></td>";
$html .=		"</tr>";
for($i=0;$i<3;$i++){
	$cc=$i+1;
	$html .= "<tr>";
	if($target == "en")
		$html .= "<td>Strength #$cc</td>";
	else
		$html .= "<td>".transTo($target, "Strength #$cc")."</td>";
	if(isset($details))
		$html .= "<td>{$details[$c]}</td>
				</tr>";
	else
		$html .= "<td>&nbsp;</td>
				</tr>";
	$c++;
}

$html .=		"<tr>";
if($target == "en")
	$html .= "<td style='width:20%'>What professional skills/gifts do you have that are particularly valuable to your patients?</td>";
else
	$html .= "<td style='width:20%'>".transTo($target, "What professional skills/gifts do you have that are particularly valuable to your patients?")."</td>";
if(isset($details))
		$html .= "<td>{$details[89]}</td>
				</tr>";
	else
		$html .= "<td>&nbsp;</td>
				</tr>";

$html .= "<tr coslpan='2'><td>&nbsp;</td></tr>";
$html .=		"<tr>";
if($target == "en")
	$html .= "<td style='width:20%'>What are your greatest passions in life and in your profession?</td>";
else
	$html .= "<td style='width:20%'>".transTo($target, "What are your greatest passions in life and in your profession?")."</td>";
if(isset($details))
		$html .= "<td>{$details[90]}</td>
				</tr>";
	else
		$html .= "<td>&nbsp;</td>
				</tr>";
$html .= "<tr coslpan='2'><td>&nbsp;</td></tr>";
$html .= "</table>
		</div>";
if($target!='en')
	$html .= "<p>".transTo($target,"Wotmed is a medical network that connects patients with medical and dental professionals globally")."</p>";
else
	$html .= "<p>Wotmed is a medical network that connects patients with medical and dental professionals globally</p>";
$html .= "</div>";
$mpdf->WriteHTML($html);
$mpdf->Output();





// $pdf->Output($row['PRACTITIONER_BUSINESSNAME'].' Wotmed Resume ' . $target . '.pdf', 'D');
