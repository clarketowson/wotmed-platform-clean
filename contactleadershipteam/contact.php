<?php

if(!$_POST) exit;

// Email address verification, do not edit.
function isEmail($email) {
	return(preg_match("/^[-_.[:alnum:]]+@((([[:alnum:]]|[[:alnum:]][[:alnum:]-]*[[:alnum:]])\.)+(ad|ae|aero|af|ag|ai|al|am|an|ao|aq|ar|arpa|as|at|au|aw|az|ba|bb|bd|be|bf|bg|bh|bi|biz|bj|bm|bn|bo|br|bs|bt|bv|bw|by|bz|ca|cc|cd|cf|cg|ch|ci|ck|cl|cm|cn|co|com|coop|cr|cs|cu|cv|cx|cy|cz|de|dj|dk|dm|do|dz|ec|edu|ee|eg|eh|er|es|et|eu|fi|fj|fk|fm|fo|fr|ga|gb|gd|ge|gf|gh|gi|gl|gm|gn|gov|gp|gq|gr|gs|gt|gu|gw|gy|hk|hm|hn|hr|ht|hu|id|ie|il|in|info|int|io|iq|ir|is|it|jm|jo|jp|ke|kg|kh|ki|km|kn|kp|kr|kw|ky|kz|la|lb|lc|li|lk|lr|ls|lt|lu|lv|ly|ma|mc|md|me|mg|mh|mil|mk|ml|mm|mn|mo|mp|mq|mr|ms|mt|mu|museum|mv|mw|mx|my|mz|na|name|nc|ne|net|nf|ng|ni|nl|no|np|nr|nt|nu|nz|om|org|pa|pe|pf|pg|ph|pk|pl|pm|pn|pr|pro|ps|pt|pw|py|qa|re|ro|ru|rw|sa|sb|sc|sd|se|sg|sh|si|sj|sk|sl|sm|sn|so|sr|st|su|sv|sy|sz|tc|td|tf|tg|th|tj|tk|tm|tn|to|tp|tr|tt|tv|tw|tz|ua|ug|uk|um|us|uy|uz|va|vc|ve|vg|vi|vn|vu|wf|ws|ye|yt|yu|za|zm|zw)$|(([0-9][0-9]?|[0-1][0-9][0-9]|[2][0-4][0-9]|[2][5][0-5])\.){3}([0-9][0-9]?|[0-1][0-9][0-9]|[2][0-4][0-9]|[2][5][0-5]))$/i",$email));
}

if (!defined("PHP_EOL")) define("PHP_EOL", "\r\n");

$name     = $_POST['name'];
$email    = $_POST['email'];
$phone   = $_POST['phone'];
$comments = $_POST['comments'];

if(trim($name) == '') {
	echo '<div class="error_message">You must enter your name.</div>';
	exit();
} else if(trim($email) == '') {
	echo '<div class="error_message">Please enter a valid email address.</div>';
	exit();
} else if(trim($phone) == '') {
	echo '<div class="error_message">Please enter a valid phone number.</div>';
	exit();
} else if(!is_numeric($phone)) {
	echo '<div class="error_message">Your phone number can only contain digits.</div>';
	exit();
} else if(!isEmail($email)) {
	echo '<div class="error_message">You have enter an invalid e-mail address, please try again.</div>';
	exit();
}


if(trim($comments) == '') 
{
	echo '<div class="error_message">Please enter your message.</div>';
	exit();
} 

if(get_magic_quotes_gpc()) {
	$comments = stripslashes($comments);
}


// Configuration option.
// Enter the email address that you want to emails to be sent to.
// Example $address = "joe.doe@yourdomain.com";

$address = "generalfeedback@wotmed.com";


// Configuration option.
// i.e. The standard subject will appear as, "You've been contacted by John Doe."

// Example, $e_subject = '$name . ' has contacted you via Your Website.';

$e_subject = 'Attention Wotmed Team: You\'ve been contacted by ' . $name . '.';


// ***** CUSTOM HTML EMAIL FILE WRITE ****

$numberofLines = 0;

$file_handle = fopen("GeneralPublicContact.html", "rb");
$writeFileName = "GeneralPublicContactCustom.html";
$writeFileHandle = fopen($writeFileName, 'w') or die("can't open file");

while (!feof($file_handle) ) 
{
$numberofLines++;

$line_of_text = fgets($file_handle);

if ($numberofLines == 321)
{
  $line_of_text_to_write = "My name is " . $name . " and I have a message for you as detailed below.  You can contact me via my email " . $email . " or you can call me on " . $phone . " to discuss my comments.  Thank you for your time.";
  fwrite($writeFileHandle, $line_of_text_to_write);
  fwrite($writeFileHandle, "\n"); 
}

else if ($numberofLines == 323)
{
  $line_of_text_to_write = $comments;
  fwrite($writeFileHandle, $line_of_text_to_write);
  fwrite($writeFileHandle, "\n");
}

else
{

fwrite($writeFileHandle, $line_of_text);
fwrite($writeFileHandle, "\n");
}

}

fclose($file_handle);
fclose($writeFileHandle);

// ***** END CUSTOM HTML EMAIL FILE WRITE ****


	$msg = file_get_contents('GeneralPublicContactCustom.html');
	$headers = 'From: noreply@wotmed.com' . "\r\n" .
    'Reply-To: noreply@wotmed.com' . "\r\n" .
    'X-Mailer: PHP/' . phpversion();
	$headers .= "MIME-Version: 1.0\r\n";
	$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";


if(mail($address, $e_subject, $msg, $headers)) {

	// Email has sent successfully, echo a success page.

	echo "<fieldset>";
	echo "<div id='success_page'>";
	echo "<h1>Email Sent Successfully.</h1>";
	echo "<p>Thank you <strong>$name</strong>, your message has been submitted to the Wotmed Leadership Team.</p>";
	echo "</div>";
	echo "</fieldset>";

} else {

	echo 'An unexpected error occurred';

}

// delete the custom created HTML email file as we are now finished with it

//unlink($writeFileName);