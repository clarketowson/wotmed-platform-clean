

<?php
	/*if ($_SERVER['HTTPS'] != "on") {
		$url = "https://". $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
		header("location: $url");
	}  */

	session_start();
	include("../includes/connect.php");
	include("../includes/functions.php");
	
	if(isset($_SESSION['id']))
	{
		$redirect="profile.php";
		if(isset($_SESSION['practitioner_id']))
			$redirect='practitioner_profile.php';
		redirect($redirect);
	}
	elseif(isset($_POST['email']))
	{
		$inputtext=$_POST['inputtext'];
	$inputpassword=$_POST['inputpassword'];
		login($conn,$inputtext,$inputpassword);
	}
?>



<!DOCTYPE html PUBLIC
"-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
		<title>Welcome to Wotmed</title>
		<meta http-equiv="Content-Type" content="'text'/html; charset=utf-8" />

    <!-- ADDTHIS BUTTON BEGIN -->
    <script type="text/javascript">
        var addthis_config = {
            pubid: "ra-556a5489192d5a6e"
        }
    </script>

    <script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js"></script>
    <!-- ADDTHIS BUTTON END -->
        
        
 <script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-38343871-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-49954656-1', 'wotmed.com');
		  ga('send', 'pageview');

		</script>

       
		<script type="text/javascript" src="../classes/jquery.js"></script>
		<script type="text/javascript" src="../classes/hover.js"></script>
		<script src="../classes/jquery.validation.js"></script>
		<script src="../classes/dateSelectBoxes.js"></script>

    <!-- Wotmed Chat System NOTE - MUST BE KEPT EXACTLY HERE-->
    <script type="text/javascript" src="http://platform.wotmed.com/WotmedChat/livechat/php/app.php?widget-init.js"></script>
    <!-- Wotmed Chat System -->

    <link href="../style/i_style.css" rel="stylesheet"></link>

    <link rel="stylesheet" type="text/css" href="../signup.css">

    <link rel="stylesheet" href="../proposals/date/css/pikaday.css">

 <!--   <link rel="stylesheet" href="/proposals/date/css/site.css"> -->


 <!--   <style type="text/css">
        <!--
        #PatientBudget {	font-size:14px;
        }
        #PatientCountries {	font-size:14px;
        }
        #ProcedureList {
            font-size:14px;
        }
        #Submit {	background-color:#125c5d;
            -moz-border-radius:1px;
            -webkit-border-radius:1px;
            border-radius:1px;
            display:inline-block;
            cursor:pointer;
            color:#ffffff;
            font-family:arial;
            font-size:22px;
            padding:12px 50px;
            text-decoration:none;
            text-shadow:0px 1px 0px #125c5d;
        }

    </style> -->



    <meta name="description" content="Wotmed.com - Wotmed Patient Story Submissions">
    <meta name="keywords" content="Wotmed.com - Packaging Medical Tourism.  Cheap Overseas Dental Care, Cheap overseas Dentists, Cheap Overseas Doctors, Overseas Cosmetic Surgery, Overseas Surgery, Overseas Dental Surgery, Tummy Tucks overseas, Boob Jobs overseas, Breast Augmentation overseas, Mummy Makeovers">


    <meta property="og:locale" content="en_US">
    <meta property="og:type" content="website">
    <meta property="og:title" content="Welcome to Wotmed">
    <meta property="og:description" content="Wotmed.com - Packaging Medical Tourism">
    <meta property="og:site_name" content="Wotmed">
    <meta property="og:url" content="http://platform.wotmed.com">

    <meta property="og:image" content="http://platform.wotmed.com/WotmedFacebookLogo6.png">
        
	</head>
	<body onload="searchUsers()">
		<div class="topBar" >
			<div class="bar_frame">
				<div class="w_logo"><img src="../images/WotmedLogoTransparent.png" height="45" alt="wotmedLogo" style='position:relative; left:-30px; padding:0px;padding-top:10px;' /></div>
				<div class="div_login">
					<form id="loginForm" method="post" action="../login.php">
					<table>
						<tr>
							<td><label>Email</label></td>
							<td><label>Password</label></td>
						</tr>
						<tr>
							<td width="150"><input type="text" class="inputtext required email" name="email" id="email"/></td>
							<td width="150"><input type="password" class="inputpassword required" name="password" id="password"/></td>
							<td><input type="submit" value="Login"/></td>
						</tr>
						<tr>
							<td><input name="persistent" type="checkbox" style="float:left; margin: 0px; padding: 0px;"/> <label>Keep me logged in</label></td>
							<td><a href="../resetPassword.php"><label style="cursor:pointer;text-decoration:underline;">Forget your password?</label></a></td>
                            
                           
						</tr>
                        
					</table>
					</form>
				<div id="errMsg">
				</div>
				</div>
				<div>
					<?php //include "fbConnect.php"; ?>
				</div>
				<div>
					<?php //include "googleConnect.php"; ?>
				</div>
			</div>
		</div>



		<div id="container">


            <h3><a href="#" onclick="return false;" class="about">Wotmed is the medical network that connects Patients with Practitioners and Surgery Facilitators globally</a></h3>
            <p>
                &nbsp;</p>

            <div id="system">


                <article class="item">

                    <a href="http://www.addthis.com/bookmark.php?v=250"
                       class="addthis_button"><img
                            src="http://s7.addthis.com/static/btn/v2/lg-share-en.gif"
                            width="125" height="16" border="0" alt="Share" /></a>

<?php



$_SESSION['pageNameDetails'] = basename($_SERVER['PHP_SELF']); /* Returns The Current PHP File Name */


$currentPageNameDetails = $_SESSION['pageNameDetails'];

$withoutExtLink = preg_replace('/\\.[^.\\s]{3,4}$/', '', $currentPageNameDetails);  // remove .php from the hyperlink

// $connGetAuthorDetails = new mysqli('localhost','wotmedco_user','Password1', 'WotmedDBLiveMirror');

$connGetAuthorDetails = new mysqli('liberty.wotmed.com','wotmedco_user','e5~1]qu1|&8)6wJ', 'wotmedco_db');

if ($connGetAuthorDetails->connect_error)
{
    header('Location: /DatabaseError.shtml');
}

$authorDetails = $connGetAuthorDetails->query("SELECT ARTICLEHYPERLINK, ARTICLESUBMITTERNAME, ARTICLESUBMITTERCOUNTRY, ARTICLESUBMITTERPHONENUMBER, ARTICLETITLE, ARTICLESUBJECT, PRACTICESPECIALITY, PRACTICEADDRESS, PRACTICEINDEPENDENTWEBSITEHYPERLINK, PRACTICEMARKETINGBLURB, PRACTICENAME FROM PRACTITIONERARTICLES WHERE ARTICLEHYPERLINK = '$withoutExtLink'");

$connGetAuthorDetails->close();

if ($authorDetails->num_rows > 0)
{
    echo "<div class='content clearfix'>";
    echo "<div class='StoryContainer'>";


// output data of each row
   while($row = $authorDetails->fetch_assoc())          // only show the form once as author may have submitted many articles
   {

                        echo "<h1><strong>";
                        echo $row["ARTICLETITLE"];
                        echo "</strong></h1>";

                        echo "<p> Author: ";
                        echo $row["ARTICLESUBMITTERNAME"];
                        echo "</p>";

                         echo "<p> Country: ";
                        echo $row["ARTICLESUBMITTERCOUNTRY"];
                        echo "</p>";

                        echo "<p> Subject: ";
                        echo $row["ARTICLESUBJECT"];
                         echo "</p>";

                        echo "<p> Speciality: ";
                        echo $row["PRACTICESPECIALITY"];
                         echo "</p>";

                        echo "<p> Practice Name: ";
                        echo $row["PRACTICENAME"];
                        echo "</p>";

                        echo "<p> Practice Slogan: ";
                        echo $row["PRACTICEMARKETINGBLURB"];
                         echo "</p>";

                        echo "<p> Practice Phone: ";
                        echo $row["ARTICLESUBMITTERPHONENUMBER"];
                         echo "</p>";

                        echo "<p> Practice Address: ";
                        echo $row["PRACTICEADDRESS"];
                        echo "</p>";

                        echo "<p> Practice Website: ";
                        $input = $row["PRACTICEINDEPENDENTWEBSITEHYPERLINK"];

                        // in case scheme relative URI is passed, e.g., //www.google.com/
                        $input = trim($input, '/');

                        // If scheme not included, prepend it
                        if (!preg_match('#^http(s)?://#', $input))
                        {
                             $input = 'http://' . $input;
                        }
                         $urlParts = parse_url($input);

                        // remove www
                        $domain = preg_replace('/^www\./', '', $urlParts['host']);

                        $webLink = $domain;

                        $srtEmail=substr($webLink,0,20);

                        echo "<a href='http://$webLink' target='_blank'><span title='$webLink'>". $srtEmail ."</span></a>";
                        echo "</p>";

                        echo "<p> Practice Logo: ";
                        echo "<img src='../images/WotMedLogoMedium.jpg' width='80' height='20' alt='Wotmed' />";
                        echo "</p>";
   }
}

?>





                            <?php

                       //     $conn = new mysqli('localhost','wotmedco_user','Password1', 'WotmedDBLiveMirror');

                           $conn = new mysqli('liberty.wotmed.com','wotmedco_user','e5~1]qu1|&8)6wJ', 'wotmedco_db');






                            if ($conn->connect_error)
                            {
                                header('Location: /DatabaseError.shtml');
                            }

                            $authorName = " ";

                            ?>


                        <br>

                            <div class="WotmedOfficialCommentContainer">

                                <h1><strong>Wotmed Comment:</strong></h1>
                                <p> No Comment.<br> </p>

                                <br>


                                <a href="../../WotmedPractitionerArticles.php">Back to Wotmed Practitioner Articles</a>

                                <br>
                                <br>
                                <br>



                            <?php

                            // check to see if the author has chosen to be contactable by other patients
                            // if so - display the contact author form
                            // otherwise display a message saying they are not contactable by other patients



                            $_SESSION['pageName'] = basename($_SERVER['PHP_SELF']); /* Returns The Current PHP File Name */

                            $currentPageName = $_SESSION['pageName'];

                          //  echo $currentPageName;

                            $withoutExt = preg_replace('/\\.[^.\\s]{3,4}$/', '', $currentPageName);  // remove .php from the hyperlink

                         //   echo $withoutExt;

                            // select the users data
                            $result = $conn->query("SELECT ARTICLESUBMITTERNAME, ARTICLESUBMITTEREMAILADDRESS, ARTICLEHYPERLINK, ISAUTHORCONTACTABLE FROM PRACTITIONERARTICLES WHERE ARTICLEHYPERLINK = '$withoutExt'");



                            $conn->close();

                            $onlyShowFormOnce = 0;

                            if ($result->num_rows > 0) {
                                // output data of each row
                                //   while($row = $result->fetch_assoc())          // only show the form once as author may have submitted many articles
                                //   {
                                //   echo "Name" . $row["STORYSUBMITTERNAME"]. " - Email Address: " . $row["STORYSUBMITTEREMAILADDRESS"] . "<br>";

                                $conn2 = new mysqli('liberty.wotmed.com','wotmedco_user','e5~1]qu1|&8)6wJ', 'wotmedco_db');

                            //    $conn2 = new mysqli('localhost','wotmedco_user','Password1', 'WotmedDBLiveMirror');

                             //   $contactable = $conn2->query("SELECT STORYHYPERLINK FROM PATIENTSTORIES WHERE STORYHYPERLINK = '$withoutExt'");

                                $contactable = $conn2->query("SELECT ISAUTHORCONTACTABLE FROM PRACTITIONERARTICLES WHERE ARTICLEHYPERLINK = '$withoutExt'");

                                $conn2->close();

                                while($row = $result->fetch_assoc())
                                {
                                    $isAuthorContactable = $row["ISAUTHORCONTACTABLE"];
                                  //  echo $isAuthorContactable;
                                }


                             //   if ($contactable->num_rows > 0) {

                                if ($isAuthorContactable == "Yes")
                                {
                                    // the author is contactable


                                    echo "The author of this article has chosen to be contactable via email.  You can use the form below to do this<br>";
                                    echo "
                                                <h1><strong>Contact this Practitioner</strong></h1>


                                                </p>
                                                <form method='post' action='/proposals/proposalsubmit/WotmedPractitionerArticlePractitionerOrPatientContact.php' name='requestforProposal' id='requestforProposal'>
                                                    <table width='922' border='0'>
                                                        <tr>
                                                            <td width='282'>Name</td>
                                                            <td width='508'><label>
                                                                    <input name='Name' type='text' id='Name' size='70' />
                                                                </label></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Email Address</td>
                                                            <td><input name='EmailAddress' type='text' id='EmailAddress' size='40' /></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Your comments</td>
                                                            <td><label>
                                                                    <textarea name='Comments' id='Comments' cols='65' rows='10'></textarea>
                                                                </label></td>
                                                        </tr>
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                    </table>
                                                    <p>&nbsp;</p>
                                                    <p>
                                                        <label>
                                                            <input type='submit' name='PractitionerArticleSubmission' id='signupsubmit' value='Contact Practitioner' />
                                                        </label>
                                                    </p>

                                                </form>




";
                                }
                                else
                                {
                                    echo "The author of this article has chosen not to be contactable via email.<br>";
                                }
                            }



                            ?>

                        <br><br><br>
                        <div class="fSma fcg">

                            <span>

                                 Wotmed Platform Status: <?php

                                $myfile = fopen("../WotmedPlatformStatus.txt", "r") or die("Undetermined");
                                echo fread($myfile,filesize("../WotmedPlatformStatus.txt"));
                                fclose($myfile);

                                ?>

                            </span>
                            <br>

                            <span>Copyright &copy; Wotmed.com <?php	echo date('Y'); ?> All rights reserved.  </span>
                        </div>

