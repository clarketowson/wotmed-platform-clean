<?php
session_start();
include "../includes/functions.php";
include "../includes/connect.php";
$days=array("Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday");	
	$today = getdate();
	$order=$_GET['order'];
	$id=$_GET['id'];
	$query="SELECT * FROM AVAILABILITY WHERE PRACTITIONER_NUMBER ='" . $id . "' ORDER BY TIMESTART";
	$query;
	$schedules=mysqli_query($conn,$query);
	//check for photos
	$listOfSchedules = "
				<div style='border-bottom:1px solid #AAA;padding-bottom:5px;margin-bottom:5px;'>
					<img src='images/WotmedPractitionerScheduleIcon.png' style='width:30px;'> 
					<b style='color:rgb(16, 93, 87);font-size:16px'>Practitioner's Availability</b>
				</div>";
	$listOfSchedules .= "<table border='0'>";
	$listOfSchedules .= "<tr>";
	for($i=0;$i<7;$i++)
	{
		$differentDays=$i-($today['wday']-1+1);
		$thisYear=date("Y")-1+countYear(($order+$differentDays),date("Y"));
		$thisDate=date('d', strtotime($thisYear . '-01-01 +' . (($order+$differentDays-1+date('L', strtotime($thisYear."-01-01")))%(365+date('L', strtotime($thisYear."-01-01")))) . 'days'));
		$thisMonth=date('m', strtotime($thisYear . '-01-01 +' . (($order+$differentDays-1+date('L', strtotime($thisYear."-01-01")))%(365+date('L', strtotime($thisYear."-01-01")))) . 'days'));
		if (checkdate($thisMonth,$thisDate,$thisYear))
		{
			$scheduleDate=$thisDate."-".$thisMonth."-".$thisYear;
		}
		$listOfSchedules .= "<th width='80' align='center'>" . $days[$i] . "<br>" . $scheduleDate . "</th>";
	}
	$listOfSchedules .= "</tr>";
	if(mysqli_num_rows($schedules)!=0){
		$schedulesLeft="";
		$listOfSchedules .= "<tr>";
		for($i=0;$i<7;$i++)
		{
			$numbOfSchedules=0;
			$schedulesLeft .= "<td align='center'>";
			$availability=mysqli_data_seek($schedules,0);
			while($availability=mysqli_fetch_array($schedules)){
				if($availability[2]==$i AND (($order-1+1)+$i)>=(($today['yday']-1+1)+$today['wday']))
				{	
					$differentDays=$i-($today['wday']-1+1);
					$thisYear=date("Y")-1+countYear(($order+$differentDays),date("Y"));
					$thisDate=date('d', strtotime($thisYear . '-01-01 +' . (($order+$differentDays-1+date('L', strtotime($thisYear."-01-01")))%(365+date('L', strtotime($thisYear."-01-01")))) . 'days'));
					$thisMonth=date('m', strtotime($thisYear . '-01-01 +' . (($order+$differentDays-1+date('L', strtotime($thisYear."-01-01")))%(365+date('L', strtotime($thisYear."-01-01")))) . 'days'));
					$scheduleDate=$thisYear."-".$thisMonth."-".$thisDate;
					$found=true;
					$times=timeToMin($availability[4])-timeToMin($availability[3]);
					$totalOfSchedules=$times/$availability[5];
					$totalOfSchedules=floor($totalOfSchedules);
					$newSchedule=$availability[3];
					for($j=0;$j<=$totalOfSchedules;$j++)
					{
						$schedulesLeft .= "<a href='booking.php?practitionerId=" . $id . "&date=" . $scheduleDate . "&time=" . $newSchedule . "'>" . $newSchedule . "</a><br>";
						$newSchedule=addedTime($newSchedule,$availability[5]);
						$numbOfSchedules++;
					}
				}
			}
			$schedulesLeft .= "</td>";
		}
		if($numbOfSchedules==0)
			$schedulesLeft="<td colspan='7' align='center'>No schedule is available</td>";
		$listOfSchedules .= $schedulesLeft . "</tr>"; 
		echo $listOfSchedules;?>
		<form>
			<input type='hidden' name='id' id='id' value='<?php echo $id; ?>'>
			<input type='hidden' name='datePrev' id='datePrev' value='<?php echo ($order-1+1-7); ?>'>
			<input type='hidden' name='dateNext' id='dateNext' value='<?php echo ($order-1+1+7); ?>'>
			<?php if(($order-1+1-7)>=($today['yday']-1+1)) { ?>
			<input type='button' name='prev' id='prev' value='Prev' onclick="updateAvailableSchedule(document.getElementById('id').value,document.getElementById('datePrev').value)">
			<?php } else
				echo "<input type='button' name='prev' id='prev' value='Prev' disabled>";
			?>
			<input type='button' name='next' id='next' value='next' onclick="updateAvailableSchedule(document.getElementById('id').value,document.getElementById('dateNext').value)">
		</form>
		<?php
	}
	else
	{
		echo "<tr><td colspan='7' align='center'>No schedule is available</td></tr>";
	}
	echo "</table>";
?>
