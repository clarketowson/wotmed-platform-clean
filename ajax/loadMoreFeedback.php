<?php
	if(isset($_POST['limit'])){
		session_start();
		include "../includes/connect.php";
		include "../includes/functions.php";
		$res="<table border=0 style='width:100%;'>";
			$id=$_POST['pracId'];
			$limit=$_POST['limit']-1+6;
			$feedback = getFeedback($conn,$id,$limit);
			$row=getPractitionerDetail($conn,$id);
			if($feedback!=null){
				while($userComments=mysqli_fetch_array($feedback)){
					$temp = getParticipantDetail($conn,$userComments['PARTICIPANT_NUMBER']);
					if($temp['PROFILEPHOTO']=="" || $userComments['FUTURECONTACT']!=1){
						$pp="blankSilhouetteMale.png";
					}else{
						$pp=$temp['PROFILEPHOTO'];
					}
					$res.= "<tr>
								<td rowspan='2' style='width:90px;border-bottom:1px solid #AAA;'><img src='photos/thumbs/{$pp}' width='75px' /></td>";
					$name="Private User";
					if($userComments['FUTURECONTACT']!=1){
						$res.= "<td style='width:250px;padding-top:10px;'><b>$name</b></td>";
					}
					else{
						$name=$temp['FIRSTNAME']." ".$temp['SURNAME'];
						if($_SESSION['id']==$userComments['PARTICIPANT_NUMBER'])
							$name.=" (You)";
						$res.= "<td 	style='width:250px;padding-top:10px;'><b><a href='profile.php?id={$temp['PARTICIPANT_NUMBER']}'>$name</a></b></td>";
					}
					if($userComments['FUTURECONTACT']==1){
						$res.= "<td rowspan='2' align='center' style='border-bottom:1px solid #AAA;'><a href='profile.php?id={$temp['PARTICIPANT_NUMBER']}'><img src='images/viewProfile.png' height='30' width='40px'/><br/>View Your Patient Profile</a></td>";
						$res.= "<td rowspan='2' align='center' style='border-bottom:1px solid #AAA;'><a href='mailto:{$temp['EMAILADDRESS']}'><img src='images/contactRecommend.png' height='30' width='40px'/><br/>Email Patient</a></td>";
					}else{
						$res.= "<td rowspan='2' colspan='2' style='border-bottom:1px solid #AAA;'>&nbsp;</td>";
					}
					$res.= "</tr>";
					$review=$userComments['COMMENT'];
					if($review=="")
						$review="$name has recommended {$row['PRACTITIONER_BUSINESSNAME']} but has chosen not to provide a writen review";
					$res.= "<tr>
							<td style='border-bottom:1px solid #AAA;padding-bottom:10px;width:250px;'>$review</td>
						</tr>";
				}
				if($limit<getNumberOfFeedback($conn,$id))
					$res.= "<tr>
						<td colspan='3'><a href='' onclick='loadMore({$limit},{$id});return false;'>Load more</a></td>
						</tr>";
			}
		$res.="</table>";
		echo $res;
	}
?>
