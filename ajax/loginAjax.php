<?php
	if(isset($_POST['inputpassword'])){
		session_start();
		include "../includes/connect.php";
		include "../includes/functions.php";

		$inputtext=$_POST['inputtext'];
		$inputpassword=$_POST['inputpassword'];
		if(isset($_COOKIE['lt'])){
			if($_COOKIE['lt']<20){
				$res = login($conn,$inputtext,$inputpassword,"0","../");
			}else{
				$res="You are trying too often. Please try again later.";
			}
		}else{
			$res = login($conn,$inputtext,$inputpassword,"0","../");
		}
		if($res==""){
			$redirect="profile.php";
			if(isset($_SESSION['practitioner_id']))
				$redirect='practitioner_profile.php';
			if(isset($_COOKIE['lt'])){
				setcookie("lt", 0, time()-1);
			}
			echo $redirect;
		}
		else{
			if(isset($_COOKIE['lt'])){
				setcookie("lt", ($_COOKIE['lt']+1), time()+3600);
			}else{
				setcookie("lt", 1, time()+3600);
			}
			echo $res;
		}	
	}
?>
