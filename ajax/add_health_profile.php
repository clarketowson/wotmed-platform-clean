<?php
	session_start();
	if(!isset($_SESSION['id'])){
		header("location:../login.php");
	}
	include "../includes/connect.php";
	include "../includes/functions.php";
	if(isset($_SESSION['id'])){
		$rowSession=getParticipantDetail($conn,$_SESSION['id']);
	}
	if($rowSession['PROFILEPHOTO']==""){
		$ppFileNameSession="blankSilhouetteMale.png";
	}else{
		$ppFileNameSession=$rowSession['PROFILEPHOTO'];
	}
	$row=getParticipantDetail($conn,$_SESSION['id']);
	$header=array( 5=>"Birthday", 6=>"Gender", 7=>"Email");
?>
	<?php
		if(isset($_POST['checkupId']))
		{
			$checkup_number=mysqli_real_escape_string($conn,$_POST['checkupId']);
		}
		if(isset($_POST['Day']))
		{
			$date=mysqli_real_escape_string($conn,$_POST['Year']."-".$_POST['Month']."-".$_POST['Day']);
			if(isset($_POST['desc'])){
				$desc=mysqli_real_escape_string($conn,$_POST['desc']);
			}else{
				$desc="";
				foreach($_POST as $key => $value){
					if(($key!="Year")&&($key!="Month")&&($key!="Day")&&($key!="checkup_number")){
						$desc.=$value."/";
					}
				}
				$desc=mysqli_real_escape_string($conn,substr($desc,0,strlen($desc)-1));
			}
			$id=$_SESSION['id'];
			$checkup_number=$_POST['checkup_number'];
			$query="INSERT INTO HEALTHPROFILE (PARTICIPANT_NUMBER,UPDATETIME,CHECKUP_NUMBER,DESCRIPTION) VALUES ('" . $id . "','" . $date . "','" . $checkup_number . "','" . $desc . "');";
			mysqli_query($conn,$query);
			?>
			<script language="javascript"> 
				window.location = "../profile.php";
			</script>
			<?php
		}
	?>
		<form method="post" class="result" action="ajax/add_health_profile.php" style="width: 300px;">
		<?php echo "<input type='hidden' name='checkup_number' value='" . $checkup_number . "' />";?>
			<table>
				<tr>
					<td class="label">
						<label>Result:</label>
					</td>
					<?php
						if($checkUpResult=getCheckupResultValues($conn,$checkup_number)){
							echo "<td><select name='desc'>";
							while($checkupResultRow=mysqli_fetch_array($checkUpResult)){
								echo "<option value='$checkupResultRow[1]'>$checkupResultRow[1]</option>";
							}
							echo "</select></td>";
						}else{
							switch($checkup_number){
								case 1 :
									echo '<td><input size="3" type="text" name="sist" class="required"/><input size="3" type="text" name="dias" class="required"/></td>';
									break;
								case 4 :
									echo '<td><input type="text" name="bmi" class="required"></input></td>';
									break;
								default:
									echo '<td><textarea rows="4" cols="20" name="desc"></textarea></td>';
									break;
							}
						}
					?>
					
				</tr>
				<tr>
					<td class="label">
						<label>Test Date:</label>
					</td>
					<td>
						<div class="field_container">
							<select id="birthYear2" name="Year">
							<option label="Year:" selected="selected">Year:</option>
							<?php 
							echo $nowDate=date("j");
							echo $nowMonth=date("n");
							echo $nowYear=date("Y");
							$currentYear = getdate();
							for($i=0;$i<100;$i++){
								$year=$currentYear["year"]-$i;
								if($nowYear!=$year)
									echo "<option value='$year'>$year</option>";
								else
									echo "<option value='$year' selected='selected'>$year</option>";
							} ?>
							</select>
							<select id="birthMonth2" name="Month">
							<option label="Month:">Month:</option>
							<option label="January" value="1"<?php if($nowMonth==1){echo " selected='selected'"; }?>>January</option>
							<option label="February" value="2"<?php if($nowMonth==2){echo " selected='selected'"; }?>>February</option>
							<option label="March" value="3"<?php if($nowMonth==3){echo " selected='selected'"; }?>>March</option>
							<option label="April" value="4"<?php if($nowMonth==4){echo " selected='selected'"; }?>>April</option>
							<option label="May" value="5"<?php if($nowMonth==5){echo " selected='selected'"; }?>>May</option>
							<option label="June" value="6"<?php if($nowMonth==6){echo " selected='selected'"; }?>>June</option>
							<option label="July" value="7"<?php if($nowMonth==7){echo " selected='selected'"; }?>>July</option>
							<option label="August" value="8"<?php if($nowMonth==8){echo " selected='selected'"; }?>>August</option>
							<option label="September" value="9"<?php if($nowMonth==9){echo " selected='selected'"; }?>>September</option>
							<option label="October" value="10"<?php if($nowMonth==10){echo " selected='selected'"; }?>>October</option>
							<option label="November" value="11"<?php if($nowMonth==11){echo " selected='selected'"; }?>>November</option>
							<option label="December" value="12"<?php if($nowMonth==12){echo " selected='selected'"; }?>>December</option>
							</select>
							<select id="birthDay2" name="Day">
							<option label="Day:" selected="selected">Day:</option>
							<?php
								for ($i=1;$i<=31;$i++){
									if($nowDate!=$i)
										echo "<option value='{$i}'>{$i}</option>";
									else
										echo "<option value='{$i}' selected='selected'>{$i}</option>";
								}
							?>
							</select>
						</div>
					</td>
				</tr>
				<tr>
					<td><input type="submit" value="Update"/></td>
					<td><input type="button" value="Cancel" class="close" onclick="closeDiv(<?php echo $checkup_number; ?>)"/></td>
				</tr>
			</table>
		</form>
		
			
