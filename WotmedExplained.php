<?php
	/*if ($_SERVER['HTTPS'] != "on") {
		$url = "https://". $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
		header("location: $url");
	}  */

	session_start();
	include("includes/connect.php");
	include("includes/functions.php");
	
	if(isset($_SESSION['id']))
	{
		$redirect="profile.php";
		if(isset($_SESSION['practitioner_id']))
			$redirect='practitioner_profile.php';
		redirect($redirect);
	}
	elseif(isset($_POST['email']))
	{
		$inputtext=$_POST['inputtext'];
	$inputpassword=$_POST['inputpassword'];
		login($conn,$inputtext,$inputpassword);
	}
?>



<!DOCTYPE html PUBLIC
"-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
		<title>Welcome to Wotmed</title>
		<meta http-equiv="Content-Type" content="'text'/html; charset=utf-8" />

    <!-- A minimal Flowplayer setup to get you started -->


    <!--
        include flowplayer JavaScript file that does
        Flash embedding and provides the Flowplayer API.
    -->
    <script type="text/javascript" src="flowplayer-3.2.11.min.js"></script>

    <!-- some minimal styling, can be removed -->
    <link rel="stylesheet" type="text/css" href="style.css">



    <script type="text/javascript">
        if((navigator.userAgent.match(/iPhone/i)) ||
            (navigator.userAgent.match(/iPad/i))) {
            if (document.cookie.indexOf("iphone_redirect=false") == -1) window.location = "http://platform.wotmed.com/WotmedExplainedMobile.php";
        }
    </script>

    <script type="text/javascript"> // <![CDATA[
        if ( (navigator.userAgent.indexOf('Android') != -1) ) {
            document.location = "http://platform.wotmed.com/WotmedExplainedMobile.php";
        } // ]]>
    </script>



    <script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-38343871-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-49954656-1', 'wotmed.com');
		  ga('send', 'pageview');

		</script>

       
		<script type="text/javascript" src="classes/jquery.js"></script>
		<script type="text/javascript" src="classes/hover.js"></script>
		<script src="classes/jquery.validation.js"></script>
		<script src="classes/dateSelectBoxes.js"></script>

    <link href="style/i_style.css" rel="stylesheet"></link>

    <link rel="stylesheet" type="text/css" href="signup.css">

    <link rel="stylesheet" href="/proposals/date/css/pikaday.css">

 <!--   <link rel="stylesheet" href="/proposals/date/css/site.css"> -->


 <!--   <style type="text/css">
        <!--
        #PatientBudget {	font-size:14px;
        }
        #PatientCountries {	font-size:14px;
        }
        #ProcedureList {
            font-size:14px;
        }
        #Submit {	background-color:#125c5d;
            -moz-border-radius:1px;
            -webkit-border-radius:1px;
            border-radius:1px;
            display:inline-block;
            cursor:pointer;
            color:#ffffff;
            font-family:arial;
            font-size:22px;
            padding:12px 50px;
            text-decoration:none;
            text-shadow:0px 1px 0px #125c5d;
        }

    </style> -->



    <meta name="description" content="Wotmed.com - Packaging Medical Tourism">
    <meta name="keywords" content="Wotmed.com - Packaging Medical Tourism.  Cheap Overseas Dental Care, Cheap overseas Dentists, Cheap Overseas Doctors, Overseas Cosmetic Surgery, Overseas Surgery, Overseas Dental Surgery, Tummy Tucks overseas, Boob Jobs overseas, Breast Augmentation overseas, Mummy Makeovers">

    <meta property="og:locale" content="en_US">
    <meta property="og:type" content="website">
    <meta property="og:title" content="Welcome to Wotmed">
    <meta property="og:description" content="Wotmed.com - Packaging Medical Tourism">
    <meta property="og:site_name" content="Wotmed">
    <meta property="og:url" content="http://platform.wotmed.com">

    <meta property="og:image" content="http://platform.wotmed.com/WotmedFacebookLogo6.png">
        
	</head>
	<body onload="searchUsers()">
		<div class="topBar" >
			<div class="bar_frame">
				<div class="w_logo"><img src="images/WotmedLogoTransparent.png" height="45" alt="wotmedLogo" style='position:relative; left:-30px; padding:0px;padding-top:10px;' /></div>
				<div class="div_login">
					<form id="loginForm" method="post" action="login.php">
					<table>
						<tr>
							<td><label>Email</label></td>
							<td><label>Password</label></td>
						</tr>
						<tr>
							<td width="150"><input type="text" class="inputtext required email" name="email" id="email"/></td>
							<td width="150"><input type="password" class="inputpassword required" name="password" id="password"/></td>
							<td><input type="submit" value="Login"/></td>
						</tr>
						<tr>
							<td><input name="persistent" type="checkbox" style="float:left; margin: 0px; padding: 0px;"/> <label>Keep me logged in</label></td>
							<td><a href="resetPassword.php"><label style="cursor:pointer;text-decoration:underline;">Forget your password?</label></a></td>
                            
                           
						</tr>
                        
					</table>
					</form>
				<div id="errMsg">
				</div>
				</div>
				<div>
					<?php //include "fbConnect.php"; ?>
				</div>
				<div>
					<?php //include "googleConnect.php"; ?>
				</div>
			</div>
		</div>



		<div id="container">


            <h3><a href="#" onclick="return false;" class="about">Wotmed is the medical network that connects Patients with Practitioners and Surgery Facilitators globally</a></h3>
            <p>
                &nbsp;</p>



                <div class="VideoContainer" align='left'>

                    <div class="page-element widget-container page-element-type-text widget-text" id="element-6" style="display:block;width:615px;height:344px; margin-left:0px; margin-top:0px;""><a href="/WotmedFirstAdvertisementVer2.flv" style="display:block;width:615px;height:344px; margin-left:0px; margin-top:35px;" id="player2">

                            <!-- specify a splash image inside the container -->
                            <img
                                src="WotmedExplainedSplash3.jpg"
                                alt="Wotmed explained video" /></a>

                        <a href "WotmedExplainedThaiTranscript.php" onclick="return popitup('WotmedExplainedThaiTranscript.php')"></a><a href="WotmedExplainedThaiTranscript.php" onClick="return popitup('WotmedExplainedThaiTranscript.php')">Thai language transcript</a>

                    </div>
        </div>


        <br><br>

                <div class="signupForms">

                    <form id="registerFormPatient" method="post" action="reg-participant.php""><a href="/WotmedFirstAdvertisementVer2.flv" ">

                    <table class="uiGrid editor" cellpadding="1" cellspacing="0" >
                        <tr>
                            <td><input type="submit" value="Patient Sign Up" id='signupsubmit'/>
                                </form>

                                <form id="registerFormPractitioner" method="post" action="reg-practitioner.php">
                                    <td><input type="submit" value="Practitioner Sign Up" id='signupsubmitpractitioner'/></td>
                                </form>

                                <form id="registerFormFacilitator" method="post" action="reg-facilitator.php">
                                    <td><input type="submit" value="Surgery Facilitator Sign Up" id='signupsubmitfacilitator'/></td>
                                </form>

                    </table>

                </div>



                        <!-- this A tag is where your Flowplayer will be placed. it can be anywhere -->
                        <!-- specify a splash image inside the container -->

                        <!-- <img
                src="/media/img/home/flow_eye.jpg"
                alt="Search engine friendly content" />
            </a> -->

                        <!-- this will install flowplayer inside previous A- tag. -->
                        <script>
                            flowplayer("player2", "flowplayer.commercial-3.2.12.swf", {
                                // commercial version license key
                                key: '#$1e327ef0a3f199461a1',
                                clip: {
                                    // these two configuration variables does the trick
                                    autoPlay: false,
                                    accelerated: false,
                                    autoBuffering: true // <- do not place a comma here


                                }
                                ,
                                plugins:  {
                                    controls: {

                                        // tooltips configuration
                                        tooltips: {

                                            // enable english tooltips on all buttons
                                            buttons: true,

                                            // customized texts for buttons
                                            play: 'Play',
                                            pause: 'Pause',
                                            fullscreen: 'Full Screen'
                                        },

                                        // background color for all tooltips
                                        tooltipColor: '#008584',

                                        // text color
                                        tooltipTextColor: '#FFFFFF'
                                    }
                                }
                                ,
                                // now we can tweak the menu
                                contextMenu: [
                                    // 1. load related videos with jQuery and AJAX into the HTML DIV
                                    {
                                        'Show related videos' : function() {
                                            $("#related").load("/demos/configuration/related.html");
                                        }
                                    },

                                    // 2. display custom player information
                                    "wotmed.com video player 1.1"
                                ],


                            });

                        </script>

        </a>

			<br><br>
			<?php include "includes/p_footer.php"; ?>
