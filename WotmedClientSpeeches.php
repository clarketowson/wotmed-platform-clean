<script type="application/ld+json">
{
"@context": "http://schema.org",
"@type": "Service",
"name": "Wotmed Client Speeches",
"description": "Speeches from Practitioners and Surgery Facilitators from around the world",
"serviceType": "Speech Publication Service",
"url": "http://platform.wotmed.com/WotmedClientSpeeches.php"
}
}
</script>

<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "VideoObject",
  "name": "Start With Why",
  "description": "Clarke Towson talks about why Wotmed has been created",
  "thumbnailUrl": "http://platform.wotmed.com/StartWithWhyThumbnail.jpg",
  "uploadDate": "2015-06-06",
  "duration": "PT6M24S",
  "contentUrl": "http://platform.wotmed.com/ImageUpload/uploads/clientspeeches/StartWithWHYMultiCamera.flv",
  "transcript": "Our valued clients,
I would like to express my appreciation to you for taking the time out of your busy schedules to hear me talk today on why we have created Wotmed. I will be talking a lot in this speech about what I believe because the Wotmed platform has been built around my beliefs and the beliefs of the Wotmed team.
So let me begin today by talking about the power of belief and asking that all important question: Why.
Why have we built Wotmed?
Firstly - and most importantly - we believe that all the people of this world deserve to be empowered to access affordable medical services - no matter what country they are located in. There are countries in this world like the United States of America in which medical services are so expensive that should a person get sick and not have insurance - the medical bills can bankrupt them.
We have built Wotmed so that no matter what country patients are located in - they can become medical tourists and gain access to the medical services of Practitioners located in any country of the world. Our platform is country independent and is open to all the citizens of the world. We have built this platform for equal access and equal opportunity for all the Patients and Practitioners of this world.
We believe in the people of disruptive technology to change the world.  We believed in ourselves to develop that disruptive technology. Wotmed is the result of this belief. We know that we can help to make the world a better place and bring opportunity for access to affordable medical services to those who need them. We are the facilitators who have empowered you - Patients and Practitioners of the world to connect with each other for your mutual self interest.
To the brave men, women, children and transgendered patients who have no choice but to seek affordable medical services in the developing world - this platform is for you. Before Wotmed you had to go it alone. You had to face the fear of uncertainty in travelling to a foreign land for access to affordable medical services. You are not alone anymore. Wotmed is here to help you.
By creating a Wotmed profile - patients located anywhere in the world can connect with and establish relationships with practitioners located anywhere in the world in a libertarian spirit of freedom, individual liberty and voluntary association with little to no government interference.
We believe in the power of the free market. We have built an open and transparent global marketplace where the prices of all medical services are published and known by all the participants on the platform. Our platform is a global marketplace for the voluntarily establishment of relationships between patients and practitioners guided by each parties mutual self interest. Our platform enables patients to buy medical services from practitioners globally and includes the ability to book flights, hotel accommodation and car hire as part of this buying process.
We believe so much in the power of connecting Patients with Practitioners globally that we have spent the past year and a half working nights and weekends - we have given up our personal time and we have made great personal sacrifices to bring the Wotmed platform to the world. We have worked without funding or payment. We have worked with great passion, commitment and determination. We have worked with the knowledge that we may fail. We have worked with inspiration that the world needs the platform that we have developed. Nothing worthwhile is ever done without great effort. We have expended great time and effort to create the Wotmed platform.
So - I have talked a lot about why we have built Wotmed. I have told you today what I believe and why I believe it. Our goal is to do business with those of you who believe what we believe.
We believe in freedom, openness and transparency. We believe in the magic of a global, free market. We believe in human liberty and small government. We believe in peace and prosperity for all the citizens of the world. We welcome change as change brings disruption and creates opportunity. This opportunity can lead to more freedom.
I ask you to have faith in us and in the platform that we have developed for you. Here today I would like to ask you to help us by spreading the word about Wotmed to your friends, your family and your co-workers.
I ask you - Practitioners from every country of the world - to join us in our quest to help humanity. Create an account on Wotmed and establish relationships with patients from around the world who need your services. Fill out your Wotmed resume so we can connect you with patients globally who are in desperate need of your life saving skills.
I ask you - Patients from every country of the world - join us to connect with and establish relationships with Practitioners from around the world who can help you to gain access to affordable medical services.
No matter what country you come from or what your politics are - you are welcome on our platform.
No matter what your religious beliefs - you are welcome.
No matter if you are Straight, Gay, Lesbian, Bisexual or Trans Gendered - you are welcome on our platform.
Our platform exists to empower you - Practitioners and Patients of this world. It's now up to you to create an account on the platform and establish relationships with each other for your mutual self interest.
Once again - I would like to express my appreciation to you for taking the time out of your busy schedules to hear me talk today on our beliefs and why we have created Wotmed. Myself and the Wotmed team look forward with anticipation to doing business with those of you who believe what we believe.
Thank You. I am Clarke Towson."
}
</script>

<?php
	/*if ($_SERVER['HTTPS'] != "on") {
		$url = "https://". $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
		header("location: $url");
	}  */

	session_start();
	include("includes/connect.php");
	include("includes/functions.php");
	
	if(isset($_SESSION['id']))
	{
		$redirect="profile.php";
		if(isset($_SESSION['practitioner_id']))
			$redirect='practitioner_profile.php';
		redirect($redirect);
	}
	elseif(isset($_POST['email']))
	{
		$inputtext=$_POST['inputtext'];
	$inputpassword=$_POST['inputpassword'];
		login($conn,$inputtext,$inputpassword);
	}
?>



<!DOCTYPE html PUBLIC
"-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
		<title>Wotmed Client Speeches</title>
		<meta http-equiv="Content-Type" content="'text'/html; charset=utf-8" />
        
        
 <script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-38343871-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-49954656-1', 'wotmed.com');
		  ga('send', 'pageview');

		</script>

       
		<script type="text/javascript" src="classes/jquery.js"></script>
		<script type="text/javascript" src="classes/hover.js"></script>
		<script src="classes/jquery.validation.js"></script>
		<script src="classes/dateSelectBoxes.js"></script>

    <!-- Wotmed Chat System NOTE - MUST BE KEPT EXACTLY HERE-->
    <script type="text/javascript" src="http://platform.wotmed.com/WotmedChat/livechat/php/app.php?widget-init.js"></script>
    <!-- Wotmed Chat System -->

    <link href="style/i_style.css" rel="stylesheet"></link>

    <link rel="stylesheet" type="text/css" href="signup.css">

    <link rel="stylesheet" href="/proposals/date/css/pikaday.css">

 <!--   <link rel="stylesheet" href="/proposals/date/css/site.css"> -->


 <!--   <style type="text/css">
        <!--
        #PatientBudget {	font-size:14px;
        }
        #PatientCountries {	font-size:14px;
        }
        #ProcedureList {
            font-size:14px;
        }
        #Submit {	background-color:#125c5d;
            -moz-border-radius:1px;
            -webkit-border-radius:1px;
            border-radius:1px;
            display:inline-block;
            cursor:pointer;
            color:#ffffff;
            font-family:arial;
            font-size:22px;
            padding:12px 50px;
            text-decoration:none;
            text-shadow:0px 1px 0px #125c5d;
        }

    </style> -->



    <meta name="description" content="Submit a Speech to Wotmed Now.  Wotmed Video Speeches.  Audience of Doctors, Dentists and Patients globally.  Wotmed.com – Facilitating Medical Travel">
    <meta name="keywords" content="Wotmed.com, Speeches, Submit Speeches, Watch Speeches, Audience of Dentists, Patients and Doctors globally, Facilitating Medical Travel">

    <meta property="og:locale" content="en_US">
    <meta property="og:type" content="website">
    <meta property="og:title" content="Welcome to Wotmed">
    <meta property="og:description" content="Wotmed.com - Packaging Medical Tourism">
    <meta property="og:site_name" content="Wotmed">
    <meta property="og:url" content="http://platform.wotmed.com/WotmedClientSpeeches.php">
    <meta property="og:image" content="http://platform.wotmed.com/WotmedFacebookLogo2015.png">
        
	</head>
	<body onLoad="searchUsers()">
		<div class="topBar" >
			<div class="bar_frame">
				<div class="w_logo"><img src="images/WotmedLogoTransparent.png" height="45" alt="wotmedLogo" style='position:relative; left:-30px; padding:0px;padding-top:10px;' /></div>
				<div class="div_login">
					<form id="loginForm" method="post" action="login.php">
					<table>
						<tr>
							<td><label>Email</label></td>
							<td><label>Password</label></td>
						</tr>
						<tr>
							<td width="150"><input type="text" class="inputtext required email" name="email" id="email"/></td>
							<td width="150"><input type="password" class="inputpassword required" name="password" id="password"/></td>
							<td><input type="submit" value="Login"/></td>
						</tr>
						<tr>
							<td><input name="persistent" type="checkbox" style="float:left; margin: 0px; padding: 0px;"/> <label>Keep me logged in</label></td>
							<td><a href="resetPassword.php"><label style="cursor:pointer;text-decoration:underline;">Forget your password?</label></a></td>
                            
                           
						</tr>
                        
					</table>
					</form>
				<div id="errMsg">
				</div>
				</div>
				<div>
					<?php //include "fbConnect.php"; ?>
				</div>
				<div>
					<?php //include "googleConnect.php"; ?>
				</div>
			</div>
		</div>



		<div id="container">


            <h3><a href="#" onClick="return false;" class="about">Wotmed is the medical network that connects Patients with Practitioners and Surgery Facilitators globally</a></h3>
            <p>&nbsp;
                </p>

            <div id="system">


                <article class="item">




                    <div class="content clearfix">


                        <h1><strong>Wotmed Client Speeches</strong></h1>
                        <p> This section of the site is dedicated to empowering you as a practitioner or surgery facilitator to post video speeches.</p>
                        <p> Your speeches may be of great value to other patients around the world who are considering undergoing surgery.</p>
                        <p> Please note that the Wotmed Team moderates all video speeches submissions before publishing.</p>
                        <br>

                        <p> You can submit your video speech to the Wotmed Team by

                            <a href="WotmedClientSpeechSubmissions.php"><label style="cursor:pointer;text-decoration:underline;">clicking here</label></a>
<br><br>
                       <style>
                        .column-left{ float: left; width: 20%; }
                        .column-right{ float: right; width: 30%; }
                        .column-center{ display: inline-block; width: 50%; }
                       </style>

                        <div class="StoryContainer">
                            <div class="column-center"><b>Speech Title</b>
                                <BR> <BR>
                                <a href='WotmedClientSpeeches/2015-05-30.php'>Start With Why</a>
                                <br>




                            </div>

                            <div class="column-right"><b>Speaker</b>
                                <BR> <BR>
                                <a href="WotmedClientSpeeches/2015-05-30.php">Clarke Towson Wotmed CTO</a> <a href="WotmedClientSpeeches/2015-05-30.php"><img src="WotmedClientSpeeches/2015-05-30Thumb.jpg"/></a>




                            </div>

                            <div class="column-left"><b>Date Published</b>
                                <BR> <BR>
                                Saturday June 6th 2015



                    </div>










            <p>&nbsp;</p>



        </div>


			<p>&nbsp;</p>
			<p>&nbsp;</p>
			<?php include "includes/p_footer.php"; ?>
