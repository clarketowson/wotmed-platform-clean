<?php

// HTML Email template
// 
// Clarke towson
// June 10th 2014
// Version 1

$to      = 'participantsignup@wotmed.com';
$subject = 'A Practitioner has just signed up to Wotmed';

$message = file_get_contents('HTMLEmailTemplate/WotmedEmailTemplate/Practitioner.html');

$headers = 'From: participantsignup@wotmed.com' . "\r\n" .
    'Reply-To: participantsignup@wotmed.com' . "\r\n" .
    'X-Mailer: PHP/' . phpversion();
$headers .= "MIME-Version: 1.0\r\n";
$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

mail($to, $subject, $message, $headers);

?> 