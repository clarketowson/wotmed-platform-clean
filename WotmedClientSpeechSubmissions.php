<script type="application/ld+json">
{
"@context": "http://schema.org",
"@type": "Service",
"name": "Wotmed Video Speeches",
"description": "Empowering Practitioners and Surgery Facilitators to submit video speeches",
"serviceType": "Video Advertising on the Wotmed Platform",
"url": "http://platform.wotmed.com/WotmedClientSpeechSubmissions.php",
"image": "http://localhost/WotmedPractitionerSpeechesIcon.jpg"

}
}
</script>

<script src="formvalidate/dist/parsley.min.js"></script>

<?php
	/*if ($_SERVER['HTTPS'] != "on") {
		$url = "https://". $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
		header("location: $url");
	}  */

	session_start();
	include("includes/connect.php");
	include("includes/functions.php");

require_once "phpfileuploader2015/phpuploader/include_phpuploader.php";
	

?>



<!DOCTYPE html PUBLIC
"-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
		<title>Wotmed Submit Speech</title>
		<meta http-equiv="Content-Type" content="'text'/html; charset=utf-8" />
        
        
 <script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-38343871-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

    <!-- Wotmed Chat System NOTE - MUST BE KEPT EXACTLY HERE-->
    <script type="text/javascript" src="http://platform.wotmed.com/WotmedChat/livechat/php/app.php?widget-init.js"></script>
    <!-- Wotmed Chat System -->

    <link href="style/i_style.css" rel="stylesheet"></link>

    <link rel="stylesheet" type="text/css" href="signup.css">





    <script type="text/javascript" src="ImageUpload/js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="ImageUpload/js/jquery.form.min.js"></script>



    <script type="text/javascript">
        $(document).ready(function() {
            var options = {
                target: '#output',   // target element(s) to be updated with server response
                beforeSubmit: beforeSubmit,  // pre-submit callback
                success: afterSuccess,  // post-submit callback
                resetForm: true        // reset the form after successful submit
            };

            $('#MyUploadForm').submit(function() {
                $(this).ajaxSubmit(options);
                // always return false to prevent standard browser submit and page navigation
                return false;
            });
        });

        function afterSuccess()
        {
            $('#submit-btn').show(); //hide submit button
            $('#loading-img').hide(); //hide submit button

        }

        //function to check file size before uploading.
        function beforeSubmit(){
            //check whether browser fully supports all File API
            if (window.File && window.FileReader && window.FileList && window.Blob)
            {

                if( !$('#imageInput').val()) //check empty input filed
                {
                    $("#output").html("No Image specified");
                    return false
                }

                var fsize = $('#imageInput')[0].files[0].size; //get file size
                var ftype = $('#imageInput')[0].files[0].type; // get file type


                //allow only valid image file types
                switch(ftype)
                {
                    case 'image/png': case 'image/gif': case 'image/jpeg': case 'image/pjpeg':
                    break;
                    default:
                        $("#output").html("<b>"+ftype+"</b> Unsupported file type!");
                        return false
                }

                //Allowed file size is less than 1 MB (1048576)
                if(fsize>1048576)
                {
                    $("#output").html("<b>"+bytesToSize(fsize) +"</b> Too big Image file! <br />Please reduce the size of your photo using an image editor.");
                    return false
                }

                $('#submit-btn').hide(); //hide submit button
                $('#loading-img').show(); //hide submit button
                $("#output").html("");
            }
            else
            {
                //Output error to older browsers that do not support HTML5 File API
                $("#output").html("Please upgrade your browser, because your current browser lacks some new features we need!");
                return false;
            }
        }

        //function to format bites bit.ly/19yoIPO
        function bytesToSize(bytes) {
            var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
            if (bytes == 0) return '0 Bytes';
            var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
            return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
        }

    </script>


    <link href="ImageUpload/style/style.css" rel="stylesheet" type="text/css">

<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-49954656-1', 'wotmed.com');
		  ga('send', 'pageview');

		</script>





 <!--   <link rel="stylesheet" href="/proposals/date/css/site.css"> -->


 <!--   <style type="text/css">
        <!--
        #PatientBudget {	font-size:14px;
        }
        #PatientCountries {	font-size:14px;
        }
        #ProcedureList {
            font-size:14px;
        }
        #Submit {	background-color:#125c5d;
            -moz-border-radius:1px;
            -webkit-border-radius:1px;
            border-radius:1px;
            display:inline-block;
            cursor:pointer;
            color:#ffffff;
            font-family:arial;
            font-size:22px;
            padding:12px 50px;
            text-decoration:none;
            text-shadow:0px 1px 0px #125c5d;
        }

    </style> -->



    <meta name="description" content="Wotmed.com - Packaging Medical Tourism">
    <meta name="keywords" content="Wotmed.com - Packaging Medical Tourism.  Cheap Overseas Dental Care, Cheap overseas Dentists, Cheap Overseas Doctors, Overseas Cosmetic Surgery, Overseas Surgery, Overseas Dental Surgery, Tummy Tucks overseas, Boob Jobs overseas, Breast Augmentation overseas, Mummy Makeovers">

    <meta property="og:locale" content="en_US">
    <meta property="og:type" content="website">
    <meta property="og:title" content="Welcome to Wotmed">
    <meta property="og:description" content="Wotmed.com - Packaging Medical Tourism">
    <meta property="og:site_name" content="Wotmed">
    <meta property="og:url" content="http://platform.wotmed.com">
    <meta property="og:image" content="http://platform.wotmed.com/WotmedFacebookLogo2015.png">
        
	</head>
	<body onload="searchUsers()">
		<div class="topBar" >
			<div class="bar_frame">
				<div class="w_logo"><img src="images/WotmedLogoTransparent.png" height="45" alt="wotmedLogo" style='position:relative; left:-30px; padding:0px;padding-top:10px;' /></div>
				<div class="div_login">
					<form id="loginForm" method="post" action="login.php">
					<table>
						<tr>
							<td><label>Email</label></td>
							<td><label>Password</label></td>
						</tr>
						<tr>
							<td width="150"><input type="text" class="inputtext required email" name="email" id="email"/></td>
							<td width="150"><input type="password" class="inputpassword required" name="password" id="password"/></td>
							<td><input type="submit" value="Login"/></td>
						</tr>
						<tr>
							<td><input name="persistent" type="checkbox" style="float:left; margin: 0px; padding: 0px;"/> <label>Keep me logged in</label></td>
							<td><a href="resetPassword.php"><label style="cursor:pointer;text-decoration:underline;">Forget your password?</label></a></td>
                            
                           
						</tr>
                        
					</table>
					</form>
				<div id="errMsg">
				</div>
				</div>
				<div>
					<?php //include "fbConnect.php"; ?>
				</div>
				<div>
					<?php //include "googleConnect.php"; ?>
				</div>
			</div>
		</div>



        <div id="container">


            <h3><a href="#" onclick="return false;" class="about">Wotmed is the medical network that connects Patients with Practitioners and Surgery Facilitators globally</a></h3>
            <p>
                &nbsp;</p>

            <div id="system">

                <article class="item">
                    <div class="content clearfix">


                        <h1><strong>Wotmed Client Speech Submissions</strong></h1>

                        <p> <b>Please ensure that you have any ad blocking software turned off to view this page correctly.</b></p>
                        <img src="WotmedPractitionerSpeechesIcon.jpg" alt="Wotmed Practitioner Speeches" width="256" height="256" />

                        <p> This section of the site is dedicated to empowering you as a practitioner or surgery facilitator to submit a speech.</p>
                        <p> Your speeches may be of great value to other patients around the world who are considering undergoing surgery.</p>
                        <p> Please note that the Wotmed Team moderates all client speech submissions before publishing.</p>

                        <p> We will publish your speech along with your name and practice / business details.</p>
                        <p> Your email address will not be published anywhere on the site.</p>
                        <p> If you choose to be contactable other practitioners, surgery facilitators and patients will be able to contact you via a contact form.</p>
                        <p> <b>We recommend that you write your speech transcript in your favourite word processor and then copy the text in here just in case you encounter internet connection issues or other difficulties submitting your speech transcript.</b></p>
                        <p> </p>

                        <br>
                        <p> You can view other Wotmed Client Speeches by

                            <a href="WotmedClientSpeeches.php"><label style="cursor:pointer;text-decoration:underline;">clicking here</label></a>
                            <br><br>

                        <p> <b> Please follow the steps below to upload your speech video and images</b> </p><br>
                        <p> <b> Important: Please upload one video and image at a time and wait until each has successfully uploaded</b> </p><br>

                        <p> <b> Step 1:</b> Upload your Speech Video</p>

                        <form id="form1" method="POST" >
                            <?php
                            $uploader=new PhpUploader();
                            $uploader->MaxSizeKB=100000;
                            $uploader->Name="myuploader";
                            $uploader->ProgressInfoStyle="padding-left:3px;font:normal 12px Helvetica Neue Light;";
                            $uploader->UploadingMsg="Uploading Video Advertisement..";
                            $uploader->InsertText="Select Video (Max 100M)";
                            $uploader->AllowedFileExtensions="*.mp4,*.flv,*.f4v";
                            $uploader->MultipleFilesUpload=false;
                            $uploader->InsertButtonID="uploadbutton";
                            $uploader->CancelButtonID="uploadercancelbutton";
                            $uploader->ProgressPicture="phpfileuploader2015/phpuploader/resources/VideoUploadProgressButton.png";
                            $uploader->Render();
                            ?>
                            <img id="uploadbutton" alt="Upload Speech Video" src="uploadVideoAd.png" />
                            <img id="uploadercancelbutton" alt="Cancel Upload" src="uploadVideoAdCancel.png" />

                        </form>


                        <?php
                        //Gets the GUID of the file based on uploader name
                        $fileguid=@$_POST["myuploader"];
                        if($fileguid)
                        {
                            //get the uploaded file based on GUID
                            $mvcfile=$uploader->GetUploadedFile($fileguid);
                            if($mvcfile)
                            {
                                //Gets the name of the file.
                                echo "<BR>";
                                echo "Video file name: ";
                                echo($mvcfile->FileName);
                                echo "<BR>";

                                $videoFileName = $mvcfile->FileName;

                                $_SESSION["VideoFileNameIs"] = $videoFileName;

                                // echo $_SESSION["VideoFileNameIs"];

                                //Gets the temp file path.
                                //  echo($mvcfile->FilePath);
                                //   echo "<BR><BR>";

                                //Gets the size of the file.
                                echo "Video file size: ";
                                echo($mvcfile->FileSize);
                                echo " bytes";
                                echo "<BR><BR>";

                                $videoFileSize = $mvcfile->FileSize;

                                //Success message
                                echo "Your Video Advertisement was successfully uploaded to Wotmed.";
                                echo "<BR><BR>";

                                //Moves the uploaded file to a new location.
                                $mvcfile->MoveTo("ImageUpload/uploads/clientspeeches");

                            }
                        }
                        ?>

                        <p> <b> Step 2:</b> Upload a Photo of yourself</p>

                        <form id="form2" method="POST" >
                            <?php
                            $uploader=new PhpUploader();
                            $uploader->MaxSizeKB=10000;
                            $uploader->Name="myuploader2";
                            $uploader->ProgressInfoStyle="padding-left:3px;font:normal 12px Helvetica Neue Light;";
                            $uploader->UploadingMsg="Uploading Photo..";
                            $uploader->InsertText="Upload a photo of yourself";
                            $uploader->AllowedFileExtensions="*.jpg,*.gif,*.png,*.bmp";
                            $uploader->MultipleFilesUpload=false;
                            $uploader->InsertButtonID="uploadbuttonphoto";
                            $uploader->CancelButtonID="uploadercancelbuttonphoto";
                            $uploader->ProgressPicture="phpfileuploader2015/phpuploader/resources/VideoUploadProgressButton.png";
                            $uploader->Render();
                            ?>
                            <img id="uploadbuttonphoto" alt="Upload Photo" src="uploadClientPhoto2.jpg" />
                            <img id="uploadercancelbuttonphoto" alt="Cancel Upload" src="uploadVideoAdCancel.png" />

                        </form>


                        <?php
                        //Gets the GUID of the file based on uploader name
                        $fileguid=@$_POST["myuploader2"];
                        if($fileguid)
                        {
                            //get the uploaded file based on GUID
                            $mvcfile=$uploader->GetUploadedFile($fileguid);
                            if($mvcfile)
                            {
                                //Gets the name of the file.
                                echo "<BR>";
                                echo "Your photo filename: ";
                                echo($mvcfile->FileName);
                                echo "<BR>";

                                $photoFileName = $mvcfile->FileName;

                                $_SESSION["PhotoFileNameIs"] = $photoFileName;

                                //Gets the size of the file.
                                echo "Photo file size: ";
                                echo($mvcfile->FileSize);
                                echo " bytes";
                                echo "<BR><BR>";

                                $photoFileSize = $mvcfile->FileName;

                                //Success message
                                echo "Your photo was successfully uploaded to Wotmed.";
                                echo "<BR><BR>";

                                //Moves the uploaded file to a new location.
                                $mvcfile->MoveTo("ImageUpload/uploads/clientspeeches");

                            }
                        }
                        ?>

    <p> <b> Step 3:</b> Upload your practice or business logo</p>

    <form id="form3" method="POST" >
        <?php
        $uploader=new PhpUploader();
        $uploader->MaxSizeKB=10000;
        $uploader->Name="myuploader3";
        $uploader->ProgressInfoStyle="padding-left:3px;font:normal 12px Helvetica Neue Light;";
        $uploader->UploadingMsg="Uploading Practice or Business Logo..";
        $uploader->InsertText="Upload your Practice or Business Logo";
        $uploader->AllowedFileExtensions="*.jpg,*.gif,*.png,*.bmp";
        $uploader->MultipleFilesUpload=false;
        $uploader->InsertButtonID="uploadbuttonlogo";
        $uploader->CancelButtonID="logouploadercancelbuttonphoto";
        $uploader->ProgressPicture="phpfileuploader2015/phpuploader/resources/VideoUploadProgressButton.png";
        $uploader->Render();
        ?>
        <img id="uploadbuttonlogo" alt="Upload Logo" src="uploadLogo.png" />
        <img id="logouploadercancelbuttonphoto" alt="Cancel Upload" src="uploadVideoAdCancel.png" />

    </form>


<?php
//Gets the GUID of the file based on uploader name
$fileguid=@$_POST["myuploader3"];
if($fileguid)
{
    //get the uploaded file based on GUID
    $mvcfile=$uploader->GetUploadedFile($fileguid);
    if($mvcfile)
    {
        //Gets the name of the file.
        echo "<BR>";
        echo "Your practice or business logo filename: ";
        echo($mvcfile->FileName);
        echo "<BR>";

        $practiceOrBusinessLogoFileName = $mvcfile->FileName;

        $_SESSION["PracticeOrBusinessFileNameIs"] = $practiceOrBusinessLogoFileName;

        //Gets the size of the file.
        echo "logo file size: ";
        echo($mvcfile->FileSize);
        echo " bytes";
        echo "<BR><BR>";

        $practiceOrBusinessLogoFileSize = $mvcfile->FileSize;

        //Success message
        echo "Your practice or business logo was successfully uploaded to Wotmed.";
        echo "<BR><BR>";

        //Moves the uploaded file to a new location.
        $mvcfile->MoveTo("ImageUpload/uploads/clientspeeches");

    }
}
?>

                        <p> <b> Step 4:</b> Upload a relevant image to accompany your video which will be placed on your speech page to help market your video</p>

                        <form id="form4" method="POST" >
                            <?php
                            $uploader=new PhpUploader();
                            $uploader->MaxSizeKB=10000;
                            $uploader->Name="myuploader4";
                            $uploader->ProgressInfoStyle="padding-left:3px;font:normal 12px Helvetica Neue Light;";
                            $uploader->UploadingMsg="Uploading Marketing Image..";
                            $uploader->InsertText="Upload your a marketing image";
                            $uploader->AllowedFileExtensions="*.jpg,*.gif,*.png,*.bmp";
                            $uploader->MultipleFilesUpload=false;
                            $uploader->InsertButtonID="uploadbuttonmarketing";
                            $uploader->CancelButtonID="marketinguploadercancelbuttonphoto";
                            $uploader->ProgressPicture="phpfileuploader2015/phpuploader/resources/VideoUploadProgressButton.png";
                            $uploader->Render();
                            ?>
                            <img id="uploadbuttonmarketing" alt="Upload Marketing Image" src="uploadMarketingImage.png" />
                            <img id="marketinguploadercancelbuttonphoto" alt="Cancel Upload" src="uploadVideoAdCancel.png" />

                        </form>


                        <?php
                        //Gets the GUID of the file based on uploader name
                        $fileguid=@$_POST["myuploader4"];
                        if($fileguid)
                        {
                            //get the uploaded file based on GUID
                            $mvcfile=$uploader->GetUploadedFile($fileguid);
                            if($mvcfile)
                            {
                                //Gets the name of the file.
                                echo "<BR>";
                                echo "Your marketing image filename: ";
                                echo($mvcfile->FileName);
                                echo "<BR>";

                                $practiceOrBusinessMarketingImage = $mvcfile->FileName;

                                $_SESSION["PracticeOrBusinessMarketingImageFileNameIs"] = $practiceOrBusinessMarketingImage;

                                //Gets the size of the file.
                                echo "Image file size: ";
                                echo($mvcfile->FileSize);
                                echo " bytes";
                                echo "<BR><BR>";

                                $practiceOrBusinessMarketingImageFileSize = $mvcfile->FileSize;

                                //Success message
                                echo "Your marketing image was successfully uploaded to Wotmed.";
                                echo "<BR><BR>";

                                //Moves the uploaded file to a new location.
                                $mvcfile->MoveTo("ImageUpload/uploads/clientspeeches");


                            }



                        }



                        ?>

                        <p> <b> Step 5:</b> Upload an image to use as a thumbnail for your speech</p>

                        <form id="form5" method="POST" >
                            <?php
                            $uploader=new PhpUploader();
                            $uploader->MaxSizeKB=10000;
                            $uploader->Name="myuploader5";
                            $uploader->ProgressInfoStyle="padding-left:3px;font:normal 12px Helvetica Neue Light;";
                            $uploader->UploadingMsg="Uploading Video Thumbnail Image..";
                            $uploader->InsertText="Upload a video thumbnail image";
                            $uploader->AllowedFileExtensions="*.jpg,*.gif,*.png,*.bmp";
                            $uploader->MultipleFilesUpload=false;
                            $uploader->InsertButtonID="uploadbuttonthumb";
                            $uploader->CancelButtonID="thumbuploadercancelbuttonphoto";
                            $uploader->ProgressPicture="phpfileuploader2015/phpuploader/resources/VideoUploadProgressButton.png";
                            $uploader->Render();
                            ?>
                            <img id="uploadbuttonthumb" alt="Upload Video Thumbnail Image" src="uploadVideoThumb.png" />
                            <img id="thumbuploadercancelbuttonphoto" alt="Cancel Upload" src="uploadVideoAdCancel.png" />

                        </form>


                        <?php
                        //Gets the GUID of the file based on uploader name
                        $fileguid=@$_POST["myuploader5"];
                        if($fileguid)
                        {
                            //get the uploaded file based on GUID
                            $mvcfile=$uploader->GetUploadedFile($fileguid);
                            if($mvcfile)
                            {
                                //Gets the name of the file.
                                echo "<BR>";
                                echo "Your video thumb filename: ";
                                echo($mvcfile->FileName);
                                echo "<BR>";

                                $practiceOrBusinessThumbImage = $mvcfile->FileName;

                                $_SESSION["PracticeOrBusinessThumbImageFileNameIs"] = $practiceOrBusinessThumbImage;

                                //Gets the size of the file.
                                echo "Thumbnail file size: ";
                                echo($mvcfile->FileSize);
                                echo " bytes";
                                echo "<BR><BR>";

                                $practiceOrBusinessThumbImageFileSize = $mvcfile->FileSize;

                                //Success message
                                echo "Your video thumb image was successfully uploaded to Wotmed.";
                                echo "<BR><BR>";

                                //Moves the uploaded file to a new location.
                                $mvcfile->MoveTo("ImageUpload/uploads/clientspeeches");


                            }



                        }



                        ?>

                        <p> <b> Step 6:</b> Please fill out the form below</p>
                        <p> <font color="red">* </font>Mandatory fields are marked with an asterisk<p><br>

                        <BR><BR>
                        <form method="post" action="/proposals/proposalsubmit/WotmedClientSpeechSubmission.php" name="requestforProposal" id="requestforProposal">
                            <table width="922" border="0">




                                 <tr>
                                        <td width="282"> <p> <font color="red">* </font>Name</td>
                                <td width="508"><label>
                                        <input name="SpeechSubmitterName" type="text" id="SpeechSubmitterName" size="70" required/>
                                    </label></td>
                                </tr>

                                <tr>
                                    <td width="282"> <p> <font color="red">* </font>Position</td>
                                    <td width="508"><label>
                                            <input name="SpeechSubmitterPosition" type="text" id="SpeechSubmitterPosition" size="70" required/>
                                        </label></td>
                                </tr>

                                <tr>
                                    <td width="282"> <p> <font color="red">* </font>Short phrase/blub you would like to accompany your speech</td>
                                    <td width="508"><label>
                                            <input name="SpeechShortPhraseToRemember" type="text" id="SpeechShortPhraseToRemember" size="70" required/>
                                        </label></td>
                                </tr>




                                <tr>
                                    <td>What country are you from?</td>
                                    <td><select name="SpeechSubmitterCountry" id="SpeechSubmitterCountry" type="text">
                                            <option selected="Please select your country">Please select your country</option>
                                            <option value="Afghanistan">Afghanistan</option>
                                            <option value="Albania">Albania</option>
                                            <option value="Algeria">Algeria</option>
                                            <option value="American Samoa">American Samoa</option>
                                            <option value="Andorra">Andorra</option>
                                            <option value="Angola">Angola</option>
                                            <option value="Anguilla">Anguilla</option>
                                            <option value="Antarctica">Antarctica</option>
                                            <option value="Antigua and Barbuda">Antigua and Barbuda</option>
                                            <option value="Argentina">Argentina</option>
                                            <option value="Armenia">Armenia</option>
                                            <option value="Aruba">Aruba</option>
                                            <option value="Australia">Australia</option>
                                            <option value="Austria">Austria</option>
                                            <option value="Azerbaijan">Azerbaijan</option>
                                            <option value="Bahamas">Bahamas</option>
                                            <option value="Bahamas The">Bahamas The</option>
                                            <option value="Bahrain">Bahrain</option>
                                            <option value="Bangladesh">Bangladesh</option>
                                            <option value="Barbados">Barbados</option>
                                            <option value="Belarus">Belarus</option>
                                            <option value="Belgium">Belgium</option>
                                            <option value="Belize">Belize</option>
                                            <option value="Benin">Benin</option>
                                            <option value="Bermuda">Bermuda</option>
                                            <option value="Bhutan">Bhutan</option>
                                            <option value="Bolivia">Bolivia</option>
                                            <option value="Bosnia">Bosnia</option>
                                            <option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
                                            <option value="Botswana">Botswana</option>
                                            <option value="Brazil">Brazil</option>
                                            <option value="British Virgin Islands">British Virgin Islands</option>
                                            <option value="Brunei">Brunei</option>
                                            <option value="Bulgaria">Bulgaria</option>
                                            <option value="Burkina Faso">Burkina Faso</option>
                                            <option value="Burundi">Burundi</option>
                                            <option value="Cambodia">Cambodia</option>
                                            <option value="Cameroon">Cameroon</option>
                                            <option value="Canada">Canada</option>
                                            <option value="Cape Verde">Cape Verde</option>
                                            <option value="Cayman Islands">Cayman Islands</option>
                                            <option value="Central African Republic">Central African Republic</option>
                                            <option value="Chad">Chad</option>
                                            <option value="Chile">Chile</option>
                                            <option value="China">China</option>
                                            <option value="Christmas Island">Christmas Island</option>
                                            <option value="Colombia">Colombia</option>
                                            <option value="Comoros">Comoros</option>
                                            <option value="Congo">Congo</option>
                                            <option value="Congo Democratic Republic of the">Congo Democratic Republic of the</option>
                                            <option value="Cook Islands">Cook Islands</option>
                                            <option value="Costa Rica">Costa Rica</option>
                                            <option value="Cote d'Ivoire">Cote d'Ivoire</option>
                                            <option value="Croatia">Croatia</option>
                                            <option value="Cuba">Cuba</option>
                                            <option value="Curacao">Curacao</option>
                                            <option value="Cyprus">Cyprus</option>
                                            <option value="Czech Republic">Czech Republic</option>
                                            <option value="Denmark">Denmark</option>
                                            <option value="Djibouti">Djibouti</option>
                                            <option value="Dominica">Dominica</option>
                                            <option value="Dominican Republic">Dominican Republic</option>
                                            <option value="East Timor">East Timor</option>
                                            <option value="Ecuador">Ecuador</option>
                                            <option value="Egypt">Egypt</option>
                                            <option value="El Salvador">El Salvador</option>
                                            <option value="Equatorial Guinea">Equatorial Guinea</option>
                                            <option value="Eritrea">Eritrea</option>
                                            <option value="Estonia">Estonia</option>
                                            <option value="Ethiopia">Ethiopia</option>
                                            <option value="European Union">European Union</option>
                                            <option value="Falkland Islands">Falkland Islands</option>
                                            <option value="Faroe Islands">Faroe Islands</option>
                                            <option value="Federated States of Micronesia">Federated States of Micronesia</option>
                                            <option value="Fiji">Fiji</option>
                                            <option value="Finland">Finland</option>
                                            <option value="France">France</option>
                                            <option value="Gabon">Gabon</option>
                                            <option value="Gambia">Gambia</option>
                                            <option value="Gambia The">Gambia The</option>
                                            <option value="Georgia">Georgia</option>
                                            <option value="Germany">Germany</option>
                                            <option value="Ghana">Ghana</option>
                                            <option value="Gibraltar">Gibraltar</option>
                                            <option value="Greece">Greece</option>
                                            <option value="Greenland">Greenland</option>
                                            <option value="Grenada">Grenada</option>
                                            <option value="Guadeloupe">Guadeloupe</option>
                                            <option value="Guam">Guam</option>
                                            <option value="Guatemala">Guatemala</option>
                                            <option value="Guernsey">Guernsey</option>
                                            <option value="Guinea">Guinea</option>
                                            <option value="Guinea Bissau">Guinea Bissau</option>
                                            <option value="Guyana">Guyana</option>
                                            <option value="Haiti">Haiti</option>
                                            <option value="Herzegovina">Herzegovina</option>
                                            <option value="Honduras">Honduras</option>
                                            <option value="Hong Kong">Hong Kong</option>
                                            <option value="Hungary">Hungary</option>
                                            <option value="Iceland">Iceland</option>
                                            <option value="India">India</option>
                                            <option value="Indonesia">Indonesia</option>
                                            <option value="Iran">Iran</option>
                                            <option value="Iraq">Iraq</option>
                                            <option value="Ireland">Ireland</option>
                                            <option value="Isle Of Man">Isle Of Man</option>
                                            <option value="Israel">Israel</option>
                                            <option value="Italy">Italy</option>
                                            <option value="Jamaica">Jamaica</option>
                                            <option value="Japan">Japan</option>
                                            <option value="Jersey">Jersey</option>
                                            <option value="Jordan">Jordan</option>
                                            <option value="Kazakhstan">Kazakhstan</option>
                                            <option value="Kenya">Kenya</option>
                                            <option value="Kiribati">Kiribati</option>
                                            <option value="KoreaSouth">KoreaSouth</option>
                                            <option value="Kosovo">Kosovo</option>
                                            <option value="Kuwait">Kuwait</option>
                                            <option value="Kyrgyzstan">Kyrgyzstan</option>
                                            <option value="Laos">Laos</option>
                                            <option value="Latvia">Latvia</option>
                                            <option value="Lebanon">Lebanon</option>
                                            <option value="Lesotho">Lesotho</option>
                                            <option value="Liberia">Liberia</option>
                                            <option value="Libya">Libya</option>
                                            <option value="Liechtenstein">Liechtenstein</option>
                                            <option value="Lithuania">Lithuania</option>
                                            <option value="Luxembourg">Luxembourg</option>
                                            <option value="Macau">Macau</option>
                                            <option value="Macedonia">Macedonia</option>
                                            <option value="Macedonia Republic of">Macedonia Republic of</option>
                                            <option value="Madagascar">Madagascar</option>
                                            <option value="Maianmar">Maianmar</option>
                                            <option value="Malawi">Malawi</option>
                                            <option value="Malaysia">Malaysia</option>
                                            <option value="Maldives">Maldives</option>
                                            <option value="Mali">Mali</option>
                                            <option value="Malta">Malta</option>
                                            <option value="Marshall Islands">Marshall Islands</option>
                                            <option value="Mauritania">Mauritania</option>
                                            <option value="Mauritius">Mauritius</option>
                                            <option value="Mexico">Mexico</option>
                                            <option value="Micronesia">Micronesia</option>
                                            <option value="Moldova">Moldova</option>
                                            <option value="Monaco">Monaco</option>
                                            <option value="Mongolia">Mongolia</option>
                                            <option value="Mont Serrat">Mont Serrat</option>
                                            <option value="Montenegro">Montenegro</option>
                                            <option value="Morocco">Morocco</option>
                                            <option value="Mozambique">Mozambique</option>
                                            <option value="Myanmar">Myanmar</option>
                                            <option value="Myanmar (Burma)">Myanmar (Burma)</option>
                                            <option value="Namibia">Namibia</option>
                                            <option value="Nauru">Nauru</option>
                                            <option value="Nepal">Nepal</option>
                                            <option value="Netherlands">Netherlands</option>
                                            <option value="New Caledonia">New Caledonia</option>
                                            <option value="New Zealand">New Zealand</option>
                                            <option value="Nicaragua">Nicaragua</option>
                                            <option value="Niger">Niger</option>
                                            <option value="Nigeria">Nigeria</option>
                                            <option value="Niue">Niue</option>
                                            <option value="Norfolk Island">Norfolk Island</option>
                                            <option value="North Korea">North Korea</option>
                                            <option value="Northern Cyprus">Northern Cyprus</option>
                                            <option value="Northern Mariana Islands">Northern Mariana Islands</option>
                                            <option value="Norway">Norway</option>
                                            <option value="Oman">Oman</option>
                                            <option value="Pakistan">Pakistan</option>
                                            <option value="Palau">Palau</option>
                                            <option value="Palestinian Territories">Palestinian Territories</option>
                                            <option value="Panama">Panama</option>
                                            <option value="Papua New Guinea">Papua New Guinea</option>
                                            <option value="Paraguay">Paraguay</option>
                                            <option value="Peru">Peru</option>
                                            <option value="Philippines">Philippines</option>
                                            <option value="Pitcairn Islands">Pitcairn Islands</option>
                                            <option value="Poland">Poland</option>
                                            <option value="Portugal">Portugal</option>
                                            <option value="Puerto Rico">Puerto Rico</option>
                                            <option value="Qatar">Qatar</option>
                                            <option value="Republic of Macedonia">Republic of Macedonia</option>
                                            <option value="Romania">Romania</option>
                                            <option value="Russia">Russia</option>
                                            <option value="Rwanda">Rwanda</option>
                                            <option value="Saint Helena Ascension Tristan Da Cunha">Saint Helena Ascension Tristan Da Cunha</option>
                                            <option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
                                            <option value="Saint Lucia">Saint Lucia</option>
                                            <option value="Saint Vincent and the Grenadines">Saint Vincent and the Grenadines</option>
                                            <option value="Samoa">Samoa</option>
                                            <option value="San Marino">San Marino</option>
                                            <option value="Sao Tone and Preincipe">Sao Tone and Preincipe</option>
                                            <option value="Saudi Arabia">Saudi Arabia</option>
                                            <option value="Senegal">Senegal</option>
                                            <option value="Serbia">Serbia</option>
                                            <option value="Seychelles">Seychelles</option>
                                            <option value="Sierra Leone">Sierra Leone</option>
                                            <option value="Singapore">Singapore</option>
                                            <option value="Sint Maarten">Sint Maarten</option>
                                            <option value="Slovakia">Slovakia</option>
                                            <option value="Slovenia">Slovenia</option>
                                            <option value="Solomon Islands">Solomon Islands</option>
                                            <option value="Somalia">Somalia</option>
                                            <option value="South Africa">South Africa</option>
                                            <option value="South Korea">South Korea</option>
                                            <option value="Spain">Spain</option>
                                            <option value="Sri Lanka">Sri Lanka</option>
                                            <option value="St Lucia">St Lucia</option>
                                            <option value="St. Helena">St. Helena</option>
                                            <option value="St. Pierre and Miquelon">St. Pierre and Miquelon</option>
                                            <option value="Sudan South Sudan">Sudan South Sudan</option>
                                            <option value="Suriname">Suriname</option>
                                            <option value="Swaziland">Swaziland</option>
                                            <option value="Sweden">Sweden</option>
                                            <option value="Switzerland">Switzerland</option>
                                            <option value="Syria">Syria</option>
                                            <option value="Taiwan">Taiwan</option>
                                            <option value="Tajikistan">Tajikistan</option>
                                            <option value="Tanzania">Tanzania</option>
                                            <option value="Tasmania">Tasmania</option>
                                            <option value="Thailand">Thailand</option>
                                            <option value="Tobago">Tobago</option>
                                            <option value="Togo">Togo</option>
                                            <option value="Tokelau">Tokelau</option>
                                            <option value="Tonga">Tonga</option>
                                            <option value="Trinidad">Trinidad</option>
                                            <option value="Trinidad and Tobago">Trinidad and Tobago</option>
                                            <option value="Tunisia">Tunisia</option>
                                            <option value="Turkey">Turkey</option>
                                            <option value="Turkmenistan">Turkmenistan</option>
                                            <option value="Turks and Caicos Islands">Turks and Caicos Islands</option>
                                            <option value="Tuvalu">Tuvalu</option>
                                            <option value="Tuvalu">Tuvalu</option>
                                            <option value="Uganda">Uganda</option>
                                            <option value="Ukraine">Ukraine</option>
                                            <option value="United Arab Emirates">United Arab Emirates</option>
                                            <option value="United Kingdom">United Kingdom</option>
                                            <option value="United States">United States</option>
                                            <option value="Uruguay">Uruguay</option>
                                            <option value="US Virgin Islands">US Virgin Islands</option>
                                            <option value="Uzbekistan">Uzbekistan</option>
                                            <option value="Vanuatu">Vanuatu</option>
                                            <option value="Vatican City">Vatican City</option>
                                            <option value="Venezuela">Venezuela</option>
                                            <option value="Vietnam">Vietnam</option>
                                            <option value="Virgin Islands">Virgin Islands</option>
                                            <option value="Wallis and Futuna Islands">Wallis and Futuna Islands</option>
                                            <option value="Western Sahara">Western Sahara</option>
                                            <option value="Yemen">Yemen</option>
                                            <option value="Zambia">Zambia</option>
                                            <option value="Zimbabwe">Zimbabwe</option>
                                        </select>
                                        </label></td>
                                </tr>

                                <tr>
                                    <td width="282"> <p> <font color="red">* </font>Contact Phone Number</td>
                                    <td width="508"><label>
                                            <input name="SpeechSubmitterPhoneNumber" type="text" id="SpeechSubmitterPhoneNumber" size="70" required/>
                                        </label></td>
                                </tr>

                                <tr>
                                    <td width="282"> <p> <font color="red">* </font>Email Address</td>
                                    <td width="508"><label>
                                            <input type="email" name="SpeechSubmitterEmailAddress" id="SpeechSubmitterEmailAddress" size="40" data-parsley-trigger="change" required />
                                        </label></td>
                                </tr>

                                <tr>
                                    <td width="282"> <p> <font color="red">* </font>Speech Title</td>
                                    <td width="508"><label>
                                            <input name="SpeechTitle" type="text" id="SpeechTitle" size="70" required/>
                                        </label></td>
                                </tr>

                                <tr>
                                    <td width="282"> <p> <font color="red">* </font>Speech Subject</td>
                                    <td width="508"><label>
                                            <input name="SpeechSubject" type="text" id="SpeechSubject" size="70" required/>
                                        </label></td>
                                </tr>


                                <tr>
                                    <td>Would you like to give other practitioners, surgery facilitators and patients the ability to contact you regarding your speech?</td>

                                    <td><label>
                                            <select name="IsSpeakerContactable" id="IsSpeakerContactable" type="text">
                                                <option value="Yes">Yes</option>
                                                <option value="No">No</option>
                                            </select>
                                        </label></td>
                                </tr>

                                <tr>
                                    <td width="282"> <p> <font color="red">* </font>Speech SEO Keywords (Enter keywords from your speech separated by commas) eg: Thailand, Cosmetic Surgery, Rhinoplasty</td>
                                    <td width="508"><label>
                                            <input name="SpeechSEOKeywords" type="text" id="SpeechSEOKeywords" size="70" required/>
                                        </label></td>
                                </tr>

                                <tr>
                                    <td>Speech Transcript</td>
                                    <td><label>
                                            <textarea name="SpeechTranscript" id="SpeechTranscript" cols="65" rows="10"></textarea>
                                        </label></td>
                                </tr>

                                <tr>

                                    <td><b>We will place your marketing details along with your speech</b> </td>

                                </tr>


                                <tr>
                                    <td width="282"> <p> <font color="red">* </font>Practice or Business Name</td>
                                    <td width="508"><label>
                                            <input name="PracticeOrBusinessName" type="text" id="PracticeOrBusinessName" size="70" required/>
                                        </label></td>
                                </tr>

                                <tr>
                                    <td width="282"> <p> <font color="red">* </font>Practice or Business Speciality</td>
                                    <td width="508"><label>
                                            <input name="PracticeOrBusinessSpeciality" type="text" id="PracticeOrBusinessSpeciality" size="70" required/>
                                        </label></td>
                                </tr>

                                <tr>
                                    <td width="282"> <p> <font color="red">* </font>Practice or Business Phone Number</td>
                                    <td width="508"><label>
                                            <input name="PracticeOrBusinessPhone" type="text" id="PracticeOrBusinessPhone" size="70" required/>
                                        </label></td>
                                </tr>

                                <tr>
                                    <td width="282"> <p> <font color="red">* </font>Practice or Business Address</td>
                                    <td width="508"><label>
                                            <input name="PracticeOrBusinessAddress" type="text" id="PracticeOrBusinessAddress" size="70" required/>
                                        </label></td>
                                </tr>

                                <tr>
                                    <td width="282"> <p> <font color="red">* </font>Practice or Business Independent Website Hyperlink</td>
                                    <td width="508"><label>
                                            <input name="PracticeOrBusinessIndependentWebsiteHyperlink" type="text" id="PracticeOrBusinessIndependentWebsiteHyperlink" size="70" required/>
                                        </label></td>
                                </tr>


                                <tr>
                                    <td>Practice or Business Marketing Blurb</td>
                                    <td><input name="PracticeOrBusinessMarketingBlurb" type="text" id="PracticeOrBusinessMarketingBlurb" size="70"></td>
                                    </td>
                                </tr>



                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>


                            </table>

                            <p>
                                By submitting any material to Wotmed, you automatically grant the royalty-free, perpetual, irrevocable, exclusive right and license to use, reproduce, modify, edit, adapt, publish, translate, create derivative works from, distribute, perform and display such material (in whole or part) worldwide and/or to incorporate it in other works in any form, media, or technology now known or later developed for the full term of any rights that may exist in such content. You acknowledge that Wotmed is not obliged to publish any material submitted by you.
                            </p>

                            <p>
                                <label>
                                    <input type="submit" name="ClientSpeechSubmission" id='signupsubmit' value="Submit your Speech" " />

                                </label>
                            </p>
                            <p>&nbsp;</p>
                        </form>

                    </div>



            <p>&nbsp;</p>



        </div>


			<p>&nbsp;</p>
			<p>&nbsp;</p>
			<?php include "includes/p_footer.php"; ?>
