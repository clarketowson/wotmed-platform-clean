<?php
	/*if ($_SERVER['HTTPS'] != "on") {
		$url = "https://". $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
		header("location: $url");
	}  */

	session_start();
	include("includes/connect.php");
	include("includes/functions.php");
	
	if(isset($_SESSION['id']))
	{
		$redirect="profile.php";
		if(isset($_SESSION['practitioner_id']))
			$redirect='practitioner_profile.php';
		redirect($redirect);
	}
	elseif(isset($_POST['email']))
	{
		$inputtext=$_POST['inputtext'];
	$inputpassword=$_POST['inputpassword'];
		login($conn,$inputtext,$inputpassword);
	}
?>



<!DOCTYPE html PUBLIC
"-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
		<title>Welcome to Wotmed</title>
		<meta http-equiv="Content-Type" content="'text'/html; charset=utf-8" />
        
        
 <script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-38343871-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-49954656-1', 'wotmed.com');
		  ga('send', 'pageview');

		</script>

       
		<script type="text/javascript" src="classes/jquery.js"></script>
		<script type="text/javascript" src="classes/hover.js"></script>
		<script src="classes/jquery.validation.js"></script>
		<script src="classes/dateSelectBoxes.js"></script>

    <link href="style/i_style.css" rel="stylesheet"></link>

    <link rel="stylesheet" type="text/css" href="signup.css">

    <link rel="stylesheet" href="/proposals/date/css/pikaday.css">

 <!--   <link rel="stylesheet" href="/proposals/date/css/site.css"> -->


 <!--   <style type="text/css">
        <!--
        #PatientBudget {	font-size:14px;
        }
        #PatientCountries {	font-size:14px;
        }
        #ProcedureList {
            font-size:14px;
        }
        #Submit {	background-color:#125c5d;
            -moz-border-radius:1px;
            -webkit-border-radius:1px;
            border-radius:1px;
            display:inline-block;
            cursor:pointer;
            color:#ffffff;
            font-family:arial;
            font-size:22px;
            padding:12px 50px;
            text-decoration:none;
            text-shadow:0px 1px 0px #125c5d;
        }

    </style> -->



    <meta name="description" content="Wotmed.com - Packaging Medical Tourism">
    <meta name="keywords" content="Wotmed.com - Packaging Medical Tourism.  Cheap Overseas Dental Care, Cheap overseas Dentists, Cheap Overseas Doctors, Overseas Cosmetic Surgery, Overseas Surgery, Overseas Dental Surgery, Tummy Tucks overseas, Boob Jobs overseas, Breast Augmentation overseas, Mummy Makeovers">

    <meta property="og:locale" content="en_US">
    <meta property="og:type" content="website">
    <meta property="og:title" content="Welcome to Wotmed">
    <meta property="og:description" content="Wotmed.com - Packaging Medical Tourism">
    <meta property="og:site_name" content="Wotmed">
    <meta property="og:url" content="http://platform.wotmed.com">

    <meta property="og:image" content="http://platform.wotmed.com/WotmedFacebookLogo6.png">
        
	</head>
	<body onload="searchUsers()">
		<div class="topBar" >
			<div class="bar_frame">
				<div class="w_logo"><img src="images/WotmedLogoTransparent.png" height="45" alt="wotmedLogo" style='position:relative; left:-30px; padding:0px;padding-top:10px;' /></div>
				<div class="div_login">
					<form id="loginForm" method="post" action="login.php">
					<table>
						<tr>
							<td><label>Email</label></td>
							<td><label>Password</label></td>
						</tr>
						<tr>
							<td width="150"><input type="text" class="inputtext required email" name="email" id="email"/></td>
							<td width="150"><input type="password" class="inputpassword required" name="password" id="password"/></td>
							<td><input type="submit" value="Login"/></td>
						</tr>
						<tr>
							<td><input name="persistent" type="checkbox" style="float:left; margin: 0px; padding: 0px;"/> <label>Keep me logged in</label></td>
							<td><a href="resetPassword.php"><label style="cursor:pointer;text-decoration:underline;">Forget your password?</label></a></td>
                            
                           
						</tr>
                        
					</table>
					</form>
				<div id="errMsg">
				</div>
				</div>
				<div>
					<?php //include "fbConnect.php"; ?>
				</div>
				<div>
					<?php //include "googleConnect.php"; ?>
				</div>
			</div>
		</div>



		<div id="container">


            <h3><a href="#" onclick="return false;" class="about">Wotmed is the medical network that connects Patients with Practitioners and Surgery Facilitators globally</a></h3>
            <p>
                &nbsp;</p>

            <div id="system">


                <article class="item">




                    <div class="content clearfix">


                        <h1><strong>Wotmed Practitioner Disclaimer</strong></h1>
                        <p>By signing up to Wotmed you agree to the following</p>


                        General Provisions.

                        Wotmed.com is a medical network that connects patients with medical professionals and surgery facilitators globally.

                        Wotmed.com helps facilitate the relationship between Practitioners, Patients and Surgery Facilitators.

                        Wotmed.com does not employ or endorse any of the Patients who have accounts on the Wotmed.com platform

                        Wotmed.com will never advise nor recommend to you any particular Patient who is present on the Wotmed.com platform.

                        Wotmed.com is not liable in any way for you choosing to establish a relationship with any Patient who is present on the Wotmed.com platform.

                        Wotmed.com is not liable for any advertising, marketing or promotional material that you as a Practitioner present on the Wotmed.com platform that seeks to persuade Patients to undertake surgery with you in your particular Practice/Clinic or associated Hospital.

                        Wotmed.com strongly advises you as a Practitioner to do your own due diligence when choosing to accept Patients on your Wotmed.com profile.

                        Establishing and maintaining relationships with Patients who add you to their Wotmed.com profile and vice versa is your responsibility as a Practitioner and is also the responsibility of your Patient.

                        Wotmed.com is not liable for the views, opinions, recommendations or thanks of Patients on the Wotmed.com platform which may be visible on your Practitioner profile.

                        Wotmed.com provides a variety of functions to help Patients to develop an informed judgement about Practitioners like yourself who have accounts on the Wotmed.com platform. These functions include but are not limited to the Wotmed Thanks button, Wotmed Indices, Wotmed Practitioner Governance System, Wotmed Patient-Practitioner Recommendations and Reviews.

                        Warranty Disclaimer.

                        Wotmed.com does not guarantee the services advertised by you on your Wotmed.com Practitioner website profile or the services performed by you as a Practitioner who has an account on the Wotmed.com platform.

                        Wotmed.com does not guarantee the travel services provides by external third party businesses such as Airlines, Hotel & Accommodation Providers, Car Hire Companies or Train Companies.

                        Wotmed.com does not guarantee the reliability, responsibility, character, commitment, trustworthiness, honesty or other personal qualities of Patients who establish relationships with you via the Wotmed.com platform.

                        Wotmed.com does not guarantee the reliability, responsibility, character, commitment, trustworthiness, honesty or other personal qualities of Practitioners who establish relationships with you via the Wotmed.com platform.

                        Wotmed.com does not guarantee the professional qualifications of Practitioners who establish relationships with you via the Wotmed.com platform.

                        Wotmed.com has developed various frameworks including the Wotmed.com Governance Framework and

                        Wotmed.com Resume Framework which have been developed specifically to help vet the boni fides of Practitioners who create accounts on the Wotmed.com platform. Wotmed.com cannot however guarantee 100% the boni fides of any Practitioner who has an account on the platform.

                        Limited Liability.

                        Wotmed.com is not responsible for any losses you as a Practitioner may incur that are caused by any Patient, Practitioner or other party.

                        Wotmed.com is not responsible for any surgerical procedure performed by you as a Practitioner on any Patient that you have established a relationship with via the Wotmed.com platform.

                        Wotmed.com is not responsible for any injury, death or other conseqence of a Patient travelling for surgery or to undertake treatment performed by you as a Practitioner.

                        Wotmed.com is not responsible for any third party recommendation or referel that you make to a Patient.

                        Wotmed.com is not responsible for any injury, death or other conseqence of a Patient travelling for surgery or to undertake treatment by a Practitioner who has been referred to that Patient by yourself.

                        Limited Damages.

                        Wotmed.com is not responsible for any money claimed by a client for loss or injury, for any reason.

                        Severability.

                        If a provision of this Agreement is or becomes illegal, invalid or unenforceable in any jurisdiction, that shall not affect:

                        1. the validity or enforceability in that jurisdiction of any other provision of this Agreement; or

                        2. the validity or enforceability in other jurisdictions of that or any other provision of this Agreement.

                        Attorney's Fees.

                        Wotmed.com is not liable for the payment of attorney's fees in the event of a dispute between a Practitioner and a Patient nor between a Practitioner and a Practitioner.

                        <script src="/proposals/date/pikaday.js"></script>
                        <script>

                            var picker = new Pikaday(
                                {
                                    field: document.getElementById('datepicker'),
                                    firstDay: 1,
                                    minDate: new Date('2000-01-01'),
                                    maxDate: new Date('2020-12-31'),
                                    yearRange: [2000,2020]
                                });

                        </script>
                    </div>










            <p>&nbsp;</p>



        </div>


			<p>&nbsp;</p>
			<p>&nbsp;</p>
			<?php include "includes/p_footer.php"; ?>
