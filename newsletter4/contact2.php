<?php




// include('../HTMLEmailTemplate/WotmedEmailTemplate/Newsletter.php');


if(!$_POST) exit;

// Email address verification, do not edit.
function isEmail($email) {
	return(preg_match("/^[-_.[:alnum:]]+@((([[:alnum:]]|[[:alnum:]][[:alnum:]-]*[[:alnum:]])\.)+(ad|ae|aero|af|ag|ai|al|am|an|ao|aq|ar|arpa|as|at|au|aw|az|ba|bb|bd|be|bf|bg|bh|bi|biz|bj|bm|bn|bo|br|bs|bt|bv|bw|by|bz|ca|cc|cd|cf|cg|ch|ci|ck|cl|cm|cn|co|com|coop|cr|cs|cu|cv|cx|cy|cz|de|dj|dk|dm|do|dz|ec|edu|ee|eg|eh|er|es|et|eu|fi|fj|fk|fm|fo|fr|ga|gb|gd|ge|gf|gh|gi|gl|gm|gn|gov|gp|gq|gr|gs|gt|gu|gw|gy|hk|hm|hn|hr|ht|hu|id|ie|il|in|info|int|io|iq|ir|is|it|jm|jo|jp|ke|kg|kh|ki|km|kn|kp|kr|kw|ky|kz|la|lb|lc|li|lk|lr|ls|lt|lu|lv|ly|ma|mc|md|me|mg|mh|mil|mk|ml|mm|mn|mo|mp|mq|mr|ms|mt|mu|museum|mv|mw|mx|my|mz|na|name|nc|ne|net|nf|ng|ni|nl|no|np|nr|nt|nu|nz|om|org|pa|pe|pf|pg|ph|pk|pl|pm|pn|pr|pro|ps|pt|pw|py|qa|re|ro|ru|rw|sa|sb|sc|sd|se|sg|sh|si|sj|sk|sl|sm|sn|so|sr|st|su|sv|sy|sz|tc|td|tf|tg|th|tj|tk|tm|tn|to|tp|tr|tt|tv|tw|tz|ua|ug|uk|um|us|uy|uz|va|vc|ve|vg|vi|vn|vu|wf|ws|ye|yt|yu|za|zm|zw)$|(([0-9][0-9]?|[0-1][0-9][0-9]|[2][0-4][0-9]|[2][5][0-5])\.){3}([0-9][0-9]?|[0-1][0-9][0-9]|[2][0-4][0-9]|[2][5][0-5]))$/i",$email));
}

if (!defined("PHP_EOL")) define("PHP_EOL", "\r\n");

$name     = $_POST['name'];
$email    = $_POST['email'];

//$phone   = $_POST['phone'];
//$subject  = $_POST['subject'];
//$comments = $_POST['comments'];
//$verify   = $_POST['verify'];


if(trim($name) == '') 
{
	echo '<div class="error_message">You must enter your name.</div>';
	exit();
} 

else if(trim($email) == '') 
{
	echo '<div class="error_message">Please enter a valid email address.</div>';
	exit();
	
}
	
//} else if(trim($phone) == '') {
//	echo '<div class="error_message">Attention! Please enter a valid phone number.</div>';
//	exit();
//} else if(!is_numeric($phone)) {
//	echo '<div class="error_message">Attention! Phone number can only contain digits.</div>';
//	exit();

else if(!isEmail($email)) 
{
   echo '<div class="error_message">You have entered an invalid e-mail address, please try again.</div>';
   exit();
}


//if(trim($subject) == '') {
//	echo '<div class="error_message">Attention! Please enter a subject.</div>';
//	exit();
//} 

//else if(trim($comments) == '') {
//	echo '<div class="error_message">Attention! Please enter your message.</div>';
//	exit();

//} else if(!isset($verify) || trim($verify) == '') {
//	echo '<div class="error_message">Attention! Please enter the verification number.</div>';
//	exit();
//} else if(trim($verify) != '4') {
//	echo '<div class="error_message">Attention! The verification number you entered is incorrect.</div>';
//	exit();
//}

if(get_magic_quotes_gpc()) {
	$comments = stripslashes($comments);
}


// Configuration option.
// Enter the email address that you want to emails to be sent to.
// Example $address = "joe.doe@yourdomain.com";

//$address = "example@themeforest.net";
$address = "newslettersignups@wotmed.com";


// Configuration option.
// i.e. The standard subject will appear as, "You've been contacted by John Doe."

// Example, $e_subject = '$name . ' has contacted you via Your Website.';

//$e_subject = 'You\'ve been contacted by ' . $name . '.';


// Configuration option.
// You can change this if you feel that you need to.
// Developers, you may wish to add more fields to the form, in which case you must be sure to add them here.

$e_body = "$name has signed up to receive the Wotmed Newsletter";
$e_content = "\"$comments\"" . PHP_EOL . PHP_EOL;
$e_reply = "You can contact $name via email, $email";

$msg = wordwrap( $e_body . $e_content . $e_reply, 70 );

//$headers = "From: $email" . PHP_EOL;
//$headers .= "Reply-To: $email" . PHP_EOL;
//$headers .= "MIME-Version: 1.0" . PHP_EOL;
//$headers .= "Content-type: text/html; charset=ISO-8859-1" . PHP_EOL;
//$headers .= "Content-Transfer-Encoding: quoted-printable" . PHP_EOL;




//$to      = 'newslettersignups@wotmed.com';
$subject = 'A potential client: ' . $name . ' with email address: ' . $email . ' has just signed up to receive the Wotmed Newsletter';

$message = file_get_contents('../HTMLEmailTemplate/WotmedEmailTemplate/Newsletter.html');

$headers = 'From: newslettersignups@wotmed.com' . "\r\n" .
    'Reply-To: newslettersignups@wotmed.com' . "\r\n" .
    'X-Mailer: PHP/' . phpversion();
$headers .= "MIME-Version: 1.0\r\n";
$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

//mail($to, $subject, $message, $headers);




if(mail($address, $subject, $message, $headers)) 
{
	
	
	//echo $_POST["name"];
//echo $_POST["email"];
	
	
	

 
 
 //$url = '/newsletter4/call.php?name=$name&email=$email';
 
 // header("Location: $url");
 

 
//error_reporting(E_ALL | E_WARNING | E_NOTICE);
//ini_set('display_errors', TRUE);


//flush();
//ob_start();


//header("Location: http://www.website.com/");
//die('should have redirected by now');



//error_reporting(E_ALL | E_WARNING | E_NOTICE);
//ini_set('display_errors', TRUE);


//header('Location: http://localhost/newsletter4/call.php?name=Clarke&email=clarke@test.com'); 			

	// Email has sent successfully, echo a success page.

	echo "<fieldset>";
	echo "<div id='success_page'>";
	echo "<h1>Newsletter Sign Up Successful</h1>";
	echo "<p>Thank you <strong>$name</strong>, you are now signed up to receive the Wotmed Newsletter.</p>";
	echo "</div>";
	echo "</fieldset>";
	


	


	 
	 
// echo "<a href='newsletter4/call.php?name=$name&email=$email'>Add to vTiger</a>";





//  $url = 'http://www.wotmed.com/newsletter4/call.php';


// echo "<form method='POST' action='newsletter4/call.php?name=$name&email=$email'>";

//echo "<input type=submit name='submit' value='ok'>";
//echo "</form>";

//error_reporting(E_ALL | E_WARNING | E_NOTICE);
//ini_set('display_errors', TRUE);


//$h = curl_init();

//  curl_setopt($h, CURLOPT_URL, "newsletter4/call.php?"); 
 // curl_setopt($h, CURLOPT_POST, true);
 // curl_setopt($h, CURLOPT_POSTFIELDS, array(
 // 'name' => 'Clarke',
//  'email' => 'clarke@test.com'
//  ));
//  curl_setopt($h, CURLOPT_HEADER, false);
//  curl_setopt($h, CURLOPT_RETURNTRANSFER, 1);

 // $result = curl_exec($h);
//  echo $result;


  

// didn't work

	// add the users information to vTiger
	
//	$hostname = "http://www.wotmed.com/";
//	$url = "newsletter4/call.php?name=";
//	$fullstring = $hostname . $url . $name;
	
//	echo $fullstring;
	
//	$ch = curl_init();
//	curl_setopt($ch, CURLOPT_URL, $fullstring);
//	curl_setopt($ch, CURLOPT_HEADER, 0);
//	curl_exec($ch);
//	curl_close ($ch);

	
	
 // $url = "http://www.wotmed.com/newsletter4/call.php?name=$name&email=$email";
 
 // $url = "http://www.wotmed.com/newsletter4/call2.php;

//	echo $url;
  
  
  
   


//echo "<a href='http://www.YAHOO.com' target='_blank'>Yahoo!</a>";


//$url = 'http://www.wotmed.com/newsletter4/call.php?name=$name&email=$email';

//	$ch = curl_init();
//	curl_setopt($ch, CURLOPT_URL, $url);
//	curl_setopt($ch, CURLOPT_HEADER, 0);
//	curl_exec($ch);
//	curl_close ($ch);



	

} else {

	echo 'ERROR!';

}


// call the vTiger form submittion page so that we can add the potential client to vTiger database
// we want to pass the following details to the form

// $name
// $email

// $name will be lastname
// $email will be email


//echo $_POST["name"];
//echo $_POST["email"];


// didn't work

	// add the users information to vTiger
// $url = "http://www.wotmed.com/newsletter4/call.php?name=$name&email=$email";
//  $url = "http://www.wotmed.com/newsletter4/call2.php;
//  $url = "http://www.wotmed.com/";
//  $url2 = "http://www.wotmed.com/blank.html";
//	echo $url;

//	$ch = curl_init();
//	curl_setopt($ch, CURLOPT_URL, $url);
//	curl_setopt($ch, CURLOPT_HEADER, 0);
//	curl_exec($ch);
//	curl_close ($ch);
	
// header('Location: http://www.wotmed.com/blank.html');
	
//	print file_get_contents($url);



