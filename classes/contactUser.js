
			function reset(){
				$('#info').css('display','none');
				$('#botFields').css('display','none');
			}
			function recommend(){
				reset();
				$('#botFields').css('display','block');
				$('#status').val('true');
			}
			function notRecommend(){
				reset();
				$('#info').css('display','block');
				$('#botFields').css('display','block');
				$('#status').val('false');
			}
			
	// load the modal window
	function displayContactUser(id){
		// scroll to top
		document.body.style.overflow= "hidden";
		$('html, body').animate({scrollTop:0}, 'fast');

		// before showing the modal window, reset the form incase of previous use.
		$('.success, .error').hide();
		$('form#contactUserForm').show();
		/* Reset all the default values in the form fields
		$('#name').val('Your name');
		$('#email').val('Your email address');
		$('#comment').val('Enter your comment or query...');*/

		//show the mask and recommend divs
		$('#mask').show().fadeTo('', 0.7);
		$('div#contactUser').fadeIn();
		$('#part_id').val(id);

		// stop the modal link from doing its default action
		return false;
	}


	function sendMsg(to, msgSubject,msgText){
		$.ajax({
		type: "POST",
		url: "ajax/sendContactEmail.php",
		data: {to: to, msgSubject: msgSubject, msgText: msgText }
	}).done(function ( msg ) {
		//alert(msg);
		alert("Your email has been successfully sent");
		$('div#contactUser, div#mask').stop().fadeOut('slow');
		document.body.style.overflow= "visible";
		$('#email').val("");
		$('#msg').val("");
		});
	}
$(function() {

	$('#submit').click(function() {
  		sendMsg($('#part_id').val(), $('#email').val(), $('#msg').val());
	});

	// close the modal window is close div or mask div are clicked.
	$('div#close, div#mask').click(function() {
		$('div#contactUser, div#mask').stop().fadeOut('slow');
		document.body.style.overflow= "visible";
	});

	// when the Submit button is clicked...
	$('#contactForm input#submit').click(function() {
		//Inputed Strings
		var username = $('#name').val(),
			email = $('#email').val(),
			comment = $('#comment').val();

		//Error Count
		var error_count=0;

		//Regex Strings
		var username_regex = /^[a-z0-9_-]{3,16}$/,
			email_regex = /^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/;

			//Test Email
			if(!email_regex.test(email)) {
				$('#contact_header').after('<p class=error>Invalid email entered!</p>');
				error_count += 1;
			}

			//Blank Comment?
			if(comment == '') {
				$('#contact_header').after('<p class=error>No Comment was entered!</p>');
				error_count += 1;
			}

			//No Errors?
			if(error_count == 0) {
			/*
				$.ajax({
					type: "post",
					url: "ajax/contact.php",
					data: "name=" + username + "&email=" + email + "&comment=" + comment,
					error: function() {
						$('.error').hide();
						$('#sendError').slideDown('slow');
					},
					success: function (data) {
						$('.error').hide();
						$('.success').slideDown('slow');
						$('form#contactForm').fadeOut('slow');
					}
				});*/
			}

			else {
                $('.error').show();
            }
			error_count=0;

		return false;
	});

});