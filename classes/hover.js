/**
* hoverIntent r6 // 2011.02.26 // jQuery 1.5.1+
* <http://cherne.net/brian/resources/jquery.hoverIntent.html>
* 
* @param  f  onMouseOver function || An object with configuration options
* @param  g  onMouseOut function  || Nothing (use configuration options object)
* @author    Brian Cherne brian(at)cherne(dot)net
*/
(function($){$.fn.hoverIntent=function(f,g){var cfg={sensitivity:7,interval:100,timeout:0};cfg=$.extend(cfg,g?{over:f,out:g}:f);var cX,cY,pX,pY;var track=function(ev){cX=ev.pageX;cY=ev.pageY};var compare=function(ev,ob){ob.hoverIntent_t=clearTimeout(ob.hoverIntent_t);if((Math.abs(pX-cX)+Math.abs(pY-cY))<cfg.sensitivity){$(ob).unbind("mousemove",track);ob.hoverIntent_s=1;return cfg.over.apply(ob,[ev])}else{pX=cX;pY=cY;ob.hoverIntent_t=setTimeout(function(){compare(ev,ob)},cfg.interval)}};var delay=function(ev,ob){ob.hoverIntent_t=clearTimeout(ob.hoverIntent_t);ob.hoverIntent_s=0;return cfg.out.apply(ob,[ev])};var handleHover=function(e){var ev=jQuery.extend({},e);var ob=this;if(ob.hoverIntent_t){ob.hoverIntent_t=clearTimeout(ob.hoverIntent_t)}if(e.type=="mouseenter"){pX=ev.pageX;pY=ev.pageY;$(ob).bind("mousemove",track);if(ob.hoverIntent_s!=1){ob.hoverIntent_t=setTimeout(function(){compare(ev,ob)},cfg.interval)}}else{$(ob).unbind("mousemove",track);if(ob.hoverIntent_s==1){ob.hoverIntent_t=setTimeout(function(){delay(ev,ob)},cfg.timeout)}}};return this.bind('mouseenter',handleHover).bind('mouseleave',handleHover)}})(jQuery);

/*$(function() {
   $('#flag').hover( function(){
      //$(this).css('width', '110%');
		$('#your_div_id').css({
		left:  e.pageX,
		top:   e.pageY
    });
   },
   $(function(){
      //$(this).css('width', '100%');
   });
});
*/

	function popUp(id){
	    $("#"+id).hoverIntent(function(e){
			$('#'+id+'PopUp').css({
				left:  $(this).position().left-20,
				top:   $(this).position().top+20,
				//display:'block',
				position:'absolute',
				width:'auto',
				maxWidth:'600px'
			});
			$('#'+id+'PopUp').fadeIn(200);
		},function(){
			/*$('#'+id+'PopUp').css({
				display:'none'
			});*/
			$('#'+id+'PopUp').fadeOut(200);
		});
	}
	function popUpIcon(id){
	    $("#"+id).hoverIntent(function(e){
			$('#'+id+'PopUp').css({
				left:  $(this).position().left+25,
				top:   $(this).position().top+$(this).height(),
				//display:'block',
				position:'absolute',
				width:'auto',
				maxWidth:'600px'
			});
			$('#'+id+'PopUp').fadeIn(200);
		},function(){
			/*$('#'+id+'PopUp').css({
				display:'none'
			});*/
			$('#'+id+'PopUp').fadeOut(200);
		});
	}
	function popUpClass(cl){
	    $("."+cl).hoverIntent(function(e){
			$('.'+cl+'PopUp').css({
				left:  $(this).position().left+25,
				top:   $(this).position().top+$(this).height(),
				//display:'block',
				position:'absolute',
				width:'auto',
				maxWidth:'600px'
			});
			$('.'+cl+'PopUp').fadeIn(200);
		},function(){
			/*$('.'+cl+'PopUp').css({
				display:'none'
			});*/
			$('.'+cl+'PopUp').fadeOut(200);
		});
	}

$(function() {
	popUp("aboutMePractitionerVideo");
	popUp("aboutMeAudio");
	popUp("resumeIcon");
	popUp("businessCardIcon");
	popUp("dipMissionIcon");
	popUp("flagDiv");
	popUp("pressIcon");
	popUp("freedomIcon");
	popUp("hdiIcon");
	popUp("economicIcon");
	popUp("democracyIcon");
	popUp("physicians");
	
	popUpIcon("addAlbum");
	popUpIcon("gallery");
	popUpIcon("galleryBack");
	popUpIcon("videoGallery");
	popUpIcon("aboutMe");
	popUpIcon("conditionReport");
	popUpIcon("trust");
	popUpIcon("cpanelIcon");
	popUpIcon("cardIcon");
	popUpIcon("trustIcon");
	popUpIcon("aUser");
	popUpIcon("paypal");
	popUpIcon("trustCPanel");
	//popUpIcon("recommendButton");
	popUpIcon("thanksButton");
	//popUpIcon("trustButton");
	
	popUpIcon("trustText");
	popUpIcon("connectedToText");
	popUpIcon("thankText");
	popUpIcon("recomText");
	
	popUpIcon("trustIndex");
	
	popUpClass("recommendButton");
	popUpClass("thanksButton");
	popUpClass("trustButton");
	popUpClass("profPicDivClass");
});