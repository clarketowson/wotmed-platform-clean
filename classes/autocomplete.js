$(document).ready(function(){
	$("#RELIGION").autocomplete("ajax/updateReligionList.php", {  //we have set data with source here
		formatItem: function(rowdata) { //This function is called right before the item is displayed on the dropdown, so we splitted and returned what we show in the selection box
			var info = rowdata[0].split(":");
			return info[1];
		},
		formatResult: function (rowdata) { // This function is called when any item selected, here also we returned that what we wanted to show our customerName field.
			var info = rowdata[0].split(":");
			return info[1];
		}
	}).result(function(event, data, formatted){ //Here we do our most important task :)
			if(!data) { //If no data selected set the customer_id field value as 0
				$("#RELIGION_NUMBER").val('0');
			} else { // Set the value for the customer id
				var info = formatted.split(":");
				$("#RELIGION_NUMBER").val(info[0]);
			}
	});
	
	
	$("#ETHNIC").autocomplete("ajax/updateEthnicList.php", {  //we have set data with source here
		formatItem: function(rowdata) { //This function is called right before the item is displayed on the dropdown, so we splitted and returned what we show in the selection box
			var info = rowdata[0].split(":");
			return info[1];
		},
		formatResult: function (rowdata) { // This function is called when any item selected, here also we returned that what we wanted to show our customerName field.
			var info = rowdata[0].split(":");
			return info[1];
		}
	}).result(function(event, data, formatted){ //Here we do our most important task :)
			if(!data) { //If no data selected set the customer_id field value as 0
				$("#ETHNIC_NUMBER").val('0');
			} else { // Set the value for the customer id
				var info = formatted.split(":");
				$("#ETHNIC_NUMBER").val(info[0]);
			}
	});
	
	
	$(".LANGUAGE").autocomplete("ajax/updateLanguageList.php", {  //we have set data with source here
		formatItem: function(rowdata) { //This function is called right before the item is displayed on the dropdown, so we splitted and returned what we show in the selection box
			var info = rowdata[0].split(":");
			return info[1];
		},
		formatResult: function (rowdata) { // This function is called when any item selected, here also we returned that what we wanted to show our customerName field.
			var info = rowdata[0].split(":");
			return info[1];
		}
	}).result(function(event, data, formatted){ //Here we do our most important task :)
			if(!data) { //If no data selected set the id field value as 0
				$("#"+this.id+"_NUMBER").val('0');
			} else { // Set the value for the customer id
				var info = formatted.split(":");
				$("#"+this.id+"_NUMBER").val(info[0]);
			}
	});
 
});