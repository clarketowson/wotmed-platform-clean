
function centerImage(className){
	var conHeight = $("."+className).height();
	var imgHeight = $("."+className+" img").height();
	var gap = Math.abs(imgHeight - conHeight) / 2;
	$("."+className+" img").css("margin-top", -gap);
	
	var conWidth = $("."+className).width();
	var imgWidth = $("."+className+" img").width();
	gap = Math.abs(imgWidth - conWidth) / 2;
	$("."+className+" img").css("margin-left", conWidth>imgWidth ? -gap : gap);
}
//centerImage("profPicDiv");
//centerImage("tinyDiv");
//centerImage("pracListDic");

