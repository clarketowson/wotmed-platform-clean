﻿var places = [];
			var details = [];
			var markersArray = [];
			var adsDetails;
			var clicked=true;
			function setPlace(locations,data){
				places.length = 0;
				details.length = 0;
				markersArray.length = 0;
				var temp = locations.split(";");
				for(var i=0;i < temp.length; i++){
					var tempLoc = temp[i].split(",");
					places.push(tempLoc);
				}
				
				var temp = data.split(";");
				for(var i=0;i < temp.length; i++){
					var tempLoc = temp[i].split("|");
					details.push(tempLoc);
				}
				initialize(places[0][0],places[0][1]);
			}
			/*
			function setDetail(data){
				var temp = data.split(";");
				for(var i=0;i < temp.length; i++){
					var tempLoc = temp[i].split("|");
					details.push(tempLoc);
				}
			}*/
			
			function initialize(lng,lat) {
				//alert(places.length);
				var myLatlng = new google.maps.LatLng(lng,lat);
				
				var mapOptions = {
				  center: myLatlng,
				  zoom: 15,
				  mapTypeId: google.maps.MapTypeId.ROADMAP
				};

				var map = new google.maps.Map(document.getElementById("map_canvas"),
					mapOptions);
				
				setMarkers(map, places, details);
			}
			var cX = 0; var cY = 0; var rX = 0; var rY = 0;
			function UpdateCursorPosition(e){ cX = e.pageX; cY = e.pageY;}
			function UpdateCursorPositionDocAll(e){ cX = event.clientX; cY = event.clientY;}
			if(document.all) { document.onmousemove = UpdateCursorPositionDocAll; }
			else { document.onmousemove = UpdateCursorPosition; }
			function AssignPosition(d) {
			if(self.pageYOffset) {
			rX = self.pageXOffset;
			rY = self.pageYOffset;
			}
			else if(document.documentElement && document.documentElement.scrollTop) {
			rX = document.documentElement.scrollLeft;
			rY = document.documentElement.scrollTop;
			}
			else if(document.body) {
			rX = document.body.scrollLeft;
			rY = document.body.scrollTop;
			}
			if(document.all) {
			cX += rX;
			cY += rY;
			}
			d.style.left = (cX+10) + "px";
			d.style.top = (cY+10) + "px";
			}
			function HideText(d) {
			if(d.length < 1) { return; }
			document.getElementById(d).style.display = "none";
			}
			function ShowText(d) {
			if(d.length < 1) { return; }
			var dd = document.getElementById(d);
			AssignPosition(dd);
			dd.style.display = "block";
			}
			function ReverseContentDisplay(d) {
			if(d.length < 1) { return; }
			var dd = document.getElementById(d);
			AssignPosition(dd);
			if(dd.style.display == "none") { dd.style.display = "block"; }
			else { dd.style.display = "none"; }
			}
			
			function setMarkers(map, locations, data) {
			  for (var i = 0; i < locations.length; i++) {
				var loc = locations[i];
				var detail = data[i];
				var myLatLng = new google.maps.LatLng(loc[0], loc[1]);
				/*
				var marker = new google.maps.Marker({
					position: myLatLng,
					map: map,
					title: "Marker " + i,
					zIndex: loc[3]
				});
				google.maps.event.addListener(marker, 'click', function() { 
					alert("I am marker " + marker.title); 
				});
				//var marker = createMarker(new google.maps.LatLng(loc[0], loc[1]), "m"+i,loc);
				
				var tmpLat = dealer_markers[a].lat;
				var tmpLng = dealer_markers[a].lng;
				var tmpName = dealer_markers[a].name;
				var tmpAdr = dealer_markers[a].adr;
				var tmpTel = dealer_markers[a].pc;
				var tmpPc = dealer_markers[a].tel;*/
				var marker = _newGoogleMapsMarker({
					_map: map,
					_lat: loc[0],
					_lng: loc[1],
					_head: detail[1],
					_data: '<div id="bg">\
					<table border="0">\
					<tr><td>\
					<h3 >' + detail[1] + '</h2>\
					<h5 >' + "Speciality : " + detail[7] + '</h3>\
					<h5 >' + detail[2] + '</h3>\
					</td>\
					<td>\
					<img src="photos/originals/' + detail[0] + '" height="50px">\
					</td></tr>\
					<tr style="background: rgb(253,249,228);">\
						<input type="hidden" id="adsID" name="adsID" value="'+ detail[3] + '">\
						<td colspan="2" onclick="advertisment()" onmouseover="ShowText(\"Message\"); return true;" onmouseout="HideText(\"Message\"); return true;">\
						<h3>'+ detail[4] + '</h3>\
						'+ detail[5] + '<br>\
						<a href="ajax/addAdsClick.php?id='+ detail[3] + '">'+ detail[6] + '</a></td></tr>\
					<tr><td colspan="2">\
					<div id="ads"></div>\
					</td></tr>\
				</table>\
				<div id="Message" class="box">\
				Click here to advertise your Practice on Wotmed.com\
				</div>\
				</div>'
				});
				markersArray.push(marker);
			  }
			}
			function getAdvert(lng,lat){
				if (window.XMLHttpRequest)
				{// code for IE7+, Firefox, Chrome, Opera, Safari
					xmlhttp=new XMLHttpRequest();
				}
				else
				{// code for IE6, IE5
					xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
				}
				xmlhttp.onreadystatechange=function()
				{
					//alert("masuk ready stage");
					if (xmlhttp.readyState==4 && xmlhttp.status==200)
					{
						adsDetails=xmlhttp.responseText;
						document.getElementById("invis").innerHTML=adsDetails;
						initialize(lng,lat);
					}
				}
				xmlhttp.open("GET","ajax/getAdvert.php",true);
				xmlhttp.send();
			}
			function addImpression(id){
				if (window.XMLHttpRequest)
				{// code for IE7+, Firefox, Chrome, Opera, Safari
					xmlhttp=new XMLHttpRequest();
				}
				else
				{// code for IE6, IE5
					xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
				}
				xmlhttp.onreadystatechange=function()
				{
					if (xmlhttp.readyState==4 && xmlhttp.status==200)
					{
						//alert("success");
					}
				}
				xmlhttp.open("GET","ajax/addAdsImpression.php?id="+id,true);
				xmlhttp.send();
			}
			function leaveMarker() {
				for(var i=0;i < markersArray.length; i++){
					markersArray[i].setAnimation(null);
				}
			}
			function changeMarker(marker_id) {
				//alert(marker_id);
				var marker=markersArray[marker_id];
				//if (marker.getAnimation() != null) {
					//marker.setAnimation(null);
				//} else {
					marker.setAnimation(google.maps.Animation.BOUNCE);
				//}
			}
			function advertisment(){
				window.location="profile.php";
			}
			
			function _newGoogleMapsMarker(param) {
				var r = new google.maps.Marker({
					map: param._map,
					position: new google.maps.LatLng(param._lat, param._lng),
					title: param._head
				});
				if (param._data) {
					google.maps.event.addListener(r, 'click', function() {
						// this -> the marker on which the onclick event is being attached
						if (!this.getMap()._infoWindow) {
							this.getMap()._infoWindow = new google.maps.InfoWindow();
						}
						this.getMap()._infoWindow.close();
						this.getMap()._infoWindow.setContent(param._data);
						this.getMap()._infoWindow.open(this.getMap(), this);
						if(clicked){
							addImpression(document.getElementById('adsID').value);
							clicked=!clicked;
						}
					});
				}
				return r;
			}