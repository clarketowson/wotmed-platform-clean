
<!DOCTYPE html PUBLIC
"-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
		<title>Welcome to Wotmed</title>
		<meta http-equiv="Content-Type" content="'text'/html; charset=utf-8" />
        
        
 <script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-38343871-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-49954656-1', 'wotmed.com');
		  ga('send', 'pageview');

		</script>

       
		<script type="text/javascript" src="classes/jquery.js"></script>
		<script type="text/javascript" src="classes/hover.js"></script>
		<script src="classes/jquery.validation.js"></script>
		<script src="classes/dateSelectBoxes.js"></script>
		<script type="text/javascript">
			function searchUsers()
			{
				if (window.XMLHttpRequest)
				{// code for IE7+, Firefox, Chrome, Opera, Safari
					xmlhttp=new XMLHttpRequest();
				}
				else
				{// code for IE6, IE5
					xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
				}
				xmlhttp.onreadystatechange=function()
				{
					if (xmlhttp.readyState==4 && xmlhttp.status==200)
					{
						//alert("masuk");
						//document.getElementById("result").innerHTML=xmlhttp.responseText;
					}
				}
				
				xmlhttp.open("GET","ajax/reminder.php",true);
				
				xmlhttp.send();
			}
			function registerCRM(){
				$.ajax({
					type: "POST",
					url: "https://wotmed3.od1.vtiger.com/modules/Webforms/capture.php",
					data: {
						publicid : "480b38eabf8d286a92d96ccc1363d661",
						name : "practitioner registration",
						lastname : $("#lastname2").val(),
						firstname : $("#firstname2").val(),
						email : $("#reg_email__").val()
						}
				}).done(function ( msg ) {
					alert(msg);
				}).fail(function() {
					$("#registerForm").submit();
				});
			}
			$(document).ready(function(){
			
				jQuery.validator.addMethod("validPassword", function(value, element) { 
				  return /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/.test(value); 
				}, "six or more characters<br/> that contains at least one digit<br/> at least one uppercase character; and<br/> at least one lowercase character:");
				
				$().dateSelectBoxes($('#birthMonth2'),$('#birthDay2'),$('#birthYear2'),true);
				$().dateSelectBoxes($('#birthMonth'),$('#birthDay'),$('#birthYear'),true);
				$("#loginForm").validate({
					messages: {
						email: "*",
						password: "*"
					},
					submitHandler: function(form) {
						//TODO ajax code goes here
						$.ajax({
						  type: "POST",
						  url: "ajax/loginAjax.php",
						  data: { inputtext: $('#email').val(), inputpassword: $('#password').val()}
						}).done(function( msg ) {
							if(msg=="profile.php" || msg=="practitioner_profile.php"){
								window.location = msg;
							}
							else{
							$('#errMsg').html(msg);
							}
						});
						return false;
					}
				});
				$("#registerForm").validate({
					messages: {
						sex: "*",
						birthMonth: "*",
						birthDay: "*",
						birthYear: "*",
						reg_email_confirmation__:{
							equalTo: "Your email does not match"
						}
					},
					rules: {
						reg_email_confirmation__: {
						  equalTo: "#reg_email__"
						},
						reg_passwd__: {
							validPassword: true
						},
						sex:{
							min: 1
						},
						birthMonth:{
							min: 1
						},
						birthDay:{
							min: 1
						},
						birthYear:{
							min: 1
						}
				  }
				});
				$("#registerForm2").validate({
					messages: {
						sex: "*",
						birthMonth: "*",
						birthDay: "*",
						birthYear: "*",
						reg_email_confirmation__:{
							equalTo: "Your email does not match"
						}
					},
					rules: {
						reg_email_confirmation__: {
						  equalTo: "#reg_email__2"
						},
						reg_passwd__: {
							validPassword: true
						},
						sex:{
							min: 1
						},
						birthMonth:{
							min: 1
						},
						birthDay:{
							min: 1
						},
						birthYear:{
							min: 1
						}
				  }
				});
			});
		</script>
		<link href="style/i_style.css" rel="stylesheet"></link>

    <link rel="stylesheet" type="text/css" href="signup.css">
        
	</head>
	<body onload="searchUsers()">
		<div class="topBar" >
			<div class="bar_frame">
				<div class="w_logo"><img src="images/WotmedLogoTransparent.png" height="45" alt="wotmedLogo" style='position:relative; left:-30px; padding:0px;padding-top:10px;' /></div>
				<div class="div_login">
					<form id="loginForm" method="post" action="login.php">
					<table>
						<tr>
							<td><label>Email</label></td>
							<td><label>Password</label></td>
						</tr>
						<tr>
							<td width="150"><input type="text" class="inputtext required email" name="email" id="email"/></td>
							<td width="150"><input type="password" class="inputpassword required" name="password" id="password"/></td>
							<td><input type="submit" value="Login"/></td>
						</tr>
						<tr>
							<td><input name="persistent" type="checkbox" style="float:left; margin: 0px; padding: 0px;"/> <label>Keep me logged in</label></td>
							<td><a href="resetPassword.php"><label style="cursor:pointer;text-decoration:underline;">Forget your password?</label></a></td>
						</tr>
					</table>
					</form>
				<div id="errMsg">
				</div>
				</div>
				<div>
					<?php //include "fbConnect.php"; ?>
				</div>
				<div>
					<?php //include "googleConnect.php"; ?>
				</div>
			</div>
		</div>


		<div id="container">
			<!--
			<div id='physicians'>
				<h2><a href="#" onclick="return false;">Today's Wotmed Honour</a></h2>
                
                
                
        
                
                
                
                
			</div>-->
			<h3><a href="#" onclick="return false;" class="about">Wotmed Platform Network Status</a></h3>
			<table width="840" border="1">
			  <tr>
			    <td bgcolor="#CCCCCC"><strong>Start Date</strong></td>
			    <td bgcolor="#CCCCCC"><strong>Start Time</strong></td>
			    <td bgcolor="#CCCCCC"><strong>End Date</strong></td>
			    <td bgcolor="#CCCCCC"><strong>End Time</strong></td>
			    <td bgcolor="#CCCCCC"><strong>Description</strong></td>
			    <td bgcolor="#CCCCCC"><strong>Wotmed Platform Status</strong></td>
		      </tr>
			  <tr>
			    <td>18/10/2014</td>
			    <td>UTC+09:00</td>
			    <td>25/10/2014</td>
			    <td>UTC+09:45</td>
			    <td>Major security upgrades &amp; system maintenance</td>
			    <td><span style="color: #ff0000;">Offline</span></td>
		      </tr>
			  <tr>
			    <td>25/10/2014</td>
			    <td>UTC+10:00</td>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
			    <td>Service restored / Wotmed Platform operational</td>
			    <td><span style="color: #18575c;">Online</span></td>
		      </tr>
			  <tr>
			    <td>31/10/2014</td>
			    <td>UTC+10:00</td>
			    <td>1/1/2014</td>
			    <td>UTC+08:00</td>
			    <td>Website &amp; platform speed improvement work</td>
			    <td><span style="color: #18575c;">Online</span></td>
		      </tr>
			  <tr>
			    <td>23/11/2014</td>
			    <td>UTC 11:30</td>
			    <td>24/11/2014</td>
			    <td>UTC+06:15</td>
			    <td>Down for maintenance</td>
			    <td><span style="color: #ff0000;">Offline</span></td>
		      </tr>
			  <tr>
			    <td>24/11/2014</td>
			    <td>UTC+06:15</td>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
			    <td>Service restored / Wotmed Platform operational</td>
			    <td><span style="color: #18575c;">Online</span></td>
		      </tr>
			  <tr>
			    <td>13/5/2015</td>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
			    <td>Service restored / Wotmed Platform operational (Substantial bug fixes and user interface improvements)</td>
			    <td><span style="color: #18575c;">
                
                <?php

$myfile = fopen("WotmedPlatformStatus.txt", "r") or die("Wotmed Platform Status Undetermined");
echo fread($myfile,filesize("WotmedPlatformStatus.txt"));
fclose($myfile);

?>
                
                </span></td>
		      </tr>
	      </table>
			<p>&nbsp;</p>
			<?php include "includes/p_footer.php"; ?>
