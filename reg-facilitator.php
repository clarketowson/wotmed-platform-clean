

<script type="application/ld+json">
{
"@context": "http://schema.org",
"@type": "Service",
"name": "Wotmed Surgery Facilitator Registration",
"description": "Sign up today as a Wotmed Surgery Facilitator and connect with Patients globally",
"serviceType": "Surgery Facilitator Registration Service",
"url": "http://platform.wotmed.com/reg-facilitator.php"
}
}
</script>

<?php
	/*if ($_SERVER['HTTPS'] != "on") {
		$url = "https://". $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
		header("location: $url");
	}  */
	session_start();
	include("includes/connect.php");
	include("includes/functions.php");
	if(isset($_SESSION['id'])){
		$redirect="profile.php";
		if(isset($_SESSION['practitioner_id']))
			$redirect='practitioner_profile.php';
		redirect($redirect);
	}
	elseif(isset($_POST['email'])){
		$inputtext=$_POST['inputtext'];
		$inputpassword=$_POST['inputpassword'];
		login($conn,$inputtext,$inputpassword);
	}
?>
<!DOCTYPE html PUBLIC
"-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Wotmed Surgery Facilitator Sign Up</title>
		<meta http-equiv="Content-Type" content="'text'/html; charset=utf-8" />
        
        <script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-38343871-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();∂

</script>

<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-49954656-1', 'wotmed.com');
		  ga('send', 'pageview');

		</script>

        <meta name="description" content="Wotmed.com - Sign Up or Login">
        <meta name="keywords" content="Wotmed.com - Packaging Medical Tourism.  Cheap Overseas Dental Care, Cheap overseas Dentists, Cheap Overseas Doctors, Overseas Cosmetic Surgery, Overseas Surgery, Overseas Dental Surgery, Tummy Tucks overseas, Boob Jobs overseas, Breast Augmentation overseas, Mummy Makeovers">

        <meta property="og:locale" content="en_US">
        <meta property="og:type" content="website">
        <meta property="og:title" content="Welcome to Wotmed">
        <meta property="og:description" content="Wotmed.com - Packaging Medical Tourism">
        <meta property="og:site_name" content="Wotmed">
        <meta property="og:url" content="http://platform.wotmed.com/reg-facilitator">
        <meta property="og:image" content="http://platform.wotmed.com/WotmedFacebookLogo2015.png">

        
		<script type="text/javascript" src="classes/jquery.js"></script>
		<script type="text/javascript" src="classes/hover.js"></script>
		<script src="classes/jquery.validation.js"></script>
		<script src="classes/dateSelectBoxes.js"></script>
		<script type="text/javascript">
			function searchUsers()
			{
				if (window.XMLHttpRequest)
				{// code for IE7+, Firefox, Chrome, Opera, Safari
					xmlhttp=new XMLHttpRequest();
				}
				else
				{// code for IE6, IE5
					xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
				}
				xmlhttp.onreadystatechange=function()
				{
					if (xmlhttp.readyState==4 && xmlhttp.status==200)
					{
						//alert("masuk");
						//document.getElementById("result").innerHTML=xmlhttp.responseText;
					}
				}
				xmlhttp.open("GET","ajax/reminder.php",true);
				xmlhttp.send();
			}
			function registerCRM(){
				$.ajax({
					type: "POST",
					url: "https://wotmed3.od1.vtiger.com/modules/Webforms/capture.php",
					data: {
						publicid : "480b38eabf8d286a92d96ccc1363d661",
						name : "practitioner registration",
						lastname : $("#lastname2").val(),
						firstname : $("#firstname2").val(),
						email : $("#reg_email__").val()
						}
				}).done(function ( msg ) {
					alert(msg);
				}).fail(function() {
					$("#registerForm").submit();
				});
			}
			$(document).ready(function(){
			
				jQuery.validator.addMethod("validPassword", function(value, element) { 
				  return /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/.test(value); 
				}, "six or more characters<br/> that contains at least one digit<br/> at least one uppercase character; and<br/> at least one lowercase character:");
				
				$().dateSelectBoxes($('#birthMonth2'),$('#birthDay2'),$('#birthYear2'),true);
				$().dateSelectBoxes($('#birthMonth'),$('#birthDay'),$('#birthYear'),true);
				$("#loginForm").validate({
					messages: {
						email: "*",
						password: "*"
					},
					submitHandler: function(form) {
						//TODO ajax code goes here
						$.ajax({
						  type: "POST",
						  url: "ajax/loginAjax.php",
						  data: { inputtext: $('#email').val(), inputpassword: $('#password').val()}
						}).done(function( msg ) {
							if(msg=="profile.php" || msg=="practitioner_profile.php"){
								window.location = msg;
							}
							else{
							$('#errMsg').html(msg);
							}
						});
						return false;
					}
				});
				$("#registerForm").validate({
					messages: {
						sex: "*",
						birthMonth: "*",
						birthDay: "*",
						birthYear: "*",
						reg_email_confirmation__:{
							equalTo: "Your email does not match"
						}
					},
					rules: {
						reg_email_confirmation__: {
						  equalTo: "#reg_email__"
						},
						reg_passwd__: {
							validPassword: true
						},
						sex:{
							min: 1
						},
						birthMonth:{
							min: 1
						},
						birthDay:{
							min: 1
						},
						birthYear:{
							min: 1
						}
				  }
				});
				$("#registerForm2").validate({
					messages: {
						sex: "*",
						birthMonth: "*",
						birthDay: "*",
						birthYear: "*",
						reg_email_confirmation__:{
							equalTo: "Your email does not match"
						}
					},
					rules: {
						reg_email_confirmation__: {
						  equalTo: "#reg_email__2"
						},
						reg_passwd__: {
							validPassword: true
						},
						sex:{
							min: 1
						},
						birthMonth:{
							min: 1
						},
						birthDay:{
							min: 1
						},
						birthYear:{
							min: 1
						}
				  }
				});
			});
		</script>

        <!-- Wotmed Chat System NOTE - MUST BE KEPT EXACTLY HERE-->
        <script type="text/javascript" src="http://platform.wotmed.com/WotmedChat/livechat/php/app.php?widget-init.js"></script>
        <!-- Wotmed Chat System -->

		<link href="style/i_style.css" rel="stylesheet"></link>
        
         <link rel="stylesheet" type="text/css" href="signup.css">
         
	</head>
	<body onload="searchUsers()">
		<div class="topBar" >
			<div class="bar_frame">
                <div class="w_logo"><img src="images/WotmedLogoTransparent.png" height="45" alt="wotmedLogo" style='position:relative; left:-30px; padding:0px;padding-top:10px;' /></div>
				<div class="div_login">
					<form id="loginForm" method="post" action="login.php">
					<table>
						<tr>
							<td><label>Email</label></td>
							<td><label>Password</label></td>
						</tr>
						<tr>
							<td width="150"><input type="text" class="inputtext required email" name="email" id="email"/></td>
							<td width="150"><input type="password" class="inputpassword required" name="password" id="password"/></td>
							<td><input type="submit" value="Login"/></td>
						</tr>
						<tr>
							<td><input name="persistent" type="checkbox" style="float:left; margin: 0px; padding: 0px;"/> <label>Keep me logged in</label></td>
							<td><a href="resetPassword.php"><label style="cursor:pointer;text-decoration:underline;">Forget your password?</label></a></td>
						</tr>
					</table>
					</form>
				<div id="errMsg">
				</div>
				</div>
				<div>
					<?php //include "fbConnect.php"; ?>
				</div>
				<div>
					<?php //include "googleConnect.php"; ?>
				</div>
			</div>
		</div>
		

		<div id="container">
			<!--
			<div id='physicians'>
				<h2><a href="#" onclick="return false;">Today's Wotmed Honour</a></h2>
				
                
                
                
			</div>-->
			<h3><a href="#" onclick="return false;" class="about">Wotmed is the medical network that connects Patients with Practitioners and Surgery Facilitators globally</a></h3>
			<div class="signupForm">
			  <div class="marBotBig headerTextContainer">
					<img src="images/WotmedSurgeryFacilitatorIcon.jpg" alt="" width="92" height="92" />
					<div class="marBotBig fColor fBig bold">

                        <div class='lfloat' style='padding:0px;' id='VideoIcon'>Surgery Facilitator Sign Up  <a href='WotmedExplained.php'><img src='SignUpVideoIconFacilitator.gif' height='20'></a><div id='cardIconPopUp' class='Indices'><a href='WotmedExplained.php'></a></div></div>
                        <br>

                        <div class="fSma fcg">

                            <span>

                                    or
                                 <a href="WotmedSurgeryFacilitatorArticles.php">click here to read surgery facilitator article submissions</a>

                                or
                                 <a href="WotmedClientSpeechSubmissions.php">here to watch or submit a speech</a>

                                or
                                 <a href="http://platform.wotmed.com/WotmedAdvertisingRequest.php">here to buy advertising</a>

                            </span>

                        </div>

		        </div>
					<!--<div class="marBotBig subtitle fMid fColor">&nbsp;</div>-->
				</div>
				<form id="registerForm3" method="post" action="register.php">
				<input type="hidden" name="facilitator" value="TRUE"/>
				<input type='hidden' value='practitioner' name='reg'/>
				<table class="uiGrid editor" cellpadding="1" cellspacing="0">
					<tbody>
						<tr>
							<td class="label">
								<label for="firstname">First Name:</label>
							</td>
							<td>
								<div class="field_container">
									<input class="inputtext required" id="firstname" name="firstname" type=
									"text" />
								</div>
							</td>
						</tr>
						<tr>
							<td class="label">
								<label for="lastname">Last Name:</label>
							</td>
							<td>
								<div class="field_container">
									<input class="inputtext required" id="lastname" name="lastname" type=
									"text" />
								</div>
							</td>
						</tr>
						<tr>
							<td class="label">
								<label for="reg_email__">Your Email:</label>
							</td>
							<td>
								<div class="field_container">
									<input class="inputtext required email" id="reg_email__2" name="reg_email__"
									type="text" />
								</div>
							</td>
						</tr>
						<tr>
							<td class="label">
								<label for="reg_email_confirmation__">Re-enter Email:</label>
							</td>
							<td>
								<div class="field_container">
									<input class="inputtext required" id="reg_email_confirmation__" name=
									"reg_email_confirmation__" type="text" />
								</div>
							</td>
						</tr>
						<tr>
							<td class="label">
								<label for="reg_passwd__">New Password:</label>
							</td>
							<td>
								<div class="field_container">
									<input class="inputtext" id="reg_passwd__" name="reg_passwd__"
									value="" type="password" />
								</div>
							</td>
						</tr>
						<tr>
							<td class="label">
								<label>I am:</label>
							</td>
							<td>
								<div class="field_container">
									<div class="hidden_elem"></div><select class="select" name="sex"
									id="sex">
										<option value="0">
											Select Sex:
										</option>
										<option value="1">
											Female
										</option>
										<option value="2">
											Male
										</option>

									</select>
								</div>
							</td>
						</tr>

						<tr>
							<td class="label">
								<label>Birthday:</label>
							</td>
							<td>
								<div class="field_container">
								  <select id="birthYear" name="birthYear">
									<option label="Year:" value="0" selected="selected">Year:</option>
									<?php
									$currentYear = getdate();
									for($i=0;$i<100;$i++){
										$year=$currentYear["year"]-$i;
										echo "<option value='$year'>$year</option>";
									} 									
									?>
								  </select>
									<select id="birthMonth" name="birthMonth">
									<option label="Month:" value="0" selected="selected">Month:</option>
									<option label="January" value="1">January</option>
									<option label="February" value="2">February</option>
									<option label="March" value="3">March</option>
									<option label="April" value="4">April</option>
									<option label="May" value="5">May</option>
									<option label="June" value="6">June</option>
									<option label="July" value="7">July</option>
									<option label="August" value="8">August</option>
									<option label="September" value="9">September</option>
									<option label="October" value="10">October</option>
									<option label="November" value="11">November</option>
									<option label="December" value="12">December</option>
									</select>
									<select id="birthDay" name="birthDay">
									<option label="Day:" value="0" selected="selected">Day:</option>
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">5</option>
									<option value="6">6</option>
									<option value="7">7</option>
									<option value="8">8</option>
									<option value="9">9</option>
									<option value="10">10</option>
									<option value="11">11</option>
									<option value="12">12</option>
									<option value="13">13</option>
									<option value="14">14</option>
									<option value="15">15</option>
									<option value="16">16</option>
									<option value="17">17</option>
									<option value="18">18</option>
									<option value="19">19</option>
									<option value="20">20</option>
									<option value="21">21</option>
									<option value="22">22</option>
									<option value="23">23</option>
									<option value="24">24</option>
									<option value="25">25</option>
									<option value="26">26</option>
									<option value="27">27</option>
									<option value="28">28</option>
									<option value="29">29</option>
									<option value="30">30</option>
									<option value="31">31</option>
									</select>
								</div>
							</td>
						</tr>
						<tr>
							<td class="label"></td>
							<td>
								<div>
									<p class="privacy_policy_text text">
										By clicking Sign Up, you agree to our <a href="WotmedFacilitatorDisclaimer.php"
										>Surgery Facilitator Disclaimer</a>
									</p>
								</div>
							</td>
						</tr>
						<tr>
							<td class="label"></td>
							<td>
								<input type="submit" value="Surgery Facilitator Sign Up" id='signupsubmitfacilitator'/>
							</td>
						</tr>
					</tbody>
				</table>
				</form>
			</div>
			<?php include "includes/p_footer.php"; ?>
