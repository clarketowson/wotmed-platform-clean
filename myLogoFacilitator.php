<?php
	session_start();
	
	header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");


	include "includes/connect.php";
	include "includes/functions.php";
	include "classes/SimpleImage.php";
	
	$rowSession=getParticipantDetail($conn,$_SESSION['id']);
	$row=getPractitionerDetail($conn,$_SESSION['id']);
	
	$ppFileNameSession="blankSilhouetteMale.png";
	$ppFileName="blankSilhouetteMale.png";
	
	



	
	if($row['PRACTITIONER_BUSINESSLOGO']!="")
	{
		$ppFileNameSession=$row['PRACTITIONER_BUSINESSLOGO'];
		$ppFileName=$row['PRACTITIONER_BUSINESSLOGO'];
	}
	
	
	
//	if($rowSession['PROFILEPHOTO']!="")
//	{
//		$ppFileNameSession=$rowSession['PROFILEPHOTO'];
//	}

	if(isset($_SESSION['id']))
	{
		$rowSession=getParticipantDetail($conn,$_SESSION['id']);
		$row=getPractitionerDetail($conn,$_SESSION['id']);
		if($rowSession['PROFILEPHOTO']=="")
		{
			$ppFileNameSession="blankSilhouetteMale.png";
		}else
		{
			$ppFileNameSession=$row['PRACTITIONER_BUSINESSLOGO'];
		
		}
	}
	
	$query="SELECT COUNT(*) FROM PATIENTPRACTITIONERRELATIONSHIP WHERE PRACTITIONER_NUMBER = '" . $_SESSION['practitioner_id'] . "' AND RECOMMENDEDAS='1'";
		$tempOfRecommend=mysqli_query($conn,$query);
		if(mysqli_num_rows($tempOfRecommend)!=0)
			$numbOfRecommend=mysqli_fetch_array($tempOfRecommend);
		else
			$numbOfRecommend[]=0;
		if(isset($_FILES) && isset($_FILES["flogo"]["name"])){
			if($_FILES["flogo"]["name"]!=NULL){
				$name=$_FILES["flogo"]["name"];
				$tmpname=$_FILES["flogo"]["tmp_name"];
				$size=$_FILES["flogo"]["size"];
				
				if (($_FILES["flogo"]["type"] == "image/jpeg") || ($_FILES["flogo"]["type"] == "image/png") || ($_FILES["flogo"]["type"] == "image/gif"))
				{
					$type="";
					switch ($_FILES["flogo"]["type"])
					{
						case "image/jpeg":
							$type=".jpg";
							break;
						case "image/png":
							$type=".png";
							break;
						case "image/gif":
							$type=".gif";
							break;
					}
					
					// note carefully - updated the _ to have a P in front of it so that the image does not
					// conflict with the profile image
					// profile image is l_ and business logo is P_
					// this fixes the problem where the profile photo and the business logo
					// were being mixed up in the control panel and on the practitioner profiles
					// Clarke towson change Friday 30th May 2014
					
					
					$image = new SimpleImage();
					$image->load($_FILES['flogo']['tmp_name']);
					$image->save('photos/originals/SFPP' . $_SESSION['id'] . $type);
					$image->resizeToHeight(175);
					$image->save('photos/thumbs/SFPP' . $_SESSION['id'] . $type);
					
					@mysqli_query($conn,"UPDATE PRACTITIONER SET PRACTITIONER_BUSINESSLOGO='SFPP" . $_SESSION['id'] . $type . "' WHERE PARTICIPANT_NUMBER='" . $_SESSION['id'] . "'");
					
			//		@mysqli_query($conn,"UPDATE PARTICIPANT SET PROFILEPHOTO='l_" . $_SESSION['id'] . $type . "' WHERE PARTICIPANT_NUMBER='" . $_SESSION['id'] . "'");
			
					?>
					<script language="javascript"> 
						<?php echo "window.location = 'myLogoFacilitator.php'";?>
					</script> <?php
				}
				else
					echo "<BR><BR><BR>Upload Image Failed";
			}
		}
		$result=null;
		if($result==null){
		}else{
			$data=mysqli_fetch_array($result);
		}
	
	
	
	
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link href="<?php echo $path; ?>style/p_style.css" rel="stylesheet"></link>
<script type="text/javascript" src="<?php echo $path; ?>classes/jquery.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>My Logo</title>
<link href="style/apple.css" rel="stylesheet" type="text/css" />
</head>

<body>
<?php include $path."includes/p_header.php"; ?>
<?php
if($row['ISFACILITATOR'] == 1){
	$backLink = "
		<a href='facilitator/cpanel.php'>Back to Your Control Panel</a>
	";
	include $path."facilitator/cpanelHeader.php"; 
}
else{
	$backLink = "
		<a href='cpanel.php'>Back to Your Control Panel</a>
	";
	include $path."cpanelHeader.php"; 
}
?>
<div class='lfloat' style='width:98%'>
	<div class="stdWrapper" onMouseOver="this.style.background='white';"onmouseout="this.style.background='#F0F0F0';">
		<?php
			$logo=mysqli_query($conn,"SELECT PRACTITIONER_BUSINESSLOGO FROM PRACTITIONER WHERE PARTICIPANT_NUMBER='" . $_SESSION['id'] . "'");
			$logoImage=mysqli_fetch_array($logo);
		?>
	  <p><span class="PractitionerBody"><span class="copyrightText">
	  <?php
	  if($logoImage[0]!="")
		echo "<img src='photos/thumbs/" . $logoImage[0] . "' alt='' height='72' />";
	  else
		echo "<img src='images/default_logo.jpg' alt='' width='72' height='72' />";
	  ?>
	  </span><h3 class="hyperlinks">Update Logo</h3><a href="http://www.wotmed.com/myLogoFacilitator.php" class="hyperlinks"></a></span></p>
	  <p class="PractitionerMainText">Update your logo here</p>
	  <form action="" method="post" enctype="multipart/form-data" name="UpdatePractitionerPromotion" id="UpdatePractitionerPromotion">
		<table width="789" border="0">
		  <tr>
			<td colspan="2">
			
			
			</td>
		  </tr>
		  <tr>
			<td><span class="PractitionerMainText">Image</span></td>
			<td><input type="file" name="flogo" id="PreviewLogo2" value="Upload Logo" /></td>
		  </tr>
		  <!--<tr>
			<td><span class="PractitionerMainText">Logo Text</span></td>
			<td><span class="PractitionerMainText">
			  <label>
				<textarea name="SloganDescription2" id="SloganDescription2" cols="80" rows="5"></textarea>
			  </label>
			</span></td>
		  </tr>-->
		</table>
		<p class="PractitionerMainText">
		<label>
		  <input type="submit" name="PreviewLogo" id="PreviewLogo" value="Preview Logo" />
		</label>
		<input type="submit" name="UploadLogo" id="UploadLogo" value="Update Logo" />
	  </p>
	  </form>
	</div>
</div>
<p>&nbsp;</p>
<?php include $path."includes/p_footer.php"; ?>
</body>

</html>
