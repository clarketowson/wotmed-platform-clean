<?php
	session_start();
	include "includes/connect.php";
	include "includes/functions.php";
	if(isset($_SESSION['id'])){
		$rowSession=getParticipantDetail($conn,$_SESSION['id']);
	}
	if($rowSession['PROFILEPHOTO']==""){
		$ppFileNameSession="blankSilhouetteMale.png";
	}else{
		$ppFileNameSession=$rowSession['PROFILEPHOTO'];
	}
?>
<html>
	<head>
		<title>Advance Search</title>
		<meta http-equiv="Content-Type" content="'text'/html; charset=utf-8" />
		<link href="style/p_style.css" rel="stylesheet"></link>
		<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
		<script type="text/javascript" src="classes/jquery.js"></script>
		<style type="text/css">
		  html { height: 100% }
		  body { width: 100%; height: 100%; margin: 0; padding: 0 }
		  #map_canvas { height: 100% }
		</style>
		<script type="text/javascript"
		  src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCtaXJ8ECL-qmxpuKQc5HCmXK9VxF8uiW4&sensor=true">
		</script>
		<script type="text/javascript" src="classes/maps.js">
		</script>
		<script type="text/javascript">
			
			$(document).ready(function(){
				$('#container').css('width','95%');
				$('#container').css('min-width','980px');
				$('#container').css('margin-top','8px');
			});
		</script>
		<script type="text/javascript">
		function back()
		{
			window.location = "profile.php"
		}
		</script>
	</head>
	<body>
	<?php include "includes/p_header.php"; ?>
	<div class="lfloat" style="width:20%">
		<form method="post" action="advanceSearch.php">
			<table>
				<tr>
					<td>Practitioner Name : </td>
				</tr>
				<tr>
					<td><input type='text' name='name'></td>
				</tr>
				<tr>
					<td>Language Spoken : </td>
				</tr>
				<tr>
					<td><input type='text' name='language'></td>
				</tr>
				<tr>
					<td>Speciality : </td>
				</tr>
				<tr>
					<td>
						<select name='speciality' class='myselect' style='width:200px;'>
							<option value='0'>Any</option>
						<?php
							$query="SELECT * FROM SPECIALITY";
							$result=mysqli_query($conn,$query);
							while($speciality=mysqli_fetch_array($result)){
								if(isset($_POST['speciality']) && $_POST['speciality']!=""){
									if($_POST['speciality']==$speciality[0])
										echo "<option value='" . $speciality[0] . "' selected>" . $speciality[1] . "</option>";
									else
										echo "<option value='" . $speciality[0] . "'>" . $speciality[1] . "</option>";
								}
								else
									echo "<option value='" . $speciality[0] . "'>" . $speciality[1] . "</option>";
							}
						?>
						</select>
					</td>
				</tr>
				<tr>
					<td>Gender : </td>
				</tr>
				<tr>
					<td>
						<select name='gender'>
							<option value='A'>Any</option>
							<option value='F'>Female</option>
							<option value='M'>Male</option>
						</select>
					</td>
				</tr>
				<tr>
					<td>Area : </td>
				</tr>
				<tr>
					<td><input type='text' name='area'/></td>
				</tr>
				<tr>
					<td>Suburb : </td>
				</tr>
				<tr>
					<?php
					if(isset($_POST['suburb']) && $_POST['suburb']!="")
						echo "<td><input type='text' name='suburb' value='" . $_POST['suburb'] . "'></td>";
					else
						echo "<td><input type='text' name='suburb'></td>";
					?>
				</tr>
				<tr>
					<td>Zip : </td>
				</tr>
				<tr>
					<td><input type='text' name='zip'></td>
				</tr>
				<tr>
					<td>Who accepts this insurance : </td>
				</tr>
				<tr>
					<td>
						<select name='insurance' class='myselect' style='width:200px;'>
							<option value='0'>Any</option>
						<?php
							$result=getInsuranceList($conn);
							while($insurance=mysqli_fetch_array($result)){
								if(isset($_POST['insurance']) && $_POST['insurance']!=""){
									if($_POST['insurance']==$insurance[1])
										echo "<option value='" . $insurance[0] . "' selected>" . $insurance[1] . "</option>";
									else
										echo "<option value='" . $insurance[0] . "'>" . $insurance[1] . "</option>";
								}
								else
									echo "<option value='" . $insurance[0] . "'>" . $insurance[1] . "</option>";
							}
						?>
						</select>
					</td>
				</tr>
				<tr>
					<td>Available on : </td>
				</tr>
				<tr>
					<td>
						<select name='day' id='day'>
							<option value='0'>Sunday</option>
							<option value='1'>Monday</option>
							<option value='2'>Tuesday</option>
							<option value='3'>Wednesday</option>
							<option value='4'>Thursday</option>
							<option value='5'>Friday</option>
							<option value='6'>Saturday</option>
							<option value='7' selected>Any</option>
						</select>
					</td>
				</tr>
				<tr>
					<td>at : <input type='text' name='startTime1' maxlength='2' size='2'>:<input type='text' name='startTime2' maxlength='2' size='2'> To <input type='text' name='endTime1' maxlength='2' size='2'>:<input type='text' name='endTime2' maxlength='2' size='2'></td>
				</tr>
				<tr>
					<td><input type="submit" value="Find Now">
					<input type="button" value="Cancel" onClick="back()"/></td>
				</tr>
			</table>
		</form>
		<?php
			if(isset($_POST['name']))
			{
				$query="SELECT pr.PRACTITIONER_BUSINESSNAME, pr.PARTICIPANT_NUMBER FROM PARTICIPANT pa, PRACTITIONER pr LEFT JOIN SPECIALITY sp ON (pr.SPECIALITY_NUMBER = sp.SPECIALITY_NUMBER) WHERE pr.PARTICIPANT_NUMBER = pa.PARTICIPANT_NUMBER AND pr.PRACTITIONER_NUMBER NOT IN (2,3,4,5) ";
				$name=mysqli_real_escape_string($conn,$_POST['name']);
				if($name!="")
				{
					$query.=" AND pr.PRACTITIONER_BUSINESSNAME LIKE '" . $name . "%'";
				}
				$language=mysqli_real_escape_string($conn,$_POST['language']);
				if($language!="")
				{
					$query.=" AND pa.PARTICIPANT_NUMBER IN (SELECT PARTICIPANT_NUMBER FROM LANGUAGESPOKEN ls, MASTERLANGUAGE ml WHERE ls.LANGUAGE_NUMBER = ml.LANGUAGE_NUMBER AND ml.LANGUAGE LIKE '" . $language . "%')";
				}
				$speciality=mysqli_real_escape_string($conn,$_POST['speciality']);
				if($speciality!="0")
				{
					$query.=" AND pr.SPECIALITY_NUMBER='" . $speciality . "'";
				}
				$gender=mysqli_real_escape_string($conn,$_POST['gender']);
				if($gender!="A")
				{
					$query.=" AND pa.GENDER='" . $gender . "'";
				}
				$area=mysqli_real_escape_string($conn,$_POST['area']);
				$suburb=mysqli_real_escape_string($conn,$_POST['suburb']);
				if($suburb!="")
				{
					$query.=" AND pr.PRACTITIONER_BUSINESSSUBURB='" . $suburb . "'";
				}
				$zip=mysqli_real_escape_string($conn,$_POST['zip']);
				if($zip!="")
				{
					$query.=" AND pr.PRACTITIONER_BUSINESSPOSTCODE='" . $zip . "'";
				}
				$insurance=mysqli_real_escape_string($conn,$_POST['insurance']);
				if($insurance!="0")
				{
					$query.=" AND pr.PRACTITIONER_NUMBER IN (SELECT PRACTITIONER_NUMBER FROM ACCEPTEDHEALTHINSURANCE ai, HEALTHINSURANCEPROVIDER hi WHERE hi.HEALTHINSURANCEPROVIDER_NUMBER = ai.HEALTHINSURANCEPROVIDER_NUMBER AND hi.HEALTHINSURANCEPROVIDER_NUMBER = '" . $insurance . "')";
				}
				$day=mysqli_real_escape_string($conn,$_POST['day']);
				if($day!="7")
				{
					$query.=" AND pr.PRACTITIONER_NUMBER IN (SELECT PRACTITIONER_NUMBER FROM AVAILABILITY WHERE DAY ='" . $day . "'";
					$startTime1=mysqli_real_escape_string($conn,$_POST['startTime1']);
					if($startTime1!="")
					{
						$query.=" AND SUBSTRING(TIMESTART,1,2) <= " . $startTime1;
					}
					$startTime2=mysqli_real_escape_string($conn,$_POST['startTime2']);
					if($startTime2!="")
					{
						$query.=" AND SUBSTRING(TIMESTART,4,2) <= " . $startTime2;
					}
					$endTime1=mysqli_real_escape_string($conn,$_POST['endTime1']);
					if($endTime1!="")
					{
						$query.=" AND SUBSTRING(TIMEEND,1,2) >= " . $endTime1;
					}
					$endTime2=mysqli_real_escape_string($conn,$_POST['endTime2']);
					if($endTime2!="")
					{
						$query.=" AND SUBSTRING(TIMEEND,4,2) >= " . $endTime2;
					}
					$query.=" GROUP BY PRACTITIONER_NUMBER)";
				}
				$result=mysqli_query($conn,$query);
				//echo $query;
				/*
				if(mysqli_num_rows($result)!=0){
					while($practitionerFound=mysqli_fetch_array($result)){
					echo "<a href='practitioner_profile.php?id=" . $practitionerFound[1] . "'>" . $practitionerFound[0] . "</a><br>";
					}
				}
				else
				{
					echo "<br>No practitioner found for this search";
				}*/
			}
			else if(isset($_POST['suburb'])){
					$query="SELECT pr.PRACTITIONER_BUSINESSNAME, pr.PARTICIPANT_NUMBER FROM PARTICIPANT pa, PRACTITIONER pr LEFT JOIN SPECIALITY sp ON (pr.SPECIALITY_NUMBER = sp.SPECIALITY_NUMBER) WHERE pr.PARTICIPANT_NUMBER = pa.PARTICIPANT_NUMBER";
					$speciality=$_POST['speciality'];
						if($speciality!="0")
						{
							$query.=" AND pr.SPECIALITY_NUMBER='" . $speciality . "'";
						}
					$insurance=$_POST['insurance'];
						if($insurance!="0")
						{
							$query.=" AND pr.PRACTITIONER_NUMBER IN (SELECT PRACTITIONER_NUMBER FROM ACCEPTEDHEALTHINSURANCE ai, HEALTHINSURANCEPROVIDER hi WHERE hi.HEALTHINSURANCEPROVIDER_NUMBER = ai.HEALTHINSURANCEPROVIDER_NUMBER AND hi.HEALTHINSURANCEPROVIDER_NUMBER = '" . $insurance . "')";
						}
					$result=mysqli_query($conn,$query);
				}
			else{
				$result=mysqli_query($conn,"SELECT PRACTITIONER_BUSINESSNAME, PARTICIPANT_NUMBER FROM PRACTITIONER AND PRACTITIONER_NUMBER NOT IN (5,2,3,4)");
			}
		?>
	</div>
	<div class="lfloat" style="width:18%; overflow:auto;">
		<table>
		<?php
			if(mysqli_num_rows($result)!=0){
				$add_str = "";
				$prac_str = "";
				$c=0;
				while($practitionerFound=mysqli_fetch_array($result)){
					$practitioner_details=getPractitionerDetail($conn,$practitionerFound[1]);
					if($practitioner_details['PROFILEPHOTO']=="")
					{
					$pp="blankSilhouetteMale.png";
					}
					else
					{
						$pp=$practitioner_details['PROFILEPHOTO'];
							
					}
					
					if($practitioner_details['ISFACILITATOR']=="1")
					{
						if($practitioner_details['PRACTITIONER_BUSINESSLOGO'] =="")
							{
								$pp="blankSilhouetteMale.png";
							 }
							 else
							 {
								$pp=$practitioner_details['PRACTITIONER_BUSINESSLOGO'];
							 }
					}
					
					
					
					
					/*
					
					
					
					if($practitioner_details['PRACTITIONER_BUSINESSADDRESS'] != ""){
						$address = $practitioner_details['PRACTITIONER_BUSINESSADDRESS'] . ", " . $practitioner_details['PRACTITIONER_BUSINESSSUBURB'] . " " .$practitioner_details['PRACTITIONER_BUSINESSPOSTCODE'];
						
						$tempLoc= convertAddressToLngLat($address) . ";";
						echo $tempLoc . "<br>";
						@mysqli_query($conn,"UPDATE PRACTITIONER SET PRACTITIONER_BUSINESSGOOGLEMAPADDRESS='" . $tempLoc . "' WHERE PRACTITIONER_NUMBER='" . $practitioner_details['PRACTITIONER_NUMBER'] . "'");
					}*/
					$query="SELECT SPECIALITY FROM SPECIALITY WHERE SPECIALITY_NUMBER ='" . $practitioner_details['SPECIALITY_NUMBER'] . "'";
					$speciality=mysqli_query($conn,$query);
					if(mysqli_num_rows($speciality)!=0){
						$spec=mysqli_fetch_array($speciality);
						$spec[0];
					}
					$marker_id="marker_id".$c;
					$ads1="";$ads2="";$ads3="";$ads4="";
					$adsQuery="SELECT * FROM ADVERTS WHERE PRACTITIONER_NUMBER = {$practitioner_details['PARTICIPANT_NUMBER']} LIMIT 0,1";
					$adsResult=mysqli_query($conn,$adsQuery);
					if(mysqli_num_rows($adsResult)!=0){
						$data=mysqli_fetch_array($adsResult);/*
						$link=urlencode($data["ADVERTSHYPERLINK"]);
						$adsTable = "
						<tr style='background: rgb(253,249,228);'>
						<input type='hidden' id='adsID' name='adsID' value='{$data["ADVERTS_NUMBER"]}'>
						<td colspan='2' onclick='advertisment()' onmouseover='ShowText(\'Message\'); return true;' onmouseout='HideText(\'Message\'); return true;'>
						<h3>{$data["ADVERTSTITLE"]}</h3>
						{$data["ADVERTSRTEXT"]}<br>
						<a href='ajax/addAdsClick.php?id={$data["ADVERTS_NUMBER"]}'>{$data["ADVERTSHYPERLINK"]}</a></td></tr>";*/
						$ads1=$data["ADVERTS_NUMBER"];
						$ads2=$data["ADVERTSTITLE"];
						$ads3=$data["ADVERTSRTEXT"];
						$ads4=$data["ADVERTSHYPERLINK"];
					}
					$add_str .= $practitioner_details["PRACTITIONER_BUSINESSGOOGLEMAPADDRESS"];
					$prac_str .= $practitioner_details["PRACTITIONER_BUSINESSLOGO"] . "|" . $practitioner_details["PRACTITIONER_BUSINESSNAME"] . "|" . $practitioner_details["PRACTITIONER_BUSINESSADDRESS"] . "," . $practitioner_details["PRACTITIONER_BUSINESSSUBURB"] . " " . $practitioner_details["PRACTITIONER_BUSINESSPOSTCODE"] . "|" . $ads1 . "|" . $ads2 . "|" . $ads3 . "|" . $ads4 . "|" . $spec[0] . ";";
					echo "<tr onmouseover='changeMarker({$marker_id}.value)' onmouseleave='leaveMarker()'><td><img src='photos/thumbs/" . $pp . "' height='50px'/></td>";
					if($practitioner_details['ISFACILITATOR']==1)
						echo "<td>
							<input type='hidden' id='{$marker_id}' name='{$marker_id}' value='{$c}'>
							<a href='facilitator/index.php?id=" . $practitionerFound[1] . "'> " . $practitionerFound[0] . "</a>
							</td>
						</tr>";
					else
						echo "<td>
							<input type='hidden' id='{$marker_id}' name='{$marker_id}' value='{$c}'>
							<a href='practitioner_profile.php?id=" . $practitionerFound[1] . "'>Dr " . $practitionerFound[0] . "</a>
							</td>
						</tr>";
					$c++;
				}
			}
			else
			{
				echo "<tr><td colspan='2'>No practitioner found for this search</td></tr>";
			}
		?>
		</table>
	</div>
	<div id="invis" style="display:none;"></div>
	<div class="lfloat" style="width:62%">
		<div id="map_canvas" style="width:100%; height:90%"\></div>
		<script type="text/javascript">
			setPlace('<?php echo $add_str; ?>','<?php echo $prac_str; ?>');
		</script>
		<?php
		//echo "<input type='button' name='test' id='test' value='show' onclick=\"setPlace('" . $add_str . "')\">";
		?>
	</div>
	<?php include "includes/p_footer.php"; ?>
	</body>
</html>
