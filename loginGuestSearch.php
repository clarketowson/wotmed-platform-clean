<?php

ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(-1);

session_start();

$errMsg="";

include("includes/connect.php");
include("includes/functions.php");

$status=false;

$email = 'guestsearch@wotmed.com';
$password = 'Password1';

$errMsg=login($conn,$email,$password,"0","");
$redirect="WotmedOpenSearchGuest.php";


//if(isset($_POST['email'])){
//	$inputtext=mysqli_real_escape_string($conn,$_POST['email']);
//	$inputpassword=mysqli_real_escape_string($conn,$_POST['password']);
//	$inputpassword=($_POST['password']);
//	$errMsg=login($conn,$inputtext,$inputpassword,"0","");
//	$redirect="profileGuestSearch.php";
//}
if(isset($_SESSION['practitioner_id'])){
	if(isset($_POST['video'])){
		?>
			<script language="javascript"> 
				<?php echo "window.location = 'gallery/videoPreview.php?fileName=" . $_POST['video'] . "'";?>
			</script> <?php
	}else if(isset($_POST['photo'])){
		?>
			<script language="javascript"> 
				<?php echo "window.location = 'gallery/photoPreview.php?id=" . $_POST['photo'] . "&userId=" .  $_POST['id'] . "'";?>
			</script> <?php
	}
	else{/*
		$rowSession=getPractitionerDetail($conn,$_SESSION['practitioner_id']);
		$query="SELECT CURRENCY_CODE FROM COUNTRY WHERE COUNTRY_NUMBER = '" . $rowSession['B_COUNTRY_NUMBER'] ."'";
		$localCur=mysqli_query($conn,$query);
		if(mysqli_num_rows($localCur)!=0){
			$curCurrency=mysqli_fetch_array($localCur);
			if($curCurrency[0]!="" || $curCurrency[0]!=null)
				setcookie("Currency", $curCurrency[0], time()+3600,"/");
			else
				setcookie("Currency", "AUD", time()+3600,"/");
			
			foreach( readFromXML() as $item){
				if($item[0]=="AUD"){
					$curValue=$item[3];
					setcookie("CurrencyAUD", $curValue, time()+3600,"/");
				}
				if($item[0]==$curCurrency[0]){
					$curValue=$item[2];
					setcookie("CurrencyUSD", $curValue, time()+3600,"/");
				}
			}
		}*/
		$status=true;
		$redirect="practitioner_profile.php";
	}
}
if(isset($_SESSION['id'])){
	$status=true;
}
/*
if(isset($_SESSION['id'])){
	/*
	$row=getParticipantDetail($conn,$_SESSION['id']);
	
	$items=readFromXML("");
	if(!is_array($items)) echo "null";
	echo $row['COUNTRY_NUMBER'];
	if($row['COUNTRY_NUMBER']!=null){
		$query="SELECT CURRENCY_CODE FROM COUNTRY WHERE COUNTRY_NUMBER = '" . $row['COUNTRY_NUMBER'] ."'";
		$localCur=mysqli_query($conn,$query);
		if(mysqli_num_rows($localCur)!=0){
			$curCurrency=mysqli_fetch_array($localCur);
			
			setcookie("Currency", $curCurrency[0], time()+3600,"/");
		
			
			foreach( $items as $item){
				if($item[0]=="AUD"){
					$curValue=$item[3];
					setcookie("CurrencyAUD", $curValue, time()+3600,"/");
				}
				if($item[0]==$curCurrency[0]){
					$curValue=$item[2];
					setcookie("CurrencyUSD", $curValue, time()+3600,"/");
				}
			}
		}else{
		}
	}
	else{
		setcookie("Currency", "AUD", time()+3600,"/");
	
		foreach( $items as $item){
			if($item[0]=="AUD"){
				$curValue=$item[3];
				setcookie("CurrencyAUD", $curValue, time()+3600,"/");
			}
			if($item[0]=="AUD"){
				$curValue=$item[2];
				setcookie("CurrencyUSD", $curValue, time()+3600,"/");
			}
		}
	}
	$status=true;
	$redirect="profile.php";
}*/


if($status)
{
?>
	<script language="javascript"> 
		<?php echo "window.location ='" . $redirect . "'";?>
	</script> <?php
}
?>


<!DOCTYPE html PUBLIC
"-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Please Login to Wotmed</title>
		<meta http-equiv="Content-Type" content="'text'/html; charset=utf-8" />
		<script type="text/javascript" src="classes/jquery.js"></script>
		<script src="classes/jquery.validation.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				$("#loginForm").validate({
					messages: {
						email: "*",
						password: "*"
					}
				});
			});
		</script>
        
        
        
        
        
        
		<link href="style/l_style.css" rel="stylesheet"></link>
        
         <link rel="stylesheet" type="text/css" href="signup.css">
         
         
	</head>
	<body>
		<div class="topBar" >
			<div class="bar_frame">
                <div class="w_logo"><img src="images/WotmedLogoTransparent.png" height="45" alt="wotmedLogo" style='position:relative; left:-30px; padding:0px;padding-top:10px;' /></div>

			</div>
		</div>

		<div id="container">
			<div id="mainLogin">
				<div class="marBotBig headerTextContainer">
					<div class="bold marBotBig subtitle fMid fColor">Login</div>
				</div>
				<?php if($errMsg!=""){ ?>
                
				<div id="errorMessage">
					<div class="bold fSma">Login Failed</div>
					<?php echo $errMsg; ?>
				</div>
				<?php } ?>
				<form id="loginForm" method="post" action="login.php">
                
                
                
                
                
                
                
				<?php
					if(isset($_GET['video']))
						echo "<input type='hidden' id='video' name='video' value='" . $_GET['video'] . "'>";
					if(isset($_GET['photo'])){
						echo "<input type='hidden' id='photo' name='photo' value='" . $_GET['photo'] . "'>";
						echo "<input type='hidden' id='id' name='id' value='" . $_GET['id'] . "'>";
					}
				?>
				<table class="uiGrid loginTable" cellpadding="1" cellspacing="0">
					<tbody>
						<tr>
							<td class="label">
								<label for="reg_email__">Email:</label>
							</td>
							<td>
								<div class="field_container">
									<input class="inputtext required email" id="email" name="email"
									type="text" />
								</div>
							</td>
						</tr>
						<tr>
							<td class="label">
								<label for="reg_passwd__">Password:</label>
							</td>
							<td>
								<div class="field_container">
									<input class="inputtext required" id="password" name="password"
									value="" type="password" />
								</div>
							</td>
						</tr>
						<tr>
							<td class="label"></td>
							<td>
								<input type="submit" value="Login" id='signupsubmit'/>
							</td>
						</tr>
					</tbody>
				</table>
				</form>
			</div>
			<div id="footer">
				<div id="contentCurve"></div>
				<div class="clearfix" id="footerContainer">
					<div class="mrl lfloat" role="contentinfo">
						<div class="fSma fcg">
							<span>Wotmed &copy; 2014</span>
						</div>
					</div>
					<div class="navigation fSma fcg" role="navigation"><a href=
						"#" accesskey="9" title=
						"Review our terms and policies.">Terms</a> &middot; <a href=
						"#" accesskey="0" title=
						"Visit our Help Center.">Help</a>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>
