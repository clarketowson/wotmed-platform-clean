<?php
	session_start();
	include "includes/connect.php";
	include "includes/functions.php";
	$rowSession=getParticipantDetail($conn,$_SESSION['id']);
	$row=getPractitionerDetail($conn,$_SESSION['id']);
	$ppFileNameSession="blankSilhouetteMale.png";
	$ppFileName="blankSilhouetteMale.png";
	
	
	// if the logged in user is a surgery facilitator then we want to show the profile photo at left as the business logo
// otherwise the user is a practitioner and we show the profile photo on the left as the practitioner photo

if($row['ISFACILITATOR'] == 1)
{
	if($row['PRACTITIONER_BUSINESSLOGO']=="")
	{
			$ppFileNameSession="blankSilhouetteMale.png";
	}else
	
		{
			$ppFileNameSession=$row['PRACTITIONER_BUSINESSLOGO'];
		}
		
	}

else
{
	if($row['PRACTITIONER_BUSINESSLOGO']!="")
	{
		$ppFileNameSession=$row['PRACTITIONER_BUSINESSLOGO'];
		$ppFileName=$row['PRACTITIONER_BUSINESSLOGO'];
	}
	if($rowSession['PROFILEPHOTO']!="")
	{
		$ppFileNameSession=$rowSession['PROFILEPHOTO'];
	}
	
}

	
//	if($row['PRACTITIONER_BUSINESSLOGO']!=""){
//		$ppFileNameSession=$row['PRACTITIONER_BUSINESSLOGO'];
//		$ppFileName=$row['PRACTITIONER_BUSINESSLOGO'];
//	}
//	if($rowSession['PROFILEPHOTO']!=""){
//		$ppFileNameSession=$rowSession['PROFILEPHOTO'];
//	}

	$query="SELECT COUNT(*) FROM PATIENTPRACTITIONERRELATIONSHIP WHERE PRACTITIONER_NUMBER = '" . $_SESSION['practitioner_id'] . "' AND RECOMMENDEDAS='1'";
	$tempOfRecommend=mysqli_query($conn,$query);
	if(mysqli_num_rows($tempOfRecommend)!=0)
		$numbOfRecommend=mysqli_fetch_array($tempOfRecommend);
	else
		$numbOfRecommend[]=0;
	if(isset($_POST) && $_POST!=NULL)
	{	
		if($_POST['status']=="insert")
			insertDetails($conn,$row["PRACTITIONER_NUMBER"],$_POST,6);
		if($_POST['status']=="update")
			updateDetails($conn,$_POST['detail_id'],$_POST);
		if($_POST['status']=="delete")
			deleteDetails($conn,$_POST['detail_id']);
		?>
		<script language="javascript"> 
			<?php echo "window.location = 'myDisclaimer.php'";?>
		</script>
		<?php
	}
	$details=getDetails($conn,$row["PRACTITIONER_NUMBER"],6);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link href="<?php echo $path; ?>style/p_style.css" rel="stylesheet"></link>
<script type="text/javascript" src="<?php echo $path; ?>classes/jquery.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>My Disclaimer</title>
<link href="style/apple.css" rel="stylesheet" type="text/css" />
</head>

<body>
<?php include $path."includes/p_header.php"; ?>
<?php
if($row['ISFACILITATOR'] == 1){
	$backLink = "
		<a href='facilitator/cpanel.php'>Back to Your Control Panel</a>
	";
	include $path."facilitator/cpanelHeader.php"; 
}
else{
	$backLink = "
		<a href='cpanel.php'>Back to Your Control Panel</a>
	";
	include $path."cpanelHeader.php"; 
}
?>
<div class='lfloat' style='width:98%'>
	<div class="stdWrapper" onMouseOver="this.style.background='white';"onmouseout="this.style.background='#F0F0F0';">
	  <p><span class="PractitionerBody"><img src="images/justice-balance-icon.png" alt="" width="72" height="72" /><h3 class="hyperlinks">Create New Disclaimer Policy</h3></span></p>
	  <p class="PractitionerMainText">Create your disclaimer policy here</p>
	  <form action="myDisclaimer.php" method="post" enctype="multipart/form-data" name="CreatePractitionerService" id="form1">
		<input type="hidden" name="status" value="insert">
		<table width="789" border="0">
		  <tr>
			<td><span class="PractitionerMainText">Name</span></td>
			<td><span class="PractitionerMainText">
			  <input name="DETAIL_SUBTITLE" type="text" id="DETAIL_SUBTITLE" size="80" maxlength="80" />
			</span></td>
		  </tr>
		  <tr>
			<td><span class="PractitionerMainText">Date</span></td>
			<td><span class="PractitionerMainText">
			  <input name="DETAIL_DATE" type="text" id="DETAIL_DATE" size="40" maxlength="40" placeholder='e.g. 2013/01/31' />
			</span></td>
		  </tr>
		  <tr>
			<td><span class="PractitionerMainText">Disclaimer Description</span></td>
			<td><span class="PractitionerMainText">
			  <label>
				<textarea name="DETAIL_MAINBODY" id="DETAIL_MAINBODY" cols="80" rows="20"></textarea>
			  </label>
			</span></td>
		  </tr>
		</table>
		<p>
		  <label>
			<input type="button" name="PreviewDisclaimer" id="PreviewDisclaimer" value="Preview Disclaimer Policy" />
		  </label>
		  <input type="submit" name="UploadDisclaimer " id="UploadDisclaimer " value="Create New Disclaimer Policy" />
		</p>
	  </form>
	</div>
	<?php 
	if($details!=NULL)
	{
		$total=mysqli_fetch_array(countDetails($conn,$row["PRACTITIONER_NUMBER"],6));
		//$total=5;
		$prev_index=0;
		$counter=1;
		if(!isset($_GET['detail_id']))
		{
			$data=mysqli_fetch_array($details);
		}
		else
		{
			$data=mysqli_fetch_array($details);
			while($data['PRACTITIONERDETAIL_ID']!=$_GET['detail_id'])
			{
				$prev_index=$data['PRACTITIONERDETAIL_ID'];	
				$data=mysqli_fetch_array($details);
				$counter++;
				if($counter > $total[0])
					break;
			}
		}
	?>
	<div class="stdWrapper" onMouseOver="this.style.background='white';"onmouseout="this.style.background='#F0F0F0';">
	  <p><span class="PractitionerBody"><img src="images/justice-balance-icon.png" alt="" width="72" height="72" /><h3 class="hyperlinks">Update your Disclaimer Policy</h3></span></p>
	  <p class="PractitionerMainText">Update your disclaimer policy here</p>
	  <form action="myDisclaimer.php" method="post" enctype="multipart/form-data" name="CreatePractitionerService" id="form1">
		<input type="hidden" name="status" id="status" value="update">
		<input type="hidden" name="detail_id" value="<?php echo $data['PRACTITIONERDETAIL_ID'] ?>">
		<table width="789" border="0">
		  <tr>
			<td><span class="PractitionerMainText">Name</span></td>
			<td><span class="PractitionerMainText">
			  <input name="DETAIL_SUBTITLE" type="text" id="DETAIL_SUBTITLE" size="80" maxlength="80" value="<?php echo $data['DETAIL_SUBTITLE']?>" />
			</span></td>
		  </tr>
		  <tr>
			<td><span class="PractitionerMainText">Date</span></td>
			<td><span class="PractitionerMainText">
			  <input name="DETAIL_DATE" type="text" id="DETAIL_DATE" size="40" maxlength="40" value="<?php echo substr($data['DETAIL_DATE'],0,10)?>" placeholder='e.g. 2013/01/31' />
			</span></td>
		  </tr>
		  <tr>
			<td><span class="PractitionerMainText">Disclaimer Description</span></td>
			<td><span class="PractitionerMainText">
			  <label>
				<textarea name="DETAIL_MAINBODY" id="DETAIL_MAINBODY" cols="80" rows="20"><?php echo $data['DETAIL_MAINBODY']?></textarea>
			  </label>
			</span></td>
		  </tr>
		</table>
		<p>
		<?php if($prev_index != 0){ ?>
		<a href="myDisclaimer.php?detail_id=<?php echo $prev_index;?>"><< Prev</a>
		&nbsp;&nbsp;&nbsp;
		<?php } 
		if($total>1 && $counter<$total[0]){ 
		$data=mysqli_fetch_array($details);
		$next_index=$data['PRACTITIONERDETAIL_ID'];?>
		<a href="myDisclaimer.php?detail_id=<?php echo $next_index;?>">Next >></a>
		<?php } ?>
		</p>
		<p class="PractitionerMainText">
		  <label>
			<input type="button" name="PreviewDisclaimer" id="PreviewDisclaimer" value="Preview Disclaimer  Policy" />
		  </label>
		  <input type="submit" name="UploadDisclaimer" id="UploadDisclaimer" value="Update Disclaimer  Policy" />
		  <input type="submit" name="DeleteDisclaimer" id="DeleteDisclaimer" value="Delete Disclaimer" onClick="javascript:document.getElementById('status').value='delete'" />
		</p>
	  </form>
	</div>
	<?php } ?>
</div>
<p>&nbsp;</p>
<?php include $path."includes/p_footer.php"; ?>
</body>

</html>
