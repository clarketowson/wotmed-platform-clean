

<script type="application/ld+json">
{
"@context": "http://schema.org",
"@type": "Service",
"name": "Buy Wotmed Advertising",
"description": "Buy Wotmed Banner Advertising",
"serviceType": "Banner Advertising on the Wotmed Platform",
"url": "http://platform.wotmed.com/WotmedAdvertisingRequest.php",
"image": "http://platform.wotmed.com/ImagineYourBrandOnWotmedLogo.jpg"

}
}
</script>

<script src="formvalidate/dist/parsley.min.js"></script>

<?php
	/*if ($_SERVER['HTTPS'] != "on") {
		$url = "https://". $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
		header("location: $url");
	}  */

	session_start();
	include("includes/connect.php");
	include("includes/functions.php");

    include_once("paypal/sitebanneradvertising/config.php");


// echo $_SESSION["UniqueFileName"];
	
//	if(isset($_SESSION['id']))
//	{
//		$redirect="profile.php";
//		if(isset($_SESSION['practitioner_id']))
//			$redirect='practitioner_profile.php';
//		redirect($redirect);
//	}
//	elseif(isset($_POST['email']))
//	{
//		$inputtext=$_POST['inputtext'];
//	$inputpassword=$_POST['inputpassword'];
//		login($conn,$inputtext,$inputpassword);
//	}
?>

<?php

$totalVisitorsForMonth = trim(file_get_contents('totalMonthlyUniqueVisitors.txt'));

$totalVisitorsForMonthDividedByTen = $totalVisitorsForMonth / 10;

$conn = new mysqli('liberty.wotmed.com','wotmedco_user','e5~1]qu1|&8)6wJ', 'wotmedco_db');

$conn2 = new mysqli('liberty.wotmed.com','wotmedco_user','e5~1]qu1|&8)6wJ', 'wotmedco_db');

//$conn = new mysqli('localhost','wotmedco_user','Password1', 'WotmedDBLiveMirror');

//$conn2 = new mysqli('localhost','wotmedco_user','Password1', 'WotmedDBLiveMirror');

if ($conn->connect_error)
{
    header('Location: /DatabaseError.shtml');
}

$darkOrangeFactor = 2;
$orangeFactor = 1.5;
$yellowFactor = 1;
$whiteFactor = 0.5;

$oneHundredPercentOfAdvertisingFee = 1;
$fiftyFivePercentOfAdvertisingFee = 0.55;
$fiftyPercentOfAdvertisingFee = 0.5;
$thirtyPercentOfAdvertisingFee = 0.30;
$fifteenPercentOfAdvertisingFee = 0.15;
$tenPercentOfAdvertisingFee = 0.10;

$darkOrangeAdvertisingFeeTotal = $totalVisitorsForMonthDividedByTen * $darkOrangeFactor;
$orangeAdvertisingFeeTotal = $totalVisitorsForMonthDividedByTen * $orangeFactor;
$yellowAdvertisingFeeTotal = $totalVisitorsForMonthDividedByTen * $yellowFactor;
$whiteAdvertisingFeeTotal = $totalVisitorsForMonthDividedByTen * $whiteFactor;

$bannerAdSizeThreeHundredByTwoHundredFiftyDarkOrangeLocationFee = $darkOrangeAdvertisingFeeTotal * $oneHundredPercentOfAdvertisingFee;
$bannerAdSizeThreeHundredByTwoHundredFiftyOrangeLocationFee = $orangeAdvertisingFeeTotal * $oneHundredPercentOfAdvertisingFee;
$bannerAdSizeThreeHundredByTwoHundredFiftyYellowLocationFee = $yellowAdvertisingFeeTotal * $oneHundredPercentOfAdvertisingFee;
$bannerAdSizeThreeHundredByTwoHundredFiftyWhiteLocationFee = $whiteAdvertisingFeeTotal * $oneHundredPercentOfAdvertisingFee;

$result = $conn->query("UPDATE SITEBASEDADVERTISING SET PACKAGEPRICE='$bannerAdSizeThreeHundredByTwoHundredFiftyDarkOrangeLocationFee' WHERE ADVERTISEMENTSIZEANDPACKAGE='300 X 250 DARK ORANGE'");
$result = $conn->query("UPDATE SITEBASEDADVERTISING SET PACKAGEPRICE='$bannerAdSizeThreeHundredByTwoHundredFiftyOrangeLocationFee' WHERE ADVERTISEMENTSIZEANDPACKAGE='300 X 250 ORANGE'");
$result = $conn->query("UPDATE SITEBASEDADVERTISING SET PACKAGEPRICE='$bannerAdSizeThreeHundredByTwoHundredFiftyYellowLocationFee' WHERE ADVERTISEMENTSIZEANDPACKAGE='300 X 250 YELLOW'");
$result = $conn->query("UPDATE SITEBASEDADVERTISING SET PACKAGEPRICE='$bannerAdSizeThreeHundredByTwoHundredFiftyWhiteLocationFee' WHERE ADVERTISEMENTSIZEANDPACKAGE='300 X 250 WHITE'");


$bannerAdSizeSevenHundredTwentyEightByNinetyDarkOrangeLocationFee = $darkOrangeAdvertisingFeeTotal * $oneHundredPercentOfAdvertisingFee;
$bannerAdSizeSevenHundredTwentyEightByNinetyOrangeLocationFee =  $orangeAdvertisingFeeTotal * $oneHundredPercentOfAdvertisingFee;
$bannerAdSizeSevenHundredTwentyEightByNinetyYellowLocationFee = $yellowAdvertisingFeeTotal * $oneHundredPercentOfAdvertisingFee;
$bannerAdSizeSevenHundredTwentyEightByNinetyWhiteLocationFee = $whiteAdvertisingFeeTotal * $oneHundredPercentOfAdvertisingFee;

$result = $conn->query("UPDATE SITEBASEDADVERTISING SET PACKAGEPRICE='$bannerAdSizeSevenHundredTwentyEightByNinetyDarkOrangeLocationFee' WHERE ADVERTISEMENTSIZEANDPACKAGE='728 x 90 DARK ORANGE'");
$result = $conn->query("UPDATE SITEBASEDADVERTISING SET PACKAGEPRICE='$bannerAdSizeSevenHundredTwentyEightByNinetyOrangeLocationFee' WHERE ADVERTISEMENTSIZEANDPACKAGE='728 x 90 ORANGE'");
$result = $conn->query("UPDATE SITEBASEDADVERTISING SET PACKAGEPRICE='$bannerAdSizeSevenHundredTwentyEightByNinetyYellowLocationFee' WHERE ADVERTISEMENTSIZEANDPACKAGE='728 x 90 YELLOW'");
$result = $conn->query("UPDATE SITEBASEDADVERTISING SET PACKAGEPRICE='$bannerAdSizeSevenHundredTwentyEightByNinetyWhiteLocationFee' WHERE ADVERTISEMENTSIZEANDPACKAGE='728 x 90 WHITE'");


$bannerAdSizeOneHundredTwentyBySixHundredDarkOrangeLocationFee = $darkOrangeAdvertisingFeeTotal * $oneHundredPercentOfAdvertisingFee;
$bannerAdSizeOneHundredTwentyBySixHundredOrangeLocationFee = $orangeAdvertisingFeeTotal * $oneHundredPercentOfAdvertisingFee;
$bannerAdSizeOneHundredTwentyBySixHundredYellowLocationFee = $yellowAdvertisingFeeTotal * $oneHundredPercentOfAdvertisingFee;
$bannerAdSizeOneHundredTwentyBySixHundredWhiteLocationFee = $whiteAdvertisingFeeTotal * $oneHundredPercentOfAdvertisingFee;

$result = $conn->query("UPDATE SITEBASEDADVERTISING SET PACKAGEPRICE='$bannerAdSizeOneHundredTwentyBySixHundredDarkOrangeLocationFee' WHERE ADVERTISEMENTSIZEANDPACKAGE='120 x 600 DARK ORANGE'");
$result = $conn->query("UPDATE SITEBASEDADVERTISING SET PACKAGEPRICE='$bannerAdSizeOneHundredTwentyBySixHundredOrangeLocationFee' WHERE ADVERTISEMENTSIZEANDPACKAGE='120 x 600 ORANGE'");
$result = $conn->query("UPDATE SITEBASEDADVERTISING SET PACKAGEPRICE='$bannerAdSizeOneHundredTwentyBySixHundredYellowLocationFee' WHERE ADVERTISEMENTSIZEANDPACKAGE='120 x 600 YELLOW'");
$result = $conn->query("UPDATE SITEBASEDADVERTISING SET PACKAGEPRICE='$bannerAdSizeOneHundredTwentyBySixHundredWhiteLocationFee' WHERE ADVERTISEMENTSIZEANDPACKAGE='120 x 600 WHITE'");


$bannerAdSizeFourHundredSixtyEightBySixtyDarkOrangeLocationFee = $darkOrangeAdvertisingFeeTotal * $fiftyFivePercentOfAdvertisingFee;
$bannerAdSizeFourHundredSixtyEightBySixtyOrangeLocationFee = $orangeAdvertisingFeeTotal * $fiftyFivePercentOfAdvertisingFee;
$bannerAdSizeFourHundredSixtyEightBySixtyYellowLocationFee = $yellowAdvertisingFeeTotal * $fiftyFivePercentOfAdvertisingFee;
$bannerAdSizeFourHundredSixtyEightBySixtyWhiteLocationFee = $whiteAdvertisingFeeTotal * $fiftyFivePercentOfAdvertisingFee;

$result = $conn->query("UPDATE SITEBASEDADVERTISING SET PACKAGEPRICE='$bannerAdSizeFourHundredSixtyEightBySixtyDarkOrangeLocationFee' WHERE ADVERTISEMENTSIZEANDPACKAGE='468 x 60 DARK ORANGE'");
$result = $conn->query("UPDATE SITEBASEDADVERTISING SET PACKAGEPRICE='$bannerAdSizeFourHundredSixtyEightBySixtyOrangeLocationFee' WHERE ADVERTISEMENTSIZEANDPACKAGE='468 x 60 ORANGE'");
$result = $conn->query("UPDATE SITEBASEDADVERTISING SET PACKAGEPRICE='$bannerAdSizeFourHundredSixtyEightBySixtyYellowLocationFee' WHERE ADVERTISEMENTSIZEANDPACKAGE='468 x 60 YELLOW'");
$result = $conn->query("UPDATE SITEBASEDADVERTISING SET PACKAGEPRICE='$bannerAdSizeFourHundredSixtyEightBySixtyWhiteLocationFee' WHERE ADVERTISEMENTSIZEANDPACKAGE='468 x 60 WHITE'");


$bannerAdSizeOneHundredTwentyByTwoHundredFortyDarkOrangeLocationFee = $darkOrangeAdvertisingFeeTotal * $fiftyPercentOfAdvertisingFee;
$bannerAdSizeOneHundredTwentyByTwoHundredFortyOrangeLocationFee = $orangeAdvertisingFeeTotal * $fiftyPercentOfAdvertisingFee;
$bannerAdSizeOneHundredTwentyByTwoHundredFortyYellowLocationFee = $yellowAdvertisingFeeTotal * $fiftyPercentOfAdvertisingFee;
$bannerAdSizeOneHundredTwentyByTwoHundredFortyWhiteLocationFee = $whiteAdvertisingFeeTotal * $fiftyPercentOfAdvertisingFee;

$result = $conn->query("UPDATE SITEBASEDADVERTISING SET PACKAGEPRICE='$bannerAdSizeOneHundredTwentyByTwoHundredFortyDarkOrangeLocationFee' WHERE ADVERTISEMENTSIZEANDPACKAGE='120 x 240 DARK ORANGE'");
$result = $conn->query("UPDATE SITEBASEDADVERTISING SET PACKAGEPRICE='$bannerAdSizeOneHundredTwentyByTwoHundredFortyOrangeLocationFee' WHERE ADVERTISEMENTSIZEANDPACKAGE='120 x 240 ORANGE'");
$result = $conn->query("UPDATE SITEBASEDADVERTISING SET PACKAGEPRICE='$bannerAdSizeOneHundredTwentyByTwoHundredFortyYellowLocationFee' WHERE ADVERTISEMENTSIZEANDPACKAGE='120 x 240 YELLOW'");
$result = $conn->query("UPDATE SITEBASEDADVERTISING SET PACKAGEPRICE='$bannerAdSizeOneHundredTwentyByTwoHundredFortyWhiteLocationFee' WHERE ADVERTISEMENTSIZEANDPACKAGE='120 x 240 WHITE'");

$bannerAdSizeOneHundredTwentyFiveByOneHundredTwentyFiveDarkOrangeLocationFee = $darkOrangeAdvertisingFeeTotal * $fiftyPercentOfAdvertisingFee;
$bannerAdSizeOneHundredTwentyFiveByOneHundredTwentyFiveOrangeLocationFee = $orangeAdvertisingFeeTotal * $fiftyPercentOfAdvertisingFee;
$bannerAdSizeOneHundredTwentyFiveByOneHundredTwentyFiveYellowLocationFee = $yellowAdvertisingFeeTotal * $fiftyPercentOfAdvertisingFee;
$bannerAdSizeOneHundredTwentyFiveByOneHundredTwentyFiveWhiteLocationFee = $whiteAdvertisingFeeTotal * $fiftyPercentOfAdvertisingFee;

$result = $conn->query("UPDATE SITEBASEDADVERTISING SET PACKAGEPRICE='$bannerAdSizeOneHundredTwentyFiveByOneHundredTwentyFiveDarkOrangeLocationFee' WHERE ADVERTISEMENTSIZEANDPACKAGE='125 x 125 DARK ORANGE'");
$result = $conn->query("UPDATE SITEBASEDADVERTISING SET PACKAGEPRICE='$bannerAdSizeOneHundredTwentyFiveByOneHundredTwentyFiveOrangeLocationFee' WHERE ADVERTISEMENTSIZEANDPACKAGE='125 x 125 ORANGE'");
$result = $conn->query("UPDATE SITEBASEDADVERTISING SET PACKAGEPRICE='$bannerAdSizeOneHundredTwentyFiveByOneHundredTwentyFiveYellowLocationFee' WHERE ADVERTISEMENTSIZEANDPACKAGE='125 x 125 YELLOW'");
$result = $conn->query("UPDATE SITEBASEDADVERTISING SET PACKAGEPRICE='$bannerAdSizeOneHundredTwentyFiveByOneHundredTwentyFiveWhiteLocationFee' WHERE ADVERTISEMENTSIZEANDPACKAGE='125 x 125 WHITE'");

$bannerAdSizeOneHundredTwentyByNinetyDarkOrangeLocationFee = $darkOrangeAdvertisingFeeTotal * $thirtyPercentOfAdvertisingFee;
$bannerAdSizeOneHundredTwentyByNinetyOrangeLocationFee = $orangeAdvertisingFeeTotal * $thirtyPercentOfAdvertisingFee;
$bannerAdSizeOneHundredTwentyByNinetyYellowLocationFee = $yellowAdvertisingFeeTotal * $thirtyPercentOfAdvertisingFee;
$bannerAdSizeOneHundredTwentyByNinetyWhiteLocationFee = $whiteAdvertisingFeeTotal * $thirtyPercentOfAdvertisingFee;

$result = $conn->query("UPDATE SITEBASEDADVERTISING SET PACKAGEPRICE='$bannerAdSizeOneHundredTwentyByNinetyDarkOrangeLocationFee' WHERE ADVERTISEMENTSIZEANDPACKAGE='120 x 90 DARK ORANGE'");
$result = $conn->query("UPDATE SITEBASEDADVERTISING SET PACKAGEPRICE='$bannerAdSizeOneHundredTwentyByNinetyOrangeLocationFee ' WHERE ADVERTISEMENTSIZEANDPACKAGE='120 x 90 ORANGE'");
$result = $conn->query("UPDATE SITEBASEDADVERTISING SET PACKAGEPRICE='$bannerAdSizeOneHundredTwentyByNinetyYellowLocationFee' WHERE ADVERTISEMENTSIZEANDPACKAGE='120 x 90 YELLOW'");
$result = $conn->query("UPDATE SITEBASEDADVERTISING SET PACKAGEPRICE='$bannerAdSizeOneHundredTwentyByNinetyWhiteLocationFee' WHERE ADVERTISEMENTSIZEANDPACKAGE='120 x 90 WHITE'");

$bannerAdSizeOneHundredTwentyBySixtyDarkOrangeLocationFee = $darkOrangeAdvertisingFeeTotal * $fifteenPercentOfAdvertisingFee;
$bannerAdSizeOneHundredTwentyBySixtyOrangeLocationFee = $orangeAdvertisingFeeTotal * $fifteenPercentOfAdvertisingFee;
$bannerAdSizeOneHundredTwentyBySixtyYellowLocationFee = $yellowAdvertisingFeeTotal * $fifteenPercentOfAdvertisingFee;
$bannerAdSizeOneHundredTwentyBySixtyWhiteLocationFee = $whiteAdvertisingFeeTotal * $fifteenPercentOfAdvertisingFee;

$result = $conn->query("UPDATE SITEBASEDADVERTISING SET PACKAGEPRICE='$bannerAdSizeOneHundredTwentyBySixtyDarkOrangeLocationFee' WHERE ADVERTISEMENTSIZEANDPACKAGE='120 x 60 DARK ORANGE'");
$result = $conn->query("UPDATE SITEBASEDADVERTISING SET PACKAGEPRICE='$bannerAdSizeOneHundredTwentyBySixtyOrangeLocationFee ' WHERE ADVERTISEMENTSIZEANDPACKAGE='120 x 60 ORANGE'");
$result = $conn->query("UPDATE SITEBASEDADVERTISING SET PACKAGEPRICE='$bannerAdSizeOneHundredTwentyBySixtyYellowLocationFee' WHERE ADVERTISEMENTSIZEANDPACKAGE='120 x 60 YELLOW'");
$result = $conn->query("UPDATE SITEBASEDADVERTISING SET PACKAGEPRICE='$bannerAdSizeOneHundredTwentyBySixtyWhiteLocationFee' WHERE ADVERTISEMENTSIZEANDPACKAGE='120 x 60 WHITE'");

$bannerAdSizeEightyEightByThirtyOneDarkOrangeLocationFee = $darkOrangeAdvertisingFeeTotal * $tenPercentOfAdvertisingFee;
$bannerAdSizeEightyEightByThirtyOneOrangeLocationFee = $orangeAdvertisingFeeTotal * $tenPercentOfAdvertisingFee;
$bannerAdSizeEightyEightByThirtyOneYellowLocationFee = $yellowAdvertisingFeeTotal * $tenPercentOfAdvertisingFee;
$bannerAdSizeEightyEightByThirtyOneWhiteLocationFee = $whiteAdvertisingFeeTotal * $tenPercentOfAdvertisingFee;

$result = $conn->query("UPDATE SITEBASEDADVERTISING SET PACKAGEPRICE='$bannerAdSizeEightyEightByThirtyOneDarkOrangeLocationFee' WHERE ADVERTISEMENTSIZEANDPACKAGE='88 x 31 DARK ORANGE'");
$result = $conn->query("UPDATE SITEBASEDADVERTISING SET PACKAGEPRICE='$bannerAdSizeEightyEightByThirtyOneOrangeLocationFee ' WHERE ADVERTISEMENTSIZEANDPACKAGE='88 x 31 ORANGE'");
$result = $conn->query("UPDATE SITEBASEDADVERTISING SET PACKAGEPRICE='$bannerAdSizeEightyEightByThirtyOneYellowLocationFee' WHERE ADVERTISEMENTSIZEANDPACKAGE='88 x 31 YELLOW'");
$result = $conn->query("UPDATE SITEBASEDADVERTISING SET PACKAGEPRICE='$bannerAdSizeEightyEightByThirtyOneWhiteLocationFee' WHERE ADVERTISEMENTSIZEANDPACKAGE='88 x 31 WHITE'");

$conn->close();

?>




<!DOCTYPE html PUBLIC
"-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
		<title>Wotmed Buy Advertising</title>
		<meta http-equiv="Content-Type" content="'text'/html; charset=utf-8" />

    <!-- A minimal Flowplayer setup to get you started -->


    <!--
        include flowplayer JavaScript file that does
        Flash embedding and provides the Flowplayer API.
    -->
    <script type="text/javascript" src="flowplayer-3.2.11.min.js"></script>


        
        
 <script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-38343871-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

    <!-- Wotmed Chat System NOTE - MUST BE KEPT EXACTLY HERE-->
    <script type="text/javascript" src="http://platform.wotmed.com/WotmedChat/livechat/php/app.php?widget-init.js"></script>
    <!-- Wotmed Chat System -->

    <link href="style/i_style.css" rel="stylesheet"></link>

    <link rel="stylesheet" type="text/css" href="signup.css">



    <script type="text/javascript" src="ImageUpload/js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="ImageUpload/js/jquery.form.min.js"></script>



    <script type="text/javascript">
        $(document).ready(function() {
            var options = {
                target: '#output',   // target element(s) to be updated with server response
                beforeSubmit: beforeSubmit,  // pre-submit callback
                success: afterSuccess,  // post-submit callback
                resetForm: true        // reset the form after successful submit
            };

            $('#MyUploadForm').submit(function() {
                $(this).ajaxSubmit(options);
                // always return false to prevent standard browser submit and page navigation
                return false;
            });
        });

        function afterSuccess()
        {
            $('#submit-btn').show(); //hide submit button
            $('#loading-img').hide(); //hide submit button

        }

        //function to check file size before uploading.
        function beforeSubmit(){
            //check whether browser fully supports all File API
            if (window.File && window.FileReader && window.FileList && window.Blob)
            {

                if( !$('#imageInput').val()) //check empty input filed
                {
                    $("#output").html("No Image specified");
                    return false
                }

                var fsize = $('#imageInput')[0].files[0].size; //get file size
                var ftype = $('#imageInput')[0].files[0].type; // get file type


                //allow only valid image file types
                switch(ftype)
                {
                    case 'image/png': case 'image/gif': case 'image/jpeg': case 'image/pjpeg':
                    break;
                    default:
                        $("#output").html("<b>"+ftype+"</b> You have attempted to upload an unsupported file type.  Your uploaded advertisement must be an image.");
                        return false
                }

                //Allowed file size is less than 1 MB (1048576)
                if(fsize>1048576)
                {
                    $("#output").html("<b>"+bytesToSize(fsize) +"</b> Your advertisement is too big <br />Please reduce the size of your banner advertisement using an image editor.");
                    return false
                }

                $('#submit-btn').hide(); //hide submit button
                $('#loading-img').show(); //hide submit button
                $("#output").html("");
            }
            else
            {
                //Output error to older browsers that do not support HTML5 File API
                $("#output").html("Please upgrade your browser, because your current browser lacks the features required to upload your advertisement");
                return false;
            }
        }

        //function to format bites bit.ly/19yoIPO
        function bytesToSize(bytes) {
            var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
            if (bytes == 0) return '0 Bytes';
            var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
            return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
        }

    </script>


    <link href="ImageUpload/style/style.css" rel="stylesheet" type="text/css">

<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-49954656-1', 'wotmed.com');
		  ga('send', 'pageview');

		</script>





 <!--   <link rel="stylesheet" href="/proposals/date/css/site.css"> -->


 <!--   <style type="text/css">
        <!--
        #PatientBudget {	font-size:14px;
        }
        #PatientCountries {	font-size:14px;
        }
        #ProcedureList {
            font-size:14px;
        }
        #Submit {	background-color:#125c5d;
            -moz-border-radius:1px;
            -webkit-border-radius:1px;
            border-radius:1px;
            display:inline-block;
            cursor:pointer;
            color:#ffffff;
            font-family:arial;
            font-size:22px;
            padding:12px 50px;
            text-decoration:none;
            text-shadow:0px 1px 0px #125c5d;
        }

    </style> -->



    <meta name="description" content="Buy Wotmed Advertising Now.  Affordable advertising packages.  Advertise to Doctors and Dentists globally.  Wotmed.com – Facilitating Medical Travel">
    <meta name="keywords" content="Wotmed.com, Buy Advertising, Banner Advertising, Buy Banner Advertising, Affordable advertising packages, Advertise to Dentists and Doctors globally, Facilitating Medical Travel">

    <meta property="og:locale" content="en_US">
    <meta property="og:type" content="website">
    <meta property="og:title" content="Buy Wotmed Advertising">
    <meta property="og:description" content="Imagine Your Brand on Wotmed.com">
    <meta property="og:site_name" content="Wotmed">
    <meta property="og:url" content="http://platform.wotmed.com/WotmedAdvertisingRequest.php">
    <meta property="og:image" content="http://platform.wotmed.com/ImagineyourBrandWotmedFacebook.png">
        
	</head>
	<body onload="searchUsers()">
		<div class="topBar" >
			<div class="bar_frame">
				<div class="w_logo"><img src="images/WotmedLogoTransparent.png" height="45" alt="wotmedLogo" style='position:relative; left:-30px; padding:0px;padding-top:10px;' /></div>
				<div class="div_login">
					<form id="loginForm" method="post" action="login.php">
					<table>
						<tr>
							<td><label>Email</label></td>
							<td><label>Password</label></td>
						</tr>
						<tr>
							<td width="150"><input type="text" class="inputtext required email" name="email" id="email"/></td>
							<td width="150"><input type="password" class="inputpassword required" name="password" id="password"/></td>
							<td><input type="submit" value="Login"/></td>
						</tr>
						<tr>
							<td><input name="persistent" type="checkbox" style="float:left; margin: 0px; padding: 0px;"/> <label>Keep me logged in</label></td>
							<td><a href="resetPassword.php"><label style="cursor:pointer;text-decoration:underline;">Forget your password?</label></a></td>
                            
                           
						</tr>
                        
					</table>
					</form>
				<div id="errMsg">
				</div>
				</div>
				<div>
					<?php //include "fbConnect.php"; ?>
				</div>
				<div>
					<?php //include "googleConnect.php"; ?>
				</div>
			</div>
		</div>



        <div id="container">


            <h3><a href="#" onclick="return false;" class="about">Wotmed is the medical network that connects Patients with Practitioners and Surgery Facilitators globally</a></h3>
            <p>
                &nbsp;</p>

            <div id="system">


                <article class="item">




                    <div class="content clearfix">


                        <h1><strong>Buy Wotmed Advertising</strong></h1>



                        <p> <b>Please ensure that you have any ad blocking software turned off to view this page correctly.</b></p>
                        <BR>
                        <img src="ImagineYourBrandOnWotmedLogo.jpg" height="375" width="500">
                        <p> Are you a Doctor, Dentist or other Medical or Dental Practitioner that would like to sell to Patients globally?</p>
                        <p> Are you a Doctor, Dentist or other Medical or Dental Practitioner that would like to sell to other Doctors, Dentists or other Medical or Dental Practitioners globally?</p>
                        <p> Are you a Marketer that would like to sell products or services to Doctors, Dentists or other Medical or Dental Practitioners or even Patients globally?</p>
                        <p> Are you a Surgery Facilitator that would like to sell your facilitation services to Patients globally?</p>


                        <br><p><b>If so - you're in the right place to buy advertising!</b></p><br>
                        <p> We will place your advertisement as a banner on the Wotmed site.  We also accept <a href="http://platform.wotmed.com/WotmedVideoAdvertisingRequestStep1.php">video advertising which is our premium advertising service</a></p>
                        <p> Your advertisement will run for a period of <b>one month from your date of purchase.</b></p><BR>
                        <p> Please note that the Wotmed Team reserves the right to reject advertising that is not in keeping with our values.</p>
                        <p>  Your email address will not be published anywhere on the site.</p>
                        <p> </p>

                        <br>
                        <p><b>Want to know more about Wotmed and what we do?  Please check out the Wotmed Explained video below:</b></p>

                        <span style="text-align: center;"><a
                                href="WotmedFirstAdvertisementVer2.flv"
                                style="display:block;width:615px;height:344px"
                                id="player2">

                                <!-- specify a splash image inside the container -->
                                <img
                                    src="WotmedExplainedSplash3.jpg"
                                    alt="Wotmed Explained Video" /></a>

                            </span>




                        <br>

                        <p>  <b>Image 1</b>: The Wotmed Advertisement Heatmap</p>
                        <img src="SiteAds/WotmedCustomAdHeatMapV4.png" >

                        <p>  Wotmed advertisement prices vary based on the location on the site you would like your advertisement placed and the size of the advertisement you would like to purchase.</p>
                        <p>  Our advertising fee uses a pricing formula that takes the total number of daily visitors to our site and divides them by 10.</p>
                        <p>  This is the total dollar value you will charged to place an advertisement on our site.  </p>
                        <p>  The total is then multiplied by a factor depending on the location you would like your advertisement placed on the site (see the heatmap above)</p>
                        <BR>
                        <p>  Example: If the total number of visitors to the Wotmed site for the month of April is: 5000</p>
                        <p>  (Total Monthly Visitors / 10 = Wotmed Advertising Fee) eg: 5000 / 10 = 500</p>
                        <p>  The monthly advertising fee based on these visitors is thus: $500</p>
                        <p>  The monthly advertising fee is then multiplied by the heat factor locations to determine the final advertising fee:</p>
                        <BR>
                        <p>  Dark Orange: 2 </p>
                        <p>  Orange: 1.5 </p>
                        <p>  Yellow: 1 </p>
                        <p>  White: 0.5 </p>
                        <BR>
                        <p>  So the total advertising fee for the heatmap locations above if the total number of visitors to the Wotmed site is 500: </p>
                        <BR>
                        <p>  Dark Orange: 500 x 2 = $1,000 per month </p>
                        <p>  Orange: 500 x 1.5 = $750 per month </p>
                        <p>  Yellow: 500 x 1 = $500 per month </p>
                        <p>  White: 500 x 0.5 = $250 per month </p>
                        <BR>

                        <p>  The fee is then varied based on the size of advertisement you would like to place: </p>
<BR>
                        <p>300 x 250: 100% of advertising fee eg: $1,000 per month Dark Orange location, $750 per month Orange location, $500 per month Yellow location, $250 per month White location </p>
                        <img src="SiteAds/Dentist300x250.png" width="300" height="250"><br><br>

                        <p>  728 x 90: 100% of advertising fee eg: $1,000 per month Dark Orange location, $750 per month Orange location, $500 per month Yellow location, $250 per month White location</p>
                        <img src="SiteAds/Dentist728x90.jpg" width="728" height="90"><br><br>

                        <p>  120 x 600: 100% of advertising fee eg: $1,000 per month Dark Orange location, $750 per month Orange location, $500 per month Yellow location, $250 per month White location</p>
                        <img src="SiteAds/120x600.png" width="120" height="600"><br><br>

                        <p>  468 x 60: 55% of advertising fee eg: $550 per month Dark Orange location, $412.50 per month Orange location, $275 per month Yellow location, $137.50 per month White location</p><br>
                        <img src="SiteAds/468x60.jpg" width="468" height="60"><br><br>

                        <p>  120 x 240: 50% of advertising fee eg: $500 per month Dark Orange location, $375 per month Orange location, $250 per month Yellow location, $125 per month White location</p>
                        <img src="SiteAds/120x240.jpg" width="120" height="240"><br><br>

                        <p>  125 x 125: 50% of advertising fee eg: $500 per month Dark Orange location, $375 per month Orange location, $250 per month Yellow location, $125 per month White location</p><br>
                        <img src="SiteAds/125x125.jpg" width="125" height="125"><br><br>

                        <p>  120 x 90: 30% of advertising fee eg: $300 per month Dark Orange location, $225 per month Orange location, $150 per month Yellow location, $75 per month White location </p><br>
                        <img src="SiteAds/120x90.jpg" width="120" height="90"><br><br>

                        <p>  120 x 60: 15% of advertising fee eg: $150 per month Dark Orange location, $112.50 per month Orange location, $75 per month Yellow location, $37.50 per month White location</p><br>
                        <img src="SiteAds/120x60.png" width="120" height="60"><br><br>

                        <p>  88 x 31: 10% of advertising fee eg: $100 per month Dark Orange location, $75 per month Orange location, $50 per month Yellow location, $25 per month White location</p><br>
                        <img src="SiteAds/88x31.gif" width="88" height="31"><br><br>

                        <br>

                        <?php

                        $monthNum = date('m');
                        $monthName = date("F", mktime(0, 0, 0, $monthNum, 10));

                      //  echo "<b>Wotmed has a total of " . $totalVisitorsForMonth . " unique visitors for this month of " . $monthName . '</b>';

                        // remember that its the total for the previous month

                        echo "<BR>" . "<H1>Our advertising package prices are based on the previous months total monthly unique visitors</H1>" . "<BR>";
                        echo "<H2>Wotmed had a total of <b>" . $totalVisitorsForMonth . " </b>unique visitors in the previous month </H2>";



                        echo "<br><br>";
                        echo "<h3>The package prices below are based on this visitor count as at " . date('d/m/Y') . "<br></h3>";


                        ?>


                        <div id="upload-wrapper">
                            <div align="center">
                                <b>Upload your banner advertisement.</b><BR><BR>
                                Note: Each time you click the upload button your advertisement will be uploaded to the Wotmed Team<BR><BR>
                                Please ensure that the advertisement you upload is sized correctly as per the advertising package size you want:<BR><BR>
                                eg: 300 pixels x 250 pixels (300 x 250) or 728 pixels x 90 pixels (728 x 90) etc<BR><BR>
                                <form action="ImageUpload/processupload.php" method="post" enctype="multipart/form-data" id="MyUploadForm">
                                    <BR>
                                    <input name="image_file" id="imageInput" type="file" />
                                    <input type="submit"  id="submit-btn" value="Upload" />
                                    <img src="ImageUpload/images/ajax-loader.gif" id="loading-img" style="display:none;" alt="Please Wait"/>
                                </form>
                                <div id="output"></div>
                            </div>
                        </div>

                        <BR><BR>

                        <p>Please select the advertising package you would like to purchase</p>



                        <form method="post" action="/proposals/proposalsubmit/WotmedAdvertiserSubmission.php" name="requestforProposal" id="requestforProposal">
                            <table width="922" border="0">

                                <input type="hidden" id="itemname" name="itemname" value="Wotmed Advertising" />
                                <input type="hidden" id="itemnumber" name="itemnumber" value="1" />
                                <input type="hidden" id="itemdesc" name="itemdesc" value="Banner Ad" />


                                <input type="hidden" id="itemprice" name="itemprice" value="500" />

                                <input type="hidden" name="itemQty" value="1" />

                                <?php

                                echo "<tr>";
                                echo "<label>";
                                echo "<select name='AdvertisementFee' id='AdvertisementFee' type='text'>";
                                $result2 = $conn2->query("SELECT ID, ADVERTISEMENTSIZEANDPACKAGE, PACKAGEPRICE FROM SITEBASEDADVERTISING");

                                if ($result2->num_rows > 0)
                                {
                                    // output data of each row
                                    while($row = $result2->fetch_assoc())
                                    {
                                        //  echo "Advertisement Size:" . $row["ADVERTISEMENTSIZEANDPACKAGE"] . " - Package Price $" . $row["PACKAGEPRICE"] . "<br>";
                                        $advertisementSize = $row["ADVERTISEMENTSIZEANDPACKAGE"];
                                        $packagePrice = $row["PACKAGEPRICE"];
                                        $combinedString = $advertisementSize . ' ' . '$' . $packagePrice;
                                    //    echo "<option value='".$combinedString."'>".$combinedString."    </option>";

                                        echo "<option value='$combinedString'>$combinedString</option>";

                                        // separate the package name and price

                                        list($packageName, $packagePriceTotal) = explode('$', $combinedString);

//                                      $packagetemp = "<input type='hidden' id='itemprice' name='itemprice' value='$packagePriceTotal' />";


                                   //     echo "<option id='itemprice' name='itemprice' value='$packagePriceTotal'>$packagePriceTotal</option>";



                                    }
                                }
                                $conn2->close();

                                echo "</select>";
                                echo "</label></td>";
                                echo "</tr>";
                                echo "<BR><BR>";

                            //    list($packageName, $packagePriceTotal) = explode('$', $combinedString);
                            //    echo "<input type='hidden' id='itemprice' name='itemprice' value='$packagePriceTotal' />";




                             //   echo $combinedString;
                             //   echo "<BR>";
                             //   echo $packageName;
                             //   echo "<BR>";
                          //      echo $packagePriceTotal;
                           //     echo "<BR>";

                           //     $_SESSION["packagePrice"] = $packagePriceTotal;

                          //      echo("{$_SESSION['packagePrice']}"."<br />");

                                ?>





                                <p> <font color="red">* </font>Mandatory fields are marked with an asterisk<p><br>


                                <tr>
                                    <td width="282"> <p> <font color="red">* </font>Advertiser Name</td>
                                    <td width="508"><label>
                                            <input name="AdvertiserSubmitterName" type="text" id="AdvertiserSubmitterName" size="70" required/>
                                        </label></td>
                                </tr>

                                <tr>
                                    <td>What country are you from?</td>
                                    <td><select name="AdvertiserSubmitterCountry" id="AdvertiserSubmitterCountry" type="text">
                                            <option selected="Please select your country">Please select your country</option>
                                            <option value="Afghanistan">Afghanistan</option>
                                            <option value="Albania">Albania</option>
                                            <option value="Algeria">Algeria</option>
                                            <option value="American Samoa">American Samoa</option>
                                            <option value="Andorra">Andorra</option>
                                            <option value="Angola">Angola</option>
                                            <option value="Anguilla">Anguilla</option>
                                            <option value="Antarctica">Antarctica</option>
                                            <option value="Antigua and Barbuda">Antigua and Barbuda</option>
                                            <option value="Argentina">Argentina</option>
                                            <option value="Armenia">Armenia</option>
                                            <option value="Aruba">Aruba</option>
                                            <option value="Australia">Australia</option>
                                            <option value="Austria">Austria</option>
                                            <option value="Azerbaijan">Azerbaijan</option>
                                            <option value="Bahamas">Bahamas</option>
                                            <option value="Bahamas The">Bahamas The</option>
                                            <option value="Bahrain">Bahrain</option>
                                            <option value="Bangladesh">Bangladesh</option>
                                            <option value="Barbados">Barbados</option>
                                            <option value="Belarus">Belarus</option>
                                            <option value="Belgium">Belgium</option>
                                            <option value="Belize">Belize</option>
                                            <option value="Benin">Benin</option>
                                            <option value="Bermuda">Bermuda</option>
                                            <option value="Bhutan">Bhutan</option>
                                            <option value="Bolivia">Bolivia</option>
                                            <option value="Bosnia">Bosnia</option>
                                            <option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
                                            <option value="Botswana">Botswana</option>
                                            <option value="Brazil">Brazil</option>
                                            <option value="British Virgin Islands">British Virgin Islands</option>
                                            <option value="Brunei">Brunei</option>
                                            <option value="Bulgaria">Bulgaria</option>
                                            <option value="Burkina Faso">Burkina Faso</option>
                                            <option value="Burundi">Burundi</option>
                                            <option value="Cambodia">Cambodia</option>
                                            <option value="Cameroon">Cameroon</option>
                                            <option value="Canada">Canada</option>
                                            <option value="Cape Verde">Cape Verde</option>
                                            <option value="Cayman Islands">Cayman Islands</option>
                                            <option value="Central African Republic">Central African Republic</option>
                                            <option value="Chad">Chad</option>
                                            <option value="Chile">Chile</option>
                                            <option value="China">China</option>
                                            <option value="Christmas Island">Christmas Island</option>
                                            <option value="Colombia">Colombia</option>
                                            <option value="Comoros">Comoros</option>
                                            <option value="Congo">Congo</option>
                                            <option value="Congo Democratic Republic of the">Congo Democratic Republic of the</option>
                                            <option value="Cook Islands">Cook Islands</option>
                                            <option value="Costa Rica">Costa Rica</option>
                                            <option value="Cote d'Ivoire">Cote d'Ivoire</option>
                                            <option value="Croatia">Croatia</option>
                                            <option value="Cuba">Cuba</option>
                                            <option value="Curacao">Curacao</option>
                                            <option value="Cyprus">Cyprus</option>
                                            <option value="Czech Republic">Czech Republic</option>
                                            <option value="Denmark">Denmark</option>
                                            <option value="Djibouti">Djibouti</option>
                                            <option value="Dominica">Dominica</option>
                                            <option value="Dominican Republic">Dominican Republic</option>
                                            <option value="East Timor">East Timor</option>
                                            <option value="Ecuador">Ecuador</option>
                                            <option value="Egypt">Egypt</option>
                                            <option value="El Salvador">El Salvador</option>
                                            <option value="Equatorial Guinea">Equatorial Guinea</option>
                                            <option value="Eritrea">Eritrea</option>
                                            <option value="Estonia">Estonia</option>
                                            <option value="Ethiopia">Ethiopia</option>
                                            <option value="European Union">European Union</option>
                                            <option value="Falkland Islands">Falkland Islands</option>
                                            <option value="Faroe Islands">Faroe Islands</option>
                                            <option value="Federated States of Micronesia">Federated States of Micronesia</option>
                                            <option value="Fiji">Fiji</option>
                                            <option value="Finland">Finland</option>
                                            <option value="France">France</option>
                                            <option value="Gabon">Gabon</option>
                                            <option value="Gambia">Gambia</option>
                                            <option value="Gambia The">Gambia The</option>
                                            <option value="Georgia">Georgia</option>
                                            <option value="Germany">Germany</option>
                                            <option value="Ghana">Ghana</option>
                                            <option value="Gibraltar">Gibraltar</option>
                                            <option value="Greece">Greece</option>
                                            <option value="Greenland">Greenland</option>
                                            <option value="Grenada">Grenada</option>
                                            <option value="Guadeloupe">Guadeloupe</option>
                                            <option value="Guam">Guam</option>
                                            <option value="Guatemala">Guatemala</option>
                                            <option value="Guernsey">Guernsey</option>
                                            <option value="Guinea">Guinea</option>
                                            <option value="Guinea Bissau">Guinea Bissau</option>
                                            <option value="Guyana">Guyana</option>
                                            <option value="Haiti">Haiti</option>
                                            <option value="Herzegovina">Herzegovina</option>
                                            <option value="Honduras">Honduras</option>
                                            <option value="Hong Kong">Hong Kong</option>
                                            <option value="Hungary">Hungary</option>
                                            <option value="Iceland">Iceland</option>
                                            <option value="India">India</option>
                                            <option value="Indonesia">Indonesia</option>
                                            <option value="Iran">Iran</option>
                                            <option value="Iraq">Iraq</option>
                                            <option value="Ireland">Ireland</option>
                                            <option value="Isle Of Man">Isle Of Man</option>
                                            <option value="Israel">Israel</option>
                                            <option value="Italy">Italy</option>
                                            <option value="Jamaica">Jamaica</option>
                                            <option value="Japan">Japan</option>
                                            <option value="Jersey">Jersey</option>
                                            <option value="Jordan">Jordan</option>
                                            <option value="Kazakhstan">Kazakhstan</option>
                                            <option value="Kenya">Kenya</option>
                                            <option value="Kiribati">Kiribati</option>
                                            <option value="KoreaSouth">KoreaSouth</option>
                                            <option value="Kosovo">Kosovo</option>
                                            <option value="Kuwait">Kuwait</option>
                                            <option value="Kyrgyzstan">Kyrgyzstan</option>
                                            <option value="Laos">Laos</option>
                                            <option value="Latvia">Latvia</option>
                                            <option value="Lebanon">Lebanon</option>
                                            <option value="Lesotho">Lesotho</option>
                                            <option value="Liberia">Liberia</option>
                                            <option value="Libya">Libya</option>
                                            <option value="Liechtenstein">Liechtenstein</option>
                                            <option value="Lithuania">Lithuania</option>
                                            <option value="Luxembourg">Luxembourg</option>
                                            <option value="Macau">Macau</option>
                                            <option value="Macedonia">Macedonia</option>
                                            <option value="Macedonia Republic of">Macedonia Republic of</option>
                                            <option value="Madagascar">Madagascar</option>
                                            <option value="Maianmar">Maianmar</option>
                                            <option value="Malawi">Malawi</option>
                                            <option value="Malaysia">Malaysia</option>
                                            <option value="Maldives">Maldives</option>
                                            <option value="Mali">Mali</option>
                                            <option value="Malta">Malta</option>
                                            <option value="Marshall Islands">Marshall Islands</option>
                                            <option value="Mauritania">Mauritania</option>
                                            <option value="Mauritius">Mauritius</option>
                                            <option value="Mexico">Mexico</option>
                                            <option value="Micronesia">Micronesia</option>
                                            <option value="Moldova">Moldova</option>
                                            <option value="Monaco">Monaco</option>
                                            <option value="Mongolia">Mongolia</option>
                                            <option value="Mont Serrat">Mont Serrat</option>
                                            <option value="Montenegro">Montenegro</option>
                                            <option value="Morocco">Morocco</option>
                                            <option value="Mozambique">Mozambique</option>
                                            <option value="Myanmar">Myanmar</option>
                                            <option value="Myanmar (Burma)">Myanmar (Burma)</option>
                                            <option value="Namibia">Namibia</option>
                                            <option value="Nauru">Nauru</option>
                                            <option value="Nepal">Nepal</option>
                                            <option value="Netherlands">Netherlands</option>
                                            <option value="New Caledonia">New Caledonia</option>
                                            <option value="New Zealand">New Zealand</option>
                                            <option value="Nicaragua">Nicaragua</option>
                                            <option value="Niger">Niger</option>
                                            <option value="Nigeria">Nigeria</option>
                                            <option value="Niue">Niue</option>
                                            <option value="Norfolk Island">Norfolk Island</option>
                                            <option value="North Korea">North Korea</option>
                                            <option value="Northern Cyprus">Northern Cyprus</option>
                                            <option value="Northern Mariana Islands">Northern Mariana Islands</option>
                                            <option value="Norway">Norway</option>
                                            <option value="Oman">Oman</option>
                                            <option value="Pakistan">Pakistan</option>
                                            <option value="Palau">Palau</option>
                                            <option value="Palestinian Territories">Palestinian Territories</option>
                                            <option value="Panama">Panama</option>
                                            <option value="Papua New Guinea">Papua New Guinea</option>
                                            <option value="Paraguay">Paraguay</option>
                                            <option value="Peru">Peru</option>
                                            <option value="Philippines">Philippines</option>
                                            <option value="Pitcairn Islands">Pitcairn Islands</option>
                                            <option value="Poland">Poland</option>
                                            <option value="Portugal">Portugal</option>
                                            <option value="Puerto Rico">Puerto Rico</option>
                                            <option value="Qatar">Qatar</option>
                                            <option value="Republic of Macedonia">Republic of Macedonia</option>
                                            <option value="Romania">Romania</option>
                                            <option value="Russia">Russia</option>
                                            <option value="Rwanda">Rwanda</option>
                                            <option value="Saint Helena Ascension Tristan Da Cunha">Saint Helena Ascension Tristan Da Cunha</option>
                                            <option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
                                            <option value="Saint Lucia">Saint Lucia</option>
                                            <option value="Saint Vincent and the Grenadines">Saint Vincent and the Grenadines</option>
                                            <option value="Samoa">Samoa</option>
                                            <option value="San Marino">San Marino</option>
                                            <option value="Sao Tone and Preincipe">Sao Tone and Preincipe</option>
                                            <option value="Saudi Arabia">Saudi Arabia</option>
                                            <option value="Senegal">Senegal</option>
                                            <option value="Serbia">Serbia</option>
                                            <option value="Seychelles">Seychelles</option>
                                            <option value="Sierra Leone">Sierra Leone</option>
                                            <option value="Singapore">Singapore</option>
                                            <option value="Sint Maarten">Sint Maarten</option>
                                            <option value="Slovakia">Slovakia</option>
                                            <option value="Slovenia">Slovenia</option>
                                            <option value="Solomon Islands">Solomon Islands</option>
                                            <option value="Somalia">Somalia</option>
                                            <option value="South Africa">South Africa</option>
                                            <option value="South Korea">South Korea</option>
                                            <option value="Spain">Spain</option>
                                            <option value="Sri Lanka">Sri Lanka</option>
                                            <option value="St Lucia">St Lucia</option>
                                            <option value="St. Helena">St. Helena</option>
                                            <option value="St. Pierre and Miquelon">St. Pierre and Miquelon</option>
                                            <option value="Sudan South Sudan">Sudan South Sudan</option>
                                            <option value="Suriname">Suriname</option>
                                            <option value="Swaziland">Swaziland</option>
                                            <option value="Sweden">Sweden</option>
                                            <option value="Switzerland">Switzerland</option>
                                            <option value="Syria">Syria</option>
                                            <option value="Taiwan">Taiwan</option>
                                            <option value="Tajikistan">Tajikistan</option>
                                            <option value="Tanzania">Tanzania</option>
                                            <option value="Tasmania">Tasmania</option>
                                            <option value="Thailand">Thailand</option>
                                            <option value="Tobago">Tobago</option>
                                            <option value="Togo">Togo</option>
                                            <option value="Tokelau">Tokelau</option>
                                            <option value="Tonga">Tonga</option>
                                            <option value="Trinidad">Trinidad</option>
                                            <option value="Trinidad and Tobago">Trinidad and Tobago</option>
                                            <option value="Tunisia">Tunisia</option>
                                            <option value="Turkey">Turkey</option>
                                            <option value="Turkmenistan">Turkmenistan</option>
                                            <option value="Turks and Caicos Islands">Turks and Caicos Islands</option>
                                            <option value="Tuvalu">Tuvalu</option>
                                            <option value="Tuvalu">Tuvalu</option>
                                            <option value="Uganda">Uganda</option>
                                            <option value="Ukraine">Ukraine</option>
                                            <option value="United Arab Emirates">United Arab Emirates</option>
                                            <option value="United Kingdom">United Kingdom</option>
                                            <option value="United States">United States</option>
                                            <option value="Uruguay">Uruguay</option>
                                            <option value="US Virgin Islands">US Virgin Islands</option>
                                            <option value="Uzbekistan">Uzbekistan</option>
                                            <option value="Vanuatu">Vanuatu</option>
                                            <option value="Vatican City">Vatican City</option>
                                            <option value="Venezuela">Venezuela</option>
                                            <option value="Vietnam">Vietnam</option>
                                            <option value="Virgin Islands">Virgin Islands</option>
                                            <option value="Wallis and Futuna Islands">Wallis and Futuna Islands</option>
                                            <option value="Western Sahara">Western Sahara</option>
                                            <option value="Yemen">Yemen</option>
                                            <option value="Zambia">Zambia</option>
                                            <option value="Zimbabwe">Zimbabwe</option>
                                        </select>
                                        </label></td>
                                </tr>


                                <tr>
                                    <td width="282"> <p> <font color="red">* </font>Advertiser Phone Number</td>
                                    <td width="508"><label>
                                            <input name="AdvertiserSubmitterPhoneNumber" type="text" id="AdvertiserSubmitterPhoneNumber" size="70" required/>
                                        </label></td>
                                </tr>

                                <tr>
                                    <td width="282"> <p> <font color="red">* </font>Email Address</td>
                                    <td width="508"><label>
                                            <input type="email" name="AdvertiserSubmitterEmailAddress" size="40" data-parsley-trigger="change" required />
                                        </label></td>
                                </tr>


                                <tr>
                                    <td>Are you a Practitioner, Surgery Facilitator or a Marketer?</td>

                                    <td><label>
                                            <select name="AdvertiserType" id="AdvertiserType" type="text">
                                                <option value="Practitioner">Practitioner</option>
                                                <option value="Surgery Facilitator">Surgery Facilitator</option>
                                                <option value="Marketer">Marketer</option>
                                            </select>
                                        </label></td>
                                </tr>

                                <tr>
                                    <td width="282"> <p> <font color="red">* </font>Advertisement Title</td>
                                    <td width="508"><label>
                                            <input name="AdvertisementTitle" type="text" id="AdvertisementTitle" size="70" required/>
                                        </label></td>
                                </tr>

                                <tr>
                                    <td width="282"> <p> <font color="red">* </font>Advertisement Subject</td>
                                    <td width="508"><label>
                                            <input name="AdvertisementSubject" type="text" id="AdvertisementSubject" size="40" required/>
                                        </label></td>
                                </tr>


                                <tr>
                                    <td>Where on the site would you like your advertisement to appear?</td>

                                    <td><label>
                                            <select name="AdvertisementPlacement" id="AdvertisementPlacement" type="text">

                                                <option value="  Client Speeches Front Page Top">Client Speeches Front Page Top</option>
                                                <option value="  Client Speeches Front Page Left Side">Client Speeches Front Page Left Side</option>
                                                <option value="  Client Speeches Front Page Right Side">Client Speeches Front Page Right Side</option>
                                                <option value="  Client Speeches Page Bottom">Client Speeches Page Bottom</option>

                                                <option value="  Specific Client Speech Front Page Top">Specific Client Speech Front Page Top</option>
                                                <option value="  Specific Client Speech Front Page Left Side">Specific Client Speech Front Page Left Side</option>
                                                <option value="  Specific Client Speech Front Page Right Side">Specific Client Speech Front Page Right Side</option>
                                                <option value="  Specific Client Speech Page Bottom">Specific Client Speech Page Bottom</option>



                                                <option value="  Landing Page Top">Landing Page Top</option>
                                                <option value="  Landing Page Left Side">Landing Page Left Side</option>
                                                <option value="  Landing Page Right Side">Landing Page Right Side</option>
                                                <option value="  Landing Page Bottom">Landing Page Bottom</option>
                                                <option value="  Patient Sign Up Page Top">Patient Sign Up Page Top</option>
                                                <option value="  Patient Sign Up Page Left Side">Patient Sign Up Page Left Side</option>
                                                <option value="  Patient Sign Up Page Right Side">Patient Sign Up Page Right Side</option>
                                                <option value="  Patient Sign Up Page Bottom">Patient Sign Up Page Bottom</option>
                                                <option value="  Patient Profile Top">Patient Profile Top</option>
                                                <option value="  Patient Profile Left Side">Patient Profile Left Side</option>
                                                <option value="  Patient Profile Right Side">Patient Profile Right Side</option>
                                                <option value="  Patient Profile Bottom">Patient Profile Bottom</option>


                                                <option value="  Patient Request for Proposal Front Page Top">Patient Request for Proposal Front Page Top</option>
                                                <option value="  Patient Request for Proposal Front Page Left Side">Patient Request for Proposal Front Page Left Side</option>
                                                <option value="  Patient Request for Proposal Front Page Right Side">Patient Request for Proposal Front Page Right Side</option>
                                                <option value="  Patient Request for Proposal Page Bottom">Patient Request for Proposal Page Bottom</option>



                                                <option value="  Patient Stories Front Page Top">Patient Stories Front Page Top</option>
                                                <option value="  Patient Stories Front Page Left Side">Patient Stories Front Page Left Side</option>
                                                <option value="  Patient Stories Front Page Right Side">Patient Stories Front Page Right Side</option>
                                                <option value="  Patient Stories Front Page Bottom">Patient Stories Front Page Bottom</option>
                                                <option value="  Specific Patient Story Front Page Top">Specific Patient Story Front Page Top</option>
                                                <option value="  Specific Patient Story Front Page Left Side">Specific Patient Story Front Page Left Side</option>
                                                <option value="  Specific Patient Story Front Page Right Side">Specific Patient Story Front Page Right Side</option>
                                                <option value="  Specific Patient Story Front Page Bottom">Specific Patient Story Front Page Bottom</option>

                                                <option value="  Custom Location on Patient Page (Please call or email us)">Custom Location on Patient Page (Please call or email us)</option>


                                                <option value="  Practitioner Sign Up Page Top">Practitioner Sign Up Page Top</option>
                                                <option value="  Practitioner Sign Up Page Left Side">Practitioner Sign Up Page Left Side</option>
                                                <option value="  Practitioner Sign Up Page Right Side">Practitioner Sign Up Page Right Side</option>
                                                <option value="  Practitioner Sign Up Page Bottom">Practitioner Sign Up Page Bottom</option>
                                                <option value="  Practitioner Profile Top">Practitioner Profile Top</option>
                                                <option value="  Practitioner Profile Left Side">Practitioner Profile Left Side</option>
                                                <option value="  Practitioner Profile Right Side">Practitioner Profile Right Side</option>
                                                <option value="  Practitioner Profile Bottom">Practitioner Profile Bottom</option>
                                                <option value="  Practitioner Search Results Top">Practitioner Search Results Top</option>
                                                <option value="  Practitioner Search Results Left Side">Practitioner Search Results Left Side</option>
                                                <option value="  Practitioner Search Results Right Side">Practitioner Search Results Right Side</option>
                                                <option value="  Practitioner Search Results Bottom">Practitioner Search Results Bottom</option>
                                                <option value="  Practitioner About Video Top">Practitioner About Video Top</option>
                                                <option value="  Practitioner About Video Left Side">Practitioner About Video Left Side</option>
                                                <option value="  Practitioner About Video Right Side">Practitioner About Video Right Side</option>
                                                <option value="  Practitioner About Video Bottom">Practitioner About Video Bottom</option>
                                                <option value="  Practitioner About Audio Top">Practitioner About Audio Top</option>
                                                <option value="  Practitioner About Audio Left Side">Practitioner About Audio Left Side</option>
                                                <option value="  Practitioner About Audio Right Side">Practitioner About Audio Right Side</option>
                                                <option value="  Practitioner About Audio Bottom">Practitioner About Audio Bottom</option>
                                                <option value="  Practitioner Google Map Bottom">Practitioner Google Map Bottom</option>


                                                <option value="  Practitioner Articles Front Page Top">Practitioner Articles Front Page Top</option>
                                                <option value="  Practitioner Articles Front Page Left Side">Practitioner Articles Front Page Left Side</option>
                                                <option value="  Practitioner Articles Front Page Right Side">Practitioner Articles Front Page Right Side</option>
                                                <option value="  Practitioner Articles Front Page Bottom">Practitioner Articles Front Page Bottom</option>
                                                <option value="  Specific Practitioner Article Front Page Top">Specific Practitioner Article Front Page Top</option>
                                                <option value="  Specific Practitioner Article Front Page Left Side">Specific Practitioner Article Front Page Left Side</option>
                                                <option value="  Specific Practitioner Article Front Page Right Side">Specific Practitioner Article Front Page Right Side</option>
                                                <option value="  Specific Practitioner Article Front Page Bottom">Specific Practitioner Article Front Page Bottom</option>

                                                <option value="  Custom Location on Practitioner Page (Please call or email us)">Custom Location on Practitioner Page (Please call or email us)</option>


                                                <option value="  Surgery Facilitator Profile Top">Surgery Facilitator Profile Top</option>
                                                <option value="  Surgery Facilitator Profile Left Side">Surgery Facilitator Profile Left Side</option>
                                                <option value="  Surgery Facilitator Profile Right Side">Surgery Facilitator Profile Right Side</option>
                                                <option value="  Surgery Facilitator Profile Bottom">Surgery Facilitator Profile Bottom</option>
                                                <option value="  Surgery Facilitator Search Results Top">Surgery Facilitator Search Results Top</option>
                                                <option value="  Surgery Facilitator Results Left Side">Surgery Facilitator Results Left Side</option>
                                                <option value="  Surgery Facilitator Results Right Side">Surgery Facilitator Results Right Side</option>
                                                <option value="  Surgery Facilitator Results Bottom">Surgery Facilitator Results Bottom</option>
                                                <option value="  Surgery Facilitator About Video Top">Surgery Facilitator About Video Top</option>
                                                <option value="  Surgery Facilitator About Video Left Side">Surgery Facilitator About Video Left Side</option>
                                                <option value="  Surgery Facilitator About Video Right Side">Surgery Facilitator About Video Right Side</option>
                                                <option value="  Surgery Facilitator About Video Bottom">Surgery Facilitator About Video Bottom</option>
                                                <option value="  Surgery Facilitator About Audio Top">Surgery Facilitator About Audio Top</option>
                                                <option value="  Surgery Facilitator About Audio Left Side">Surgery Facilitator About Audio Left Side</option>
                                                <option value="  Surgery Facilitator About Audio Right Side">Surgery Facilitator About Audio Right Side</option>
                                                <option value="  Surgery Facilitator About Audio Bottom">Surgery Facilitator About Audio Bottom</option>

                                                <option value="  Surgery Facilitator Articles Front Page Top">Surgery Facilitator Articles Front Page Top</option>
                                                <option value="  Surgery Facilitator Articles Front Page Left Side">Surgery Facilitator Articles Front Page Left Side</option>
                                                <option value="  Surgery Facilitator Articles Front Page Right Side">Surgery Facilitator Articles Front Page Right Side</option>
                                                <option value="  Surgery Facilitator Articles Front Page Bottom">Surgery Facilitator Articles Front Page Bottom</option>
                                                <option value="  Specific Surgery Facilitator Article Front Page Top">Specific Surgery Facilitator Article Front Page Top</option>
                                                <option value="  Specific Surgery Facilitator Article Front Page Left Side">Specific Surgery Facilitator Article Front Page Left Side</option>
                                                <option value="  Specific Surgery Facilitator Article Front Page Right Side">Specific Surgery Facilitator Article Front Page Right Side</option>
                                                <option value="  Specific Surgery Facilitator Article Front Page Bottom">Specific Surgery Facilitator Article Front Page Bottom</option>
                                                <option value="  Surgery Facilitator Google Map Bottom">Surgery Facilitator Google Map Bottom</option>

                                                <option value="  Custom Location on Surgery Facilitator Page (Please call or email us)">Custom Location on Surgery Facilitator Page (Please call or email us)</option>


                                            </select>
                                        </label></td>
                                </tr>

                                <tr>
                                    <td width="282"> <p> <font color="red">* </font>Advertisement SEO Keywords (Enter keywords for your advertisement separated by commas) eg: Thailand, Cosmetic Surgery, Rhinoplasty</td>
                                    <td width="508"><label>
                                            <input name="AdvertiserSEOKeywords" type="text" id="AdvertiserSEOKeywords" size="70" required/>
                                        </label></td>
                                </tr>

                                <tr>
                                    <td>Advertisement Text</td>
                                    <td><label>
                                            <textarea name="AdvertisementText" id="AdvertisementText" cols="65" rows="10" required></textarea>
                                        </label></td>
                                </tr>


                                <tr>
                                    <td width="282"> <p> <font color="red">* </font>Practice or Business Name</td>
                                    <td width="508"><label>
                                            <input name="PracticeOrBusinessName" type="text" id="PracticeOrBusinessName" size="70" required/>
                                        </label></td>
                                </tr>

                                <tr>
                                    <td width="282"> <p> <font color="red">* </font>Practice or Business Speciality</td>
                                    <td width="508"><label>
                                            <input name="PracticeOrBusinessSpeciality" type="text" id="PracticeOrBusinessSpeciality" size="70" required/>
                                        </label></td>
                                </tr>

                                <tr>
                                    <td width="282"> <p> <font color="red">* </font>Practice or Business Phone Number</td>
                                    <td width="508"><label>
                                            <input name="PracticeOrBusinessPhone" type="text" id="PracticeOrBusinessPhone" size="70" required/>
                                        </label></td>
                                </tr>

                                <tr>
                                    <td width="282"> <p> <font color="red">* </font>Practice or Business Address</td>
                                    <td width="508"><label>
                                            <input name="PracticeOrBusinessAddress" type="text" id="PracticeOrBusinessAddress" size="70" required/>
                                        </label></td>
                                </tr>

                                <tr>
                                    <td width="282"> <p> <font color="red">* </font>Practice or Business Independent Website Hyperlink</td>
                                    <td width="508"><label>
                                            <input name="PracticeOrBusinessIndependentWebsiteHyperlink" type="text" id="PracticeOrBusinessIndependentWebsiteHyperlink" size="70" required/>
                                        </label></td>
                                </tr>


                                <tr>
                                    <td>Practice or Business Marketing Blurb</td>
                                    <td><input name="PracticeOrBusinessMarketingBlurb" type="text" id="PracticeOrBusinessMarketingBlurb" size="70"></td>
                                    </td>
                                </tr>

                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>


                            </table>


                            <p>
                                <label>
                                    <input type="submit" name="AdvertiserSubmission" id='signupsubmit' value="Buy Advertising" />

                                </label>
                            </p>
                            <BR><BR>

                            <p>
                                WOTMED ADVERTISING TERMS
                            <p>These terms apply to all advertising provided to any person (“Customer“) by Wotmed Pty Ltd ACN 096 283 493 or a subsidiary (“Wotmed“). Customer includes an advertiser on whose behalf Advertising is placed and any media company or agency that arranges the Advertising for its clients.
                            <p>1. Publication of Advertising
                            <p>1.1 Subject to these Terms, Wotmed will use its reasonable endeavours to publish advertising (“Advertising“) in the format and in the position agreed with the Customer. “Advertising” includes images submitted for publication and content or information relating to published Advertisements.
                            <p>1.2 Customer grants Wotmed a worldwide, royalty-free, non-exclusive, irrevocable licence to publish, and to sub-licence the publication of, the Advertising in any form or medium, including print, online or other. Customer warrants that it is authorised to grant Wotmed the licence in this clause 1.
                            <p>2. Right to Refuse Advertising
                            <p>2.1 Neither these Terms nor any written or verbal quotation by Wotmed represents an agreement to publish Advertising. An agreement will only be formed between Wotmed and Customer when Wotmed accepts the Advertising in writing or generates a tax invoice for that Advertising.
                            <p>2.2 Wotmed reserves the right to refuse or withdraw from publication any Advertising at any time without giving reasons (even if the Advertising has previously been published by Wotmed).
                            <p>3. Right to vary Format, Placement or Distribution
                            <p>3.1 Wotmed will use reasonable efforts to publish Advertising in the format and in the position requested by the Customer. However, Wotmed reserves the right to vary the placement of Advertising within a title or website or to change the format of Advertising (including changing colour to black and white).
                            <p>3.2 N/A
                            <p>3.3 Except in accordance with clause 12, Wotmed will not be liable for any loss or damage incurred by a Customer arising from Wotmed”s failure to publish Advertising in accordance with a Customer”s request.
                            <p>3.4 If Wotmed changes the configuration for a publication, Wotmed reserves the right to shrink or enlarge the Advertising by up to 10% without notice to Customer or any change to rates.
                            <p>4. Submission of Advertising
                            <p>4.1 Customer warrants to Wotmed that the publication of the Advertising does not breach or infringe:
                            <p>(a) the Competition and Consumer Act (Cth) or equivalent State legislation of Victoria, AUSTRALIA;
                            <p>(b) any copyright, trade mark, obligation of confidentiality or other personal or proprietary right;
                            <p>(c) any law of defamation, obscenity or contempt of any court, tribunal or royal commission;
                            <p>(d) State or Commonwealth privacy legislation or anti-discrimination legislation;
                            <p>(e) any financial services law as defined in the Corporations Act 2001 (Cth); or
                            <p>(f) any other law or applicable code (including any common law, statute, delegated legislation, rule or ordinance of the Commonwealth, or a State or Territory).
                            <p>4.2 Customer warrants that if Advertising contains the name or photographic or pictorial representation of
                            <p>any living person and/or any copy by which any living person can be identified, the Customer has obtained the authority of that person to make use of his/her name or representation or the copy.
                            <p>4.3 Advertising containing contact details for the Customer must contain the full name and street address of the Customer. Post office box and email addresses alone are insufficient.
                            <p>4.4 If a Customer submits Advertising that looks, in Wotmed”s opinion, like editorial material, Wotmed may publish the Advertising under the heading “Advertising” with a border distinguishing it from editorial.
                            <p>4.5 Wotmed will not be responsible for any loss or damage to any Advertising material left in its control.
                            <p>4.6 Advertising submitted electronically must comply with Wotmed”s specifications. Wotmed may reject the Advertising material if it is not submitted in accordance with such specifications.
                            <p>4.7 Advertising material delivered digitally must include the advertisers business or practice name.
                            <p>4.8 If Customer is a corporation and the Advertising contains the price for consumer goods or services, Customer warrants that the Advertising complies with the component pricing provisions of the Competition and Consumer Act (Cth) and contains, as a single price, the minimum total price to the extent quantifiable at time of the Advertising.
                            <p>4.9 Customer must not resell Advertising space to any third party without Wotmed”s consent.
                            <p>4.10 If Advertising promotes a competition or trade promotion, Customer warrants it has obtained all relevant permits and indemnifies Wotmed against any loss in connection with the Advertising.
                            <p>5. N/A
                            <p>5.1 N/A
                            <p>6. Online Advertising
                            <p>6.1 For online banner and display Advertising, Customer must submit creative materials and a clickthrough URL to Wotmed at least 3 working days (5 working days for non-gif material) or within such other deadline advised by Wotmed at its discretion before publication date. Wotmed may charge Customer for online Advertising cancelled on less than 30 days notice or if creative materials are not submitted in accordance with this clause 6.1.
                            <p>6.2 All online Advertising (including rich media) must comply with Wotmed”s advertising specifications.
                            <p>6.3 Wotmed will measure online display and banner Advertising (including impressions delivered and clicks achieved) through its ad-serving systems. Results from Customer or third party ad-servers will not be accepted for the purposes of Wotmed”s billing and assessment of Advertising.
                            <p>6.4 Wotmed is not liable for loss or damage from an internet or telecommunications failure.
                            <p>6.5 Customer acknowledges that Wotmed may at its discretion include additional features or inclusions such as third party advertisements within online classified Advertising.
                            <p>7. Errors
                            <p>7.1 Customer must promptly check proofs of Advertising (if provided to the Customer by Wotmed) and notify Wotmed of any errors in the proofs or in published Advertising.
                            <p>7.2 Wotmed does not accept responsibility for any errors submitted by the Customer or its agent, including errors in Advertising placed over the telephone.
                            <p>7.3 Customer must send any claim for credit or republication in writing to Wotmed no later than 7 days after the date of publication of the Advertising.
                            <p>8. Advertising Rates and GST
                            <p>8.1 The Customer must pay for Advertising, unless otherwise agreed, at the casual ratecard rate. Ratecard rates may be varied at any time by Wotmed without notice. Customer must pay GST at the time it pays for Advertising (GST is included in the advertised price). Wotmed will provide a tax invoice or adjustment note (as applicable) via PayPal.
                            <p>8.2 Eligibility for discounts or rebates will be based on the Customer”s GST-exclusive advertising spend.
                            <p>9. Credit and Customer Accounts
                            <p>9.1 Wotmed may grant, deny or withdraw credit to a Customer at any time in its discretion. Customer must ensure that its Customer account number is available only to those employees authorised to use it. Customer acknowledges it will be liable for all Advertising placed under Customer”s business name.
                            <p>10. Payment
                            <p>10.1 The Customer must pre-pay for Advertising via PayPal. If Advertising is on account, payment must be within 7 days of date of the invoice. If a commercial account has been established with Wotmed, payment must be within 30 days of invoice date.
                            <p>10.2 If Customer fails to provide the copy for a booking by publication deadline, Customer will be charged unless a cancellation is approved by Wotmed. If Wotmed accepts Advertising after the deadline, it will be deemed out of specification. Customer has no claim against Wotmed for credit, republication or otherremedy for out of specification Advertising.
                            <p>10.3 Customer must pay the full price for Advertising even if Wotmed varied the format or placement of the Advertising or if there is an error in the Advertising, unless the error was Wotmed”s fault. Customer must pay its electronic transmission costs.
                            <p>11. Failure to Pay and Other Breach
                            <p>11.1 If Customer breaches these terms, fails to pay for Advertising or suffers an Insolvency Event (defined in clause 11.2), Wotmed may (in its discretion and without limitation):
                            <p>(a) cancel any provision of credit to Customer;
                            <p>(b) require cash pre-payment for further Advertising;
                            <p>(c) charge interest on all overdue amounts at the rate 2% above the NAB Overdraft Base Rate;
                            <p>(d) take proceedings against the Customer for any outstanding amounts;
                            <p>(e) recover Wotmed”s costs including mercantile agency and legal costs on a full indemnity basis;
                            <p>(f) cease publication of further Advertising or terminate an agreement for Advertising not published;
                            <p>(g) exercise any other rights at law.
                            <p>11.2 A Customer suffers an “Insolvency Event” if:
                            <p>(a) Customer is a natural person and commits an act of bankruptcy; or
                            <p>(b) Customer is a body corporate and cannot pay its debts as and when they fall due or enters an arrangement with its creditors other than in the ordinary course of business or passes a resolution for administration, winding up or liquidation (other than for the purposes of reorganisation or reconstruction); or has a receiver, manager, liquidator or administrator appointed to any of its property or assets or has a petition presented for its winding up.
                            <p>11.3 Wotmed may withhold any discounts or rebates if Customer fails to comply with its payment obligations.
                            <p>11.4 A written statement of debt signed by an authorised employee of Wotmed is evidence of the amount owed by the Customer to Wotmed.
                            <p>12. Liability
                            <p>12.1 The Customer acknowledges that it has not relied on any advice given or representation made by or on behalf of Wotmed in connection with the Advertising.
                            <p>12.2 Wotmed excludes all implied conditions and warranties from these terms, except any condition or warranty (such as conditions and warranties implied by the Competition and Consumer Act and equivalent State acts) which cannot by law be excluded (“Non-excludable Condition“).
                            <p>12.3 Wotmed limits its liability for breach of any Non-Excludable Condition (to the extent such liability can be limited) and for any other error in published Advertising caused by Wotmed to the re-supply of the Advertising or payment of the cost of re-supply (at Wotmed”s option).
                            <p>12.4 Subject to clauses 12.2 and 12.3, Wotmed excludes all other liability to the Customer for any costs, expenses, losses and damages incurred in relation to Advertising published by Wotmed, whether that liability arises in contract, tort (including by Wotmed”s negligence) or under statute. Without limitation, Wotmed will in no circumstances be liable for any indirect or consequential losses, loss of profits, loss of revenue or loss of business opportunity.
                            <p>12.5 The Customer indemnifies Wotmed and its officers, employees, contractors and agents (the “Indemnified“) against any costs, expenses, losses, damages and liability suffered or incurred by the Indemnified arising from the Customer”s breach of these Terms and any negligent or unlawful act or omission of the Customer in connection with the Advertising.
                            <p>13. Privacy
                            <p>13.1 Wotmed collects a Customer’s personal information to provide the Advertising to the Customer and for invoicing purposes. Wotmed may disclose this personal information to its related bodies corporate, to credit reporting agencies and other third parties as part of provision of the Advertising and for overdue accounts, to debt collection agencies to recover amounts owing.
                            <p>13.2 Wotmed provides some published Advertising to third party service providers. Where such Advertising contains personal information, Customer consents to the disclosure of their personal information in the advertising to third parties and to the personal information being republished by a third party.
                            <p>13.3 Customers may gain access to their personal information by contacting the Privacy Officer (Mr Clarke Towson Wotmed Chief Technology Officer). Wotmed’s privacy policy is at www.wotmed.com.
                            <p>14. Confidentiality
                            <p>14.1 Each party will treat as confidential, and will procure that its advertising agents, other agents, and contractors (“Agents“) treat as confidential and will not disclose, unless disclosure is required by law:
                            <p>(a) the terms of this Agreement (including terms relating to volumes and pricing);
                            <p>(b) information generated for the performance of this Agreement, including all data relating to advertising schedules, budgets, forecasts, booked advertising, prices or volumes;
                            <p>(c) any other information that ought in good faith to be treated as confidential given the circumstances of disclosure or the nature of the information;
                            <p>(d) any information derived wholly or partly for any information referred to in (a) to
                            <p>(c) above;
                            <p>Each party agrees to take all reasonable precautions to prevent any unauthorised use, disclosure, publication or dissemination of the confidential information by or on behalf of itself or any third party.
                            <p>15. General
                            <p>15.1 These Terms, with any other written agreement, represent the entire agreement of the Customer and Wotmed for Advertising. They can only be varied in writing by an authorised officer of Wotmed. No purchase order or other document issued by the Customer will vary these Terms.
                            <p>15.2 Wotmed will not be liable for any delay or failure to publish Advertising caused by a factor outside Wotmed”s reasonable control (including but not limited to any act of God, war, breakdown of plant, industrial dispute, electricity failure, governmental or legal restraint).
                            <p>15.3 Wotmed may serve notice on Customer by post or fax to the last known address of the Customer.
                            <p>15.4 These Terms are governed by the laws of the State in which the billing company for the Advertising is located and each party submits to the non-exclusive jurisdiction of that State.


                            </p>


                            <p>&nbsp;</p>
                        </form>

                    </div>



            <p>&nbsp;</p>



        </div>


			<p>&nbsp;</p>
			<p>&nbsp;</p>

            <div class="fSma fcg">

                            <span>

                                 Wotmed Platform Status: <?php

                                $myfile = fopen("WotmedPlatformStatus.txt", "r") or die("Undetermined");
                                echo fread($myfile,filesize("WotmedPlatformStatus.txt"));
                                fclose($myfile);

                                ?>

                            </span>
                <br>

                <span>Copyright &copy; Wotmed.com <?php	echo date('Y'); ?> All rights reserved.  </span>




            </div>

            <!-- this will install flowplayer inside previous A- tag. -->
            <script>
                flowplayer("player2", "../flowplayer.commercial-3.2.12.swf", {
                    // commercial version license key
                    key: '#$1e327ef0a3f199461a1',



                    clip: {
                        // these two configuration variables does the trick

                        autoPlay: false,
                        accelerated: false,
                        autoBuffering: true // <- do not place a comma here


                    }
                    ,


                    plugins:  {
                        controls: {

                            // tooltips configuration
                            tooltips: {

                                // enable english tooltips on all buttons
                                buttons: true,

                                // customized texts for buttons
                                play: 'Play',
                                pause: 'Pause',
                                fullscreen: 'Full Screen'
                            },

                            // background color for all tooltips
                            tooltipColor: '#195297',

                            // text color
                            tooltipTextColor: '#FFFFFF'
                        }
                    }
                    ,



                    // now we can tweak the menu
                    contextMenu: [
                        // 1. load related videos with jQuery and AJAX into the HTML DIV
                        {
                            'Show related videos' : function() {
                                $("#related").load("/demos/configuration/related.html");
                            }
                        },

                        // 2. display custom player information
                        "wotmed.com video player 1.1"
                    ],


                });
            </script>
