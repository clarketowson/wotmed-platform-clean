<?php
	session_start();
	include "../includes/connect.php";
	include "../includes/functions.php";
	$path="../";
	if(isset($_GET['id'])){
		$row=getPractitionerDetail($conn,$_GET['id']);
		$id=$_GET['id'];
	}
	else{
		$row=getPractitionerDetail($conn,$_SESSION['id']);
		$id=$_SESSION['id'];
	}
	$rowSession=getParticipantDetail($conn,$_SESSION['id']);
	if($rowSession['PROFILEPHOTO']==""){
		$ppFileNameSession="blankSilhouetteMale.png";
	}else{
		$ppFileNameSession=$rowSession['PROFILEPHOTO'];
	}
	
	$headerDB=array("RESUME_NUMBER","PARTICIPANT_NUMBER","TITLEOBJECTVE","TITLEOBJECTIVESLOGAN","SPECIFICAREASOFEXPERTISE","CLINICALAFFILIATIONS","PE_COMPANYNAME1","PE_COMPANYADDRESS1","PE_YEARFROM1","PE_YEARTO1","PE_ROLEPOSITIONHELD1","PE_KEYACCOMPLISHMENTS1","PE_COMPANYNAME2","PE_COMPANYADDRESS2","PE_YEARFROM2","PE_YEARTO2","PE_ROLEPOSITIONHELD2","PE_KEYACCOMPLISHMENTS2","PE_COMPANYNAME3","PE_COMPANYADDRESS3","PE_YEARFROM3","PE_YEARTO3","PE_ROLEPOSITIONHELD3","PE_KEYACCOMPLISHMENTS3","PE_COMPANYNAME4","PE_COMPANYADDRESS4","PE_YEARFROM4","PE_YEARTO4","PE_ROLEPOSITIONHELD4","PE_KEYACCOMPLISHMENTS4","PE_COMPANYNAME5","PE_COMPANYADDRESS5","PE_YEARFROM5","PE_YEARTO5","PE_ROLEPOSITIONHELD5","PE_KEYACCOMPLISHMENTS5","ED_UNIVERSITYNAME1","ED_UNIVERSITYADDRESS1","ED_QUALIFICATIONEARNED1","ED_YEARFROM1","ED_YEARTO1","ED_UNIVERSITYNAME2","ED_UNIVERSITYADDRESS2","ED_QUALIFICATIONEARNED2","ED_YEARFROM2","ED_YEARTO2","ED_UNIVERSITYNAME3","ED_UNIVERSITYADDRESS3","ED_QUALIFICATIONEARNED3","ED_YEARFROM3","ED_YEARTO3","ED_UNIVERSITYNAME4","ED_UNIVERSITYADDRESS4","ED_QUALIFICATIONEARNED4","ED_YEARFROM4","ED_YEARTO4","ED_UNIVERSITYNAME5","ED_UNIVERSITYADDRESS5","ED_QUALIFICATIONEARNED5","ED_YEARFROM5","ED_YEARTO5","RI_RESEARCHINTERESTNAME1","RI_RESEARCHINTERESTDETAILS1","RI_RESEARCHINTERESTNAME2","RI_RESEARCHINTERESTDETAILS2","RI_RESEARCHINTERESTNAME3","RI_RESEARCHINTERESTDETAILS3","RI_RESEARCHINTERESTNAME4","RI_RESEARCHINTERESTDETAILS4","RI_RESEARCHINTERESTNAME5","RI_RESEARCHINTERESTDETAILS5","AW_AWARDNAME1","AW_AWARDYEAR1","AW_AWARDDETAILS1","AW_AWARDNAME2","AW_AWARDYEAR2","AW_AWARDDETAILS2","AW_AWARDNAME3","AW_AWARDYEAR3","AW_AWARDDETAILS3","AW_AWARDNAME4","AW_AWARDYEAR4","AW_AWARDDETAILS4","AW_AWARDNAME5","AW_AWARDYEAR5","AW_AWARDDETAILS5","STRENGTH1","STRENGTH2","STRENGTH3","SKILLSGIFTS","PASSIONS");
	$headerPersonalDetail=array("","PRACTITIONER_BUSINESSNAME","PRACTITIONER_BUSINESSNUMBER","EMAILADDRESS","PRACTITIONER_BUSINESSADDRESS","PRACTITIONER_BUSINESSSUBURB","PRACTITIONER_BUSINESSPOSTCODE","PRACTITIONER_BUSINESSSTATE","COUNTRYNAME", "", "", "QUALIFICATIONS", "EDUCATION", "BOARDCERTIFICATIONS", "PRACTITIONER_PROFESSIONALMEMBERSHIP", "HOSPITALAFFILIATION", "C_TRAINED", "C_DEGREE");
	$header1=array("Title", "Business Name", "Business Number", "Email Address", "Business Address", "Suburb", "Post Code", "State", "Country", "Independent Website", "Speciality", "Qualification", "Education", "Board Certifications", "Professional Membership", "Hospital Affiliation", "Country Trained", "Country Degree Obtained");
	$header2=array("Title/Objective (50 Characters Max)", "Title/Objective Slogan (Unlimited Characters)", "Specific Areas of Expertise (Unlimited Characters)", "Clinical Affiliations (Unlimited Characters)");
	$header3=array("Company Name  (120 Characters Max)", "Company Address  (255 Characters Max)", "Year From  (80 Characters Max)", "Year To  (80 Characters Max)", "Role/Position Held  (120 Characters Max)", "Key Accomplishments (Unlimited Characters)");
	$header4=array("University Name  (100 Characters Max)", "University Address  (255 Characters Max)", "Qualification Earned  (80 Characters Max)", "Year From", "Year To");
	$header5=array("Research Interest Name  (80 Characters Max)","Research Interest Details  (80 Characters Max)");
	$header6=array("Award Name  (80 Characters Max)","Award Year  (80 Characters Max)","Award Details  (80 Characters Max)");

	//language list
	$langs = array("Afrikaans", "Albanian", "Arabic", "Azerbaijani", "Basque", "Bengali", "Belarusian", "Bulgarian", "Catalan", "Chinese Simplified", "Chinese Traditional", "Croatian", "Czech", "Danish", "Dutch", "English", "Esperanto", "Estonian", "Filipino", "Finnish", "French", "Galician", "Georgian", "German", "Greek", "Gujarati", "Haitian Creole", "Hebrew", "Hindi", "Hungarian", "Icelandic", "Indonesian", "Irish", "Italian", "Japanese", "Kannada", "Korean", "Latin", "Latvian", "Lithuanian", "Macedonian", "Malay", "Maltese", "Norwegian", "Persian", "Polish", "Portuguese", "Romanian", "Russian", "Serbian", "Slovak", "Slovenian", "Spanish", "Swahili", "Swedish", "Tamil", "Telugu", "Thai", "Turkish", "Ukrainian", "Urdu", "Vietnamese", "Welsh", "Yiddish");

	//google language target
	$glangs = array("af", "sq", "ar", "az", "eu", "bn", "be", "bg", "ca", "zh-CN", "zh-TW", "hr", "cs", "da", "nl", "en", "eo", "et", "tl", "fi", "fr", "gl", "ka", "de", "el", "gu", "ht", "iw", "hi", "hu", "is", "id", "ga", "it", "ja", "kn", "ko", "la", "lv", "lt", "mk", "ms", "mt", "no", "fa", "pl", "pt", "ro", "ru", "sr", "sk", "sl", "es", "sw", "sv", "ta", "te", "th", "tr", "uk", "ur", "vi", "cy", "yi");

	$query="SELECT * FROM RESUME WHERE PARTICIPANT_NUMBER = $id";
	//echo $query."<BR>";
	$result=mysqli_query($conn,$query);
	
	
	
	
	$available=false;
	if(mysqli_num_rows($result)!=0){
		$details=mysqli_fetch_array($result);
		$available=true;
	}
	
	if(isset($_POST['TITLEOBJECTVE'])){
		if(!$available){
			$query="INSERT INTO RESUME (PARTICIPANT_NUMBER,";
			for($i=2;$i<count($headerDB)-1;$i++){
				$query.=$headerDB[$i].",";
			}
			$query.=$headerDB[count($headerDB)-1].") VALUES ($id,'";
			for($i=2;$i<count($headerDB)-1;$i++){
				$query.=$_POST[$headerDB[$i]]."','";
			}
			$query.=$_POST[$headerDB[count($headerDB)-1]]."')";
			//echo $query."<BR>";
			@mysqli_query($conn,$query);
			
			
			
		}else{
			$query="UPDATE RESUME SET ";
			for($i=2;$i<count($headerDB)-1;$i++){
				$query.=$headerDB[$i]."='".$_POST[$headerDB[$i]]."',";
			}
			$query.=$headerDB[count($headerDB)-1]."='".$_POST[$headerDB[count($headerDB)-1]]."' WHERE PARTICIPANT_NUMBER=".$id;
			//echo $query."<BR>";
			@mysqli_query($conn,$query);
			
	
			// display a message box indicating if the resume was updated correctly or not
			
			
			if($result)
	{
	print '<script type="text/javascript">';
	print 'alert("Your Resume has been updated successfully")';
	print '</script>'; 
	}
	else
	{
	print '<script type="text/javascript">';
	print 'alert("Error: Your Resume was not updated correctly")';
	print '</script>'; 

	}
			
		}
		echo "<script type='text/javascript'>
				window.location = 'resume.php'
		</script>";
	}
?>
<html>
<head>
<link href="<?php echo $path; ?>style/p_style.css" rel="stylesheet"></link>




   
   
<script type="text/javascript" src="<?php echo $path; ?>classes/jquery.js"></script>
<script type="text/javascript">
	function resumeDownload(){
		//alert("BCDownload.php?target="+$('#langs').val());
		window.open("../resume_download.php?target="+$('#langs').val());
	}
</script>
</head>
<body>
<?php include $path."includes/p_header.php"; ?>
<div id="google_translate_element" style='float:right;padding-top:10;'></div><BR>
<script type="text/javascript">
	function googleTranslateElementInit() {
	  new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
	}
</script>
<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
<h3>Wotmed Custom Resume</h3>
<?php if(!isset($_GET['id'])){ ?>
<p>Your Wotmed Custom Resume is a very important tool to help us at Wotmed connect you with your peers and patients globally.<BR><BR>
The Wotmed Custom Resume is a structured breakdown of the information contained within your resume which is usually in Microsoft Word format. By copying the information from your Microsoft Word resume and pasting it into the fields below you make it really easy for the Wotmed team to create very sophisticated database queries which are used to help connect you with your peers and patients seeking your skills and expertise globally. Wotmed will suggest which Practitioners you have a mutual self interest in connecting with. These suggestions appear on both your profiles and it is then up to both of you to take the next step and introduce yourselves to one another. We will also provide suggestions on what Patients have a strong self interest in connecting with you in order to receive treatment. These suggestions appear on both your profiles and it is then up to both of you to take the next step and introduce yourselves to one another.<BR><BR>
Wotmed will additionally use the information that you provide here to determine the global supply for and demand of your skills, experience, qualifications - everything that makes up your "human capital". This will help us to answer important questions that we will provide answers to you about such as:<BR><BR>
What country are my skills most in demand?<BR><BR>
What country are my skills in very low supply?<BR><BR>
What people globally can most benefit from my skills, knowledge, experience and qualifications - my human capital?<BR><BR>
What patients around the world can I help the most?<BR><BR>
What people globally can most benefit from my intellectual, social, experiential, talent, creatve and passion capital?<BR><BR>
What countries pay the highest salaries for my human capital? Where can I earn the highest wage?<BR><BR>
As a Doctor you are a person of influence amongst the general population. We want to help you to understand the scarcity of what you have to offer so that you can seek to serve what the people that you connect with around the world need the most.<BR><BR>
Note that Wotmed uses the Google Translator to translate your resume into different languages to help make it easier for Practitioners from countries where English is a secondary language.<BR><BR>
<select id='langs'>
		<?php
			foreach($langs as $key => $val){
				if($val == "English")
					echo "<option value='{$glangs[$key]}' selected='selected'>$val</option>";
				else
					echo "<option value='{$glangs[$key]}'>$val</option>";
			}
		?>
		</select>
		<a href="#" onClick="resumeDownload()" return false>Download PDF File Here</a><br>
<!--<a href="../resume_download.php">Download PDF File Here</a><BR><BR>-->
<b>Please do create your Wotmed Custom Resume. You will be really glad you did!</b></p><BR>
<?php } ?>
<form action="" method="post">
<?php
	if($available){
?>
	<table>
		<?php
			$c=2;
			for($i=0;$i<count($header1);$i++){
				if($i>0){
					echo "
					<tr>
						<td>{$header1[$i]} :</td>";
						if(!isset($_GET['id']))
							if($header1[$i] == "Independent Website"){
								$webLink="";
								$temp=getDetails($conn,$row['PRACTITIONER_NUMBER'],10);
								if($temp!=null){
									$detailResult= mysqli_fetch_array($temp);
									$webLink.=$detailResult['DETAIL_TITLE'];
								}
								echo "<td>$webLink</td>";

							}elseif($header1[$i] == "Speciality"){
								$specName="";
								$query="SELECT SPECIALITY FROM SPECIALITY WHERE SPECIALITY_NUMBER ='" . $row['SPECIALITY_NUMBER'] . "'";
								$speciality=mysqli_query($conn,$query);
								if(mysqli_num_rows($speciality)!=0){
									$spec=mysqli_fetch_array($speciality);
									$specName = $spec[0];
								}
								echo "<td>$specName</td>";
							}
							else
								echo "<td>{$row[$headerPersonalDetail[$i]]}</td>";
						else{
							if($header1[$i] == "Independent Website"){
								$webLink="";
								$temp=getDetails($conn,$row['PRACTITIONER_NUMBER'],10);
								if($temp!=null){
									$detailResult= mysqli_fetch_array($temp);
									$webLink.=$detailResult['DETAIL_TITLE'];
								}
								echo "<td>$webLink</td>";
							}elseif($header1[$i] == "Speciality"){
								$specName="";
								$query="SELECT SPECIALITY FROM SPECIALITY WHERE SPECIALITY_NUMBER ='" . $row['SPECIALITY_NUMBER'] . "'";
								$speciality=mysqli_query($conn,$query);
								if(mysqli_num_rows($speciality)!=0){
									$spec=mysqli_fetch_array($speciality);
									$specName = $spec[0];
								}
								echo "<td>$specName</td>";
							}
							else
								echo "<td>{$row[$headerPersonalDetail[$i]]}</td>";

						}
					echo "</tr>";
				}
				else{
					echo "
					<tr>
						<td>{$header1[$i]} :</td>";
						if(!isset($_GET['id']))
							echo "<td>&nbsp;</td>";
						else
							echo "<td>&nbsp</td>";
					echo "</tr>";
				}
			}
		?>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2"><b>About you</b></td>
		</tr>
		<?php
			for($i=0;$i<count($header2);$i++){
					echo "
					<tr>
						<td>{$header2[$i]} :</td>";
						if(!isset($_GET['id']))
							echo "<td><textarea id='{$headerDB[$c]}' name='{$headerDB[$c]}'>$details[$c]</textarea></td>";
						else
							echo "<td>{$details[$c]}</td>";
					echo "</tr>";
				$c++;
			}
		?>
		<?php
			for($i=0;$i<5;$i++){
		?>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2"><b>Professional Experience #<?php echo $i+1; ?></b></td>
		</tr>
		<?php 
			
				for($j=0;$j<count($header3);$j++){
					if($j==count($header3)-1){
						echo "
						<tr>
							<td>{$header3[$j]} :</td>";
						if(!isset($_GET['id']))
							echo "<td><textarea id='{$headerDB[$c]}' name='{$headerDB[$c]}'>$details[$c]</textarea></td>";
						else
							echo "<td>{$details[$c]}</td>";
						echo "</tr>";
					}
					else{
						echo "
						<tr>
							<td>{$header3[$j]} :</td>";
						if(!isset($_GET['id']))
							echo "<td><input type='input' id='{$headerDB[$c]}' name='{$headerDB[$c]}' value='$details[$c]'></td>";
						else
							echo "<td>{$details[$c]}</td>";
						echo "</tr>";
					}
				$c++;
				}
			} 
		?>
		<?php
			for($i=0;$i<5;$i++){
		?>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2"><b>Education #<?php echo $i+1; ?></b></td>
		</tr>
		<?php 
			
				for($j=0;$j<count($header4);$j++){
					echo "
					<tr>
						<td>{$header4[$j]} :</td>";
						if(!isset($_GET['id']))
							echo "<td><input type='input' id='{$headerDB[$c]}' name='{$headerDB[$c]}' value='$details[$c]'></td>";
						else
							echo "<td>{$details[$c]}</td>";
						echo "</tr>";
					$c++;
				}
			} 
		?>
		<?php
			for($i=0;$i<5;$i++){
		?>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2"><b>Research Interests #<?php echo $i+1; ?></b></td>
		</tr>
		<?php 
			
				for($j=0;$j<count($header5);$j++){
					if($j==count($header5)-1){
						echo "
						<tr>
							<td>{$header5[$j]} :</td>";
						if(!isset($_GET['id']))
							echo "<td><textarea id='{$headerDB[$c]}' name='{$headerDB[$c]}'>$details[$c]</textarea></td>";
						else
							echo "<td>{$details[$c]}</td>";
						echo "</tr>";
					}
					else{
						echo "
						<tr>
							<td>{$header5[$j]} :</td>";
						if(!isset($_GET['id']))
							echo "<td><input type='input' id='{$headerDB[$c]}' name='{$headerDB[$c]}' value='$details[$c]'></td>";
						else
							echo "<td>{$details[$c]}</td>";
						echo "</tr>";
					}
				$c++;
				}
			} 
		?> 
		<?php
			for($i=0;$i<5;$i++){
		?>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2"><b>Awards #<?php echo $i+1; ?></b></td>
		</tr>
		<?php 
			
				for($j=0;$j<count($header6);$j++){
					if($j==count($header6)-1){
						echo "
						<tr>
							<td>{$header6[$j]} :</td>";
						if(!isset($_GET['id']))
							echo "<td><textarea id='{$headerDB[$c]}' name='{$headerDB[$c]}'>$details[$c]</textarea></td>";
						else
							echo "<td>{$details[$c]}</td>";
						echo "</tr>";
					}
					else{
						echo "
						<tr>
							<td>{$header6[$j]} :</td>";
						if(!isset($_GET['id']))
							echo "<td><input type='input' id='{$headerDB[$c]}' name='{$headerDB[$c]}' value='$details[$c]'></td>";
						else
							echo "<td>{$details[$c]}</td>";
						echo "</tr>";
					}
					
				$c++;
				}
			} 
		?>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2"><b>Your Top Three Strengths</b></td>
		</tr>
		<?php 		
			for($j=0;$j<3;$j++){
				$i=$j+1;
				echo "
				<tr>
					<td>Strength #$i :</td>";
					if(!isset($_GET['id']))
						echo "<td><input type='input' id='{$headerDB[$c]}' name='{$headerDB[$c]}' value='$details[$c]'></td>";
					else
						echo "<td>{$details[$c]}</td>";
					echo "</tr>";
				$c++;
			}
		?>
		
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td style="max-width:50px;"><b>What professional skills/gifts do you have that are particularly valuable to your patients?	</b></td>
			<?php if(!isset($_GET['id'])) { ?>
			<td><textarea id='SKILLSGIFTS' name='SKILLSGIFTS'><?php echo $details[89]; ?></textarea></td>
			<?php }
				else{
			?>
				<td><?php echo $details[89]; ?></td>
			<?php } ?>
		</tr>
		
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td style="max-width:50px;"><b>What are your greatest passions in life and in your profession?</b></td>
			<?php if(!isset($_GET['id'])) { ?>
			<td><textarea id='PASSIONS' name='PASSIONS'><?php echo $details[90]; ?></textarea></td>
			<?php }
				else{
			?>
				<td><?php echo $details[90]; ?></td>
			<?php } ?>
		</tr>
		<?php if(!isset($_GET['id'])) { ?>
		<tr>

        <link href="ResumeStyle.css" rel="stylesheet"></link>

			<td colspan="2"><input type="submit" value="Create Wotmed Resume" id='buttonsubmit'></td>
		</tr> 	 
		<?php } ?>
	</table>
<?php
	}
	else{
?>
	<table>
		<?php
			$c=2;
			for($i=0;$i<count($header1);$i++){
				if($i>0){
					echo "
					<tr>
						<td>{$header1[$i]} :</td>";
						if(!isset($_GET['id']))
							echo "<td>{$row[$headerPersonalDetail[$i]]}</td>";
						else
							echo "<td>{$row[$headerPersonalDetail[$i]]}</td>";
					echo "</tr>";
				}
				else{
					echo "
					<tr>
						<td>{$header1[$i]} :</td>";
						if(!isset($_GET['id']))
							echo "<td>&nbsp;</td>";
						else
							echo "<td>&nbsp</td>";
					echo "</tr>";
				}
			}
		?>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2"><b>About you</b></td>
		</tr>
		<?php
			for($i=0;$i<count($header2);$i++){
				if($i!=0){
					echo "
					<tr>
						<td>{$header2[$i]} :</td>";
					if(!isset($_GET['id']))
						echo "<td><textarea id='{$headerDB[$c]}' name='{$headerDB[$c]}'></textarea></td>";
					else
						echo "<td>&nbsp;</td>";
					echo "</tr>";
				}
				else{
					echo "
					<tr>
						<td>{$header2[$i]} :</td>";
					if(!isset($_GET['id']))
						echo "<td><input type='input' id='{$headerDB[$c]}' name='{$headerDB[$c]}' value=''></td>";
					else
						echo "<td>&nbsp;</td>";
					echo "</tr>";
				}
				$c++;
			}
		?>
		<?php
			for($i=0;$i<5;$i++){
		?>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2"><b>Professional Experience #<?php echo $i+1; ?></b></td>
		</tr>
		<?php 
			
				for($j=0;$j<count($header3);$j++){
					if($j==count($header3)-1){
						echo "
						<tr>
							<td>{$header3[$j]} :</td>";
						if(!isset($_GET['id']))
							echo "<td><textarea id='{$headerDB[$c]}' name='{$headerDB[$c]}'></textarea></td>";
						else
							echo "<td>&nbsp;</td>";
						echo "</tr>";
					}
					else{
						echo "
						<tr>
							<td>{$header3[$j]} :</td>";
						if(!isset($_GET['id']))
							echo "<td><input type='input' id='{$headerDB[$c]}' name='{$headerDB[$c]}' value=''></td>";
						else
							echo "<td>&nbsp;</td>";
						echo "</tr>";
					}
				$c++;
				}
			} 
		?>
		<?php
			for($i=0;$i<5;$i++){
		?>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2"><b>Education #<?php echo $i+1; ?></b></td>
		</tr>
		<?php 
			
				for($j=0;$j<count($header4);$j++){
					echo "
					<tr>
						<td>{$header4[$j]} :</td>";
						if(!isset($_GET['id']))
							echo "<td><input type='input' id='{$headerDB[$c]}' name='{$headerDB[$c]}' value=''></td>";
						else
							echo "<td>&nbsp;</td>";
						echo "</tr>";
					$c++;
				}
			} 
		?>
		<?php
			for($i=0;$i<5;$i++){
		?>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2"><b>Research Interests #<?php echo $i+1; ?></b></td>
		</tr>
		<?php 
			
				for($j=0;$j<count($header5);$j++){
					if($j==count($header5)-1){
						echo "
						<tr>
							<td>{$header5[$j]} :</td>";
							if(!isset($_GET['id']))
								echo "<td><textarea id='{$headerDB[$c]}' name='{$headerDB[$c]}'></textarea></td>";
							else
								echo "<td>&nbsp;</td>";
							echo "</tr>";
						}
					else{
						echo "
						<tr>
							<td>{$header5[$j]} :</td>";
							if(!isset($_GET['id']))
								echo "<td><input type='input' id='{$headerDB[$c]}' name='{$headerDB[$c]}' value=''></td>";
							else
								echo "<td>&nbsp;</td>";
							echo "</tr>";
						}
					
				$c++;
				}
			} 
		?> 
		<?php
			for($i=0;$i<5;$i++){
		?>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2"><b>Awards #<?php echo $i+1; ?></b></td>
		</tr>
		<?php 
			
				for($j=0;$j<count($header6);$j++){
					if($j==count($header6)-1){
						echo "
						<tr>
							<td>{$header6[$j]} :</td>";
							if(!isset($_GET['id']))
								echo "<td><textarea id='{$headerDB[$c]}' name='{$headerDB[$c]}'></textarea></td>";
							else
								echo "<td>&nbsp;</td>";
							echo "</tr>";
						}
					else{
						echo "
						<tr>
							<td>{$header6[$j]} :</td>";
							if(!isset($_GET['id']))
								echo "<td><input type='input' id='{$headerDB[$c]}' name='{$headerDB[$c]}' value=''></td>";
							else
								echo "<td>&nbsp;</td>";
							echo "</tr>";
						}
					
				$c++;
				}
			} 
		?>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2"><b>Your Top Three Strengths</b></td>
		</tr>
		<?php 		
			for($j=0;$j<3;$j++){
				$i=$j+1;
				echo "
				<tr>
					<td>Strength #$i :</td>";
					if(!isset($_GET['id']))
						echo "<td><input type='input' id='{$headerDB[$c]}' name='{$headerDB[$c]}' value=''></td>";
					else
						echo "<td>&nbsp;</td>";
					echo "</tr>";
					
				$c++;
			}
		?>
		
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td style="max-width:50px;"><b>What professional skills/gifts do you have that are particularly valuable to your patients?	</b></td>
			<?php if(!isset($_GET['id'])) { ?>
				<td><textarea id='SKILLSGIFTS' name='SKILLSGIFTS'></textarea></td>
			<?php }
				else{
			?>
				<td>&nbsp;</td>
			<?php } ?>
		</tr>
		
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td style="max-width:50px;"><b>What are your greatest passions in life and in your profession?</b></td>
			<?php if(!isset($_GET['id'])) { ?>
			<td><textarea id='PASSIONS' name='PASSIONS'></textarea></td>
			<?php }
				else{
			?>
				<td>&nbsp;</td>
			<?php } ?>
		</tr>
		<?php if(!isset($_GET['id'])) { ?>
		<tr>
        
        <link href="ResumeStyle.css" rel="stylesheet"></link>

        
			<td colspan="2"><br><br><input type="submit" value="Create Wotmed Resume" id='buttonsubmit'></td>
		</tr> 	 
		<?php } ?>
	</table>
<?php

echo "<br>";
	}
?>
</form>
<?php include $path."includes/p_footer.php"; ?>
</body>
</html>
