<?php
	session_start();
	require_once('../classes/getid3/getid3.php');
	include "../includes/connect.php";
	include "../includes/functions.php";
	$path="../";
	require_once $path."classes/phpuploader/include_phpuploader.php";
	
	
	if(isset($_GET['id']))
		$row=getPractitionerDetail($conn,$_GET['id']);
	else
		$row=getPractitionerDetail($conn,$_SESSION['id']);
	$rowSession=getParticipantDetail($conn,$_SESSION['id']);
	if($rowSession['PROFILEPHOTO']==""){
		$ppFileNameSession="blankSilhouetteMale.png";
	}else{
		$ppFileNameSession=$rowSession['PROFILEPHOTO'];
	}
	
	
	
	/*
	if(isset($_POST) && $_POST!=NULL)
	{		
		if(isset($_FILES) && $_FILES["flogo"]["name"]!=NULL)
		{
			$name=$_FILES["flogo"]["name"];
			$tmpname=$_FILES["flogo"]["tmp_name"];
			$size=$_FILES["flogo"]["size"];
			if (($_FILES["flogo"]["type"] == "video/x-flv") || ($_FILES["flogo"]["type"] == "video/mpeg4"))
			{
				$type="";
				switch ($_FILES["flogo"]["type"])
				{
					case "video/x-flv":
						$type=".flv";
						break;
					case "video/mpeg4":
						$type=".mp4";
						break;
				}
				$id=$row['PRACTITIONER_NUMBER'];
				$title="video_practitioner_".$id."_1".$type;
				$targetName="media/" . $title;
				$thumbsName="media/thumbs/video_practitioner_".$id."_1.jpg";
				$desc=$_POST['transcript'];
				if(move_uploaded_file($_FILES["flogo"]["tmp_name"],$targetName))
				{
					$query="UPDATE PRACTITIONER SET VIDEO='$title', VIDEOTRANSCRIPT='$desc' WHERE PRACTITIONER_NUMBER=$id";
					//echo $query."<BR>".$targetName."<BR>".$_FILES['flogo']['tmp_name']."<BR>".$_FILES['flogo']['error']."<BR>";
					@mysqli_query($conn,$query);
					exec("ffmpeg -i $targetName -ss 0 -vframes 1 -f image2 -s 425x300 $thumbsName");
					echo "<script type='text/javascript'>
							window.location = 'aboutMePractitionerVideo.php'
					</script>";
				}
			}
			else
				echo "Error".$_FILES["flogo"]["type"]."<BR>";
			
			echo "Success";
		}
	}*/
	$uploader=new PhpUploader();
	$uploader->MaxSizeKB=100000;
	$uploader->Name="myuploader";
	$uploader->InsertText="Select Video (Max 100M)";
	$uploader->AllowedFileExtensions="*.mp4,*.flv,*.f4v";
	$uploader->MultipleFilesUpload=false;
	$uploader->ManualStartUpload=true;
// $uploader->InsertButtonID="uploadbutton";
// $uploader->CancelButtonID="uploadercancelbutton";

//$uploader->ProgressPicture="phpfileuploader2015/phpuploader/resources/VideoUploadProgressButton.png";

	$uploader->UploadUrl="uploadVideo.php";
?>
<html>
<head>
	<script src="../includes/flowplayer/flowplayer-3.2.11.min.js"></script>
	<link href="<?php echo $path; ?>style/p_style.css" rel="stylesheet"></link>
	<script type="text/javascript" src="<?php echo $path; ?>classes/jquery.js"></script>
	<script language='javascript'>
		function doStartUpload()
		{
			var uploadobj = document.getElementById('myuploader');
			if (uploadobj.getqueuecount() > 0)
			{
				uploadobj.startupload();
				document.getElementById("buttonupload").disabled=true;
				document.getElementById("album").disabled=true;
			}
			else
			{
				alert("Please browse files for upload");
			}
		}
		function CuteWebUI_AjaxUploader_OnStop()
		{
			document.getElementById("buttonupload").disabled=false;
		}
		function CuteWebUI_AjaxUploader_OnPostback()
		{
			$.ajax({
			  type: "POST",
			  url: "updateTranscript.php",
			  data: { type: "v", transcript: $('#transcript').val() }
			}).done(function( msg ) {
				location.reload();
			});
			document.getElementById("buttonupload").disabled=false;
		}
		function del()
		{
			$.ajax({
			  type: "POST",
			  url: "deleteAboutMe.php",
			  data: { type: "video"}
			}).done(function( msg ) {
				location.reload();
			});
		}
	</script>
</head>
<body>
<?php include $path."includes/p_header.php"; ?>
	<h3>Practitioner About Me Video</h3>
	<?php 
	if($row['VIDEO']==null){
		if(!isset($_GET['id'])){ ?>
		<p>You currently do not have an About Video on your Wotmed profile.  You should seriously consider creating and uploading an About Video to your Wotmed profile.  <br><br>

		Your Practitioner About Video is an important part of your personal brand and your image as a Practitioner on the Wotmed platform.  A corporate video is a powerful way to promote yourself and your practice and is a great way to engage with Patients on the Wotmed platform.  You will find that adding an About Video to your Wotmed profile will improve the trust that Patients have for you as a Practitioner and is a great way to market yourself and your practice.  <br><br>

		This is your chance to showcase your talents to patients globally so upload an About Video to your Wotmed profile today!  You will find that it will dramatically improve the volume and quality of Patient enquiries. <br><br>

		Please also use the form below to create a transcript of your About Video. This is useful for Patients who may have difficulty understanding your language. We have integrated the Google Translator into the Wotmed Platform so your patients can simply click the translate button, turn the sound down and yet still understand the message of your video. <br><br>

		Creating a transcript of your About Video also helps us at Wotmed to connect you with patients and your peers who are searching for you globally.<br><br>

		Your Practitioner About Video is limited to 10 minutes in length.</p>
		<form method='post' action="" enctype="multipart/form-data">
		<table>
			<tr>
				<td>Practitioner Name :</td>
				<td><input type='text' value='<?php echo $row['PRACTITIONER_BUSINESSNAME']; ?>' id='name' value='name'></td>
			</tr>
			<tr>
				<td>File : </td>
				<td><?php $uploader->Render(); ?><br>
				</td>
				<div id="bar_blank">
					<div id="bar_color"></div>
				</div>
				<div id="status"></div>
			</tr>
			<tr>
				<td>About Me Video Transcript :</td>
				<td><textarea id='transcript' name='transcript'></textarea></td>
			</tr>
			<tr>
				<td colspan="2"><input type="button" value="Upload Video & Transcript" onClick="doStartUpload(); return false;" id="buttonupload"/></td>

            <!--    <img id="uploadbutton" alt="Upload Video & Transcript"  onClick="doStartUpload(); return false;" id="buttonupload" src="../uploadVideoAd.png" /></td>
                <img id="uploadercancelbutton" alt="Upload Video" src="../uploadVideoAdCancel.png" /> -->

			</tr>
		</table>
		</form>
		<?php } 
	}?>
	<div>
	<?php
	if($row['VIDEO']!=null){ ?>
		<p>Practitioner About Video is below.  Please click the Play button to view the video.</p>
		<?php
		$detail=explode(".",$row['VIDEO']);
		$getID3 = new getID3;
		$file = $getID3->analyze("media/".$row['VIDEO']);
		$length=$file['playtime_string'];
		echo getPlayer("media/".$row['VIDEO'],"media/thumbs/{$detail[0]}.jpg",$length); ?>
		<script type="text/javascript" src='../classes/flowplayerSetting.js' />
		<BR>
		<div id="google_translate_element"></div><BR>
		<script type="text/javascript">
			function googleTranslateElementInit() {
			  new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
			}
		</script>
		<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
		<?php
		echo "<p>".$row['VIDEOTRANSCRIPT']."</p>";
		if(!isset($_GET['id']))
			echo "<p>Please ensure that you are happy with your About Video.  If you are not happy with your video you can delete it via the link below.</p>
			<a href='#' onclick='del();return false;'><p>Click here to delete your Practitioner About Video and Video Transcript</p></a>";
		else	
			echo "<a href='media/{$row['VIDEO']}'>Download this video</a>";
	}
	else{
		if(isset($_GET['id'])){
			echo "No video uploaded for this practitioner";
		}
	}
	?>
	</div>
<?php include $path."includes/p_footer.php"; ?>
</body>
</html>
