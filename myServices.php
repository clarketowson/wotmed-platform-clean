<?php
	session_start();
	include "includes/connect.php";
	include "includes/functions.php";
	
	$connection = @mysqli_connect(DB_SERVER,DB_USER,DB_PASS) or die('Failed to connect to server');
	@mysqli_select_db($connection, DB_NAME) or die('Database not available');
	
	$rowSession=getParticipantDetail($conn,$_SESSION['id']);
	$row=getPractitionerDetail($conn,$_SESSION['id']);
	
	
	// if the logged in user is a surgery facilitator then we want to show the profile photo at left as the business logo
// otherwise the user is a practitioner and we show the profile photo on the left as the practitioner photo

if($row['ISFACILITATOR'] == 1)
{
	if($row['PRACTITIONER_BUSINESSLOGO']=="")
	{
			$ppFileNameSession="blankSilhouetteMale.png";
	}else
	
		{
			$ppFileNameSession=$row['PRACTITIONER_BUSINESSLOGO'];
		}
		
	}

else
{
	if($row['PRACTITIONER_BUSINESSLOGO']!="")
	{
		$ppFileNameSession=$row['PRACTITIONER_BUSINESSLOGO'];
		$ppFileName=$row['PRACTITIONER_BUSINESSLOGO'];
	}
	if($rowSession['PROFILEPHOTO']!="")
	{
		$ppFileNameSession=$rowSession['PROFILEPHOTO'];
	}
	
}


	
	
//	if($row['PRACTITIONER_BUSINESSLOGO']!=""){
//		$ppFileNameSession=$row['PRACTITIONER_BUSINESSLOGO'];
//		$ppFileName=$row['PRACTITIONER_BUSINESSLOGO'];
//	}
//	if($rowSession['PROFILEPHOTO']!=""){
//		$ppFileNameSession=$rowSession['PROFILEPHOTO'];
//	}
	
	
	
	
	$query="SELECT COUNT(*) FROM PATIENTPRACTITIONERRELATIONSHIP WHERE PRACTITIONER_NUMBER = '" . $_SESSION['practitioner_id'] . "' AND RECOMMENDEDAS='1'";
	$tempOfRecommend=mysqli_query($conn,$query);
	if(mysqli_num_rows($tempOfRecommend)!=0)
		$numbOfRecommend=mysqli_fetch_array($tempOfRecommend);
	else
		$numbOfRecommend[]=0;
	if(isset($_POST) && $_POST!=NULL)
	{
		if($_POST['status']=="insert")
		{
			if(isset($_POST['custome'])){
				$cat=$_POST['catId'];
				$sub=$_POST['subCatId'];
				$name=$_POST['serviceName'];

                // check the determine if the price entered is a number only with no currency symbol

                $price=$_POST['ServicePrice'];



				$id=$row["PRACTITIONER_NUMBER"];
				$query="INSERT INTO MASTERSERVICE (MASTERSUBCATEGORY_NUMBER,CATEGORY_NUMBER,TECHNICALPROCEDURENAME) VALUES ('" . $sub . "','" . $cat . "','" . $name . "');";
				mysqli_query($conn,$query);
				//echo "<BR><BR><BR><BR>".$query;
				$query = "SELECT MASTERSERVICE_NUMBER FROM MASTERSERVICE WHERE MASTERSUBCATEGORY_NUMBER = $sub AND CATEGORY_NUMBER = $cat AND TECHNICALPROCEDURENAME = '$name' ORDER BY MASTERSERVICE_NUMBER DESC";
				//echo "<BR><BR><BR><BR>".$query;
				$result=mysqli_query($conn,$query);
				if(mysqli_num_rows($result)!=0)
					$service=mysqli_fetch_array($result);
				$query="INSERT INTO SERVICES (PRACTITIONER_NUMBER,MASTERSERVICE_NUMBER,PRICE) VALUES ('" . $id . "','" . $service[0] . "','" . $price . "');";
				//echo "<BR><BR><BR><BR>".$query;
				if($service[0]!=-1){
					mysqli_query($conn,$query);
					 msgBox("The service has been successfully added to the front page of your public facing profile");
				}
			}else{
				$service=$_POST['service'];

				$price=$_POST['price'];



				$id=$row["PRACTITIONER_NUMBER"];
				$query="INSERT INTO SERVICES (PRACTITIONER_NUMBER,MASTERSERVICE_NUMBER,PRICE) VALUES ('" . $id . "','" . $service . "','" . $price . "');";
				if($service!=-1){
					mysqli_query($conn,$query);
					 msgBox("The service has been successfully added to the front page of your public facing profile");
				}
			}
		}
		if($_POST['status']=="update")
		{
			$serviceId=$_POST['serviceId'];
			$newPrice=$_POST['newPrice'];
			$id=$row["PRACTITIONER_NUMBER"];
			$query="UPDATE SERVICES SET PRICE='" . $newPrice . "' WHERE PRACTITIONER_NUMBER='" . $id . "' AND SERVICES_NUMBER='" . $serviceId . "'";
			mysqli_query($conn,$query);
			 msgBox("The service has been successfully updated on the front page of your public facing profile.");
		}
		if($_POST['status']=="delete")
		{
			$serviceId=$_POST['serviceId'];
			$id=$row["PRACTITIONER_NUMBER"];
			$query="DELETE FROM SERVICES WHERE PRACTITIONER_NUMBER='" . $id . "' AND SERVICES_NUMBER='" . $serviceId . "'";
			//msgBox($query);
			mysqli_query($connection,$query);
			msgBox("The service has been successfully removed from the front page of your public facing profile.");
		}
		?>
		<script language="javascript"> 
			<?php echo "window.location = 'myServices.php'";?>
		</script> <?php
	}
	$servicesList=getServices($conn,$row["PRACTITIONER_NUMBER"]);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link href="<?php echo $path; ?>style/p_style.css" rel="stylesheet"></link>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>My Services</title>
<link href="style/apple.css" rel="stylesheet" type="text/css" />
<link href="style/jquery.autocomplete.css" rel="stylesheet"></link>
<script type="text/javascript" src="classes/jquery.js"></script>
<script type="text/javascript" src="classes/jquery.autocomplete.js"></script>
<script type="text/javascript" src="classes/autocomplete.js"></script>
<script language="javascript"> 
	function updateServiceList(categoryNumber,subCategoryNumber)
	{
		if (window.XMLHttpRequest)
		{// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp=new XMLHttpRequest();
		}
		else
		{// code for IE6, IE5
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange=function()
		{
			if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("service").innerHTML=xmlhttp.responseText;
			}
		}
		xmlhttp.open("GET","ajax/updateServiceList.php?catNumber="+categoryNumber+"&subCatNumber="+subCategoryNumber,true);
		xmlhttp.send();
	}
	function showCustome(dive,texte){
		//alert($('#cusSerive').css( "display"));
		if($('#cusSerive').css( "display")=='none'){
			dive.css( "display", "block" );
			texte.html("Hide");
		}
		else{
			dive.css( "display", "none" );
			texte.html("Cannot find your service?<br> Add here");
		}
	}
</script>
</head>

<body>
<?php include $path."includes/p_header.php"; ?>
<?php
if($row['ISFACILITATOR'] == 1){
	$backLink = "
		<a href='facilitator/cpanel.php'>Back to Your Control Panel</a>
	";
	include $path."facilitator/cpanelHeader.php"; 
}
else{
	$backLink = "
		<a href='cpanel.php'>Back to Your Control Panel</a>
	";
	include $path."cpanelHeader.php"; 
}	
?>
<div class='lfloat' style='width:98%'>
<?php if($row['ISFACILITATOR'] == 0) { ?>
<div class="stdWrapper" onMouseOver="this.style.background='white';"onmouseout="this.style.background='#F0F0F0';">
  <p><span class="PractitionerBody"><a href="myServices.html"><img src="images/Service-icon.png" width="72" height="72" /></a><a href="myServices.html" class="hyperlinks">Create New Services</a></span></p>
  <p class="PractitionerMainText">Please follow the five steps below to add the professional services that you provide to your patients together with a corresponding list of prices.</p>
  <p class="PractitionerMainText">&nbsp; </p>
  <p class="PractitionerMainText"><strong>Step 1:</strong> Select your area of specialisation in the first drop down text field on the left.</p>
  <p class="PractitionerMainText"><strong>Step 2:</strong> Select the specialisation subcategory in the second drop down text field on the right.</p>
  <p class="PractitionerMainText"><strong>Step 3:</strong> Select the final procedure name in the third drop down text field.</p>
  <p class="PractitionerMainText"><strong>Step 4: </strong>Enter the price that you provide this procedure for in the price field.</p>
  <p class="PractitionerMainText"><strong>Step 5:</strong> Click the Add service to your Wotmed Profile button to add the procedure to your list of services in your public facing Wotmed profile.</p>
  </span>
  </td>
  <p>&nbsp;</p>
  <form action="" method="post" enctype="multipart/form-data" name="CreatePractitionerService" id="form1">
    <input type="hidden" name="status" value="insert">
    <table width="789" border="0">
      <tr>
		<td><span class="PractitionerMainText">
      <?php
		$query="SELECT * FROM MASTERCATEGORY ORDER BY CATEGORY";
		$result=mysqli_query($conn,$query);
		echo "<select id='category' onchange=\"updateServiceList(this.value,document.getElementById('subcategory').value)\">";
		echo "<option value=''>Please select your area of specialisation</option>";
		while($catList=mysqli_fetch_array($result)){
			echo "<option value='" . $catList['CATEGORY_NUMBER'] . "'>" . $catList['CATEGORY'] . "</option>";
		}
		echo "</select>";
		echo "</span></td>";
		echo "<td><span class='PractitionerMainText'>";
		$query="SELECT * FROM MASTERSUBCATEGORY ORDER BY SUBCATEGORY";
		$result=mysqli_query($conn,$query);
		echo "<select id='subcategory' onchange=\"updateServiceList(document.getElementById('category').value,this.value)\">";
		echo "<option value=''>Please select a specialisation subcategory</option>";
		while($subList=mysqli_fetch_array($result)){
			echo "<option value='" . $subList['MASTERSUBCATEGORY_NUMBER'] . "'>" . $subList['SUBCATEGORY'] . "</option>";
		}
		echo "</select>";
		?>		</tr>
	<tr>
		<td>&nbsp;</td>
		<td><span class="PractitionerMainText">
		  <table width="322" border="0">
		    <tr>
		      <td width="32">&nbsp;</td>
		      <td width="558"><span class="PractitionerMainText">
		        <select name="service" id="service" style="width:280px;">
		          <?php
			$query="SELECT MASTERSERVICE_NUMBER,TECHNICALPROCEDURENAME FROM MASTERSERVICE ORDER BY TECHNICALPROCEDURENAME";
			$result=mysqli_query($conn,$query);
			while($listOfServie=mysqli_fetch_array($result)){
				echo "<option value='" . $listOfServie['MASTERSERVICE_NUMBER'] . "'>" . $listOfServie['TECHNICALPROCEDURENAME'] . "</option>";
			}
			?>
	            </select>
		      </span></td>
	        </tr>
	      </table>
		  <p> Price : $ (do not enter currency symbol)
		    <input type="text" name="price"/>
        </p></td>
	</tr>
	<tr>
		<td><input type="submit" value="Add service to your Wotmed Profile"/>
		</span></td>
		<td> * Enter 0 if you do not want to list the price of your service</td>
	</tr>
	</form>
	<tr>
		<td colspan='2'><p>&nbsp;</p>
	    <p><a href='#' onClick="showCustome($('#cusSerive'),$('#addText'))"><span>Cannot find your service?<br> Click here to create a new service</span></a></p></td>
	</tr>
	</table>
	<div style='display:none' id='cusSerive'>
	<form action="" method="post" enctype="multipart/form-data" name="CreatePractitionerService" id="form2">
    <input type="hidden" name="status" value="insert">
	<table>
		<input type='hidden' name='custome' value='custome service'>
		<tr>
			<td>Category:</td>
			<td>
			<?php
			$query="SELECT * FROM MASTERCATEGORY ORDER BY CATEGORY";
			$result=mysqli_query($conn,$query);
			echo "<select name='catId'>";
			while($catList=mysqli_fetch_array($result)){
				echo "<option value='" . $catList['CATEGORY_NUMBER'] . "'>" . $catList['CATEGORY'] . "</option>";
			}
			echo "</select>";
			?>
			</td>
		</tr>
		<tr>
			<td>Sub Category: </td>
			<td>
			<?php
			$query="SELECT * FROM MASTERSUBCATEGORY ORDER BY SUBCATEGORY";
			$result=mysqli_query($conn,$query);
			echo "<select name='subCatId'>";
			while($subList=mysqli_fetch_array($result)){
				echo "<option value='" . $subList['MASTERSUBCATEGORY_NUMBER'] . "'>" . $subList['SUBCATEGORY'] . "</option>";
			}
			echo "</select>";
			?>
			</td>
		</tr>
		<tr>
			<td>Sevice Name: </td>
			<td><input type='text' name='serviceName'></td>
		</tr>
		<tr>
			<td>Price:</td>
			<td><input type='text' name='ServicePrice'></td>
		</tr>
		<tr>
			<td colspan=2><input type="submit" value="Create"/></td>
		</tr>
	</table>
	</div>
  </form>
</div>


<?php } ?>
<?php 
if($servicesList!=NULL)
{
	$total=mysqli_fetch_array(countServices($conn,$row["PRACTITIONER_NUMBER"]));
	//$total=5;
	$prev_index=0;
	$counter=1;
	if(!isset($_GET['detail_id']))
	{
		$data=mysqli_fetch_array($servicesList);
	}
	else
	{
		$data=mysqli_fetch_array($servicesList);
		while($data['SERVICES_NUMBER']!=$_GET['detail_id'] && $counter<$total)
		{
			$prev_index=$data['SERVICES_NUMBER'];	
			$data=mysqli_fetch_array($servicesList);
			$counter++;
			if($counter > $total[0])
				break;
		}
	}
?>
<div class="stdWrapper" onMouseOver="this.style.background='white';"onmouseout="this.style.background='#F0F0F0';">
  <p><span class="PractitionerBody"><img src="images/Service-icon.png" alt="" width="72" height="72" /><h3 class="hyperlinks">Update Existing Services</h3></span></p>
  <p class="PractitionerMainText">Use this form to update your existing Services on your public facing Wotmed profile page. </p>

  <?php 
  if($row['ISFACILITATOR'] == 1){
  	echo "<p>
	<b>IMPORTANT: </b><br>
	<br>
	Please ensure that the price that you charge your patients for your surgery facilitation services is high enough for you to provide those services.  Note carefully that Wotmed charges you a percentage of your surgery facilitation service fee and you need to ensure that you are charging your patients a price that is also high enough for you to pay this fee.<br>
	<br>
	<b>Example:</b><br>
	<br>
	You list your Surgery Facilitation Services fee as: $1,500<br>
	Wotmed fee: 20%<br>
	Wotmed fee: $300<br>
	<br>
	Your bottom line: $1,200<br>
	<br>
	You will publicly list your fee as $1,500 if you need to make $1,200 for your surgery facilitation services.<BR>
	<br>
	Important Note: the price you add here needs to be in Australian dollars AUD
	</p>
  	";
  }
  ?>
  <form action="" method="post" enctype="multipart/form-data" name="UpdatePractitionerPromotion" id="UpdatePractitionerPromotion">
	<input type="hidden" name="status" id="status" value="update">
	<?php if(isset($_GET['detail_id']))
		echo "<input type='hidden' name='serviceId' value='" . $_GET['detail_id'] . "'>";
	else
		echo "<input type='hidden' name='serviceId' value='" . $data['SERVICES_NUMBER'] . "'>";
	?>
  <table width="789" border="0">
    <tr>
        <td><span class="PractitionerMainText">Service Name</span></td>
        <td><span class="PractitionerMainText">
          <?php 
		  $query="SELECT TECHNICALPROCEDURENAME FROM MASTERSERVICE WHERE MASTERSERVICE_NUMBER='" . $data['MASTERSERVICE_NUMBER'] . "'";
			$result=mysqli_query($conn,$query);
			$techName=mysqli_fetch_array($result);
			echo $techName[0];
		  ?>
        </span></td>
      </tr>
      <tr>
        <td><span class="PractitionerMainText">Price</span></td>
        <td><span class="PractitionerMainText">
          <input name="newPrice" type="text" id="newPrice" size="40" maxlength="40" value="<?php 
          $tempPrice=$data['PRICE'];
          echo substr($tempPrice,0,strlen($tempPrice)-3);?>" />
        </span></td>
      </tr>
	  <tr>
		<td>&nbsp;</td>
		<?php
		if($row['ISFACILITATOR'] == 0){?>
		<td> * set 0 if you don't want to list your price </td>
		<?php } ?>
	  </td>
  </table>
	<p>
	<?php if($prev_index != 0){ ?>
	<a href="myServices.php?detail_id=<?php echo $prev_index;?>"><< Prev</a>
	&nbsp;&nbsp;&nbsp;
	<?php } 
	if($total>1 && $counter<$total[0]){ 
		$data=mysqli_fetch_array($servicesList);
	$next_index=$data['SERVICES_NUMBER'];?>
	<a href="myServices.php?detail_id=<?php echo $next_index;?>">Next >></a>
	<?php } ?>
	</p>
	<p class="PractitionerMainText">
    <label>
		<input type="submit" name="updateService" id="updateService" value="Update Service" />
    </label>
		<input type="submit" name="deleteService" id="deleteService" value="Delete Service" onclick=	"javascript:document.getElementById('status').value='delete'" />
  </form>
</div>
<?php } 
mysqli_close($connection);?>
</div>
<p>&nbsp;</p>
<?php include $path."includes/p_footer.php"; ?>
</body>

</html>
